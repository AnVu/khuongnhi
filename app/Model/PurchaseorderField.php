<?php

$ModuleField = array();
$ModuleField = array(
    'module_name' => __('Purchaseorder'),
    'module_label' => __('Purchase Order'),
    'colection' => 'tb_purchaseorder',
    'title_field' => array('company_name', 'contact_name', 'invoice_status', 'our_rep'),
);


//============= *** FIELDS *** =============//
// Panel 1
$ModuleField['field']['panel_1'] = array(
    'setup' => array(
        'css' => 'width:100%;',
        'lablewith' => '25',
        'blockcss' => 'width:30%;float:left;',
    ),
    'code' => array(
        'name' => __('Ref no'),
        'type' => 'text',
        'css' => 'padding-left:2%;',
        'moreclass' => 'fixbor',
        'listview' => array(
            'order' => '1',
            'css' => 'width:5%;',
            'sort' => '1',
        ),
    ),
    'mongo_id' => array(
        'type' => 'id',
        'element_input' => ' class="jthidden"',
    ),
    'date_modified' => array(
        'type' => 'hidden',
    ),
    'created_by' => array(
        'type' => 'hidden',
    ),
    'modified_by' => array(
        'type' => 'hidden',
    ),
    'description' => array(
        'name' => __('Description'),
        'type' => 'hidden',
    ),
    'company_name' => array(
        'name' => __('Company'),
        'type' => 'relationship',
        'cls' => 'companies',
        'id' => 'company_id',
        'css' => 'padding-left:2%;',
        'para' => ',"?is_supplier=1"',
        'lock' => '0',
        'listview' => array(
            'order' => '2',
            'with' => '15',
            'css' => 'width:15%;',
            'sort' => '1',
        ),
    ),
    'company_id' => array(
        'type' => 'id',
        'element_input' => ' class="jthidden"',
    ),
    'contact_name' => array(
        'name' => __('Contact'),
        'type' => 'relationship',
        'cls' => 'contacts',
        'id' => 'contact_id',
        'css' => 'padding-left:2%;',
        'para' => ',get_para_contact()',
    ),
    'contact_id' => array(
        'type' => 'id',
        'element_input' => ' class="jthidden"',
    ),
    'contact_last_name' => array(
        'type' => 'hidden'
    ),
    'phone' => array(
        'name' => __('Phone'),
        'type' => 'phone',
        'css' => 'padding-left:2%;',
    ),
    'fax' => array(
        'name' => __('Fax'),
        'type' => 'phone',
        'css' => 'padding-left:2%;',
    ),
    'email' => array(
        'name' => __('Email'),
        'type' => 'email',
        'css' => 'padding-left:2%;',
    ),
    'purchord_date' => array(
        'name' => __('Date'),
        'type' => 'date',
        'css' => 'padding-left:2%;',
        'listview' => array(
            'order' => '3',
            'with' => '15',
            'css' => 'width:5%;',
            'sort' => '1',
        ),
    ),
    'our_rep' => array(
        'name' => __('Our rep'),
        'type' => 'relationship',
        'cls' => 'contacts',
        'id' => 'our_rep_id',
        'para' => ',get_para_employee()',
        'not_custom' => '1',
    ),
    'our_rep_id' => array(
        'type' => 'id',
        'element_input' => ' class="jthidden"',
    ),
    'none' => array(
        'type' => 'not_in_data',
        'moreclass' => 'fixbor2',
    ),
);

// Panel 3
$ModuleField['field']['panel_2'] = array(
    /*'setup' => array(
        'css' => 'width:33%;',
        'lablewith' => '45', //%
        'blockcss' => 'width:69%;float:right;',
    ),
    'name' => array(
        'name' => __('Heading'),
        'type' => 'text',
        'moreclass' => 'fixbor',
    ),
    'required_date' => array(
        'name' => __('Required date'),
        'type' => 'date',
        'moreclass' => 'fixbor3',
    ),
    'supplier_quote_ref' => array(
        'name' => __('Supplier quote ref'),
        'type' => 'text',
    ),
    'ship_to_company_name' => array(
        'name' => __('Ship to: Company'),
        'type' => 'relationship',
        'cls' => 'companies',
        'id' => 'ship_to_company_id',
        'css' => 'padding-left:2%;',
        'para' => ',"?name=anvy"',
        'lock' => '0',
        'not_custom' => '1',
    ),
    'ship_to_company_id' => array(
        'type' => 'id',
        'element_input' => ' class="jthidden"',
    ),
    'ship_to_contact_name' => array(
        'name' => __('Ship to: Contact'),
        'type' => 'relationship',
        'cls' => 'contacts',
        'id' => 'ship_to_contact_id',
        'css' => 'padding-left:2%;',
        'para' => ',get_para_ship_contact()',
        'lock' => '0',
        'not_custom' => '1',
    ),
    'ship_to_contact_id' => array(
        'type' => 'id',
        'element_input' => ' class="jthidden"',
    ),
    'tracking_no' => array(
        'name' => __('Tracking no'),
        'type' => 'text',
    ),
    'shipper_company_name' => array(
        'name' => __('Shipper'),
        'type' => 'relationship',
        'cls' => 'companies',
        'id' => 'shipper_company_id',
        'css' => 'padding-left:2%;',
        'para' => ',get_company_is_shipper()',
        'lock' => '0',
        'not_custom' => '1',
    ),
    'shipper_company_id' => array(
        'type' => 'id',
        'element_input' => ' class="jthidden"',
    ),
    'none4' => array(
        'type' => 'not_in_data',
    ),
    'none5' => array(
        'type' => 'not_in_data',
        'moreclass' => 'fixbor2'
    ),*/
);

// Panel 3
$ModuleField['field']['panel_3'] = array(
/*    'setup' => array(
        'css' => 'width:35%;',
        'lablewith' => '35', //%
        'blockcss' => 'width:35%;float:right;',
        'blocktype' => 'address',
    ),*/
    'setup' => array(
        'css' => 'width:33%;',
        'lablewith' => '45', //%
        'blockcss' => 'width:69%;float:right;',
        'blocktype' => 'address',
    ),
    'shipping_address' => array(
        'name' => __('Shipping address'),
        'type' => 'text',
    ),
);


// Panel 4
$ModuleField['field']['panel_4'] = array(
    'setup' => array(
        'css' => 'width:33%;',
        'lablewith' => '35',
    ),
    'purchase_orders_status' => array(
        'name' => __('Status'),
        'type' => 'select',
        'droplist' => 'purchase_orders_status',
        'default' => 'Mới',
        'css' => 'width:100%;padding-left:3%;',
        'not_custom' => '1',
        'listview' => array(
            'order' => '5',
            'with' => '5',
            'css' => 'width:5%;',
            'sort' => '1',
        ),
    ),
    'none' => array(
        'type' => 'not_in_data',
    ),

    'none1' => array(
        'type' => 'not_in_data',
    ),
    'none2' => array(
        'type' => 'not_in_data',
    ),
    'none3' => array(
        'type' => 'not_in_data',
    ),
    'none4' => array(
        'type' => 'not_in_data',
    ),
    'none5' => array(
        'type' => 'not_in_data',
    ),
    'none6' => array(
        'type' => 'not_in_data',
    ),
    'none8' => array(
        'type' => 'not_in_data',
    ),
);

$ModuleField['field']['panel_5'] = array(
    'sum_amount' => array(
        'type' => 'price',
        'name' => __('Tổng tiền toa hàng'),
        'element_input' => ' class="jthidden"',
        'listview' => array(
            'order' => '4',
            'css' => 'width:10%; text-align: right',
            'sort' => 1,
            'width' => '15',
        ),
    ),
);



//============ *** RELATIONSHIP *** =============//
//====== LINE ENTRY =======//
$ModuleField['relationship']['line_entry']['name'] = __('Line entry');

//Line entry Details
$ModuleField['relationship']['line_entry']['block']['products'] = array(
    'title' => __('Details'),
    'type' => 'listview_box',
    'css' => 'width:100%;margin-top:0;',
    'height' => '600',//282
    'add' => __('Add line'),
    'custom_box_bottom' => '1',
    'custom_box_top' => '1',
    'link' => array('w' => '1', 'cls' => 'products'),
    'reltb' => 'tb_purchaseorder@products', //tb@option
    'delete' => '1',
    'field'=> array(
            'code' => array(
                'name'      =>  __('Code'),
                'type'  => 'hidden',
            ),
            'sku' => array(
                'name'      =>  __('SKU'),
                'type'  => 'link_icon',
                'link_field'    => 'products_id',
                'module_rel'    => 'products',
                'popup_title'   => 'Specify Products',
                'popup_key' => 'change',
                'width'=>7,
                'align' => 'left',
                'indata' => '0',
                'edit'=>'1',
                'para'=>'"?no_supplier=1&products_product_type=Product"',
            ),
            'products_name' => array(
                'name'      =>  __('Tên sản phẩm'),
                'width'=>35,
                'edit'  => '0',
                'default'=> 'Click for edit',
                //comment
            ),
            'products_id' => array(
                'name'      =>  __('Products ID'),
                'type' =>'hidden',
            ),
            'oum'       =>array(
                'name'      =>  __('Đơn vị tính'),
                'type'      => 'select',
                'droplist' => 'product_oum_unit',
                'default' => 'unit',
                'element_input' => 'combobox_blank="1"',
                'width'     =>8,
                'edit'      => '1',
                ),
            'specification'     =>array(
                'name'      =>  __('Quy cách'),
                'type'      => 'select',
                'width'     => 10,
                'default'   => '',
                'element_input' => 'combobox_blank="1"',
                'droplist'  => 'product_specific',
                'edit'      => '1',
                ),

            'prime_cost'    =>array(
                'name'      =>  __('Đơn giá'),
                'type'      => 'price',
                'width'     => 7,
                'numformat'=>0,
                'align'     => 'right',
                'edit'      => '1',
                ),
            'quantity' => array(
                'name'      =>  __('Quantity'),
                'type' => 'price',
                'align' => 'right',
                'width'=>'6',
                'edit'  => '1',
                'numformat' => 0,
                'isInt' => '1',
                'default'=> '1',
            ),
            /*'option' => array(
                'name'  =>  __('Số lượng trả'),
                'type'  => 'link_plus',
                'width' => '5',
                'align' => 'center',
            ),*/
            'amount' => array(
                'name'      =>  __('Thành tiền'),
                'width'=>7,
                'align' => 'right',
                'default'=> '0',
                'type' => 'price',
                'numformat' => 0,
            ),
        ),
);


//====== TEXT ENTRY =======//
$ModuleField['relationship']['text_entry']['name'] = __('Text entry');
$ModuleField['relationship']['text_entry']['hidden'] =  true;


//Text entry Details
$ModuleField['relationship']['text_entry']['block']['products'] = array(
    'title' => __('Details'),
    'type' => 'listview_box',
    'css' => 'width:100%;margin-top:0;',
    'height' => '264',
    'add' => __('Add line'),
    'custom_box_bottom' => '1',
    'custom_box_top' => '1',
    'link' => array('w' => '1', 'cls' => 'products'),
    'reltb' => 'tb_purchaseorder@products', //tb@option
    'delete' => '1',
    'linecss' => 'h_entry',
    'cellcss' => 'h_entryin',
	'full_height' => '1',
    'field' => array(
        'products_name' => array(
            'name' => __('Name / details'),
            'width' => '33',
            'edit' => '1',
            'type' => 'textarea',
            'default' => 'Click for edit',
        ),
        'products_id' => array(
            'name' => __('Products ID'),
            'type' => 'id',
        ),
        /* 'option' => array(
          'name' 		=>  __('Option'),
          'width'=>'7',
          'type' => 'text',
          ), */
        'sizew' => array(
            'name' => __('Size-W'),
            'type' => 'price',
            'width' => '3',
            'edit' => '1',
            'mod' => 'text',
        ),
        'sizew_unit' => array(
            'name' => __(''),
            'type' => 'select',
            'droplist' => 'product_oum_size',
            'default' => 'inch',
            'not_custom' => '1',
            'edit' => '1',
            'width' => '3',
            'mod' => 'text',
        ),
        'sizeh' => array(
            'name' => __('Size-H'),
            'type' => 'price',
            'width' => '3',
            'edit' => '1',
            'mod' => 'text',
        ),
        'sizeh_unit' => array(
            'name' => __(''),
            'type' => 'select',
            'droplist' => 'product_oum_size',
            'default' => 'inch',
            'not_custom' => '1',
            'edit' => '1',
            'width' => '3',
            'mod' => 'text',
        ),
        'sell_by' => array(
            'name' => __('Sold by'),
            'type' => 'select',
            'width' => '3',
            'droplist' => 'product_sell_by',
            'edit' => '1',
            'mod' => 'text',
        ),
        'sell_price' => array(
            'name' => __('Cost price'),
            'type' => 'price',
            'width' => '5',
            'align' => 'right',
            'edit' => '1',
            'mod' => 'text',
        ),
        'oum' => array(
            'name' => __(''),
            'type' => 'select_dynamic',
            'droplist' => 'product_oum_area',
            'width' => '3',
            'edit' => '1',
            'mod' => 'text',
        ),
        'unit_price' => array(
            'name' => __('Unit price'),
            'type' => 'price',
            'width' => '5',
            'align' => 'right',
            'mod' => 'text',
            'isInt'=>1,
            'numformat'=>4,
        ),
        'quantity' => array(
            'name' => __('Quantity'),
            'type' => 'price',
            'align' => 'right',
            'width' => '3',
            'edit' => '1',
            'default' => '0',
            'mod' => 'text',
            'isInt'=>'1',
            'numformat'=>0,
        ),
        'sub_total' => array(
            'name' => __('Sub total'),
            'width' => '5',
            'align' => 'right',
            'type' => 'text',
            'mod' => 'text',
        ),
        'taxper' => array(
            'name' => __('Tax %'),
            'type' => 'hidden',
            'mod' => 'text',
        ),
        'tax' => array(
            'name' => __('Tax'),
            'indata' => '0',
            'width' => '5',
            'align' => 'right',
            'mod' => 'text',
        ),
        'amount' => array(
            'name' => __('Amount'),
            'width' => '7',
            'align' => 'right',
            'mod' => 'text',
        ),
    ),
);

//====== Shipping received =======//

$ModuleField['relationship']['shipping_received']['name'] = __('Shipping / received');
$ModuleField['relationship']['shipping_received']['hidden'] =  true;

$ModuleField['relationship']['shipping_received']['block']['shipping'] = array(
    'title' => __('Shipping (items shipped / received / returned) related to this purchase order'),
    'type' => 'listview_box',
    'css' => 'width:100%;margin-top:0;',
    'height' => '150',
    'custom_box_top' => '1',
    'link'      => array('w'=>'1', 'cls'=>'shippings'),
    'reltb' => 'tb_purchaseorder@shipping', //tb@option
    'delete' => '0',
    'field' => array(
        'no' => array(
            'name' => __('Ref no'),
            'align' => 'center',
            'edit' => '0',
            'width' => '3',
        ),
        'type' => array(
            'name' => __('Type'),
            'align'=>'center',
            'width' => '3',
            'type' => 'text',
            'cls'=>'shipping_type'

        ),
        'return' => array(
            'name' => __('Return'),
            'align'=>'center',
            'width' => '4',
            'type'=>'checkbox',
            'edit' => '0',

        ),
        'shipping_date' => array(
            'name' => __('Date'),
            'align'=>'center',
            'width' => '5',
            'edit' => '0',

        ),
        'our_rep' => array(
            'name' => __('Our rep'),
            'width' => '6',
            'edit' => '0',
        ),
        'carrier' => array(
            'name' => __('Carrier'),
            'width' => '8',
            'edit' => '0',
        ),
        'tracking_no' => array(
            'name' => __('Tracking no'),
            'width' => '8',
            'edit' => '0',
        ),
        'heading' => array(
            'name' => __('Heading'),
            'width' => '16',
            'edit' => '0',
        ),
        'status' => array(
            'name' => __('Status'),
            'width' => '4',
            'edit' => '0',
        ),
    )
);


//====== Supplier invoice =======//
$ModuleField['relationship']['supplier_invoice']['name'] = __('Supplier Invoice');
$ModuleField['relationship']['supplier_invoice']['hidden'] =  true;

$ModuleField['relationship']['supplier_invoice']['block']['invoice'] = array(
    'title' => __('Supplier invoice received linked to this order'),
    'type' => 'listview_box',
    'css' => 'width:100%;margin-top:0;',
    'height' => '150',
    'add' => __('Create supplier invoice'),
   // 'reltb' => 'tb_purchaseorder@invoice', //tb@option
    'delete' => '1',
    'field' => array(
        'no' => array(
            'name' => __('Supplier invoice no'),
            'align' => 'left',
            'edit' => '1',
            'width' => '10',
        ),
        'invoice_day' => array(
            'name' => __('Date'),
            'width' => '5',
            'type' => 'text',
        ),
        'type' => array(
            'name' => __('Type'),
            'width' => '4',
            'edit' => '1',
        ),
        'status' => array(
            'name' => __('Status'),
            'width' => '4',
            'edit' => '1',
            'type' => 'select',
            'droplist' => 'product_oum_size',
        ),
        'terms' => array(
            'name' => __('Terms'),
            'width' => '3',
            'edit' => '1',
        ),
        'due_1' => array(
            'name' => __('Due'),
            'width' => '4',
            'edit' => '1',
        ),
        'day_left' => array(
            'name' => __('Day left'),
            'width' => '4',
            'edit' => '1',
        ),
        'due_2' => array(
            'name' => __('Due'),
            'width' => '2',
            'edit' => '1',
            'align' => 'center',
            'type' => 'checkbox'
        ),
        'note' => array(
            'name' => __('Notes'),
            'width' => '10',
            'edit' => '1',
        ),
        'nc' => array(
            'name' => __('N/C'),
            'width' => '4',
            'edit' => '1',
        ),
        'amount' => array(
            'name' => __('Amount'),
            'width' => '4',
            'edit' => '1',
            'align' => 'right'
        ),
        'tax' => array(
            'name' => __('Tax %'),
            'width' => '3',
            'edit' => '1',
            'align' => 'center'
        ),
        'total_inc_tax' => array(
            'name' => __('Total inc tax'),
            'width' => '6',
            'edit' => '1',
            'align' => 'right'
        ),
        'approved' => array(
            'name' => __('Approved'),
            'width' => '4',
            'edit' => '1',
            'type' => 'checkbox'
        ),
        'approved_by' => array(
            'name' => __('Approved by'),
            'width' => '6',
            'edit' => '1',
        ),
        'paid' => array(
            'name' => __('paid'),
            'width' => '4',
            'edit' => '1',
            'align' => 'right'
        ),
    )
);

//====== DOCUMENTS =======//
$ModuleField['relationship']['tasks']['name'] = __('Tasks');
$ModuleField['relationship']['tasks']['hidden'] =  true;


//====== DOCUMENTS =======//
$ModuleField['relationship']['documents']['name'] = __('Documents');
$ModuleField['relationship']['documents']['hidden'] =  true;


$ModuleField['relationship']['documents']['block']['docs'] = array(
    'title' => __('Document / file management'),
    'type' => 'listview_box',
    'css' => 'width:100%;margin-top:0;',
    'height' => '150',
    'add' => __('Add document'),
    'link' => array('w' => '2', 'cls' => 'docs'),
    'reltb' => 'tb_document@docs', //tb@option
    'delete' => '1',
    'field' => array(
        'docs_id' => array(
            'name' => __('Document ID'),
            'type' => 'id',
        ),
        'file_name' => array(
            'name' => __('Document / file name'),
            'width' => '26',
            'indata' => '0',
        ),
        'location' => array(
            'name' => __('Location'),
            'width' => '10',
            'indata' => '0',
        ),
        'error' => array(
            'name' => __('Error'),
            'width' => '9',
            'indata' => '0',
        ),
        'category' => array(
            'name' => __('Category'),
            'width' => '8',
            'indata' => '0',
        ),
        'ext' => array(
            'name' => __('Ext'),
            'type' => 'checkbox',
            'width' => '3',
            'indata' => '0',
        ),
        'types' => array(
            'name' => __('Type'),
            'width' => '5',
            'indata' => '0',
        ),
        'version' => array(
            'name' => __('Version'),
            'indata' => '0',
            'width' => '5',
        ),
        'description' => array(
            'name' => __('Description'),
            'indata' => '0',
            'width' => '20',
        ),
    ),
);

//====== OTHER =======//
$ModuleField['relationship']['other']['name'] = __('Other');
$ModuleField['relationship']['other']['hidden'] =  true;

//Line entry Details
$ModuleField['relationship']['other']['block']['other'] = array(
    'title' => __('Other'),
    'type' => 'listview_box',
    'css' => 'width:100%;margin-top:0;',
    'height' => '150',
    'add' => __('Add document'),
    'link' => array('w' => '2', 'cls' => 'docs'),
    'reltb' => 'tb_document@docs', //tb@option
    'delete' => '1',
    'field' => array(
        'docs_id' => array(
            'name' => __('Document ID'),
            'type' => 'id',
        ),
        'file_name' => array(
            'name' => __('Document / file name'),
            'width' => '26',
            'indata' => '0',
        ),
        'location' => array(
            'name' => __('Location'),
            'width' => '10',
            'indata' => '0',
        ),
        'error' => array(
            'name' => __('Error'),
            'width' => '9',
            'indata' => '0',
        ),
        'category' => array(
            'name' => __('Category'),
            'width' => '8',
            'indata' => '0',
        ),
        'ext' => array(
            'name' => __('Ext'),
            'type' => 'checkbox',
            'width' => '3',
            'indata' => '0',
        ),
        'types' => array(
            'name' => __('Type'),
            'width' => '5',
            'indata' => '0',
        ),
        'version' => array(
            'name' => __('Version'),
            'indata' => '0',
            'width' => '5',
        ),
        'description' => array(
            'name' => __('Description'),
            'indata' => '0',
            'width' => '20',
        ),
    ),
);
$PurchaseorderField = $ModuleField;
