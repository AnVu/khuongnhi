<?php
$ModuleField = array();
$ModuleField = array(
	'module_name' 	=> __('Task'),
	'module_label' 	=> __('Tasks'),
	'colection' 	=> 'tb_task',
	'title_field'	=> array('code','name','type','our_rep'),
);


//============= *** FIELDS *** =============//

// Panel 1
$ModuleField['field']['panel_1'] = array(
	'setup'	=> array(
			'css'	=> 'width:100%;',
			'lablewith' => '29',
			'blockcss' => 'width:27%;float:left;',
	),
	'code' => array(
		'name' 		=> __('Task no'),
		'type' 		=> 'text',
		'moreclass' => 'fixbor',
		'lock' => '1',
		'listview'	=>	array(
						'order'	=>	'1',
						'with'	=>	'100',
						'css'	=>	'width:5%;',
						'align'	=>	'center',
					),
	),
	'name' => array(
		'name' 		=> __('Heading'),
		'type' 		=> 'text',
		'listview'	=>	array(
						'order'	=>	'1',
						'with'	=>	'8',
						'css'	=>	'width:8%;',
					),
	),
	'type'	=>array(
			'name' 		=> __('Task type '),
			'type' 		=> 'select',
			'droplist' => 'enquirynote_status',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'8',
							'css'	=>	'width:4%;',
						),
	),
	'type_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
	),
	'responsible'	=> array(
			'name' 		=>  __('Responsible'),
			'type' 		=> 'relationship',
			'cls'		=> 'companies',
			'id'		=> 'company_id',
			'css'		=> 'padding-left:2%;',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'15',
							'css'	=>	'width:15%;',
							'sort'=> '1',
			),

	),
	'responsible_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
	),
	'mongo_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
			),
	'none'	=>array(
		'type' 		=> 'not_in_data',
		'moreclass' => 'fixbor2',
	),
);



$ModuleField['field']['panel_2'] = array(
	'setup'	=> array(
			'css'	=> 'width:33.33%;',  // <- 70%
			'lablewith' => '37',//%
			'blockcss' => 'width:71.5%;float:right;',
			'blocktype'=> '',
	),
	'work_start'	=>array(
			'name' 		=> __('Work start'),
			'type' 		=> 'datetime',
			'default' => '',
			'fulltime'	=>  '1',
			'moreclass' => 'fixbor1',
	),
	'work_end'	=>array(
			'name' 		=> __('Work end'),
			'type' 		=> 'datetime',
	),
	'priority' => array(
			'name' 		=> __('Priority'),
			'type' 		=> 'select',
			'droplist' => 'case_priority',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'8',
							'css'	=>	'width:4%;',
						),
	),
	'status'	=>array(
			'name' 		=> __('Status'),
			'type' 		=> 'select',
			'droplist' => 'enquirynote_status',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'8',
							'css'	=>	'width:4%;',
						),
	),
	'Days left'	=>array(
			'name' 		=> __('Days left'),
			'type' 		=> 'text',
			'moreclass' => 'fixbor2',
	),
);

// Panel 3
$ModuleField['field']['panel_3'] = array(
	'setup'	=> array(
			'css'	=> 'width:33.33%;',
			'lablewith' => '37',
			'blockcss' => 'width:32%;float:left;',
			),
	'company'	=> array(
			'name' 		=>  __('Company'),
			'type' 		=> 'relationship',
			'cls'		=> 'companies',
			'id'		=> 'company_id',
			'css'		=> 'padding-left:2%;',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'15',
							'css'	=>	'width:15%;',
							'sort'=> '1',
			),

	),
	'company_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
	),
	'contact_name'	=>array(
			'name' 		=>  __('Contact'),
			'type' 		=> 'relationship',
			'cls'		=> 'companies',
			'id'		=> 'contact_id',
			'css'		=> 'padding-left:2%;',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'8',
							'css'	=>	'width:8%;',
							'sort'=> '1',
						),
	),
	'contact_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
	),
	'none'	=>array(
		'type' 		=> 'not_in_data',
		),
	'none1'	=>array(
		'type' 		=> 'not_in_data',
		),
	'none2'	=>array(
		'type' 		=> 'not_in_data',
		),
);




// Panel 4
$ModuleField['field']['panel_4'] = array(
	'setup'	=> array(
			'css'	=> 'width:33.33%;',
			'lablewith' => '37',
			'blockcss' => 'width:49%;float:left;',
	),
	'enquiry'	=> array(
			'name' 		=>  __('Enquiry'),
			'type' 		=> 'relationship',
			'cls'		=> 'enquiries',
			'id'		=> 'enquiry_id',
			'css'		=> 'padding-left:2%;',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'15',
							'css'	=>	'width:15%;',
							'sort'=> '1',
			),
	),
	'enquiry_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
	),

	'quotation'	=> array(
			'name' 		=>  __(' Quotation'),
			'type' 		=> 'relationship',
			'cls'		=> 'quotations',
			'id'		=> 'quotation_id',
			'css'		=> 'padding-left:2%;',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'15',
							'css'	=>	'width:15%;',
							'sort'=> '1',
			),
	),
	'quotation_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
	),

	'job'	=> array(
			'name' 		=>  __(' Job'),
			'type' 		=> 'relationship',
			'cls'		=> 'jobs',
			'id'		=> 'job_id',
			'css'		=> 'padding-left:2%;',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'15',
							'css'	=>	'width:15%;',
							'sort'=> '1',
			),
	),
	'job_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
	),

	'sales_order'	=> array(
			'name' 		=>  __('Sales order'),
			'type' 		=> 'relationship',
			'cls'		=> 'sales_orders',
			'id'		=> 'sales_order_id',
			'css'		=> 'padding-left:2%;',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'15',
							'css'	=>	'width:15%;',
							'sort'=> '1',
			),
	),
	'sales_order_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
	),

	'purchase_order'	=> array(
			'name' 		=>  __(' Purchase Order'),
			'type' 		=> 'relationship',
			'cls'		=> 'purchase_orders',
			'id'		=> 'purchase_order_id',
			'css'		=> 'padding-left:2%;',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'15',
							'css'	=>	'width:15%;',
							'sort'=> '1',
			),
	),
	'purchase_order_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
	),

);





//============ *** RELATIONSHIP *** =============//



//====== General =======//
$ModuleField['relationship']['general']['name'] =  __('General'); // other cua salesinvoice

//Line entry Details
$ModuleField['relationship']['general']['block']['note_activity'] = array(
	'title'	=>__('Note & activities'),
	'type'	=>'listview_box',
	'css'	=>'width:47%;margin-top:0;',
	'height' => '320',
	'add'	=> __('Add new line'),
	'reltb'		=> 'tb_basic@note_activity',//tb@option
	'delete' => '3',
	'field'=> array(
				'note_type' => array(
					/*'name' 		=>  __('Type'),
					'type'	=> 'select',
					'droplist'	=> 'com_type',
					'width' => '10',*/
					'name' => __('Type xyz'),
					'width' => '10',
		            'type'=>'select',
					'droplist' => 'note_type',
					//'edit' => '1',
					'not_custom'=>'1',
				),
				'note_dates' => array(
					'name' 		=>  __('Date'),
					'type'	=> 'text',
					'width' => '10',
				),
				'note_by' => array(
					'name' => __('By'),
					'type' 		=> 'relationship',
					'cls'		=> 'contacts', // chi dinh loai popup
					'id'		=> 'note_by_id',
					'para'		=> ',get_para_employee()',
					//'not_custom'=> '1',
					'edit'	=> '1',
					'width' => '20',
					'syncname'	=> 'first_name',
					'listview'	=>	array(
									'order'	=>	'1',
									'css'	=>	'width:30%;',
									'sort'=> '1',
								),
				),

				'note_by_id' => array(
					'name' => __('By ID'),
					'type' => 'id',
				),
				'note_details' => array(
					'name' 		=>  __('Details'),
					'width' => '51',
					'type'	=> 'text',
					'edit'	=> '1',
				),
			),
);

$ModuleField['relationship']['general']['block']['contacts_task'] = array(
		'title'	=>__('Contacts related to this task'),
		'type'	=>'listview_box',
		'css'	=>'width:100%;float: left; padding-left:6px',
		'height' => '131',
		'link' => array('w' => '2', 'cls' => 'contacts','field'=>'contact_id'),
		'add'	=> __('Add new line'),
		'reltb'		=> 'tb_task@contacts_task',//tb@option
		'field'=> array(
			'contact_id' => array(
		            'name' => __('ID'),
					'type'=>'id',
				),
			'contact_name'	=> array(
						'name' =>'Contact',
						'type'	=> 'text',
						'align' => 'center',
						'width' => '59',
					),
			'manager' => array(
						'name' 		=>  __('Manager'),
						'width' => '25',
						'type' 		=> 'checkbox',
						'align' => 'center',
						'edit'	=> '1',
					),
		),
);
$ModuleField['relationship']['general']['block']['alerts'] = array(
		'title'	=>__('Alerts'),
		'type'	=>'listview_box',
		'css'	=>'width:100%;float: left; padding-left:6px; padding-top:6px',
		'height' => '131',
		'link' => array('w' => '2', 'cls' => 'contacts','field'=>'contact_id'),
		'reltb'		=> 'tb_task@alerts',//tb@option
		'field'=> array(
			'contact_id' => array(
		            'name' => __('ID'),
					'type'=>'id',
				),
			'contact_name'	=> array(
						'name' =>'Contact',
						'type'	=> 'text',
						'align' => 'center',
						'width' => '59',
					),
			'manager' => array(
						'name' 		=>  __('Manager'),
						'width' => '25',
						'type' 		=> 'checkbox',
						'align' => 'center',
						'edit'	=> '1',
					),
		),
);
$ModuleField['relationship']['general']['block']['timelog'] = array(
	'title' => 'Timelog budget related',
    'css' => 'width:25%;float:right',
    'height' => '313',
    'reltb' => 'tb_salesinvoice@products', //tb@option
    'type' => 'editview_box',
    'field' => array(
    	   'x1' => array(
		            'name' => __(''),
		            'width' => '2',
		            'type' => 'text',
		            'edit' => '1'
	        ),
    	    'x2' => array(
		            'name' => __(''),
		            'width' => '2',
		            'type' => 'text',
		            'edit' => '1',
	        ),
	        'x3' => array(
					'name' 		=>  __(''),
					'type' => 'text',
					'width'		=> '10',
					'edit'		=> '1',
			),
			'x4' => array(
					'name' 		=>  __(''),
					'width'		=> '10',
					'type' => 'text',
					'edit'		=> '1',

			),
			'x5' => array(
					'name' 		=>  __(''),
					'width'		=> '10',
					'type' => 'text',
					'edit'		=> '1',

			),
			'x6' => array(
					'name' 		=>  __(''),
					'width'		=> '10',
					'type' => 'text',
					'edit'		=> '1',

			),
			'x7' => array(
					'name' 		=>  __(''),
					'width'		=> '10',
					'type' => 'text',
					'edit'		=> '1',

			),
			'x8' => array(
					'name' 		=>  __(''),
					'width'		=> '10',
					'type' => 'text',
					'edit'		=> '1',

			),
			'x9' => array(
					'name' 		=>  __(''),
					'width'		=> '10',
					'type' => 'text',
					'edit'		=> '1',

			),
    ),
);



//====== Resources =======//
$ModuleField['relationship']['resource']['name'] =  __('Resources');

//Option list data
$ModuleField['relationship']['resource']['block']['resource'] = array(
	'title'	=>__('Details'),
	'type'	=>'listview_box',
	'link' => array('w' => '1', 'cls' => 'resources','field'=>'_id'),
	'css'	=>'width:100%;margin-top:0;',
	'height' => '220',
	'reltb'		=> 'tb_resource@resource',//tb@option
	'custom_box_top' => '1',
	'field'=> array(
				'_id' => array(
		            'name' => __('ID'),
					'type'=>'id',
				),
				'type' => array(
					'name' 		=>  __('Type'),
					'width'		=> '10',
				),
				'name' => array(
					'name' 		=>  __('Name'),
					'width'		=> '15',
				),

				'work_start' => array(
					'name' 		=>  __('Start (schedule)'),
					'type' 		=> 'date',
					'fulltime'	=>  '1',
					'edit'		=> '1',
					'not_custom'=> '1',
					'width'		=> '10',
				),
				/*'time_start' => array(
					'name' 		=>  __('Time start'),
					'width'		=> '5',
					'type' 		=> 'select',
					'droplist' => 'equipments_status',
					'edit'	=> '1',
				),*/
				'work_end' => array(
					'name' 		=>  __('End (schedule)'),
					'type' 		=> 'date',
					'edit'		=> 1,
					'fulltime'	=>  '1',
					'width'		=> '10',
				),
				/*'time_end' => array(
					'name' 		=>  __('Time end'),
					'type' 		=> 'select',
					'droplist' => 'equipments_status',
					'width'		=> '5',
					'edit'	=> '1',
				),*/
				'status' => array(
					'name' 		=>  __('Status'),
					'width'		=> '10',
					'type' 		=> 'select',
					'droplist' => 'equipments_status',
					'edit'	=> '1',
				),
				'note' => array(
					'name' 		=>  __('Note'),
					'width'		=> '28',
					'edit'	=> '1',
				),
			),
);






//====== ORDERS =======//
$ModuleField['relationship']['order']['name'] = __('TimeLog');
//Sales orders
$ModuleField['relationship']['order']['block']['order'] = array(
    'title' => 'Sales orders for this item',
    'css' => 'width:100%;',
    'height' => '220',
    'custom_box_bottom' => '1',
    'footlink' => array(
        'label' => __('Click to view full details')
    ),
    'link' => array('w' => '1', 'cls' => 'salesorders','field'=>'_id'),
    'reltb' => 'tb_salesorder@products', //tb@option
   /* 'total' => 'Total (not inc. cancelled)',
    'total_css' => 'margin-right:3%;',*/
    'type' => 'listview_box',
    'add'	=> __('Add option'),
    'field' => array(
    		'_id' => array(
		            'name' => __('ID'),
					'type'=>'id',
				),
	        'code' => array(
	            'name' => __('Ref no'),
	            'width' => '6',
	        ),
	        'salesorder_date' => array(
	            'name' => __('Date in'),
	            'width' => '10',
	            'type' => 'date',
	            'align' => 'center',
	        ),
	        'payment_due_date' => array(
	            'name' => __('Due date'),
	            'width' => '10',
	            'type' => 'date',
	            'align' => 'center',
	        ),
	        'status' => array(
	            'name' => __('Status'),
	            'width' => '8',
	            'type' => 'idlink',
	            'relid' => 'contact_id',
	            'cls' => 'contacts',
	        ),
	        'our_rep' => array(
	            'name' => __('Assign to'),
	            'width' => '8',
	            'type' => 'idlink',
	            'relid' => 'contact_id',
	            'cls' => 'contacts',
	        ),
	        'our_rep_id' => array(
	            'name' => __('Assign to ID'),
	            'type' => 'id',
	        ),
	        'heading' => array(
	            'name' => __('Heading'),
	            'width' => '10',
	        ),
	        'comments' => array(
	            'name' => __('Comments'),
	            'width' => '30',
	        ),
    ),
);



//====== Expensive =======//
$ModuleField['relationship']['expensive']['name'] = __('Expensive');
//Sales orders
$ModuleField['relationship']['expensive']['block']['expense'] = array(
	'title'	=>__('Expenses for this tasks'),
	'type'	=>'listview_box',
	'css'	=>'width:47%;margin-top:0;',
	'height' => '320',
	'add'	=> __('Add new line'),
	'reltb'		=> 'tb_basic@expense',//tb@option
	'delete' => '3',
	'field'=> array(
				'heading' => array(
					'name' 		=>  __('Heading'),
					'width' => '40',
					'type'	=> 'text',
					'edit'	=> '1',
				),
				'details' => array(
					'name' 		=>  __('Details'),
					'width' => '52',
					'type'	=> 'text',
					'edit'	=> '1',
				),
			),
);

$ModuleField['relationship']['expensive']['block']['group'] = array(
		'title'	=>__('Group'),
		'type'	=>'listview_box',
		'css'	=>'width:26%;float: left; padding-left:6px;',
		'height' => '320',
		'reltb'		=> 'tb_task@alerts',//tb@option
		'field'=> array(
			'group'	=> array(
						'name' =>'Group',
						'type'	=> 'text',
						'align' => 'center',
					),
		),
);

/*$ModuleField['relationship']['expensive']['block']['profile'] = array(
	'title' => 'Profile',
    'css' => 'width:26%;float:right',
    'height' => '290',
    'reltb' => 'tb_salesinvoice@profile', //tb@option
    'type' => 'editview_box',
    'field' => array(
    	   'type' => array(
		            'name' => __('Type'),
		            'width' => '2',
		            'type' => 'text',
		            'default'	=> '0',
		            'edit' => '1'
	        ),
    	    'category' => array(
		            'name' => __('Category'),
		            'width' => '2',
		            'type' => 'text',
		            'edit' => '1',
		            'default'	=> '0',
	        ),
	        'rating' => array(
					'name' 		=>  __('Rating'),
					'type' => 'text',
					'width'		=> '10',
					'default'	=> '0',
					'edit'		=> '1',
			),
			'no_staff' => array(
					'name' 		=>  __('No of staff'),
					'type' => 'text',
					'width'		=> '10',
					'default'	=> '0',
					'edit'		=> '1',
			),
			'none1' => array(
				'name' => __('Phone Related'),
				'type' => 'header',
			),
			'speed_dial' => array(
		            'name' => __('Speed Dial'),
		            'width' => '2',
		            'type' => 'text',
		            'default'	=> '0',
		            'edit' => '1'
	        ),
    	    'phonebook' => array(
		            'name' => __('Include in phonebook'),
		            'width' => '2',
		            'type' => 'text',
		            'edit' => '1',
		            'default'	=> '0',
	        ),
	        'none2' => array(
				'name' => __('Custom field'),
				'type' => 'header',
			),
			'custom_field_1' => array(
		            'name' => __('Custom 1'),
		            'width' => '2',
		            'type' => 'text',
		            'default'	=> '0',
		            'edit' => '1'
	        ),
    	    'custom_field_2' => array(
		            'name' => __('Custom 2'),
		            'width' => '2',
		            'type' => 'text',
		            'edit' => '1',
		            'default'	=> '0',
	        ),
	        'custom_field_3' => array(
		            'name' => __('Custom 3'),
		            'width' => '2',
		            'type' => 'text',
		            'edit' => '1',
		            'default'	=> '0',
	        ),
    ),
);*/
$ModuleField['relationship']['expensive']['block']['profile'] = array(
    'title' => 'Profile',
    'css' => 'width:26%;float:right;',
    'height' => '290',
    'reltb' => 'tb_task@profile',
    'type' => 'editview_box',
    'field' => array(
		'profile_type' => array(
			'name' => __('Type'),
			'type' => 'select',
			'droplist' => 'company_type',
			'default' => '',
			'not_custom'=>'1',
			'edit'		=>'1',
		),
		'category' => array(
			'name' => __('Category'),
			'type' => 'select',
			'css'=>'text-align:left;',
			'droplist' => 'company_category',
			'default' => '',
			'edit'		=>'1',
		),
		'rating' => array(
			'name' => __('Rating'),
			'type' => 'select',
			'css'=>'text-align:left;',
			'droplist' => 'company_rating',
			'default' => '',
		),
		'no_of_staff' => array(
			'name' => __('No of staff'),
			'type' => 'text',
			'css'=>'text-align:left;',
			'edit'		=>'1',
		),
		'none' => array(
			'name' => __('Phone Related'),
			'type' => 'header',
		),
		'speed_dial' => array(
			'name' => __('Speed Dial'),
			'type' => 'text',
			'label'=> ' using serial numbers &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
		),
		'generate_serials' => array(
			'name'  => __('For'),
			'type' 		=> 'select',
			'droplist'	=> 'expensive_phone_for',
			'default'	=> '',
			'not_custom'=>'1',
			'edit'		=>'1',
		),
		'include_in_phone_book' => array(
			'name'  => __('Include in phonebook'),
			'type' => 'checkbox',
			'css'		=> 'padding-left:2%; border-bottom:none',
			'default'   => '0',
		),
		'phone_sort_by' => array(
			'name'  => __('Sort by'),
			'type' => 'text',
			'element_input'=>'maxlength="20"',
		),
		'none2' => array(
			'name' => __('Custom field'),
			'type' => 'header',
		),
	 	'custom_field_1' => array(
		            'name' => __('Custom 1'),
		            'width' => '2',
		            'type' => 'text',
		            'edit' => '1'
	        ),
	    'custom_field_2' => array(
	            'name' => __('Custom 2'),
	            'width' => '2',
	            'type' => 'text',
	            'edit' => '1',
        ),
        'custom_field_3' => array(
	            'name' => __('Custom 3'),
	            'width' => '2',
	            'type' => 'text',
	            'edit' => '1',
        ),
    ),
);




//====== DOCUMENTS =======//
// Dung chung ham document cua anh nam
$ModuleField['relationship']['documents']['name'] =  __('Documents');
// end document


//====== Premission =======//
$ModuleField['relationship']['other']['name'] =  __('Other');
//Premission list data

$ModuleField['relationship']['other']['block']['other1'] = array(
    'css' => 'width:30%',
    'height' => '80',
    'reltb' => 'tb_salesinvoice@products', //tb@option
    'type' => 'editview_box',
    'field' => array(
	    	'unknown1' => array(
						'name' 		=>  __(''),
						'width'		=> '30',
						'type' => 'text',
						'edit'		=> '1',
			),
	    	'unknown2' => array(
						'name' 		=>  __(''),
						'width'		=> '30',
						'type' => 'text',
						'edit'		=> '1',
			),
			'unknown3' => array(
						'name' 		=>  __(''),
						'width'		=> '30',
						'type' => 'text',
						'edit'		=> '1',
			),

	),
);
$ModuleField['relationship']['other']['block']['other'] = array(
	'title'	=>__(''),
	'type'	=>'text_box',
	'css'	=>'width:69%;margin-top:0;float:right',
	'textarea_css'=> 'height:200px;font-size:16px;line-height:20px',
	'height' => '220',
	'reltb'		=> 'tb_basic@subtab02',//tb@option
	'field'=> array(
				'other' => array(
					'name' 		=>  __(''),
					'edit'		=> '1',
					'default'	=> 'Click for edit',
				),
	),
);
$ModuleField['relationship']['other']['block']['other2'] = array(
    'css' => 'width:30%;margin-top:5px',
    'height' => '80',
    'reltb' => 'tb_salesinvoice@products', //tb@option
    'type' => 'editview_box',
    'field' => array(
	    	'unknown4' => array(
						'name' 		=>  __(''),
						'width'		=> '30',
						'type' => 'text',
						'edit'		=> '1',
			),
	    	'unknown5' => array(
						'name' 		=>  __(''),
						'width'		=> '30',
						'type' => 'text',
						'edit'		=> '1',
			),
			'unknown6' => array(
						'name' 		=>  __(''),
						'width'		=> '30',
						'type' => 'text',
						'edit'		=> '1',
			),

	),
);



$TaskField = $ModuleField;




//============ *** RELATIONSHIP *** =============//

