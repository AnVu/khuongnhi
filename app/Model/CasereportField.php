<?php
$ModuleField = array();
$ModuleField = array(
	'module_name' 	=> __('Casereport'),
	'module_label' 	=> __('Case'),
	'colection' 	=> 'tb_casereport',
	'title_field'	=> array('','name','',''),
);


//============= *** FIELDS *** =============//

// Panel 1
$ModuleField['field']['panel_1'] = array(
	'setup'	=> array(
			'css'	=> 'width:100%;',
			'lablewith' => '20',
			'blockcss' => 'width:32%;float:left;',
			),
	'code' => array(
		'name' 		=> __('Ref no'),
		'type' 		=> 'text',
		'moreclass' => 'fixbor',
		'lock' => '1',
		'listview'	=>	array(
						'order'	=>	'1',
						'with'	=>	'8',
						'css'	=>	'width:8%;',
					),
	),
	'name'	=>array(
			'name' 		=> __('Name'),
			'type' 		=> 'text',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'8',
							'css'	=>	'width:8%;',
						),
			),
	'mongo_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
			),
	'description'=>array(
			'name' 		=>  __('Description:'),
			'type' 		=> 'text',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'8',
							'css'	=>	'width:8%;',
						),
	
			),
	'none'	=>array(
		'type' 		=> 'not_in_data',
		'moreclass' => 'fixbor2',
		),
);



// Panel 2 ////////////////////////////////////////////////////////////
$ModuleField['field']['panel_2'] = array(
	'setup'	=> array(
			'css'	=> 'width:50%;',
			'lablewith' => '20',
			'blockcss' => 'width:67%;float:right',
			),
	'priority' => array(
			'name' 		=> __('Priority'),
			'type' 		=> 'select',
			'droplist' => 'case_priority',
        	'default' => 'Important',
        	'field_class' => 'fieldclass',
			'moreclass' => 'fixbor',			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'8',
							'css'	=>	'width:8%;',
						),
			),
    'status' => array(
            'name' 		=> __('Status'),
          	'type' 		=> 'select',
            'droplist' => 'case_status',
        	'field_class' => 'fieldclass',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'8',
							'css'	=>	'width:8%;',
						),
    ),
    'type' => array(
            'name' 		=> __('Type'),
           	'type' 		=> 'select',
            'droplist' => 'case_type',
        	'default' => 'Type1',
        	'field_class' => 'fieldclass',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'8',
							'css'	=>	'width:8%;',
						),
    ),
	'none'	=>array(
		'type' 		=> 'not_in_data',
		'moreclass' => 'fixbor2',
		),
);


// Panel 3 //////////////////////////////////////////////////////////////
$ModuleField['field']['panel_3'] = array(
	'setup'	=> array(
			'css'	=> 'width:50%;',
			'lablewith' => '20',
			'blockcss' => 'width:32%;float:left;margin-left:1.5%;',
			),
    'assignto'	=>array(
			'name' 		=>  __('Assign to'),
			'type' 		=> 'relationship',
			'cls'		=> 'contacts',
			'id'		=> 'assignto_id',
			'para'		=> ',get_para_employee()',
			'not_custom'=> '1',
			'syncname'	=> 'first_name',
			'listview'	=>	array(
							'order'	=>	'1',
							'css'	=>	'width:10%;',
							'sort'=> '1',
						),
			),
	'assignto_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
			),
	'company'	=>array(
			'name' 		=>  __('Company'),
			'type' 		=> 'relationship',
			'cls'		=> 'companies',
			'id'		=> 'company_id',
			'not_custom'=> '1',
			'css'		=> 'padding-left:2%;',
			'lock'		=> '0',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'15',
							'css'	=>	'width:15%;',
							'sort'=> '1',
			),
	),
	'company_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
			),
    'moduletype' => array(
            'name' 		=> __('Module type:'),
           	'type' 		=> 'select',
            'droplist' => 'case_moduletype',
        	'default' => 'ModuleType1',
        	'field_class' => 'fieldclass',
			'listview'	=>	array(
							'order'	=>	'1',
							'with'	=>	'8',
							'css'	=>	'width:8%;',
						),
    ),
	'none'	=>array(
		'type' 		=> 'not_in_data',
		),
);


//============ *** RELATIONSHIP *** =============//

//====== Option =======//
$ModuleField['relationship']['general']['name'] =  __('General');

//Option list data
$ModuleField['relationship']['general']['block']['detail'] = array(
	'title'	=>__('Detail'),
	'type'	=>'text_box',
	'css'	=>'width:100%;margin-top:0;',
	'height' => '150',
	'textarea_css' => 'height:150px;font-size:16px;line-height:20px',
	'field'=> array(
				'detail' => array(
					'name' 		=>  __('Name 1'),
					'edit'		=> '1',
					
				)
			),	
);



//====== Premission =======//
$ModuleField['relationship']['note_activity']['name'] =  __('Note & activities');
//Premission list data
$ModuleField['relationship']['note_activity']['block']['note_activity'] = array(
	'title'	=>__('Note & activities'),
	'type'	=>'listview_box',
	'css'	=>'width:100%;margin-top:0;',
	'height' => '150',
	'add'	=> __('Add new line'),
	'reltb'		=> 'tb_basic@note_activity',//tb@option
	'delete' => '6',
	'field'=> array(
				'note_type' => array(
					/*'name' 		=>  __('Type'),
					'type'	=> 'select',
					'droplist'	=> 'com_type',
					'width' => '10',*/
					'name' => __('Type xyz'),
					'width' => '10',
		            'type'=>'select',
					'droplist' => 'note_type',
					//'edit' => '1',
					'not_custom'=>'1',
				),
				'note_dates' => array(
					'name' 		=>  __('Date'),
					'type'	=> 'text',
					'width' => '10',
				),
				'note_by' => array(
					'name' => __('By'),
					'type' 		=> 'relationship',
					'cls'		=> 'contacts', // chi dinh loai popup
					'id'		=> 'note_by_id',
					'para'		=> ',get_para_employee()',
					//'not_custom'=> '1',
					'edit'	=> '1',
					'width' => '15',
					'syncname'	=> 'first_name',
					'listview'	=>	array(
									'order'	=>	'1',
									'css'	=>	'width:30%;',
									'sort'=> '1',
								),
				),
				
				'note_by_id' => array(
					'name' => __('By ID'),
					'type' => 'id',
				),
				'note_details' => array(
					'name' 		=>  __('Details'),
					'width' => '53',
					'type'	=> 'text',
					'edit'	=> '1',
				),
			),
);


$CasereportField = $ModuleField;