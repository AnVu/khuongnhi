<?php

$ModuleField = array();
$ModuleField = array(
	'module_name' => __('Salesorder'),
	'module_label' => __('Sales Order'),
	'colection' => 'tb_salesorder',
	'title_field' => array('company_name', 'contact_name', 'status', 'user_responsible'),
);


//============= *** FIELDS *** =============//
// Panel 1
$ModuleField['field']['panel_1'] = array(
	'setup' => array(
		'css' => 'width:100%;',
		'lablewith' => '25',
		'blockcss' => 'width:30%;float:left;',
	),
	'code' => array(
		'name' => __('Mã đặt hàng'),
		'type' => 'text',
		'moreclass' => 'fixbor',
		'width' => '30%;text-align:right;',
		'css' => 'width:50%; padding-left:6.5%;',
		'after_field' => 'sales_order_type',
		'moreinline' => 'Type',
		'element_input' => 'onkeypress="return isCode(event);"',
		'listview' => array(
			'order' => '1',
			'with' => '100',
			'align' => 'center',
			'css' => 'width:5%;',
			'sort' => '1',
		),
	),
	'sales_order_type' => array(
		'name' => __('Type'),
		'type' => 'select',
		'other_type' => 'after_other',
		'droplist' => 'sales_order_type',
		'default' => 'Sales Order',
		'classselect' => 'jt_after_field',
		'width' => '41%;" id="field_after_quotetype" alt="',
		'element_input' => 'combobox_blank="1"',
		'css' => ' width:110%;',
		'not_custom' => '1',
	),
	'mongo_id' => array(
		'type' => 'id',
		'element_input' => ' class="jthidden"',
	),
	'date_modified' => array(
		'type' => 'hidden',
	),
	'created_by' => array(
		'type' => 'hidden',
	),
	'modified_by' => array(
		'type' => 'hidden',
	),
	'description' => array(
		'name' => __('Description'),
		'type' => 'hidden',
	),
	'company_name' => array(
		'name' => __('Tên công ty'),
		'type' => 'relationship',
		'cls' => 'companies',
		'list_syncname' => 'company_name',
		'id' => 'company_id',
		'css' => 'padding-left:2%;',
		'lock' => '0',
		'listview' => array(
			'order' => '2',
			'with' => '15',
			'css' => 'width:15%;',
			'sort' => '1',
		),
	),
	'company_id' => array(
		'type' => 'id',
		'element_input' => ' class="jthidden"',
	),
	'contact_name' => array(
		'name' => __('Contact'),
		'type' => 'relationship',
		'cls' => 'contacts',
		'list_syncname' => 'contact_name',
		'syncname' => 'first_name',
		'id' => 'contact_id',
		'css' => 'padding-left:2%;',
		'para' => ',get_para_contact()',
	),
	'contact_id' => array(
		'type' => 'id',
		'element_input' => ' class="jthidden"',
	),
	'phone' => array(
		'name' => __('Phone'),
		'type' => 'phone',
		'css' => 'padding-left:2%;',
	),
	'email' => array(
		'name' => __('Email'),
		'type' => 'email',
		'css' => 'padding-left:2%;',
	),
	'salesorder_date' => array(
		'name' => __('Ngày đặt hàng'),
		'type' => 'date',
		'css' => 'padding-left:2%;',
		'listview' => array(
			'order' => '3',
			'with' => '6',
			'css' => 'width:6%;',
			'sort' => '1',
		),
	),
/*	'payment_due_date' => array(
		'name' => __('Due date'),
		'type' => 'date',
		'css' => 'padding-left:2.5%;',
	),*/
	'our_rep' => array(
		'name' => __('Our rep'),
		'type' => 'relationship',
		'cls' => 'contacts',
		'id' => 'our_rep_id',
		'syncname' => 'first_name',
		'para' => ',get_para_employee()',
		'not_custom' => '1',
	),
	'our_rep_id' => array(
		'type' => 'id',
		'element_input' => ' class="jthidden"',
	),
	'our_csr' => array(
		'name' => __('Our CSR'),
		'type' => 'relationship',
		'cls' => 'contacts',
		'id' => 'our_csr_id',
		'syncname' => 'first_name',
		'para' => ',get_para_employee()',
		'not_custom' => '1',
	),
	'our_csr_id' => array(
		'type' => 'id',
		'element_input' => ' class="jthidden"',
	),
	'none9' => array(
		'type' => 'not_in_data',
	),
	'none' => array(
		'type' => 'not_in_data',
		'moreclass' => 'fixbor2',
	),
);

$ModuleField['field']['panel_2'] = array(
	'setup' => array(
		'css' => 'width:70%;',
		'lablewith' => '35', //%
		'blockcss' => 'width:69%;float:right;',
		'blocktype' => 'address',
	),
	'invoice_address' => array(
		'name' => __('Invoice address'),
		'type' => 'text',
	),
	'shipping_address' => array(
		'name' => __('Shipping address'),
		'type' => 'text',
	),
);


// Panel 4
$ModuleField['field']['panel_4'] = array(
	'setup' => array(
		'css' => 'width:33%;',
		'lablewith' => '35',
	),

	'status' => array(
		'name' => __('Tình trạng'),
		'type' => 'select',
		'droplist' => 'salesorders_status',
		'default' => 'Mới',
		'not_custom' => '1',
		'element_input' => 'combobox_blank="1"',
		'listview' => array(
			'order' => '5',
			'with' => '5',
			'css' => 'width:5%;',
			'sort' => '1',
		),
	),
	'payment_terms' => array(
		'name' => __('Payment terms'),
		// 'type' => 'select',
		'type' => 'hidden',
		'droplist' => 'salesinvoices_payment_terms',
		'element_input' => 'combobox_blank="1"',
		'width' => '41%',
		'css' => 'padding-left:4.5%;',
		'after' => '<div class="jt_after float_left" id="mx_payment_terms">&nbsp;days</div>',
		'default' => 0,
	),
	'tax' => array(
		'name' => __('Tax %'),
		'type' => 'hidden',
		// 'type' => 'select',
		//'droplist' => 'product_pst_tax',
		'default' => '0% VAT',
		'element_input' => 'combobox_blank="1"',
	),
	'taxval' => array(
		'name' => __('Tax'),
		'type' => 'hidden',
		'default' => '5',
	),
	'customer_po_no' => array(
		'name' => __('Customer PO no'),
		'type' => 'hidden',
		// 'type' => 'text',
	),
	'heading' => array(
		'name' => __('Heading'),
		'type' => 'hidden',
		// 'type' => 'text',
	),
	'name' => array(
		'type' => 'hidden',
	),
	'job_name' => array(
		'name' => __('Job'),
		'type' => 'hidden',
		// 'type' => 'relationship',
		'cls' => 'jobs',
		'id' => 'job_id',
		'before_field' => 'job_number',
		'list_syncname' => 'job_number',
		'width' => '44.5%',
		'css' => 'float:left;',
		'not_custom' => '1',
	),
	'job_number' => array(
		'name' => __(''),
		'type' => 'hidden',
		// 'type' => 'text',
		'other_type' => '1',
		'width' => '15%',
		'lock' => '1',
		'css' => 'width:91%;float:left;padding-left:5%;padding-right: 2%;',
	),
	'job_id' => array(
		'type' => 'hidden',
		// 'type' => 'id',
		'element_input' => ' class="jthidden"',
	),
	'quotation_name' => array(
		'name' => __('Quotation'),
		'type' => 'hidden',
		// 'type' => 'relationship',
		'cls' => 'quotations',
		'id' => 'quotation_id',
		'syncname' => 'name',
		'before_field' => 'quotation_number',
		'width' => '44.5%',
		'css' => 'float:left;',
		'not_custom' => '1',
	),
	'quotation_number' => array(
		'type' => 'hidden',
		// 'type' => 'text',
		'other_type' => '1',
		'width' => '15%',
		'lock' => '1',
		'css' => 'width:91%;float:left;padding-left:5%;padding-right: 2%;',
	),
	'quotation_id' => array(
		'type' => 'hidden',
		// 'type' => 'id',
		'element_input' => ' class="jthidden"',
	),
	'delivery_method' => array(
		'name' => __('Delivery method'),
		'droplist' => 'salesorder_delivery_method',
		'element_input' => 'combobox_blank="1"',
		'type' => 'hidden',
		// 'type' => 'select',
	),
	'shipper' => array(
		'name' => __('Shipper'),
		'type' => 'hidden',
		// 'type' => 'relationship',
		'cls' => 'companies',
		'id' => 'shipper_id',
		'para' => ',get_company_is_shipper()',
	),
	'shipper_id' => array(
		'name' => __(''),
		'type' => 'hidden',
		// 'type' => 'id',
		'element_input' => ' class="jthidden"',
	),
	'shipper_account' => array(
		'name' => __('Shipper account'),
		'type' => 'hidden',
		// 'type' => 'text',
		'moreclass' => 'fixbor3',
	),

	'products' => array(
		'type' => 'hidden',
		// 'type' => 'fieldsave',
		'rel_name' => 'products',
	),
	'none' => array(
		'type' => 'not_in_data',
	),
	'none' => array(
		'type' => 'not_in_data',
	),
	'none1' => array(
		'type' => 'not_in_data',
	),
	'none2' => array(
		'type' => 'not_in_data',
	),
	'none3' => array(
		'type' => 'not_in_data',
	),
	'none4' => array(
		'type' => 'not_in_data',
	),
	'none5' => array(
		'type' => 'not_in_data',
	),
	'none6' => array(
		'type' => 'not_in_data',
	),
	'none7' => array(
		'type' => 'not_in_data',
	),
	'none8' => array(
		'type' => 'not_in_data',
	),
);

$ModuleField['field']['panel_5'] = array(
	'setup' => array(
		'css' => 'width:33%;',
		'lablewith' => '35',
	),
	'quotation_name' => array(
		'name' => __('Total Quotation'),
		'type' => 'relationship@price',
		'cls' => 'quotations',
		'id' => 'quotation_id',
		'width' => '44.5%',
		'list_syncname'=>'sum_sub_total',
		'not_custom' => '1',
		'isInt'=>1,
		'numformat'=>2,
	),
	'sum_amount' => array(
		'type' => 'price',
		'name' => __('Tổng tiền toa hàng'),
		'element_input' => ' class="jthidden"',
		'listview' => array(
			'order' => '2',
			'css' => 'width:10%; text-align: right',
			'sort' => 1,
		),
	),
	'sum_tax' => array(
		'type' => 'fieldsave',
	),
);


//============ *** RELATIONSHIP *** =============//
//====== LINE ENTRY =======//
$ModuleField['relationship']['line_entry']['name'] =  __('Line entry');

//Line entry Details
$ModuleField['relationship']['line_entry']['block']['products'] = array(
	'title'	=>__('Details'),
	'type'	=>'listview_box',
	'css'	=>'width:100%;margin-top:0;',
	//'height' => '282',
	'height' => '600',
	'add'	=> __('Add line'),
	'custom_box_bottom' => '1',
	'custom_box_top' => '1',
	//'link'		=> array('w'=>'1', 'cls'=>'products'),
	'reltb'		=> 'tb_salesorder@products',//tb@option
	'delete' => '1',
	'field'=> array(
				'code' => array(
					'name' 		=>  __('Code'),
					'type'	=> 'hidden',
				),
				'sku' => array(
					'name' 		=>  __('SKU'),
					'type'	=> 'link_icon',
					'link_field'	=> 'products_id',
					'module_rel'	=> 'products',
					'popup_title'	=> 'Specify Products',
					'popup_key'	=> 'change',
					'width'=>7,
					'align' => 'left',
					'indata' => '0',
					'edit'=>'1',
					'para'=>'"?no_supplier=1&products_product_type=Product"',
				),
				'products_name' => array(
					'name' 		=>  __('Tên sản phẩm'),
					'width'=>35,
					'edit'	=> '0',
					'default'=> 'Click for edit',
					//comment
				),
				'products_id' => array(
					'name' 		=>  __('Products ID'),
					'type' =>'hidden',
				),
				'oum'		=>array(
					'name' 		=>  __('Đơn vị tính'),
					'type' 		=> 'select',
					'droplist' => 'product_oum_unit',
        			'default' => 'unit',
					'element_input' => 'combobox_blank="1"',
					'width' 	=>8,
					'edit'		=> '1',
					),
				'specification'		=>array(
					'name' 		=>  __('Quy cách'),
					'type' 		=> 'select',
					'width'		=> 10,
					'default'	=> '',
					'element_input' => 'combobox_blank="1"',
					'droplist'	=> 'product_specific',
					'edit'		=> '1',
					),

				/*'sell_price'	=>array(
					'name' 		=>  __('Đơn giá'),
					'type' 		=> 'price',
					'width'		=> 7,
					'numformat'=>0,
					'align' 	=> 'right',
					'edit'		=> '1',
					),*/
				'sell_price'	=>array(
					'name' 		=>  __('Đơn giá'),
					'type' 		=> 'select_price',
					'width'		=> 7,
					'droplist'	=> 'product_specific',
					'default' => '',
					'element_input' => 'combobox_blank="1"',
					'edit'		=> '1',
					),
				'quantity' => array(
					'name' 		=>  __('Quantity'),
					'type' => 'price',
					'align' => 'right',
					'width'=>'6',
					'edit'	=> '1',
					'numformat' => 0,
					'isInt' => '1',
					'default'=> '1',
				),
				/*'option' => array(
					'name' 	=>  __('Số lượng trả'),
					'type' 	=> 'link_plus',
					'width'	=> '5',
					'edit'  => 0,
					'align' => 'center',
				),*/
				'amount' => array(
					'name' 		=>  __('Thành tiền'),
					'width'=>7,
					'align' => 'right',
					'default'=> '0',
					'type' => 'price',
					'numformat' => 0,
				),
				'amount_interest' => array(
					'name' 		=>  __('Lãi'),
					'width'=>7,
					'align' => 'right',
					'default'=> '0',
					'type' => 'price',
					'numformat' => 0,
				),
			),
);


//====== TEXT ENTRY =======//
$ModuleField['relationship']['text_entry']['name'] =  __('Chi tiết');
$ModuleField['relationship']['text_entry']['hidden'] =  true;

//Text entry Details
$ModuleField['relationship']['text_entry']['block']['products'] = array(
	'title'	=>__('Details'),
	'type'	=>'listview_box',
	'css'	=>'width:100%;margin-top:0;',
	'height' => '264',
	'add'	=> __('Add line'),
	'custom_box_bottom' => '1',
	'custom_box_top' => '1',
	'reltb'		=> 'tb_salesorder@products',//tb@option
	'delete' => '1',
	'full_height' => '1',
	'field'=> array(
				'code' => array(
					'name' 		=>  __('Code'),
					'type'	=> 'hidden',
				),
				'sku' => array(
					'name' 		=>  __('SKU'),
					'type'	=> 'link_icon',
					'link_field'	=> 'products_id',
					'module_rel'	=> 'products',
					'popup_title'	=> 'Specify Products',
					'popup_key'	=> 'change',
					'width'=>7,
					'align' => 'left',
					'indata' => '0',
					'edit'=>'1',
					'para'=>'"?no_supplier=1&products_product_type=Product"',
				),
				'products_name' => array(
					'name' 		=>  __('Tên sản phẩm'),
					'width'=>35,
					'edit'	=> '1',
					'default'=> 'Click for edit',
					//comment
				),
				'products_id' => array(
					'name' 		=>  __('Products ID'),
					'type' =>'hidden',
				),
				'oum'		=>array(
					'name' 		=>  __('Đơn vị tính'),
					'type' 		=> 'select',
					'droplist' => 'product_oum_unit',
        			'default' => 'unit',
					'element_input' => 'combobox_blank="1"',
					'width' 	=>8,
					'edit'		=> '1',
					),
				'specification'		=>array(
					'name' 		=>  __('Quy cách'),
					'type' 		=> 'select',
					'width'		=> 10,
					'default'	=> '',
					'element_input' => 'combobox_blank="1"',
					'droplist'	=> 'product_specific',
					'edit'		=> '1',
					),

				'sell_price'	=>array(
					'name' 		=>  __('Đơn giá'),
					'type' 		=> 'price',
					'width'		=> 7,
					'numformat'=>0,
					'align' 	=> 'right',
					'edit'		=> '1',
					),
				'return_quantity' => array(
					'name' 		=>  __('Số lượng trả'),
					'type' => 'price',
					'align' => 'right',
					'width'=>'6',
					'edit'	=> '1',
					'numformat' => 0,
					'isInt' => '1',
					'default'=> '1',
				),
				'amount' => array(
					'name' 		=>  __('Thành tiền'),
					'width'=>7,
					'align' => 'right',
					'default'=> '0',
					'type' => 'price',
					'numformat' => 0,
				),
			),
);


//====== TASK =======//
$ModuleField['relationship']['tasks']['name'] = __('Task');
$ModuleField['relationship']['tasks']['hidden'] =  true;
//dùng hàm cũ của Nam
//====== SHIP/INVOICE =======//
$ModuleField['relationship']['ship_invoice']['name'] = __('Ship/Invoice');
$ModuleField['relationship']['ship_invoice']['hidden'] =  true;

$ModuleField['relationship']['ship_invoice']['block']['invoice'] = array(
	'title' => __('Invoices for this sales order'),
	'type' => 'listview_box',
	'css' => 'width:52%;margin-top:0;',
	'height' => '192',
	'custom_box_top' => '1',
	'custom_box_bottom' => '1',
//	'add' => __('Add line'),
	'footlink' => array('label'=>'Click to view'),
	'link' => array('w' => '2', 'cls' => 'salesinvoices','field'=>'_id'),
	'reltb' => 'tb_salesinvoices@products', //tb@option
	'field' => array(
		'code' => array(
			'name' => __('Ref no'),
			'width' => '6',
			'align'=>'center',
			'default' => 'Click for edit',
		),
		'invoice_type' => array(
			'name' => __('Type'),
			'width' => '10',
			'align'=>'center',
			'default' => 'Click for edit',
		),
		'invoice_date' => array(
			'name' => __('Date'),
			'type'=>'date',
			'align'=>'center',
			'width' => '10',
			'default' => 'Click for edit',
		),
		'sum_amount' => array(
			'name' => __('Total'),
			'width' => '15',
			'type'=>'price',
			'numformat'=>'2',
			'align'=>'right',
			'default' => 'Click for edit',
		),
		'invoice_status' => array(
			'name' => __('Status'),
			'width' => '10',
			'align'=>'center',
			'default' => 'Click for edit',
		),
		'our_rep' => array(
			'name' => __('Our rep'),
			'width' => '15',
			'align'=>'center',
			'default' => 'Click for edit',
		),
		'other_comment' => array(
			'name' => __('Comments'),
			'width' => '22',
			'align'=>'left',
			'default' => 'Click for edit',
		),


	),
);
$ModuleField['relationship']['ship_invoice']['block']['shipping'] = array(
	'title' => __('Shipping for this sales order'),
	'type' => 'listview_box',
//	'add' => __('Add line'),
	'footlink' => array('label'=>'Click to view'),
	'custom_box_bottom' => '1',
	'custom_box_top' => '1',
	'link' => array('w' => '2', 'cls' => 'shippings','field' => '_id',),
	'css' => 'width:47%;margin-left:1%;',
	'height' => '192',
	'reltb' => 'tb_shippings@products',
	'field' => array(
		'code' => array(
			'name' => __('Ref no'),
			'width' => '6',
			'align'=>'center',
			'default' => 'Click for edit',
			'type' => 'text',
		),
		'id' => array(
			'type'=>'id',
		),
		'shipping_type' => array(
			'name' => __('Type'),
			'width' => '6',
			'align'=>'center',
			'default' => 'Click for edit',
		),
		'return_status' => array(
			'name' => __('Return'),
			'type'=>'checkbox',
			'width' => '10',
			'align'=>'center',
			'default' => 'Click for edit',
		),
		'shipping_date' => array(
			'name' => __('Date'),
			'width' => '12',
			'align'=>'center',
			'type'=>'date',
			'default' => 'Click for edit',
		),
		'shipping_status' => array(
			'name' => __('Status'),
			'width' => '12',
			'align'=>'center',
			'default' => 'Click for edit',
		),
		'our_rep' => array(
			'name' => __('Our Rep'),
			'width' => '15',
			'align'=>'center',
			'default' => 'Click for edit',
		),
		'shipper' => array(
			'name' => __('Shipper'),
			'width' => '25',
			'align'=>'left',
			'default' => 'Click for edit',
		),
	),
);

//====== DOCUMENTS =======//
$ModuleField['relationship']['documents']['name'] = __('Documents');
$ModuleField['relationship']['documents']['hidden'] =  true;

//dùng hàm cũ của Nam
//Line entry Details
$ModuleField['relationship']['documents']['block']['docs'] = array(
	'title' => __('Document / file management'),
	'type' => 'listview_box',
	'css' => 'width:100%;margin-top:0;',
	'height' => '150',
	'add' => __('Add document'),
	'link' => array('w' => '2', 'cls' => 'docs'),
	'reltb' => 'tb_document@docs', //tb@option
	'delete' => '1',
	'field' => array(
		'docs_id' => array(
			'name' => __('Document ID'),
			'type' => 'id',
		),
		'file_name' => array(
			'name' => __('Document / file name'),
			'width' => '26',
			'indata' => '0',
		),
		'location' => array(
			'name' => __('Location'),
			'width' => '10',
			'indata' => '0',
		),
		'error' => array(
			'name' => __('Error'),
			'width' => '9',
			'indata' => '0',
		),
		'category' => array(
			'name' => __('Category'),
			'width' => '8',
			'indata' => '0',
		),
		'ext' => array(
			'name' => __('Ext'),
			'type' => 'checkbox',
			'width' => '3',
			'indata' => '0',
		),
		'types' => array(
			'name' => __('Type'),
			'width' => '5',
			'indata' => '0',
		),
		'version' => array(
			'name' => __('Version'),
			'indata' => '0',
			'width' => '5',
		),
		'description' => array(
			'name' => __('Description'),
			'indata' => '0',
			'width' => '20',
		),
	),
);

//====== OTHER =======//
$ModuleField['relationship']['other']['name'] = __('Other');
$ModuleField['relationship']['other']['hidden'] =  true;
//Line entry Details
$ModuleField['relationship']['other']['block']['other'] = array(
	'title' => __('Other'),
	'type' => 'listview_box',
	'css' => 'width:100%;margin-top:0;',
	'height' => '150',
	'add' => __('Add document'),
	'link' => array('w' => '2', 'cls' => 'docs'),
	'reltb' => 'tb_document@docs', //tb@option
	'delete' => '1',
	'field' => array(
		'docs_id' => array(
			'name' => __('Document ID'),
			'type' => 'id',
		),
		'file_name' => array(
			'name' => __('Document / file name'),
			'width' => '26',
			'indata' => '0',
		),
		'location' => array(
			'name' => __('Location'),
			'width' => '10',
			'indata' => '0',
		),
		'error' => array(
			'name' => __('Error'),
			'width' => '9',
			'indata' => '0',
		),
		'category' => array(
			'name' => __('Category'),
			'width' => '8',
			'indata' => '0',
		),
		'ext' => array(
			'name' => __('Ext'),
			'type' => 'checkbox',
			'width' => '3',
			'indata' => '0',
		),
		'types' => array(
			'name' => __('Type'),
			'width' => '5',
			'indata' => '0',
		),
		'version' => array(
			'name' => __('Version'),
			'indata' => '0',
			'width' => '5',
		),
		'description' => array(
			'name' => __('Description'),
			'indata' => '0',
			'width' => '20',
		),
	),
);

//Production
$ModuleField['relationship']['asset_tags']['name'] =  __('Asset Tags');
$ModuleField['relationship']['asset_tags']['hidden'] =  true;
$ModuleField['relationship']['asset_tags']['block']['asset_tags'] = array(
	'title'	=>__('Asset Tags of products'),
	'type'	=>'listview_box',
	'css'	=>'width:100%;margin-top:0;',
	'link'	=> array('w'=>'1', 'cls'=>'products','field'=>'product_id'),
	'height' => '280',
	'custom_box_bottom' => '1',
	'custom_box_top' => '1',
	'reltb'		=> 'tb_salesorder@production',
	'field'=> array(
	            'product_id' => array(
	                'name' 	=>  __('Product ID'),
					'type'  =>'hidden',
	            ),
	            'key' => array(
	                'name' 	=>  __('Product Key'),
					'type'  =>'hidden',
	            ),
	            'asset_key' => array(
	                'name' 	=>  __('Product Key'),
					'type'  =>'hidden',
	            ),
				'code' => array(
					'name' 	=>  __('Code'),
					'type'  =>'text',
					'width'=>'4',
					'align'=>'center',
				),
				'products_name' => array(
					'name' 		=>  __('Name'),
					'type'  =>'text',
					'width'=>'19',
				),
				'product_type' => array(
					'name' 		=>  __('Type'),
					'width'=>'8',
					'type' => 'select',
        			'droplist' => 'product_type',
				),
				'tag' => array(
					'name' 		=>  __('Asset Tags'),
					'width'=>'12',
					'type' => 'text',
				),
				'factor' => array(
					'name' 		=>  __('Factor'),
					'width'=>'5',
					'type' => 'price',
					'align'=>'right',
					'edit'=>'1',
				),
				'min_of_uom' => array(
					'name' 		=>  __('Min minute/UOM'),
					'width'=>'7',
					'type' => 'price',
					'align'=>'right',
					'edit'=>'1',
				),
				'sizew'		=>array(
					'name' 		=>  __('Width'),
					'width'=>'5',
					'type'  =>'text',
					'align'=>'right',
					),
				'sizeh'		=>array(
					'name' 		=>  __('Height'),
					'width'=>'5',
					'type'  =>'text',
					'align'=>'right',
					),
				'oum' => array(
					'name' 		=>  __('Sold by'),
					'width' =>'5',
					'type'  => 'select',
        			'droplist' => 'product_oum_unit',
				),
				'quantity' => array(
					'name' =>  __('Quantity'),
					'type' => 'price',
					'width'=>'5',
					'align'=>'right',
					'numformat'=>0,
				),
				'production_time' => array(
					'name' 		=>  __('Production time'),
					'width'=>'8',
					'type' => 'price',
					'align'=>'right',
				),
			),
);

// $ModuleField['relationship']['costings']['name'] = __('Costings');
// $ModuleField['relationship']['costings']['block']['costings'] = array(
// 'title' => __('This item is made up of the following costs / items'),
//     'moreclass' => 'full_width',
//     'height' => '280',
// 	'css' => 'margin-bottom:1%',
//     'foottext' => array(
//         'label' => __('Note: Stock items automatically get deducted from stock when the  main item is used on a job assembly.'),
//     ),
//     //'total' => 'Total cost',
//     'total_css' => 'margin-right:3%;',
//     'type' => 'listview_box',
//     'link' => array('w' => '1', 'cls' => 'products','field'=>'product_id'),
//     'reltb'		=> 'tb_salesorder@production',
//     'field' => array(
//         'code' => array(
//             'name' => __('Code'),
//             'type' => 'link_code',
//             'module_rel' => 'products',
//             'popup_title' => 'Change Material',
//             'popup_key' => 'change',
//             'width' => '3',
//             'align' => 'center',
//             'indata' => '0',
//         ),
// 		'sku' => array(
//             'name' => __('SKU'),
//             'width' => '6',
// 			'type' => 'text',
//         ),
//         'product_name' => array(
//             'name' => __('Name'),
//             'width' => '20',
//         ),
// 		'product_id' => array(
//             'name' => __('ID'),
// 			'type'=>'id',
// 		),
// 		'product_type' => array(
//             'name' => __('Type'),
// 			'type'=>'select',
// 			'width' => '8',
// 			'droplist' => 'product_type',
// 		),

//         'category' => array(
//             'name' => __('Category'),
//             'width' => '0',
//             'type' => 'hidden',
//         	'droplist' => 'product_category',
//         ),
// 		'company_id' => array(
//             'type' => 'id',
// 			'width' => '0',
//         ),
//         'company_name' => array(
//             'name' => __('Supplier'),
//             'type' => 'text',
//             'align' => 'left',
//             'width' => '10',
// 			'title' => 'Specify Current supplier',
//             'para' => ",'?is_supplier=1'",
// 			'indata' => '0',
//         ),
//         'unit_price' => array(
//             'name' => __('Unit cost'),
//             'width' => '5',
//             'type' => 'price',
//             'align' => 'right',
// 			'numformat'=>3,
//         ),
// 		'oum' => array(
//             'name' => __('UOM'),
//             'width' => '3',
//             'type' 		=> 'select',
// 			'droplist'	=> 'product_oum_area',
// 			'align' => 'center',
//         ),
// 		'markup' => array(
//             'name' => __('%Markup'),
//             'width' => '5',
//             'type' => 'price',
//             'align' => 'right',
//             'edit' => '1',
//             'default' => '0',
//         ),
// 		'margin' => array(
//             'name' => __('%Margin'),
//             'width' => '5',
//             'type' => 'price',
//             'align' => 'right',
//             'edit' => '1',
//             'default' => '0',
//         ),
//         'quantity' => array(
//             'name' => __('Quantity'),
//             'width' => '5',
//             'type' => 'text',
//             'align' => 'right',
//             'edit' => '1',
//             'default' => '1',
//         ),
//         'sub_total' => array(
//             'name' => __('Sub total'),
//             'width' => '7',
//             'align' => 'right',
//             'type' => 'price',
//             'indata' => '0',
//         ),
//         'view_in_detail' => array(
//            'name' => __('<span title="List this costing in Text entry">Detail List</span>'),
//            'width' => '4',
//            'align' => 'center',
//            'type' => 'checkbox',
//            'edit'  => '1',
//         ),
//     ),
// );
$ModuleField['relationship']['costings']['name'] =  __('Costings');
$ModuleField['relationship']['costings']['hidden'] =  true;

//Line entry Details
$ModuleField['relationship']['costings']['block']['costings'] = array(
	'title'	=>__('Costing details for line entry'),
	'type'	=>'listview_box',
	'css'	=>'width:100%;margin-top:0;',
	'height' => '282',
	'custom_box_bottom' => '1',
	//'link'		=> array('w'=>'1', 'cls'=>'products'),
	'reltb'		=> 'tb_quotation@products',//tb@option
	'field'=> array(
				'code' => array(
					'name' 		=>  __('Code'),
					'type'	=> 'hidden',
				),
				'sku' => array(
					'name' 		=>  __('SKU'),
					'type'	=> 'text',
					'width'=>'7',
					'align' => 'left',
					'indata' => '0',
					'edit'=>'0',
				),
				'products_name' => array(
					'name' 		=>  __('Name / details'),
					'width'=>'30',
					'edit'	=> '0',
				),
				'products_id' => array(
					'name' 		=>  __('Products ID'),
					'type' =>'hidden',
				),
				'sizew'	=>array(
						'name' 		=>  __('Size-W'),
						'type' 		=> 'price',
						'width'		=>'3',
						'edit'		=>'0',
						),
				'sizew_unit'	=>array(
						'name' 		=> __(''),
						'type' 		=> 'text',
						'edit'		=>'0',
						'width'		=>'2',
						),
				'sizeh'	=>array(
						'name' 		=>  __('Size-H'),
						'type' 		=> 'price',
						'width'		=>'3',
						'edit'		=>'0',
						),
				'sizeh_unit'	=>array(
						'name' 		=> __(''),
						'type' 		=> 'text',
						'width'		=>'3',
						'edit'		=>'0',
						),
				'area'	=>array(
						'name' 		=>  __('Area'),
						'type' 		=> 'hidden',
						),
				'sell_by'		=>array(
					'name' 		=>  __('Sold by'),
					'type' 		=> 'text',
					'width'		=>'4',
					'edit'		=> '0',
					),
				'oum'		=>array(
					'name' 		=>  __(''),
					'type' 		=> 'text',
					'width' 	=>'4',
					'edit'		=> '0',
					),
				'cost_price'		=>array(
					'name' 		=>  __('Cost price'),
					'type' 		=> 'price',
					'numformat' => 0,
					'width'=>'7',
					'default'=> '0',
					'align' => 'right',
					),
				'quantity' => array(
					'name' 		=>  __('Quantity'),
					'type' => 'text',
					'align' => 'right',
					'width'=>'5',
					'edit'	=> '0',
					'numformat' => 0,
				),
				'adj_qty' => array(
					'name' 		=>  __('Adj Qty'),
					'type' => 'price',
					'align' => 'right',
					'width'=>'5',
					'numformat' => 0,
				),
				'amount' => array(
					'name' 		=>  __('Amount'),
					'width'=>'10',
					'type' => 'price',
					'numformat' => 0,
					'align' => 'right',
					'default'=> '0',
				),
			),
);
$SalesorderField = $ModuleField;
