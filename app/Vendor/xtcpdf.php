<?php
App::import('Vendor','tcpdf/tcpdf');
class XTCPDF  extends TCPDF
{

    var $xheadercolor = array(200,200,200);
    var $xfootertext  = "Số 5 Đinh Tiên Hoàng, P3, Bình Thạnh, TP.Hồ Chí Minh - ĐT: 08.35171589 - DĐ: 091 8844179 - HOTLINE: 0903 681447   ";
    var $xfooterfont  = "freeserif" ;
    var $xfooterfontsize = 8 ;


    /**
    * Overwrites the default header
    * set the text in the view using
    *    $fpdf->xheadertext = 'YOUR ORGANIZATION';
    * set the fill color in the view using
    *    $fpdf->xheadercolor = array(0,0,100); (r, g, b)
    * set the font in the view using
    *    $fpdf->setHeaderFont(array('YourFont','',fontsize));
    */

    public function Header()
    {

    }

    /**
    * Overwrites the default footer
    * set the text in the view using
    * $fpdf->xfootertext = 'Copyright Â© %d YOUR ORGANIZATION. All rights reserved.';
    */
    public function Footer()
    {
        $this->SetTextColor(10, 10, 10);
        $this->SetFont($this->xfooterfont,'',$this->xfooterfontsize);
        $footertext = sprintf($this->xfootertext.'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages());
        $this->SetY(-15);
        if(isset($this->isSO)){
            $style= '<style>
                     td#main{
                        border-top:1px solid #000;
                    }
                </style>';
            $this->writeHTML($style.'<table><tr><td style="text-align: right;">Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().'</td></tr><tr><td id="main">'.$this->xfootertext.'</td></tr></table>');
        }
        else
            $this->Cell(0,8, $footertext,'T',1,'C');
    }
}
?>