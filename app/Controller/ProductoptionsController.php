<?php
App::uses('AppController', 'Controller');
class ProductoptionsController extends AppController {

	var $name = 'Productoptions';
	public $helpers = array();
	public $opm; //Option Module

	public function beforeFilter(){
		parent::beforeFilter();
		//$this->set: name, arr_settings, arr_options, iditem, entry_menu
		$this->set_module_before_filter('Productoption');
		$this->sub_tab_default = 'general';
	}

	public function entry(){
		$arr_set = $this->opm->arr_settings;

		// Check url to get value id
		$iditem = $this->get_id();
		if($iditem=='')
			$iditem = $this->get_last_id();
		$this->set('iditem',$iditem);
		//Load record by id
		if($iditem!=''){
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
			foreach($arr_set['field'] as $ks => $vls){
				foreach($vls as $field => $values){
					if(isset($arr_tmp[$field])){
						$arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
						if(in_array($field,$arr_set['title_field']))
							$item_title[$field] = $arr_tmp[$field];
					}
				}
			}
			$arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
			$this->Session->write($this->name.'ViewId',$iditem);

			//BEGIN special
			if(isset($arr_set['field']['panel_1']['code']['default']))
				$item_title['code'] = $arr_set['field']['panel_1']['code']['default'];
			else
				$item_title['code'] = '1';

			if(isset($arr_set['field']['panel_2']['map_formula']['default']))
				$this->set('map_formula',$arr_set['field']['panel_2']['map_formula']['default']);
			//END special
			$this->set('item_title',$item_title);

			//show footer info
			$this->show_footer_info($arr_tmp);

		//add, setup field tự tăng
		}else{
			$codeauto = $this->opm->max_field('code');
			if(isset($codeauto['code']))
				$codeauto = 1+(int)$codeauto['code'];
			else
				$codeauto = 1;
			$arr_set['field']['panel_1']['code']['default'] = $codeauto;
			$this->set('item_title',array('code'=>$codeauto));
		}

		$this->set('arr_settings',$arr_set);
		$this->sub_tab('',$iditem);

		$this->selectModel('Product');
		$this->Product->arrfield();
		$this->set('product_arrfield',$this->Product->arr_temp);
		parent::entry();
	}

	public function general(){
		$subdatas = array();
		$ids = $this->get_id();

		//value list
		$valuelist = array();
		if($ids!=''){
			$valuelist = $this->opm->select_field_arr($ids,'valuelist');
		}
		$subdatas['valuelist'] = $valuelist;

		//childlist
		$childlist = array();
		if($ids!=''){
			$where = array('parent_id' =>new MongoId($ids));

			$query = $this->opm->select_all(array(
				'arr_where' => $where,
				'arr_order' => array('_id' => -1)
			));
			$childlist = iterator_to_array($query);
		}

		$subdatas['childlist'] = $childlist;


		//otheroption
		$otheroption = array();
		if($ids!='')
			$where = array('_id' => array('$ne'=>new MongoId($ids)));
		else
			$where = array();

		$query = $this->opm->select_all(array(
			'arr_where' => $where,
			'arr_order' => array('_id' => -1)
		));
		$otheroption = iterator_to_array($query);
		$subdatas['otheroption'] = $otheroption;

		$this->set('subdatas', $subdatas);


		$this->selectModel('Setting');
		$arr_set = $this->opm->arr_settings;
		$arr_s1 = $arr_set['field']['panel_1']['types']['droplist'];
		$option_select['types'] = $this->Setting->select_option_vl(array('setting_value'=>$arr_s1));
		$arr_s2 = $arr_set['field']['panel_1']['field_types']['droplist'];
		$option_select['field_types'] = $this->Setting->select_option_vl(array('setting_value'=>$arr_s2));
		$this->set('option_select',$option_select);
	}

	public function productsetup(){
		$protype = array();
		$subdatas['protype'] = $protype;
		$proitem = array();
		$subdatas['proitem'] = $proitem;
		$this->set('subdatas', $subdatas);
	}


}