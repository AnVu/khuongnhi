<?php
// Attach lib cal_price
App::import('Vendor','cal_price/cal_price');

App::uses('AppController', 'Controller');
class ShippingsController extends AppController {

	var $name = 'Shippings';
	var $modelName = 'Shipping';
	public $helpers = array();
	public $opm; //Option Module
	public $cal_price; //Option cal_price

	public function beforeFilter(){
		parent::beforeFilter();
		$this->set_module_before_filter('Shipping');

	}

	//Các điều kiện mở/khóa field trong entry
	public function check_lock(){
		if($this->get_id()!=''){
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
			if(isset($arr_tmp['shipping_status']) && $arr_tmp['shipping_status']!='In progress' && $arr_tmp['shipping_status']!='Đang tiến hành')
				return true;

		}else
			return false;
	}


	public function rebuild_setting($arr_setting=array()){
		parent::rebuild_setting($arr_setting=array());
		$arr_setting = $this->opm->arr_settings;
		$iditem = $this->get_id();
		if($iditem!=''){
			$query = $this->opm->select_one(array('_id' => new MongoId($iditem)));
			if( (isset($query['salesorder_id']) && strlen((string)$query['salesorder_id'])==24)
			||(isset($query['salesinvoice_id']) && strlen((string)$query['salesinvoice_id'])==24)
			){
				unset($arr_setting['relationship']['line_entry']['block']['products']['field']['quantity']['edit']);
				unset($arr_setting['relationship']['line_entry']['block']['products']['field']['shipped']['edit']);
				unset($arr_setting['relationship']['line_entry']['block']['products']['field']['balance_shipped']['edit']);
				unset($arr_setting['relationship']['line_entry']['block']['products']['add']);
				unset($arr_setting['relationship']['line_entry']['block']['products']['delete']);
				unset($arr_setting['relationship']['line_entry']['block']['products']['field']['code']['edit']);


				unset($arr_setting['relationship']['text_entry']['block']['products']['field']['quantity']['edit']);
				unset($arr_setting['relationship']['text_entry']['block']['products']['field']['shipped']['edit']);
				unset($arr_setting['relationship']['text_entry']['block']['products']['field']['balance_shipped']['edit']);
				unset($arr_setting['relationship']['text_entry']['block']['products']['add']);
				unset($arr_setting['relationship']['text_entry']['block']['products']['delete']);
			}
		}

		$this->opm->arr_settings = $arr_setting;
	}

	public function entry(){
		$mod_lock = '0';
		if($this->check_lock()){
			$this->opm->set_lock(array('shipping_status'),'out');
			$mod_lock = '1';
			$this->set('address_lock','1');
			$this->opm->set_lock_option('line_entry','products');
			$this->opm->set_lock_option('text_entry','products');

		}

		$arr_set = $this->opm->arr_settings;
		// Get value id
		$iditem = $this->get_id();
		if($iditem=='')
			$iditem = $this->get_last_id();

		$this->set('iditem',$iditem);
		//Load record by id
		if($iditem!=''){
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
			foreach($arr_set['field'] as $ks => $vls){
				foreach($vls as $field => $values){
					if(isset($arr_tmp[$field])){
						$arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
						if(in_array($field,$arr_set['title_field']))
							$item_title[$field] = $arr_tmp[$field];

						if(preg_match("/_date$/",$field) && is_object($arr_tmp[$field]))
							$arr_set['field'][$ks][$field]['default'] = date('m/d/Y',$arr_tmp[$field]->sec);
						//chế độ lock, hiện name của các relationship custom
						else if(($field=='company_name' || $field=='contact_name') && $mod_lock=='1')
							$arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];

						else if($this->opm->check_field_link($ks,$field)){
							$field_id = $arr_set['field'][$ks][$field]['id'];
							if(!isset($arr_set['field'][$ks][$field]['syncname']))
								$arr_set['field'][$ks][$field]['syncname'] = 'name';
							$arr_set['field'][$ks][$field]['default'] = $this->get_name($this->ModuleName($arr_set['field'][$ks][$field]['cls']),$arr_tmp[$field_id],$arr_set['field'][$ks][$field]['syncname']);

						}else if($field=='company_name' && isset($arr_tmp['company_id']) && $arr_tmp['company_id']!=''){
							$arr_set['field'][$ks][$field]['default'] = $this->get_name('Company',$arr_tmp['company_id']);

						}else if($field=='contact_name' && isset($arr_tmp['contact_id']) && $arr_tmp['contact_id']!=''){
							$arr_set['field'][$ks][$field]['default'] = $this->get_name('Contact',$arr_tmp['contact_id']);
							$item_title[$field] = $this->get_name('Contact',$arr_tmp['contact_id']);
						}
						else if($field=='shipper' && isset($arr_tmp['shipper_id']) && $arr_tmp['shipper_id']!=''){
							$arr_set['field'][$ks][$field]['default'] = $this->get_name('Shipper',$arr_tmp['shipper_id']);
						}
					}
				}
			}
			$arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
			$this->Session->write($this->name.'ViewId',$iditem);

			//BEGIN custom
			if(isset($arr_set['field']['panel_1']['code']['default']))
				$item_title['code'] = $arr_set['field']['panel_1']['code']['default'];
			else
				$item_title['code'] = '1';
			if(isset($arr_tmp['shipping_type']))
				$arr_set['field']['panel_1']['code']['after'] = "<div class=\"jttype\">Type: <span class=\"bold\">".$arr_tmp['shipping_type']."</span></div>";
			if(isset($arr_tmp['shipping_type']) && $arr_tmp['shipping_type'] =='In')
				$item_title['shipping_type'] = 'Incoming <span class="inoutgoing"><-</span>';
			else if(isset($arr_tmp['shipping_type']) && $arr_tmp['shipping_type'] =='Out')
				$item_title['shipping_type'] = 'Outgoing <span class="inoutgoing">-></span>';
			else
				$item_title['shipping_type'] ='';
			$this->set('item_title',$item_title);

			//END custom

			//show footer info
			$this->show_footer_info($arr_tmp);


		//add, setup field tự tăng
		}else{
			$nextcode = $this->opm->get_auto_code('code');
			$arr_set['field']['panel_1']['code']['default'] = $nextcode;
			$this->set('item_title',array('code'=>$nextcode));
		}
		$this->set('arr_settings',$arr_set);
		$this->sub_tab_default = 'line_entry';
		$this->sub_tab('',$iditem);
		$this->set_entry_address($arr_tmp,$arr_set);

		parent::entry();
	}


	public function arr_associated_data($field = '', $value = '', $valueid = '') {

		$arr_return[$field]=$value;
		/**
		* Chọn Company
		*/
		if ($field == 'shipper' && $valueid != '') {
			$arr_return['shipper_id'] = new MongoId($valueid);
			$this->selectModel('Company');
			$arr_company = $this->Company->select_one(array('_id'=>new MongoId($valueid)));
			if(isset($arr_company['tracking_url'])&&$arr_company['tracking_url']!='')
				$arr_return['web_tracker']=$arr_company['tracking_url'];
		}
		if ($field == 'signed_by_detail' && $valueid != '') {
			$arr_return['signed_by_detail_id'] = new MongoId($valueid);
		}
		if ($field == 'tracking_no' && $valueid != '') {

			$arr_return['tracking_no'] =$value.' ';
		}
		if($field == 'company_name' && $valueid !=''){
			$arr_return = array(
				'company_name'	=>'',
				'company_id'	=>'',
				'contact_name'	=>'',
				'contact_id'	=>'',
				'our_csr'		=>'',
				'our_csr_id'	=>'',
				'our_rep'		=>'',
				'our_rep_id'	=>'',
				'phone'			=>'',
				'email'			=>'',
				'invoice_address' => array(),
				'shipping_address' => array(),
			);
			//change company
			$arr_return['company_name'] = $value;
			$arr_return['company_id'] = new MongoId($valueid);

			//find contact and more from Company
			$this->selectModel('Company');
			$arr_company = $this->Company->select_one(array('_id'=>new MongoId($valueid)));

			$this->selectModel('Contact');
			$arr_contact = $arrtemp = array();
			// is set contact_default_id
			if(isset($arr_company['contact_default_id']) && is_object($arr_company['contact_default_id'])){
				$arr_contact = $this->Contact->select_one(array('_id'=>$arr_company['contact_default_id']));

			// not set contact_default_id
			}else{
				$arr_contact = $this->Contact->select_all(array(
					'arr_where' => array('company_id'=>new MongoId($valueid)),
					'arr_order' => array('_id'=>-1),
				));
				$arrtemp = iterator_to_array($arr_contact);
				if(count($arrtemp)>0){
					$arr_contact = current($arrtemp);
				}else
					$arr_contact = array();
			}
			//change contact
			if(isset($arr_contact['_id']))
			{
				$arr_return['contact_name']=$arr_contact['first_name'].' '.$arr_contact['last_name'];
				$arr_return['contact_id'] = $arr_contact['_id'];
			}
			else
			{
				$arr_return['contact_name']='';
				$arr_return['contact_id'] = '';
			}




			//change our_csr
			if(isset($arr_company['our_csr']) && isset($arr_company['our_csr_id']) && $arr_company['our_csr_id']!=''){
				$arr_return['our_csr_id'] = $arr_company['our_csr_id'];
				$arr_return['our_csr'] = $arr_company['our_csr'];
			}else{
				$arr_return['our_csr_id'] = $this->opm->user_id();
				$arr_return['our_csr'] = $this->opm->user_name();
			}


			//change our_rep
			if(isset($arr_company['our_responsible']) && isset($arr_company['our_responsible_id']) && $arr_company['our_responsible_id']!=''){
				$arr_return['our_rep_id'] = $arr_company['our_responsible_id'];
				$arr_return['our_rep'] = $arr_company['our_responsible'];
			}else{
				$arr_return['our_rep_id'] = $this->opm->user_id();
				$arr_return['our_rep'] = $this->opm->user_name();
			}

			//change our_rep
			if(isset($arr_company['our_rep']) && isset($arr_company['our_rep_id']) && $arr_company['our_rep_id']!=''){
				$arr_return['our_rep_id'] = $arr_company['our_rep_id'];
				$arr_return['our_rep'] = $arr_company['our_rep'];
			}else{
				$arr_return['our_rep_id'] = $this->opm->user_id();
				$arr_return['our_rep'] = $this->opm->user_name();
			}


			//change phone
			if(isset($arr_company['phone']))
				$arr_return['phone'] = $arr_company['phone'];
			else  // neu khong co phone thi lay phone cua contact mac dinh
			{
				if(isset($arr_contact['direct_dial']))
					$arr_return['phone']=$arr_contact['direct_dial'];
				elseif(!isset($arr_contact['direct_dial'])&&isset($arr_contact['mobile']))
					$arr_return['phone']=$arr_contact['mobile'];
				elseif(!isset($arr_contact['direct_dial'])&&!isset($arr_contact['mobile']))
					$arr_return['phone']='';  //bat buoc phai co dong nay khong thi no se lay du lieu cua cty truoc
			}


			if(isset($arr_company['email']))
				$arr_return['email'] = $arr_company['email'];
			elseif (isset($arr_contact['email']))
				$arr_return['email']=$arr_contact['email'];
			elseif  (!isset($arr_contact['email']))
				$arr_return['email']='';

			if(isset($arr_company['fax']))
				$arr_return['fax'] = $arr_company['fax'];
			elseif (isset($arr_contact['fax']))
				$arr_return['fax']=$arr_contact['fax'];
			elseif  (!isset($arr_contact['fax']))
				$arr_return['fax']='';


			//change address
			if(isset($arr_company['addresses_default_key']))
			{
				$add_default = $arr_company['addresses_default_key'];
				$arr_return['addresses_default_key']= $arr_company['addresses_default_key'];
			}

			if(isset($add_default) && isset($arr_company['addresses'][$add_default])){
				foreach($arr_company['addresses'][$add_default] as $ka=>$va){
					if($ka!='deleted')
						$arr_return['invoice_address'][0]['invoice_'.$ka] = $va;
					else
						$arr_return['invoice_address'][0][$ka] = $va;
				}
			}




		/**
		* Chọn Contact
		*/
		}else if($field == 'contact_name' && $valueid !=''){
			$arr_return = array(
				'contact_name'	=>'',
				'contact_id'	=>'',
				'phone'			=>'',
				'email'			=>'',
			);
			//change company
			$arr_return['contact_name'] = $value;
			$arr_return['contact_id'] = new MongoId($valueid);
			//find more from contact
			$this->selectModel('Contact');
			$arr_contact = $this->Contact->select_one(array('_id'=>new MongoId($valueid)));
			//change phone
			if(isset($arr_contact['direct_dial']) && $arr_contact['direct_dial']!='')
				$arr_return['phone'] = $arr_contact['direct_dial'];
			//change email
			if(isset($arr_contact['email']))
				$arr_return['email'] = $arr_contact['email'];
			//nếu company khác hiện có
			if(isset($arr_contact['company_id'])){
				echo '';
			}

		}elseif($field == 'shipper' && $valueid != '') {
			$arr_return['shipper_id'] = new MongoId($valueid);
		}



		return $arr_return;
    }


	public function entry_search(){
		//parent class
		$arr_set = $this->opm->arr_settings;
		$arr_set['field']['panel_1']['code']['lock'] = '';
		$arr_set['field']['panel_1']['quotation_type']['element_input'] = '';
		$arr_set['field']['panel_1']['our_rep']['not_custom'] = '0';
		$arr_set['field']['panel_1']['our_csr']['not_custom'] = '0';
		$arr_set['field']['panel_4']['job_name']['not_custom'] = '0';
		$arr_set['field']['panel_4']['job_number']['lock'] = '0';
		$arr_set['field']['panel_4']['salesorder_name']['not_custom'] = '0';
		$arr_set['field']['panel_4']['salesorder_number']['lock'] = '0';
		$arr_set['field']['panel_1']['quotation_type']['default'] = '';
		$arr_set['field']['panel_1']['quotation_date']['default'] = '';
		$arr_set['field']['panel_4']['quotation_status']['default'] = '';
		$arr_set['field']['panel_4']['payment_due_date']['default'] = '';
		$arr_set['field']['panel_4']['payment_terms']['default'] = '';
		$arr_set['field']['panel_4']['tax']['default'] = '';

		$this->set('search_class','jt_input_search');
		$this->set('search_class2','jt_select_search');
		$this->set('search_flat','placeholder="1"');
		$where = array();
		if($this->Session->check($this->name.'_where'))
			$where = $this->Session->read($this->name.'_where');
		if(count($where)>0){
			foreach($arr_set['field'] as $ks => $vls){
				foreach($vls as $field => $values){
					if(isset($where[$field])){
						$arr_set['field'][$ks][$field]['default'] = $where[$field]['values'];
					}
				}
			}
		}
		//end parent class
		$this->set('arr_settings',$arr_set);

		//set address
		$address_label = array('Shipping address','Invoice Address');
		$this->set('address_label',$address_label);
		$address_conner[0]['top'] 		= 'hgt fixbor';
		$address_conner[0]['bottom'] 	= 'fixbor2 jt_ppbot';
		$address_conner[1]['top'] 		= 'hgt';
		$address_conner[1]['bottom'] 	= 'fixbor3 jt_ppbot';
		$this->set('address_conner',$address_conner);
		$this->set('address_more_line',2);//set
		$address_hidden_field = array('shipping_address','invoice_address');
		$this->set('address_hidden_field',$address_hidden_field);//set
		$address_country = $this->country();
		$this->set('address_country',$address_country);//set
		$this->set('address_country_id','Canada');//set
		$address_province['invoice'] = $address_province['shipping'] = $this->province("CA");
		$this->set('address_province',"");//set
		$this->set('address_province_id',"");//set
		$this->set('address_onchange',"save_address_pr('\"+keys+\"');");
		$address_hidden_value=array('','');
		$this->set('address_hidden_value',$address_hidden_value);
		$this->set('address_mode','search');
	}



	// Options list
	public function swith_options($keys) {
        parent::swith_options($keys);
	 	if ($keys == 'in_porgress')
	 	{
            $arr_where['shipping_status'] = array('values' => 'In progress', 'operator' => '=');
            $this->Session->write($this->name . '_where', $arr_where);
            echo URL . '/' . $this->params->params['controller'] . '/lists';
        }
        else if ($keys == 'complete')
	 	{
            $arr_where['shipping_status'] = array('values' => 'Completed', 'operator' => '=');
            $this->Session->write($this->name . '_where', $arr_where);
            echo URL . '/' . $this->params->params['controller'] . '/lists';
        }
        else if ($keys == 'cancelled')
	 	{
            $arr_where['shipping_status'] = array('values' => 'Cancelled', 'operator' => '=');
            $this->Session->write($this->name . '_where', $arr_where);
            echo URL . '/' . $this->params->params['controller'] . '/lists';
        }
        else if ($keys == 'outstanding') {
            $or_where = array(
                array("quotation_status" => 'Approved'),
                array("quotation_status" => 'Rejected'),
                array("quotation_status" => 'Cancelled')
            );
            $arr_where = array();
            $arr_where[] = array('values' => $or_where, 'operator' => 'or');
            $this->Session->write($this->name . '_where', $arr_where);
            echo URL . '/' . $this->params->params['controller'] . '/lists';
        }
        else if ($keys == 'existing')
            echo URL . '/' . $this->params->params['controller'] . '/entry';
        else if ($keys == 'under')
            echo URL . '/' . $this->params->params['controller'] . '/entry';
        else if ($keys == 'report_by_customer_summary')
            echo URL . '/' . $this->params->params['controller'] . '/option_summary_customer_find';
        else if ($keys == 'report_by_customer_detailed')
            echo URL . '/' . $this->params->params['controller'] . '/option_detailed_customer_find';
        else if ($keys == 'report_by_product_summary')
            echo URL . '/' . $this->params->params['controller'] . '/option_summary_product_find';
        else if ($keys == 'report_by_product_detailed')
            echo URL . '/' . $this->params->params['controller'] . '/option_detailed_product_find';
        else if ($keys == 'print_mini_list')
	        echo URL . '/' . $this->params->params['controller'] . '/view_minilist';
        else if ($keys == 'print_shipping_note')
	        echo URL . '/' . $this->params->params['controller'] . '/view_shipping_note';


        else if ($keys == 'email_shipping_note')
	        echo URL . '/' . $this->params->params['controller'] . '/email_shipping_note';



        else
            echo '';
        die;
    }

	var $is_text = 0;

	public function email_shipping_note(){
		$this->layout = 'pdf';
		$ids = $this->get_id();
		if($ids!=''){
			$query = $this->opm->select_one(array('_id' =>new MongoId($ids)));
			$arrtemp = $query;
			//set header
			$this->set('logo_link','img/logo_anvy.jpg');
			$this->set('company_address','Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />');

			//customer address
			$customer = '';
			if(isset($arrtemp['company_id']) && strlen($arrtemp['company_id'])==24)
				$customer .= '<b>'.$this->get_name('Company',$arrtemp['company_id']).'</b><br />';
			else if(isset($arrtemp['company_name']))
				$customer .= '<b>'.$arrtemp['company_name'].'</b><br />';
			if(isset($arrtemp['contact_id']) && strlen($arrtemp['contact_id'])==24)
				$customer .= $this->get_name('Contact',$arrtemp['contact_id']).'<br />';
			else if(isset($arrtemp['contact_name']))
				$customer .= $arrtemp['contact_name'].'<br />';

			//loop 2 address
			$arradd = array('invoice','shipping');
			foreach($arradd as $vvs){
				$kk = $vvs; $customer_address = '';
				if(isset($arrtemp[$kk.'_address']) && isset($arrtemp[$kk.'_address'][0]) && count($arrtemp[$kk.'_address'])>0){
					$temp = $arrtemp[$kk.'_address'][0];
					if(isset($temp[$kk.'_address_1']) && $temp[$kk.'_address_1']!='')
						$customer_address .= $temp[$kk.'_address_1'].' ';
					if(isset($temp[$kk.'_address_2']) && $temp[$kk.'_address_2']!='')
						$customer_address .= $temp[$kk.'_address_2'].' ';
					if(isset($temp[$kk.'_address_3']) && $temp[$kk.'_address_3']!='')
						$customer_address .= $temp[$kk.'_address_3'].'<br />';
					else
						$customer_address .= '<br />';
					if(isset($temp[$kk.'_town_city']) && $temp[$kk.'_town_city']!='')
						$customer_address .= $temp[$kk.'_town_city'];

					if(isset($temp[$kk.'_province_state']))
						$customer_address .= ' '.$temp[$kk.'_province_state'].' ';
					else if(isset($temp[$kk.'_province_state_id']) && isset($temp[$kk.'_country_id'])){
						$keytemp = $temp[$kk.'_province_state_id'];
						$provkey = $this->province($temp[$kk.'_country_id']);
						if(isset($provkey[$temp]))
							$customer_address .= ' '.$provkey[$temp].' ';
					}


					if(isset($temp[$kk.'_zip_postcode']) && $temp[$kk.'_zip_postcode']!='')
						$customer_address .= $temp[$kk.'_zip_postcode'];

					if(isset($temp[$kk.'_country']) && isset($temp[$kk.'_country_id']) && (int)$temp[$kk.'_country_id']!="CA")
						$customer_address .= ' '.$temp[$kk.'_country'].'<br />';
					else
						$customer_address .= '<br />';
					$arr_address[$kk] = $customer_address;
				}
			}

			if(isset($arrtemp['name']) && $arrtemp['name']!='')
				$heading = $arrtemp['name'];
			else
				$heading = '';
			if(!isset($arr_address['invoice']))
				$arr_address['invoice'] = '';
			$this->set('customer_address',$customer.$arr_address['invoice']);
			if(!isset($arr_address['shipping']))
				$arr_address['shipping'] = '';
			$this->set('shipping_address',$arr_address['shipping']);

			// info data
			$info_data = (object) array();
			$info_data->contact_name = $arrtemp['contact_name'];
			$info_data->no = $arrtemp['code'];
			$info_data->job_no =  isset($arrtemp['job_number'])?$arrtemp['job_number']:'';
			$info_data->date = (isset($arrtemp['invoice_date'])&&$arrtemp['invoice_date']!='' ? $this->opm->format_date($arrtemp['invoice_date']) : '');
			$info_data->po_no = (isset($arrtemp['customer_po_no']) ? $arrtemp['customer_po_no'] : '');
			$info_data->ac_no = '';
			$info_data->terms = (isset($arrtemp['payment_terms']) ? $arrtemp['payment_terms'] : '');
			$info_data->required_date = (isset($arrtemp['payment_due_date'])&&$arrtemp['payment_due_date']!='' ? $this->opm->format_date($arrtemp['payment_due_date']) : '');
			$this->set('info_data', $info_data);


			//$this->set('quote_date',$this->opm->format_date($arrtemp['quotation_date']));
			/**Nội dung bảng giá */
			$date_now = date('Ymd');
			$time=time();
			$filename = 'SN'.$date_now.$time.'-'.$info_data->no;

			$thisfolder = 'upload'.DS.date("Y_m");
			$thisfolder_1='upload'.','.date("Y_m");

			$folder = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.$thisfolder;
			if (!file_exists($folder)) {
				mkdir($folder, 0777, true);
			}

			$this->set('filename', $filename);
			$this->set('heading',$heading);
			$html_cont = '';
			if(isset($arrtemp['products']) && is_array($arrtemp['products']) && count($arrtemp['products'])>0){
				$line = 0; $colum = 7;
				foreach($arrtemp['products'] as $keys=>$values){
					if(!$values['deleted']){
						if($line%2==0)
							$bgs = '#fdfcfa';
						else
							$bgs = '#eeeeee';
						//code
						$html_cont .= '<tr style="background-color:'.$bgs.';"><td class="first">';
						if(isset($values['code']))
							$html_cont .= '  '.$values['code'];
						else
							$html_cont .= '  #'.$keys;
						//desription
						$html_cont .= '</td><td>';
						if(isset($values['products_name']))
							$html_cont .= str_replace("\n","<br />",$values['products_name']);
						else
							$html_cont .= 'Empty';
						//width
						$html_cont .= '</td><td align="right">';
						if(isset($values['sizew']) && $values['sizew']!='' && isset($values['sizew_unit']) && $values['sizew_unit']!='')
							$html_cont .= $values['sizew'].' ('.$values['sizew_unit'].')';
						else if(isset($values['sizew'])&& $values['sizew']!='')
							$html_cont .= $values['sizew'].' (in.)';
						else
							$html_cont .= '';
						//height
						$html_cont .= '</td><td align="right">';
						if(isset($values['sizeh']) && $values['sizeh']!='' && isset($values['sizeh_unit']) && $values['sizeh_unit']!='')
							$html_cont .= $values['sizeh'].' ('.$values['sizeh_unit'].')';
						else if(isset($values['sizeh']) && $values['sizeh']!='' )
							$html_cont .= $values['sizeh'].' (in.)';
						else
							$html_cont .= '';
						//Unit price
						$html_cont .= '</td><td align="right">';
						if(isset($values['unit_price']))
							$html_cont .= $this->opm->format_currency($values['unit_price']);
						else
							$html_cont .= '0.00';
						//Qty
						$html_cont .= '</td><td align="right">';
						if(isset($values['quantity']))
							$html_cont .= $values['quantity'];
						else
							$html_cont .= '';
						//line total
						$html_cont .= '</td><td align="right" class="end">';
						if(isset($values['sub_total']))
							$html_cont .= $this->opm->format_currency($values['sub_total']);
						else
							$html_cont .= '';


						$html_cont .= '</td></tr>';
						$line++;
					}//end if deleted
				}//end for


				if($line%2==0){
					$bgs = '#fdfcfa';$bgs2 = '#eeeeee';
				}else{
					$bgs = '#eeeeee';$bgs2 = '#fdfcfa';
				}

				$sub_total = $total = $taxtotal = 0.00;
				if(isset($arrtemp['sum_sub_total']))
					$sub_total = $arrtemp['sum_sub_total'];
				if(isset($arrtemp['sum_tax']))
					$taxtotal = $arrtemp['sum_tax'];
				if(isset($arrtemp['sum_amount']))
					$total = $arrtemp['sum_amount'];
				//Sub Total
				$html_cont .= '<tr style="background-color:'.$bgs.';">
									<td colspan="'.($colum-1).'" align="right" style="font-weight:bold;border-top:2px solid #aaa;" class="first">Sub Total:</td>
									<td align="right" style="border-top:2px solid #aaa;" class="end">'.$this->opm->format_currency($sub_total).'</td>
							   </tr>';
				//GST
				$html_cont .= '<tr style="background-color:'.$bgs2.';">
									<td colspan="'.($colum-1).'" align="right" style="font-weight:bold;" class="first">HST/GST:</td>
									<td align="right" class="end">'.$this->opm->format_currency($taxtotal).'</td>
							   </tr>';
				//Total
				$html_cont .= '<tr style="background-color:'.$bgs.';">
									<td colspan="'.($colum-1).'" align="right" style="font-weight:bold;" class="first bottom">Total:</td>
									<td align="right" class="end bottom">'.$this->opm->format_currency($total).'</td>
							   </tr>';

			}//end if


			$this->set('html_cont',$html_cont);
			if(isset($arrtemp['our_csr'])){
				$this->set('user_name',' '.$arrtemp['our_csr']);
			}else
				$this->set('user_name',' '.$this->opm->user_name());
			//end set content

			//set footer

			$this->set('link_this_folder',$thisfolder);

			$this->render('email_shipping_note');

			$v_link_pdf= $thisfolder_1.','.$filename.'.pdf';
			$v_file_name=$filename.'.pdf';

			$this->redirect('/docs/add_from_option/'.$this->ModuleName().'/'.$this->get_id().'/'.$v_link_pdf.'/'.$v_file_name.'/'.$this->params->params['controller'].'');

		}
		die;
	}
	//subtab line entry
	public function view_shipping_note(){
		$this->layout = 'pdf';
		$ids = $this->get_id();
		if($ids!=''){
			$query = $this->opm->select_one(array('_id' =>new MongoId($ids)));
			$arrtemp = $query;
			//set header
			$this->set('logo_link','img/logo_anvy.jpg');
			$this->set('company_address','Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />');

			//customer address
			$customer = '';
			if(isset($arrtemp['company_id']) && strlen($arrtemp['company_id'])==24)
				$customer .= '<b>'.$this->get_name('Company',$arrtemp['company_id']).'</b><br />';
			else if(isset($arrtemp['company_name']))
				$customer .= '<b>'.$arrtemp['company_name'].'</b><br />';
			if(isset($arrtemp['contact_id']) && strlen($arrtemp['contact_id'])==24)
				$customer .= $this->get_name('Contact',$arrtemp['contact_id']).'<br />';
			else if(isset($arrtemp['contact_name']))
				$customer .= $arrtemp['contact_name'].'<br />';

			//loop 2 address
			$arradd = array('invoice','shipping');
			foreach($arradd as $vvs){
				$kk = $vvs; $customer_address = '';
				if(isset($arrtemp[$kk.'_address']) && isset($arrtemp[$kk.'_address'][0]) && count($arrtemp[$kk.'_address'])>0){
					$temp = $arrtemp[$kk.'_address'][0];
					if(isset($temp[$kk.'_address_1']) && $temp[$kk.'_address_1']!='')
						$customer_address .= $temp[$kk.'_address_1'].' ';
					if(isset($temp[$kk.'_address_2']) && $temp[$kk.'_address_2']!='')
						$customer_address .= $temp[$kk.'_address_2'].' ';
					if(isset($temp[$kk.'_address_3']) && $temp[$kk.'_address_3']!='')
						$customer_address .= $temp[$kk.'_address_3'].'<br />';
					else
						$customer_address .= '<br />';
					if(isset($temp[$kk.'_town_city']) && $temp[$kk.'_town_city']!='')
						$customer_address .= $temp[$kk.'_town_city'];

					if(isset($temp[$kk.'_province_state']))
						$customer_address .= ' '.$temp[$kk.'_province_state'].' ';
					else if(isset($temp[$kk.'_province_state_id']) && isset($temp[$kk.'_country_id'])){
						$keytemp = $temp[$kk.'_province_state_id'];
						$provkey = $this->province($temp[$kk.'_country_id']);
						if(isset($provkey[$temp]))
							$customer_address .= ' '.$provkey[$temp].' ';
					}


					if(isset($temp[$kk.'_zip_postcode']) && $temp[$kk.'_zip_postcode']!='')
						$customer_address .= $temp[$kk.'_zip_postcode'];

					if(isset($temp[$kk.'_country']) && isset($temp[$kk.'_country_id']) && (int)$temp[$kk.'_country_id']!="CA")
						$customer_address .= ' '.$temp[$kk.'_country'].'<br />';
					else
						$customer_address .= '<br />';
					$arr_address[$kk] = $customer_address;
				}
			}

			if(isset($arrtemp['name']) && $arrtemp['name']!='')
				$heading = $arrtemp['name'];
			else
				$heading = '';
			if(!isset($arr_address['invoice']))
				$arr_address['invoice'] = '';
			$this->set('customer_address',$customer.$arr_address['invoice']);
			if(!isset($arr_address['shipping']))
				$arr_address['shipping'] = '';
			$this->set('shipping_address',$arr_address['shipping']);

			// info data
			$info_data = (object) array();
			$info_data->contact_name = $arrtemp['contact_name'];
			$info_data->no = $arrtemp['code'];
			$info_data->job_no = isset($arrtemp['job_number'])?$arrtemp['job_number']:'';
			$info_data->date = (isset($arrtemp['invoice_date'])&&$arrtemp['invoice_date']!='' ? $this->opm->format_date($arrtemp['invoice_date']) : '');
			$info_data->po_no = (isset($arrtemp['customer_po_no']) ? $arrtemp['customer_po_no'] : '');
			$info_data->ac_no = '';
			$info_data->terms = (isset($arrtemp['payment_terms']) ? $arrtemp['payment_terms'] : '');
			$info_data->required_date = (isset($arrtemp['payment_due_date'])&&$arrtemp['payment_due_date']!='' ? $this->opm->format_date($arrtemp['payment_due_date']) : '');
			$this->set('info_data', $info_data);


			//$this->set('quote_date',$this->opm->format_date($arrtemp['quotation_date']));
			/**Nội dung bảng giá */
			$date_now = date('Ymd');
			$time=time();
			$filename = 'SN'.$date_now.$time.'-'.$info_data->no;

			$this->set('filename', $filename);
			$this->set('heading',$heading);
			$html_cont = '';
			if(isset($arrtemp['products']) && is_array($arrtemp['products']) && count($arrtemp['products'])>0){
				$line = 0; $colum = 7;
				foreach($arrtemp['products'] as $keys=>$values){
					if(!$values['deleted']){
						if($line%2==0)
							$bgs = '#fdfcfa';
						else
							$bgs = '#eeeeee';
						//code
						$html_cont .= '<tr style="background-color:'.$bgs.';"><td class="first">';
						if(isset($values['code']))
							$html_cont .= '  '.$values['code'];
						else
							$html_cont .= '  #'.$keys;
						//desription
						$html_cont .= '</td><td>';
						if(isset($values['products_name']))
							$html_cont .= str_replace("\n","<br />",$values['products_name']);
						else
							$html_cont .= 'Empty';
						//width
						$html_cont .= '</td><td align="right">';
						if(isset($values['sizew']) && $values['sizew']!='' && isset($values['sizew_unit']) && $values['sizew_unit']!='')
							$html_cont .= $values['sizew'].' ('.$values['sizew_unit'].')';
						else if(isset($values['sizew'])&& $values['sizew']!='')
							$html_cont .= $values['sizew'].' (in.)';
						else
							$html_cont .= '';
						//height
						$html_cont .= '</td><td align="right">';
						if(isset($values['sizeh']) && $values['sizeh']!='' && isset($values['sizeh_unit']) && $values['sizeh_unit']!='')
							$html_cont .= $values['sizeh'].' ('.$values['sizeh_unit'].')';
						else if(isset($values['sizeh']) && $values['sizeh']!='' )
							$html_cont .= $values['sizeh'].' (in.)';
						else
							$html_cont .= '';
						//Unit price
						$html_cont .= '</td><td align="right">';
						if(isset($values['unit_price']))
							$html_cont .= $this->opm->format_currency($values['unit_price']);
						else
							$html_cont .= '0.00';
						//Qty
						$html_cont .= '</td><td align="right">';
						if(isset($values['quantity']))
							$html_cont .= $values['quantity'];
						else
							$html_cont .= '';
						//line total
						$html_cont .= '</td><td align="right" class="end">';
						if(isset($values['sub_total']))
							$html_cont .= $this->opm->format_currency($values['sub_total']);
						else
							$html_cont .= '';


						$html_cont .= '</td></tr>';
						$line++;
					}//end if deleted
				}//end for


				if($line%2==0){
					$bgs = '#fdfcfa';$bgs2 = '#eeeeee';
				}else{
					$bgs = '#eeeeee';$bgs2 = '#fdfcfa';
				}

				$sub_total = $total = $taxtotal = 0.00;
				if(isset($arrtemp['sum_sub_total']))
					$sub_total = $arrtemp['sum_sub_total'];
				if(isset($arrtemp['sum_tax']))
					$taxtotal = $arrtemp['sum_tax'];
				if(isset($arrtemp['sum_amount']))
					$total = $arrtemp['sum_amount'];
				//Sub Total
				$html_cont .= '<tr style="background-color:'.$bgs.';">
									<td colspan="'.($colum-1).'" align="right" style="font-weight:bold;border-top:2px solid #aaa;" class="first">Sub Total:</td>
									<td align="right" style="border-top:2px solid #aaa;" class="end">'.$this->opm->format_currency($sub_total).'</td>
							   </tr>';
				//GST
				$html_cont .= '<tr style="background-color:'.$bgs2.';">
									<td colspan="'.($colum-1).'" align="right" style="font-weight:bold;" class="first">HST/GST:</td>
									<td align="right" class="end">'.$this->opm->format_currency($taxtotal).'</td>
							   </tr>';
				//Total
				$html_cont .= '<tr style="background-color:'.$bgs.';">
									<td colspan="'.($colum-1).'" align="right" style="font-weight:bold;" class="first bottom">Total:</td>
									<td align="right" class="end bottom">'.$this->opm->format_currency($total).'</td>
							   </tr>';

			}//end if


			$this->set('html_cont',$html_cont);
			if(isset($arrtemp['our_csr'])){
				$this->set('user_name',' '.$arrtemp['our_csr']);
			}else
				$this->set('user_name',' '.$this->opm->user_name());
			//end set content

			//set footer
			$this->render('view_shipping_note');
			$this->redirect('/upload/'.$filename.'.pdf');
		}
		die;
	}
	public function line_entry(){
		$is_text = $this->is_text;
		if($this->check_lock()){
			$this->opm->set_lock_option('line_entry','products');
			$this->opm->set_lock_option('text_entry','products');
		}

		$subdatas = $arr_ret = array(); $codeauto = 0; $opname = 'products';
		$sum_sub_total = $sum_tax = 0;
		$subdatas[$opname] = array();
		$ids = $this->get_id();
		if($ids!=''){
			//update sum
			$keyfield = array(
							"sub_total"		=> "sub_total",
							"tax"			=> "tax",
							"amount"		=> "amount",
							"sum_sub_total"	=> "sum_sub_total",
							"sum_tax"		=> "sum_tax",
							"sum_amount"	=> "sum_amount",
						);
			$arr_sum = $this->update_sum('products',$keyfield);
			//get entry data
			$arr_ret = $this->line_entry_data($opname,$is_text);
			if(isset($arr_ret[$opname]))
				$subdatas[$opname] = $arr_ret[$opname];
		}
		$this->set('subdatas', $subdatas);
		$codeauto = $this->opm->get_auto_code('code');
		$this->set('nextcode',$codeauto);
		$this->set('file_name','quotation_'.$this->get_id());
		$this->set('sum_sub_total',$arr_ret['sum_sub_total']);
		$this->set('sum_amount',$arr_ret['sum_amount']);
		$this->set('sum_tax',$arr_ret['sum_tax']);
		$this->set_select_data_list('relationship','line_entry');
	}

	//check and cal for Line Entry
	public function line_entry_data($opname='',$is_text=0){
		$arr_ret = array();
		$this->selectModel('Setting');
		if($this->get_id()!=''){
			$newdata = $option_select_dynamic = array();
			$query = $this->opm->select_one(array('_id' =>new MongoId($this->get_id())));
			//set sum
			$arr_ret['sum_sub_total'] = $arr_ret['sum_amount'] = $arr_ret['sum_tax'] = '0.00';
			if(isset($query['sum_sub_total']) && $query['sum_sub_total']!='')
				$arr_ret['sum_sub_total'] = $query['sum_sub_total'];
			if(isset($query['sum_amount']) && $query['sum_amount']!='')
				$arr_ret['sum_amount'] = $query['sum_amount'];
			if(isset($query['sum_tax']) && $query['sum_tax']!='')
				$arr_ret['sum_tax'] = $query['sum_tax'];

			if(isset($query[$opname]) && is_array($query[$opname])){
				foreach($query[$opname] as $key=>$arr){
					if(!$arr['deleted']){
						$newdata[$key] = $arr;
						//chuyển html products_name <br /> thành \n
						//$newdata[$key]['products_name'] = str_replace("<br>","\n",$arr['products_name']);
						//nếu là line entry thì hiện products_name 1 dòng
						if(isset($newdata[$key]['products_name']) && $is_text!=1){
							$arrtmp = explode("\n",$newdata[$key]['products_name']);
							$newdata[$key]['products_name'] = $arrtmp[0];
						}
						//set all price in display
						if(isset($arr['area']))
							$newdata[$key]['area'] = (float)$arr['area'];
						if(isset($arr['unit_price']))
							$newdata[$key]['unit_price'] = number_format((float)$arr['unit_price'],3);
						else
							$newdata[$key]['unit_price'] = '0.000';
						if(isset($arr['sub_total']))
							$newdata[$key]['sub_total'] = number_format((float)$arr['sub_total'],2);
						else
							$newdata[$key]['sub_total'] =  '0.00';
						if(isset($arr['tax']))
							$newdata[$key]['tax'] = number_format((float)$arr['tax'],3);
						else
							$newdata[$key]['tax'] = '0.000';
						if(isset($arr['amount']))
							$newdata[$key]['amount'] = number_format((float)$arr['amount'],2);
						else
							$newdata[$key]['amount'] = '0.00';

						$newdata[$key]['_id'] = $key;

						//tính tổng khi lặp vòng
						/*if(isset($arr['sub_total']))
							$arr_ret['sum_sub_total'] += (float)$arr['sub_total'];
						if(isset($arr['tax']))
							$arr_ret['sum_tax'] += (float)$arr['tax'];*/

						//data RFQ's
						$receipts = 0;
						if(isset($query['rfqs']) && is_array($query['rfqs']) && count($query['rfqs'])>0){
							foreach($query['rfqs'] as $rk=>$rv){
								if(!$rv['deleted'] && isset($rv['rfq_code']) && (int)$rv['rfq_code']==$key){
									$receipts = 1;
								}
							}
							$newdata[$key]['receipts'] = $receipts;
						}else
							$newdata[$key]['receipts'] = 0;

						//chặn không cho custom size nếu is_custom_size = 1
						if(isset($arr['is_custom_size']) && (int)$arr['is_custom_size'] ==1){
							$newdata[$key]['attr']['sizeh'] = 'readonly="readonly"';
							$newdata[$key]['attr']['sizew'] = 'readonly="readonly"';
							$newdata[$key]['attr']['sizeh_unit'] = 'readonly="readonly"';
							$newdata[$key]['attr']['sizew_unit'] = 'readonly="readonly"';
							$newdata[$key]['attr']['sell_by'] = 'readonly="readonly"';
						}
						//set lại select dựa vào loại sell_by
						if(isset($newdata[$key]['sell_by'])){
							$option_select_dynamic['oum_'.$key] = $this->Setting->select_option_vl(array('setting_value'=>'product_oum_'.strtolower($arr['sell_by'])));
						}
					} //end if
				}
			}
			$arr_ret[$opname] = $newdata;
			//pr($arr_ret);die;
		}
		$this->set('option_select_dynamic',$option_select_dynamic);
		return $arr_ret;
	}


	//subtab Text entry
	public function view_product_option(){
		echo '';
		die;
	}

	//subtab Text entry
	public function text_entry(){
		$this->is_text = 1;
		$this->line_entry();
	}

	//Text pdf
	public function test_pdf(){
		$this->layout = 'pdf';
		//set footer
		$this->render('test_pdf');
	}
	public function email_pdf(){
		$this->layout = 'pdf';
		$ids = $this->get_id();
		if($ids!=''){
			$query = $this->opm->select_one(array('_id' =>new MongoId($ids)));
			$arrtemp = $query;
			//set header
			$this->set('logo_link','img/logo_anvy.jpg');
			$this->set('company_address','Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />');

			//customer address
			$customer = '';
			if(isset($arrtemp['company_id']) && strlen($arrtemp['company_id'])==24)
				$customer .= '<b>'.$this->get_name('Company',$arrtemp['company_id']).'</b><br />';
			else if(isset($arrtemp['company_name']))
				$customer .= '<b>'.$arrtemp['company_name'].'</b><br />';
			if(isset($arrtemp['contact_id']) && strlen($arrtemp['contact_id'])==24)
				$customer .= $this->get_name('Contact',$arrtemp['contact_id']).'<br />';
			else if(isset($arrtemp['contact_name']))
				$customer .= $arrtemp['contact_name'].'<br />';

			//loop 2 address
			$arradd = array('invoice','shipping');
			foreach($arradd as $vvs){
				$kk = $vvs; $customer_address = '';
				if(isset($arrtemp[$kk.'_address']) && isset($arrtemp[$kk.'_address'][0]) && count($arrtemp[$kk.'_address'])>0){
					$temp = $arrtemp[$kk.'_address'][0];
					if(isset($temp[$kk.'_address_1']) && $temp[$kk.'_address_1']!='')
						$customer_address .= $temp[$kk.'_address_1'].' ';
					if(isset($temp[$kk.'_address_2']) && $temp[$kk.'_address_2']!='')
						$customer_address .= $temp[$kk.'_address_2'].' ';
					if(isset($temp[$kk.'_address_3']) && $temp[$kk.'_address_3']!='')
						$customer_address .= $temp[$kk.'_address_3'].'<br />';
					else
						$customer_address .= '<br />';
					if(isset($temp[$kk.'_town_city']) && $temp[$kk.'_town_city']!='')
						$customer_address .= $temp[$kk.'_town_city'];

					if(isset($temp[$kk.'_province_state']))
						$customer_address .= ' '.$temp[$kk.'_province_state'].' ';
					else if(isset($temp[$kk.'_province_state_id']) && isset($temp[$kk.'_country_id'])){
						$keytemp = $temp[$kk.'_province_state_id'];
						$provkey = $this->province($temp[$kk.'_country_id']);
						if(isset($provkey[$temp]))
							$customer_address .= ' '.$provkey[$temp].' ';
					}


					if(isset($temp[$kk.'_zip_postcode']) && $temp[$kk.'_zip_postcode']!='')
						$customer_address .= $temp[$kk.'_zip_postcode'];

					if(isset($temp[$kk.'_country']) && isset($temp[$kk.'_country_id']) && (int)$temp[$kk.'_country_id']!="CA")
						$customer_address .= ' '.$temp[$kk.'_country'].'<br />';
					else
						$customer_address .= '<br />';
					$arr_address[$kk] = $customer_address;
				}
			}

			if(isset($arrtemp['name']) && $arrtemp['name']!='')
				$heading = $arrtemp['name'];
			else
				$heading = '';
			if(!isset($arr_address['invoice']))
				$arr_address['invoice'] = '';
			$this->set('customer_address',$customer.$arr_address['invoice']);
			if(!isset($arr_address['shipping']))
				$arr_address['shipping'] = '';
			$this->set('shipping_address',$arr_address['shipping']);

			// info data
			$info_data = (object) array();
			$info_data->contact_name = $arrtemp['contact_name'];
			$info_data->no = $arrtemp['code'];
			$info_data->job_no = (isset($arrtemp['job_number']) ? $arrtemp['job_number'] : '');
			$info_data->date = (isset($arrtemp['invoice_date'])&&$arrtemp['invoice_date']!='' ? $this->opm->format_date($arrtemp['invoice_date']) : '');
			$info_data->po_no = (isset($arrtemp['customer_po_no']) ? $arrtemp['customer_po_no'] : '');
			$info_data->ac_no = '';
			$info_data->terms = (isset($arrtemp['payment_terms']) ? $arrtemp['payment_terms'] : '');
			$info_data->required_date = (isset($arrtemp['payment_due_date'])&&$arrtemp['payment_due_date']!='' ? $this->opm->format_date($arrtemp['payment_due_date']) : '');
			$this->set('info_data', $info_data);


			//$this->set('quote_date',$this->opm->format_date($arrtemp['quotation_date']));
			/**Nội dung bảng giá */
			$date_now = date('Ymd');
			$time=time();
			$filename = 'SH'.$date_now.$time.'-'.$info_data->no;

			$thisfolder = 'upload'.DS.date("Y_m");
			$thisfolder_1='upload'.','.date("Y_m");

			$folder = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.$thisfolder;
			if (!file_exists($folder)) {
				mkdir($folder, 0777, true);
			}



			$this->set('filename', $filename);
			$this->set('heading',$heading);
			$html_cont = '';
			if(isset($arrtemp['products']) && is_array($arrtemp['products']) && count($arrtemp['products'])>0){
				$line = 0; $colum = 7;
				foreach($arrtemp['products'] as $keys=>$values){
					if(!$values['deleted']){
						if($line%2==0)
							$bgs = '#fdfcfa';
						else
							$bgs = '#eeeeee';
						//code
						$html_cont .= '<tr style="background-color:'.$bgs.';"><td class="first">';
						if(isset($values['code']))
							$html_cont .= '  '.$values['code'];
						else
							$html_cont .= '  #'.$keys;
						//desription
						$html_cont .= '</td><td>';
						if(isset($values['products_name']))
							$html_cont .= str_replace("\n","<br />",$values['products_name']);
						else
							$html_cont .= 'Empty';
						//quantity
						$html_cont .= '</td><td align="right">';
						$html_cont .= (isset($values['quantity']) ? $values['quantity'] : 0);
						//prev.
						$html_cont .= '</td><td align="right">';
						$html_cont .= (isset($values['prev_shipped']) ? $values['prev_shipped'] : '');
						//Now
						$html_cont .= '</td><td align="right">';
						$html_cont .= (isset($values['shipped']) ? $values['shipped'] : 0);
						//B/O
						$html_cont .= '</td><td align="right">';
						$html_cont .= (isset($values['balance_shipped']) ? $values['balance_shipped'] : 0);


						$html_cont .= '</td></tr>';
						$line++;
					}//end if deleted
				}//end for


				if($line%2==0){
					$bgs = '#fdfcfa';$bgs2 = '#eeeeee';
				}else{
					$bgs = '#eeeeee';$bgs2 = '#fdfcfa';
				}


			}//end if


			$this->set('html_cont',$html_cont);
			if(isset($arrtemp['our_csr'])){
				$this->set('user_name',' '.$arrtemp['our_csr']);
			}else
				$this->set('user_name',' '.$this->opm->user_name());
			//end set content

			//set footer
			$this->set('link_this_folder',$thisfolder);
			$this->render('email_pdf');
			$v_link_pdf= $thisfolder_1.','.$filename.'.pdf';
			$v_file_name=$filename.'.pdf';
			$this->redirect('/docs/add_from_option/'.$this->ModuleName().'/'.$this->get_id().'/'.$v_link_pdf.'/'.$v_file_name.'/'.$this->params->params['controller'].'');

		}
		die;
	}
	//Export pdf
	public function view_pdf($getfile=false){
		$this->layout = 'pdf';
		$ids = $this->get_id();
		if($ids!=''){
			$query = $this->opm->select_one(array('_id' =>new MongoId($ids)));
			$arrtemp = $query;
			//set header
			$this->set('logo_link','img/logo_anvy.jpg');
			$this->set('company_address','Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />');

			//customer address
			$customer = '';
			if(isset($arrtemp['company_id']) && strlen($arrtemp['company_id'])==24)
				$customer .= '<b>'.$this->get_name('Company',$arrtemp['company_id']).'</b><br />';
			else if(isset($arrtemp['company_name']))
				$customer .= '<b>'.$arrtemp['company_name'].'</b><br />';
			if(isset($arrtemp['contact_id']) && strlen($arrtemp['contact_id'])==24)
				$customer .= $this->get_name('Contact',$arrtemp['contact_id']).'<br />';
			else if(isset($arrtemp['contact_name']))
				$customer .= $arrtemp['contact_name'].'<br />';

			//loop 2 address
			$arradd = array('invoice','shipping');
			foreach($arradd as $vvs){
				$kk = $vvs; $customer_address = '';
				if(isset($arrtemp[$kk.'_address']) && isset($arrtemp[$kk.'_address'][0]) && count($arrtemp[$kk.'_address'])>0){
					$temp = $arrtemp[$kk.'_address'][0];
					if(isset($temp[$kk.'_address_1']) && $temp[$kk.'_address_1']!='')
						$customer_address .= $temp[$kk.'_address_1'].' ';
					if(isset($temp[$kk.'_address_2']) && $temp[$kk.'_address_2']!='')
						$customer_address .= $temp[$kk.'_address_2'].' ';
					if(isset($temp[$kk.'_address_3']) && $temp[$kk.'_address_3']!='')
						$customer_address .= $temp[$kk.'_address_3'].'<br />';
					else
						$customer_address .= '<br />';
					if(isset($temp[$kk.'_town_city']) && $temp[$kk.'_town_city']!='')
						$customer_address .= $temp[$kk.'_town_city'];

					if(isset($temp[$kk.'_province_state']))
						$customer_address .= ' '.$temp[$kk.'_province_state'].' ';
					else if(isset($temp[$kk.'_province_state_id']) && isset($temp[$kk.'_country_id'])){
						$keytemp = $temp[$kk.'_province_state_id'];
						$provkey = $this->province($temp[$kk.'_country_id']);
						if(isset($provkey[$temp]))
						$customer_address .= ' '.$provkey[$temp].' ';
					}


					if(isset($temp[$kk.'_zip_postcode']) && $temp[$kk.'_zip_postcode']!='')
						$customer_address .= $temp[$kk.'_zip_postcode'];

					if(isset($temp[$kk.'_country']) && isset($temp[$kk.'_country_id']) && (int)$temp[$kk.'_country_id']!="CA")
						$customer_address .= ' '.$temp[$kk.'_country'].'<br />';
					else
						$customer_address .= '<br />';
					$arr_address[$kk] = $customer_address;
				}
			}

			if(isset($arrtemp['name']) && $arrtemp['name']!='')
				$heading = $arrtemp['name'];
			else
				$heading = '';
			if(!isset($arr_address['invoice']))
				$arr_address['invoice'] = '';
			$this->set('customer_address',$customer.$arr_address['invoice']);
			if(!isset($arr_address['shipping']))
				$arr_address['shipping'] = '';
			$this->set('shipping_address',$arr_address['shipping']);

			// info data
			$info_data = (object) array();
			$info_data->contact_name = $arrtemp['contact_name'];
			$info_data->no = $arrtemp['code'];
			$info_data->job_no = (isset($arrtemp['job_number']) ? $arrtemp['job_number'] : '');
			$info_data->date = (isset($arrtemp['invoice_date'])&&$arrtemp['invoice_date']!='' ? $this->opm->format_date($arrtemp['invoice_date']) : '');
			$info_data->po_no = (isset($arrtemp['customer_po_no']) ? $arrtemp['customer_po_no'] : '');
			$info_data->ac_no = '';
			$info_data->terms = (isset($arrtemp['payment_terms']) ? $arrtemp['payment_terms'] : '');
			$info_data->required_date = (isset($arrtemp['payment_due_date'])&&$arrtemp['payment_due_date']!='' ? $this->opm->format_date($arrtemp['payment_due_date']) : '');
			$this->set('info_data', $info_data);


			//$this->set('quote_date',$this->opm->format_date($arrtemp['quotation_date']));
			/**Nội dung bảng giá */
			$date_now = date('Ymd');
			$filename = 'SH'.$date_now.'-'.$info_data->no;

			$this->set('filename', $filename);
			$this->set('heading',$heading);
			$html_cont = '';
			if(isset($arrtemp['products']) && is_array($arrtemp['products']) && count($arrtemp['products'])>0){
				$line = 0; $colum = 7;
				foreach($arrtemp['products'] as $keys=>$values){
					if(!$values['deleted']){
						if($line%2==0)
							$bgs = '#fdfcfa';
						else
							$bgs = '#eeeeee';
						//code
						$html_cont .= '<tr style="background-color:'.$bgs.';"><td class="first">';
							if(isset($values['code']))
								$html_cont .= '  '.$values['code'];
							else
								$html_cont .= '  #'.$keys;
						//desription
						$html_cont .= '</td><td>';
							if(isset($values['products_name']))
								$html_cont .= str_replace("\n","<br />",$values['products_name']);
							else
								$html_cont .= 'Empty';
						//quantity
						$html_cont .= '</td><td align="right">';
						$html_cont .= (isset($values['quantity']) ? $values['quantity'] : 0);
						//prev.
						$html_cont .= '</td><td align="right">';
						$html_cont .= (isset($values['prev_shipped']) ? $values['prev_shipped'] : '');
						//Now
						$html_cont .= '</td><td align="right">';
						$html_cont .= (isset($values['shipped']) ? $values['shipped'] : 0);
						//B/O
						$html_cont .= '</td><td align="right">';
						$html_cont .= (isset($values['balance_shipped']) ? $values['balance_shipped'] : 0);


						$html_cont .= '</td></tr>';
						$line++;
					}//end if deleted
				}//end for


				if($line%2==0){
					$bgs = '#fdfcfa';$bgs2 = '#eeeeee';
				}else{
					$bgs = '#eeeeee';$bgs2 = '#fdfcfa';
				}


			}//end if


			$this->set('html_cont',$html_cont);
			if(isset($arrtemp['our_csr'])){
				$this->set('user_name',' '.$arrtemp['our_csr']);
			}else
				$this->set('user_name',' '.$this->opm->user_name());
			//end set content

			//set footer
			$this->render('view_pdf');
			if($getfile)
				return $filename.'.pdf';
			$this->redirect('/upload/'.$filename.'.pdf');
		}
		die;
	}



	/*================ RFQ's ==================*/

	public function rfqs(){
		$subdatas = array();
		$subdatas['rfqs'] = $arr_temp = array();
		if($this->get_id()!=''){
			$links = "onclick=\" window.location.assign('".URL."/quotations/rfqs_entry/".$this->get_id()."/";
			$query = $this->opm->select_one(array('_id' =>new MongoId($this->get_id())));
			if(isset($query['rfqs']) && is_array($query['rfqs']) && count($query['rfqs'])>0){
				foreach($query['rfqs'] as $kss=>$vss){
					if(!$vss['deleted']){
						$arr_temp[$kss] = $vss;
						$arr_temp[$kss]['_id'] = $arr_temp[$kss]['rfqs_id'] = $kss;
						if(isset($vss['rfq_date']) && is_object($vss['rfq_date']))
						$arr_temp[$kss]['rfq_date'] = $vss['rfq_date']->sec;
						if(isset($vss['deadline_date']) && is_object($vss['deadline_date']))
							$arr_temp[$kss]['deadline_date'] = $vss['deadline_date']->sec;
						else
							$arr_temp[$kss]['deadline_date'] = '';
						$arr_temp[$kss]['set_link'] = $links.$arr_temp[$kss]['_id']."');\"";
						if(isset($arr_temp[$kss]['rfq_code'])){
							$temp = $arr_temp[$kss]['rfq_code'];
							$arr_temp[$kss]['rfq_code'] = $query['products'][$temp]['code'];
							$arr_temp[$kss]['name_details'] = $query['products'][$temp]['products_name'];
						}
					}
				}
				$subdatas['rfqs'] = $arr_temp;
			}
		}
		$this->set('subdatas', $subdatas);
	}


	/*================ Doccument ==================*/

	//address
	public function set_entry_address($arr_tmp,$arr_set){
		$address_fset = array('address_1','address_2','address_3','town_city','country','province_state','zip_postcode');
		$address_value = $address_province_id = $address_country_id = $address_province = $address_country = array();
		$address_controller = array('shipping','invoice');
		$address_value['invoice'] = $address_value['shipping']= array('','','','',"CA",'','');
		$this->set('address_controller',$address_controller);//set
		$address_key 	= array('shipping','invoice');
		$this->set('address_key',$address_key);//set
		$address_country 	= $this->country();
		foreach($address_key as $kss=>$vss){
			//neu ton tai address trong data base
			if(isset($arr_tmp[$vss.'_address'][0])){
				$arr_temp_op = $arr_tmp[$vss.'_address'][0];
				for($i=0;$i<count($address_fset);$i++){ //loop field and set value for display
					if(isset($arr_temp_op[$vss.'_'.$address_fset[$i]])){
						$address_value[$vss][$i] = $arr_temp_op[$vss.'_'.$address_fset[$i]];
					}else{
						$address_value[$vss][$i] = '';
					}
				}//pr($arr_temp_op);die;
				//get province list and country list

				if(isset($arr_temp_op[$vss.'_country_id']))
					$address_province[$vss] = $this->province($arr_temp_op[$vss.'_country_id']);
				else
					$address_province[$vss] = $this->province();
				//set province
				if(isset($arr_temp_op[$vss.'_province_state_id']) && $arr_temp_op[$vss.'_province_state_id']!='' && isset($address_province[$vss][$arr_temp_op[$vss.'_province_state_id']]) )
					$address_province_id[$kss] = $arr_temp_op[$vss.'_province_state_id'];
				else if(isset($arr_temp_op[$vss.'_province_state']))
					$address_province_id[$kss] = $arr_temp_op[$vss.'_province_state'];
				else
					$address_province_id[$kss] = '';

				//set country
				if(isset($arr_temp_op[$vss.'_country_id'])){
					$address_country_id[$kss] = $arr_temp_op[$vss.'_country_id'];
					$address_province[$vss] = $this->province($arr_temp_op[$vss.'_country_id']);
				}else{
					$address_country_id[$kss] = "CA";
					$address_province[$vss] = $this->province("CA");
				}

				$address_add[$vss] = '0';
			//chua co address trong data
			}else{
				$address_country_id[$kss] = "CA";
				$address_province[$vss] = $this->province("CA");
				$address_add[$vss] = '1';
			}
		}
		//pr($address_province);
		$this->set('address_value',$address_value);
		$address_hidden_field = array('shipping_address','invoice_address');
		$this->set('address_hidden_field',$address_hidden_field);//set
		$address_label[1] = $arr_set['field']['panel_2']['invoice_address']['name'];
		$address_label[0] = $arr_set['field']['panel_2']['shipping_address']['name'];
		$this->set('address_label',$address_label);//set
		$address_conner[0]['top'] 		= 'hgt fixbor';
		$address_conner[0]['bottom'] 	= 'fixbor2 jt_ppbot';
		$address_conner[1]['top'] 		= 'hgt';
		$address_conner[1]['bottom'] 	= 'fixbor3 jt_ppbot';
		$this->set('address_conner',$address_conner);//set
		$this->set('address_country',$address_country);//set
		$this->set('address_country_id',$address_country_id);//set
		$this->set('address_province',$address_province);//set
		$this->set('address_province_id',$address_province_id);//set
		$this->set('address_more_line',2);//set
		$this->set('address_onchange',"save_address_pr('\"+keys+\"');");
		if(isset($arr_tmp['company_id']) && strlen($arr_tmp['company_id'])==24)
		$this->set('address_company_id','company_id');
		if(isset($arr_tmp['contact_id']) && strlen($arr_tmp['contact_id'])==24)
		$this->set('address_contact_id','contact_id');
		$this->set('address_add',$address_add);
	}


	// Popup form orther module
    public function popup($key = '') {
        $this->set('key', $key);
        $limit = 100; $skip = 0; $cond = array();
        // Nếu là search GET
        if (!empty($_GET)) {
            $tmp = $this->data;
            if (isset($_GET['company_id'])) {
                $cond['company_id'] = new MongoId($_GET['company_id']);
                $tmp['Quotation']['company'] = $_GET['company_name'];
            }
            if (isset($_GET['is_customer'])) {
                $cond['is_customer'] = 1;
                $tmp['Quotation']['is_customer'] = 1;
            }
            if (isset($_GET['is_employee'])) {
                $cond['is_employee'] = 1;
                $tmp['Quotation']['is_employee'] = 1;
            }
            $this->data = $tmp;
        }

        // Nếu là search theo phân trang
        $page_num = 1;
        if( isset($_POST['pagination']) && $_POST['pagination']['page-num'] > 0 ){

            // $limit = $_POST['pagination']['page-list'];
            $page_num = $_POST['pagination']['page-num'];
            $limit = $_POST['pagination']['page-list'];
            $skip = $limit*($page_num - 1);
        }
        $this->set('page_num', $page_num);
        $this->set('limit', $limit);
        $arr_order = array('first_name' => 1);
        if( isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0 ){
            $sort_type = 1;
            if( $_POST['sort']['type'] == 'desc' ){
                $sort_type = -1;
            }
            $arr_order = array($_POST['sort']['field'] => $sort_type);

            $this->set('sort_field', $_POST['sort']['field']);
            $this->set('sort_type', ($sort_type === 1)?'asc':'desc');
            $this->set('sort_type_change', ($sort_type === 1)?'desc':'asc');
        }

        // search theo submit $_POST kèm điều kiện
        if (!empty($this->data) && !empty($_POST) && isset($this->data['Quotation']) ) {
            $arr_post = $this->data['Quotation'];

            if (isset($arr_post['name']) && strlen($arr_post['name']) > 0) {
                $cond['full_name'] = new MongoRegex('/' . trim($arr_post['name']) . '/i');
            }

            if (strlen($arr_post['company']) > 0) {
                $cond['company'] = new MongoRegex('/' . $arr_post['company'] . '/i');
            }

            if( $arr_post['inactive'] )
                $cond['inactive'] = 1;

            if (is_numeric($arr_post['is_customer']) && $arr_post['is_customer'])
                $cond['is_customer'] = 1;

            if (is_numeric($arr_post['is_employee']) && $arr_post['is_employee'])
                $cond['is_employee'] = 1;
        }

        $this->selectModel('Quotation');
        $arr_quotation = $this->Quotation->select_all(array(
            'arr_where' => $cond,
            'arr_order' => $arr_order,
            'limit' => $limit,
            'skip' => $skip
                // 'arr_field' => array('name', 'is_customer', 'is_employee', 'company_id', 'company_name')
        ));
        $this->set('arr_quotation', $arr_quotation);

        $total_page = $total_record = $total_current = 0;
        if( is_object($arr_quotation) ){
            $total_current = $arr_quotation->count(true);
            $total_record = $arr_quotation->count();
            if( $total_record%$limit != 0 ){
                $total_page = floor($total_record/$limit) + 1;
            }else{
                $total_page = $total_record/$limit;
            }
        }
        $this->set('total_current', $total_current);
        $this->set('total_page', $total_page);
        $this->set('total_record', $total_record);

        $this->layout = 'ajax';
    }

	public function create_shipping_from_salesorder($salesorder_id){
		$this->selectModel('Salesorder');

		if($salesorder_id!=''){
			$arr_salesorder = $this->Salesorder->select_one(array('_id' => new MongoId($salesorder_id)));
			$arr_shipping = $this->opm->select_one(array('salesorder_id' => new MongoId($salesorder_id)));
		}

		$arr_save=array();

		if(isset($arr_salesorder['company_id']))
			$arr_save = $this->arr_associated_data('company_name',$arr_salesorder['company_name'], $arr_salesorder['company_id']);

		$arr_save['shipping_type']='Out';
		$arr_save['shipping_status']='In progress';
		$arr_save['shipping_date']=new MongoDate(time());
		$arr_save['salesorder_id']=isset($salesorder_id)?new MongoId($salesorder_id):'';
		$arr_save['salesorder_number']=isset($arr_salesorder['code'])?$arr_salesorder['code']:'';
		$arr_save['salesorder_name']=(isset($arr_salesorder['name'])?$arr_salesorder['name']:'');
		$arr_save['shipper'] = (isset($arr_salesorder['shipper']) ? $arr_salesorder['shipper'] : '');
		$arr_save['shipper_id'] = (isset($arr_salesorder['shipper_id']) ? $arr_salesorder['shipper_id'] : '');

		if(isset($arr_salesorder['our_rep']) && isset($arr_salesorder['our_rep_id']) && $arr_salesorder['our_rep_id']!=''){
			$arr_save['our_rep_id'] = $arr_salesorder['our_rep_id'];
			$arr_save['our_rep'] = $arr_salesorder['our_rep'];
		}else{
			$arr_save['our_rep_id'] = $this->opm->user_id();
			$arr_save['our_rep'] = $this->opm->user_name();
		}

		if(isset($arr_salesorder['our_csr']) && isset($arr_salesorder['our_csr_id']) && $arr_salesorder['our_csr_id']!=''){
			$arr_save['our_csr_id'] = $arr_salesorder['our_csr_id'];
			$arr_save['our_csr'] = $arr_salesorder['our_csr'];
		}else{
			$arr_save['our_csr_id'] = $this->opm->user_id();
			$arr_save['our_csr'] = $this->opm->user_name();
		}

		if(isset($arr_salesorder['shipping_address'])&&is_array($arr_salesorder['shipping_address'])){



			if(isset($arr_save['addresses_default_key']))
				$v_default=$arr_save['addresses_default_key'];
			else
				$v_default=0;

			if((isset($arr_salesorder['shipping_address'][0]['shipping_address_1'])&&$arr_salesorder['shipping_address'][0]['shipping_address_1']!='')
			||(isset($arr_salesorder['shipping_address'][0]['shipping_address_2'])&&$arr_salesorder['shipping_address'][0]['shipping_address_2']!='')
			||(isset($arr_salesorder['shipping_address'][0]['shipping_address_3'])&&$arr_salesorder['shipping_address'][0]['shipping_address_3']!='')
			||(isset($arr_salesorder['shipping_address'][0]['shipping_town_city'])&&$arr_salesorder['shipping_address'][0]['shipping_town_city']!='')
			||(isset($arr_salesorder['shipping_address'][0]['shipping_province_state'])&&$arr_salesorder['shipping_address'][0]['shipping_province_state']!='')
			||(isset($arr_salesorder['shipping_address'][0]['shipping_province_state_id'])&&$arr_salesorder['shipping_address'][0]['shipping_province_state_id']!='')
			||(isset($arr_salesorder['shipping_address'][0]['shipping_zip_postcode'])&&$arr_salesorder['shipping_address'][0]['shipping_zip_postcode']!='')
			)
			{

				$arr_save['shipping_address']=$arr_salesorder['shipping_address'];
			}
			else
			{
				if(is_array($arr_save['invoice_address'])){

					foreach($arr_save['invoice_address'][$v_default] as $ka=>$va){
						if($ka!='deleted')
						{
							$ka1=substr($ka, 8);
							$arr_save['shipping_address'][0]['shipping_'.$ka1] = $va;
						}
						else
						{
							$arr_save['shipping_address'][0][$ka] = $va;
						}

					}
				}
			}



//			pr($arr_save['shipping_address']);
//			die;

		}





		//Products-------------------------------------------------------------
		$arr_save_temp=array();
		$arr_save['products']=is_array($arr_salesorder['products'])?$arr_salesorder['products']:array();
		if(is_array($arr_save['products'])){
			foreach($arr_save['products'] as $key=>$value)
			{
				$arr_save['products'][$key]['shipped']=(int)$value['quantity'];
				$arr_save['products'][$key]['balance_shipped']=0;
			}
			foreach($arr_salesorder['products'] as $key=>$value)
			{
				$arr_salesorder['products'][$key]['shipped']=(int)$value['quantity'];
				$arr_salesorder['products'][$key]['balance_shipped']=0;
			}
			$this->Salesorder->save($arr_salesorder);
		}
		//---------------------------------------------------------------------



		$arr_save['code'] =$this->opm->get_auto_code('code');

		if ($this->opm->save($arr_save)) {
			$this->redirect('/shippings/entry/'. $this->opm->mongo_id_after_save);
		}
		$this->redirect('/shippings/entry');
	}
    // $id = id quotation;
    public function create_sale_invoice($id = ''){
    	$id = new MongoId($id);
    	$this->selectModel('Salesorder');
    	// defind
    	$data = (object) array();
     	// get Salesorder by id
    	$sales_order = $this->Salesorder->select_one(array('_id' => $id));
    	// chuyen Salesorder sang object
    	$sales_order = (object) $sales_order;
    	// check Salesorder exiting
    	if($sales_order){
			$data->code              = $this->opm->get_auto_code('code');
			$data->invoice_type      = 'Invoice';
			$data->company_name      = $sales_order->company_name;
			$data->company_id        = $sales_order->company_id;
			$data->contact_name      = $sales_order->contact_name;
			$data->contact_id        = $sales_order->contact_id;
			$data->customer_po_no    = $sales_order->customer_po_no;
			$data->description       = $sales_order->description;
			$data->email             = $sales_order->email;
			$data->invoice_address   = $sales_order->invoice_address;
			$data->job_id            = $sales_order->job_id;
			$data->job_name          = $sales_order->job_name;
			$data->job_number        = $sales_order->job_number;
			$data->name              = $sales_order->name;
			$data->our_csr           = $sales_order->our_csr;
			$data->our_csr_id        = $sales_order->our_csr_id;
			$data->our_rep           = $sales_order->our_rep;
			$data->our_rep_id        = $sales_order->our_rep_id;
			$data->payment_due_date  = '&nbsp;';
			$data->payment_terms     = $sales_order->payment_terms;
			$data->phone             = $sales_order->phone;
			$data->products          = $sales_order->products;
			$data->salesorder_id     = $sales_order->_id;
			$data->salesorder_name   = $sales_order->name;
			$data->salesorder_number = $sales_order->code;
			$data->invoice_date      = new MongoDate();
			$data->shipping_address  = $sales_order->shipping_address;
			$data->invoice_status	 = 'Invoiced';
			$data->paid_date         = '&nbsp;';
			$data->sum_amount        = $sales_order->sum_amount;
			$data->sum_sub_total     = $sales_order->sum_sub_total;
			$data->sum_tax           = $sales_order->sum_tax;
			$data->tax               = $sales_order->tax;
			$data->taxval            = $sales_order->taxval;

    		// convert $data object to array
    		$data = (array) $data;
    		$this->selectModel('Salesinvoice');
    		// save sale invoice success
    		if($this->Salesinvoice->save($data)){
    			// return id sale invoice after save;
    			$return_id = $this->Salesinvoice->mongo_id_after_save;
    			$this->redirect('entry/'.$return_id);
    		}else{
    			echo 'Error: ' . $this->Salesinvoice->arr_errors_save[1];
    		}
    	}else{
    		die();
    	}
    }

	function general_auto_save($id) {
		$arr_save=array();
		if (!empty($_POST)) {
			$arr_save['_id'] = new MongoId($id);
			if(isset($_POST['data']))
				$arr_save['other_comment'] = $_POST['data'];
			if(isset($_POST['content']))
				$arr_save['web_tracker'] = $_POST['content'];
			$error = 0;
			$this->selectModel('Shipping');
			if (!$error) {
				if ($this->Shipping->save($arr_save)) {
					echo 'ok';
				} else {
					echo 'Error: ' . $this->Shipping->arr_errors_save[1];
				}
			}
		}
		die;
	}
	public function set_cal_price() {
		$this->cal_price = new cal_price; //Option cal_price
		//set arr_price_break default
		$this->cal_price->arr_price_break = array();
		//set arr_product default
		$this->cal_price->arr_product = array();
		//set arr_product item default
		$this->cal_price->arr_product_items = array();
	}
	public function ajax_cal_line() {
		$this->set_cal_price();
		$arr_ret = $arr_product_items = array();
		if (isset($_POST['arr'])) {
			$getdata = $_POST['arr'];
			$getdata = (array) $getdata;
			//truong hop co id
			if (isset($getdata['id'])) {
				$get_id = $getdata['id'];
				$qr = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
				$query = $qr;
				if (isset($query['products']))
					$arr_pro = $arr_insert['products'] = (array) $query['products'];
				if (is_array($arr_pro) && count($arr_pro) > 0 && isset($arr_pro[$get_id]) && !$arr_pro[$get_id]['deleted']) {
					$arr_pro = array_merge((array) $arr_pro[$get_id], (array) $getdata);
					$this->cal_price->arr_product_items = $arr_pro;
					$arr_ret = $this->cal_price->cal_price_items();
					//Save all data
					$arr_insert['_id'] = new MongoId($this->get_id());
					$arr_insert['products'][$get_id] = array_merge((array) $arr_pro, (array) $arr_ret);
					$qty = (isset($arr_insert['products'][$get_id]['quantity'])&&$arr_insert['products'][$get_id]['quantity']!='' ? $arr_insert['products'][$get_id]['quantity'] : 0);
					$now = (isset($arr_insert['products'][$get_id]['shipped'])&&$arr_insert['products'][$get_id]['shipped']!='' ? $arr_insert['products'][$get_id]['shipped'] : 0);
					$prev = (isset($arr_insert['products'][$get_id]['prev_shipped'])&&$arr_insert['products'][$get_id]['prev_shipped']!='' ? $arr_insert['products'][$get_id]['prev_shipped'] : 0);
					$arr_insert['products'][$get_id]['balance_shipped'] = ($qty!=0 ? $qty - $prev - $now : '');
					$this->opm->save($arr_insert);
					//update sum
					$keyfield = array(
						"sub_total" => "sub_total",
						"tax" => "tax",
						"amount" => "amount",
						"sum_sub_total" => "sum_sub_total",
						"sum_tax" => "sum_tax",
						"sum_amount" => "sum_amount"
					);
					$arr_sum = $this->update_sum('products', $keyfield);
					$arr_ret = array_merge((array) $arr_ret, (array) $arr_sum);
					//Return data for display
					echo json_encode($arr_ret);
				}

				//truong hop khong chon id nao
			} else {

				echo '';
			}
		}
		die;
	}
	public function tracking(){
		$subdatas = array();
		$subdatas['tracking_detail'] = array();
		$subdatas['web_tracker'] = array();


		$this->selectModel('Shipping');
		$arr_shipping = $this->Shipping->select_one(array('_id' => new MongoId($this->get_id())));

		$subdatas['tracking_detail']=$arr_shipping;
		$subdatas['web_tracker']=$arr_shipping;

		$this->selectModel('Setting');
		$arr_setting=$this->Setting->select_one(array('setting_value' => 'shipping_method'));


		$arr_options_custom = $this->set_select_data_list('relationship', 'tracking');


		$this->set('arr_options_custom', $arr_options_custom);

		$this->selectModel('Company');
		$arr_company_shipper=array();
		if(isset($arr_shipping['shipper_id'])&&$arr_shipping['shipper_id']!=null)
			$arr_company_shipper = $this->Company->select_one(array('_id' => new MongoId($arr_shipping['shipper_id'])));

		if(isset($arr_company_shipper['tracking_url'])&&$arr_company_shipper['tracking_url']!='')
			$this->set('tracking_url_iframe',$arr_company_shipper['tracking_url']);

		$this->set('_id', $this->get_id());
		$this->set('subdatas', $subdatas);
		//$this->set('arr_shipping', $arr_shipping);
	}

	//An other
	public function other(){
		$subdatas = array();
		$id=$this->get_id();
		$this->selectModel('Shipping');
		if(isset($id)){
			$arr_shipping = $this->Shipping->select_one(array('_id' => new MongoId($id)));
		}
        $subdatas['other_pricing'] = array($arr_shipping);
        $subdatas['other_comment']= array('other_comment'=>isset($arr_shipping['other_comment'])?$arr_shipping['other_comment']:'');
        $this->set('subdatas', $subdatas);
		$this->selectModel('Shipping');
		$arr_options_custom = $this->set_select_data_list('relationship', 'other');
		$this->set('arr_options_custom', $arr_options_custom);
		$this->communications($id, true);
	}

	/*
        Tung Report
    */
    public function report_pdf($data) {

        App::import('Vendor', 'xtcpdf');
        $pdf = new XTCPDF();
        $textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Anvy Digital');
        $pdf->SetTitle('Anvy Digital Quotation');
        $pdf->SetSubject('Quotation');
        $pdf->SetKeywords('Quotation, PDF');

        // set default header data
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(2);

        // set margins
        $pdf->SetMargins(10, 3, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------
        // set font
        $pdf->SetFont($textfont, '', 9);

        // add a page
        $pdf->AddPage();


        // writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
        // writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
        // create some HTML content


        $html = '
        <table cellpadding="2" cellspacing="0" style="width:100%; margin: 0px auto">
           <tbody>
              <tr>
                 <td width="32%" valign="top" style="color:#1f1f1f;">
                    <img src="img/logo_anvy.jpg" alt="" margin-bottom:0px>
                    <p style="margin-bottom:5px; margin-top:0px;">Unit 103, 3016 - 10th Ave NE<br/ >Calgary  AB  T2A  6A3</p>
                 </td>
                 <td width="68%" valign="top" align="right">
                    <table>
                       <tbody>
                          <tr>
                             <td width="10%">&nbsp;</td>
                             <td width="90%">
                                <span style="text-align:right; font-size:21px; font-weight:bold; color: #919295;">
                                    ' . $data['title'] . '<br />';
        if (isset($data['date_equals']))
            $date = '<span style="font-size:12px; font-weight:normal">' . $data['date_equals'] . '</span>';
        else {
            if (isset($data['date_from']) && isset($data['date_to']))
                $date = '<span style="font-size:12px; font-weight:normal">( ' . $data['date_from'] . ' - ' . $data['date_to'] . ' )</span>';
            else if (isset($data['date_from']))
                $date = '<span style="font-size:12px; font-weight:normal">From ' . $data['date_from'] . '</span>';
            else if (isset($data['date_to']))
                $date = '<span style="font-size:12px; font-weight:normal">To ' . $data['date_to'] . '</span>';
            else
                $date = '';
        }
        $html .= $date;
        $html .= '
                                </span>
                                <div style=" border-bottom: 1px solid #cbcbcb;height:5px">&nbsp;</div>
                             </td>
                          </tr>
                          <tr>
                             <td colspan="2">
                                    <span style="font-weight:bold;">Printed at: </span>' . $data['current_time'] . '
                             </td>
                          </tr>
                       </tbody>
                    </table>
                 </td>
              </tr>
           </tbody>
        </table>
        <div class="option">' . @$data['heading'] . '</div>
        <br />
        <br />
        <div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>
        <br />
        <style>
           td{
           line-height:2px;
           }
           td.first{
            text-align: center;
           border-left:1px solid #e5e4e3;
           }
           td.end{
           border-right:1px solid #e5e4e3;
           }
           td.top{
           color:#fff;
           text-align: center;
           font-weight:bold;
           background-color:#911b12;
           border-top:1px solid #e5e4e3;
           }
           td.bottom{
           border-bottom:1px solid #e5e4e3;
           }
           td.content{
            border-right: 1px solid #E5E4E3;
            text-align: center;
           }
           .option{
           color: #3d3d3d;
           font-weight:bold;
           font-size:20px;
           text-align: center;
           width:100%;
           }
           table.maintb{
           }
        </style>
        <br />
        ';
        $html .= $data['html_loop'];

        $pdf->writeHTML($html, true, false, true, false, '');



        // reset pointer to the last page
        $pdf->lastPage();



        // ---------------------------------------------------------
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        //$pdf->Output('example_001.pdf', 'I');




        $pdf->Output('upload/' . $data['filename'] . '.pdf', 'F');
    }
    public function option_summary_customer_find()
    {
        $arr_data['shippings_type'] = $this->Setting->select_option_vl(array('setting_value'=>'shipping_types'));
        $arr_data['shippings_status'] = $this->Setting->select_option_vl(array('setting_value'=>'shipping_statuses'));

        $this->set('arr_data',$arr_data);
    }
    public function option_detailed_customer_find()
    {
        $arr_data['shippings_type'] = $this->Setting->select_option_vl(array('setting_value'=>'shipping_types'));
        $arr_data['shippings_status'] = $this->Setting->select_option_vl(array('setting_value'=>'shipping_statuses'));
        $this->set('arr_data',$arr_data);
    }
    public function check_exist_customer()
	{
		$data = $_POST;
		//Dùng ajax gọi hàm này, $_POST là serialize của form
		//parse_str thành mảng
		parse_str($data['data'],$data);
		if(isset($data)&&$data!='')
		{
			$data = $this->Common->strip_search($data);
			$arr_where = array();
			$arr_where['deleted'] = false;
			if(@$data['type']!='')
				$arr_where['shipping_type'] = @$data['type'];
			if(@$data['status'])
				$arr_where['shipping_status'] = $data['status'];
			//Check loại trừ cancel thì bỏ các status bên dưới
			if(@$data['is_not_cancel']==1)
			{
				$arr_where['shipping_status'] = array('$nin'=>array('Delivered','Cancelled'));
				//Tuy nhiên nếu ở ngoài combobox nếu có chọn, thì ưu tiên nó, set status lại
				if(@$data['status']!='')
				{
					$arr_where['shipping_status'] = '';
					$arr_where['shipping_status'] = $data['status'];
				}

			}
			$arr_where['return_status'] = 0;
			if(@$data['is_return']==1)
				$arr_where['return_status'] = 1;
			//Có hai trường hợp, hoặc là receiver (type là in) hoặc là sender (type là out)
			if(@$data['company_receiver']!=''|| @$data['contact_receiver']!='')
			{
				if(@$data['company_receiver']!='')
					$arr_where['company_name'] = new MongoRegex('/'.trim($data['company_receiver']).'/i');
				if(@$data['contact_receiver']!='')
					$arr_where['contact_name'] = new MongoRegex('/'.trim($data['contact_receiver']).'/i');
				if(@$data['status']=='')
					$arr_where['shipping_type'] = 'In';
			}
			else if(@$data['company_sender']!='' || @$data['contact_sender'] != '')
			{
				if(@$data['company_sender']!='')
					$arr_where['company_name'] = new MongoRegex('/'.trim($data['company_sender']).'/i');
				if(@$data['contact_sender']!='')
					$arr_where['contact_name'] = new MongoRegex('/'.trim($data['contact_sender']).'/i');
				if(@$data['status']=='')
					$arr_where['shipping_type'] = 'Out';
			}
			if(@$data['carrier']!='')
				$arr_where['carrier_name'] = new MongoRegex('/'.trim($data['carrier']).'/i');
			if(@$data['job_no']!='')
				$arr_where['job_number'] = new MongoRegex('/'.trim($data['job_no']).'/i');
			//Tìm chính xác ngày
			//Vì để = chỉ tìm đc 01/01/1969 00:00:00 nên phải cộng cho 23:59:59 rồi tìm trong khoảng đó
			if(@$data['date_equals']!='')
			{
				$date_equals = $data['date_equals'];
				$date_equals = new MongoDate(strtotime(@date('Y-m-d',strtotime($date_equals))));
				$date_equals_to = new MongoDate($date_equals->sec + 24*3600 -1);
				$arr_where['shipping_date']['$gte'] = $date_equals;
				$arr_where['shipping_date']['$lt'] = $date_equals_to;
			}
			//Ngày nằm trong khoảng
			else if(@$data['date_equals'] == '')
			{
				//neu chi nhap date from
				if($date_from = $data['date_from'])
				{
					$date_from = new MongoDate(strtotime(@date('Y-m-d',strtotime(@$date_from))));
					$arr_where['shipping_date']['$gte'] = $date_from;
				}
				//neu chi nhap date to
				if($date_to = $data['date_to'])
				{
					$date_to = new MongoDate(strtotime(@date('Y-m-d',strtotime(@$date_to))));
					$date_to = new MongoDate($date_to->sec + 24*3600 -1);
					$arr_where['shipping_date']['$lte'] = $date_to;
				}
			}
			if(@$data['our_rep']!='')
				$arr_where['our_rep'] = new MongoRegex('/'.$data['our_rep'].'/i');
			if(@$data['our_csr']!='')
				$arr_where['our_csr'] = new MongoRegex('/'.$data['our_csr'].'/i');
			$this->selectModel('Shipping');
			//lay het shipping, voi where nhu tren va lay sum_amount giam dan
			$shipping = $this->Shipping->select_all(array(
					'arr_where'=>$arr_where,
					'arr_field'=>array('_id','code','company_name','company_id','contact_name','contact_id','shipping_type','carrier_name','shipping_date','shipping_status','tracking_no')
				));
			if($shipping->count()==0)
			{
				echo 'empty';
				die;
			}
			else
			{
				$url = '';
				if($data['report_type']=='summary')
					$url = $this->summary_customer_report_pdf($shipping,$data);
				else if($data['report_type'] == 'detailed')
					$url = $this->detailed_customer_report_pdf($shipping,$data);
				else
					$url = $this->summary_customer_report_pdf($shipping,$data);

				echo URL.$url;
				die;
			}
		}

	}
	public function summary_customer_report_pdf($shipping,$data)
	{

		//--------------------------------------
		$sum = 0;
		$html_loop = '';
		$i = 0;
		$color = '';
		$group = array();
		$this->selectModel('Company');
		$this->selectModel('Contact');
		//Có 2 trường hợp là công ty hoặc cá nhân,
		//nên phải lọc theo _id tương ứng, ưu tiên company nếu có cả 2
		foreach($shipping as $value)
		{

			if($value['company_id']!=''&&isset($value['company_name'])&&$value['company_name']!='')
			{
				$company = $this->Company->select_one(array('_id'=> new MongoId($value['company_id'])
															),array('email','phone'));
				$group[(string)$value['company_id']]['company_name'] = $value['company_name'];
				$group[(string)$value['company_id']]['contact_name'] = $value['contact_name'];
				$group[(string)$value['company_id']]['phone'] = @$company['phone'];
				$group[(string)$value['company_id']]['email'] = @$company['email'];
				if(!isset($group[(string)$value['company_id']]['number_of_shipment']))
					$group[(string)$value['company_id']]['number_of_shipment'] = 0;
				$group[(string)$value['company_id']]['number_of_shipment']++;
			}
			else if(!isset($value['company_name'])||$value['company_name']=='')
			{
				$contact = $this->Company->select_one(array('arr_where'=>array('_id'=> new MongoId($value['contact_id'])),
															'arr_field'=>array('direct_dial','email','mobile')
															));
				$group[(string)$value['contact_id']]['company_name'] = '';
				$group[(string)$value['contact_id']]['contact_name'] = $value['contact_name'];
				$group[(string)$value['contact_id']]['phone'] = (isset($contact['direct_dial']) ? $contact['direct_dial']
																				: (isset($contact['mobile']) ? $contact['mobile'] : ''));
				$group[(string)$value['contact_id']]['email'] = $contact['email'];
				if(!isset($group[(string)$value['contact_id']]['number_of_shipment']))
					$group[(string)$value['contact_id']]['number_of_shipment'] = 0;
				$group[(string)$value['contact_id']]['number_of_shipment']++;
			}

		}
		$html_loop = '
				<table cellpadding="3" cellspacing="0" class="maintb">
			      <tr>
			         <td width="30%" class="first top">
			            Company
			         </td>
			         <td width="20%" class="top">
			           	Contact
			         </td>
			         <td width="15%" class="top">
			            Phone
			         </td>
			         <td width="20%" class="top">
			            Email
			         </td>
			        <td width="15%" class="end top">
			           No. of SH
			         </td>
			      </tr>
			';
		foreach($group as $value)
		{
			$color = '#fdfcfa';
			if($i%2==0)
				$color = '#eeeeee';
			$html_loop .= '
					<tr style="background-color:'.$color.';">
				         <td class="first content" align="left">'.@$value['company_name'].'</td>
				         <td class="content" align="left">'.@$value['contact_name'].'</td>
				         <td class="content" align="left">'.@$value['phone'].'</td>
				         <td class="content" align="left">'.@$value['email'].'</td>
				         <td class="content">'.@$value['number_of_shipment'].'</td>
			      	</tr>
				';

			$i++;
		}
		$color = '#fdfcfa';
		if($i%2==0)
			$color = '#eeeeee';
		$html_loop .= '
					<tr style="background-color:'.$color.';">
				         <td align="left" class="first bottom">'.$i.' record(s) listed</td>
				         <td class="bottom">&nbsp;</td>
				         <td class="bottom">&nbsp;</td>
				         <td class="bottom">&nbsp;</td>
				         <td class="bottom">&nbsp;</td>
	  				</tr>
				</table>
  				';
  		//========================================
  		$pdf['current_time'] = date('h:i a m/d/Y');
  		$pdf['title'] = '<span style="color:#b32017">S</span>hipment <span style="color:#b32017">R</span>eport <span style="color:#b32017">B</span>y <span style="color:#b32017">C</span>ustomer<br /> (Summary)';
		$this->layout = 'pdf';
			//set header
		$pdf['logo_link'] = 'img/logo_anvy.jpg';
		$pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
		$pdf['heading'] = $data['heading'];
		if(isset($data['date_equals'])&&$data['date_equals']!='')
		{
			$pdf['date_equals'] = $data['date_equals'];
		}
		else
		{
			if(isset($data['date_from'])&&$data['date_from']!='')
				$pdf['date_from']  = $data['date_from'];
			if(isset($data['date_to'])&&$data['date_to']!='')
				$pdf['date_to'] = $data['date_to'];
		}
		$pdf['html_loop'] = $html_loop;
		$pdf['filename'] = 'SH_'.md5($pdf['current_time']);
		//pr($html_loop);die;
		$this->report_pdf($pdf);
		return '/upload/'.$pdf['filename'].'.pdf';
	}
	public function detailed_customer_report_pdf($shipping, $data) {


        $html_loop = '';
        $i = 0;
        $color = '';
        $this->selectModel('Company');
        $this->selectModel('Contact');
        //Có 2 trường hợp là công ty hoặc cá nhân,
		//nên phải lọc theo _id tương ứng, ưu tiên company nếu có cả 2
		//vì shipping chỉ lưu first name, nên phải dò từ Contact
        foreach ($shipping as $value) {
            {
            	if($value['company_id']!=''&&isset($value['company_name'])&&$value['company_name']!='')
            	{
	                $company = $this->Company->select_one(array('_id'=> new MongoId($value['company_id'])
																),array('email','phone'));
	                $group[(string)$value['company_id']]['contact_name']='';
	                if(isset($value['contact_id'])&&$value['contact_id']!='')
	                {
	                	$contact = $this->Contact->select_one(array('_id'=> new MongoId($value['contact_id'])),
															array('full_name')
															);

	                	$group[(string)$value['company_id']]['contact_name'] = $contact['full_name'];
	                }
					$group[(string)$value['company_id']]['company_name'] = $value['company_name'];
					$group[(string)$value['company_id']]['phone'] = @$company['phone'];
					$group[(string)$value['company_id']]['email'] = @$company['email'];
					if(!isset($group[(string)$value['company_id']]['number_of_shipment']))
						$group[(string)$value['company_id']]['number_of_shipment'] = 0;
					$group[(string)$value['company_id']]['number_of_shipment']++;
	                //Tach theo shipping code
	                $group[(string) $value['company_id']]['shipping'][$value['code']]['shipping_code'] = $value['code'];
	                $group[(string) $value['company_id']]['shipping'][$value['code']]['shipping_type'] = $value['shipping_type'];
	                $group[(string) $value['company_id']]['shipping'][$value['code']]['shipping_date'] = $this->opm->format_date($value['shipping_date']);
	                $group[(string) $value['company_id']]['shipping'][$value['code']]['shipping_status'] = $value['shipping_status'];
	                $group[(string) $value['company_id']]['shipping'][$value['code']]['shipping_carrier'] = $value['carrier_name'];
	                $group[(string) $value['company_id']]['shipping'][$value['code']]['shipping_tracking_no'] = $value['tracking_no'];
            	}
            	else if(isset($value['contact_name'])&&$value['contact_name']!='')
				{
					$contact = $this->Contact->select_one(array('_id'=> new MongoId($value['contact_id']),
															'arr_field'=>array('full_name','direct_dial','email','mobile')
															));
					$group[(string)$value['contact_id']]['company_name'] = '';
					$group[(string)$value['contact_id']]['contact_name'] = $contact['full_name'];
					$group[(string)$value['contact_id']]['phone'] = (isset($contact['direct_dial']) ? $contact['direct_dial']
																					: (isset($contact['mobile']) ? $contact['mobile'] : ''));
					$group[(string)$value['contact_id']]['email'] = @$contact['email'];
					if(!isset($group[(string)$value['contact_id']]['number_of_shipment']))
						$group[(string)$value['contact_id']]['number_of_shipment'] = 0;
					$group[(string)$value['contact_id']]['number_of_shipment']++;
	                //Tach theo shipping code
	                $group[(string) $value['contact_id']]['shipping'][$value['code']]['shipping_code'] = $value['code'];
	                $group[(string) $value['contact_id']]['shipping'][$value['code']]['shipping_type'] = $value['shipping_type'];
	                $group[(string) $value['contact_id']]['shipping'][$value['code']]['shipping_date'] = $this->opm->format_date($value['shipping_date']);
	                $group[(string) $value['contact_id']]['shipping'][$value['code']]['shipping_status'] = $value['shipping_status'];
	                $group[(string) $value['contact_id']]['shipping'][$value['code']]['shipping_carrier'] = $value['carrier_name'];
	                $group[(string) $value['contact_id']]['shipping'][$value['code']]['shipping_tracking_no'] = @$value['tracking_no'];
				}
            }
        }
        //pr($group);die;
        foreach ($group as $key => $shipping) {
            $html_loop .= '
            <table cellpadding="3" cellspacing="0" class="maintb">
               <tbody>
                  <tr>
			         <td width="30%" class="first top">
			            Company
			         </td>
			         <td width="20%" class="top">
			           	Contact
			         </td>
			         <td width="15%" class="top">
			            Phone
			         </td>
			         <td width="20%" class="top">
			            Email
			         </td>
			        <td width="15%" class="end top">
			           No. of SH
			         </td>
			      </tr>
                  <tr style="background-color:#eeeeee;">
			         <td class="first content" align="left">'.@$shipping['company_name'].'</td>
			         <td class="content" align="left">'.@$shipping['contact_name'].'</td>
			         <td class="content" align="left">'.@$shipping['phone'].'</td>
			         <td class="content" align="left">'.@$shipping['email'].'</td>
			         <td class="content">'.@$shipping['number_of_shipment'].'</td>
		      	  </tr>
               </tbody>
            </table>
            <div class="option"></div><br />';
            $html_loop .= '<table cellpadding="3" cellspacing="0" class="maintb">
                        <tbody>
                          <tr>
                             <td width="10%" class="first top">
                                SH#
                             </td>
                             <td width="25%" class="top">
                                Type
                             </td>
                             <td width="15%" class="top">
                                Date
                             </td>
                             <td width="20%" class="top">
                                Carrier
                             </td>
                             <td width="15%" class="top">
                                Tracking No.
                             </td>
                             <td width="15%" class="top">
                                No. of SH.
                             </td>
                          </tr>';

            $i = 0;
            if (is_array($shipping)) {
                foreach ($shipping['shipping'] as $key => $value) {
                    $color = '#fdfcfa';
                    if ($i % 2 == 0)
                        $color = '#eeeeee';
                    $html_loop .= '
                          <tr style="background-color:' . $color . ';">
                             <td class="first content">' . $value['shipping_code'] . '</td>
                             <td class="content">' . $value['shipping_type'] . '</td>
                             <td class="content">' . $value['shipping_date'] . '</td>
                             <td class="content">' . $value['shipping_carrier'] . '</td>
                             <td class="content">' . $value['shipping_tracking_no'] . '</td>
                             <td class="content"  align="right" class="end">1</td>
                          </tr>';
                    $i++;
                }
            }
            $color = '#fdfcfa';
            if ($i % 2 == 0)
                $color = '#eeeeee';
            $html_loop .= '
                            <tr style="background-color:' . $color . '">
                             <td colspan="6" align="left" class="first bottom">' . $i . ' record(s) listed</td>
                          </tr>
                        </tbody>
                    </table>
                    <br />
                    <div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>
                    <br />';
        }
        //========================================
        $pdf['current_time'] = date('h:i a m/d/Y');
        $pdf['title'] = '<span style="color:#b32017">S</span>hipping <span style="color:#b32017">R</span>eport <span style="color:#b32017">B</span>y <span style="color:#b32017">C</span>ustomer<br /> (Detail)';
        $this->layout = 'pdf';
        //set header
        $pdf['logo_link'] = 'img/logo_anvy.jpg';
        $pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
        $pdf['heading'] = $data['heading'];
        if (isset($data['date_equals']) && $data['date_equals'] != '') {
            $pdf['date_equals'] = $data['date_equals'];
        } else {
            if (isset($data['date_from']) && $data['date_from'] != '')
                $pdf['date_from'] = $data['date_from'];
            if (isset($data['date_to']) && $data['date_to'] != '')
                $pdf['date_to'] = $data['date_to'];
        }
        $pdf['html_loop'] = $html_loop;
        $pdf['filename'] = 'SH_' . md5($pdf['current_time']);

        $this->report_pdf($pdf);
        return '/upload/' . $pdf['filename'] . '.pdf';
    }
    public function option_summary_product_find()
    {
    	$arr_data['shippings_type'] = $this->Setting->select_option_vl(array('setting_value'=>'shipping_types'));
        $arr_data['shippings_status'] = $this->Setting->select_option_vl(array('setting_value'=>'shipping_statuses'));
        $arr_data['product_category'] = $this->Setting->select_option_vl(array('setting_value' => 'product_category'));
        $this->set('arr_data',$arr_data);
    }
    public function option_detailed_product_find()
    {
    	$arr_data['shippings_type'] = $this->Setting->select_option_vl(array('setting_value'=>'shipping_types'));
        $arr_data['shippings_status'] = $this->Setting->select_option_vl(array('setting_value'=>'shipping_statuses'));
        $arr_data['product_category'] = $this->Setting->select_option_vl(array('setting_value' => 'product_category'));
        $this->set('arr_data',$arr_data);
    }
    public function get_cate_product($value) {
        $cate = $this->Setting->select_option_vl(array('setting_value' => 'product_category'));
        if(isset($cate[$value]))
            echo $cate[$value];
        else
            echo '';
        die();
    }
    public function check_exist_product()
    {
		$data = $_POST;
		parse_str($data['data'],$data);
		if(isset($data)&&$data!='')
		{
			$data = $this->Common->strip_search($data);

			$arr_where = array();
			$arr_where['deleted'] = false;
			if(@$data['type']!='')
				$arr_where['shipping_type'] = @$data['type'];
			if(@$data['status']!='')
				{
					$arr_where['shipping_status'] = $data['status'];
				}
			//Check loại trừ cancel thì bỏ các status bên dưới
			if(@$data['is_not_cancel']==1)
			{
				$arr_where['shipping_status'] = array('$nin'=>array('Delivered','Cancelled'));
				//Tuy nhiên nếu ở ngoài combobox nếu có chọn, thì ưu tiên nó, set status lại
				if(@$data['status']!='')
				{
					$arr_where['shipping_status'] = '';
					$arr_where['shipping_status'] = $data['status'];
				}

			}
			$arr_where['return_status'] = 0;
			if(@$data['is_return']==1)
				$arr_where['return_status'] = 1;
			else
				$arr_where['return_status'] = 0;
			//Có hai trường hợp, hoặc là receiver (type là in) hoặc là sender (type là out)
			if(@$data['company_receiver']!=''|| @$data['contact_receiver']!='')
			{
				if(@$data['company_receiver']!='')
					$arr_where['company_name'] = new MongoRegex('/'.trim($data['company_receiver']).'/i');
				if(@$data['contact_receiver']!='')
					$arr_where['contact_name'] = new MongoRegex('/'.trim($data['contact_receiver']).'/i');
				if(@$data['status']=='')
					$arr_where['shipping_type'] = 'In';
			}
			else if(@$data['company_sender']!='' || @$data['contact_sender'] != '')
			{
				if(@$data['company_sender']!='')
					$arr_where['company_name'] = new MongoRegex('/'.trim($data['company_sender']).'/i');
				if(@$data['contact_sender']!='')
					$arr_where['contact_name'] = new MongoRegex('/'.trim($data['contact_sender']).'/i');
				if(@$data['status']=='')
					$arr_where['shipping_type'] = 'Out';
			}
			if(@$data['carrier']!='')
				$arr_where['carrier_name'] = new MongoRegex('/'.trim($data['carrier']).'/i');
			//Tìm chính xác ngày
			//Vì để = chỉ tìm đc 01/01/1969 00:00:00 nên phải cộng cho 23:59:59 rồi tìm trong khoảng đó
			if(@$data['date_equals']!='')
			{
				$date_equals = $data['date_equals'];
				$date_equals = new MongoDate(strtotime(@date('Y-m-d',strtotime($date_equals))));
				$date_equals_to = new MongoDate($date_equals->sec + 24*3600 -1);
				$arr_where['shipping_date']['$gte'] = $date_equals;
				$arr_where['shipping_date']['$lt'] = $date_equals_to;
			}
			//Ngày nằm trong khoảng
			else if(@$data['date_equals'] == '')
			{
				//neu chi nhap date from
				if($date_from = $data['date_from'])
				{
					$date_from = new MongoDate(strtotime(@date('Y-m-d',strtotime(@$date_from))));
					$arr_where['shipping_date']['$gte'] = $date_from;
				}
				//neu chi nhap date to
				if($date_to = $data['date_to'])
				{
					$date_to = new MongoDate(strtotime(@date('Y-m-d',strtotime(@$date_to))));
					$date_to = new MongoDate($date_to->sec + 24*3600 -1);
					$arr_where['shipping_date']['$lte'] = $date_to;
				}
			}
			//Employee có thể là our_rep hoặc our_csr
			if (trim($data['employee']) != '') {
                $arr_where['$or'][]['our_rep'] = new MongoRegex('/' . trim($data['employee']) . '/i');
                $arr_where['$or'][]['our_csr'] = new MongoRegex('/' . trim($data['employee']) . '/i');
            }
            //Kiểm tra nếu có thông tin liên quan đến product tồn tại
            $pro_list = array();
            if(@$data['product_code']!= '' || @$data['product_category'] != '' || @$data['product_sell_price']!= '' || @$data['product_name']!= '')
            {

            	$pro_where = array();
            	if(isset($data['product_code'])&&$data['product_code']!='')
            		$pro_where['code'] = new MongoRegex('/' . trim($data['product_code']).'/i');
            	if(isset($data['product_name'])&&$data['product_name']!='')
            		$pro_where['name'] = new MongoRegex('/' . trim($data['product_name']).'/i');
            	if(isset($data['product_sell_price'])&&$data['product_sell_price']!='')
            		$pro_where['sell_price'] = $data['product_sell_price'];
            	if(isset($data['product_category_id'])&&$data['product_category_id']!='')
	            	$pro_where['category'] = new MongoRegex('/'.trim($data['product_category_id'].'/i'));
	            if($pro_where!='')
	            {
	            	//Lấy ra _id của Product phù hợp với điều kiện trên
	            	$this->selectModel('Product');
	           		$pro_list = $this->Product->select_all(array(
	            							'arr_where'=>$pro_where,
	            							'arr_field'=>array('_id')
	            		));
	            }
            }
            if($pro_list!='')
            	foreach($pro_list as $p_id)
            		$arr_where['products_id']['$in'][] = new MongoId($p_id['_id']);
 			$this->selectModel('Shipping');
			//lay het shipping, voi where nhu tren va lay sum_amount giam dan
			$shipping = $this->Shipping->select_all(array(
					'arr_where'=>$arr_where,
					'arr_order'=>array('sum_amount'=>-1),
					'arr_field'=>array('_id','code','company_name','company_id','contact_name','contact_id','shipping_type','carrier_name','shipping_date','shipping_status','products')
				));
			if($shipping->count()==0)
			{
				echo 'empty';
				die;
			}
			else
			{
				$data['product_category'] = $this->Setting->select_option_vl(array('setting_value'=>'product_category'));
				$url = '';
				if($data['report_type']=='summary')
					$url = $this->summary_product_report_pdf($shipping,$data);
				else if($data['report_type'] == 'detailed')
					$url = $this->detailed_product_report_pdf($shipping,$data);
				else
					$url = $this->summary_product_report_pdf($shipping,$data);

				echo URL.$url;
				die;
			}
		}
    }
    public function summary_product_report_pdf($shipping,$data,$pro_list = '')
    {
        $group = array();
        $color = '';
        $html_loop = '';
        $total = 0;
        //Nếu tìm theo product sẽ xuất hiện 1 list các (hoặc chỉ 1 nếu tìm chính xác) product_id, lọc theo product_id
        if($pro_list!='')
        {
            foreach($pro_list as $pro)
            {
                foreach($shipping as $value)
                 {
                 	if($value['products']!='')
	                     foreach($value['products'] as $products)
	                     {
	                        if($products['products_id'] == $pro['_id'] &&@$products['deleted']==false&&isset($products['code'])&&$products['code']!='')
	                        {
	                        	if(!isset($products['code'])&&$products['code']=='') continue;
	                            $code = $products['code'];
	                            $group[$code]['shipping_status'] = $value['shipping_status'];
	                            $group[$code]['name'] = $products['products_name'];
	                            if(!isset($group[$code]['quantity']))
	                                $group[$code]['quantity'] = 0;
	                            $group[$code]['quantity'] += $products['quantity'];
	                            if(!isset($group[$code]['sub_total']))
	                                $group[$code]['sub_total'] = 0;
	                            $group[$code]['sub_total'] += $products['sub_total'];
	                        }
	                     }
                 }
            }
        }
        else
        {

            foreach($shipping as $value)

             {
             	if(isset($value['products'])&&$value['products']!='')
	                 foreach($value['products'] as $products)
	                 {
	                    if($products['deleted']==false&&isset($products['code'])&&$products['code']!='')
	                    {
                    		if(!isset($products['code'])&&$products['code']=='') continue;
                            $code = $products['code'];
                            $group[$code]['shipping_status'] = $value['shipping_status'];
                            $group[$code]['name'] = $products['products_name'];
                            if(!isset($group[$code]['quantity']))
                                $group[$code]['quantity'] = 0;
                            $group[$code]['quantity'] += $products['quantity'];
                            if(!isset($group[$code]['sub_total']))
                                $group[$code]['sub_total'] = 0;
                            $group[$code]['sub_total'] += $products['sub_total'];
	                     }
	                 }
             }
        }
        $html_loop .= '
                <table cellpadding="3" cellspacing="0" class="maintb">
                  <tr>
                     <td width="15%" class="first top">
                        P.Code
                     </td>
                     <td width="30%" class="top">
                        Product Name
                     </td>
                     <td width="20%" class="top">
                        Category
                     </td>
                     <td width="15%" class="top">
                        Qty
                     </td>
                     <td colspan="2" width="20%" class="end top">
                       Ex. Tax Total
                     </td>
                  </tr>';
        $this->selectModel('Product');

        $i = 0;
        //pr($data);
        foreach($group as $code=>$value)
        {
            $product = $this->Product->select_one(array('code'=>$code));

            $color = '#fdfcfa;';
            if($i%2==0)
                $color = '#eeeeee;';
            $html_loop .= '
                <tr style="background-color:'.$color.'">
                     <td class="content" style="padding-left:5px" align="left">'.$code.'</td>
                     <td class="content" align="left">'.@$value['name'].'</td>
                     <td class="content" align="left">'.@$data['product_category'][$product['category']].'</td>
                     <td class="content">'.@$value['quantity'].'</td>
                     <td class="content" colspan="2"  align="right" class="end">'.(@$value['sub_total'] != '' ? number_format(@$value['sub_total'],2,'.',',') : 0).'</td>
                </tr>
            ';
            $total += (@$value['sub_total'] != '' ? @$value['sub_total'] : 0);
            $i++;

        }
        $color = '#fdfcfa';
        if($i%2==0)
            $color = '#eeeeee';
        $html_loop .= '
                    <tr style="background-color:'.$color.'">
                         <td align="left" class="first bottom">'.($i).' records listed</td>
                         <td class="bottom">&nbsp;</td>
                         <td class="bottom">&nbsp;</td>
                         <td class="bottom">&nbsp;</td>
                         <td align="left" class="bottom"><span style="font-weight:bold; padding-left:20px">Total:   </span></td>
                         <td align="right" class="content bottom">'.number_format($total,2,'.',',').'</td>
                      </tr>
                    </table>
                    <br />
                    <div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>
                    <br />';


        //========================================
        $pdf['current_time'] = date('h:i a m/d/Y');
        $pdf['title'] = '<span style="color:#b32017">P</span>roduct <span style="color:#b32017">S</span>hipping <span style="color:#b32017">R</span>eport <br />(Summary)';
        $this->layout = 'pdf';
            //set header
        $pdf['logo_link'] = 'img/logo_anvy.jpg';
        $pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
        $pdf['heading'] = $data['heading'];
        if(isset($data['date_equals'])&&$data['date_equals']!='')
        {
            $pdf['date_equals'] = $data['date_equals'];
        }
        else
        {
            if(isset($data['date_from'])&&$data['date_from']!='')
                $pdf['date_from']  = $data['date_from'];
            if(isset($data['date_to'])&&$data['date_to']!='')
                $pdf['date_to'] = $data['date_to'];
        }
        $pdf['html_loop'] = $html_loop;
        $pdf['filename'] = 'SH_'.md5($pdf['current_time']);

        $this->report_pdf($pdf);
        return '/upload/'.$pdf['filename'].'.pdf';
    }
    public function detailed_product_report_pdf($shipping,$data,$pro_list = '')
    {
        $group = array();
        $color = '';
        $html_loop = '';
        $total = 0;
        //Nếu tìm theo product sẽ xuất hiện 1 list các (hoặc chỉ 1 nếu tìm chính xác) product_id, lọc theo product_id
        if($pro_list!='')
        {
            foreach($pro_list as $pro)
            {
                foreach($shipping as $value)
                 {
                 	if(isset($value['products'])&&!empty($value['products']))
	                     foreach($value['products'] as $products)
	                     {
	                        if($products['products_id'] == $pro['_id'] &&@$products['deleted']==false&&isset($products['code'])&&$products['code']!='')
	                        {
                                if(!isset($products['code'])||$products['code']=='') continue;
	                            $code = $products['code'];
	                            $group[$code]['shipping_status'] = $value['shipping_status'];
	                            $group[$code]['name'] = $products['products_name'];
	                            $group[$code]['shipping'][$value['code']]['company_name'] = $value['company_name'];
	                            $group[$code]['shipping'][$value['code']]['code'] = $value['code'];
	                            $group[$code]['shipping'][$value['code']]['shipping_date'] = ($value['shipping_date']!= '' ? $this->opm->format_date($value['shipping_date']) : '');
	                            if(isset($group[$code]['shipping'][$value['code']]['quantity']))
	                                $group[$code]['shipping'][$value['code']]['quantity'] += (isset($products['quantity'])&&$products['quantity']!= '' ? $products['quantity'] : 0);
	                            else
	                                $group[$code]['shipping'][$value['code']]['quantity'] = (isset($products['quantity'])&&$products['quantity']!= '' ? $products['quantity'] : 0);
	                            if(isset($group[$code]['shipping'][$value['code']]['sell_price']))
	                                $group[$code]['shipping'][$value['code']]['sell_price'] += (isset($products['sell_price'])&&$products['sell_price'] != '' ? $products['sell_price'] : 0);
	                            else
	                                $group[$code]['shipping'][$value['code']]['sell_price'] = (isset($products['sell_price'])&&$products['sell_price'] != '' ? $products['sell_price'] : 0);
	                            if(isset($group[$code]['shipping'][$value['code']]['sub_total']))
	                                $group[$code]['shipping'][$value['code']]['sub_total'] += (isset($products['sub_total'])&&$products['sub_total'] != '' ? $products['sub_total'] : 0);
	                            else
	                                $group[$code]['shipping'][$value['code']]['sub_total'] = (isset($products['sub_total'])&&$products['sub_total'] != '' ? $products['sub_total'] : 0);
	                            if(!isset($group[$code]['total']))
	                                $group[$code]['total'] = 0;
	                            $group[$code]['total'] += (isset($products['sub_total'])&&$products['sub_total'] != '' ? $products['sub_total'] : 0);
	                            if(!isset($group[$code]['quantity']))
	                                $group[$code]['quantity'] = 0;
	                            $group[$code]['quantity'] += $group[$code]['shipping'][$value['code']]['quantity'];
	                        }
	                     }
                 }
            }
        }
        else
        {

            foreach($shipping as $value)

             {
             	if(isset($value['products'])&&!empty($value['products']))
	                 foreach($value['products'] as $products)
	                 {
	                    if($products['deleted']==false&&isset($products['code'])&&$products['code']!='')
	                    {
	                    	if(!isset($products['code'])||$products['code']=='') continue;
	                        $code = $products['code'];
                            $group[$code]['shipping_status'] = $value['shipping_status'];
                            $group[$code]['name'] = $products['products_name'];
                            $group[$code]['shipping'][$value['code']]['company_name'] = $value['company_name'];
                            $group[$code]['shipping'][$value['code']]['code'] = $value['code'];
                            $group[$code]['shipping'][$value['code']]['shipping_date'] = ($value['shipping_date']!= '' ? $this->opm->format_date($value['shipping_date']) : '');
                            if(isset($group[$code]['shipping'][$value['code']]['quantity']))
                                $group[$code]['shipping'][$value['code']]['quantity'] += (isset($products['quantity'])&&$products['quantity']!= '' ? $products['quantity'] : 0);
                            else
                                $group[$code]['shipping'][$value['code']]['quantity'] = (isset($products['quantity'])&&$products['quantity']!= '' ? $products['quantity'] : 0);
                            if(isset($group[$code]['shipping'][$value['code']]['sell_price']))
                                $group[$code]['shipping'][$value['code']]['sell_price'] += (isset($products['sell_price'])&&$products['sell_price'] != '' ? $products['sell_price'] : 0);
                            else
                                $group[$code]['shipping'][$value['code']]['sell_price'] = (isset($products['sell_price'])&&$products['sell_price'] != '' ? $products['sell_price'] : 0);
                            if(isset($group[$code]['shipping'][$value['code']]['sub_total']))
                                $group[$code]['shipping'][$value['code']]['sub_total'] += (isset($products['sub_total'])&&$products['sub_total'] != '' ? $products['sub_total'] : 0);
                            else
                                $group[$code]['shipping'][$value['code']]['sub_total'] = (isset($products['sub_total'])&&$products['sub_total'] != '' ? $products['sub_total'] : 0);
                            if(!isset($group[$code]['total']))
                                $group[$code]['total'] = 0;
                            $group[$code]['total'] += (isset($products['sub_total'])&&$products['sub_total'] != '' ? $products['sub_total'] : 0);
                            if(!isset($group[$code]['quantity']))
                                $group[$code]['quantity'] = 0;
                            $group[$code]['quantity'] += $group[$code]['shipping'][$value['code']]['quantity'];
	                     }
	                 }
             }
        }
        $html_loop .= '
                <table cellpadding="3" cellspacing="0" class="maintb">
                  <tr>
                     <td width="15%" class="first top">
                        P.Code
                     </td>
                     <td width="30%" class="top">
                        Product Name
                     </td>
                     <td width="20%" class="top">
                        Category
                     </td>
                     <td width="15%" class="top">
                        Qty
                     </td>
                     <td colspan="2" width="20%" class="end top">
                       Ex. Tax Total
                     </td>
                  </tr>';
        $this->selectModel('Product');

        $i = 0;
        foreach($group as $code=>$value)
        {
        	$total = ($value['total']!= '' ? number_format($value['total'],2,',','.') : 0);
            $product = $this->Product->select_one(array('code'=>$code));
            $html_loop .= '
            <table cellpadding="3" cellspacing="0" class="maintb">
               <tbody>
                  <tr>
                    <td width="10%" class="first top">
                        P. Code
                     </td>
                     <td width="40%" class="top">
                        Product Name
                     </td>
                     <td width="25%" class="top">
                        Category
                     </td>
                     <td colspan="2" width="25%" class="top">
                        Group total (ex. tax)
                     </td>
                  </tr>
                  <tr style="background-color:#eeeeee;">
                     <td class="first content" align="left">'.$code.'</td>
                     <td class="content" align="left">'.@$value['name'].'</td>
                     <td class="content">'.@$data['product_category'][$product['category']].'</td>
                     <td colspan="2" class="content" align="right">'.$total.'</td>
                  </tr>
               </tbody>
            </table>
            <br /><br />';
            $html_loop .= '<table cellpadding="3" cellspacing="0" class="maintb">
                        <tbody>
                          <tr>
                             <td width="7%" class="first top">
                                SH#
                             </td>
                             <td width="28%" class="top">
                                Company
                             </td>
                             <td width="15%" class="top">
                                Date
                             </td>
                             <td width="10%" class="top">
                                Unit Price
                             </td>
                             <td width="20%" class="top">
                                Quantity
                             </td>
                             <td colspan="2" width="20%" class="end top">
                                Ex. Tax total
                             </td>
                          </tr>';
            if(is_array($value))
            {
                $i = 0;
                foreach($value['shipping'] as $val)
                {
                	$sub_total = ($val['sub_total'] != 0 ? number_format($val['sub_total'],2,'.',',') : 0);
                    $color = '#fdfcfa';
                    if($i%2==0)
                        $color = '#eeeeee';

                    $html_loop .= '
                          <tr style="background-color:'.$color.';">
                             <td class="first content">'.$val['code'].'</td>
                             <td class="content">'.$val['company_name'].'</td>
                             <td class="content">'.$val['shipping_date'].'</td>
                             <td class="content">'.@$val['sell_price'].'</td>
                             <td class="content">'.@$val['quantity'].'</td>
                             <td colspan="2" class="content"  align="right" class="end">'.$sub_total.'</td>
                          </tr>';
                    $i++;
                }
            }
            $color = '#fdfcfa';
            if(!isset($i))
                $i =0;
            if($i%2==0)
                $color = '#eeeeee';
            $html_loop .= '
                            <tr style="background-color:'.$color.'">
                             <td colspan="4" align="left" class="first bottom">'.$i.' record(s) listed</td>
                             <td class="bottom">&nbsp;</td>
                             <td align="left" class="bottom"><span style="font-weight:bold; padding-left:20px">Total:</span></td>
                             <td align="right" class="content bottom">'.$total.'</td>
                          </tr>
                        </tbody>
                    </table>
                    <br />
                    <div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div><br />';

        }

        //========================================
        $pdf['current_time'] = date('h:i a m/d/Y');
        $pdf['title'] = '<span style="color:#b32017">P</span>roduct <span style="color:#b32017">S</span>hipping <span style="color:#b32017">R</span>eport <br />(Detail)';
        $this->layout = 'pdf';
            //set header
        $pdf['logo_link'] = 'img/logo_anvy.jpg';
        $pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
        $pdf['heading'] = $data['heading'];
        if(isset($data['date_equals'])&&$data['date_equals']!='')
        {
            $pdf['date_equals'] = $data['date_equals'];
        }
        else
        {
            if(isset($data['date_from'])&&$data['date_from']!='')
                $pdf['date_from']  = $data['date_from'];
            if(isset($data['date_to'])&&$data['date_to']!='')
                $pdf['date_to'] = $data['date_to'];
        }
        $pdf['html_loop'] = $html_loop;
        $pdf['filename'] = 'SH_'.md5($pdf['current_time']);

        $this->report_pdf($pdf);
        return '/upload/'.$pdf['filename'].'.pdf';
    }
    /*
	End report
    */
    public function check_condition_shipping()
    {
    	$this->selectModel('Shipping');
    	$id = $this->get_id();
    	if($id=='')
    		$shipping = $this->Shipping->select_one(array(),array(),array('_id'=>-1));
    	$shipping = $this->Shipping->select_one(array('_id' => new MongoId($id)));
    	if($shipping!='')
    	{

    		if($shipping['shipping_type']=='In')
    			return array('err1');
    		else if($shipping['company_id']==''
    				&&$shipping['contact_id']=='')
    			return array('err3');
    		else if($shipping['products']=='')
    			return array('err2');

    		else if(isset($shipping['invoice_id'])&&$shipping['invoice_id']!='')
    			return array('err4',$shipping['invoice_id']);
    		return $shipping;
    	}
    	return false;

    }




    public function create_sales_invoice()
    {
    	$this->autoRender = FALSE;
    	$check = $this->check_condition_shipping();
    	if(@$check[0] == 'err1')
    	{
    		echo json_encode(array('status'=>'error','mess'=>'Sales invoices are not created from incoming shippings.'));
    	}
    	else if(@$check[0] == 'err2')
    	{
    		echo json_encode(array('status'=>'error','mess'=>'No items have been entered on this transaction yet.'));
    	}
    	else if(@$check[0] == 'err3')
    	{
    		echo json_encode(array('status'=>'error','mess'=>'This function cannot be performed as there is no company or contact linked to this record.'));
    	}
    	else if(@$check[0] == 'err4')
    	{
    		echo json_encode(array('status'=>'exist','mess'=>'This shipping record is already linked to a sales invoice. View sales invoice?','url'=>URL.'/salesinvoices/entry/'.$check[1]));
    	}
    	else if(is_array($check))
    	{
    		$arr_save = $check;
    		$this->selectModel('Salesinvoice');
			$arr_save['code'] = $this->Salesinvoice->get_auto_code('code');
    		$arr_save['invoice_type'] =  "Invoice";
    		$arr_save['invoice_status'] = 'Invoice';
    		$arr_save['invoice_date'] = new MongoDate();
    		$arr_save['shipping_id'] = new MongoId($check['_id']);
    		$arr_save['shipping_code'] = $check['code'];
    		$arr_save['job_id'] = '';
    		$arr_save['job_name'] = '';
    		$arr_save['job_number'] = '';
    		$arr_save['paid_date'] = '';
    		$arr_save['payment_due_date'] = '';
    		$arr_save['payment_term'] = '';
    		$arr_save['salesorder_id'] = '';
    		$arr_save['salesorder_name'] = '';
    		$arr_save['salesorder_number'] = '';
    		//Hiện tại product chưa thêm được nên ko có các giá trị tiền
    		//Và ko có cả các field bên dưới
    		// => sales invoice sẽ ko có giá trị tiền
    		$arr_save['sum_amount']  = (isset($check['sum_amount']) ? $check['sum_amount'] : 0);
			$arr_save['sum_sub_total']  = (isset($check['sum_sub_total']) ? $check['sum_sub_total'] : 0);
			$arr_save['sum_tax'] = (isset($check['sum_tax']) ? $check['sum_tax'] : 0);
			$arr_save['tax']  = (isset($check['tax']) ? $check['tax'] : 0);
			$arr_save['taxval'] = (isset($check['taxval']) ? $check['taxval'] : 0);
    		unset($arr_save['_id']);
    		unset($arr_save['carrier_id']);
    		unset($arr_save['carrier_name']);
    		unset($arr_save['date_modifide']);
    		unset($arr_save['invoice_id']);
    		unset($arr_save['invoice_name']);
    		unset($arr_save['modified_by']);
    		unset($arr_save['received_date']);
    		unset($arr_save['return_status']);
    		unset($arr_save['shipping_date']);
    		unset($arr_save['shipping_status']);
    		unset($arr_save['shipping_type']);
    		unset($arr_save['tracking_no']);
    		unset($arr_save['traking']);
    		if($this->Salesinvoice->save($arr_save))
    		{
    			$id = $this->Salesinvoice->mongo_id_after_save;
    			$check['invoice_code'] = $arr_save['code'];
    			$check['invoice_id'] = $id;
    			$this->selectModel('Shipping');
    			$this->Shipping->save($check);
    			echo json_encode(array('status'=>'ok','url'=>URL.'/salesinvoices/entry/'.$id));
    		}
    	}
    	die;
    }

	function view_minilist() {
		$this->layout = 'pdf';

		$date_now = date('Ymd');
		$time=time();
		$filename = 'SHIP'.$date_now.$time;
		$this->selectModel('Shipping');
		$sort_field = '_id';
		$sort_type = 1;
		if( isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0 ){
			if( $_POST['sort']['type'] == 'desc' ){
				$sort_type = -1;
			}
			$sort_field = $_POST['sort']['field'];
			$this->Session->write('shippings_lists_search_sort', array($sort_field, $sort_type));

		}elseif( $this->Session->check('shippings_lists_search_sort') ){
			$session_sort = $this->Session->read('shippings_lists_search_sort');
			$sort_field = $session_sort[0];
			$sort_type = $session_sort[1];
		}
		$arr_order = array($sort_field => $sort_type);
		$cond = array();
		if( $this->Session->check('shippings_entry_search_cond') ){
			$cond = $this->Session->read('shippings_entry_search_cond');
		}
		$arr_pur = $this->Shipping->select_all(array(
			'arr_where' => $cond,
			'arr_order' => $arr_order,
			'limit' => LIMIT_PRINT_PDF
		));
		$html='';
		$this->selectModel('Contact');
		$i=0;
		foreach($arr_pur as $key=>$value){

			if($i%2==0)
				$html .= ' <table cellpadding="4" cellspacing="0" class="tab_nd">';
			else
				$html .= '<table cellpadding="4" cellspacing="0" class="tab_nd2">';

			$html .= ' <tr class="border_2">
			<td width="6%" class="first top border_left border_btom">';

			if(isset($value['code']))
				$html .= $value['code'];

			$html .= '</td>
			<td width="8%" class="top border_btom border_left" align="center">';

			if(isset($value['shipping_type']))
				$html .=$value['shipping_type'];

			$html .='</td>
			<td width="8%" class="top border_btom border_left " align="center">';


			if(isset($value['return_status'])&&$value['return_status']==1)
				$html .= 'x';
			else
				$html .= '';

			$html .='</td>
			<td align="left" width="22%" class="top border_btom border_left">
				';


			if(isset($value['company_name']))
				$html .= $value['company_name'];



			$html.='
			</td>
			<td align="center" width="6%" class="top border_btom border_left">
				';


			if(isset($value['customer_po_no']))
				$html .= $value['customer_po_no'];



			$html.='
			</td>
			<td width="11%" class="end top border_btom border_left">
				';

			if(isset($value['phone']))
				$html .= $value['phone'];


			$html.='
			</td>
			<td width="8%" class="end top border_btom border_left">
				';

			if(isset($value['shipping_date'])&&is_object($value['shipping_date']))
				$html .= $this->opm->format_date($value['shipping_date']->sec,false);


			$html.='
			</td>
			<td width="22%" class="end top border_btom border_left">
				';

			if(isset($value['carrier_name']))
				$html .= $value['carrier_name'];

			$html.='
			</td>
			<td width="9%" class="end top border_btom border_left">
				';

			if(isset($value['shipping_status']))
				$html .= $value['shipping_status'];


			$html.='
			</td>
		</tr>
	</table>
';


			$i+=1;
		}
		$html_new = $html;

		// =================================================== tao file PDF ==============================================//
		include(APP.'Vendor'.DS.'nguyenpdf.php');

		$pdf = new XTCPDF();
		date_default_timezone_set('UTC');
		$pdf->today=date("g:i a, j F, Y");
		$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Anvy Digital');
		$pdf->SetTitle('Anvy Digital Company');
		$pdf->SetSubject('Company');
		$pdf->SetKeywords('Company, PDF');

// set default header data
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);

// set default monospaced font
		$pdf->SetDefaultMonospacedFont(2);

// set margins
		$pdf->SetMargins(10, 52, 10);
		$pdf->file3 = 'img'.DS.'bar_975x23.png';

		$pdf->file2_left=208;
		$pdf->file2='img'.DS.'Shipping_title.png';


		$pdf->bar_top_left=208;
		$pdf->bar_top_top=23;
		$pdf->bar_top_content='------------------------------------------------------------------';

		$pdf->hidden_left=251;
		$pdf->hidden_top=19;
		$pdf->hidden_content='(with main shipping)';

		$pdf->bar_big_content='------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';

		$pdf->bar_words_content='Ref No          Type             Return         Name                                                    Po No            Phone                 Date         Carrier                                                        Status';
//		$pdf->bar_mid_content='         |                                                    |                               |                         |                          |              |';
		$pdf->bar_mid_content='';

		$pdf->printedat_left=223;
		$pdf->printedat_top=28;
		$pdf->time_left=241;
		$pdf->time_top=28;

		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 30);

// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		if (@file_exists(dirname(__FILE__).DS.'lang'.DS.'eng.php')) {
			require_once(dirname(__FILE__).DS.'lang'.DS.'eng.php');
			$pdf->setLanguageArray($l);
		}

// ---------------------------------------------------------
// set font
		$pdf->SetFont($textfont, '', 9);

// add a page
		$pdf->AddPage('L', 'A4');
		$pdf->SetMargins(10, 19, 10);

		$pdf->file1 = 'img'.DS.'null.png';
		$pdf->file2 = 'img'.DS.'null.png';
		$pdf->file4 = 'img'.DS.'null.png';
		$pdf->file5 = 'img'.DS.'null.png';
		$pdf->file6 = 'img'.DS.'null.png';
		$pdf->file7 = 'img'.DS.'null.png';

		$pdf->file3_top=10;
		$pdf->bar_words_top=11;
		$pdf->bar_mid_top=10.6;
		$pdf->hidden_content='';
		$pdf->bar_top_content='';
		$pdf->today='';
		$pdf->print='';
		$pdf->address_1='';
		$pdf->address_2='';
		$pdf->bar_big_content='';
		$html='
			<style>
				table{
					font-size: 12px;
					font-family: arial;
				}
				td.first{
					border-left:1px solid #e5e4e3;
				}
				td.end{
					border-right:1px solid #e5e4e3;
				}
				td.top{
					color:#fff;
					font-weight:bold;
					background-color:#911b12;
					border-top:1px solid #e5e4e3;
				}
				td.bottom{
					border-bottom:1px solid #e5e4e3;
				}
				.option{
					color: #3d3d3d;
					font-weight:bold;
					font-size:18px;
					text-align: center;
					width:100%;
				}
				.border_left{
					border-left:1px solid #A84C45;
				}
				.border_1{
					border-bottom:1px solid #911b12;
				}

			</style>
			<style>
		            table.tab_nd{
		                font-size: 12px;
		                font-family: arial;
		            }
		            table.tab_nd td.first{
		                border-left:1px solid #e5e4e3;
		            }
		            table.tab_nd td.end{
		                border-right:1px solid #e5e4e3;
		            }
		            table.tab_nd td.top{
		                background-color:#FDFBF9;
		                border-top:1px solid #e5e4e3;
		                font-weight: normal;
		                color: #3E3D3D;
		            }
		            table.tab_nd .border_2{
		                border-bottom:1px solid red;
		            }
		            table.tab_nd .border_left{
		                border-left:1px solid #E5E4E3;
		                border-bottom:1px solid #E5E4E3;
		            }
		            table.tab_nd .border_btom{
		                border-bottom:1px solid #E5E4E3;
		            }

		        </style>
		        <style>
		                table.tab_nd2{
		                    font-size: 12px;
		                    font-family: arial;
		                }
		                table.tab_nd2 td.first{
		                    border-left:1px solid #e5e4e3;
		                }
		                table.tab_nd2 td.end{
		                    border-right:1px solid #e5e4e3;
		                }
		                table.tab_nd2 td.top{
		                    background-color:#EDEDED;
		                    border-top:1px solid #e5e4e3;
		                    font-weight: normal;
		                    color: #3E3D3D;
		                }
		                table.tab_nd2 .border_2{
		                    border-bottom:1px solid red;
		                }
		                table.tab_nd2 .border_left{
		                    border-left:1px solid #E5E4E3;
		                    border-bottom:1px solid #E5E4E3;
		                }
		                table.tab_nd2 .border_btom{
		                    border-bottom:1px solid #E5E4E3;
		                }
		                .size_font{
		                    font-size: 12px !important;
		                }

		            </style>
		';
		$html.=$html_new;
		$html .= '

	<table cellpadding="3" cellspacing="0" class="tab_nd2">
		<tr class="border_2">
			<td width="80%" class="first top border_btom size_font">
				&nbsp;';

		$html .= $i;

		$html .=' records listed
			</td>
			<td width="20%" class="end top border_btom">
				&nbsp;
			</td>
		</tr>
	</table>
	<div style=" clear:both; color: #c9c9c9;"><br />
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	</div><br />
	';
		$pdf->writeHTML($html, true, false, true, true, '');
		$pdf->Output(APP. 'webroot'.DS. 'upload'.DS .$filename.'.pdf', 'F');
		$this->redirect('/upload/'. $filename .'.pdf');
		die;
	}



}