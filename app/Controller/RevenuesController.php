<?php
App::import('Controller', 'Salesorders');
App::import('Controller', 'Receipts');
App::uses('AppController', 'Controller');
class RevenuesController extends AppController {

	var $name = 'Revenues';
	var $modelName = 'Revenue';
	public $helpers = array();
	public $opm; //Option Module

	public function beforeFilter(){
		parent::beforeFilter();
		$this->set_module_before_filter("Revenue");
	}

	public function rebuild_setting($arr_setting=array()){
        $arr_setting = $this->opm->arr_settings;
        if(!$this->check_permission($this->name.'_@_entry_@_edit'))
            foreach($arr_setting['field']['panel_1'] as $key=>$value)
            	$arr_setting['field']['panel_1'][$key]['lock'] = 1;
        $receipt = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
        $html = '<span style="cursor:default" >Customer</span>';
        if(isset($receipt['company_id'])&&is_object($receipt['company_id'])){
        	$html = '<span class="jt_box_line_span jt_link_on"><a style="color:#444;" href="'.URL.'/companies/entry/'.$receipt['company_id'].'">Customer</a></span>';
        } else if(isset($receipt['contact_id'])&&is_object($receipt['contact_id'])){
        	$html = '<span class="jt_box_line_span jt_link_on"><a style="color:#444;" href="'.URL.'/contacts/entry/'.$receipt['contact_id'].'">Customer</a></span>';
        }
        $this->set('link',$html);
        $this->opm->arr_settings = $arr_setting;
	    $arr_tmp = $this->opm->arr_field_key('cls');
	    $arr_link = array();
	    if(!empty($arr_tmp))
	    	foreach($arr_tmp as $key=>$value)
	    		$arr_link[$value][] = $key;
	    $this->set('arr_link',$arr_link);
    }

	//Các điều kiện mở/khóa field trong entry
	public function check_lock(){
		if($this->get_id()!=''){
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($this->get_id())),array('ext_accounts_sync'));
			if(isset($arr_tmp['ext_accounts_sync']) && $arr_tmp['ext_accounts_sync']==1)
				return true;
		}else
			return false;
	}
	public function swith_options($keys='')
	{
        parent::swith_options($keys);
		if ($keys == 'receipts_today') {
            $current_date = strtotime(date("Y-m-d"));
			$current_date_end = $current_date + DAY - 1;
			$arr_where['receipt_date']['>='] = array('values' => new MongoDate($current_date), 'operator' => 'day>=');
			$arr_where['receipt_date']['<='] = array('values' => new MongoDate($current_date_end), 'operator' => 'day<=');
			$this->Session->write($this->name . '_where', $arr_where);
			echo URL . '/' . $this->params->params['controller'] . '/lists';
        } else if ($keys == 'receipts_this_week') {
        	$week_start = strtotime("this week");
			$week_end = strtotime("this week +6 days") + DAY -1;
			$arr_where['receipt_date']['>='] = array('values' => new MongoDate($week_start), 'operator' => 'day>=');
			$arr_where['receipt_date']['<='] = array('values' => new MongoDate($week_end), 'operator' => 'day<=');
			$this->Session->write($this->name . '_where', $arr_where);
			echo URL . '/' . $this->params->params['controller'] . '/lists';
        }
        else if($keys == 'receipts_this_month'){
        	$month_ini = strtotime("first day of this month");
			$month_end = strtotime("last day of this month") + DAY -1;
			$arr_where['receipt_date']['>='] = array('values' => new MongoDate($month_ini), 'operator' => 'day>=');
			$arr_where['receipt_date']['<='] = array('values' => new MongoDate($month_end), 'operator' => 'day<=');
			$this->Session->write($this->name . '_where', $arr_where);
			echo URL . '/' . $this->params->params['controller'] . '/lists';
        }
        else if($keys == 'receipts_this_year'){
        	$year_start = strtotime("first day of january");
			$year_end = strtotime("last day of this year") + DAY - 1;
			$arr_where['receipt_date']['>='] = array('values' => new MongoDate($year_start), 'operator' => 'day>=');
			$arr_where['receipt_date']['<='] = array('values' => new MongoDate($year_end), 'operator' => 'day<=');
			$this->Session->write($this->name . '_where', $arr_where);
			echo URL . '/' . $this->params->params['controller'] . '/lists';
        }
        else if($keys == 'unallocated_receipts'){
        	$arr_where['unallocated'] = array('values' => 0, 'operator' => 'exists@!=');
			$this->Session->write($this->name . '_where', $arr_where);
			echo URL . '/' . $this->params->params['controller'] . '/lists';
        }
        else if($keys == 'payment_receive_report')
        	echo URL . '/' . $this->params->params['controller'] . '/payment_receive_report';
        else if($keys == 'customer_detailed_report')
        	$this->customer_detailed_report();
        else if($keys == 'customer_summary_report')
        	$this->customer_summary_report();
        else if($keys == 'email_receipt'){
        	$this->create_email_pdf(true);
        }
        else echo URL . '/' . $this->params->params['controller'] . '/entry';
        die;
	}
	//entry main
	public function entry(){

		$this->selectModel('Receipt');
		$abc = new ReceiptsController;
		$arr_salesorder = $abc->cong_no_nam_chi_tiet(2015);
		$arr_purchaseorder = $abc->cong_no_nam_chi_tiet_ncc(2015);
		/*$arr_data = array();
		foreach($arr_salesorder as $key => $value){
			$arr_data[$key]['amount'] = 0;
			foreach($value['thanh_toan'] as $k => $v){
				$arr_data[$key]['date'] = $v['date_modified'];
				if(!isset($v['amount']) || empty($v['amount'])) continue;
				$arr_data[$key]['amount'] += $v['amount'];
			}
		}
		$arr_pur = array();
		foreach($arr_purchaseorder as $key => $value){
			$arr_pur[$key]['name'] = $value['name'];
			if(!isset($value['thanh_toan']) || empty($value['thanh_toan'])) continue;
			$arr_pur[$key]['amount'] = 0;
			foreach($value['thanh_toan'] as $k => $v){
				$arr_pur[$key]['date'] = $v['date_modified'];
				if(!isset($v['amount']) || empty($v['amount'])) continue;
				$arr_pur[$key]['amount'] += $v['amount'];
			}
		}*/

		$this->selectModel('Revenue');
		$this->Revenue->has_field_deleted = false;
		$arr_where_other = array('deleted'=>false);
		if(isset($_POST['sort'])){
			if(isset($_POST['rep']) && $_POST['rep'] !='all'){
				$arr_where_other['our_rep_id'] = new MongoId($_POST['rep']);
			}
		}
		$arr_other = $this->Revenue->select_all(array(
				'arr_where' => $arr_where_other,
				'arr_field' => array(),
				'arr_order' => array('date_paid'=>-1)
			));

		$arr_search_month = array();
		$arr_search_all = array();
		$arr_now = array();
		$arr_return = array();
		foreach ($arr_other as $key => $value) {
			if(isset($_POST['year'])){
				if(intval(date('Y',$value['date_paid']->sec)) == intval($_POST['year'])){
					if($_POST['month'] != 'all'){
						if(intval($_POST['month'])==intval(date('m',$value['date_paid']->sec))){
							$arr_search_month[] = $value;
						}
					}
						$arr_search_all[] = $value;
				}
			}
		}

		if(isset($_POST['year'])){
			if($_POST['month'] != 'all'){
				$arr_return = $arr_search_month;
			}else{
				$arr_return = $arr_search_all;
			}

		}else{
			$arr_return = $arr_other;
		}
		$this->set('arr_other', $arr_return);
		$this->set('arr_pur', $arr_purchaseorder);
		$this->set('arr_data',$arr_salesorder);
		$arr_our_rep = array();
		$this->selectModel('Company');
		$company_id = $this->Company->select_one(array('no'=>1),array('_id'));
		$company_id = (string)$company_id['_id'];
		$this->selectModel('Contact');
		$arr_contact = $this->Contact->select_all(array('company_id'=> new MongoID($company_id), array('_id','full_name')));
		foreach ($arr_contact as $key => $value) {
			if(isset($value['full_name']) && $value['full_name'] !='')
				$arr_our_rep[(string)$value['_id']] = $value['full_name'];
		}
		$first_order = $this->Revenue->select_one(array('deleted'=>false),array(),array('date_paid'=>1));
		if($first_order){
			$first_month = array(
				'month' 	=> intval(date('m',$first_order['date_paid']->sec)),
				'year'		=> intval(date('Y',$first_order['date_paid']->sec))
				);
		}else{
			$first_month = array(
				'month' 	=> intval(date('m')),
				'year'		=> intval(date('Y'))
				);
		}

		$this->set('first_month', $first_month);

		$this->set('arr_our_rep',$arr_our_rep);
		if ($this->request->is('ajax')) {
			echo json_encode( $arr_return);
			die;
		}
		parent::entry();
	}

	public function entry_search(){
		//parent
		$arr_set = $this->opm->arr_settings;

		$arr_set['field']['panel_1']['code']['lock'] = '';
		$arr_set['field']['panel_1']['amount_received']['default'] = '';
		$arr_set['field']['panel_1']['receipt_date']['default'] = '';
		$arr_set['field']['panel_1']['paid_by']['default'] = '';
		$arr_set['field']['panel_1']['our_bank_account']['default'] = '';
		$arr_set['field']['panel_1']['name']['default'] = '';
		$arr_set['field']['panel_1']['our_rep']['default'] = '';
		$arr_set['field']['panel_1']['our_csr']['default'] = '';
		$this->set('arr_settings',$arr_set);
		$where = array();
		if($this->Session->check($this->name.'_where'))
			$where = $this->Session->read($this->name.'_where');
		if(count($where)>0){
			foreach($arr_set['field'] as $ks => $vls){
				foreach($vls as $field => $values){
					if(isset($where[$field])){
						$arr_set['field'][$ks][$field]['default'] = $where[$field]['values'];
					}
				}
			}
		}
		//end parent
	}



	//allocation
	public function allocation(){
		$ret = array();
		$total_allocated = 0;
		$option_select=array();
		if($this->get_id()!=''){
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
			if(isset($arr_tmp['allocation']) && !empty($arr_tmp['allocation'])){
				foreach($arr_tmp['allocation'] as $key=>$allocation){
					if(isset($allocation['deleted'])&&$allocation['deleted']==true) continue;
					if(!isset($allocation['salesinvoice_code']))
						$allocation['salesinvoice_code'] = '';
					$ret[$key] = $allocation;
					$ret[$key]['salesinvoice_code_lock_field'] = true;
					$ret[$key]['salesinvoice_code_name'] = $allocation['salesinvoice_code'];
					$total_allocated	   += $allocation['amount'];
				}
			}
		}
		$this->set('total_allocated',$total_allocated);
		return $ret;
	}

	//outstanding
	public function outstanding($company_id=''){
		$total_all = 0;
		$balance_all = 0;
		$receipt_all = 0;
		$current_date = strtotime(date('d-m-y'));
		$salesinvoice_code = array();
		if($company_id!='')
			$allocation=$this->sum_allocated($company_id);
		else
			$allocation = array();
		$obj_invoice = $this->outstanding_data($company_id);
		pr($obj_invoice);die;
		$arr = array();
		$this->selectModel('Company');
		$orgirin_minimum = 50;
        $this->selectModel('Stuffs');
        $product = $this->Stuffs->select_one(array('value'=>"Minimun Order Adjustment"),array('product_id'));
        $product_id = $product['product_id'];
        if(isset($product['product_id'])&&is_object($product['product_id'])){
            $this->selectModel('Product');
            $product = $this->Product->select_one(array('_id'=> new MongoId($product_id)),array('sell_price'));
            $orgirin_minimum = $product['sell_price'];
            $product_id = $product['_id'];
        }
		if($obj_invoice->count()){
			foreach($obj_invoice as $key=>$invoice){
				$arr[$key]['code'] = $invoice['code'];
				$arr[$key]['invoice_type'] = $invoice['invoice_type'];
				if(isset($invoice['invoice_date']) && is_object($invoice['invoice_date']))
					$arr[$key]['invoice_date'] = date('d M, Y',$invoice['invoice_date']->sec);
				if(isset($invoice['payment_due_date']) && is_object($invoice['payment_due_date'])){
					$arr[$key]['due'] = 0;
					if($current_date > $invoice['payment_due_date']->sec)
						$arr[$key]['due'] = 1;
				}
				//id saleinvoice
				$ids = (string)$invoice['_id'];
				$arr[$key]['_id'] = $key;
				$arr[$key]['total'] 		= 0;
				$arr[$key]['receipts'] 	= 0;
				$arr[$key]['balance'] 	= 0;

				//find receipts
				if(isset($allocation[$ids]))
					$arr[$key]['receipts'] 	= (float)$allocation[$ids];
				$minimum = $orgirin_minimum;
				if(is_object($invoice['company_id'])){
					$company = $this->Company->select_one(array('_id'=>$company_id),array('pricing'));
					if(isset($company['pricing'])){
						foreach($company['pricing'] as $pricing){
							if(isset($pricing['deleted'])&&$pricing['deleted']) continue;
							if((string)$pricing['product_id']!=(string)$product_id) continue;
							if(!isset($pricing['price_break']) || empty($pricing['price_break'])) continue;
							$price_break = reset($pricing['price_break']);
							$minimum =  (float)$price_break['unit_price']; break;
						}
					}
				}
				if(isset($invoice['sum_sub_total']) && $invoice['sum_sub_total']<$minimum){
					$invoice['taxval'] = (isset($invoice['taxval']) ? $invoice['taxval'] : 0);
					$invoice['sum_amount'] = $minimum + ($minimum*$invoice['taxval']/100);
				} else {
					$invoice['sum_sub_total'] = 0;
				}
				$arr[$key]['total'] = $invoice['sum_amount'];
				$arr[$key]['balance'] = round($arr[$key]['total'] - $arr[$key]['receipts'] ,2);
				$total_all += $arr[$key]['total'];
				//select option
				if(isset($invoice['code']) )
					$salesinvoice_code[$invoice['code']] = $invoice['code'];

				$receipt_all += $arr[$key]['receipts'];
				//pr($balance_all);

				if($invoice['invoice_status']=='Paid')
					$arr[$key]['deleted'] = true;
				$arr[$key]['receipt_this'] = '<span class="icon_empleft add_receipt_link" title="Add receipt allocation" id="'.$ids.'" onclick="confirm_add(\''.$ids.'\');"></span>';
				if(isset($arr[$key]['invoice_type']))
					$arr[$key]['invoice_type'] .= '<a href="'.URL.'/salesinvoices/entry/'.$ids.'"><span class="icon_linkleft" title="View" onclick=""></span></a>';
				else
					$arr[$key]['invoice_type'] = '<a href="'.URL.'/salesinvoices/entry/'.$ids.'"><span class="icon_linkleft" title="View" onclick=""></span></a>';
			}
		}
		$balance_all = round($total_all - $receipt_all,2);
		$option_select = array();
		$option_select['salesinvoice_code'] = $salesinvoice_code;
		$this->set('option_select',$option_select);
		$this->set('total',$total_all);
		$this->set('receipts',$receipt_all);
		$this->set('balance',$balance_all);
		// pr($option_select);
		//die;
		return $arr;
	}

	//outstanding data
	public function outstanding_data($company_id=''){
		$arr_query = array();
		if($company_id!=''){
			$company_id = new MongoId($company_id);
			$this->selectModel('Salesinvoice');
			$arr_query = $this->Salesinvoice->select_all(array(
				'arr_where' => array(
					'company_id' => $company_id,
					'invoice_status'=>'Invoiced',
				),
				'arr_field' => array('_id','code','company_id','company_name','invoice_date','invoice_type','invoice_status','name','payment_due_date','payment_terms','sum_amount','sum_sub_total','taxval'),
				'arr_order' => array('_id' => -1)
			));
		}
		return $arr_query;
	}

	public function sum_allocated($company_id=''){
		$datas = array();
		if($company_id!=''){
			if(!is_object($company_id))
				$company_id = new MongoId($company_id);
			$arr_query = $this->opm->select_all(array(
				'arr_where' => array(
						'company_id' => $company_id,
				),
				'arr_order' => array('_id' => -1)
			));
			$ret = array();
			foreach($arr_query as $kss=>$vss){
				if(isset($vss['allocation']) && !empty($vss['allocation'])){
					foreach($vss['allocation'] as $kk=>$vv){
						if(isset($vv['deleted'])&&$vv['deleted']) continue;
						if(isset($vv['salesinvoice_id']) && is_object($vv['salesinvoice_id'])){
							$ids = (string)$vv['salesinvoice_id'];
						}else
							$ids = $kss;
						if(isset($datas[$ids]) && isset($vv['amount']))
							$datas[$ids] += (float)$vv['amount'];
						else if(isset($vv['amount']))
							$datas[$ids] = (float)$vv['amount'];
					}
				}
			}
		}
		return $datas;
	}


	public function reload_box($boxname=''){
		if(!$this->check_permission($this->name.'_@_entry_@_edit') || !$this->check_permission($this->name.'_@_entry_@_add'))
			die;
		if(isset($_POST['boxname']))
			$boxname = $_POST['boxname'];

		if($this->get_id()!=''){
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
			$subdatas = array(); $panel = 'panel_2';
			if($boxname=='outstanding'){
				$panel = 'panel_3';
				$paid_received = array();
				if(isset($arr_tmp['allocation']) && count($arr_tmp['allocation'])>0)
					$allocation = $arr_tmp['allocation'];
				if(isset($arr_tmp['company_id']))
					$subdatas[$boxname] = $this->outstanding($arr_tmp['company_id'],$allocation);
				else
					$subdatas[$boxname] = array();

			}else if($boxname=='allocation'){
				$panel = 'panel_2';
				if(isset($arr_tmp['allocation']) && count($arr_tmp['allocation'])>0)
					$subdatas[$boxname] = $this->allocation();
				else
					$subdatas[$boxname] = array();

				//Set outstanding again
				$subdatas['outstanding'] = array();
				if(isset($arr_tmp['company_id'])){
					$subdatas['outstanding'] = $this->outstanding($arr_tmp['company_id']);
				}
			}
			$arr_settings = $this->opm->arr_settings;


			if(isset($arr_tmp['amount_received']))
			$this->set('amount_allocated',(float)$arr_tmp['amount_received']);

			$this->set('blockname', $boxname);
			$this->set('arr_subsetting',$arr_settings['field'][$panel]['block']);
			$this->set('subdatas', $subdatas);
			$this->set('box_type', $arr_settings['field'][$panel]['block'][$boxname]['type']);
		}else
			die;
	}



	//Khi $field thay đổi thì các field này cũng thay đổi theo
	public function arr_associated_data($field='',$value='',$ids='',$field_change = ''){
		if(!$this->check_permission($this->name.'_@_entry_@_edit')){
			echo 'You do not have permission on this action.';
			die;
		}
		$arr_return = array();
		if($field=='salesaccount_name'){
			$receipt = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())),array('company_id','amount_received'));
			$arr_value = (array)json_decode($value);
			$salesaccount_id = (array)$arr_value['_id'];
			$salesaccount_id = $salesaccount_id['$id'];
			$arr_return['salesaccount_id'] = new MongoId($salesaccount_id);
			$this->selectModel('Salesaccount');
			if(isset($arr_value['company_id'])&&is_object($arr_value['company_id'])){
				//Nếu có company cũ
				if(isset($receipt['company_id']))
					$this->Salesaccount->update_account($receipt['company_id'], array(
													'model' => 'Company',
													'balance' => (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0) ,
													'receipts' => -(isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0),
													));
				$company_id = (array)$arr_value['company_id'];
				$company_id = $company_id ['$id'];
				$arr_return['company_id'] = $company_id;
				$this->Salesaccount->update_account($arr_return['company_id'], array(
													'model' => 'Company',
													'balance' => -(isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0) ,
													'receipts' => (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0),
													));

				$arr_return['salesaccount_name'] = $arr_return['company_name'] = $this->get_name('Company',$arr_return['company_id'],'name');
				$arr_return['company_id'] = new MongoId($arr_return['company_id']);
			}
			return $arr_return;
		} else if($field=='allocation') {
			$this->selectModel('Salesinvoice');
			$receipt = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())),array('amount_received','allocation'));
			if($field_change == 'salesinvoice_code'){
				$salesinvoice_code = $value[$ids]['salesinvoice_code'];
				$invoice = $this->Salesinvoice->select_one(array('code'=>$salesinvoice_code),array('total_receipt','sum_amount','company_id','sum_sub_total','taxval'));
				$value[$ids]['salesinvoice_id'] = $invoice['_id'];
				$this->selectModel('Company');
				$minimum = 50;
		        $this->selectModel('Stuffs');
		        $product = $this->Stuffs->select_one(array('value'=>"Minimun Order Adjustment"),array('product_id'));
		        $product_id = $product['product_id'];
		        if(isset($product['product_id'])&&is_object($product['product_id'])){
		            $this->selectModel('Product');
		            $product = $this->Product->select_one(array('_id'=> new MongoId($product_id)),array('sell_price'));
		            $minimum = $product['sell_price'];
		            $product_id = $product['_id'];
		        }
				if(is_object($invoice['company_id'])){
					$company_id = $invoice['company_id'];
					$company = $this->Company->select_one(array('_id'=>$company_id),array('pricing'));
					if(isset($company['pricing'])){
						foreach($company['pricing'] as $pricing){
							if(isset($pricing['deleted'])&&$pricing['deleted']) continue;
							if((string)$pricing['product_id']!=(string)$product_id) continue;
							if(!isset($pricing['price_break']) || empty($pricing['price_break'])) continue;
							$price_break = reset($pricing['price_break']);
							$minimum =  (float)$price_break['unit_price']; break;
						}
					}
				}
				if($invoice['sum_sub_total']<$minimum){
					$invoice['taxval'] = (isset($invoice['taxval']) ? $invoice['taxval'] : 0);
					$invoice['sum_amount'] = $minimum + ($minimum*$invoice['taxval']/100);
				}
				$value[$ids]['amount'] = $invoice['sum_amount'];
				if($value[$ids]['mod'] == 'Fully')
					$invoice['invoice_status'] = 'Paid';
				else if($value[$ids]['mod'] == 'Part'
				   	&&isset($value[$ids]['salesinvoice_id']) && is_object($value[$ids]['salesinvoice_id'])){
					$total_amount = 0;
					foreach($value as $allocation){
						if(isset($allocation['deleted']) && $allocation['deleted']) continue;
						if(!isset($allocation['salesinvoice_id']) || !is_object($allocation['salesinvoice_id'])) continue;
						if($allocation['salesinvoice_id'] != $value[$ids]['salesinvoice_id']) continue;
						$total_amount += (isset($allocation['amount']) ? (float)$allocation['amount'] : 0);
					}
					if($total_amount >= $invoice['sum_amount']
					   	&&isset($value[$ids]['amount']) && $value[$ids]['amount'] > 0){
						$value[$ids]['amount'] = $invoice['sum_amount'] - $value[$ids]['amount'];
						$invoice['invoice_status'] = 'Paid';
					}
				}
				if(isset($receipt['allocation'][$ids]['salesorder_code'])
				   &&is_object($receipt['allocation'][$ids]['salesorder_code'])
				   &&$receipt['allocation'][$ids]['salesorder_code']!=$value[$ids]['salesorder_code']){
					$old_invoice = $this->Salesinvoice->select_one(array('_id'=>new MongoId($receipt['allocation'][$ids]['salesorder_id'])),array('total_receipt'));
					$old_invoice['total_receipt'] -= (float)$receipt['allocation'][$ids]['amount'];
					$this->Salesinvoice->save($old_invoice);
				}
				if(!isset($invoice['total_receipt']))
					$invoice['total_receipt'] = (float)$value[$ids]['amount'];
				else
					$invoice['total_receipt'] += (float)$value[$ids]['amount'];
				$this->Salesinvoice->save($invoice);
				$arr_return[$field] = $value;
			} else if($field_change == 'amount'){
				if(isset($value[$ids]['salesinvoice_id'])&&is_object($value[$ids]['salesinvoice_id'])){
					$invoice = $this->Salesinvoice->select_one(array('_id'=>new MongoId($value[$ids]['salesinvoice_id'])),array('total_receipt','sum_amount','company_id','taxval','sum_sub_total'));
					if(!isset($invoice['total_receipt']))
						$invoice['total_receipt'] = (float)$value[$ids]['amount'] - (isset($receipt['allocation'][$ids]['amount']) ? (float)$receipt['allocation'][$ids]['amount'] : 0);
					else
						$invoice['total_receipt'] += (float)$value[$ids]['amount'] - (isset($receipt['allocation'][$ids]['amount']) ? (float)$receipt['allocation'][$ids]['amount'] : 0);
					$this->Salesinvoice->save($invoice);

					$this->selectModel('Company');
					$minimum = 50;
			        $this->selectModel('Stuffs');
			        $product = $this->Stuffs->select_one(array('value'=>"Minimun Order Adjustment"),array('product_id'));
			        $product_id = $product['product_id'];
			        if(isset($product['product_id'])&&is_object($product['product_id'])){
			            $this->selectModel('Product');
			            $product = $this->Product->select_one(array('_id'=> new MongoId($product_id)),array('sell_price'));
			            $minimum = $product['sell_price'];
			            $product_id = $product['_id'];
			        }
					if(is_object($invoice['company_id'])){
						$company_id = $invoice['company_id'];
						$company = $this->Company->select_one(array('_id'=>$company_id),array('pricing'));
						if(isset($company['pricing'])){
							foreach($company['pricing'] as $pricing){
								if(isset($pricing['deleted'])&&$pricing['deleted']) continue;
								if((string)$pricing['product_id']!=(string)$product_id) continue;
								if(!isset($pricing['price_break']) || empty($pricing['price_break'])) continue;
								$price_break = reset($pricing['price_break']);
								$minimum =  (float)$price_break['unit_price']; break;
							}
						}
					}
					$sum_amount = $invoice['sum_amount'];
					if($invoice['sum_sub_total']<$minimum){
						$taxval = (isset($taxval) ? $taxval : 0);
						$sum_amount =  $minimum + ($minimum*$taxval/100);
					}
					if($value[$ids]['mod'] == 'Part'){
						$total_amount = 0;
						foreach($value as $allocation){
							if(isset($allocation['deleted']) && $allocation['deleted']) continue;
							if(!isset($allocation['salesinvoice_id']) || !is_object($allocation['salesinvoice_id'])) continue;
							if($allocation['salesinvoice_id'] != $value[$ids]['salesinvoice_id']) continue;
							$total_amount += (isset($allocation['amount']) ? (float)$allocation['amount'] : 0);
						}
						if($total_amount >= $invoice['sum_amount']
						   	&&isset($value[$ids]['amount']) && $value[$ids]['amount'] > 0){
							$value[$ids]['amount'] = $invoice['sum_amount'] - ($total_amount - $value[$ids]['amount']);
							$invoice['invoice_status'] = 'Paid';
							$this->Salesinvoice->save($invoice);
						}
					}
				}
			} else if($field_change == 'outstanding'){
				if($value[$ids]['mod'] == 'Fully'){
					$invoice = $this->Salesinvoice->select_one(array('_id'=>$value[$ids]['salesinvoice_id']),array('total_receipt','sum_sub_total','sum_amount','company_id','taxval'));
					$invoice['invoice_status'] = 'Paid';
					$this->selectModel('Company');
					$minimum = 50;
			        $this->selectModel('Stuffs');
			        $product = $this->Stuffs->select_one(array('value'=>"Minimun Order Adjustment"),array('product_id'));
			        $product_id = $product['product_id'];
			        if(isset($product['product_id'])&&is_object($product['product_id'])){
			            $this->selectModel('Product');
			            $product = $this->Product->select_one(array('_id'=> new MongoId($product_id)),array('sell_price'));
			            $minimum = $product['sell_price'];
			            $product_id = $product['_id'];
			        }
					if(is_object($invoice['company_id'])){
						$company = $this->Company->select_one(array('_id'=>$invoice['company_id']),array('pricing'));
						if(isset($company['pricing'])){
							foreach($company['pricing'] as $pricing){
								if(isset($pricing['deleted'])&&$pricing['deleted']) continue;
								if((string)$pricing['product_id']!=(string)$product_id) continue;
								if(!isset($pricing['price_break']) || empty($pricing['price_break'])) continue;
								$price_break = reset($pricing['price_break']);
								$minimum =  (float)$price_break['unit_price']; break;
							}
						}
					}
					if($invoice['sum_sub_total']<$minimum){
						$invoice['taxval'] = (isset($invoice['taxval']) ? $invoice['taxval'] : 0);
						$invoice['sum_amount'] = $minimum + ($minimum*$invoice['taxval']/100);
					}
					$value[$ids]['amount'] = $invoice['sum_amount'];
					if(!isset($invoice['total_receipt']))
						$invoice['total_receipt'] = (float)$value[$ids]['amount'];
					else
						$invoice['total_receipt'] += (float)$value[$ids]['amount'];
					$this->Salesinvoice->save($invoice);
					$arr_return[$field] = $value;
				}
			}
			$total_allocated = $unallocated = 0;
			$receipt['allocation'] = $value;
			foreach($receipt['allocation'] as $allocation){
				if(isset($allocation['deleted']) && $allocation['deleted']) continue;
				$total_allocated += (isset($allocation['amount']) ? (float)$allocation['amount'] : 0);
			}
			$unallocated = (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0);
			$unallocated -= $total_allocated;
			$arr_return['total_allocated'] = $total_allocated;
			$arr_return['unallocated'] = $unallocated;
		} else if($field == 'our_csr'){
			 $arr_return['our_csr_id'] = new MongoId($ids);
		}else if($field == 'our_rep'){
			$arr_return['our_rep_id'] = new MongoId($ids);
		}
		$arr_return[$field] = $value;
		return $arr_return;
	}
	function ajax_save(){
		if (isset($_POST['field']) && $_POST['field'] == "amount_received"){
			$amount_received = $_POST['value'];
			$receipt = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())),array('amount_received','company_id','total_allocated'));
			$receipt['unallocated'] = $amount_received - (isset($receipt['total_allocated']) ? $receipt['total_allocated'] : 0);
			$this->opm->save($receipt);
			$this->selectModel('Salesaccount');
			if(isset($receipt['company_id']) && is_object($receipt['company_id'])){
				$this->Salesaccount->update_account($receipt['company_id'], array(
												'model' 	=> 'Company',
												'balance' 	=> $amount_received - (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0) ,
												'receipts' 	=> $amount_received - (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0),
												));
			}
		}
		parent::ajax_save();
	}
	function delete_allocation(){
		if(isset($_POST['allocation_key'])){
			$allocation_key = $_POST['allocation_key'];
			$receipt = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())),array('allocation','amount_received'));
			$salesinvoice_id = $receipt['allocation'][$allocation_key]['salesinvoice_id'];
			$this->selectModel('Salesinvoice');
			$salesinvoice = $this->Salesinvoice->select_one(array('_id'=>$salesinvoice_id),array('total_receipt'));
			$salesinvoice['total_receipt'] -= $receipt['allocation'][$allocation_key]['amount'];
			if(isset($receipt['allocation'][$allocation_key]['mod']) && $receipt['allocation'][$allocation_key]['mod'] == "Fully")
				$salesinvoice['invoice_status']  = 'Invoiced';
			else {
				if($receipt['allocation'][$allocation_key]['amount'] > 0){
					$salesinvoice['invoice_status']  = 'Invoiced';
				} else {
					$empty = true;
					foreach($receipt['allocation'] as $allocation){
						if(isset($allocation['deleted'])&&$allocation['deleted']) continue;
						if($allocation['salesinvoice_id']==$salesinvoice_id){
							$empty = false;
							break;
						}
					}
					if($empty)
						$salesinvoice['invoice_status']  = 'Invoiced';
				}
			}
			$total_allocated = $unallocated = 0;
			foreach($receipt['allocation'] as $allocation){
				if(isset($allocation['deleted']) && $allocation['deleted']) continue;
				$total_allocated += (isset($allocation['amount']) ? (float)$allocation['amount'] : 0);
			}
			$unallocated = (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0);
			$unallocated -= $total_allocated;
			$salesinvoice['total_receipt'] -= (isset($receipt['allocation'][$allocation_key]['amount']) ? (float)$receipt['allocation'][$allocation_key]['amount'] : 0);
			$receipt['allocation'][$allocation_key] = array('deleted'=>true);
			if($this->Salesinvoice->save($salesinvoice)){
				if($this->opm->save($receipt)){
					echo json_encode(array('total_allocated'=>$total_allocated,'unallocated'=>$unallocated));
				}
			}
		}
		die;
	}
	public function save_unallocated()
	{
		if(!$this->check_permission($this->name.'_@_entry_@_edit') || !$this->check_permission($this->name.'_@_entry_@_add')){
			echo 'You do not have permission on this action.';
			die;
		}
		if(isset($_POST['unallocated']))
		{
			if(is_numeric($_POST['unallocated']))
			{
				$unallocated = (float)$_POST['unallocated'];
				$this->selectModel('Receipt');
				$receipt = $this->Receipt->select_one(array('_id'=> new MongoId($this->get_id())));
				$receipt['unallocated'] = $unallocated;
				if($this->Receipt->save($receipt))
					echo 'ok';
				else
					echo 'error';

			}
			else
				echo 'is not numeric';
		}
		die;
	}

	public function check_condition_receipt($email=false)
	{
		$receipt = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
		if(!isset($receipt['allocation']) || !is_object($receipt['company_id']))
			echo json_encode(array('status'=>'error','message'=>'No allocation have been entered on this receipt yet.'));
		else if(!isset($receipt['unallocated']) || $receipt['unallocated']!= 0)
			echo json_encode(array('status'=>'error','message'=>'The amount receipt does not balance with the total allocated.'));
		else if(!$email)
			echo json_encode(array('status'=>'ok','url'=>$this->print_receipt($receipt)));
		else
			$this->print_receipt($receipt,'',true);
		die;
	}
	public function loading()
	{

	}
	public function print_receipts()
	{
		if(!$this->check_permission($this->name.'_@_options_@_print_receipts'))
			die;
		$file = array();
		$i = 0;
		$receipts = $this->opm->select_all(array('arr_where'=>array('allocation'=>array('$ne'=>''))));
		foreach($receipts as $value)
		{
			if(!empty($value['allocation']))
			{
				$i++;
				$file[] = '<a style="text-decoration:none; color: #b22626;font-weight: bold;font-size: 15px;line-height:25px;font-family: Arial;" target="_blank" href="'.$this->print_receipt($value,'print_receipts').'">Receipt Report '.$i.'</a><br />';
			}

		}
		echo json_encode($file);
		die;
	}
	public function view_pdf($getfile=true){
		$this->autoRender = false;
		$receipt = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
		return $this->print_receipt($receipt,'',$getfile);
	}
	public function print_receipt($receipt=array(),$option='',$getfile=false)
	{
		if(!$this->check_permission($this->name.'_@_options_@_print_receipt'))
			die;
		if(!empty($receipt))
		{
			$group = array();
			$this->selectModel('Salesinvoice');
			$this->selectModel('Company');
			$company = $this->Company->select_one(array('_id'=> new MongoId($receipt['company_id'])));
			$total = 0;
			if(!empty($receipt['allocation']))
			{
				foreach($receipt['allocation'] as $key=>$value)
				{
					if(isset($value['deleted'])&&$value['deleted']==true) continue;
					if(isset($value['salesinvoice_id'])&&is_object($value['salesinvoice_id']))
					{
						$amount = (isset($value['amount'])&&$value['amount']!='' ? $value['amount']: 0);
						$total += $amount;
						$si = $this->Salesinvoice->select_one(array('_id'=>new MongoId($value['salesinvoice_id'])));
						$group[$key]['date'] = (isset($si['invoice_date'])&&$si['invoice_date']!='' ? date('M d, Y',$si['invoice_date']->sec) : '');
						$group[$key]['code'] = $si['code'];
						$group[$key]['type'] = $si['invoice_type'];
						$group[$key]['po_no'] = '';
						$group[$key]['amount'] = (isset($si['sum_amount'])&&$si['sum_amount']!='' ? number_format($si['sum_amount'],2) : 0);
						$group[$key]['paid'] = number_format($amount,2);
					}
				}
				if($option=='print_receipts')
					$group['total'] = number_format($total,2);
				else
					$group['total'] = (isset($receipt['amount_received'])&&$receipt['amount_received']!= '' ? number_format($receipt['amount_received'],2) : 0);
				$group['account_balance'] = (isset($company['account']['balance'])&&$company['account']['balance']!='' ? number_format($company['account']['balance'],2,'.','.') : 0);
				$html_loop = '<table cellpadding="3" cellspacing="0" class="maintb">
								<tr>
			                     <td width="15%" class="first top">
			                        Date
			                     </td>
			                     <td width="10%" class="top">
			                        Ref #
			                     </td>
			                     <td width="15%" class="top" align="left">
			                        Type
			                     </td>
			                     <td width="10%" class="top" align="left">
			                        PO no
			                     </td>
			                     <td width="25%" class="top" align="right">
			                        Amount
			                     </td>
			                     <td width="25%" colspan="3" class="end top" align="right">
			                        Paid
			                     </td>
			                  </tr>
				';
				$i = 0;
				foreach($group as $value)
				{
					if(is_array($value))
					{
						$color = ($i % 2 == 0 ? '#fdfcfa': '#eeeeee');
						$html_loop .= '
							<tr style="background-color:'.$color.'">
								<td class="first content">'.$value['date'].'</td>
								<td class="content">'.$value['code'].'</td>
								<td class="content">'.$value['type'].'</td>
								<td class="content" align="right">'.$value['po_no'].'</td>
								<td class="content" align="right">'.$value['amount'].'</td>
								<td colspan="3" class="content" align="right">'.$value['paid'].'</td>
							</tr>
						';
						$i++;
					}
				}
				$color = ($i % 2 == 0 ? '#fdfcfa': '#eeeeee');
				$html_loop .= '
						<tr style="background-color:'.$color.'">
							<td class="first" colspan="5" align="right"><strong>Total:</strong></td>
							<td class="end" colspan="3" align="right">'.$group['total'].'</td>
						</tr>';
				$html_loop .='
						<tr style="background-color:'.$color.'">
							<td class="first bottom" colspan="2" align="left">'.$i.' record(s) listed.</td>
							<td class="bottom" colspan="3" align="right"><strong>Account Balance:</strong></td>
							<td class="end bottom" colspan="3" align="right">'.$group['account_balance'].'</td>
						</tr>
					</table>';
				//========================================
				$pdf['customer_name'] = '<span style="color:#b32017">'.$company['name'].'</span>';
				$address_key = (isset($company['addresses_default_key'])? $company['addresses_default_key'] : 0);
				$address_tmp = (isset($company['addresses'][$address_key]) ?  $company['addresses'][$address_key] : '');
				$address='';
				if($address_tmp!='')
				{
					$address = ($address_tmp['address_1']!=''? $address_tmp['address_1'].' ' : '').($address_tmp['address_2']!=''?$address_tmp['address_2'].' ' : '').($address_tmp['address_3']!=''? $address_tmp['address_3'].', ' : '').($address_tmp['province_state']!='' ? $address_tmp['province_state'].', ': '').($address_tmp['town_city']!='' ? $address_tmp['town_city'].', ':'').$address_tmp['country'];
				}
				$pdf['customer_address'] = $address;
		        $pdf['current_time'] = date('h:i a m/d/Y');
		        $pdf['title'] = '<span style="color:#b32017">R</span>eceipt <span style="color:#b32017">R</span>eport';
		        $this->layout = 'pdf';
		        //set header
		        $pdf['logo_link'] = 'img/logo_anvy.jpg';
		        $pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
		        $pdf['html_loop'] = $html_loop;
		        $pdf['filename'] = 'RE_' . md5($pdf['current_time'].$company['_id']);
		        $pdf['code'] = $receipt['code'];
		        $pdf['date'] = date('M d, Y',$receipt['receipt_date']->sec);
		        $this->report_pdf($pdf);
		        if($getfile)
		        	return $pdf['filename'].'.pdf';
		        return URL.'/upload/' . $pdf['filename'] . '.pdf';
	    	}
		}
		return URL.'/receipts/entry';
		die;
	}
	public function update_receipt_salesaccount(){
		if(isset($_POST['salesaccount_id'])){
			$value = (float)$_POST['value'];
			$receipt = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
			$this->selectModel('Salesaccount');
			//Nếu là company
			if(isset($receipt['company_id']))
				$this->Salesaccount->update_account($receipt['company_id'], array(
												'model' => 'Company',
												'balance' => $value - (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0) ,
												'receipts' => $value - (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0),
												));
			else if(isset($receipt['contact_id'])) //Nếu là contact
				$this->Salesaccount->update_account($arr_return['contact_id'], array(
												'model' => 'Contact',
												'balance' => $value -  (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0) ,
												'receipts' => $value - (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0),
												));
			echo 'ok';

		}
		die;
	}
	public function update_receipt_salesinvoice()
	{
		if(isset($_POST['si_id']))
		{
			$this->selectModel('Salesinvoice');
			$si = $this->Salesinvoice->select_one(array('_id'=>new MongoId($_POST['si_id'])));
			if($_POST['type']=='plus')
			{
				if(!isset($si['total_receipt']))
					$si['total_receipt'] = (float)$_POST['value'];
				else
					$si['total_receipt'] = $si['total_receipt'] - (float)$_POST['old'] + (float)$_POST['value'];
			}
			else if ($_POST['type']=='minus'){
				$si['total_receipt'] -= $_POST['value'];
			}
			else if($_POST['type']=='update')
			{
				$receipt = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())));
				if(isset($_POST['key'])&&is_numeric($_POST['key']))
				{
					$old = (isset($receipt['allocation'][$_POST['key']]['amount'])&&is_numeric($receipt['allocation'][$_POST['key']]['amount']) ? $receipt['allocation'][$_POST['key']]['amount'] : 0);
					$si['total_receipt'] = $si['total_receipt'] - (float)$old + (float)$_POST['value'];
				}
			}

			if( $this->Salesinvoice->save($si) ){
				echo 'ok';die;
			}
		}
		die;
	}
	public function delete_all_associate($idopt='',$key=''){
		if(!$this->check_permission($this->name.'_@_entry_@_delete')){
			echo 'You do not have permission on this action.';
			die;
		}
		$ids = $this->get_id();
		$receipt = $this->opm->select_one(array('_id'=>new MongoId($ids)),array('amount_received','company_id'));
		$this->selectModel('Salesaccount');
		if(is_object($receipt['company_id']) && isset($receipt['amount_received'])){
			if($receipt['amount_received']=='')
				$receipt['amount_received'] = 0;
			$this->Salesaccount->update_account($receipt['company_id'], array(
												'model' => 'Company',
												'balance' =>  - (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0) ,
												'receipts' =>  - (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0),
												));
		}
	}
	public function report_pdf($data)
	{
		App::import('Vendor', 'xtcpdf');
        $pdf = new XTCPDF();
        $textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Anvy Digital');
        $pdf->SetTitle('Anvy Digital Quotation');
        $pdf->SetSubject('Quotation');
        $pdf->SetKeywords('Quotation, PDF');

        // set default header data
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(2);

        // set margins
        $pdf->SetMargins(10, 3, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------
        // set font
        $pdf->SetFont($textfont, '', 9);

        // add a page
        $pdf->AddPage();


        // writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
        // writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
        // create some HTML content


        $html = '
        <table cellpadding="2" cellspacing="0" style="width:100%; margin: 0px auto">
           <tbody>
              <tr>
                 <td width="32%" valign="top" style="color:#1f1f1f;">
                    <img src="img/logo_anvy.jpg" alt="" margin-bottom:0px>
                    <p style="margin-bottom:5px; margin-top:0px;">Unit 103, 3016 - 10th Ave NE<br/ >Calgary  AB  T2A  6A3</p>
                 </td>
                 <td width="68%" valign="top" align="right">
                    <table>
                       <tbody>
                          <tr>
                             <td width="25%">&nbsp;</td>
                             <td width="75%">
                                <span style="text-align:right; font-size:21px; font-weight:bold; color: #919295;">
                                    ' . $data['title'];
        if (isset($data['date_equals']))
            $date = '<br /><span style="font-size:12px; font-weight:normal">' . $data['date_equals'] . '</span>';
        else {
            if (isset($data['date_from']) && isset($data['date_to']))
                $date = '<br /><span style="font-size:12px; font-weight:normal">( ' . $data['date_from'] . ' - ' . $data['date_to'] . ' )</span>';
            else if (isset($data['date_from']))
                $date = '<br /><span style="font-size:12px; font-weight:normal">From ' . $data['date_from'] . '</span>';
            else if (isset($data['date_to']))
                $date = '<br /><span style="font-size:12px; font-weight:normal">To ' . $data['date_to'] . '</span>';
            else
                $date = '';
        }
        $html .= $date;
        $html .= '
                                </span>
                                <div style=" border-bottom: 1px solid #cbcbcb;height:5px;width:50%">&nbsp;</div>
                             </td>
                          </tr>
                          <tr>
                             <td colspan="2">
                                    <span style="font-weight:bold;">Printed at: </span>' . $data['current_time'] . '
                             </td>
                          </tr>
                       </tbody>
                    </table>
                 </td>
              </tr>';
        if(isset($data['customer_name'])&&$data['customer_name']!='')
        	$html .= '<tr>
        				<td width="50%"><div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>'.$data['customer_name'].'<br />'.(isset($data['customer_address']) ? $data['customer_address'] : ''
        				).'</td>
        				<td width="50%" align="right"><div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>'.(isset($data['code'])? 'Ref no: '.$data['code'].'<br />' : '').(isset($data['date']) ? 'Date: '.$data['date'] : '').'</td>
        			</tr>';
        $html .='</tbody>
        </table>';
        if(isset($data['heading'])&&$data['heading']!='')
        	$html .=	'<div class="option">' . $data['heading'] . '</div>
					        <br />
					        <br />';
        $html .= '<div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>
        <br />
        <style>
           td{
           line-height:2px;
           }
           td.first{
            text-align: center;
           border-left:1px solid #e5e4e3;
           }
           td.end{
           border-right:1px solid #e5e4e3;
           }
           td.top{
           color:#fff;
           text-align: center;
           font-weight:bold;
           background-color:#911b12;
           border-top:1px solid #e5e4e3;
           }
           td.bottom{
           border-bottom:1px solid #e5e4e3;
           }
           td.content{
            border-right: 1px solid #E5E4E3;
            text-align: center;
           }
           .option{
           color: #3d3d3d;
           font-weight:bold;
           font-size:20px;
           text-align: center;
           width:100%;
           }
           table.maintb{
           }
        </style>
        <br />
        ';
        $html .= $data['html_loop'];

        $pdf->writeHTML($html, true, false, true, false, '');



        // reset pointer to the last page
        $pdf->lastPage();



        // ---------------------------------------------------------
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        //$pdf->Output('example_001.pdf', 'I');




        $pdf->Output('upload/' . $data['filename'] . '.pdf', 'F');
	}
	public function payment_receive_report()
	{
		if(!$this->check_permission($this->name.'_@_options_@_payment_receive_report'))
			die;
		$arr_data = array();
		$arr_data['receipts_paid_by'] = $this->Setting->select_option_vl(array('setting_value' => 'receipts_paid_by'));
		if(isset($_POST)&&!empty($_POST))
		{
			$data = array();
			$arr_where = array();
			parse_str($_POST['data'],$data);
			$data = $this->Common->strip_search($data);
			if(isset($data['company_id'])&&strlen($data['company_id'])==24)
				$arr_where['company_id'] = new MongoId($data['company_id']);
			if(isset($data['company'])&&$data['company']!='')
				$arr_where['company_name'] = new MongoRegex('/'.trim($data['company']).'/i');
			//tim chinh xac ngay
            if (isset($data['date_equals'])&&$data['date_equals'] != '') {
                $date_equals = $data['date_equals'];
                $date_equals = new MongoDate(strtotime(date('Y-m-d', strtotime($date_equals))));
                $date_equals_to = new MongoDate($date_equals->sec + DAY - 1);
                $arr_where['receipt_date']['$gte'] = $date_equals;
                $arr_where['receipt_date']['$lt'] = $date_equals_to;
            }
            //ngay nam trong khoang
            else if (isset($data['date_equals'])&&$data['date_equals'] == '') {
                //neu chi nhap date from
                if (isset($data['date_from'])&&$data['date_from']!='') {
                    $arr_where['receipt_date']['$gte'] =  new MongoDate(strtotime(date('Y-m-d', strtotime($data['date_from']))));
                }
                //neu chi nhap date to
                if (isset($data['date_to'])&&$data['date_to']!='') {
                    $date_to = new MongoDate(strtotime(date('Y-m-d', strtotime($data['date_to']))));
                    $date_to = new MongoDate($date_to->sec + DAY - 1);
                    $arr_where['receipt_date']['$lte'] = $date_to;
                }
            }
            if(isset($data['amount_received'])&&$data['amount_received']!='')
            	$arr_where['amount_received'] = trim($data['amount_received']);
            if(isset($data['paid_by_id'])&&$data['paid_by_id']!='')
            	$arr_where['paid_by'] = $data['paid_by_id'];
            if(isset($data['notes'])&&$data['notes']!='')
            	$arr_where['notes'] = $data['notes'];
            if(isset($data['employee'])&&$data['employee']!='')
			{
				$arr_where['$or'][]['our_rep'] = new MongoRegex('/'.trim($data['employee']).'/i');
				$arr_where['$or'][]['our_csr'] = new MongoRegex('/'.trim($data['employee']).'/i');
			}
			if(isset($data['employee_id'])&&$data['employee_id']!='')
			{
				$arr_where['$or'][]['our_rep_id'] = new MongoRegex('/'.trim($data['employee_id']).'/i');
				$arr_where['$or'][]['our_csr_id'] = new MongoRegex('/'.trim($data['employee_id']).'/i');
			}
			if($data['allocation_amount']!=''
				||$data['sales_invoice_no']!=''
				||isset($data['write_off'])&&$data['write_off']!=0
				||isset($data['not_write_off'])&&$data['not_write_off']!=0)
			{
				$arr_where['allocation']['$elemMatch']['deleted'] = false;
				if($data['allocation_amount']!='')
					$arr_where['allocation']['$elemMatch']['amount'] = trim($data['allocation_amount']);
				if($data['sales_invoice_no'])
					$arr_where['allocation']['$elemMatch']['salesinvoice_code'] = trim($data['sales_invoice_no']);
				if(isset($data['write_off'])&&$data['write_off']!=0)
					$arr_where['allocation']['$elemMatch']['write_off'] = 1;
				else if(isset($data['not_write_off'])&&$data['not_write_off']!=0)
					$arr_where['allocation']['$elemMatch']['write_off'] = 0;
			}
			$arr_where['allocation']['$ne'] = '';
			$receipts = $this->opm->select_all(array(
								'arr_where'=>$arr_where,
								'arr_order'=>array('_id'=>1),
								'arr_field'=>array('_id','allocation','company_name','reference','receipt_date')
				));
			if($receipts->count()==0)
			{
				echo 'empty';
				die;
			}
			else
			{
				$i = 0;
				$total_amount = 0;
				$html_loop = '
							<table cellpadding="3" cellspacing="0" class="maintb">
			                  <tr>
			                     <td width="15%" class="first top">
			                        Date
			                     </td>
			                     <td width="9%" class="top">
			                        Inv #
			                     </td>
			                     <td width="20%" class="top" align="left">
			                        Customer
			                     </td>
			                     <td width="11%" class="top" align="left">
			                        Reference
			                     </td>
			                     <td width="20%" class="top" align="left">
			                        Notes allocation
			                     </td>
			                     <td width="10%" class="top">
			                        Write off
			                     </td>
			                     <td width="15%" colspan="3" class="end top" align="right">
			                        Amount
			                     </td>
			                  </tr>
				';
				foreach($receipts as $value)
				{
					if(!empty($value['allocation']))
					foreach($value['allocation'] as $val)
					{
						//allocation đã xóa ko cần kiểm tra
						if(isset($val['deleted'])&&$val['deleted'])continue;
						// nếu người dùng search allocation_amount
						if($data['allocation_amount']!=''){
							if( $data['allocation_amount']!=$val['amount'] )continue;
						}

						// nếu người dùng search salesinvoice_code
						if($data['sales_invoice_no']!=''){
							if( $data['sales_invoice_no']!=$val['salesinvoice_code'] )continue;
						}

						// nếu người dùng search write_off
						if(isset($data['write_off'])&&$data['write_off']){
							if( !$val['write_off'] )continue;
						}
						if(isset($data['not_write_off'])&&$data['not_write_off']){
							if($val['write_off'])continue;
						}
						$bg = ( $i % 2 == 0? 'background-color:#eeeeee':'background-color:#fdfcfa');
						$amount = (isset($val['amount'])&&$val['amount']!='' ? $val['amount'] : 0);
						$total_amount += $amount;
						$html_loop .='
							<tr style="'.$bg.'">
								<td class="first content">'.date('M d, Y',$value['receipt_date']->sec).'</td>
								<td class="content">'.(isset($val['salesinvoice_code']) ? $val['salesinvoice_code'] : '').'</td>
								<td class="content" align="left">'.(isset($value['company_name']) ? $value['company_name'] : '').'</td>
								<td class="content" align="left">'.(isset($val['reference']) ? $val['reference'] : '').'</td>
								<td class="content" align="left">'.(isset($val['note']) ? $val['note'] : '').'</td>
								<td class="content">'.(isset($val['write_off'])&&$val['write_off']==1 ? '<strong>X</strong>' : '').'</td>
								<td class="end content" colspan="3" align="right">'.number_format($amount,2).'</td>
							</tr>
							';
						$i++;
					}
				}
				$bg = ( $i % 2 == 0? 'background-color:#eeeeee':'background-color:#fdfcfa');
				$html_loop .='	<tr style="'.$bg.'">
									<td colspan="3" class="first bottom" align="left">'.$i.' record(s) listed</td>
									<td colspan="3" class="bottom" align="right">
										<span style="font-weight:bold; padding-left:20px">Total:</span>
									</td>
									<td colspan="3" class="end bottom" align="right">'.number_format($total_amount,2).'</td>
								</tr>
							</table>';
				//========================================
		        $pdf['current_time'] = date('h:i a m/d/Y');
		        $pdf['title'] = '<span style="color:#b32017">S</span>ales <span style="color:#b32017">R</span>eceipt <span style="color:#b32017">L</span>isting';
		        $this->layout = 'pdf';
		        //set header
		        $pdf['logo_link'] = 'img/logo_anvy.jpg';
		        $pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
		        $pdf['heading'] = $data['heading'];
		        if (isset($data['date_equals']) && $data['date_equals'] != '') {
		            $pdf['date_equals'] = $data['date_equals'];
		        } else {
		            if (isset($data['date_from']) && $data['date_from'] != '')
		                $pdf['date_from'] = $data['date_from'];
		            if (isset($data['date_to']) && $data['date_to'] != '')
		                $pdf['date_to'] = $data['date_to'];
		        }
		        $pdf['html_loop'] = $html_loop;
		        $pdf['filename'] = 'RE_' . md5($pdf['current_time']);

		        $this->report_pdf($pdf);
		        echo URL.'/upload/' . $pdf['filename'] . '.pdf';
				die;
			}
		}
		else
			$this->set('arr_data',$arr_data);
	}
	public function customer_summary_report()
	{
		if(!$this->check_permission($this->name.'_@_options_@_customer_summary_report'))
			die;
		$receipts = $this->opm->select_all(array(
						'arr_order'=>array('_id'=>1),
						'arr_field'=>array('_id','company_id','company_name','amount_received')
		));
		if($receipts->count()>0)
		{
			$i = 0;
			$group =array();
			$total_amount = 0;
			$html_loop = '
						<table cellpadding="3" cellspacing="0" class="maintb">
		                  <tr>
		                     <td width="30%" class="first top" align="left">
		                        Customer
		                     </td>
		                     <td width="45%" class="top" align="left">
		                        Address
		                     </td>
		                     <td width="25%" colspan="3" class="end top" align="right">
		                        Total
		                     </td>
		                  </tr>
			';
			$this->selectModel('Company');
			foreach($receipts as $value)
			{
				if(isset($value['company_id'])&&is_object($value['company_id']))
				{
					$company = $this->Company->select_one(array('_id'=>new MongoId($value['company_id'])),array('addresses','addresses_default_key'));
					$address_key = (isset($company['addresses_default_key'])? $company['addresses_default_key'] : 0);
					$address_tmp = (isset($company['addresses'][$address_key]) ?  $company['addresses'][$address_key] : '');
					if($address_tmp!='')
					{
						$address = ($address_tmp['address_1']!=''? $address_tmp['address_1'].' ' : '').($address_tmp['address_2']!=''?$address_tmp['address_2'].' ' : '').($address_tmp['address_3']!=''? $address_tmp['address_3'].', ' : '').($address_tmp['province_state']!='' ? $address_tmp['province_state'].', ': '').($address_tmp['town_city']!='' ? $address_tmp['town_city'].', ':'').$address_tmp['country'];
					}
					else
						$address = '';
					$id = (string)$value['company_id'];
					$amount_received = (isset($value['amount_received'])&&$value['amount_received']!= '' ? $value['amount_received'] : 0);
					$group[$id]['company_name'] = $value['company_name'];
					$group[$id]['address'] = $address;
					if(!isset($group[$id]['total']))
						$group[$id]['total'] = $amount_received;
					else
						$group[$id]['total'] += $amount_received;
					$total_amount += $amount_received;
				}

			}
			if(!empty($group))
			{
				foreach($group as $value)
				{
					$bg = ( $i % 2 == 0? 'background-color:#eeeeee':'background-color:#fdfcfa');
					$html_loop .='
						<tr style="'.$bg.'">
							<td class="first content" align="left">'.$value['company_name'].'</td>
							<td class="content" align="left">'.$value['address'].'</td>
							<td class="end content" colspan="3" align="right">'.number_format($value['total'],2).'</td>
						</tr>
						';
					$i++;
				}
				$bg = ( $i % 2 == 0? 'background-color:#eeeeee':'background-color:#fdfcfa');
				$html_loop .='	<tr style="'.$bg.'">
									<td class="first bottom" align="left">'.$i.' record(s) listed</td>
									<td class="bottom" align="right">
										<span style="font-weight:bold; padding-left:20px">Total:</span>
									</td>
									<td colspan="3" class="end bottom" align="right">'.number_format($total_amount,2).'</td>
								</tr>
							</table>';
				//========================================
		        $pdf['current_time'] = date('h:i a m/d/Y');
		        $pdf['title'] = '<span style="color:#b32017">R</span>eceipt <span style="color:#b32017">R</span>eport by <span style="color:#b32017">C</span>ustomer<br/>(Summary)';
		        $this->layout = 'pdf';
		        //set header
		        $pdf['logo_link'] = 'img/logo_anvy.jpg';
		        $pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
		        $pdf['html_loop'] = $html_loop;
		        $pdf['filename'] = 'RE_' . md5($pdf['current_time']);

		        $this->report_pdf($pdf);
		        echo URL.'/upload/' . $pdf['filename'] . '.pdf';
	    	}
			die;
		}
	}
	public function customer_detailed_report()
	{
		if(!$this->check_permission($this->name.'_@_options_@_customer_detailed_report'))
			die;
		$receipts = $this->opm->select_all(array(
						'arr_order'=>array('_id'=>1),
						'arr_field'=>array('_id','code','receipt_date','paid_by','reference','our_rep','company_id','company_name','notes','amount_received')
		));
		if($receipts->count()>0)
		{
			$group =array();
			$total_amount = 0;
			$html_loop = '';
			foreach($receipts as $key=>$value)
			{
				if(isset($value['company_id'])&&is_object($value['company_id']))
				{
					$id = (string)$value['company_id'];
					$amount_received = (isset($value['amount_received'])&&$value['amount_received']!= '' ? $value['amount_received'] : 0);
					$group[$id]['company_name'] = $value['company_name'];
					$group[$id]['group'][$key]['code'] = $value['code'];
					$group[$id]['group'][$key]['date'] = date('M d, Y',$value['receipt_date']->sec);
					$group[$id]['group'][$key]['paid_by'] = $value['paid_by'];
					$group[$id]['group'][$key]['reference'] = isset($value['reference']) ? $value['reference'] : '';
					$group[$id]['group'][$key]['our_rep'] = isset($value['our_rep']) ? $value['our_rep'] : '';
					$group[$id]['group'][$key]['notes'] = isset($value['notes']) ? $value['notes'] : '';
					$group[$id]['group'][$key]['total'] = number_format($amount_received,2);
					if(!isset($group[$id]['total_amount']))
						$group[$id]['total_amount'] = $amount_received;
					else
						$group[$id]['total_amount'] += $amount_received;
					$total_amount += $amount_received;
				}

			}
			if(!empty($group))
			{
				foreach($group as $value)
				{
					$i = 0;
					$html_loop .= '
							<table cellpadding="3" cellspacing="0" class="maintb">
			                  <tr>
			                     <td width="60%" class="first top" align="left">
			                        Customer
			                     </td>
			                     <td width="40%" class="end top" align="right">
			                        Total
			                     </td>
			                  </tr>
			                  <tr style="background-color:#eeeeee">
			                     <td class="first bottom" align="left">
			                        <strong>'.$value['company_name'].'</strong>
			                     </td>
			                     <td width="40%" class="end bottom" align="right">
			                        '.number_format($value['total_amount'],2).'
			                     </td>
			                  </tr>
			                </table>
			                <br /><br />
					';
					$html_loop .= '
							<table cellpadding="3" cellspacing="0" class="maintb">
			                  <tr>
			                     <td width="7%" class="first top">
			                        Ref #
			                     </td>
			                     <td width="15%" class="top">
			                        Date
			                     </td>
			                     <td width="10%" class="top" align="left">
			                        Paid by
			                     </td>
			                     <td width="13%" class="top" align="left">
			                        Reference
			                     </td>
			                     <td width="20%" class="top" align="left">
			                        Our rep
			                     </td>
			                     <td width="20%" class="top">
			                        Note
			                     </td>
			                     <td width="15%" colspan="3" class="end top" align="right">
			                        Amount
			                     </td>
			                  </tr>';
					foreach($value['group'] as $val)
					{
						$bg = ( $i % 2 == 0? 'background-color:#eeeeee':'background-color:#fdfcfa');
						$html_loop .='
							<tr style="'.$bg.'">
								<td class="first content" align="left">'.$val['code'].'</td>
								<td class="content">'.$val['date'].'</td>
								<td class="content" align="left">'.$val['paid_by'].'</td>
								<td class="content" align="left">'.$val['reference'].'</td>
								<td class="content" align="left">'.$val['our_rep'].'</td>
								<td class="content" align="left">'.$val['notes'].'</td>
								<td class="end content" colspan="3" align="right">'.$val['total'].'</td>
							</tr>
							';
						$i++;
					}
					$bg = ( $i % 2 == 0? 'background-color:#eeeeee':'background-color:#fdfcfa');
					$html_loop .='	<tr style="'.$bg.'">
										<td colspan="3" class="first bottom" align="left">'.$i.' record(s) listed.</td>
										<td colspan="3" class="bottom" align="right">
											<span style="font-weight:bold; padding-left:20px">Total:</span>
										</td>
										<td colspan="3" class="end bottom" align="right">'.number_format($value['total_amount'],2).'</td>
									</tr>
								</table>
								<br />
			                    <div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>
			                    <br />';
				}
				//========================================
		        $pdf['current_time'] = date('h:i a m/d/Y');
		        $pdf['title'] = '<span style="color:#b32017">R</span>eceipt <span style="color:#b32017">R</span>eport by <span style="color:#b32017">C</span>ustomer<br/>(Detailed)';
		        $this->layout = 'pdf';
		        //set header
		        $pdf['logo_link'] = 'img/logo_anvy.jpg';
		        $pdf['company_address'] = 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />';
		        $pdf['html_loop'] = $html_loop;
		        $pdf['filename'] = 'RE_' . md5($pdf['current_time']);

		        $this->report_pdf($pdf);
		        echo URL.'/upload/' . $pdf['filename'] . '.pdf';
	    	}
			die;
		}
	}
	public function view_minilist(){
		if(!isset($_GET['print_pdf'])){
			$arr_where = $this->arr_search_where();
			$receipts = $this->opm->select_all(array(
												'arr_where'  => $arr_where,
												'arr_field'  => array('_id','code','receipt_date','paid_by','reference','our_rep','company_id','company_name','notes','amount_received','unallocated','unallocated'),
												'arr_order'  => array('_id'=>1),
												'limit'		 => 2000
												));
			if($receipts->count() > 0){
				$html='';
				$i=0;
				$arr_data = array();
				$sum_amount_received = 0;
				$sum_allocated = 0;
				$sum_unallocated = 0;
				foreach($receipts as $key => $receipt){
					$sum_amount_received += $amount_received = (isset($receipt['amount_received']) ? (float)$receipt['amount_received'] : 0);
					$sum_allocated += $total_allocated = (isset($receipt['total_allocated']) ? (float)$receipt['total_allocated'] : 0);
					$sum_unallocated += $unallocated = (isset($receipt['unallocated']) ? $receipt['unallocated'] : 0);
					$html .= '<tr class="'.($i%2==0 ? 'bg_2' : 'bg_1').'">';
					$html .= '<td>'.$receipt['code'].'</td>';
					$html .= '<td>'.(isset($receipt['company_name']) ? $receipt['company_name'] : '') .'</td>';
					$html .= '<td class="center_text">'.(isset($receipt['receipt_date']) ? date('m/d/Y',$receipt['receipt_date']->sec):'') .'</td>';
					$html .= '<td>'.(isset($receipt['paid_by']) ? $receipt['paid_by'] : '') .'</td>';
					$html .= '<td>'.(isset($receipt['reference']) ? $receipt['reference'] : '') .'</td>';
					$html .= '<td class="right_text">'. number_format($amount_received,2) .'</td>';
					$html .= '<td class="right_text">'.number_format($total_allocated,2) .'</td>';
					$html .= '<td class="right_text">'.number_format($unallocated,2) .'</td>';
					$html .= '</tr>';
	                $i++;
				}
				 $html .='<tr class="last">
	                        <td colspan="4" class="bold_text right_none">'.$i.' record(s) listed.</td>
	                        <td class="right_text bold_text right_none">Total:</td>
	                        <td class="right_text bold_text right_none">'.number_format($sum_amount_received,2).'</td>
	                        <td class="right_text bold_text right_none">'.number_format($sum_allocated,2).'</td>
	                        <td class="right_text bold_text right_none">'.number_format($sum_unallocated,2).'</td>
	                        </tr>';
	            $arr_data['title'] = array('Ref #'=>'text-align: left;width: 7%;','Customer'=>'text-align: left;','Date','Paid by','Reference','Total receipts.'=>'text-align: right;width: 12%;','Allocated'=>'text-align: right;width: 12%;','Unallocated'=>'text-align: right;width: 12%;');
	            $arr_data['content'] = $html;
	            $arr_data['report_name'] = 'Receipt Mini Listing ';
	            $arr_data['report_file_name']='RE_'.md5(time());
	            Cache::write('receipts_minilist', $arr_data);
            }
        } else
            $arr_data = Cache::read('receipts_minilist');
		$this->render_pdf($arr_data);
	}

	public function thanh_toan_cong_no(){
		$this->selectModel('Salesorder');
		$companies = $this->Salesorder->collection->distinct('company_id',array('status'=>array('$nin' => array('Cancelled', 'Hủy Bỏ' ))));
		$this->selectModel('Company');
		$arr_company = $this->Company->select_all(array(
		                                          'arr_where' => array('_id' => array('$in' => $companies)),
		                                          'arr_field' => array('name'),
		                                          'arr_order' => array('code' => -1)
		                                          ));
		$this->set('company_name', $arr_company);
		//Trí
		$first_order = $this->Salesorder->select_one(array(),array(),array('salesorder_date'=>1));
		$first_month = array(
			'month' 	=> intval(date('m',$first_order['salesorder_date']->sec)),
			'year'		=> intval(date('Y',$first_order['salesorder_date']->sec))
			);
		$this->set('first_month', $first_month);
	}


	public function tinh_tong_hang_tra($salesorder_id){
		$line_entry_data = $this->requestAction(
					'salesorders/line_entry_data/products/0/0/'.$salesorder_id
		);
		$total_amount_return = 0;
        $amount = 0;
		$arr_order = array();
		foreach ($line_entry_data['products'] as $values) {
			$return_total = 0;
			if(!empty($values['return_item'])){
				foreach($values['return_item'] as $item){
					//$arr_order['return_date'] = $item['return_date'];
					$return_total += $item['return_quantity'];
				}
				$amount = $values['specification'] * $values['sell_price'] * $return_total;
            	$total_amount_return += $amount;
            }

		}
		return $total_amount_return;
	}

	public function thanh_toan_cong_no_detail($company_id, $month,$year){

		$this->selectModel('Company');
		$arr_company = $this->Company->select_one(array('_id' => new MongoID($company_id)), array('thanh_toan','no_cu'));
		$arr_company['no_cu'] = 0;


		$this->selectModel('Salesorder');
		$arr_salesorder = $this->Salesorder->select_all(array(
		                                                 'arr_where' => array('company_id' => new MongoId($company_id), 'status' => 'Hoàn thành'),
		                                                 'arr_field' => array('code','sum_amount','salesorder_date','products','status','sales_order_type','sum_amount_interest'),
		                                                 'arr_order' => array('salesorder_date' => 1)
		                                                ));
		$this->selectModel('Returnsalesorder');
		$arr_return_salesorder = $this->Returnsalesorder->select_all(array(
                                                         'arr_where' => array('company_id' => new MongoId($company_id), 'status' => 'Hoàn thành'),
                                                         'arr_field' => array('code','sum_amount','salesorder_date','products','status','sales_order_type', 'return_id','sum_amount_interest'),
                                                         'arr_order' => array('salesorder_date' => 1)
                                                        ));
		$this->selectModel('Productreturnerror');
		$arr_product_return_error = $this->Productreturnerror->select_all(array(
                                                         'arr_where' => array('company_id' => new MongoId($company_id), 'purchase_orders_status' => 'Hoàn thành'),
                                                         'arr_field' => array('code','sum_amount','products','salesorder_date','sum_amount_interest'),
                                                         'arr_order' => array('salesorder_date' => 1)
                                                        ));
		$this->selectModel('GSaleorder');
		$arr_gsalesorder = $this->GSaleorder->select_all(array(
		                                                  'arr_where' => array('company_id' => new MongoId($company_id),'status' => 'Hoàn thành'),
		                                                  'arr_field' => array('code','sum_amount','salesorder_date','products','status','sales_order_type','sum_amount_interest'),
		                                                  'arr_order' => array('salesorder_date' => 1)
		                                                  ));
		$con_lai = 0;
		$thanh_tien = 0;
		$arr_return = array();
		$arr_order = array();
		foreach($arr_salesorder as $key => $value){
			$time = $value['salesorder_date']->sec;
			while( isset($arr_return[$time]) )
				$time++;
			$value['type'] = 'Salesorder';
			$arr_return[$time] = $value;
		}
		foreach($arr_gsalesorder as $key => $value){
			$time = $value['salesorder_date']->sec;
			while( isset($arr_return[$time]) )
				$time++;
			$value['type'] = 'Salesorder';
			$arr_return[$time] = $value;
		}
		$con_lai_return = 0;
		foreach($arr_return_salesorder as $k => $v){
			$time = $v['salesorder_date']->sec;
			while( isset($arr_return[$time]) )
				$time++;
			$v['type'] = 'Returnsalesorder';
			$arr_return[$time] = $v;
		}
		foreach($arr_product_return_error as $k_product_return => $v_product_return){
			$time = $v_product_return['salesorder_date']->sec;
			while( isset($arr_return[$time]) )
				$time++;
			$v_product_return['type'] = 'Productreturnerror';
			$arr_return[$time] = $v_product_return;
		}
		ksort($arr_return);
		foreach($arr_return as $key_return => $v_return){
			if($v_return['type'] == 'Salesorder'){
				$arr_order[$key_return] = $v_return;
				$arr_order[$key_return]['khoang_giam'] = 0;
				$arr_order[$key_return]['loi_nhuan'] = $v_return['sum_amount_interest'];

			}else if($v_return['type'] == 'Returnsalesorder'){
				$arr_order[$key_return] = $v_return;
				$arr_order[$key_return]['khoang_giam'] = $v_return['sum_amount'] * (-1);
				$arr_order[$key_return]['sum_amount'] = 0;
				$arr_order[$key_return]['loi_nhuan'] = $arr_order[$key_return]['sum_amount_interest'] * (-1);
				$arr_order[$key_return]['return_id'] = 1;
			}else if($v_return['type'] == 'Productreturnerror'){
				$arr_order[$key_return] = $v_return;
				$arr_order[$key_return]['khoang_giam'] = $v_return['sum_amount'] * (-1);
				$arr_order[$key_return]['sum_amount'] = 0;
				$arr_order[$key_return]['loi_nhuan'] = $arr_order[$key_return]['sum_amount_interest'] * (-1);
				$arr_order[$key_return]['return_id'] = 1;
			}
		}
		$arr_return = array();
		$arr_month = array();
		$arr_year = array();
		foreach($arr_order as $order){
			$saleorder_month = intval( date("m", $order['salesorder_date']->sec) );
			$saleorder_year = intval( date("Y", $order['salesorder_date']->sec) );
			if( $saleorder_year == $year ){
				if($month != 'all'){
						if( $saleorder_month == $month){
							$arr_month[] = $order;
						}
				}else{
					$arr_year[] = $order;
				}
			}
		}
		if($month != 'all'){
			$arr_return = $arr_month;
		}else{
			$arr_return = $arr_year;
		}
		$this->set('month' , $month);
		$this->set('company_id', $company_id);
		$this->set('arr_order', $arr_return);
		$this->set('arr_company',$arr_company);
		return $arr_order;
	}


	public function list_salersorders() {
		if (!empty($_POST)) {
			$this->selectModel('Company');
			$arr_save = $this->Company->select_one(array('_id'=>new MongoId($_POST['company_id'])));
			$sum_amount = str_replace('(','',$_POST['sum_amount']);
			$sum_amount = str_replace(')','',$sum_amount);
			$arr_save['thanh_toan'][] = array(
			                                'id' => $_POST['salesorder_id'],
			                                'amount' => isset($_POST['paid']) ? $_POST['paid'] : 0,
			                                //'date_modified' => new MongoDate(time()),
			                                'date_modified' => $_POST['date_modified'],
			                                'sum_amount' =>$sum_amount,
			                                );
			if ($this->Company->save($arr_save)) {
				echo 'ok';
			} else {
				echo 'Error: ' . $this->Company->arr_errors_save[1];
			}
		}
		die;
	}

	public function tinh_tong_hang_tra_nha_cung_cap($purchaseorder_id){
		$line_entry_data = $this->requestAction('purchaseorders/line_entry_data/products/0/'.$purchaseorder_id);
		$total_amount_return = 0;
        $amount = 0;
		$arr_order = array();
		foreach ($line_entry_data['products'] as $values) {
			$return_total = 0;
			if(!empty($values['return_item'])){
				foreach($values['return_item'] as $item){
					$return_total += $item['return_quantity'];
				}
				$amount = $values['specification'] * $values['sell_price'] * $return_total;
            	$total_amount_return += $amount;
            }

		}
		return $total_amount_return;
	}

	public function cong_no_nha_cung_cap(){
		$this->selectModel('Purchaseorder');
		$companies = $this->Purchaseorder->collection->distinct('company_id',array('status'=>array('$nin' => array('Cancelled', 'Hủy Bỏ' ))));
		$this->selectModel('Company');
		$arr_company = $this->Company->select_all(array(
		                                          'arr_where' => array('_id' => array('$in' => $companies)),
		                                          'arr_field' => array('name'),
		                                          'arr_order' => array('code' => -1)
		                                          ));
		$this->set('company_name', $arr_company);
	}

	public function cong_no_nha_cung_cap_chi_tiet($company_id){
		$this->selectModel('Company');
		$arr_company = $this->Company->select_one(array('_id' => new MongoID($company_id)), array('thanh_toan','no_cu'));
		$arr_company['no_cu'] = 0;

		$this->selectModel('Purchaseorder');
		$arr_purchaseorder = $this->Purchaseorder->select_all(array(
		                                                 'arr_where' => array('company_id' => new MongoId($company_id),'purchase_orders_status'=>"Hoàn thành"),
		                                                 'arr_field' => array('code','sum_amount','purchord_date','products','payment_terms'),
		                                                 'arr_order' => array('purchord_date' => 1)
		                                                 ));
		$this->selectModel('Returnpurchaseorder');
		$arr_return_purchaseorder = $this->Returnpurchaseorder->select_all(array(
                                                       'arr_where' => array('company_id' => new MongoId($company_id),'purchase_orders_status'=>"Hoàn thành"),
                                                       'arr_field' => array('code','sum_amount','purchord_date','products','payment_terms','return_id'),
                                                       'arr_order' => array('purchord_date' => 1)
                                                       ));
		$this->selectModel('Producterrorcompany');
		$arr_product_error_company = $this->Producterrorcompany->select_all(array(
                                                     'arr_where' => array('company_id' => new MongoId($company_id), 'purchase_orders_status' => "Hoàn thành"),
                                                     'arr_field' => array('code','sum_amount','purchord_date','products','salesorder_date'),
                                                     'arr_order' => array('purchord_date' => 1)
                                                    ));


		$arr_pur = array();
		$con_lai = 0;
		$thanh_tien = 0;
		$thanh_tien_return = 0;
		$arr_return = array();
		foreach($arr_purchaseorder as $key => $value){
			$time = $value['purchord_date']->sec;
			while( isset($arr_return[$time]) )
				$time++;
			$value['type'] = 'Purchaseorder';
			$arr_return[$time] = $value;
		}
		$con_lai_return = 0;
		foreach($arr_return_purchaseorder as $k => $v){
			$time = $v['purchord_date']->sec;
			while( isset($arr_return[$time]) )
				$time++;
			$v['type'] = 'Returnpurchaseorder';
			$arr_return[$time] = $v;
		}
		foreach($arr_product_error_company as $k_product_return => $v_product_return){
			$time = $v_product_return['purchord_date']->sec;
			while( isset($arr_return[$time]) )
				$time++;
			$v_product_return['type'] = 'Producterrorcompany';
			$arr_return[$time] = $v_product_return;
		}
		ksort($arr_return);
		foreach($arr_return as $key_return => $v_return){
			if($v_return['type'] == 'Purchaseorder'){
				$arr_pur[$key_return] = $v_return;
				$arr_pur[$key_return]['no_cu'] = $con_lai;
				$sum_amount = isset($arr_pur[$key_return]['sum_amount']) ? $arr_pur[$key_return]['sum_amount'] : 0;
				$arr_pur[$key_return]['thanh_tien'] = $sum_amount + $arr_pur[$key_return]['no_cu'];
				if(isset($arr_company['thanh_toan']) ) {
					foreach($arr_company['thanh_toan'] as $k => $thanh_toan){
						if(!isset($thanh_toan['id']) || $thanh_toan['id'] != $v_return['_id']) continue;
						$arr_pur[$key_return]['thanh_toan'] = isset($thanh_toan['amount']) ? (double)$thanh_toan['amount'] : 0;
						//break;
					}
				}
				else
					$arr_pur[$key_return]['thanh_toan'] = 0;
				$thanh_tien = isset($arr_pur[$key_return]['thanh_toan']) ? $arr_pur[$key_return]['thanh_toan'] : 0 ;
				$con_lai = $arr_pur[$key_return]['con_lai'] = $arr_pur[$key_return]['thanh_tien'] - $thanh_tien;
			}else if($v_return['type'] == 'Returnpurchaseorder'){
				$arr_pur[$key_return] = $v_return;
				$arr_pur[$key_return]['no_cu'] = $con_lai;
				$sum_amount = isset($arr_pur[$key_return]['sum_amount']) ? $arr_pur[$key_return]['sum_amount'] : 0;
				$arr_pur[$key_return]['thanh_tien'] =   $arr_pur[$key_return]['no_cu'] - $sum_amount;
				if(isset($arr_company['thanh_toan'])){
					foreach($arr_company['thanh_toan'] as $kk => $v_thanh_toan){
						if(!isset($v_thanh_toan['id']) || $v_thanh_toan['id'] != $v_return['_id']) continue;
						$arr_pur[$key_return]['thanh_toan'] = isset($thanh_toan['amount']) ? (double)$thanh_toan['amount'] : 0;
					}
				}
				else
					$arr_pur[$key_return]['thanh_toan'] = 0;
				$thanh_tien_return = isset($arr_pur[$key_return]['thanh_toan']) ? $arr_pur[$key_return]['thanh_toan'] : 0;
				$con_lai = $arr_pur[$key_return]['con_lai'] = $arr_pur[$key_return]['thanh_tien'];
				$arr_pur[$key_return]['return_id'] = 1;
			}else if($v_return['type'] == 'Producterrorcompany'){
				$arr_pur[$key_return] = $v_return;
				$arr_pur[$key_return]['no_cu'] = $con_lai;
				$sum_amount = isset($arr_pur[$key_return]['sum_amount']) ? $arr_pur[$key_return]['sum_amount'] : 0;
				$arr_pur[$key_return]['thanh_tien'] = $arr_pur[$key_return]['no_cu'] - $sum_amount;
				if(isset($arr_company['thanh_toan'])){
					foreach($arr_company['thanh_toan'] as $kkk => $v_thanh_toan_p){
						if(!isset($v_thanh_toan_p['id']) || $v_thanh_toan_p['id'] != $v_return['_id']) continue;
						$arr_pur[$key_return]['thanh_toan'] = isset($thanh_toan['amount']) ? (double)$thanh_toan['amount'] : 0;
					}
				}else
					$arr_pur[$key_return]['thanh_toan'] = 0;
				$con_lai = $arr_pur[$key_return]['con_lai'] = $arr_pur[$key_return]['thanh_tien'];
			}
		}
		$this->set('company_id', $company_id);
		$this->set('arr_pur', $arr_pur);
		$this->set('arr_company',$arr_company);
		return $arr_pur;
	}

	/*public function cong_no_thang(){
		$this->selectModel('Company');
		$this->selectModel('Salesorder');
		$companies = $this->Salesorder->collection->distinct('company_id',array('status'=>array('$nin' => array('Cancelled', 'Hủy Bỏ' ))));
		$arr_company = $this->Company->select_all(array(
		                                          'arr_where' => array('_id' => array('$in' => $companies)),
		                                          'arr_field' => array('name'),
		                                          'arr_order' => array('code' => -1)
		                                          ));
		$this->set('company_name', $arr_company);
		return $arr_company;
	}*/

	public function cong_no_thang_chi_tiet($company_id){
		$arr_salesorder = $this->thanh_toan_cong_no_detail($company_id);
		//pr($arr_salesorder);die;
		$month = array();
		$arr_data = array();
		foreach($arr_salesorder as $key => $value){
			$month = date("m",$value['salesorder_date']->sec);
			if(date("m",$value['salesorder_date']->sec) == $month){
				$arr_data[$month]['no_cu'] = $value['no_cu'];
				$arr_data[$month]['thanh_toan'] = isset($value['thanh_toan']) ? $value['thanh_toan'] : 0;
				$arr_data[$month]['thanh_tien'] = $value['thanh_tien'];
				$arr_data[$month]['con_lai'] = $value['con_lai'];
				$arr_data[$month]['sum_amount'] = $value['sum_amount'];
				$arr_data[$month]['salesorder_date'] = $value['salesorder_date'];
			}
		}
		//pr(($arr_salesorder));die;
		$this->set('arr_data', $arr_data);
		return $arr_data;
	}

	public function cong_no_thang(){
		$this->selectModel('Company');
		$this->selectModel('Salesorder');
		$companies = $this->Salesorder->collection->distinct('company_id',array('status'=>array('$nin' => array('Cancelled', 'Hủy Bỏ' ))));
		$arr_company = $this->Company->select_all(array(
		                                          'arr_where' => array('_id' => array('$in' => $companies)),
		                                          'arr_field' => array('name'),
		                                          'arr_order' => array('code' => -1)
		                                          ));
		$this->set('company_name', $arr_company);
		//Trí
		$first_order = $this->Salesorder->select_one(array(),array(),array('salesorder_date'=>1));
		$first_month = array(
			'month' 	=> intval(date('m',$first_order['salesorder_date']->sec)),
			'year'		=> intval(date('Y',$first_order['salesorder_date']->sec))
			);
		$this->set('first_month', $first_month);
		return $arr_company;
	}

	public function cong_no_theo_thang($month_input,$year_input){
		$arr_salesorder = $this->thanh_toan_cong_no_in_month();
		$month = array();
		$total_sum_amount = 0;
		$tong_khoang_giam = 0;
		$total_amount_interest = 0;
		$arr_data = array();
		foreach($arr_salesorder as $key => $value){
			$month = intval(date("m",$value['salesorder_date']->sec));
			$year = intval(date("Y",$value['salesorder_date']->sec));
			if($month == $month_input && $year == $year_input){
				$company_id = (string)$value['company_id'];
				$arr_data[$company_id]['company_name'] = $value['company_name'];
				$arr_data[$company_id]['company_id'] = $value['company_id'];
				$is_return = isset($value['return_id']) ? $value['return_id'] : 0;
				$arr_data[$company_id]['khoang_giam'][$key] = 0;
				if($is_return == 1){
					$tong_khoang_giam = $arr_data[$company_id]['khoang_giam'][$key] = $value['giam'];
				}
				$total_sum_amount += $value['sum_amount'];
				$total_amount_interest = $value['sum_amount_interest'];
				$arr_data[$company_id]['sum_amount'][$key] = isset($value['sum_amount_so']) ? $value['sum_amount_so'] : 0;
				$arr_data[$company_id]['amount_interest'] = $value['sum_amount_interest'];
				$arr_data[$company_id]['loi_nhuan'][$key] = isset($value['sum_amount_interest_so']) ? $value['sum_amount_interest_so'] : 0;
				//$arr_data[$company_id]['loi_nhuan'] = $total_amount_interest - $tong_khoang_giam;
			}
		}
		//pr($arr_data);die;
		$this->set('arr_data', $arr_data);
		return $arr_data;
	}
	public function thanh_toan_cong_no_in_month(){

		$this->selectModel('Company');
		//$arr_company = $this->Company->select_one(array('_id' => new MongoID($company_id)), array('thanh_toan','no_cu'));
		$arr_company = array();
		$arr_company['no_cu'] = 0;


		$this->selectModel('Salesorder');
		$arr_salesorder = $this->Salesorder->select_all(array(
		                                                 'arr_where' => array('status' => 'Hoàn thành'),
		                                                 'arr_field' => array('code','sum_amount','salesorder_date','products','status','sales_order_type','company_id','company_name','sum_amount_interest'),
		                                                 'arr_order' => array('salesorder_date' => 1)
		                                                 ));
		$this->selectModel('Returnsalesorder');
		$arr_return_salesorder = $this->Returnsalesorder->select_all(array(
                                                         'arr_where' => array('status' => 'Hoàn thành'),
                                                         'arr_field' => array('code','sum_amount','salesorder_date','products','status','sales_order_type', 'return_id','company_id','company_name','sum_amount_interest'),
                                                         'arr_order' => array('salesorder_date' => 1)
                                                         ));
		$this->selectModel('Productreturnerror');
		$arr_product_return_error = $this->Productreturnerror->select_all(array(
                                                          'arr_where' => array('purchase_orders_status' => 'Hoàn thành'),
                                                          'arr_field' => array('code','sum_amount','purchord_date','products','salesorder_date','company_id','company_name','return_id','sum_amount_interest'),
                                                          'arr_order' => array('purchord_date' => 1)
                                                          ));
		$this->selectModel('GSaleorder');
		$arr_gsalesorder = $this->GSaleorder->select_all(array(
		                                                 'arr_where' => array('status' => 'Hoàn thành'),
		                                                 'arr_field' => array('code','sum_amount','salesorder_date','products','status','sales_order_type','company_id','company_name','sum_amount_interest'),
		                                                 'arr_order' => array('salesorder_date' => 1)
		                                                 ));
		$thanh_tien = 0;
		$arr_salesorder = iterator_to_array($arr_salesorder);
		if (count($arr_salesorder) == 0) die;


		foreach($arr_salesorder as $key => $value){
			$arr_salesordersort[$value['company_id']->{'$id'}][$key] = $value;
		}
		// $arr_purchaseordersort la mang purchaseorder gom lai theo company_id: array['company_id'][key]
		foreach($arr_salesordersort as $key_company => $value_company){
			foreach ($value_company as $key => $value) {
				$arr_order[$key] = $value;
				$arr_order[$key]['sum_amount_interest_so'] = $value['sum_amount_interest'];
				$arr_order[$key]['sum_amount_so'] = $value['sum_amount'];
			}
		}

		foreach($arr_gsalesorder as $k => $v){
			$arr_order[$k] = $v;
		}

		foreach($arr_return_salesorder as $k => $v){
			$arr_order[$k] = $v;
			$arr_order[$k]['return_id'] = 1;
			$arr_order[$k]['giam'] = $v['sum_amount_interest'] * -1;
		}

		foreach($arr_product_return_error as $k_product_return => $v_product_return){
			$arr_order[$k_product_return] = $v_product_return;
			$arr_order[$k_product_return]['return_id'] = 1;
			$arr_order[$k_product_return]['giam'] = $v_product_return['sum_amount_interest'] * -1;
		}
		$this->set('arr_order', $arr_order);
		$this->set('arr_company',$arr_company);
		return $arr_order;
	}

	public function cong_no_nam(){
		$company_name = $this->cong_no_thang();
		$this->set('company_name', $company_name);
		//Trí
		$first_order = $this->Salesorder->select_one(array(),array(),array('salesorder_date'=>1));
		$first_month = array(
			'month' 	=> intval(date('m',$first_order['salesorder_date']->sec)),
			'year'		=> intval(date('Y',$first_order['salesorder_date']->sec))
			);
		$this->set('first_month', $first_month);
	}

	public function cong_no_nam_chi_tiet($year_input){
		$arr_salesorder = $this->thanh_toan_cong_no_in_month(); // tat ca cac salesorder, returnsalesorders ( cac salesorder co the khac company)
		$arr_data = array();
		foreach($arr_salesorder as $key => $value){
			$year = intval(date("Y",$value['salesorder_date']->sec));
			if($year == $year_input){
				$company_id = (string)$value['company_id'];
				$arr_data[$company_id]['company_name'] = $value['company_name'];
				$arr_data[$company_id]['company_id'] = $value['company_id'];
				$is_return = isset($value['return_id']) ? $value['return_id'] : 0;
				$arr_data[$company_id]['khoang_giam'][$key] = 0;
				if($is_return == 1){
					$tong_khoang_giam = $arr_data[$company_id]['khoang_giam'][$key] = $value['giam'];
				}
				$total_amount_interest = $value['sum_amount_interest'];
				$arr_data[$company_id]['sum_amount'][$key] = isset($value['sum_amount_so']) ? $value['sum_amount_so'] : 0;
				$arr_data[$company_id]['amount_interest'] = $value['sum_amount_interest'];
				$arr_data[$company_id]['loi_nhuan'][$key] = isset($value['sum_amount_interest_so']) ? $value['sum_amount_interest_so'] : 0;
			}
		}
		$this->set('arr_data', $arr_data);
		return $arr_data;
	}

	public function cong_no_nam_chi_tiet_old($year_input){
		$arr_salesorder = $this->cong_no_thang_chi_tiet($company_id);
		$month = array();
		$year = array();
		$arr_data = array();
		foreach($arr_salesorder as $key => $value){
			$month = date("m",$value['salesorder_date']->sec);
			$year = date("y",$value['salesorder_date']->sec);
			$arr_data[$month]['no_cu'] = $value['no_cu'];
			$arr_data[$month]['thanh_toan'] = isset($value['thanh_toan']) ? $value['thanh_toan'] : 0;
			$arr_data[$month]['thanh_tien'] = $value['thanh_tien'];
			$arr_data[$month]['con_lai'] = $value['con_lai'];
			$arr_data[$month]['sum_amount'] = $value['sum_amount'];
			$arr_data[$month]['salesorder_date'] = $value['salesorder_date'];
		}
		$arr_data_year = array();
		$total_no_cu = 0;
		$total_thanh_toan = 0;
		$total_con_lai = 0;
		$total_sum_amount = 0;
		foreach($arr_data as $v){
			$total_no_cu += $v['no_cu'];
			$total_thanh_toan += $v['thanh_toan'];
			$total_con_lai += $v['con_lai'];
			$total_sum_amount += $v['sum_amount'];
		}
		$arr_data_year[$year]['no_cu']  = $total_no_cu;
		$arr_data_year[$year]['thanh_toan'] = $total_thanh_toan;
		$arr_data_year[$year]['con_lai'] = $total_con_lai;
		$arr_data_year[$year]['sum_amount'] = $total_sum_amount;
		$this->set('arr_data', $arr_data_year);
		return $arr_data_year;
	}

	function in_cong_no_khach_hang($company_id, $month){
        if(!isset($_GET['print_pdf'])){
            $arr_order = $this->thanh_toan_cong_no_detail($company_id, $month);
            $this->selectModel('Company');
            $company = $this->Company->select_one(array('_id' => new MongoId($company_id)), array('name','addresses','phone'));
            if(!empty($arr_order)){
                $group = array();
                $html = '';
                $i = 0;
                $total_sum_amount = 0;
                $tong_khoang_giam = 0;
                $tong_lai_thuc = 0;
    			$total_loi_nhuan = 0;
                $arr_data = array();
                foreach($arr_order as $key=>$order){
                	if(date("m", $order['salesorder_date']->sec) == $month){
	                	$is_return = isset($order['return_id']) ? $order['return_id'] : 0;
	                	$total_sum_amount += $order['sum_amount'];
	                	$tong_khoang_giam += $order['khoang_giam'];
	                	$tong_lai_thuc += $order['loi_nhuan'];
	                	$total_loi_nhuan += $order['loi_nhuan'];
	                    $html .= '<tr class="bg_1" style="border:1px solid">';
	                    $html .= '<td class="center_text">'.date("d-m-Y",$order['salesorder_date']->sec).'</td>';
	                   /* if($order['products'][0]['oum'] == 'Cái' || $order['return_id'] == 1)
	                    	$html .= '<td class="right_text">('.(isset($order['sum_amount']) ? $this->opm->format_currency($order['sum_amount']) : '').')</td>';
	                    else*/

	                    $html .= '<td class="right_text">'.(isset($order['sum_amount']) ? $this->opm->format_currency($order['sum_amount']) : '').'</td>';
	                	if($is_return == 1){
		                    $html .= '<td class="right_text">('.(isset($order['khoang_giam']) ? $this->opm->format_currency($order['khoang_giam'] * -1) : '').')</td>';
		                    $html .= '<td class="right_text">('.(isset($order['sum_amount_interest']) ? $this->opm->format_currency($order['loi_nhuan'] * -1) : '').')</td>';
		                    $html .= '<td class="right_text">('.(isset($order['loi_nhuan']) ? $this->opm->format_currency($order['loi_nhuan'] * -1) : '').')</td>';
		                }else{
		                	$html .= '<td class="right_text">'.(isset($order['khoang_giam']) ? $this->opm->format_currency($order['khoang_giam']) : '').'</td>';
		                    $html .= '<td class="right_text">'.(isset($order['sum_amount_interest']) ? $this->opm->format_currency($order['loi_nhuan']) : '').'</td>';
		                    $html .= '<td class="right_text">'.(isset($order['loi_nhuan']) ? $this->opm->format_currency($order['loi_nhuan']) : '').'</td>';
		                }
	                    $html .= '</tr>';
	                    $i++;
	                }
                }
                $html .='<tr class="last">
                            <td class=" bold_text right_none" style="text-align: center; border: 1px solid"> Tổng cộng</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'. number_format($total_sum_amount).'</td>
                            <td class=" right_none" style="text-align: right; border: 1px solid">'. number_format($tong_khoang_giam *-1).'</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important"> '. number_format($tong_lai_thuc).'</td>
                            <td class=" bold_text right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($total_loi_nhuan).'</td>
                         </tr>';
                $html .= '<table style="margin-top: 25px">
                            <tr style="font-size:15px" >
                                <td style="text-align:center;width: 60%">Người lập<br />(Ký, họ tên)</td>
                                <td style="text-align:center;width: 60%">Trưởng đơn vị<br />(Ký, họ tên)</td>
                            </tr>
                            <tr>
                                <td ></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                        </table>';
               $arr_data['title'] = array('Ngày' => 'width: 4%; tesxt-align: center','Doanh số' => 'width: 8%; text-align: right','Khoảng giảm' =>'width: 8%;text-align: right;','Lãi thực' => 'width: 8%; text-align:right','Lợi nhuận'=>'width: 8%;text-align: right;');
                $arr_data['content'] = $html;
                $arr_data['report_name'] = 'Chi tiết doanh thu '.$company['name'];
                $arr_data['report_file_name']='PRO_'.md5(time());
                $arr_data['report_orientation'] = 'portrait';
                $arr_data['logo'] = 'img/logo_anvy.jpg';
                $arr_data['current_day'] = date('H:i d-m-Y');
                $arr_data['company_address'] = '<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
			           						ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
			           						<span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
			           						ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
			           						Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
			           						Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>';

			    $arr_data['company_name'] = $company['name'];
			    $arr_data['phone'] = $company['phone'];
			    $arr_data['addresses'] = $company['addresses'];
                Cache::write('in_cong_no_khach_hang', $arr_data);
            }
        } else
            $arr_data = Cache::read('in_cong_no_khach_hang');
        $this->render_pdf($arr_data);
    }

    function in_cong_no_nha_cung_cap($company_id){
    	if(!isset($_GET['print_pdf'])){
            $arr_data = array();
            $arr_order = $this->cong_no_nha_cung_cap_chi_tiet($company_id);
            $this->selectModel('Company');
            $company = $this->Company->select_one(array('_id' => new MongoId($company_id)), array('name','addresses','phone'));
            if(!empty($arr_order)){
                $group = array();
                $html = '';
                $i = 0;
                $total = 0;
                $tong_doanh_thu = 0;
                $tong_thanh_toan = 0;
                $tong_no = 0;
                foreach($arr_order as $key=>$order){
                	$total += $order['con_lai'];
                	$tong_doanh_thu += $order['sum_amount'];
                	$tong_thanh_toan += $order['thanh_toan'];
                	$tong_no += $order['con_lai'];
                    $html .= '<tr class="bg_1" style="border:1px solid">';
                    $html .= '<td class="center_text">'.date("d-m-Y",$order['purchord_date']->sec).'</td>';
                    if($order['products'][0]['oum'] == 'Cái' || $order['return_id'] == 1){
                    	$html .= '<td class="right_text">('.(isset($order['sum_amount']) ? $this->opm->format_currency($order['sum_amount']) : '').')</td>';
                    }else{
                    	$html .= '<td class="right_text">'.(isset($order['sum_amount']) ? $this->opm->format_currency($order['sum_amount']) : '').'</td>';
                    }
                    $html .= '<td class="right_text">'.(isset($order['no_cu']) ? $this->opm->format_currency($order['no_cu']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($order['thanh_toan']) ? $this->opm->format_currency($order['thanh_toan']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($order['con_lai']) ? $this->opm->format_currency($order['con_lai']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($order['con_lai']) ? $this->opm->format_currency($order['con_lai']) : '').'</td>';
                    $html .= '</tr>';
                    $i++;
                }
                $html .='<tr class="last">
                			<td class="right_text bold_text right_none" style="border: 1px solid">Doanh thu</td>
                			<td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($order['con_lai'],0).'</td>
                            <td class="right_text bold_text right_none" style="border: 1px solid">Thanh toán</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($tong_thanh_toan,0).'</td>
                            <td class=" bold_text right_text right_none" style="border: 1px solid;border-right:1px solid !important">Tổng nợ</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($order['con_lai'],0).' </td>
                         </tr>';
                $html .= '<table style="margin-top: 25px">
                            <tr style="font-size:15px" >
                                <td style="text-align:center; width: 40%;">Xác nhận nhà cung cấp<br /> (Ký, họ tên)</td>
                                <td style="text-align:center;width: 40%">Người lập<br />(Ký, họ tên)</td>
                                <td style="text-align:center;width: 40%">Trưởng đơn vị<br />(Ký, họ tên)</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                        </table>';
                $arr_data['title'] = array('Ngày' => 'width: 4%; tesxt-align: center','Toa hàng' => 'width: 8%; text-align: right','Nợ cũ' =>'width: 8%;text-align: right;','Trả tiền' => 'width: 8%; text-align:right','Còn lại'=>'width: 8%;text-align: right;','Tổng tiền'=>'width: 8%;text-align: right;');
                $arr_data['content'] = $html;
                $arr_data['report_name'] = 'Công nợ Nhà cung cấp ';
                $arr_data['report_file_name']='PRO_'.md5(time());
                $arr_data['report_orientation'] = 'portrait';
                $arr_data['logo'] = 'img/logo_anvy.jpg';
                $arr_data['current_day'] = date('H:i d-m-Y');
                $arr_data['company_address'] = '<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
			           						ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
			           						<span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
			           						ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
			           						Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
			           						Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>';

			    $arr_data['company_name'] = $company['name'];
			    $arr_data['phone'] = $company['phone'];
			    $arr_data['addresses'] = $company['addresses'];

                Cache::write('in_cong_no_nha_cung_cap', $arr_data);
            }
        } else
            $arr_data = Cache::read('in_cong_no_nha_cung_cap');
        $this->render_pdf($arr_data);
    }

    /*function in_cong_no_thang_khach_hang($company_id){
    	if(!isset($_GET['print_pdf'])){
            $arr_order = $this->cong_no_thang_chi_tiet($company_id);
            $this->selectModel('Company');
            $company = $this->Company->select_one(array('_id' => new MongoId($company_id)), array('name'));
            if(!empty($arr_order)){
                $group = array();
                $html = '';
                $i = 0;
                $arr_data = array();
                foreach($arr_order as $key=>$order){
                    $html .= '<tr class="bg_1" style="border:1px solid">';
                    $html .= '<td class="center_text">'.'Tháng '.date("m",$order['salesorder_date']->sec).'</td>';
                    $html .= '<td class="right_text">'.(isset($order['sum_amount']) ? $this->opm->format_currency($order['sum_amount']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($order['no_cu']) ? $this->opm->format_currency($order['no_cu']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($order['thanh_toan']) ? $this->opm->format_currency($order['thanh_toan']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($order['con_lai']) ? $this->opm->format_currency($order['con_lai']) : '').'</td>';
                    $html .= '</tr>';
                    $i++;
                }
                $arr_data['title'] = array('Tháng' => 'width: 4%; text-align: center','Toa hàng' => 'width: 8%; text-align: right','Nợ cũ' =>'width: 8%;text-align: right;','Trả tiền' => 'width: 8%; text-align:right','Còn lại'=>'width: 8%;text-align: right;');
                $arr_data['content'] = $html;
                $arr_data['report_name'] = 'Khách Hàng '.$company['name'];
                $arr_data['report_file_name']='PRO_'.md5(time());
                $arr_data['report_orientation'] = 'portrait';
                Cache::write('in_cong_no_thang_khach_hang', $arr_data);
            }
        } else
            $arr_data = Cache::read('in_cong_no_thang_khach_hang');
        $this->render_pdf($arr_data);
    }*/
    function in_cong_no_thang_khach_hang($month_input,$year_input){
    	if(!isset($_GET['print_pdf'])){
            $arr_order = $this->cong_no_theo_thang($month_input,$year_input);
            $this->selectModel('Company');

            //$company = $this->Company->select_one(array('_id' => new MongoId($month_input)), array('name'));

            if(!empty($arr_order)){
                $group = array();
                $html = '';
                $i = 0;
                $arr_data = array();
                $tong_doanh_thu = 0;
                $tong_thanh_toan = 0;
                $tong_khoang_giam = 0;
                $tong_lai_thuc = 0;
                $tong_no = 0;
                foreach($arr_order as $key=>$order){
                	$sum_amount = 0;
                	$khoang_giam = 0;
                	$loi_nhuan = 0;
                	foreach($order['sum_amount'] as $k => $v){
                		$sum_amount += $v;
                		$tong_doanh_thu += $v;
                	}
                	foreach($order['khoang_giam'] as $kk => $vv){
                		$khoang_giam += $vv;
                		$tong_khoang_giam += $vv;
                	}
                	foreach($order['loi_nhuan'] as $kkk => $vvv){
                		$loi_nhuan += $vvv;
                		$tong_lai_thuc += $vvv;
                	}
                    $html .= '<tr class="bg_1" style="border:1px solid;font-size:18px;">';
                    $html .= '<td style="width:40%">'.(isset($order['company_name']) ? ($order['company_name']) : '').'</td>';
                    $html .= '<td class="right_text" style="width:15%">'.(isset($order['sum_amount']) ? $this->opm->format_currency($sum_amount) : 0).'</td>';
                    $html .= '<td class="right_text" style="width:15%">('.(isset($order['khoang_giam']) ? $this->opm->format_currency($khoang_giam * -1) : 0).')</td>';
                    $html .= '<td class="right_text" style="width:15%">'.(isset($order['amount_interest']) ? $this->opm->format_currency($loi_nhuan) : 0).'</td>';
                    $html .= '<td class="right_text" style="width:15%">'.(isset($order['loi_nhuan']) ? $this->opm->format_currency($loi_nhuan + $khoang_giam ) : 0).'</td>';
                    $html .= '</tr>';
                    $i++;
                }
                $html .='<tr class="last">
                            <td class="bold_text right_text right_none" style="border: 1px solid;border-right:1px solid !important">Tổng cộng</td>
                            <td class="right_text right_none" style="border: 1px solid">'.number_format($tong_doanh_thu,0).'</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($tong_khoang_giam * -1,0).'</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($tong_lai_thuc,0).'</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($tong_lai_thuc + $tong_khoang_giam,0).'</td>
                         </tr>';
                $arr_data['current_day'] = date('H:i d-m-Y');
                $arr_data['company_address'] = '<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
			           						ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
			           						<span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
			           						ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
			           						Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
			           						Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>';
                $arr_data['title'] = array('Tên công ty' => 'width: 10%; text-align: left','Doanh số' => 'width: 8%; text-align: right','Khoảng giảm' => 'width: 8%; text-align:right','Lãi thực'=>'width: 8%;text-align: right;','Lợi nhuận'=>'width: 8%;text-align: right');
                $html .= '<table style="margin-top: 25px">
                            <tr style="font-size:18px" >
                                <td style="text-align:center; width: 40%;">Người lập<br /> (Ký, họ tên)</td>
                                <td style="text-align:center;width: 30%"><br /></td>
                                <td style="text-align:center;width: 40%">Trưởng đơn vị<br />(Ký, họ tên)</td>
                            </tr>
                            <tr>
                                <td ></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                        </table>';
                $arr_data['content'] = $html;
                $arr_data['report_name'] = 'Doanh thu tháng '.$month_input;
                $arr_data['report_file_name']='PRO_'.md5(time());
                $arr_data['report_orientation'] = 'portrait';
                Cache::write('in_cong_no_thang_khach_hang', $arr_data);
            } else {
            	$arr_data = array();
            	$arr_data['report_name'] = 'Công nợ tháng '.$month_input;
            	$arr_data['current_day'] = date('H:i d-m-Y');
            	$arr_data['title'] = array('Tên công ty' => 'width: 10%; text-align: left','Doanh số' => 'width: 8%; text-align: right','Khoảng giảm' => 'width: 8%; text-align:right','Lãi thực'=>'width: 8%;text-align: right;','Lợi nhuận'=>'width: 8%;text-align: right');
            	$arr_data['current_day'] = date('H:i d-m-Y');
            	$arr_data['company_address'] = '<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
			           						ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
			           						<span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
			           						ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
			           						Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
			           						Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>';
			    Cache::write('in_cong_no_thang_khach_hang', $arr_data);
            }
        } else
            $arr_data = Cache::read('in_cong_no_thang_khach_hang');
        $this->render_pdf($arr_data);
    }

    function in_cong_no_nam_khach_hang($company_id){
    	if(!isset($_GET['print_pdf'])){
            $arr_order = $this->cong_no_thang_chi_tiet($company_id);
            $this->selectModel('Company');
            $company = $this->Company->select_one(array('_id' => new MongoId($company_id)), array('name'));
            if(!empty($arr_order)){
                $group = array();
                $html = '';
                $i = 0;
                $arr_data = array();
                foreach($arr_order as $key=>$order){
                    $html .= '<tr class="bg_1" style="border:1px solid">';
                    $html .= '<td class="center_text">'.' '.date("y",$order['salesorder_date']->sec).'</td>';
                    $html .= '<td class="right_text">'.(isset($order['sum_amount']) ? $this->opm->format_currency($order['sum_amount']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($order['no_cu']) ? $this->opm->format_currency($order['no_cu']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($order['thanh_toan']) ? $this->opm->format_currency($order['thanh_toan']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($order['con_lai']) ? $this->opm->format_currency($order['con_lai']) : '').'</td>';
                    $html .= '</tr>';
                    $i++;
                }
                $arr_data['title'] = array('Tháng' => 'width: 4%; text-align: center','Toa hàng' => 'width: 8%; text-align: right','Nợ cũ' =>'width: 8%;text-align: right;','Thanh toán' => 'width: 8%; text-align:right','Còn lại'=>'width: 8%;text-align: right;');
                $arr_data['content'] = $html;
                $arr_data['report_name'] = 'Khách Hàng '.$company['name'];
                $arr_data['report_file_name']='PRO_'.md5(time());
                $arr_data['report_orientation'] = 'portrait';
                Cache::write('cong_no_nam_chi_tiet', $arr_data);
            }
        } else
            $arr_data = Cache::read('cong_no_nam_chi_tiet');
        $this->render_pdf($arr_data);
    }

    function in_cong_no_nam($year_input){
    	if(!isset($_GET['print_pdf'])){
            $arr_order = $this->cong_no_nam_chi_tiet($year_input);
            $this->selectModel('Company');
            if(!empty($arr_order)){
                $group = array();
                $html = '';
                $i = 0;
                $arr_data = array();
                $tong_doanh_thu = 0;
                $tong_khoang_giam = 0;
                $tong_lai_thuc = 0;
                foreach($arr_order as $key=>$order){
                	$sum_amount = 0;
					foreach($order['sum_amount'] as $k => $v){
						$sum_amount += $v;
						$tong_doanh_thu += $v;
					}
					$khoang_giam = 0;
					foreach($order['khoang_giam'] as $kk => $vv){
						$khoang_giam += $vv;
						$tong_khoang_giam += $vv;
					}
					$loi_nhuan = 0;
					foreach($order['loi_nhuan'] as $kkk => $vvv){
						$loi_nhuan += $vvv;
						$tong_lai_thuc += $vvv;
					}
                    $html .= '<tr class="bg_1" style="border:1px solid">';
                    $html .= '<td class="center_text">'.' '.$order['company_name'].'</td>';
                    $html .= '<td class="right_text">'.(isset($order['sum_amount']) ? $this->opm->format_currency($sum_amount) : 0).'</td>';
                    $html .= '<td class="right_text">('. number_format($khoang_giam * -1) .')</td>';
                    $html .= '<td class="right_text">'. number_format($loi_nhuan).'</td>';
                    $html .= '<td class="right_text">'. number_format($loi_nhuan + $khoang_giam).'</td>';
                    $html .= '</tr>';
                    $i++;
                }
                $arr_data['title'] = array('Tên công ty' => 'width: 4%; text-align: center','Doanh số' => 'width: 8%; text-align: right','Khoảng giảm' =>'width: 8%;text-align: right;','Lãi thực' => 'width: 8%; text-align:right','Lợi nhuận'=>'width: 8%;text-align: right;');
                $html .='<tr class="last">
                            <td class="bold_text right_text right_none" style="border: 1px solid;border-right:1px solid !important">Tổng cộng</td>
                            <td class="right_text right_none" style="border: 1px solid">'.number_format($tong_doanh_thu,0).'</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($tong_khoang_giam * -1,0).'</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($tong_lai_thuc,0).'</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($tong_lai_thuc + $tong_khoang_giam,0).'</td>
                         </tr>';
                $html .= '<table style="margin-top: 25px">
                            <tr style="font-size:15px" >
                                <td style="text-align:center;width: 60%">Người lập<br />(Ký, họ tên)</td>
                                <td style="text-align:center;width: 60%">Trưởng đơn vị<br />(Ký, họ tên)</td>
                            </tr>
                            <tr>
                                <td ></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                        </table>';
                $arr_data['content'] = $html;
                $arr_data['report_name'] = 'Doanh thu năm '.$year_input;
                $arr_data['company_address'] = '<span style="font-size: 35px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
			           						ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
			           						<span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
			           						ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
			           						Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
			           						Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>';

                $arr_data['report_file_name']='PRO_'.md5(time());
                $arr_data['report_orientation'] = 'portrait';
                Cache::write('doanh_thu_nam', $arr_data);
            }
        } else
            $arr_data = Cache::read('doanh_thu_nam');
        $this->render_pdf($arr_data);
    }

    public function other_auto_save() {
    		$arr_return = array(
			'status'=>'error'
		);
		if (!empty($this->data) || isset($_POST['equipment_id'])) {
			$this->selectModel('Revenue');
			$arr_save = array();
			if( isset($_POST['equipment_id']) ){
				$arr_save = $this->Revenue->select_one(array('_id' => new MongoId($_POST['equipment_id'])));
				if(!isset($arr_save['color']))
					$arr_save['color'] = array();
				$arr_save['color'][$_POST['status']] = $_POST['color'];
			}else{
				$post = $this->data['Equipment'];
				$arr_save['_id'] = $post['_id'];
				$arr_save['date_paid'] = new MongoDate(strtotime(str_replace('/', '-',$post['date_paid'])));
				$amount_paid = str_replace(',','',$post['amount_paid']);
				$arr_save['amount_paid'] = doubleval($amount_paid);

				$arr_save['description'] = $post['description'];
				$arr_save['our_rep'] = $post['our_rep'];
				$arr_save['our_rep_id'] =( isset($post['our_rep_id']) && $post['our_rep_id']!='') ? new MongoId($post['our_rep_id']) : '';
				$arr_save['deleted'] = (isset($post['deleted']) ? true : false);
			}




			if ($this->Revenue->save($arr_save)) {
				$arr_revenue = $this->Revenue->select_all(array(
					'arr_where'=>array(),
					'arr_field'=>array('amount_paid')
				));

				$tong_chi_khac = 0;
				foreach ($arr_revenue as $key => $value) {
					if(isset($value['amount_paid']))
						$tong_chi_khac += $value['amount_paid'];
				}

				$arr_return = array(
					'status'=>'ok',
					'tong_chi_khac'=>number_format($tong_chi_khac),
					'tong_chi_khac_value'=>$tong_chi_khac
				);

			} else {
				$arr_return = array(
					'status'=>'error',
					'message'=>$this->Revenue->arr_errors_save[1]
				);
			}

		}
		header('Content-Type: application/json');
		echo json_encode($arr_return);
		die;
	}

	public function other_add() {
		$arr_save = array();
		$arr_save['date_paid'] =  new MongoDate(time());
		$this->selectModel('Revenue');
		if ($this->Revenue->save($arr_save)) {
			$id = $this->Revenue->mongo_id_after_save;
			echo json_encode(array('status' => 'ok', 'id' => (string)$id));
		} else {
			echo json_encode(array('status' => 'error', 'message' => 'Error: ' . $this->Revenue->arr_errors_save[1]));
		}
		die;
	}

	public function other_delete($id) {
		$arr_return = array(
			'status'=>'error'
		);

		 if(!$this->check_permission('tasks_@_entry_@_delete')){
			$arr_return = array(
				'status'=>'error',
				'message'=>'You do not have permission on this action.'
			);
			die;
		  }

		$this->selectModel('Revenue');
		$arr_save = $this->Revenue->select_one(array('_id' => new MongoId($id)));
		$arr_save['deleted'] = true;
		$delete = $this->Revenue->save($arr_save);
		if($delete){
			$arr_revenue = $this->Revenue->select_all(array(
					'arr_where'=>array(),
					'arr_field'=>array('amount_paid')
				));

				$tong_chi_khac = 0;
				foreach ($arr_revenue as $key => $value) {
					if(isset($value['amount_paid']))
						$tong_chi_khac += $value['amount_paid'];
				}

				$arr_return = array(
					'status'=>'ok',
					'tong_chi_khac'=>number_format($tong_chi_khac),
					'tong_chi_khac_value'=>$tong_chi_khac
				);

		}else{
			$arr_return = array(
					'status'=>'error',
					'message'=>$this->Revenue->arr_errors_save[1]
				);
		}
		header('Content-Type: application/json');
		echo json_encode($arr_return);
		die;
	}

}