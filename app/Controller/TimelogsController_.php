<?php
App::uses('AppController', 'Controller');
class TimelogsController extends AppController {

	var $modelName = 'Timelog';

	public function beforeFilter( ){
		// goi den before filter cha
		parent::beforeFilter();
	}

	function auto_save(){
		if(!empty($this->data)){
			$arr_post_data = $this->data['Timelog'];
			$arr_save = $arr_post_data;

			$arr_save['work_start'] = new MongoDate($this->Common->strtotime($arr_save['work_start'] . ' 00:00:00' ));
			$arr_save['work_end'] = new MongoDate($this->Common->strtotime($arr_save['work_end'] . ' 00:00:00' ));

			if(strlen(trim($arr_save['company_id'])) > 0)
			$arr_save['company_id'] = new MongoId($arr_save['company_id']);

			if(strlen(trim($arr_save['contact_id'])) > 0)
			$arr_save['contact_id'] = new MongoId($arr_save['contact_id']);

			$error = 0;
			if( !$error ){
				$this->selectModel('Timelog');
				if( $this->Timelog->save($arr_save) ){
					echo 'ok';
				}else{
					echo 'Error: ' . $this->Timelog->arr_errors_save[1];
				}
			}
		}
		die;
	}

	function delete($id=0){
		$arr_save['_id'] = $id;
		$arr_save['deleted'] = true;
		$error = 0;
		if( !$error ){
			$this->selectModel('Timelog');
			if( $this->Timelog->save($arr_save) ){
				$this->redirect('/timelogs/entry');
			}else{
				echo 'Error: ' . $this->Timelog->arr_errors_save[1];
			}
		}
		die;
	}

	function _add_get_info_save($work_start_sec = null){
		$this->selectModel('Timelog');
		$arr_tmp = $this->Timelog->select_one(array(), array(), array('no' => -1));
		$arr_save = array();
		$arr_save['no'] = 1;
		if(isset($arr_tmp['no'])){
			$arr_save['no'] = $arr_tmp['no'] + 1;
		}
		$arr_save['status'] = 0;
		$arr_save['name'] = '';
		$arr_save['company_name'] = '';
		$arr_save['contacts_default_key'] = 0;
		$arr_save['contacts'][] = array(
			"contact_name" => $_SESSION['arr_user']['contact_name'],
			"contact_id" => $_SESSION['arr_user']['contact_id'],
			"default" => true,
			"deleted" => false
	    );

		if(isset($work_start_sec) && $work_start_sec > 0){
			$arr_save['work_end'] = $arr_save['work_start'] = new MongoDate($work_start_sec);
		}else{

			$arr_save['work_end'] = $arr_save['work_start'] = new MongoDate(strtotime(date('Y-m-d H:00:00')) + 3600);
		}
		return $arr_save;
	}

	public function add($work_start_sec = 0){
		$arr_save = $this->_add_get_info_save($work_start_sec );

		if( $this->Timelog->save($arr_save) ){
			$this->redirect('/timelogs/entry/' . $this->Timelog->mongo_id_after_save);
		}else{
			echo 'Error: ' . $this->Timelog->arr_errors_save[1];
		}
		die;
	}

	public function entry( $id = '0', $num_position = -1 ){// echo date('d/m/Y', strtotime('2 Jul, 2013'));die;

		$this->redirect('/stages/entry');

		$this->selectModel('Timelog');
		if($id != '0'){
			if($id == 'first'){ // called from menu_entry.ctp
				$num_position = 1;
				$arr_tmp = $this->Timelog->select_one(array(), array(), array('_id' => 1));
			}else{
				$arr_tmp = $this->Timelog->select_one(array('_id' => new MongoId($id)));
			}
		}else{
			$arr_tmp = $this->Timelog->select_one(array(), array(), array('no' => -1));
		}
		if(!isset($arr_tmp['_id'])){ $this->redirect('/timelogs/add'); die; }

		// this valriable array use in menu_entry.ctp
		$arr_prev = $this->Timelog->select_one(array('_id' => array('$lt' => $arr_tmp['_id'])), array('_id'), array('_id' => -1));
		$this->set( 'arr_prev', $arr_prev );
		$arr_next = $this->Timelog->select_one(array('_id' => array('$gt' => $arr_tmp['_id'])), array('_id'), array('_id' => 1));
		$this->set( 'arr_next', $arr_next );
		$sum = $this->Timelog->count();
		$this->set( 'sum', $sum );
		if($num_position == -1){
			$num_position = $sum;
		}
		$this->set( 'num_position', $num_position ); // đếm entry thứ mấy trong tất cả entry

		$arr_tmp['work_start'] = (is_object($arr_tmp['work_start']))?date('m/d/Y', $arr_tmp['work_start']->sec):'';
		$arr_tmp['work_end'] = (is_object($arr_tmp['work_end']))?date('m/d/Y', $arr_tmp['work_end']->sec):'';

		$arr_tmp1['Timelog'] = $arr_tmp;
		$this->data = $arr_tmp1;

		$this->selectModel('Setting');
		$this->set( 'arr_timelogs_type', $this->Setting->select_option(array('setting_value' => 'timelogs_type'), array('option')) );
		$this->set( 'arr_timelogs_status', $this->Setting->select_option(array('setting_value' => 'timelogs_status'), array('option')) );

		$arr_contact_id = array();
		$arr_contact_id[] = $arr_tmp['created_by'];
		$arr_contact_id[] = $arr_tmp['modified_by'];
		if(isset($arr_tmp['our_rep_id']))
			$arr_contact_id[] = $arr_tmp['our_rep_id'];
		if(isset($arr_tmp['contact_id']))
			$arr_contact_id[] = $arr_tmp['contact_id'];
		$this->selectModel('Contact');
		$arr_contact = $this->Contact->select_list(array(
			'arr_where' => array(
				'_id' => array('$in' => $arr_contact_id)
			),
			'arr_field' => array('_id', 'first_name', 'last_name'),
		));
		$this->set( 'arr_contact', $arr_contact );

		// show info user in the footer
		$arr_info_footer = array();
		$arr_info_footer['date_created'] = date('d M, Y', $arr_tmp['_id']->getTimestamp());
		$arr_info_footer['date_created_hour'] = date('H:i', $arr_tmp['_id']->getTimestamp());
		$arr_info_footer['date_modified'] = date('d M, Y', $arr_tmp['date_modified']->sec);
		$arr_info_footer['date_modified_hour'] = date('H:i', $arr_tmp['date_modified']->sec);
		$arr_info_footer['created_by'] = $arr_contact[(string)$arr_tmp['created_by']];
		$arr_info_footer['modified_by'] = $arr_contact[(string)$arr_tmp['modified_by']];
		$this->set( 'arr_info_footer', $arr_info_footer );

		// Get info for subtimelog
		$this->sub_tab('', $arr_tmp['_id']);

		// Get info for subtimelog
		$this->sub_tab('', $arr_tmp['_id']);
	}

	var $name = 'Timelogs';
	var $sub_tab_default = 'general';

	function resources_auto_save(){
		foreach ($this->data['Timelog'] as $value) {
			$arr_save = $value;
		}
		$arr_save['work_start'] = new MongoDate($this->Common->strtotime($arr_save['work_start'] . '' . $arr_save['work_start_hour'] . ':00'));
		$arr_save['work_end'] = new MongoDate($this->Common->strtotime($arr_save['work_end'] . '' . $arr_save['work_end_hour'] . ':00'));
		$this->selectModel('Timelog');
		if( $this->Timelog->save($arr_save) ){
			echo 'ok';
		}else{
			echo 'Error: ' . $this->Timelog->arr_errors_save[1];
		}
		die;
	}

	function general($timelog_id){
		$this->set('timelog_id', $timelog_id);

		$this->selectModel('Timelog');
		$arr_timelog = $this->Timelog->select_one(array('_id' => new MongoId($timelog_id)), array('contacts'));
		$this->set('arr_timelog', $arr_timelog);

	}

	function general_window_contact_choose($timelog_id, $contact_id, $contact_name){

		$this->selectModel('Timelog');
		$arr_timelog = $this->Timelog->select_one(array('_id' => new MongoId($timelog_id)), array('contacts'));
		$check_not_exist = true;
		if( isset($arr_timelog['contacts']) ){
			foreach ($arr_timelog['contacts'] as $value) {
				if( (string)$value['contact_id'] == $contact_id ){
					$check_not_exist = false;
				}
			}
		}

		if($check_not_exist){

			$this->Timelog->collection->update(
				array('_id' => new MongoId($timelog_id)),
				array('$push' => array(
						'contacts' => array (
							'contact_name' => $contact_name,
							'contact_id' => new MongoId($contact_id),
							'default' => false,
							'deleted' => false
						)
					)
				)
			);
			echo 'ok';

		}else{
			echo 'Error this contact is selected before';
		}
		die;
		// $this->addresses($company_id);
		// $this->render('addresses');
	}

	function general_choose_manager( $timelog_id, $option_id, $sum ){

		// gán lại key deleted để không bị mất
		$this->selectModel('Timelog');

		$id = new MongoId($timelog_id);
		for ($i=0; $i < $sum; $i++) {
			$this->Timelog->collection->update(
				array('_id' => $id),
				array('$set' => array( 'contacts.'.$i.'.default' => false ) )
			);
		}
		$this->Timelog->collection->update(
			array('_id' => $id),
			array('$set' => array( 'contacts.'.$option_id.'.default' => true, 'contacts_default_key' => $option_id ) )
		);

		echo 'ok';
		die;
	}

	function general_delete_contact( $timelog_id, $key ){

		$this->selectModel('Timelog');
		$this->Timelog->collection->update(
			array('_id' => new MongoId($timelog_id)),
			array('$set' => array(
					'contacts.'.$key.'.deleted' => true
				)
			)
		);
		echo 'ok';
		die;
	}

	function resources($timelog_id){

		// get all equipments are used for this timelog
		$this->selectModel('Timelog');
		$arr_timelog = $this->Timelog->select_all(array(
			'arr_where' => array('timelog_id' => new MongoId($timelog_id)),
			'arr_order' => array('work_start' => 1)
		));
		$this->set( 'arr_timelog', $arr_timelog );
		$this->set( 'timelog_id', $timelog_id );
		$this->selectModel('Setting');
		$this->set( 'arr_timelogs_status', $this->Setting->select_option(array('setting_value' => 'timelogs_status'), array('option')) );
	}

	function resources_delete($timelog_id){
		$arr_save['_id'] = $timelog_id;
		$arr_save['deleted'] = true;
		$this->selectModel('Timelog');
		if( $this->Timelog->save($arr_save) ){
			echo 'ok';
		}else{
			echo 'Error: ' . $this->Timelog->arr_errors_save[1];
		}
		die;
	}

	function resources_window_list_contact($timelog_id){
		$this->selectModel('User');
		$arr_user = $this->User->select_all();
		$this->set( 'arr_user', $arr_user );
		$this->set( 'timelog_id', $timelog_id );
	}

	function resources_window_choose($timelog_id, $type, $name = '' ){
		$arr_save['name'] = $name;
		$arr_save['type'] = $type;
		$arr_save['status'] = 0;
		$arr_save['timelog_id'] = new MongoId($timelog_id);

		$this->selectModel('Timelog');
		$arr_timelog = $this->Timelog->select_one(array('_id' => new MongoId($timelog_id)));

		$arr_save['work_start'] = $arr_timelog['work_start'];
		$arr_save['work_end'] = $arr_timelog['work_end'];
		$this->selectModel('Timelog');
		if( $this->Timelog->save($arr_save) ){
			echo 'ok';
		}else{
			echo 'Error: ' . $this->Timelog->arr_errors_save[1];
		}
		die;
	}

	function resources_window_list_asset($timelog_id){
		$this->selectModel('Equipment');
		$arr_equipment = $this->Equipment->select_all();
		$this->set( 'arr_equipment', $arr_equipment );
		$this->set( 'timelog_id', $timelog_id );
	}

	function timelog(){
	}
	function expensive(){
	}

	function other(){
	}

	function lists(){
		$this->selectModel('Timelog');
		$arr_timelogs = $this->Timelog->select_all(array(
			'arr_order' => array('_id' => -1)
		));

		$this->selectModel('Setting');
		$this->set( 'arr_timelogs_type', $this->Setting->select_option(array('setting_value' => 'timelogs_type'), array('option')) );
		$this->set( 'arr_timelogs_status', $this->Setting->select_option(array('setting_value' => 'timelogs_status'), array('option')) );

		// Lấy tên các user chịu trách nhiệm timelogs
		$arr_contact_id = $arr_company_id = array(); $arr_tmp_timelog = array();
		foreach ($arr_timelogs as $value) {
			$arr_company_id[] = $value['company_id'];
			$arr_contact_id[] = $value['contact_id'];
			$arr_tmp_timelog[] = $value;
		}

		$this->set( 'arr_timelogs', $arr_tmp_timelog );

		// CONTACT - User responsible
		$arr_contact_id = array_unique($arr_contact_id);
		$arr_contacts = array();
		if( !empty($arr_contact_id) ){
			$this->selectModel('Contact');
			$arr_contacts = $this->Contact->select_list(array(
				'arr_where' => array(
					'_id' => array('$in' => $arr_contact_id)
				),
				'arr_field' => array('_id', 'first_name', 'last_name')
			));
		}
		$this->set( 'arr_contacts', $arr_contacts );

		// Company
		$arr_company_id = array_unique($arr_company_id);
		$arr_companiess = array();
		if( !empty($arr_company_id) ){
			$this->selectModel('Company');
			$arr_companiess = $this->Company->select_list(array(
				'arr_where' => array(
					'_id' => array('$in' => $arr_company_id)
				),
				'arr_field' => array('_id', 'name')
			));
		}
		$this->set( 'arr_companiess', $arr_companiess );
	}

	function lists_delete($id=0){
		$arr_save['_id'] = $id;
		$arr_save['deleted'] = true;
		$error = 0;
		if( !$error ){
			$this->selectModel('Timelog');
			if( $this->Timelog->save($arr_save) ){
				echo 'ok';
			}else{
				echo 'Error: ' . $this->Timelog->arr_errors_save[1];
			}
		}
		die;
	}

	// ================================== CALENDAR ====================================
	public function calendar( $date_from_sec = '', $date_to_sec = '' ){ // calendar week

		$this->set('set_footer', '../Communications/calendar_footer');
		$this->Session->write('calendar_last_visit', '/' . $this->request->url);

		$this->layout = 'calendar';
	}

	public function calendar_change(){
		$arr_post_data = $this->data;
		$arr_save['work_start'] = new MongoDate(strtotime($arr_post_data['work_start']));
		$arr_save['work_end'] = new MongoDate(strtotime($arr_post_data['work_end']));
		$arr_save['_id'] = $arr_post_data['id'];
		$this->selectModel('Timelog');
		if( $this->Timelog->save($arr_save) ){
			echo 'ok';
		}else{
			echo 'Error: ' . $this->Timelog->arr_errors_save[1];
		}
		die;
	}
}