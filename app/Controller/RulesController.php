<?php
/*Note for constrution
	Setup Module Controler:
		- Change Name: số nhiều: $name, Class NameController; số ít: $this->selectModel, $this->opm
		- Remove code in //BEGIN special ...//END special
		- Remove SubTab functions

	Session:
		- $name.'Subtab' : session Subtab that is relationship table
		- $name.'ViewId' : session lưu _id của record vừa xem

	$opm : Opjects của Module, kế thừa các function của class Module
	$this->params->params : lưu trữ các giá trị của URL

	Basic function : beforeFilter, index, entry, add, list, delete, ajax_add_update
	Relationship function: general, costings, pricing, others,..

	Biến tên module:
		- $this->name : Tên module viết hoa chữ đầu, số nhiều     							Products
		- arr_settings['module_name'] : Tên module viết hoa chữ đầu, số ít					Product
		- $controller : Tên module viết thường số nhiều. đã set view trong AppController	products

*/
App::uses('AppController', 'Controller');
class RulesController extends AppController {

	var $name = 'Rules';
	public $helpers = array();
	public $opm; //Option Module

	public function beforeFilter(){
		parent::beforeFilter();
		//$this->set: name, arr_settings, arr_options, iditem, entry_menu
		$this->set_module_before_filter('Rule');
	}

	public function entry(){
		$arr_set = $this->opm->arr_settings;

		// Check url to get value id
		$iditem = $this->get_id();
		if($iditem=='')
			$iditem = $this->get_last_id();

		$this->set('iditem',$iditem);
		//Load record by id
		if($iditem!=''){
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
			foreach($arr_set['field'] as $ks => $vls){
				foreach($vls as $field => $values){
					if(isset($arr_tmp[$field])){
						$arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
						if(in_array($field,$arr_set['title_field']))
							$item_title[$field] = $arr_tmp[$field];
					}
				}
			}
			$arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
			$this->Session->write($this->name.'ViewId',$iditem);

			//BEGIN special
			if(isset($arr_set['field']['panel_1']['code']['default']))
				$item_title['code'] = $arr_set['field']['panel_1']['code']['default'];
			else
				$item_title['code'] = '1';

			if(isset($arr_set['field']['panel_2']['map_formula']['default']))
				$this->set('map_formula',$arr_set['field']['panel_2']['map_formula']['default']);
			//END special


			$this->set('item_title',$item_title);



		//add, setup field tự tăng
		}else{
			$codeauto = $this->opm->max_field('code');
			if(isset($codeauto['code']))
				$codeauto = 1+(int)$codeauto['code'];
			else
				$codeauto = 1;
			$arr_set['field']['panel_1']['code']['default'] = $codeauto;
			$this->set('item_title',array('code'=>$codeauto));
		}

		$this->set('arr_settings',$arr_set);
		$this->sub_tab_default = 'general';
		$this->sub_tab();

		$this->selectModel('Product');
		$this->Product->arrfield();
		$this->set('product_arrfield',$this->Product->arr_temp);
		parent::entry();
	}


	//Add or update field when this field change, use in js.ctp
	public function ajax_save(){
		if(isset($_POST['field']) && isset($_POST['value']) && isset($_POST['func']) && !in_array((string)$_POST['field'],$this->opm->arr_autocomplete()) ){

			if($_POST['func']=='add')
				$ids = $this->opm->add($_POST['field'],$_POST['value']);
			else if($_POST['func']=='update' && isset($_POST['ids'])){
				$ids = $this->opm->update($_POST['ids'],$_POST['field'],$_POST['value']);
				$this->Session->write($this->name.'ViewId',$_POST['ids']);
			}
			echo $ids;
		}else
			echo 'error';
		die;
	}

	//Add or update field when this field change, use in js.ctp
	public function ajax_box_update(){
		$ids = $this->get_id();
		if($ids!='' && isset($_POST['moduleop']) && isset($_POST['field']) && isset($_POST['value'])){
			$arr_vl = array();
			$arr_vl['moduleids'] = $ids;
			$arr_vl['moduleop'] = $_POST['moduleop'];
			$arr_vl['field'] = $_POST['field'];
			$arr_vl['value'] = $_POST['value'];

			$idm = $_POST['ids'];
			$arr_vl['opwhere'] = array('id'=>$idm);
			if($_POST['field']=='current'){
				$arr_vl['ids'] = $_POST['ids'];
				$bbl = $this->opm->update_current_default($arr_vl);
			}else
				$bbl = $this->opm->update_value_option_of_module($arr_vl);

			//echo $bbl;
			if($bbl)
				echo $_POST['value'];
			else
				echo '';
		}
		die;
	}

	// Save option
	public function ajax_save_option(){
		if($this->Session->check($this->name.'ViewId') && isset($_POST['arr'])){
			$idsession = $this->Session->read($this->name.'ViewId');
			$arr_tmp = $this->opm->select_one(array('_id' => new MongoId($idsession)));

			$ndt = $_POST['arr'];
			$cls = $ndt[1];//company

			// kiem tra xem co ton tai gia tri chua
			if(isset($arr_tmp[$cls]) && is_array($arr_tmp[$cls])){
				$m = 0;
				foreach($arr_tmp[$cls] as $arr_tmp)
					if(isset($arr_tmp['id']) && (int)$arr_tmp['id'] > $m)
						$m = (int)$arr_tmp['id'];

				$options[0]['id'] = $m;
			}else
				$options[0]['id'] = '0';

			//setup default option field
			$name_field = $this->opm->arr_field_key('rel_name');
			$keys = $name_field[$cls];//supplier

			$name_field = $this->opm->arr_field_rel();
			$name_field = $name_field[$keys];
			foreach($name_field as $ks=>$vs){ // foreach supplier['field']
				if(isset($vs['default']))
					$options[0][$ks] = $vs['default'];
				else
					$options[0][$ks] = '';
			}
			$options[0][$cls.'_name'] = $ndt[2];// value của company_name
			$options[0][$cls.'_id'] = $ndt[3];// value của company_id

			$data_insert[$cls] = $options;
			$data_insert['_id'] = $idsession;
			if(isset($arr_tmp['name']))
			$data_insert['name'] = $arr_tmp['name'];


			if($this->opm->save($data_insert))//pr($data_insert);
				echo $this->opm->mongo_id_after_save;


		}else
			echo 'error';
		die;

	}

	public function general(){
		$subdatas = $this->opm->get_data_general($this->get_id());
		$this->set('subdatas', $subdatas);
	}

	public function others(){
	}
}