<?php
App::uses('AppController', 'Controller');
class CasereportsController extends AppController {

	var $name = 'Casereports';
	public $helpers = array();
	public $opm; //Option Module
	public function beforeFilter(){
		parent::beforeFilter();
		$this->set_module_before_filter('Casereport');
		$this->sub_tab_default = 'general';
	}


	public function rebuild_setting($arr_setting=array()){
		// parent::rebuild_setting($arr_setting);
         $arr_setting = $this->opm->arr_settings;
        if(!$this->check_permission($this->name.'_@_entry_@_edit')){
            $arr_setting = $this->opm->set_lock(array(),'out');
            $this->set('address_lock', '1');
        }
        $this->opm->arr_settings = $arr_setting;
        $arr_tmp = $this->opm->arr_field_key('cls');
        $arr_link = array();
        if(!empty($arr_tmp))
            foreach($arr_tmp as $key=>$value)
                $arr_link[$value][] = $key;
        $this->set('arr_link',$arr_link);
	}

	// Add action
    public function add() {
		$this->selectModel('Company');
        $query = $this->Company->select_one(array('system' => true));
		$arr_more = $query;
		if(isset($arr_more) && is_array($arr_more) && count($arr_more)>0){
			//lưu các giá trị của company system vào loation
			if(isset($arr_more['name']))
				$arr_tmp['company_name'] = $arr_more['name'];
			if(isset($arr_more['_id']))
				$arr_tmp['company_id'] = $arr_more['_id'];
			if(isset($arr_more['phone']))
				$arr_tmp['phone'] = $arr_more['phone'];
			if(isset($arr_more['fax']))
				$arr_tmp['fax'] = $arr_more['fax'];
			if(isset($arr_more['email']))
				$arr_tmp['email'] = $arr_more['email'];
			if(isset($arr_more['addresses'][0])){
				foreach($arr_more['addresses'][0] as $kk=>$vv){
					$arr_tmp['shipping_address'][0]['shipping_'.$kk] = $vv;
				}
			}
			//contact
			if(isset($arr_more['contact_default_id'])){
				$this->selectModel('Contact');
        		$query = $this->Contact->select_one(array('_id' => new MongoId($arr_more['contact_default_id'])));
				$contact = $query;
				if(isset($contact['first_name']))
					$arr_tmp['contact_name'] = $contact['first_name'].' ';
				if(isset($contact['last_name']))
					$arr_tmp['contact_name'] .= $contact['last_name'];
				if(isset($contact['_id']))
					$arr_tmp['contact_id'] = $contact['_id'];
			}
		}

        $ids = $this->opm->add('name', '',$arr_tmp);
        $newid = explode("||", $ids);
        $this->Session->write($this->name . 'ViewId', $newid[0]);
        $this->redirect('/' . $this->params->params['controller'] . '/entry');
        die;
    }



	//Entry - trang chi tiet
 	public function entry() {
        $arr_set = $this->opm->arr_settings;
        $arr_tmp = array();
        // Get value id
        $iditem = $this->get_id();
        if ($iditem == '')
            $iditem = $this->get_last_id();

        $this->set('iditem', $iditem);
        //Load record by id
        if ($iditem != '') {
            $arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
            foreach ($arr_set['field'] as $ks => $vls) {
                foreach ($vls as $field => $values) {
                    if (isset($arr_tmp[$field])) {
                        $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
                        if (preg_match("/_date$/", $field) && is_object($arr_tmp[$field]))
                            $arr_set['field'][$ks][$field]['default'] = date('m/d/Y', $arr_tmp[$field]->sec);
                        if (in_array($field, $arr_set['title_field']))
                            $item_title[$field] = $arr_tmp[$field];
                        if ($field == 'contact_name' && isset($arr_tmp['contact_last_name'])) {
                            $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field] . ' ' . $arr_tmp['contact_last_name'];
                            $item_title['contact_name'] = $arr_tmp[$field] . ' ' . $arr_tmp['contact_last_name'];
                        }
                    }
                }
            }

            $arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
            $this->Session->write($this->name . 'ViewId', $iditem);

            //BEGIN custom
            if (isset($arr_set['field']['panel_1']['code']['default']))
                $item_title['code'] = $arr_set['field']['panel_1']['code']['default'];
            else
                $item_title['code'] = '1';
            $this->set('item_title', $item_title);

            //END custom
            $this->set('address_lock', '1');
            //END custom
            //show footer info
            $this->show_footer_info($arr_tmp);


            //add, setup field tự tăng
        }else {
            $nextcode = $this->opm->get_auto_code('code');
            $arr_set['field']['panel_1']['code']['default'] = $nextcode;
            $this->set('item_title', array('code' => $nextcode));
        }

        $this->set('arr_settings', $arr_set);

       /* $option_select_custom= array();
        $option_select_custom['note_type'] = array('hoangvu'=>'HoangVu','hvy'=>'HVy','htr'=>'HTr');
        $this->set('option_select_custom', $option_select_custom);*/

        $this->sub_tab_default = 'general';
        $this->sub_tab('', $iditem);
        parent::entry();
    }

    
	
	//Associated data function
	public function arr_associated_data($field = '', $value = '', $valueid = '') {
        $arr_return = array();
		$arr_return[$field] = $value;
		// ..........more code
        if ($field == 'company') 
            $arr_return['company_id'] = new MongoId($valueid);

        if ($field == 'assignto') 
            $arr_return['assignto_id'] = new MongoId($valueid);


       /* if ($field == 'note_by') { 
            $arr_return['note_activity']['field']['note_type']
            $arr_return['note_by_id'] = new MongoId($valueid);
        }*/

        return $arr_return;
    }


	//Search function
	public function entry_search() {
        
    }

	//Swith options function
    public function swith_options($option = ''){
        parent::swith_options($option);
    }
	
	//Subtab function
	public function general() {
        $subdatas = array();
        $iditem = $this->get_id();
        $arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
        
        if (isset($arr_tmp['detail']))
        {
            $subdatas['detail'] = array('detail'=>$arr_tmp['detail']);
        }
        else{
             $subdatas['detail'] = array('detail'=>'...');
        }
        
       /* $option_select_custom= array();
        $option_select_custom['note_type'] = array('Inc'=>'Inc','Exc'=>'Exc');
        $this->set('option_select_custom', $option_select_custom);*/
        
        $this->set('subdatas', $subdatas);
    }
	
	//Subtab function
	public function note_activity() {
        $subdatas = array();
        $subdatas['note_activity'] = $this->get_option_data('note_activity');
        //pr($subdatas);die;
        $this->set('subdatas', $subdatas);

        $option_select_custom = array();
        $option_select_custom['note_type'] = array('Inc'=>'Inc','Exc'=>'Exc','hoangvu'=>'HoangVu');
        $this->set('option_select_custom', $option_select_custom);


    }

	// Popup form orther module
    public function popup($key = '') {
		parent::popup($key);
    }


    public function save_option1() {
        $this->selectModel('Casereport');
        $query = $this->Casereport->select_one();
        $arr_more = $query;
      
        $this->set('arr_more',  $arr_more);

    }

    

}