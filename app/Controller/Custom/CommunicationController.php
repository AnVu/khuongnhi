<?php
App::uses('CommunicationsController', 'Controller');
class CommunicationsControllerCustom extends CommunicationsController{
 	public function beforeFilter() {
        parent::beforeFilter();
    }

    function _entry_other(){
        $arr_set = $this->opm->arr_settings;
        // Get value id
        $iditem = $this->get_id();
        if ($iditem == '')
            $iditem = $this->get_last_id();
        $this->set('iditem', $iditem);
        //Load record by id
        if ($iditem != '') {
            $arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
            //echo strtolower($arr_tmp['comms_type']);die;
            if (isset($arr_tmp['comms_type'])) {
                $arr_set['field'] = $this->opm->custom_entry_layout(strtolower($arr_tmp['comms_type']));
                $arr_set['module_label'] = $arr_tmp['comms_type'];
            }
            foreach ($arr_set['field'] as $ks => $vls) {
                foreach ($vls as $field => $values) {
                    if (isset($arr_tmp[$field])) {
                        if(isset($arr_tmp['comms_status'])&&$arr_tmp['comms_status']=='Sent')
                            $arr_set['field'][$ks][$field]['lock'] = 1;
                        $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
                        if(is_object($arr_tmp[$field])&&strpos($field,'_date'))
                        {
                            $arr_set['field'][$ks][$field]['default'] = date('M d, Y',$arr_tmp[$field]->sec);
                        }
                        if (in_array($field, $arr_set['title_field']))
                            $item_title[$field] = $arr_tmp[$field];
                        if(is_object($arr_tmp[$field])&&strpos($field,'_date'))
                                $item_title[$field] = date('D, M d, Y',$arr_tmp[$field]->sec);
                    }
                }
            }
            $arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
            if(isset($arr_tmp['content']))
               $this->set('content',$arr_tmp['content']);
            $this->selectModel('Contact');
            $contact = $this->Contact->select_one(array('_id'=>new MongoId($this->opm->user_id())),array('email'));
            $this->set('current_email',(isset($contact['email']) ? $contact['email'] : ''));
            $this->Session->write($this->name . 'ViewId', $iditem);

            //BEGIN custom
            $this->set('item_title', isset($item_title) ? $item_title : '');

            $datas = $this->general_select($arr_set['field']);
            if (isset($arr_tmp['company_id']) && $arr_tmp['company_id'] != '')
                $datas['email_cc'] = $datas['email_bcc'] = $this->reload_cc($arr_tmp['company_id']);

            $this->set('status',(isset($arr_tmp['comms_status']) ? $arr_tmp['comms_status'] : ''));
            $this->set("arr_options", $datas);
            //END custom
            //show footer info
            $this->show_footer_info($arr_tmp);


            //add, setup field tự tăng
        }else {
            $nextcode = $this->opm->get_auto_code('code');
            $arr_set['field']['panel_1']['code']['default'] = $nextcode;
            $this->set('item_title', array('code' => $nextcode));
        }
        $this->set('arr_settings', $arr_set);
        $this->set('attachment',$this->get_attachment($iditem));
        //pr($arr_set);
        //address
        if (isset($arr_tmp) && isset($arr_tmp['comms_type']) && ($arr_tmp['comms_type'] == 'Letter' || $arr_tmp['comms_type'] == 'Fax')) {
            $address_fset = array('address_1', 'address_2', 'address_3', 'town_city', 'country', 'province_state', 'zip_postcode');
            $address_value = $address_province_name = $address_country_name = $address_province = array();
            $address_value['contact'] = array('', '', '', '', "VN", '', '');
            $address_controller = array('contact');
            $this->set('address_controller', $address_controller); //set
            $address_key = array('contact');
            $this->set('address_key', $address_key); //set
            foreach ($address_key as $kss => $vss) {
                if (isset($arr_tmp[$vss . '_address'][0])) {
                    $arr_temp_op = $arr_tmp[$vss . '_address'][0];
                    for ($i = 0; $i < count($address_fset); $i++) {
                        if (isset($arr_temp_op[$vss . '_' . $address_fset[$i]])) {
                            $address_value[$vss][$i] = $arr_temp_op[$vss . '_' . $address_fset[$i]];
                        } else {
                            $address_value[$vss][$i] = '';
                        }
                    }
                    $arr_country = $this->country();
                    $arr_province = $this->province();
                    if (isset($arr_temp_op[$vss . '_province_state']) && isset($arr_province[$arr_temp_op[$vss . '_province_state']]))
                        $address_province_name[$kss] = $arr_province[$arr_temp_op[$vss . '_province_state']];
                    else
                        $address_province_name[$kss] = '';

                    if (isset($arr_temp_op[$vss . '_country'])) {
                        $v_country = (int) $arr_temp_op[$vss . '_country'];
                        $address_country_name[$kss] = $arr_country[$v_country];
                        $address_province[$vss] = $this->province($v_country);
                    } else {
                        $v_country = "VN";
                        $address_country_name[$kss] = $arr_country[$v_country];
                        $arr_temp = $this->province('', $v_country);
                        $address_province[$vss][''] = '';
                        $address_province[$vss] = array_merge($address_province[$vss], $arr_temp);
                    }
                } else {
                    $v_country = "VN";
                    $arr_temp = $this->province('', $v_country);
                    $address_province[$vss][''] = '';
                    $address_province[$vss] = array_merge($address_province[$vss], $arr_temp);
                }
            }
            //pr($address_province);
            $this->set('address_value', $address_value);
            $address_hidden_field = array('contact_address');
            $this->set('address_hidden_field', $address_hidden_field); //set
            $address_label[0] = $arr_set['field']['panel_3']['contact_address']['name'];
            $this->set('address_label', $address_label); //set
            $address_conner[0]['top'] = 'hgt';
            $address_conner[0]['bottom'] = 'fixbor3 jt_ppbot';
            $this->set('address_conner', $address_conner); //set
            $address_country = $this->country();
            $this->set('address_country', $address_country); //set
            $this->set('address_country_name', $address_country_name); //set
            $this->set('address_province', $address_province); //set
            $this->set('address_province_name', $address_province_name); //set
            $this->set('address_more_line', 0); //set
            $this->set('address_onchange', "save_address_pr('\"+keys+\"');");
            $this->set('address_botclass', '');
        }
    }
}