<?php
App::uses('CompaniesController', 'Controller');
class CompaniesControllerCustom extends CompaniesController{
 	public function beforeFilter() {
        parent::beforeFilter();
    }

    public function rebuild_setting($arr_setting=array()){
        // parent::rebuild_setting($arr_setting);
         $arr_setting = $this->opm->arr_settings;
        if(!$this->check_permission($this->name.'_@_entry_@_edit')){
            $arr_setting = $this->opm->set_lock(array(),'out');
            $this->set('address_lock', '1');
        }
        $this->selectModel('Company');
        $arr_tmp = $this->Company->select_one(array('_id'=>new MongoId($this->get_id())),array('is_supplier','is_customer'));
        if($arr_tmp['is_customer']==1 && $arr_tmp['is_supplier'] != 1){
            unset($arr_setting['relationship']['products']['block'][1]);
            unset($arr_setting['relationship']['products']['block']['same_business_type_companies']);
            unset($arr_setting['relationship']['orders']['block']['orders_supplier']);
        }else if($arr_tmp['is_supplier'] == 1 && $arr_tmp['is_customer']!=1){
            unset($arr_setting['relationship']['products']['block']['pricing_category']);
            unset($arr_setting['relationship']['products']['block'][4]);
            unset($arr_setting['relationship']['products']['block'][5]);
            unset($arr_setting['relationship']['orders']['block']['orders_cus']);
        }else if($arr_tmp['is_customer'] != 1 && $arr_tmp['is_supplier'] != 1){
            unset($arr_setting['relationship']['products']['block'][1]);
            unset($arr_setting['relationship']['products']['block']['same_business_type_companies']);
            unset($arr_setting['relationship']['orders']['block']['orders_supplier']);
        }
        $this->opm->arr_settings = $arr_setting;
        $arr_tmp = $this->opm->arr_field_key('cls');
        $arr_link = array();
        if(!empty($arr_tmp))
            foreach($arr_tmp as $key=>$value)
                $arr_link[$value][] = $key;
        $this->set('arr_link',$arr_link);
    }

    public function set_entry_address($arr_tmp, $arr_set) {
        $address_fset = array('address_1', 'address_2', 'address_3', 'town_city', 'country', 'province_state', 'zip_postcode');
        $address_value = $address_province_id = $address_country_id = $address_province = $address_country = array();
        $address_controller = array('company');
        $address_value['company'] = array('', '', '', '', "VN", '', '');
        $this->set('address_controller', $address_controller); //set
        $address_key = array('company');
        $this->set('address_key', $address_key); //set
        $address_country = $this->country();
        $arr_address_tmp = array();
        if(!isset($arr_tmp['addresses_default_key']))
            $arr_tmp['addresses_default_key'] = 0;
        if(isset($arr_tmp['addresses'][$arr_tmp['addresses_default_key']])){
            foreach($arr_tmp['addresses'][$arr_tmp['addresses_default_key']] as $key=>$value){
                if($key=='deleted') continue;
                $arr_address_tmp['company_'.$key] = $value;
            }
        }
        $arr_tmp['company_address'][0] = array();
        foreach ($address_key as $kss => $vss) {
            //neu ton tai address trong data base
            if (isset($arr_tmp[$vss . '_address'][0])) {
                if(!empty($arr_address_tmp))
                    $arr_tmp[$vss . '_address'][0] = $arr_address_tmp;
                $arr_temp_op = $arr_tmp[$vss . '_address'][0];
                for ($i = 0; $i < count($address_fset); $i++) { //loop field and set value for display
                    if (isset($arr_temp_op[$vss . '_' . $address_fset[$i]])) {
                        $address_value[$vss][$i] = $arr_temp_op[$vss . '_' . $address_fset[$i]];
                    } else {
                        $address_value[$vss][$i] = '';
                    }
                }//pr($arr_temp_op);die;
                //get province list and country list

                if (isset($arr_temp_op[$vss . '_country_id']))
                    $address_province[$vss] = $this->province($arr_temp_op[$vss . '_country_id']);
                else
                    $address_province[$vss] = $this->province();
                //set province
                if (isset($arr_temp_op[$vss . '_province_state_id']) && $arr_temp_op[$vss . '_province_state_id'] != '' && isset($address_province[$vss][$arr_temp_op[$vss . '_province_state_id']]))
                    $address_province_id[$kss] = $arr_temp_op[$vss . '_province_state_id'];
                else if (isset($arr_temp_op[$vss . '_province_state']))
                    $address_province_id[$kss] = $arr_temp_op[$vss . '_province_state'];
                else
                    $address_province_id[$kss] = '';

                //set country
                if (isset($arr_temp_op[$vss . '_country_id'])) {
                    $address_country_id[$kss] = $arr_temp_op[$vss . '_country_id'];
                    $address_province[$vss] = $this->province($arr_temp_op[$vss . '_country_id']);
                } else {
                    $address_country_id[$kss] = "VN";
                    $address_province[$vss] = $this->province("VN");
                }

                $address_add[$vss] = '0';
                //chua co address trong data
            } else {
                $address_country_id[$kss] = "VN";
                $address_province[$vss] = $this->province("VN");
                $address_add[$vss] = '1';
            }
        }
        $this->set('address_value', $address_value);
        $address_hidden_field = array('invoice_address');
        $this->set('address_hidden_field', $address_hidden_field); //set

        $address_label[0] = $arr_set['field']['panel_2']['address']['name'];
        $this->set('address_label', $address_label); //set

        $address_conner[0]['top'] = 'hgt fixbor';
        $address_conner[0]['bottom'] = 'fixbor2 jt_ppbot';
        $this->set('address_conner', $address_conner); //set

        $keys = 'company';
        $address_field_name[$keys][0]['name'] = 'address_1';
        $address_field_name[$keys][0]['id']  = ucfirst($keys).'Address1';
        $address_field_name[$keys][1]['name'] = 'address_2';
        $address_field_name[$keys][1]['id']     = ucfirst($keys).'Address2';
        $address_field_name[$keys][2]['name'] = 'address_3';
        $address_field_name[$keys][2]['id']     = ucfirst($keys).'Address3';
        $address_field_name[$keys][3]['name'] = 'town_city';
        $address_field_name[$keys][3]['id']     = ucfirst($keys).'TownCity';
        $address_field_name[$keys][4]['name'] = 'country';
        $address_field_name[$keys][4]['id']     = ucfirst($keys).'Country';
        $address_field_name[$keys][5]['name'] = 'province_state';
        $address_field_name[$keys][5]['id']     = ucfirst($keys).'ProvinceState';
        $address_field_name[$keys][6]['name'] = 'zip_postcode';
        $address_field_name[$keys][6]['id']     = ucfirst($keys).'ZipPostcode';
         $this->set('address_field_name', $address_field_name); //set
        //pr($address_field_name);die;

        $this->set('address_country', $address_country); //set
        $this->set('address_country_id', $address_country_id); //set
        $this->set('address_province', $address_province); //set
        $this->set('address_province_id', $address_province_id); //set
        $this->set('address_more_line', 1); //set
        $this->set('address_onchange', "save_address_pr('\"+keys+\"');");
        $this->set('address_company_id', 'mongo_id');
        if (isset($arr_tmp['contact_id']) && strlen($arr_tmp['contact_id']) == 24)
            $this->set('address_contact_id', 'contact_id');
        $this->set('address_add', $address_add);
    }

    public function entry() {
        $arr_set = $this->opm->arr_settings;
        $arr_tmp = array();
        // Get value id
        $iditem = $this->get_id();
        if ($iditem == '')
            $iditem = $this->get_last_id();

        $this->set('iditem', $iditem);
        //Load record by id
        if ($iditem != '') {
            $arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
            foreach ($arr_set['field'] as $ks => $vls) {
                foreach ($vls as $field => $values) {
                    if (isset($arr_tmp[$field])) {
                        $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
                        if (preg_match("/_date$/", $field) && is_object($arr_tmp[$field]))
                            $arr_set['field'][$ks][$field]['default'] = date('m/d/Y', $arr_tmp[$field]->sec);
                        if (in_array($field, $arr_set['title_field']))
                            $item_title[$field] = $arr_tmp[$field];
                        if ($field == 'contact_name' && isset($arr_tmp['contact_last_name'])) {
                            $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field] . ' ' . $arr_tmp['contact_last_name'];
                            $item_title['contact_name'] = $arr_tmp[$field] . ' ' . $arr_tmp['contact_last_name'];
                        }
                    }
                }
            }

            $arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
            $this->Session->write($this->name . 'ViewId', $iditem);

            //BEGIN custom


             if($arr_set['field']['panel_1']['is_supplier']['default'] ==  1)
                $is_supplier = 'checked="checked"';
            else
                $is_supplier = '';

             if($arr_set['field']['panel_1']['is_customer']['default'] ==  1)
                $is_customer = 'checked="checked"';
              else
                $is_customer = '';

			$supplier_name = 'Supplier';
			$customer_name = 'Customer';
			if(isset($arr_set['field']['panel_1']['is_supplier']['name']))
				$supplier_name = $arr_set['field']['panel_1']['is_supplier']['name'];
			if(isset($arr_set['field']['panel_1']['is_customer']['name']))
				$customer_name = $arr_set['field']['panel_1']['is_customer']['name'];

            $arr_set['field']['panel_1']['no']['after'] = '
                <div class="jt_company_checkbox" style="margin-right:2%;">
                    <span class="inactive_fix_entry">'.$supplier_name.'</span>
                    <label class="m_check2">
                        <input type="checkbox" name="is_supplier" value="1" id="is_supplier" '.$is_supplier.'><span></span>
                    </label>
                </div>
                <div class="jt_company_checkbox">
                    <span class="inactive_fix_entry">'.$customer_name.'</span>
                    <label class="m_check2">
                        <input type="checkbox" name="is_customer" value="1" id="is_customer" '.$is_customer.'><span></span>
                    </label>
                </div>';

            if (isset($arr_set['field']['panel_1']['no']['default']))
                $item_title['no'] = $arr_set['field']['panel_1']['no']['default'];
            else
                $item_title['no'] = '1';
            $this->set('item_title', $item_title);

            //END custom
            //$this->set('address_lock', '1');
            //END custom
            //show footer info
            $this->show_footer_info($arr_tmp);


            //add, setup field tự tăng
        }else {
            $nextcode = $this->opm->get_auto_code('no');
            $arr_set['field']['panel_1']['no']['default'] = $nextcode;
            $this->set('item_title', array('no' => $nextcode));
        }
        $this->set('arr_settings', $arr_set);
        $this->sub_tab_default = 'contacts';
        $this->sub_tab('', $iditem);
        parent::entry();
        $this->set_entry_address($arr_tmp, $arr_set);
        $arr_tmp['addresses_default_key'] = isset($arr_tmp['addresses_default_key']) ? $arr_tmp['addresses_default_key'] : 0;
        $this->set('address_choose',$arr_tmp['addresses_default_key']);
    }

    public function arr_associated_data($field = '', $value = '', $valueid = '', $fieldopt = '') {
        $arr_return = array();
        $arr_return[$field] = $value;
        // ..........more code
        if($field == 'contact_default_id'){
            $arr_return[$field] = new MongoId($value);
        }
        if($field == 'our_rep'){
            $arr_return['our_rep_id'] = new MongoId($valueid);
        }
        if($field == 'our_csr'){
            $arr_return['our_csr_id'] = new MongoId($valueid);
        }
        if ($field == 'addresses') {

            $this->selectModel('Country');
            $country = $value[$valueid]['country'];
            $query = $this->Country->select_one(array( 'name' => $country ));
            $country_id = $query['value'];
            $arr_return[$field][$valueid]['country_id'] = $country_id;
            $zip_postcode = (string)$arr_return[$field][$valueid]['zip_postcode'];
            if(is_numeric($zip_postcode) && $zip_postcode[0] != 0)
                $arr_return[$field][$valueid]['zip_postcode'] = (string)'0'.$zip_postcode;
        }
        return $arr_return;
    }

    public function swith_options($keys){
        parent::swith_options($keys);
        if($keys == 'create_enquiry'){
            echo URL . DS. $this->params->params['controller'].DS.'enquiries_add_options'.DS.$this->get_id();
        }else if($keys == 'create_quotation'){
            echo URL.DS.$this->params->params['controller'].DS.'quotes_add';
        }else if($keys == 'create_job'){
            echo URL.DS.$this->params->params['controller'].DS.'jobs_add_options'.DS.$this->get_id();
        }else if($keys == 'create_task'){
            echo URL.DS.$this->params->params['controller'].DS.'tasks_add_options'.DS.$this->get_id();
        }else if($keys == 'create_sales_order'){
            echo URL.DS.$this->params->params['controller'].DS.'orders_add_salesorder'.DS.$this->get_id();
        }else if($keys == 'create_purchase_order'){
            echo URL.DS.$this->params->params['controller'].DS.'orders_add_purchasesorder'.DS.$this->get_id();
        }else if($keys == 'create_sales_invoice'){
            echo URL.DS.$this->params->params['controller'].DS.'salesinvoice_add_options'.DS.$this->get_id();
        }else if($keys == 'create_shipping'){
            echo URL.DS.$this->params->params['controller'].DS.'shipping_add_options'.DS.$this->get_id();
        }else if($keys == 'active_customers'){
            $or_where = array(
                              'is_customer' => 1,
                              'is_supplier' => 0
                              );
            $or_where1 = array(
                               'is_customer' => 1,
                               'is_supplier' => 1,
                               );
            $cond['$or']=array($or_where,$or_where1);
            $this->Session->write('companies_entry_search_cond',$cond);
            echo URL.DS.$this->params->params['controller'].DS.'lists';
        }else if($keys=='active_suppliers'){
            $or_where = array(
                'is_customer' => 0,
                'is_supplier' => 1

            );
            $or_where1 = array(
                'is_customer' => 1,
                'is_supplier' => 1

            );
            $cond['$or']=array(
                $or_where
                ,$or_where1
            );

            $this->Session->write('companies_entry_search_cond',$cond);
            echo URL . DS . $this->params->params['controller'] .DS.'lists';
        }else if($keys == 'active_customers_that_are_also_suppliers'){
            $or_where = array(
                'is_customer' => 1,
                'is_supplier' => 1

            );
            $this->Session->write('companies_entry_search_cond',$or_where);
            echo URL . DS . $this->params->params['controller'] .DS.'lists';
        }else if($keys == 'companies_that_are_not_a_customer_or_supplier'){
            $or_where = array(
                'is_customer' => 0,
                'is_supplier' => 0

            );
            $this->Session->write('companies_entry_search_cond',$or_where);
            echo URL . DS . $this->params->params['controller'] .DS.'lists';
        }else if($keys == 'print_mini_list'){
            echo URL.DS.$this->params->params['controller'].DS.'view_minilist';
        }else if($keys == 'create_email'){
            echo URL .'/'.$this->params->params['controller']. '/create_email';
        }else if($keys == 'create_fax'){
            echo URL .'/'.$this->params->params['controller']. '/create_fax';
        }else if($keys == 'create_letter'){
            echo URL .'/'.$this->params->params['controller']. '/create_letter';
        }
        die;
   }

    public function contacts(){
        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_all(array(
                                                  'arr_where' => array('company_id'=>new MongoId($this->get_id())),
                                                  'arr_order' => array('first_name' => 1),
                                                  ));

        $this->selectModel('Company');
        $arr_company = $this->Company->select_one(array('_id' => new MongoId($this->get_id())), array('_id', 'contact_default_id', 'name'));

        $arr = array();
        foreach($arr_contact as $key => $value){
            if($value['_id']==$arr_company['contact_default_id'])
                $value['contact_default'] = 1;
            $arr[$key] = $value;
        }
        $subdatas = array();
        $this->set_select_data_list('relationship','contacts');
        $subdatas['contacts'] = $arr;
        $this->set('subdatas', $subdatas);
    }

    function contacts_add($company_id, $company_name = '') {
        $arr_save = array();
        $arr_save['company_id'] = new MongoId($company_id);
        $key = 0;
        if(isset($this->data['Salesaccount']['addresses_default_key']))
        $key = $this->data['Salesaccount']['addresses_default_key'];
        $address = '';
        if(isset($this->data['Salesaccount']['addresses'][$key]['country_id']))

         $address = $this->data['Salesaccount']['addresses'][$key]['country_id'];

        $this->selectModel('Company');
        $key = 0;
        if(isset($this->data['Salesaccount']['addresses_default_key']))
        $key = $this->data['Salesaccount']['addresses_default_key'];
        $address = '';
        if(isset($this->data['Salesaccount']['addresses'][$key]['country_id']))
        $address = $this->data['Salesaccount']['addresses'][$key]['country_id'];
        $arr_company = $this->Company->select_one(array('_id' => $arr_save['company_id']), array('_id', 'name', 'addresses', 'addresses_default_key', 'system', 'fax', 'phone'));
        if( isset($arr_company['system']) && $arr_company['system'] ){
            $arr_save['is_customer'] = 0;
            $arr_save['is_employee'] = 1;
        }
        $arr_save['company'] = $arr_company['name'];
        $arr_save['fax'] = $arr_company['fax'];
        $arr_save['company_phone'] = $arr_company['phone'];
        $arr_save['addresses'] = array(
            array(
                'name' => '',
                'deleted' => false,
                'default' => true,

                /*'country' => $arr_company['addresses'][$arr_company['addresses_default_key']]['country'],
                'country_id' => $arr_company['addresses'][$arr_company['addresses_default_key']]['country_id'],
                'province_state' => $arr_company['addresses'][$arr_company['addresses_default_key']]['province_state'],
                'province_state_id' => $arr_company['addresses'][$arr_company['addresses_default_key']]['province_state_id'],
                'address_1' => $arr_company['addresses'][$arr_company['addresses_default_key']]['address_1'],
                'address_2' => $arr_company['addresses'][$arr_company['addresses_default_key']]['address_2'],
                'address_3' => $arr_company['addresses'][$arr_company['addresses_default_key']]['address_3'],
                'town_city' => $arr_company['addresses'][$arr_company['addresses_default_key']]['town_city'],
                'zip_postcode' => $arr_company['addresses'][$arr_company['addresses_default_key']]['zip_postcode'],*/

                'country' => '',
                'country_id' => '',
                'province_state' => '',
                'province_state_id' => '',
                'address_1' => '',
                'address_2' => '',
                'address_3' => '',
                'town_city' => '',
                'zip_postcode' => ''
            )
        );
        $arr_save['addresses_default_key'] = 0;
        $arr_save['language'] = 'vi';

        // Tìm kiếm trước xem company này hiện tại đã có default chưa, nếu chưa thì khi save xong sẽ save vào company này default contact
        $this->selectModel('Contact');
        $arr_contact_default = $this->Contact->select_one(array('company_id' => new MongoId($company_id)), array('_id', 'name'));

        $this->Contact->arr_default_before_save = $arr_save;
        if (!$this->Contact->add()) {
            echo 'Error: ' . $this->Contact->arr_errors_save[1];
            die;
        } else {
            if (!isset($arr_contact_default['_id'])) {
                $arr_save_company = array();
                $arr_save_company['_id'] = new MongoId($company_id);
                $arr_save_company['contact_default_id'] = $this->Contact->mongo_id_after_save;
                $this->selectModel('Company');
                if (!$this->Company->save($arr_save_company)) {
                    echo 'Error: ' . $this->Company->arr_errors_save[1];
                    die;
                }
            }
        }
        die;
    }

    function addresses_add($company_id) {
        $this->selectModel('Company');
        $this->Company->collection->update(
                array('_id' => new MongoId($company_id)), array('$push' => array(
                'addresses' => array(
                    'name' => '',
                    'default' => false,
                    'address_1' => '',
                    'address_2' => '',
                    'address_3' => '',
                    'town_city' => '',
                    'zip_postcode' => '',
                    'province_state' => '',
                    'province_state_id' => '',
                    'country' => 'Việt Nam',
                    'country_id' => "VN",
                    'deleted' => false
                )))
        );
        $this->addresses($company_id);
        $this->render('addresses');
    }

    function addresses_delete($id) {
        $this->selectModel('Company');
        $arr_save = $this->Company->select_one(array('_id'=> new MongoId($this->get_id())));
        $arr_save['addresses'][$id] = array();
        $arr_save['addresses'][$id]['deleted'] = true;

        if ($id != 0 && $arr_save['addresses_default_key'] == $id) {
            $arr_save['addresses']['0']['default'] = true;
            $arr_save['addresses_default_key']  = 0;
            $change = 1;
        } elseif ($id != 0 && $arr_save['addresses_default_key'] != $id) {
            $arr_save['addresses'][$id]['deleted'] = true;
            $change = 0;
        } else {
        }

        $arr_save['_id'] = $this->get_id();
        $this->selectModel('Company');
        if ($this->Company->save($arr_save)) {
            if ($change == 1)
                echo 'ok_change';
            else
                echo 'ok';
        } else {
            echo 'Error: ' . $this->Company->arr_errors_save[1];
        }
        die;
    }

    function quotes_delete($id) {
        $arr_save['_id'] = new MongoId($id);
        $arr_save['deleted'] = true;
        $error = 0;
        if (!$error) {
            $this->selectModel('Quotation');
            if ($this->Quotation->save($arr_save)) {
                echo 'ok';
            } else {
                echo 'Error: ' . $this->Quotation->arr_errors_save[1];
            }
        }
        die;
    }


     function products($company_id){
        if(empty($this->data) || !isset($this->data['Company'])){
            $this->selectModel('Company');
            $arr_company = $this->Company->select_one(array('_id'=>new MongoId($company_id)));
            $tmp['Company'] = $arr_company;
            $this->data = $tmp;
        }else{
            $arr_company = $this->data['Company'];
        }
        $arr_pricing = array();
        if(!empty($this->data['Company']['pricing']))
        foreach($this->data['Company']['pricing'] as $k => $v){
            if(isset($v['deleted']) && $v['deleted']) continue;
            $arr_pricing[$k] = $v;
            $arr_pricing[$k]['product_id'] = (string)$v['product_id'];

            if(isset($v['price_break']) && isset($v['price_break'][0])){
                foreach($v['price_break'] as $kk => $vv){
                    if(isset($vv['deleted']) && $vv['deleted']) continue;
                    $arr_pricing[$k]['range'] = $vv['range_from'].'-'.$vv['range_to'];
                    $arr_pricing[$k]['unit_price'] = $vv['unit_price'];
                    break;
                }
            }
        }
        $arr_tmp = array();
        if($arr_company['is_supplier']==1){
            $this->selectModel('Product');
            $arr_product_supplier=$this->Product->select_all(array(
                                                             'arr_where'=>array("company_id"=>new MongoId($company_id)),
                                                             ));
            foreach($arr_product_supplier as $key => $value){
                $arr_tmp[$key] = $value;
            }
        }

        if($arr_company['is_customer']==1){
            $arr_product_customer=array();
        }

        $subdatas = array();
        $subdatas['1'] = $arr_tmp;
        if($arr_company['is_supplier'] == 1)
            $subdatas['same_business_type_companies'] = $this->same_business_type_companies($arr_company);
        $subdatas['pricing_category'] = $arr_company;
        $subdatas['4'] = $arr_pricing;
        $subdatas['5'] = array();
        $arr_options_custom =  $this->set_select_data_list('relationship','products');
        $this->set('arr_options_custom', $arr_options_custom);
        $this->set('subdatas', $subdatas);
    }


    function same_business_type_companies($arr_company){
        $arr_companies = $this->opm->select_all(array(
                               'arr_where' => array(
                                                    'business_type'=>$arr_company['business_type'],
                                                    'is_supplier' => 1,
                                                    '_id' => array('$ne' => $arr_company['_id'])
                                                    ),
                               'arr_order' => array('name' => 1),
                               'arr_field' => array('name')
                               ));
        return $arr_companies;
    }
}