<?php
App::uses('ContactsController', 'Controller');
class ContactsControllerCustom extends ContactsController{
 	public function beforeFilter() {
        parent::beforeFilter();
    }

    public function entry() {

        $arr_set = $this->opm->arr_settings;
        $arr_tmp = array();
        // Get value id
        $iditem = $this->get_id();  // lay  "_id": ObjectId("53315b02005fc3e003001226"),
        if ($iditem == '')
            $iditem = $this->get_last_id();

        $this->set('iditem', $iditem);
        //Load record by id
        if ($iditem != '') {
            $arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
            foreach ($arr_set['field'] as $ks => $vls) {
                foreach ($vls as $field => $values) {
                    if (isset($arr_tmp[$field])) {
                        $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
                        if (preg_match("/_date$/", $field) && is_object($arr_tmp[$field]))
                            $arr_set['field'][$ks][$field]['default'] = date('m/d/Y', $arr_tmp[$field]->sec);
                        if (in_array($field, $arr_set['title_field']))
                            $item_title[$field] = $arr_tmp[$field];
                        if ($field == 'contact_name' && isset($arr_tmp['contact_last_name'])) {
                            $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field] . ' ' . $arr_tmp['contact_last_name'];
                            $item_title['contact_name'] = $arr_tmp[$field] . ' ' . $arr_tmp['contact_last_name'];
                        }
                        if ($field=='is_employee' && $arr_tmp[$field] == 1){
                            $arr_set['field']['panel_1']['code']['after_field'] = 'is_employee';
                            $arr_set['field']['panel_1']['is_employee']['type'] = 'checkbox';
                            $arr_set['field']['panel_1']['is_customer']['type'] = 'hidden';
                        }
                    }
                }
            }

            $arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
            $this->Session->write($this->name . 'ViewId', $iditem);

            //BEGIN custom
            if (isset($arr_set['field']['panel_1']['code']['default']))
                $item_title['code'] = $arr_set['field']['panel_1']['code']['default'];
            else
                $item_title['code'] = '1';
            $this->set('item_title', $item_title);

            //show footer info
            $this->show_footer_info($arr_tmp);
            //add, setup field tự tăng
        }else {
            $nextcode = $this->opm->get_auto_code('code');
            $arr_set['field']['panel_1']['code']['default'] = $nextcode;
            $this->set('item_title', array('code' => $nextcode));
        }

        $this->set('arr_settings', $arr_set);
        $this->sub_tab_default = 'addresses';
        $this->sub_tab('', $iditem);

        $this->set_entry_address($arr_tmp, $arr_set);
        $this->set('address_choose',$arr_tmp['addresses_default_key']);
        parent::entry();
    }

    public function set_entry_address($arr_tmp, $arr_set) {
        $address_fset = array('address_1', 'address_2', 'address_3', 'town_city', 'country', 'province_state', 'zip_postcode');
        $address_value = $address_province_id = $address_country_id = $address_province = $address_country = array();
        $address_controller = array('company');
        $address_value['company'] = array('', '', '', '', "VN", '', '');
        $this->set('address_controller', $address_controller); //set
        $address_key = array('invoice');
        $this->set('address_key', $address_key); //set
        $address_country = $this->country();
        $arr_address_tmp = array();
        if(!isset($arr_tmp['addresses_default_key']))
            $arr_tmp['addresses_default_key'] = 0;
        if(isset($arr_tmp['addresses'][$arr_tmp['addresses_default_key']])){
            foreach($arr_tmp['addresses'][$arr_tmp['addresses_default_key']] as $key=>$value){
                if($key=='deleted') continue;
                $arr_address_tmp['invoice_'.$key] = $value;
            }
        }
        $arr_tmp['invoice_address'][0] = array();
        foreach ($address_key as $kss => $vss) {
            //neu ton tai address trong data base
            if (isset($arr_tmp[$vss . '_address'][0])) {
                if(!empty($arr_address_tmp))
                    $arr_tmp[$vss . '_address'][0] = $arr_address_tmp;
                $arr_temp_op = $arr_tmp[$vss . '_address'][0];
                for ($i = 0; $i < count($address_fset); $i++) { //loop field and set value for display
                    if (isset($arr_temp_op[$vss . '_' . $address_fset[$i]])) {
                        $address_value[$vss][$i] = $arr_temp_op[$vss . '_' . $address_fset[$i]];
                    } else {
                        $address_value[$vss][$i] = '';
                    }
                }//pr($arr_temp_op);die;
                //get province list and country list

                if (isset($arr_temp_op[$vss . '_country_id']))
                    $address_province[$vss] = $this->province($arr_temp_op[$vss . '_country_id']);
                else
                    $address_province[$vss] = $this->province();
                //set province
                if (isset($arr_temp_op[$vss . '_province_state_id']) && $arr_temp_op[$vss . '_province_state_id'] != '' && isset($address_province[$vss][$arr_temp_op[$vss . '_province_state_id']]))
                    $address_province_id[$kss] = $arr_temp_op[$vss . '_province_state_id'];
                else if (isset($arr_temp_op[$vss . '_province_state']))
                    $address_province_id[$kss] = $arr_temp_op[$vss . '_province_state'];
                else
                    $address_province_id[$kss] = '';

                //set country
                if (isset($arr_temp_op[$vss . '_country_id'])) {
                    $address_country_id[$kss] = $arr_temp_op[$vss . '_country_id'];
                    $address_province[$vss] = $this->province($arr_temp_op[$vss . '_country_id']);
                } else {
                    $address_country_id[$kss] = "VN";
                    $address_province[$vss] = $this->province("VN");
                }

                $address_add[$vss] = '0';
                //chua co address trong data
            } else {
                $address_country_id[$kss] = "VN";
                $address_province[$vss] = $this->province("VN");
                $address_add[$vss] = '1';
            }
        }
        $this->set('address_value', $address_value);
        $address_hidden_field = array('invoice_address');
        $this->set('address_hidden_field', $address_hidden_field); //set
        $address_label[0] = $arr_set['field']['panel_3']['invoice_address']['name'];
        $this->set('address_label', $address_label); //set

        $address_conner[0]['top'] = 'hgt fixbor';
        $address_conner[0]['bottom'] = 'fixbor2 jt_ppbot';
        $this->set('address_conner', $address_conner); //set

        $keys = 'invoice';
        $address_field_name[$keys][0]['name'] = 'address_1';
        $address_field_name[$keys][0]['id']  = ucfirst($keys).'Address1';
        $address_field_name[$keys][1]['name'] = 'address_2';
        $address_field_name[$keys][1]['id']     = ucfirst($keys).'Address2';
        $address_field_name[$keys][2]['name'] = 'address_3';
        $address_field_name[$keys][2]['id']     = ucfirst($keys).'Address3';
        $address_field_name[$keys][3]['name'] = 'town_city';
        $address_field_name[$keys][3]['id']     = ucfirst($keys).'TownCity';
        $address_field_name[$keys][4]['name'] = 'country';
        $address_field_name[$keys][4]['id']     = ucfirst($keys).'Country';
        $address_field_name[$keys][5]['name'] = 'province_state';
        $address_field_name[$keys][5]['id']     = ucfirst($keys).'ProvinceState';
        $address_field_name[$keys][6]['name'] = 'zip_postcode';
        $address_field_name[$keys][6]['id']     = ucfirst($keys).'ZipPostcode';
         $this->set('address_field_name', $address_field_name); //set
        //pr($address_field_name);die;

        $this->set('address_country', $address_country); //set
        $this->set('address_country_id', $address_country_id); //set
        $this->set('address_province', $address_province); //set
        $this->set('address_province_id', $address_province_id); //set
        $this->set('address_more_line', 2); //set
        $this->set('address_onchange', "save_address_pr('\"+keys+\"');");

        $this->set('address_contact_id', 'mongo_id');
        //$this->set('address_company_id','company_id');
        if (isset($arr_tmp['contact_id']) && strlen($arr_tmp['contact_id']) == 24)
            $this->set('address_contact_id', 'contact_id');
        $this->set('address_add', $address_add);
    }

    public function arr_associated_data($field = '', $value = '', $valueid = '',$fieldopt='') {
        $arr_return = array();
        $arr_return[$field] = $value;
        $tmp_data = array();
        if(isset($_POST['arr']) && is_string($_POST['arr']) && $_POST['arr']!='')
            $tmp_data = (array)json_decode($_POST['arr']);
        if(isset($tmp_data['keys'])){
            if( ($tmp_data['keys']=='update' || $tmp_data['keys']=='add')
                &&!$this->check_permission($this->name.'_@_entry_@_edit')){
                echo 'You do not have permission on this action.';
                die;
            }
        }
        /**
         * Chọn Company  ***********************************************
         */
        if ($field == 'company' && $valueid != '') {
            $arr_return = array(
                'company_name' => '',
                'company_id' => '',
                'company_phone' => '',
            );
            //change company
            $arr_return['company_name'] = $value;
            $arr_return['company_id'] = new MongoId($valueid);

            //find contact and more from Company
            $this->selectModel('Company');
            $arr_company = $this->Company->select_one(array('_id' => new MongoId($valueid)));

            $this->selectModel('Contact');
            $arr_contact = $arrtemp = array();
            // is set contact_default_id
            if (isset($arr_company['contact_default_id']) && is_object($arr_company['contact_default_id'])) {
                $arr_contact = $this->Contact->select_one(array('_id' => $arr_company['contact_default_id']));

                // not set contact_default_id
            } else {
                $arr_contact = $this->Contact->select_all(array(
                    'arr_where' => array('company_id' => new MongoId($valueid)),
                    'arr_order' => array('_id' => -1),
                ));
                $arrtemp = iterator_to_array($arr_contact);
                if (count($arrtemp) > 0) {
                    $arr_contact = current($arrtemp);
                } else
                    $arr_contact = array();
            }
            //change phone
            if (isset($arr_company['phone']))
                $arr_return['company_phone'] = $arr_company['phone'];

            if (!isset($arr_contact['direct_dial']) && !isset($arr_contact['mobile']))
                    $arr_return['phone'] = '';  //bat buoc phai co dong nay khong thi no se lay du lieu cua cty truoc

        }else if( $field == 'addresses'){
            if($fieldopt == 'default'){
                foreach($value as $key => $val){
                    if($val['deleted']) continue;
                    if($key != $valueid)
                        $value[$key]['default'] = false;
                }
                $arr_return['addresses_default_key'] = $valueid;
                $arr_return[$field] = $value;
                return $arr_return;
            }
        } else if( $field == 'full_name'){
            $id = isset($_POST['ids']) ? $_POST['ids'] : $this->opm->get_id();
            $contact = $this->opm->select_one(array('_id'=> new MongoId($id)),array('username'));
            if(!isset($contact['username']) || $contact['username'] == '')
                $arr_return['username'] = $value;
        }

        return $arr_return;
    }

    function addresses_delete($id) {
        $this->selectModel('Contact');
        $arr_save = $this->Contact->select_one(array('_id'=> new MongoId($this->get_id())));
        $arr_save['addresses'][$id] = array();
        $arr_save['addresses'][$id]['deleted'] = true;

        if ($id != 0 && $arr_save['addresses_default_key'] == $id) {
            $arr_save['addresses']['0']['default'] = true;
            $arr_save['addresses_default_key']  = 0;
            $change = 1;
        } elseif ($id != 0 && $arr_save['addresses_default_key'] != $id) {
            $arr_save['addresses'][$id]['deleted'] = true;
            $change = 0;
        } else {
        }

        $arr_save['_id'] = $this->get_id();
        $this->selectModel('Contact');
        if ($this->Contact->save($arr_save)) {
            if ($change == 1)
                echo 'ok_change';
            else
                echo 'ok';
        } else {
            echo 'Error: ' . $this->Contact->arr_errors_save[1];
        }
        die;
    }

    function enquiries_add($contact_id) {
        $this->selectModel('Contact');
        $arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
        $this->selectModel('Enquiry');
        $arr_tmp = $this->Enquiry->select_one(array(), array(), array('no' => -1));
        $arr_save = array();
        $arr_company = $this->arr_associated_data('company',$arr_contact['company'], $arr_contact['company_id']);
        foreach ($arr_company as $key => $value) {
            $arr_save['company'] = $arr_company['company_name'];
            $arr_save['company_id'] = $arr_company['company_id'];
            $arr_save['company_phone'] = $arr_company['company_phone'];
        }
        $arr_save['no'] = 1;
        if (isset($arr_tmp['no'])) {
            $arr_save['no'] = $arr_tmp['no'] + 1;
        }

        $key = 0;
        if(isset($this->data['Contact']['addresses_default_key']))
            $key = $this->data['Contact']['addresses_default_key'];
        $address = '';
        if(isset($this->data['Contact']['addresses'][$key]['country_id']))
            $address = $this->data['Contact']['addresses'][$key]['country_id'];

        $key = isset($arr_contact['addresses_default_key'])?$arr_contact['addresses_default_key']:0;
        $arr_save['date']= new MongoDate(time());
        $arr_save['status']='Hot';
        $arr_save['default_country'] = isset($arr_contact['addresses'][$key]['country'])?$arr_contact['addresses'][$key]['country']:'';
        $arr_save['default_country_id'] = isset($arr_contact['addresses'][$key]['country_id'])?$arr_contact['addresses'][$key]['country_id']:0;
        $arr_save['default_province_state'] = isset($arr_contact['addresses'][$key]['province_state'])?$arr_contact['addresses'][$key]['province_state']:'';
        $arr_save['default_province_state_id'] = isset($arr_contact['addresses'][$key]['province_state_id'])?$arr_contact['addresses'][$key]['province_state_id']:'';
        $arr_save['default_address_1'] = isset($arr_contact['addresses'][$key]['address_1'])?$arr_contact['addresses'][$key]['address_1']:'';
        $arr_save['default_address_2'] = isset($arr_contact['addresses'][$key]['address_2'])?$arr_contact['addresses'][$key]['address_2']:'';
        $arr_save['default_address_3'] = isset($arr_contact['addresses'][$key]['address_3'])?$arr_contact['addresses'][$key]['address_3']:'';
        $arr_save['default_town_city'] = isset($arr_contact['addresses'][$key]['town_city'])?$arr_contact['addresses'][$key]['town_city']:'';
        $arr_save['default_zip_postcode'] = isset($arr_contact['addresses'][$key]['zip_postcode'])?$arr_contact['addresses'][$key]['zip_postcode']:'';
        $arr_save['contact_phone'] = isset($arr_contact['phone'])?$arr_contact['phone']:'';
        $arr_save['contact_fax'] = isset($arr_contact['fax'])?$arr_contact['fax']:'';
        $arr_save['contact_email'] = isset($arr_contact['email'])?$arr_contact['email']:'';
        $arr_save['web'] = isset($arr_contact['web'])?$arr_contact['web']:'';
        $arr_save['contact'] = isset($arr_contact['name'])?$arr_contact['name']:'';

        //   14/4/2014
        $arr_save['contact_name'] = isset($arr_contact['name'])?$arr_contact['name']:'';
        $arr_save['contact_id'] = $arr_contact['_id'];
        $arr_save['contact_phone'] = isset($arr_contact['phone'])?$arr_contact['phone']:'';
        //   14/4/2014

        $this->Enquiry->arr_default_before_save = $arr_save;
        if ($this->Enquiry->add())
           echo URL.'/enquiries/entry/' . $this->Enquiry->mongo_id_after_save;
        else
            echo URL.'/enquiries/entry';
        die;
    }

    function enquiry_delete($id) {
        $arr_save['_id'] = new MongoId($id);
        $arr_save['deleted'] = true;
        $error = 0;
        if (!$error) {
            $this->selectModel('Enquiry');
            if ($this->Enquiry->save($arr_save)) {
                echo 'ok';
            } else {
                echo 'Error: ' . $this->Enquiry->arr_errors_save[1];
            }
        }
        die;
    }

    function job_delete($id) {
        $arr_save['_id'] = new MongoId($id);
        $arr_save['deleted'] = true;
        $error = 0;
        if (!$error) {
            $this->selectModel('Job');
            if ($this->Job->save($arr_save)) {
                echo 'ok';
            } else {
                echo 'Error: ' . $this->Job->arr_errors_save[1];
            }
        }
        die;
    }

    function addresses_add($contact_id) {
        $this->selectModel('Contact');
        $this->Contact->collection->update(
                array('_id' => new MongoId($contact_id)), array('$push' => array(
                'addresses' => array(
                    'name' => '',
                    'default' => false,
                    'address_1' => '',
                    'address_2' => '',
                    'address_3' => '',
                    'town_city' => '',
                    'zip_postcode' => '',
                    'province_state' => '',
                    'province_state_id' => '',
                    'country' => 'Việt Nam',
                    'country_id' => "VN",
                    'deleted' => false
                )))
        );
        $this->addresses($contact_id);
        $this->render('addresses');
    }
}