﻿<?php
App::uses('ProductsController', 'Controller');
class ProductsControllerCustom extends ProductsController{
 	public function beforeFilter() {
        parent::beforeFilter();
    }

    public function entry() {

        $arr_set = $this->opm->arr_settings;

		//set color RED
		/*$not_yet['units_serials'] = '1';
		$not_yet['batches'] = '1';
		$this->set('not_yet', $not_yet);*/

        // Check url to get value id
        $iditem = $this->get_id();
        if ($iditem == '')
            $iditem = $this->get_last_id();
        //Load record by id and set default for fields
        if ($iditem != '') {
            $arr_tmp = $this->opm->select_one(array('_id' => new MongoId($iditem)));
            foreach ($arr_set['field'] as $ks => $vls) {
                foreach ($vls as $field => $values) {
                    if (isset($arr_tmp[$field])) {
                        $arr_set['field'][$ks][$field]['default'] = $arr_tmp[$field];
                        if ($arr_set['field'][$ks][$field]['type'] == 'select')
                            $arr_set['field'][$ks][$field]['default_id'] = $arr_tmp[$field];
                        if (in_array($field, $arr_set['title_field']))
                            $item_title[$field] = $arr_tmp[$field];


                    }
                }
            }
            $arr_set['field']['panel_1']['mongo_id']['default'] = $iditem;
            $this->Session->write($this->name . 'ViewId', $iditem);
            //BEGIN special
            if (isset($arr_tmp['company']) && is_array($arr_tmp['company'])) {
                $arr_temp = (array) $arr_tmp['company'];
                $com_name = $com_id = '';
                foreach ($arr_temp as $ok => $ov) {
                    if (isset($ov['current']) && (int) $ov['current'] == 1 && !($ov['deleted'])) {
                        if (isset($ov['company_id'])) {
                            $com_id = $ov['company_id'];
                            $com_name = $this->get_name('Company', $com_id);
                        }
                    }
                }
                $arr_set['field']['panel_3']['company']['default'] = $item_title['company'] = $com_name;
                $arr_set['field']['panel_3']['company_id']['default'] = $com_id;
            }

            if (isset($arr_set['field']['panel_1']['code']['default']))
                $item_title['code'] = $arr_set['field']['panel_1']['code']['default'];
            else
                $item_title['code'] = '1';

            $this->set('item_title', $item_title);

            //custom select data
            $datas = $this->general_select($arr_set['field']);
            $this->selectModel('Setting');
            if (isset($arr_set['field']['panel_3']['sell_by']['default']))
                $datas['oum'] = $this->Setting->select_option_vl(array('setting_value' => 'product_oum_' . strtolower($arr_set['field']['panel_3']['sell_by']['default'])));
            $this->set('arr_options', $datas);

			//END custom
            //show footer info
            if (count($arr_tmp) > 0)
                $this->show_footer_info($arr_tmp);


            //add, setup field tự tăng
        }else {
            $codeauto = $this->opm->max_field('code');
            if (isset($codeauto['code']))
                $codeauto = 1 + (int) $codeauto['code'];
            else
                $codeauto = 1;
            $arr_set['field']['panel_1']['code']['default'] = $codeauto;
            $this->set('item_title', array('code' => $codeauto));
        }

		//custom list tax
        $this->set('arr_settings', $arr_set);
        $this->sub_tab('', $iditem);
        parent::entry();
		$arr_options_custom = array();
		$arr_options_custom['gst_tax'] = $arr_options_custom['pst_tax'] = '';
		$this->selectModel('Tax');
		$arr_options_custom['gst_tax'] = $arr_options_custom['pst_tax'] = $this->Tax->tax_select_list();
		$arr_options_custom['oum_depend'] = array('unit'=>'Unit','Sq.ft.'=>'Sq.ft.');
		$this->set('arr_options_custom',$arr_options_custom);
        //pr( $arr_tmp);die;
        //pr($this->category_child());
        $arr_options_custom['category'] = $this->category_child();
        if (isset($arr_tmp['category']))
            $arr_options_custom['category2'] = $this->category_child($arr_tmp['category']);
        else
            $arr_options_custom['category2'] = array();
        if (isset($arr_tmp['category2']))
            $arr_options_custom['category3'] = $this->category_child($arr_tmp['category2']);
        else
            $arr_options_custom['category3'] = array();
        $this->set('arr_options_custom',$arr_options_custom);

    }

    	// Action Upload
    public function upload() {
        $post_file = array();
        $post_file = $_FILES['products_upload'];
        $file = $this->Common->move_file($post_file);
        $arr_save = array();
        $tempdata = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
        $arr_save['name'] = $post_file['name'];
        $arr_save['path'] = $file;
        $arr_save['type'] = $post_file['type'];
        if ($arr_save['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            $arr_save['type'] = 'excel';
        }
        $arr_save['ext'] = strtolower(substr(strrchr($post_file['name'], '.'), 1));
        $arr_save['create_by_module'] = 'Product';
        $arr_save['category'] = 3;
        $arr_save['location'] = 'Inventory-Products';
        $arr_save['description'] = 'No.' . $tempdata['code'] . ':' . $tempdata['name'];

        $this->selectModel('Doc');
        $arr_save['no'] = $this->Doc->get_auto_code('no');
        if ($this->Doc->save($arr_save)) {
            $arr_use = array();
            $arr_use['doc_id'] = $this->Doc->mongo_id_after_save;
            $arr_use['module'] = 'Product';
            $arr_use['module_controller'] = 'products';
            $arr_use['create_by_module'] = 'Product';
            $arr_use['module_detail'] = $tempdata['name'];
            $arr_use['module_id'] = new MongoId($this->get_id());
            $arr_use['module_no'] = $tempdata['code'];
            $arr_use['created_by'] = new MongoId($this->opm->user_id());

            if (isset($tempdata['big_image']))
                $tempdata['products_upload'] = $tempdata['big_image'];
            else{
                $tempdata['big_image'] = $file;
                $tempdata['products_upload'] = $file;
            }
            $tempdata['product_slide_image'][] = $file;
            $tempdata['product_slide_image_id'][] = $arr_use['doc_id'];




            $this->opm->save($tempdata);
            $this->selectModel('DocUse');
            if ($this->DocUse->save($arr_use))
                $this->redirect('/' . $this->params->params['controller'] . '/entry');
        }else {
            $this->redirect('/' . $this->params->params['controller'] . '/entry');
        }
    }

    public function arr_associated_data($field = '', $value = '', $valueid = '',$field_name = '') {
		$arr_return[$field] = $value;
		$query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));

		if($field=='thickness' || $field=='cost_price'){
			$arr_return[$field] = (float)$value;
		}
		if($field=='sku'){
			$arr_return[$field] = (string)$value.' ';
		}
        if($field =='category' || $field =='category2' || $field=='category3'){
            $arr_return['category_id'] = (string)$value;
        }

        if($field=='production_step'){
            $data = (array)json_decode($_POST['arr']);
            if($data['keys']=='add'){
                $this->selectModel('Equipment');
                $arr_tag = $this->Equipment->select_combobox_asset_old();
                foreach($value as $k=>$v){
                    if(isset($v['deleted'])&&$v['deleted']) continue;
                    if(isset($arr_tag[$v['tag']])) unset($arr_tag[$v['tag']]);
                }
                if(empty($arr_tag)){
                    echo 'max_tag';
                    die;
                }
                $value[$valueid]['tag'] = reset($arr_tag);
                $arr_return[$field] = $value;
                return $arr_return;
            }
        }else if($field == 'von_luu'){

        }
	    else if($field=='sellprices'){
            $this->selectModel('Setting');
            $sell_category = $this->Setting->select_option_vl(array('setting_value'=>'products_sell_category'));
            $i = 1; $same = 0;
			$arr_list_cate = array();
            if(isset($query['sellprices'])&&!empty($query['sellprices'])){
                foreach($query['sellprices'] as $key=>$val)
                    if(isset($val['deleted']) && !$val['deleted']){
                        $i++;
                        if(trim($value[$valueid]['sell_category'])!='' && $key!=$valueid //Tru no ra, so sanh tat ca
                            &&$value[$valueid]['sell_category']==$val['sell_category'])
                            $same++;
						if(isset($val['sell_category']) && $val['sell_category']!='')
						$arr_list_cate[] = $val['sell_category'];
                    }
            }
            //$_POST dang json_encode cua object
            $data = (array)json_decode($_POST['arr']);
            if($same>0){
                echo 'existed';
                die;
            }//kei, Add moi can dem vuot gioi han, update ko can
            else if($data['keys']!='update'&&$i>3){
                echo 'reach_limit';
                die;
            }


			//neu la default
			if(isset($value[$valueid]['deleted']) && !$value[$valueid]['deleted'] && isset($value[$valueid]['sell_default']) && $value[$valueid]['sell_default']==1){
				$arr_return['sell_price'] = (float)$value[$valueid]['sell_unit_price'];
			}


			$actionjson = json_decode($_POST['arr']);
			$actionjson = (array)$actionjson;
			//neu la add moi sell_category =''
			if(isset($query['sellprices']) && is_array($query['sellprices']) && count($query['sellprices'])>0 && isset($actionjson['keys']) && $actionjson['keys']=='add' && isset($value[$valueid]['sell_category']) && $value[$valueid]['sell_category']==''){

                foreach($sell_category as $kk=>$vv){
					if(!in_array($kk,$arr_list_cate)){
                    	$value[$valueid]['sell_category'] = $kk;
						break;
					}
                }
				//pr($value[$valueid]);die;
			}
			$arr_return[$field] = $value;

        }else if($field=='pricebreaks'){
            $data = (array)json_decode($_POST['arr']);
            $max = 0;
            if($data['keys']=='add'){
                //Tim gia tri lon nhat
                if(isset($query['pricebreaks'])&&!empty($query['pricebreaks'])){
                    $value[$valueid]['sell_category'] = (isset($_SESSION['Products_current_category'])? $_SESSION['Products_current_category'] : '');
                    foreach($query['pricebreaks'] as $val){
                        if($val['deleted']) continue;
                        if($val['sell_category']!=$value[$valueid]['sell_category']) continue;
                        if(isset($val['range_to'])&&$val['range_to']!=''){
                            if($max<$val['range_to'])
                                $max = $val['range_to'];
                        }
                    }
                }
                $range_from = 1;
                $range_to = $range_from + 4;
                if($max>0){
                    $range_from = $max;
                    $range_to = $range_from + 5;
                }
                $value[$valueid]['range_from'] = $range_from;
                $value[$valueid]['range_to'] = $range_to;
                $arr_return['pricebreaks'] = array();
                $arr_return['pricebreaks'] = $value;
                return $arr_return;
            }else if($data['keys']=='update'){
                $range_from_in_range = 0;
                $range_to_in_range = 0;
                if($value[$valueid]['range_from']>$value[$valueid]['range_to']){
                    echo 'range_from_greater_than_range_to';
                    die;
                }
                if(isset($query['pricebreaks'])&&!empty($query['pricebreaks'])&&isset($_POST['fieldchage'])){
                    if($_POST['fieldchage']=='range_from'){
                        if(!$this->pricebreaks_range_comparasion($query['pricebreaks'],$value[$valueid]['range_from'],$valueid)){
                            echo 'range_from_in_range';
                            die;
                        }
                        $arr_tmp = $this->opm->aasort($query['pricebreaks'],'range_from',1);
                        foreach($arr_tmp as $key=>$val){
                            if($val['deleted'] || $key==$valueid) continue;
                            if(isset($_SESSION['Products_current_category'])
                                &&$val['sell_category']!=$_SESSION['Products_current_category']) continue;
                            if($val['range_from']>=$value[$valueid]['range_from'] ){
                                $value[$valueid]['range_to'] = $val['range_from'];
                                break;
                            }
                        }
                    }
                    else if($_POST['fieldchage']=='range_to'){
                        if(!$this->pricebreaks_range_comparasion($query['pricebreaks'],$value[$valueid]['range_to'],$valueid)){
                            echo 'range_to_in_range';
                            die;
                        }
                        $arr_tmp = $this->opm->aasort($query['pricebreaks'],'range_to',-1);
                        foreach($arr_tmp as $key=>$val){
                            if($val['deleted'] || $key==$valueid) continue;
                            if(isset($_SESSION['Products_current_category'])
                                &&$val['sell_category']!=$_SESSION['Products_current_category']) continue;
                            if($val['range_to']<=$value[$valueid]['range_to']){
                                $value[$valueid]['range_from'] = $val['range_to'];
                                break;
                            }
                        }
                    }
                    $arr_tmp = $this->opm->aasort($value,'range_from',1);
                    $arr_return['pricebreaks'] = array();
                    if($value[$valueid]['range_from']!=$value[$valueid]['range_to']){
                        foreach($arr_tmp as $value){
                            if($value['deleted']) continue;
                            $arr_return['pricebreaks'][] = $value;
                        }
                    } else if($value[$valueid]['range_from']==$value[$valueid]['range_to']){
                        echo 'range_from_equals_range_to';
                        die;
                    }
                     return $arr_return;
                }
            }
        }


		/* Main field anh huong den note supplier */
		$main_field = array(
							'sku','name','company_name',
							'sizew','sizeh','sizew_unit',
							'sizeh_unit','sell_by','oum','sell_price','oum_depend','unit_price'
							);
		$arr_num = array('sizew','sizeh','sell_price','unit_price');

		$arr_change_unitprice = array('sizew','sizeh','sizew_unit','sizeh_unit','oum','oum_depend','sell_by','sell_price','unit_price');

		if(in_array($field,(array)$main_field)){

			$idm = $this->get_id();
			if($field=='sell_by' && $value=='unit'){
				$arr_return['oum'] = $query['oum'] = 'unit';
			}
			if($field=='sell_by' && $value=='area'){
				$arr_return['oum'] = $query['oum'] = 'Sq.ft.';
			}

			//tính lại unit price
			if(in_array($field,(array)$arr_change_unitprice)){
				$items = $query;
				if(isset($arr_return['oum']))
				$items['oum'] = $arr_return['oum'];
				$items[$field] = $value;
				$cal_price = new cal_price();
				$cal_price->arr_product_items = $items;
				$cal_price->cal_unit_price_for_product();

				foreach($arr_change_unitprice as $keys){
					if(isset($cal_price->arr_product_items[$keys]))
						$arr_return[$keys] = $cal_price->arr_product_items[$keys];
				}
			}

			//update supplier trong product cha neu la vendor
			if($idm !='' && isset($query['parent_product_id']) && strlen((string)$query['parent_product_id'])==24){
				$parent_pro = $this->opm->select_one(array('_id' => $query['parent_product_id']));
				if(isset($parent_pro['supplier']) && is_array($parent_pro['supplier']) && count($parent_pro['supplier'])>0){
				foreach($parent_pro['supplier'] as $kk=>$vv){
					$arr_tmp = array();
					$arr_tmp['supplier'] = $parent_pro['supplier'];
					$arr_tmp['_id'] 	 = $parent_pro['_id'];
						if(!$vv['deleted'] && isset($vv['product_id']) && (string)$vv['product_id'] ==$idm){
							//neu co vendor trong parent product thi update lai
							if($field =='company_name'){
								$arr_tmp['supplier'][$kk][$field] = $value;
								$arr_tmp['supplier'][$kk]['company_id'] = new MongoId($valueid);
							}else if(in_array($field,(array)$arr_num)){
								$arr_tmp['supplier'][$kk][$field] = (float)$value;
							}else
								$arr_tmp['supplier'][$kk][$field] = (string)$value;

							foreach($arr_change_unitprice as $keys){
								if(isset($arr_return[$keys])){
									if(in_array($keys,(array)$arr_num))
										$arr_tmp['supplier'][$kk][$keys] = (float)$arr_return[$keys];
									else
										$arr_tmp['supplier'][$kk][$keys] = (string)$arr_return[$keys];
								}
							}
							$this->opm->save($arr_tmp);
						}

						//kiem tra va set lai current
					}

				}

			}


			/* Current Supplier */
			if($field =='company_name'){
                if (strlen($valueid) == 24)
				    $arr_return['company_id'] = new MongoId($valueid);
                else
                    $arr_return['company_id'] = '';

				//set default cho not supplier neu la product


				if(isset($query['supplier']) && is_array($query['supplier']) && count($query['supplier'])>0){
					$supplier_po = $query['supplier'];
					$arrcost = array();
					//$cal_price=new cal_price();
					//loop va xac dinh current = on
					foreach($supplier_po as $kk=>$vv){
						if(!$vv['deleted'] && $vv['current']=='on'){
							$reset_key = $kk;
						}
						if(isset($vv['company_id']) && (string)$vv['company_id'] == $valueid){
							$comid = $vv['company_id'];
							//$cal_price->arr_product_items = $vv;
							//$cal_price->cal_area();
							//$aftercal = $cal_price->arr_product_items;
						}else
							continue;

						if((!isset($newsell_price)) || $newsell_price > (float)$vv['unit_price'] && isset($newsell_price)){							$newkeyset = $kk;
							$newsell_price = (float)$vv['unit_price'];
						}


					}
					//kiem tra va set lai current
					if(isset($newkeyset)  && isset($reset_key)){
						$supplier_po[$reset_key]['current'] = '';
						$supplier_po[$newkeyset]['current'] = 'on';
						$arr_return['supplier'] = $supplier_po;
					}
					//else : tao supplier moi
				}

			}


			// Tao moi hoac update Pricing
			if($field=='sell_price'){
				//kiem va lap vong sellprices
				if(isset($query['sellprices']) && is_array($query['sellprices']) && count($query['sellprices'])>0){
					$is_null = 1;
					foreach($query['sellprices'] as $keys=>$values){
						if(isset($values['sell_default']) && $values['sell_default']==1 && isset($values['deleted']) && !$values['deleted']){
							$is_null = 0;
							$arr_return['sellprices'] = $query['sellprices'];
							$arr_return['sellprices'][$keys]['sell_category'] = 'Retail';
							$arr_return['sellprices'][$keys]['sell_unit_price'] = $value;
							break;
						}
					}

					if($is_null==1){
						$arr_return['sellprices'][0]['sell_category'] = 'Retail';
						$arr_return['sellprices'][0]['sell_unit_price'] = $value;
						$arr_return['sellprices'][0]['sell_default'] = 1;
						$arr_return['sellprices'][0]['deleted'] = false;
					}

				}else{
					$arr_return['sellprices'][0]['sell_category'] = 'Retail';
					$arr_return['sellprices'][0]['sell_unit_price'] = $value;
					$arr_return['sellprices'][0]['sell_default'] = 1;
					$arr_return['sellprices'][0]['deleted'] = false;
				}
			}


		}





		/* Supplier po của main product */
		if($field =='supplier'){
			$arr_temp = $arr_vendor = array();
			//save product vendor
			if(isset($value[$valueid])){
				$compo = $value[$valueid];

				if(isset($compo['product_id']))
					$arr_temp['_id'] = $compo['product_id'];
				if(isset($compo['company_name']))
					$arr_temp['company_name'] = $compo['company_name'];
				if(isset($compo['company_id']))
					$arr_temp['company_id'] = $compo['company_id'];
				if(isset($compo['sku']))
					$arr_temp['sku'] = $compo['sku'];
				if(isset($compo['name']))
					$arr_temp['name'] = $compo['name'];
				$cal_price = new cal_price();
				$cal_price->arr_product_items = $compo;
				$cal_price->cal_unit_price_for_product();
				$arrnew = $cal_price->arr_product_items;
				foreach($arr_change_unitprice as $keys){
					if(isset($arrnew[$keys])){
						$arr_temp[$keys] = $value[$valueid][$keys] = $arrnew[$keys];
					}
				}
				$arr_temp['parent_product_name'] = $query['name'];
				$arr_temp['parent_product_id'] = $query['_id'];
				$arr_temp['parent_product_code'] = $query['code'];
				$arr_temp['group_type'] = 'BUY';
				//$arr_temp['code'] = $this->opm->get_auto_code('code');
				//$arr_temp['status'] = 1;
				$arr_temp['product_type'] = 'Vendor Stock';

				if(isset($compo['product_id'])) // chi cho update
					$this->opm->save($arr_temp);
				//$value[$count]['product_id'] = $this->opm->mongo_id_after_save;

				//Set Current supplier cho product
				if(isset($compo['current']) && $compo['current']=='on' && isset($compo['company_name']) && isset($compo['company_id'])){
					$arr_return['company_name'] = $compo['company_name'];
					$arr_return['company_id'] = $compo['company_id'];

					//them neu chua co item nay
					$more_madeup = 1; $idmore = -1;
					if(isset($query['madeup']) && is_array($query['madeup']) && count($query['madeup'])>0){
						$arr_return['madeup'] = $query['madeup'];
						foreach($query['madeup'] as $keys => $values){
							if(isset($values['deleted']) && !$values['deleted'] && $values['product_id'] == $compo['product_id']){
								$more_madeup = 0; //ko cho tao moi

							}
							$idmore = $keys;

						}
					}
					if($more_madeup == 1){
						$idmore = 0;
						if(isset($compo['product_id']))
							$arr_return['madeup'][$idmore]['product_id'] = $compo['product_id'];
						if(isset($compo['name']))
							$arr_return['madeup'][$idmore]['product_name'] = $compo['name'];
						if(isset($compo['oum']))
							$arr_return['madeup'][$idmore]['oum'] = $compo['oum'];
						if(isset($compo['unit_price']))
							$arr_return['madeup'][$idmore]['unit_price'] = $compo['unit_price'];
						$arr_return['madeup'][$idmore]['product_type'] = '';
						$arr_return['madeup'][$idmore]['markup'] = 0;
						$arr_return['madeup'][$idmore]['margin'] = 0;
						$arr_return['madeup'][$idmore]['quantity'] = 1;
						$arr_return['madeup'][$idmore]['deleted'] = false;
					}

				}

			}
			$arr_return[$field] = $value;
		}

		/* Locations */
		$unset = 0;
		if($field =='locations'){
			if(isset($query['locations']) && is_array($query['locations']) && count($query['locations'])>0){
				$count = count($value)-1;
				echo $count.'<br />';
				if(isset($value[$count]) && isset($value[$count]['location_id'])){
					$location_id = $this->arr_list_changed($value[$count]);
					$location_id = $location_id['location_id'];
					echo $location_id;
					foreach($value as $kk=>$vv){
						$location_list = $this->arr_list_changed($vv);
						if( $count != $kk && isset($location_list['location_id']) && $location_list['location_id']==$location_id){
							echo $location_list['location_id'];
							unset($value[$count]);
						}
					}
					$arr_return[$field] = $value;
				}
			}
		}

		/* Stocktakes */
		if($field =='stocktakes'){
			$arr_return['stocktakes'] = $value;
			if(isset($value[$valueid])){
				$arr_tmp = $value[$valueid];

				$add_local = 1;
				$last_id = -1;

				//lap vong location de gan gia tri cu vao qty_in_stock (stocktakes) và check tao moi, dem chi muc cuoi
				if(isset($query['locations']) && is_array($query['locations']) && count($query['locations'])>0){
					$arr_return['locations'] = $query['locations'];
					foreach($query['locations'] as $kk=>$vv){
						//truong hop ton tai kho can tim và không có qty_in_stock (stocktakes)
						if(!$vv['deleted'] && isset($arr_tmp['location_id']) && $vv['location_id']==$arr_tmp['location_id'] && (!isset($arr_tmp['qty_in_stock']) || (isset($arr_tmp['qty_in_stock']) && $arr_tmp['qty_in_stock']=='')) ){
							if(isset($vv['total_stock']) && isset($vv['total_stock'])!='')
								$arr_tmp['qty_in_stock'] = (int)$vv['total_stock'];
							else
								$arr_tmp['qty_in_stock'] = 0;
							$add_local = 0; //bật cờ ko cho add

						//truong hop co tim thay id location, ko thay đổi $arr_tmp['qty_in_stock']
						}else if(!$vv['deleted'] && isset($arr_tmp['location_id']) && $vv['location_id']==$arr_tmp['location_id']){
							$add_local = 0; //bật cờ ko cho add
						}

						$last_id = $kk; //đếm id cuối cùng
					}

				}

				//thay doi qty_amended
				if(isset($arr_tmp['qty_counted']) && $arr_tmp['qty_counted']!=''){
					$arr_tmp['qty_amended'] = (int)$arr_tmp['qty_counted'];
					if(isset($arr_tmp['qty_in_stock']))
						 $arr_tmp['qty_amended'] = $arr_tmp['qty_amended'] - (int)$arr_tmp['qty_in_stock']; //tính amend
					$arr_return['qty_in_stock'] = (int)$arr_tmp['qty_counted'];//gan so moi vao total bên general

					//UPDATE LOCATION
					if(isset($query['locations']) && is_array($query['locations']) && count($query['locations'])>0){
						$arr_return['qty_in_stock'] = 0;
						foreach($query['locations'] as $kk=>$vv){ // lặp vòng location để tính tổng và gán total mới
							if(!$vv['deleted']){
								if(isset($arr_tmp['location_id']) && $vv['location_id']==$arr_tmp['location_id']){
									$arr_return['locations'][$kk]['total_stock'] = (int)$arr_tmp['qty_counted'];
								}
								$arr_return['qty_in_stock'] += (int)$arr_return['locations'][$kk]['total_stock'];
							}
						}
					}
				}

				// Add location
				if($add_local ==1){
					$new_local = array();
					if(isset($arr_tmp['location_id']))
						$new_local['location_id'] = $arr_tmp['location_id'];
					if(isset($arr_tmp['location_name']))
						$new_local['location_name'] = $arr_tmp['location_name'];
					if(isset($arr_tmp['location_type']))
						$new_local['location_type'] = $arr_tmp['location_type'];
					if(isset($arr_tmp['stock_usage']))
						$new_local['stock_usage'] = $arr_tmp['stock_usage'];
					$new_local['low'] = '';
					$new_local['deleted'] = false;

					$new_local['qty_in_stock'] = (int)$arr_tmp['qty_counted'];

					$arr_return['locations'][$last_id+1] = $new_local;
				}

				//set user
				$arr_tmp['stocktakes_by'] = $this->opm->user_name();
				$arr_tmp['stocktakes_by_id'] = $this->opm->user_id();
				$value[$valueid] = $arr_tmp;
			}
			$arr_return['stocktakes'] = $value;
		}




		/* Options product */
		if($field =='options'){
			/*if(isset($value[$valueid])){
				$cal_price = new cal_price();
				$cal_price->arr_product_items = $value[$valueid];
				$cal_price->cal_price_in_markup_margin();
				$value[$valueid] = array_merge($value[$valueid],(array)$cal_price->arr_product_items);
			}*/

			//tim group_type
			if(isset($value[$valueid]) && isset($value[$valueid]['option_group']) && $value[$valueid]['option_group']!=''){
				foreach($value as $kk=>$vv){
					if($kk!=$valueid && isset($vv['group_type']) && $vv['group_type']!='' && $vv['option_group'] == $value[$valueid]['option_group'])
						$value[$valueid]['group_type'] = $vv['group_type'];

				}
			}


			//kiem tra va update neu co check Require
			if(isset($value[$valueid]) && isset($value[$valueid]['require']) && $value[$valueid]['require']==1 && isset($value[$valueid]['group_type']) && $value[$valueid]['group_type']=='Exc'){
				foreach($value as $kk=>$vv){
					if($kk!=$valueid && isset($vv['option_group']) && $vv['option_group'] == $value[$valueid]['option_group'])
						$value[$kk]['require'] = 0;
				}
			}

			$arr_return[$field] = $value;
		}
        else if($field == 'warehousing'){
            $value[$valueid]['warehousing_by'] = $this->opm->user_name();
            $value[$valueid]['warehousing_by_id'] = $this->opm->user_id();
            $value[$valueid]['warehousing_date'] = new MongoDate();
            $arr_return['warehousing'] = $value;
            $product = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())), array('warehousing','in_stock','prime_cost','specification','von_luu'));
            $new_qty = 0;
            if(!isset($product['warehousing'][$valueid]['qty_in_stock']))
                $product['warehousing'][$valueid]['qty_in_stock'] = 0;
            $new_qty = $value[$valueid]['qty_in_stock'] - $product['warehousing'][$valueid]['qty_in_stock'];
            if(!isset($product['in_stock']))
                $product['in_stock'] = 0;
            if($new_qty != 0){
                $product['in_stock'] += $new_qty;
            }
            if(!isset($product['von_luu']))
                $product['von_luu'] = 0;
            $product['von_luu'] = $product['prime_cost'] * $product['specification'] * $product['in_stock'];
            $arr_return['in_stock'] = $product['in_stock'];
            $arr_return['von_luu'] = $product['von_luu'];
         }

     /*    if( $our_rep_id != '' ){
            $cond['$or']=array(
                array('contacts' => array('$elemMatch' => array('contact_id' => new MongoId($our_rep_id)) )),
                array('our_rep_id' => new MongoId($our_rep_id))
            );*/

         if($field == 'prime_cost'){
            $this->selectModel('Salesorder');
            $id = new MongoId($this->get_id());
            $product_in_salesorder = $this->Salesorder->select_all(array(
                                                                   'arr_where' => array(
                                                                                    'products' =>array(
                                                                                                    '$elemMatch' => array(
                                                                                                        'deleted' => false,
                                                                                                        'products_id' => $id
                                                                                                        ),
                                                                                                    ),
                                                                                    ),
                                                                   'arr_field' => array('products'),
                                                                   ));

            foreach($product_in_salesorder as $order){
                foreach($order['products'] as $k => $v_product){
                    if( isset($v_product['deleted']) && $v_product['deleted'] ) continue;
                    if( !isset($v_product['products_id']) || $v_product['products_id'] != $id ) continue;
                    $order['products'][$k]['amount_interest'] = $order['products'][$k]['amount'] - ($order['products'][$k]['quantity'] * $order['products'][$k]['specification'] * $value) ;
                }
                $this->Salesorder->save($order);
            }
        }


		//pr($arr_return);die;
		return $arr_return;
	}

	public function ajax_pricing_method() {
        $arr_input = array();
        $arr_input['module_id'] = $this->get_id();
        $arr_input['option_name'] = 'pricing_method';
        if (isset($_POST['pmid']) && $_POST['pmid'] != '') {
            $arr_input['option_id'] = $_POST['pmid'];
            $arr_input['arr_value']['id'] = $_POST['pmid'];
        } else {
            $arr_input['arr_value']['id'] = 0;
        }

        if (isset($_POST['note']))
            $arr_input['arr_value']['pricing_method_name'] = $_POST['note'];
        if (isset($_POST['rule_id']))
            $arr_input['arr_value']['rule_id'] = new MongoId($_POST['rule_id']);
        if (isset($_POST['unit_price']))
            $arr_input['arr_value']['pricing_rule_unit'] = $_POST['unit_price'];

        if (isset($_POST['func']) && $_POST['func'] == 'add') {
            $str_return = $this->opm->add_option($arr_input);
        } else if (isset($_POST['func']) && $_POST['func'] == 'update') {
            $str_return = $this->opm->update_option($arr_input);
        } else if (isset($_POST['func']) && $_POST['func'] == 'delete') {
            $str_return = $this->opm->delete_option($arr_input);
        }
        die;
    }

    public function general() {
        $ids = $this->get_id();
        $subdatas = $this->opm->get_data_general($ids);
        $pricing_method = $subdatas['pricing_method'];
        if (isset($pricing_method[0]['id']))
            $pricing_method[0]['pricing_method_id'] = $pricing_method[0]['id'];
        if (isset($pricing_method[0]['rule_id']) && $pricing_method[0]['rule_id'] != '') {
            $this->selectModel('Rule');
            $ruledata = array();
            $ruledata = $this->Rule->select_one(array('_id' => new MongoId($pricing_method[0]['rule_id'])));
            $pricing_method[0]['rule_name'] = $ruledata['name'];
            $pricing_method[0]['rule_formula'] = $ruledata['map_formula'];
            $pricing_method[0]['rule_description'] = $ruledata['description'];
        }
        $subdatas['pricing_method'] = $pricing_method;
        if (isset($subdatas['supplier']) && count($subdatas['supplier']) > 0)
            foreach ($subdatas['supplier'] as $kk => $vv) {
                if (isset($vv['company_id'])) {
                    $subdatas['supplier'][$kk]['company_name'] = $this->get_name('Company', $vv['company_id']);
                }
            }
        $arr_temp = $subdatas['same_category'];
        $subdatas['same_category'] = iterator_to_array($arr_temp);

        if ($ids != '' && isset($subdatas['same_category']) && isset($subdatas['same_category'][$ids]))
            unset($subdatas['same_category'][$ids]);


		//stock tracking
		$subdatas['stocktracking'] = array();
		//product options
		$subdatas['productoptions'] = array();
		$stocktracking = $productoptions = $product = $option_select_custom= array();
		$groupstr = '';
		$arr_set = $this->opm->arr_settings;
		$field_list = $arr_set['relationship']['general']['block']['stocktracking']['field'];

		if($ids!=''){
			$query = $this->opm->select_one(array('_id'=>new MongoId($ids)));
			foreach($field_list as $key => $value){
				if(isset($query[$key])){
					$stocktracking[$key] = $query[$key];
				}
			}
			$options_data = $this->opm->options_data($ids);
			if(isset($options_data['productoptions']))
			$subdatas['productoptions'] = $options_data['productoptions'];
			$option_select_custom['option_group'] = $options_data['custom_option_group'];
			$groupstr = $options_data['groupstr'];
		}






		//set empty
		if(!isset($query['check_stock_stracking']) || ( isset($query['check_stock_stracking']) && $query['check_stock_stracking']==0))
			$subdatas['stocktracking'] = array();
		else
			$subdatas['stocktracking'] = $stocktracking;
        $this->set('subdatas', $subdatas);

		$arr_options_custom = $this->set_select_data_list('relationship', 'general');
		$this->set('arr_options_custom', $arr_options_custom);

		$option_select_custom['group_type'] = array('Inc'=>'Inc','Exc'=>'Exc');
		$this->set('option_select_custom', $option_select_custom);

		$total = 0;
        if(isset($query['products_upload'])&&!empty($query['products_upload']))
        {
            /*end($query['products_upload']);
            $key = key($query['products_upload']);
            while(!empty($query['products_upload'])&&$query['products_upload'][$key]['deleted']){
                unset($query['products_upload'][$key]);
                if(!empty($query['products_upload'])){
                    end($query['products_upload']);
                    $key = key($query['products_upload']);
                }
            }
            if(isset($query['products_upload'][$key])&&!$query['products_upload'][$key]['deleted']){
                $this->set('img_path',$query['products_upload'][$key]['path']);
                $this->set('doc_id',$query['products_upload'][$key]['doc_id']);
                $this->set('key',$key);
            }*/
        }
        //pr($query);die;
        if (!empty($query['product_slide_image']))
            $this->set('image_slide', $query['product_slide_image']);
        else $this->set('image_slide',array());

        if (!empty($query['product_slide_image_id']))
            $this->set('image_slide_id', $query['product_slide_image_id']);
        else $this->set('image_slide_id',array());



		$this->set('groupstr', $groupstr);
		$this->set('is_choice','1');
    }

    	// Stock
	public function stock() {
		$ids = $this->get_id();
        if($ids!=''){
            $query = $this->opm->select_one(array('_id'=> new MongoId($ids)));
        }
        $subdatas['stock_summary']  = array();
        if(isset($query['qty_in_stock']))
            $subdatas['stock_summary']['in_stock_total'] = (float)$query['qty_in_stock'];
        else
            $subdatas['stock_summary']['in_stock_total'] = 0;

        $subdatas['stock_summary']['in_stock_av'] = $subdatas['stock_summary']['in_stock_total'];
        $subdatas['stock_summary']['loan_total'] = '';
        $subdatas['stock_summary']['loan_av'] = $subdatas['stock_summary']['loan_total'];
        $subdatas['stock_summary']['internal_assets'] = '';
        $subdatas['stock_summary']['total'] = (float)$subdatas['stock_summary']['in_stock_total'] + (float)$subdatas['stock_summary']['loan_total'] + (float)$subdatas['stock_summary']['internal_assets'];

        $subdatas['stock_summary']['purchases_total'] = 0;
        $subdatas['stock_summary']['purchases_current'] = 0;
        $subdatas['stock_summary']['used_on_jobs'] = '';
        $subdatas['stock_summary']['used_on_stages'] = '';
        $subdatas['stock_summary']['used_on_tasks'] = '';
        $subdatas['stock_summary']['used_on_timelogs'] = '';
        $subdatas['stock_summary']['staff_expenses'] = '';
        $subdatas['stock_summary']['assembly_add_total'] = '';
        $subdatas['stock_summary']['assembly_add_current'] = '';
        $subdatas['stock_summary']['assembly_use_total'] = '';
        $subdatas['stock_summary']['assembly_use_current'] = '';
        $subdatas['stock_summary']['sales_total'] = '';
        $subdatas['stock_summary']['sales_current'] = '';
        $subdatas['stock_summary']['resource_total'] = '';
        $subdatas['stock_summary']['resource_current'] = '';
        $subdatas['stock_summary']['min_stock'] = '';
        $subdatas['stock_summary']['low'] = 'Low';

        //locations
        $subdatas['locations']  = array();
        $location_total = array();
        $location_total['onpo'] = '0';
        $location_total['minstock'] = '0';
        $location_total['avalible'] = '0';
        $location_total['assembly'] = '0';
        $location_total['inuse'] = '0';
        $location_total['onso'] = '0';
        $location_total['total'] = '0';
        if(isset($query['locations']) && is_array($query['locations']) && count($query['locations'])>0){
            $locations = array();
            foreach($query['locations'] as $keys=>$value){
                $locations[$keys] = $value;
                if(isset($locations[$keys]['on_po']))
                    $location_total['onpo'] += (int)$locations[$keys]['on_po'];
                if(isset($locations[$keys]['min_stock']))
                    $location_total['minstock'] += (int)$locations[$keys]['min_stock'];
                if(isset($locations[$keys]['avalible']))
                    $location_total['avalible'] += (int)$locations[$keys]['avalible'];
                if(isset($locations[$keys]['in_assembly']))
                    $location_total['assembly'] += (int)$locations[$keys]['in_assembly'];
                if(isset($locations[$keys]['in_use']))
                    $location_total['inuse'] += (int)$locations[$keys]['in_use'];
                if(isset($locations[$keys]['on_so']))
                    $location_total['onso'] += (int)$locations[$keys]['on_so'];
                if(isset($locations[$keys]['total_stock']))
                    $location_total['total'] += (int)$locations[$keys]['total_stock'];
                if(isset($locations[$keys]['total_stock']) && isset($locations[$keys]['min_stock']) && $locations[$keys]['total_stock'] < $locations[$keys]['min_stock'])
                    $locations[$keys]['low'] = 1;
                else
                    $locations[$keys]['low'] = 0;
            }
            $subdatas['locations'] = $locations;
        }

        $this->set('location_total', $location_total);



        //stock takes

        $subdatas['stocktakes']     = array();
        $stocktakes = array();

        $total = 0;$f_key = 0;
        if(isset($query['stocktakes']) && is_array($query['stocktakes']) && count($query['stocktakes'])>0){
            $arr_stock = $query['stocktakes'];
            $sum = count($arr_stock);
            for($m=$sum-1;$m>=0;$m--){
                if($f_key == 0 && !$arr_stock[$m]['deleted']){
                    $f_key = $m;
                }
                $stocktakes[$m] = $arr_stock[$m];
                if(isset($arr_stock[$m]['qty_amended']) && !$arr_stock[$m]['deleted'])
                    $total += (float)$arr_stock[$m]['qty_amended'];
            }
            $subdatas['stocktakes'] = $stocktakes;
        }
        $old_qty = '';
        if (isset($stocktakes[$f_key]['qty_counted']))
        {
            $old_qty = $this->opm->format_currency($stocktakes[$f_key]['qty_counted']);
        }
        $this->set('old_qty',$old_qty);
        $this->set('total', $total);
        $this->set('first_amended', $f_key);

        $total = 0;$f_key = 0;
        if(isset($query['warehousing']) && is_array($query['warehousing']) && count($query['warehousing'])>0){
            $warehousing = array();
            usort($query['warehousing'], function($a,$b){
                return $a['warehousing_date'] < $b['warehousing_date'];
            });
            $arr_stock = $query['warehousing'];
            $sum = count($arr_stock);
            for($m=$sum-1;$m>=0;$m--){
                if($f_key == 0 && !$arr_stock[$m]['deleted']){
                    $f_key = $m;
                }
                if(isset($arr_stock[$m]['purchaseorder_id'])){
                    $arr_stock[$m]['xlock']['qty_in_stock'] = 1;
                    $arr_stock[$m]['remove_deleted'] = 1;
                }
                $stocktakes[$m] = $arr_stock[$m];
                if(isset($arr_stock[$m]['qty_in_stock']) && !$arr_stock[$m]['deleted'])
                    $total += (float)$arr_stock[$m]['qty_in_stock'];
            }
        }
        $arr_product = $this->get_product_in_salesorder();
        $arr_return_product = $this->get_product_in_return_salesorder();
        $array_merge = array_merge($arr_product['product'], $arr_return_product['product']);
        $subdatas['delivery'] = $array_merge;
        $arr_product_in_pur = $this->get_product_in_pur_order();
        $arr_return_pur_order = $this->get_product_in_return_pur_order();
        $arr_result = array_merge($arr_product_in_pur['product'], $arr_return_pur_order['product']);
        //if($stocktakes['code'] == )
        if(count($stocktakes) == 0)
            $stocktakes = array();
        $last_arr_result = array_merge($arr_result, $stocktakes);
        $subdatas['warehousing']     = array();

        $subdatas['warehousing'] =  $last_arr_result;
        $total_in_salesorder = 0;
        $total_in_salesorder = $arr_product['total_in_salesorder'] - $arr_return_product['total_in_salesorder'];
        $this->set('total_in_salesorder',$total_in_salesorder);
        $this->set('in_stock', $query['in_stock']);
        $this->set('subdatas', $subdatas);
        $this->set_select_data_list('relationship', 'stock');
    }

    public function get_product_in_pur_order(){
        $arr_products = array('product' => array(),'total_in_salesorder' => 0);
        $this->selectModel('Purchaseorder');
        $arr_pur_orders = $this->Purchaseorder->collection->aggregate(
                        array(
                            '$match'=>array('purchase_orders_status' => array('$in' => array('Hoàn thành')),
                            'deleted' => false,
                            'products' => array(
                                '$exists' => true,
                                '$nin' => array('',null)
                                ),
                            ),
                        ),
                        array(
                            '$unwind'=>'$products',
                        ),
                         array(
                            '$match'=>array('products.products_id' => new MongoId($this->get_id()),'products.deleted' => false)
                        ),
                        array(
                            '$project'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','purchord_date'=>'$purchord_date','products'=>'$products','purchase_orders_status'=>'$purchase_orders_status')
                        ),
                        array(
                            '$group'=>array(
                                          '_id'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','purchord_date'=>'$purchord_date','purchase_orders_status'=>'$purchase_orders_status'),
                                          'products'=>array('$push'=>'$products')
                                        )
                        )
                    );
        if(isset($arr_pur_orders['ok']) && !empty($arr_pur_orders['result'])){
            $arr_products['total_in_salesorder']  = 0;
            foreach($arr_pur_orders['result'] as $value){
                $qty_in_salesorder = 0;
                foreach($value['products'] as $product){
                    $qty_in_salesorder += isset($product['quantity']) ? $product['quantity'] : 0;
                }
                $arr_products['total_in_salesorder']  += $qty_in_salesorder;
                $arr_products['product'][] = array(
                        'warehousing_date' => date($value['_id']['purchord_date']->sec),
                        'code' => $value['_id']['code'],
                        'warehousing_by_id' => "123",
                        'warehousing_detail' => 'Tạo từ mua hàng #'. $value['_id']['code'],
                        'location_name' => $value['_id']['company_name'],
                        'qty_in_stock' => $qty_in_salesorder,
                    );
            }
        }
        return $arr_products;
    }

    public function get_product_in_return_pur_order(){
        $arr_products = array('product' => array(),'total_in_salesorder' => 0);
        $this->selectModel('Returnpurchaseorder');
        $arr_salesorders = $this->Returnpurchaseorder->collection->aggregate(
                        array(
                            '$match'=>array('purchase_orders_status' => array('$in' => array('Hoàn thành')),
                            'deleted' => false,
                            'products' => array(
                                '$exists' => true,
                                '$nin' => array('',null)
                                ),
                            ),
                        ),
                        array(
                            '$unwind'=>'$products',
                        ),
                         array(
                            '$match'=>array('products.products_id' => new MongoId($this->get_id()),'products.deleted' => false)
                        ),
                        array(
                            '$project'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','purchord_date'=>'$purchord_date','products'=>'$products','purchase_orders_status'=>'$purchase_orders_status')
                        ),
                        array(
                            '$group'=>array(
                                          '_id'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','purchord_date'=>'$purchord_date','purchase_orders_status'=>'$purchase_orders_status'),
                                          'products'=>array('$push'=>'$products')
                                        )
                        )
                    );
        if(isset($arr_salesorders['ok']) && !empty($arr_salesorders['result'])){
            $arr_products['total_in_salesorder']  = 0;
            foreach($arr_salesorders['result'] as $value){
                $qty_in_salesorder = 0;
                foreach($value['products'] as $product){
                    $qty_in_salesorder += isset($product['quantity']) ? $product['quantity'] : 0;
                }
                $arr_products['total_in_salesorder']  += $qty_in_salesorder;
                $arr_products['product'][] = array(
                        'warehousing_date' => date($value['_id']['purchord_date']->sec),
                        'code' => $value['_id']['code'],
                        'warehousing_by_id' => "123",
                        'warehousing_detail' => 'Tạo từ trả hàng #'. $value['_id']['code'],
                        'location_name' => $value['_id']['company_name'],
                        'qty_in_stock' => $qty_in_salesorder,
                    );
            }
        }
        return $arr_products;
    }

   public function get_product_in_salesorder(){
        $arr_products = array('product' => array(),'total_in_salesorder' => 0);
        $this->selectModel('Salesorder');
        $arr_salesorders = $this->Salesorder->collection->aggregate(
                        array(
                            '$match'=>array('status' => array('$in' => array('Hoàn thành')),
                            'deleted' => false,
                            'products' => array(
                                '$exists' => true,
                                '$nin' => array('',null)
                                ),
                            ),
                        ),
                        array(
                            '$unwind'=>'$products',
                        ),
                         array(
                            '$match'=>array('products.products_id' => new MongoId($this->get_id()),'products.deleted' => false)
                        ),
                        array(
                            '$project'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','salesorder_date'=>'$salesorder_date','products'=>'$products','status'=>'$status')
                        ),
                        array(
                            '$group'=>array(
                                          '_id'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','salesorder_date'=>'$salesorder_date','status'=>'$status'),
                                          'products'=>array('$push'=>'$products')
                                        )
                        )
                    );
        if(isset($arr_salesorders['ok']) && !empty($arr_salesorders['result'])){
            $arr_products['total_in_salesorder']  = 0;
            foreach($arr_salesorders['result'] as $value){
                $qty_in_salesorder = 0;
                foreach($value['products'] as $product){
                    $qty_in_salesorder += isset($product['quantity']) ? $product['quantity'] : 0;
                }
                $arr_products['total_in_salesorder']  += $qty_in_salesorder;
                $arr_products['product'][] = array(
                        'code' => $value['_id']['code'],
                        'company_name' => $value['_id']['company_name'],
                        'company_id' => $value['_id']['company_id'],
                        'salesorder_date' => date('d, M Y',$value['_id']['salesorder_date']->sec),
                        'salesorder_id' => $value['_id']['_id'],
                        'qty_in_salesorder' => $qty_in_salesorder,
                        'status' => $value['_id']['status'],
                         'status' => 'Tạo từ đơn hàng #'. $value['_id']['code']
                    );
            }
        }
        return $arr_products;
    }
//sai cho nay
    public function get_product_in_return_salesorder(){
        $arr_products = array('product' => array(),'total_in_salesorder' => 0);
        $this->selectModel('Returnsalesorder');
        $arr_salesorders = $this->Returnsalesorder->collection->aggregate(
                        array(
                            '$match'=>array('status' => array('$in' => array('Hoàn thành')),
                            'deleted' => false,
                            'products' => array(
                                '$exists' => true,
                                '$nin' => array('',null)
                                ),
                            ),
                        ),
                        array(
                            '$unwind'=>'$products',
                        ),
                         array(
                            '$match'=>array('products.products_id' => new MongoId($this->get_id()),'products.deleted' => false)
                        ),
                        array(
                            '$project'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','salesorder_date'=>'$salesorder_date','products'=>'$products','status'=>'$status')
                        ),
                        array(
                            '$group'=>array(
                                          '_id'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','salesorder_date'=>'$salesorder_date','status'=>'$status'),
                                          'products'=>array('$push'=>'$products')
                                        )
                        )
                    );
        if(isset($arr_salesorders['ok']) && !empty($arr_salesorders['result'])){
            $arr_products['total_in_salesorder']  = 0;
            foreach($arr_salesorders['result'] as $value){
                $qty_in_salesorder = 0;
                foreach($value['products'] as $product){
                    $qty_in_salesorder += isset($product['quantity']) ? $product['quantity'] : 0;
                }
                $arr_products['total_in_salesorder']  += $qty_in_salesorder;
                $arr_products['product'][] = array(
                        'code' => $value['_id']['code'],
                        'company_name' => $value['_id']['company_name'],
                        'company_id' => $value['_id']['company_id'],
                        'salesorder_date' => date('d, M Y',$value['_id']['salesorder_date']->sec),
                        'salesorder_id' => $value['_id']['_id'],
                        'qty_in_salesorder' => $qty_in_salesorder,
                        //'status' => $value['_id']['status']
                        'status' => 'Tạo từ hàng trả #'. $value['_id']['code']
                    );
            }
        }
        return $arr_products;
    }

    public function other() {
		$sub_tab = array();
		$sub_tab['otherdetails'] = $this->get_option_data('otherdetails');
		$sub_tab['noteactive'] = $this->get_option_data('noteactive');
		//$sub_tab['production_step'] = $this->get_option_data('production_step');
        $sub_tab['production_step'] = $this->opm->get_product_asset($this->get_id());
		$this->set('subdatas', $sub_tab);
		$this->set('your_user_name', $this->opm->user_name());
		$this->set('your_user_id', $this->opm->user_id());
		$this->set_select_data_list('relationship', 'other');
		$option_select_custom['oum'] = array_merge(
		     $this->Setting->select_option_vl(array('setting_value'=>'product_oum_area'))
		     ,$this->Setting->select_option_vl(array('setting_value'=>'product_oum_lengths'))
		     ,$this->Setting->select_option_vl(array('setting_value'=>'product_oum_unit'))
		);
		$this->selectModel('Equipment');
		$option_select_custom['tag'] = $this->Equipment->select_combobox_asset_old();
		$this->set('option_select_custom', $option_select_custom);
    }

       public function category_data(){
            $query = $this->opm->category->find()->limit(1000);
            $data = iterator_to_array($query);
            foreach($data as $key => $value){
               if(isset($value['is_parent']) && $value['is_parent'] == 1){
                   // echo '<b>'.$value['name'].'</b><br />';
                    //Menu cap 1
                    $menu[$key] = $value['name'];

                    if(!empty($value['child_item'])){
                        foreach ($value['child_item'] as $subid1) {
                            if(isset($data[$subid1])){
                                $subcat = $data[$subid1];
                                //echo '--'.$subcat['name'].'<br />';
                                //Menu cap 2
                                $menu_sub1[$key.'_'.$subid1] = $subcat['name'];

                                    foreach ($data as $ids=>$category) {
                                        if(isset($category['parent_id']) && $category['parent_id']==$subid1){
                                            //echo '-----------'.$category['name'].'<br />';
                                            //Menu cap 3
                                            $menu_sub2[$key.'_'.$subid1.'_'.$ids] = $category['name'];
                                        }
                                    }

                            }
                        }
                    }

               }
            }

            return array(
                         'menu'=>$menu,
                         'menu_sub1'=>$menu_sub1,
                         'menu_sub2'=>$menu_sub2
                         );
    }


    public function category_child($parent_id=''){  // tim cha cả lớn nhất
            if(isset($_POST['parent_id']))
                $parent_id = $_POST['parent_id'];

            if($parent_id=='')
                $arr_where = array('is_parent'=>1);
            else
                $arr_where = array('parent_id'=>$parent_id);

            $query = $this->opm->category->find($arr_where,array('name'))->limit(1000);
            $data = iterator_to_array($query);
            $menu = array();
            foreach($data as $key => $value){
                $menu[$key] = $value['name'];
                //echo $menu[$key] ;
            }
            //die;
            if(isset($_POST['parent_id'])){
                echo json_encode($menu);
                die;
            }else
                return $menu;
    }
    public function import_barcode()
    {
        $this->selectModel('Unit');
        $this->selectModel('Company');

        $query = $this->Company->select_one(array('system' => true));
        $arr_more = $query;
        if(isset($arr_more) && is_array($arr_more) && count($arr_more)>0){
            //lưu các giá trị của company system vào loation
            if(isset($arr_more['name']))
                $arr_tmp['company_name'] = $arr_more['name'];
            if(isset($arr_more['_id']))
                $arr_tmp['company_id'] = $arr_more['_id'];
            if(isset($arr_more['phone']))
                $arr_tmp['phone'] = $arr_more['phone'];
            if(isset($arr_more['fax']))
                $arr_tmp['fax'] = $arr_more['fax'];
            if(isset($arr_more['email']))
                $arr_tmp['email'] = $arr_more['email'];
            if(isset($arr_more['addresses'][0])){
                foreach($arr_more['addresses'][0] as $kk=>$vv){
                    $arr_tmp['shipping_address'][0]['shipping_'.$kk] = $vv;
                }
            }
            //contact
            if(isset($arr_more['contact_default_id'])){
                $this->selectModel('Contact');
                $query = $this->Contact->select_one(array('_id' => new MongoId($arr_more['contact_default_id'])));
                $contact = $query;
                if(isset($contact['first_name']))
                    $arr_tmp['contact_name'] = $contact['first_name'].' ';
                if(isset($contact['last_name']))
                    $arr_tmp['contact_name'] .= $contact['last_name'];
                if(isset($contact['_id']))
                    $arr_tmp['contact_id'] = $contact['_id'];
            }
        }
        $arr_tmp['product_name'] = $this->get_name('Product');
        $arr_tmp['product_id'] = new MongoId($this->get_id());

        $val = $_POST;$k = 0;
        for($m=0;$m<count($val['barcode']);$m++) {
          $arr_tmp['barcode_no'] = $val['barcode'][$m];
          $arr_tmp['quantity'] = $val['qty'][$m];
          $arr_tmp['barcode_type'] = $val['type'][$m];
          $ids = $this->Unit->add('Barcode_name', $val['barcode'][$m],$arr_tmp);
          $k = $k + (int)$val['qty'][$m];
        }
        echo  $k;die;
    }

    function update_category(){
        $this->selectModel('Product');
        $arr_product = $this->Product->select_all(array('arr_where' => array()));
        $arr_save = array();
        foreach($arr_product as $key => $value){
            if($value['category3'] != "." && $value['category3'] != ""){
                $arr_save['category_id'] = $value['category3'];
            }else if($value['category2'] != "." && $value['category2'] != ""){
                $arr_save['category_id'] = $value['category2'];
            }else if($value['category'] != ""){
                $arr_save['category_id'] = $value['category'];
            }else{
                $arr_save['category_id'] ="";
            }
            $arr_save['_id'] = $value['_id'];
            $this->Product->save($arr_save);
        }
        echo 'xong';die;
    }

    function update_quantity(){
        $this->selectModel('Unit');
        $arr_unit = $this->Unit->select_all(array('arr_where' => array()));
        $arr_save = array();
        foreach($arr_unit as $key => $value){
            $value['quantity'] = $value['qty'];
            unset($value['qty']);
            $this->Unit->save($value);
        }
        echo 'xong';die;
    }

    function build_unit(){
        $this->selectModel('Product');
        $this->selectModel('Unit');
        $this->selectModel('Company');
        $this->selectModel('Contact');
        $code = $this->Unit->get_auto_code('code');
        $arr_product = $this->Product->select_all(array('arr_where' => array('qty_in_stock' => array('$gt' => 0))));
        foreach($arr_product as $key => $value){
            $sum = 0;
            $sum = $this->Unit->count(array('product_id' => $value['_id']));
            if($sum == 0){
                $query = $this->Company->select_one(array('system' => true));
                $arr_more = $query;
                if(isset($arr_more) && is_array($arr_more) && count($arr_more)>0){
                    //lưu các giá trị của company system vào loation
                    if(isset($arr_more['name']))
                        $arr_tmp['company_name'] = $arr_more['name'];
                    if(isset($arr_more['_id']))
                        $arr_tmp['company_id'] = $arr_more['_id'];
                    if(isset($arr_more['phone']))
                        $arr_tmp['phone'] = $arr_more['phone'];
                    if(isset($arr_more['fax']))
                        $arr_tmp['fax'] = $arr_more['fax'];
                    if(isset($arr_more['email']))
                        $arr_tmp['email'] = $arr_more['email'];
                    if(isset($arr_more['addresses'][0])){
                        foreach($arr_more['addresses'][0] as $kk=>$vv){
                            $arr_tmp['shipping_address'][0]['shipping_'.$kk] = $vv;
                        }
                    }
                    //contact
                    if(isset($arr_more['contact_default_id'])){
                        $query = $this->Contact->select_one(array('_id' => new MongoId($arr_more['contact_default_id'])));
                        $contact = $query;
                        if(isset($contact['first_name']))
                            $arr_tmp['contact_name'] = $contact['first_name'].' ';
                        if(isset($contact['last_name']))
                            $arr_tmp['contact_name'] .= $contact['last_name'];
                        if(isset($contact['_id']))
                            $arr_tmp['contact_id'] = $contact['_id'];
                    }
                }
                $arr_tmp['product_name'] = $value['name'];
                $arr_tmp['product_id'] = $value['_id'];
                $arr_tmp['code'] = $code; $code++;
                $arr_tmp['product_code'] = $value['code'];
                $arr_tmp['barcode_no'] = $value['sku'];
                $arr_tmp['quantity'] = $value['qty_in_stock'];
                $ids = $this->Unit->save($arr_tmp);
            }
        }
        echo 'xong';die;
    }

    function  change_name_unit(){
        $this->selectModel('Unit');
        $arr_unit = $this->Unit->select_all(array('arr_where' => array()));
        foreach($arr_unit as $k => $v){
            if(isset($v['name_name'])){
                $v['product_name'] = $v['name_name'];
                unset($v['name_name']);
            }
            if(isset($v['name_id'])){
                $v['product_id'] = $v['name_id'];
                unset($v['name_id']);
            }
            $this->Unit->save($v);
        }
        echo 'xong';die;
    }

    function view_minilist(){
        if(!isset($_GET['print_pdf'])){
            $arr_where = $this->arr_search_where();
            $products = $this->opm->select_all(array(
                                               'arr_where'  =>  $arr_where,
                                               'arr_field'  =>  array('sku','name','company_name','sell_price','in_stock','von_luu','prime_cost','specification','oum','code'),
                                               'arr_order'  =>  array('code'=> 1),
                                               ));
            if($products->count()>0){
                $group = array();
                $html = '';
                $i = 0;
                $total_vonluu = 0;
                $arr_data = array();
                foreach($products as $key=>$product){
                    $von_luu = (isset($product['von_luu']) ? (int)$product['von_luu'] : '');
                    $qty_in_stock = (isset($product['in_stock']) ? (int)$product['in_stock'] : '');
                    $total_vonluu += $von_luu;
                    $html .= '<tr class="bg_1" style="border:1px solid">';
                    $html .= '<td>'.$product['sku'].'</td>';
                    $html .= '<td>'.$product['name'].'</td>';
                    $html .= '<td>'.(isset($product['company_name']) ? $product['company_name'] : '') .'</td>';
                    $html .= '<td class="center_text">'.(isset($product['oum']) ? $product['oum'] : '') .'</td>';
                    $html .= '<td class="right_text">'.(isset($product['specification']) ? $this->opm->format_currency($product['specification']) : '').'</td>';
                    $html .= '<td class="right_text">'.$qty_in_stock.'</td>';
                    $html .= '<td class="right_text">'.(isset($product['sell_price']) ? $this->opm->format_currency($product['sell_price']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($product['prime_cost']) ? $this->opm->format_currency($product['prime_cost']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($product['von_luu']) ? $this->opm->format_currency($product['von_luu']) : '').'</td>';
                    $html .= '</tr>';
                    $i++;
                }
                $html .='<tr class="last">
                            <td colspan="8" class=" bold_text right_none" style="text-align: center; border: 1px solid">Tổng tiền:</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($total_vonluu,0).'</td>
                         </tr>';
                $arr_data['title'] = array('Mã SP' => 'width: 4%; text-align: left','Tên sản phẩm' => 'width: 15%; text-align: left','Nhà c.cấp' =>'width: 8%;text-align: left;','Đv.Bán' => 'width: 5%','Q.Cách'=>'width: 5%;text-align: right;','S.Lượng'=>'width: 5%;text-align: right;','Giá bán'=>'width: 8%;text-align: right;','Giá gốc'=>'width: 8%;text-align: right;','Vốn lưu'=>'width: 8%;text-align: right;');
                $arr_data['content'] = $html;
                $arr_data['report_name'] = 'Danh sách sản phẩm';
                $arr_data['report_file_name']='PRO_'.md5(time());
                $arr_data['report_orientation'] = 'landscape';
                $arr_data['company_address'] =  '<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
                                            ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
                                            <span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
                                            ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
                                            Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
                                            Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>';
                $arr_data['logo_link'] = 'img/logo_anvy.jpg';
                Cache::write('products_minilist', $arr_data);
            }
        } else
            $arr_data = Cache::read('products_minilist');
        $this->render_pdf($arr_data);
    }

    function find_warehouse_product(){
        if(isset($_POST['submit'])){
             $arr_where = array();
            if(isset($_POST['date_equals']) && !empty($_POST['date_equals'])){
                $date_from = strtotime(date('Y-m-d',strtotime($_POST['date_equals'])));
                $date_to = $date_from + DAY - 1;
                $arr_where = array(
                                   'warehousing' => array('$elemMatch' => array(
                                                          'warehousing_date' => array(
                                                                '$gte' => new MongoDate($date_from),
                                                                '$lte' => new MongoDate($date_to),
                                                                )
                                                            )
                                                        )
                                   );
            } else {
                if(isset($_POST['date_from'])){
                    $arr_where['warehousing']['$elemMatch']['warehousing_date']['$gte']  = new MongoDate(strtotime(date('Y-m-d',strtotime($_POST['date_from']))));
                }
                if(isset($_POST['date_to'])){
                    $arr_where['warehousing']['$elemMatch']['warehousing_date']['$lte']  = new MongoDate(strtotime(date('Y-m-d',strtotime($_POST['date_to']))) + DAY - 1);
                }
            }
            $this->Session->write($this->name . '_where',array(
                                    'warehousing' => array(
                                                           'operator' => 'elemMatch',
                                                           'values'   => $arr_where['warehousing']['$elemMatch']
                                                           )
                                  ));
            echo URL.'/products/lists';
            die;
        }
    }

    function find_delivery_product(){
        $this->selectModel('Salesorder');
        if(isset($_POST['date_equals']) && !empty($_POST['date_equals'])){
            $date_from = strtotime(date('Y-m-d',strtotime($_POST['date_equals'])));
            $date_to = $date_from + DAY - 1;

        }

        $arr_salesorders = $this->Salesorder->collection->aggregate(
                        array(
                            '$match'=>array('status' => array('$in' => array('Chấp nhận','Hoàn thành','New')),
                            'deleted' => false,
                            'products' => array(
                                '$exists' => true,
                                '$nin' => array('',null)
                                ),
                            ),
                        ),
                        array(
                            '$unwind'=>'$products',
                        ),
                         array(
                            '$match'=>array('products.products_id' => new MongoId($this->get_id()))
                        ),
                        array(
                            '$project'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','salesorder_date'=>'$salesorder_date','products'=>'$products','status'=>'$status')
                        ),
                        array(
                            '$group'=>array(
                                          '_id'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','salesorder_date'=>'$salesorder_date','status'=>'$status'),
                                          'products'=>array('$push'=>'$products')
                                        )
                        )
                    );
    pr($arr_salesorders);die;
    }

    function import_excel_to_database(){
        App::import('Vendor', 'phpexcel/PHPExcel');
        $file_path = WWW_ROOT.'upload'.DS.'6.XLS';
        try {
            $inputFileType = PHPExcel_IOFactory::identify($file_path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file_path);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($file_path,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $arr_save = array();
        $this->selectModel('Company');
        $this->selectModel('Product');
        $date_now = date('Ymd');
        $arr_company = array();
        for ($row = 3; $row <= $highestRow; $row++){
            $arr_save['code'] = $sheet->getCell('A'.$row)->getValue();
            if(!is_numeric($arr_save['code'])) break;
            $arr_save['sku'] = $sheet->getCell('B'.$row)->getValue();
            $arr_save['name'] = $sheet->getCell('C'.$row)->getValue();
            $arr_save['company_name'] =  $sheet->getCell('D'.$row)->getValue();
            if(!isset($arr_company[$arr_save['company_name']])){
                $company = $this->Company->select_one(array('name' => $arr_save['company_name']),array('_id'));
                if(!isset($company['_id']))
                    $company['_id'] = '';
                $arr_save['company_id'] = $arr_company[$arr_save['company_name']] = $company['_id'];
            } else {
                $arr_save['company_id'] = $arr_company[$arr_save['company_name']];
            }
            $arr_save['oum'] =  $sheet->getCell('E'.$row)->getValue();
            $arr_save['specification'] =  $sheet->getCell('F'.$row)->getValue();
            $arr_save['prime_cost'] = (double)$sheet->getCell('H'.$row)->getValue();
            $arr_save['in_stock'] = $sheet->getCell('G'.$row)->getValue();
            $arr_save['sell_price'] = $this->checkTest($sheet->getCell('I'.$row)->getValue(), $sheet);
            $arr_save['von_luu'] = $this->checkTest($sheet->getCell('J'.$row)->getValue(), $sheet);
            //$arr_save['von_luu'] = $arr_save['prime_cost'] * $arr_save['in_stock'] * $arr_save['specification'];
            $arr_save['color_default'] = '';
            $arr_save['date_modified'] = new MongoDate();
            $arr_save['deleted'] = false;
            $arr_save['description'] = '';
            $arr_save['product_type'] = 'Product';
            $arr_save['approved'] = 0;
            $arr_save['qty_in_stock'] = 0;
            $arr_save['warehousing'][0]['deleted'] = false;
            $arr_save['warehousing'][0]['warehousing_date'] = new MongoDate(strtotime("2015-02-28T17:00:00.0Z"));
            $arr_save['warehousing'][0]['warehousing_by'] = 'Lang Lang';
            $arr_save['warehousing'][0]['warehousing_by_id'] = '540ec4b600806a127b7a4dc7';
            $arr_save['warehousing'][0]['warehousing_detail'] = '';
            $arr_save['warehousing'][0]['location_name'] = 'NiNi';
            $arr_save['warehousing'][0]['location_id'] = '541266e200806aa8277a4dbf';
            $arr_save['warehousing'][0]['location_type']= 'Sell';
            $arr_save['warehousing'][0]['qty_in_stock'] = $sheet->getCell('G'.$row)->getValue();
            $arr_save['short_name'] = $this->valid_input_string((string)$sheet->getCell('C'.$row)->getValue());
            $arr_save['approved'] = 1;
            $this->Product->save($arr_save);
        }
        echo 'Xong';die;
    }

    function import_product_to_database(){
        if (isset($_POST['task'])&&$_POST['task']=='upload') {
            if(!isset($_FILES['file'])){
                echo 'please choose file';
                die;
            }
            else{
                $filename = $_FILES["file"]["name"];
                $source = $_FILES["file"]["tmp_name"];
                $type = $_FILES["file"]["type"];
                /*if(($type=="application/zip" )|| ($type=="application/x-zip-compressed") || ($type== "multipart/x-zip")|| ($type=="application/x-compressed")||($type=="application/octet-stream")){*/
                     if ($_FILES["file"]["error"] > 0) {
                        echo "File Error: " . $_FILES["file"]["error"] . "<br/>";die;
                     }
                     else {
                            if (file_exists(TMP.'/'.$filename)){
                                echo 'File already exists';
                                die;
                            }
                            else{
                                if(move_uploaded_file($source,WWW_ROOT.'upload/'.$filename)){
                                    echo 'Uploading success';
                                }
                                else{
                                echo 'Error upload file';
                                    die;
                                }
                            }
                     }
               /* }
                else{
                    echo "Please choose a file .zip";
                    die;
                }*/
            }
        }
        App::import('Vendor', 'phpexcel/PHPExcel');
        $file_path = WWW_ROOT.'upload'.DS.$filename;
        try {
            $inputFileType = PHPExcel_IOFactory::identify($file_path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file_path);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($file_path,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $arr_save = array();
        $this->selectModel('Company');
        $this->selectModel('Product');
        $date_now = date('Ymd');
        $arr_company = array();
        $this->selectModel('Product');
        //$product = $this->Product->select_one(array(),array('code'),array('_id' => -1));
        for ($row = 3; $row <= $highestRow; $row++){
            //$arr_save['code'] = $sheet->getCell('A'.$row)->getValue();
            $arr_save['code'] = $this->Product->get_auto_code('code');
            if(!is_numeric($arr_save['code'])) break;
            $arr_save['sku'] = $sheet->getCell('B'.$row)->getValue();
            $arr_save['name'] = $sheet->getCell('C'.$row)->getValue();
            $arr_save['company_name'] =  $sheet->getCell('D'.$row)->getValue();
            if(!isset($arr_company[$arr_save['company_name']])){
                $company = $this->Company->select_one(array('name' => $arr_save['company_name']),array('_id'));
                if(!isset($company['_id']))
                    $company['_id'] = '';
                $arr_save['company_id'] = $arr_company[$arr_save['company_name']] = $company['_id'];
            } else {
                $arr_save['company_id'] = $arr_company[$arr_save['company_name']];
            }
            $arr_save['oum'] =  $sheet->getCell('E'.$row)->getValue();
            $arr_save['specification'] =  $sheet->getCell('F'.$row)->getValue();
            $arr_save['prime_cost'] = (double)$sheet->getCell('H'.$row)->getValue();
            $arr_save['in_stock'] = 0;
            $arr_save['sell_price'] = $this->checkTest($sheet->getCell('I'.$row)->getValue(), $sheet);
            //$arr_save['von_luu'] = $this->checkTest($sheet->getCell('J'.$row)->getValue(), $sheet);
            $arr_save['von_luu'] = 0;
            $arr_save['color_default'] = '';
            $arr_save['date_modified'] = new MongoDate();
            $arr_save['deleted'] = false;
            $arr_save['description'] = '';
            $arr_save['product_type'] = 'Product';
            $arr_save['approved'] = 0;
            $arr_save['qty_in_stock'] = 0;
            $arr_save['status'] = 'Mới';
            $arr_save['sell_price1'] = (double)$sheet->getCell('K'.$row)->getValue();
            $arr_save['sell_price2'] = (double)$sheet->getCell('L'.$row)->getValue();
            $arr_save['sell_price3'] = (double)$sheet->getCell('M'.$row)->getValue();
            $arr_save['short_name'] = $this->valid_input_string((string)$sheet->getCell('C'.$row)->getValue());
            $arr_save['approved'] = 1;
            $this->Product->save($arr_save);
        }
        echo 'Done';die;
    }

    function valid_input_string($string){
        $marTViet=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă",
            "ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề"
            ,"ế","ệ","ể","ễ",
            "ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
            ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
            ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
            ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ");

            $marKoDau=array("a","a","a","a","a","a","a","a","a","a","a"
            ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
            ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
            ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
            ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D");
        $string = strtolower(str_replace($marTViet,$marKoDau,$string));
        $string = str_replace(" ","-",$string);
        $string = preg_replace('/[^a-zA-Z0-9\-]/s', '', $string);
        while (strpos($string,"--")!==false){
            $string = str_replace("--","-",$string);
        }
        if(substr($string,strlen($string)-1,1)=='-'){
                    $string = str_replace("-","",$string);
        }
        return $string;
    }

    function checkTest($value, $sheet)
    {
        $value = str_replace('=', '', $value);
        foreach(['+','-','*','/'] as $operator){
            if( strpos($value, $operator) !== false ){
                $tmp = explode($operator, $value);
                foreach($tmp as $k => $v){
                    if( !is_numeric($v) ){
                        $tmp[$k] = $sheet->getCell($v)->getValue();
                    }
                }
                $value = implode($operator, $tmp);
            }
        }
        return eval('return '.$value.';');
    }

    function get_sum_vonluu(){
        $this->selectModel('Product');
        $cond = array();
        if($this->Session->check('Products_entry_search_cond')){
            $cond = $this->Session->read('Products_entry_search_cond');
        }
        $cond = array_merge($cond, $this->arr_search_where());
        $obj_products = $this->Product->select_all(array(
                                                   'arr_where' => $cond,
                                                   'arr_field' => array('_id','von_luu'),
                                                   ));
        $total_vonluu = 0;
        foreach($obj_products as $key => $value){
            $total_vonluu += $value['von_luu'];
        }
        echo json_encode(array('sum_vonluu' => $this->Product->format_currency($total_vonluu)));
        die;
    }

    function print_pdf_by_vonluu(){
        if(!isset($_GET['print_pdf'])){
            $cond = array();
            if($this->Session->check('Products_entry_search_cond')){
                $cond = $this->Session->read('Products_entry_search_cond');
            }
            $cond = array_merge($cond, $this->arr_search_where());
            $obj_products = $this->Product->select_all(array(
                                                       'arr_where' => $cond,
                                                       'arr_field'  =>  array('sku','name','company_name','sell_price','in_stock','von_luu','prime_cost','specification','oum','code'),
                                                       'arr_order' => array('code' => 1),
                                                       ));
            if($obj_products->count() > 0){
                $group = array();
                $html = '';
                $i = 0;
                $total_vonluu = 0;
                $arr_data = array();
                foreach($obj_products as $key=>$product){
                    $von_luu = (isset($product['von_luu']) ? (int)$product['von_luu'] : '');
                    $qty_in_stock = (isset($product['in_stock']) ? (int)$product['in_stock'] : '');
                    $total_vonluu += $von_luu;
                    $html .= '<tr class="bg_1" style="border:1px solid">';
                    $html .= '<td>'.$product['sku'].'</td>';
                    $html .= '<td>'.$product['name'].'</td>';
                    $html .= '<td>'.(isset($product['company_name']) ? $product['company_name'] : '') .'</td>';
                    $html .= '<td class="center_text">'.(isset($product['oum']) ? $product['oum'] : '') .'</td>';
                    $html .= '<td class="right_text">'.(isset($product['specification']) ? $this->opm->format_currency($product['specification']) : '').'</td>';
                    $html .= '<td class="right_text">'.$qty_in_stock.'</td>';
                    $html .= '<td class="right_text">'.(isset($product['sell_price']) ? $this->opm->format_currency($product['sell_price']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($product['prime_cost']) ? $this->opm->format_currency($product['prime_cost']) : '').'</td>';
                    $html .= '<td class="right_text">'.(isset($product['von_luu']) ? $this->opm->format_currency($product['von_luu']) : '').'</td>';
                    $html .= '</tr>';
                    $i++;
                }
                $html .='<tr class="last">
                            <td colspan="8" class=" bold_text right_none" style="text-align: center; border: 1px solid">Tổng tiền:</td>
                            <td class="right_text right_none" style="border: 1px solid;border-right:1px solid !important">'.number_format($total_vonluu,0).'</td>
                         </tr>';
                $arr_data['title'] = array(
                                           'Mã SP' => 'width: 4%; text-align: left',
                                           'Tên sản phẩm' => 'width: 20%; text-align: left',
                                           'Nhà c.cấp' =>'width: 15%;text-align: left;',
                                           'Đv.Bán' => 'width: 5%',
                                           'Q.Cách'=>'width: 5%;text-align: right;',
                                           'S.Lượng'=>'width: 5%;text-align: right;',
                                           'Giá bán'=>'width: 8%;text-align: right;',
                                           'Giá gốc'=>'width: 8%;text-align: right;',
                                           'Vốn lưu'=>'width: 8%;text-align: right;'
                                           );
                $arr_data['current_day'] = date('d/m/Y h:i:s a', time());
                $arr_data['content'] = $html;
                $arr_data['report_name'] = 'Danh sách sản phẩm';
                $arr_data['report_file_name']='Product'.md5(time());
                $arr_data['report_orientation'] = 'landscape';
                $arr_data['company_address'] =  '<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
                                            ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
                                            <span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
                                            ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
                                            Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
                                            Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>';
                $arr_data['logo_link'] = 'img/logo_anvy.jpg';
                $arr_data['custom_footer'] = 'no_footer';
                $arr_data['excel_url'] = URL.'/products/list_products_excel';
                Cache::write('view_quantity', $arr_data);
            }
        }else
            $arr_data = Cache::read('view_quantity');
        $this->render_pdf($arr_data);
    }

    public function list_products_excel(){
        $this->selectModel('Product');
        $cond = array();
        if($this->Session->check('Products_entry_search_cond')){
            $cond = $this->Session->read('Products_entry_search_cond');
        }
        $cond = array_merge($cond, $this->arr_search_where());
        $arr_data = $this->Product->select_all(array(
                                                   'arr_where' => $cond,
                                                   'arr_field'  =>  array('sku','name','company_name','sell_price','in_stock','von_luu','prime_cost','specification','oum','code'),
                                                   'arr_order' => array('code' => 1),
                                                   ));
        if(!$arr_data){
            echo 'No data';die;
        }
        App::import('Vendor','phpexcel/PHPExcel');
        $objPHPExcel = new PHPExcel();
        $i = 2;
        $objPHPExcel->getProperties()->setCreator("")
                                    ->setLastModifiedBy("")
                                    ->setTitle("Danh sách sản phẩm")
                                    ->setSubject("Danh sách sản phẩm")
                                    ->setDescription("Danh sách sản phẩm")
                                    ->setKeywords("Danh sách sản phẩm")
                                    ->setCategory("Danh sách");
        $worksheet = $objPHPExcel->getActiveSheet();
        $worksheet->setCellValue('A1','Mã SP')
                    ->setCellValue('B1','Tên sản phẩm')
                    ->setCellValue('C1','Nhà cung cấp')
                    ->setCellValue('D1','Đơn vị bán')
                    ->setCellValue('E1','Quy cách')
                    ->setCellValue('F1','Số lượng')
                    ->setCellValue('G1','Giá bán')
                    ->setCellValue('H1','Giá gốc')
                    ->setCellValue('I1','Vốn lưu');
        foreach($arr_data as $value){
            $worksheet->setCellValue('A'.$i,$value['sku'])
                        ->setCellValue('B'.$i,$value['name'])
                        ->setCellValue('C'.$i,$value['company_name'])
                        ->setCellValue('D'.$i,$value['oum'])
                        ->setCellValue('E'.$i,$value['specification'])
                        ->setCellValue('F'.$i,$value['in_stock'])
                        ->setCellValue('G'.$i,$value['sell_price'])
                        ->setCellValue('H'.$i,$value['prime_cost'])
                        ->setCellValue('I'.$i,$value['von_luu']);
            $i++;
        }
        $worksheet->getStyle('G2:I'.$i)->getNumberFormat()->setFormatCode("#,##0");
        for($k = 'A'; $k !== 'O'; $k++){
            $worksheet->getColumnDimension($k)
                            ->setAutoSize(true);
        }
        $worksheet->mergeCells("A$i:H$i")
                    ->setCellValue('I'.$i,"=SUM(I2:I".($i-1).")");
        $worksheet->setTitle('Kho San Pham');
        $objPHPExcel->setActiveSheetIndex(0);//->mergeCells("A$i:E$i");
        //$objWriter = PHPExcel_IOFactory::PHPExcel_Writer_Excel5($objPHPExcel);
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        $objWriter->save(APP.DS.'webroot'.DS.'upload'.DS.'kho_san_pham.xls');
        $this->redirect('/upload/kho_san_pham.xls');
        die;
    }

    public function slug_string($string){
            $string = trim($string);
            $marTViet=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ","Á"
            ,"è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ",
                "ì","í","ị","ỉ","ĩ",
                "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
                ,"ờ","ớ","ợ","ở","ỡ",
                "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
                "ỳ","ý","ỵ","ỷ","ỹ",
                "đ",
                "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
                ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
                "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
                "Ì","Í","Ị","Ỉ","Ĩ",
                "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
                ,"Ờ","Ớ","Ợ","Ở","Ỡ",
                "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
                "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
                "Đ");

                $marKoDau=array("a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","A"
                ,"e","e","e","e","e","e","e","e","e","e","e",
                "i","i","i","i","i",
                "o","o","o","o","o","o","o","o","o","o","o","o"
                ,"o","o","o","o","o",
                "u","u","u","u","u","u","u","u","u","u","u",
                "y","y","y","y","y",
                "d",
                "A","A","A","A","A","A","A","A","A","A","A","A"
                ,"A","A","A","A","A",
                "E","E","E","E","E","E","E","E","E","E","E",
                "I","I","I","I","I",
                "O","O","O","O","O","O","O","O","O","O","O","O"
                ,"O","O","O","O","O",
                "U","U","U","U","U","U","U","U","U","U","U",
                "Y","Y","Y","Y","Y",
                "D");
            $tohop=array(
                'á','à','ả','ã','ạ',
                'ấ','ầ','ẩ','ẫ','ậ',
                'ắ','ằ','ẳ','ẵ','ặ',

                'Á','À','Ả','Ã','Ạ',
                'Ấ','Ầ','Ẩ','Ẫ','Ậ',
                'Ắ','Ằ','Ẳ','Ẵ','Ặ',

                'é','è','ẻ','ẽ','ẹ',
                'ế','ề','ể','ễ','ệ',

                'É','È','Ẻ','Ẽ','Ẹ',
                'Ế','Ề','Ể','Ễ','Ệ',

                'ó','ò','ỏ','õ','ọ',
                'ố','ồ','ổ','ỗ','ộ',
                'ớ','ờ','ở','ỡ','ợ',

                "Ó",'Ò','Ỏ','Õ','Ọ',
                'Ố','Ồ','Ổ','Ỗ','Ộ',
                'Ớ','Ờ','Ở','Ỡ','Ợ',

                'í','ì','ỉ','ĩ','ị',

                'Í','Ì','Ỉ','Ĩ','Ị',

                'ú','ù','ủ','ũ','ụ',
                'ứ','ừ','ử','ữ','ự',

                'Ú','Ù','Ủ','Ũ','Ụ',
                'Ứ','Ừ','Ử','Ữ','Ự',

                "Đ",
                'đ'

                );

                $dungsan=array(
                'á','à','ả','ã','ạ',
                'ấ','ầ','ẩ','ẫ','ậ',
                'ắ','ằ','ẳ','ẵ','ặ',

                'Á','À','Ả','Ã','Ạ',
                'Ấ','Ầ','Ẩ','Ẫ','Ậ',
                'Ắ','Ằ','Ẳ','Ẵ','Ặ',

                'é','è','ẻ','ẽ','ẹ',
                'ế','ề','ể','ễ','ệ',

                'É,','È','Ẻ','Ẽ','Ẹ',
                'Ế','Ề','Ể','Ễ','Ệ',

                'ó','ò','ỏ','õ','ọ',
                'ố','ồ','ổ','ỗ','ộ',
                'ớ','ờ','ở','ỡ','ợ',

                'Ó','Ò','Ỏ','Õ','Ọ',
                'Ố','Ồ','Ổ','Ỗ','Ộ',
                'Ớ','Ờ','Ở','Ỡ','Ợ',

                'í','ì','ỉ','ĩ','ị',

                'Í','Ì','Ỉ','Ĩ','Ị',

                'ú','ù','ủ','ũ','ụ',
                'ứ','ừ','ử','ữ','ự',

                'Ú','Ù','Ủ','Ũ','Ụ',
                'Ứ','Ừ','Ử','Ữ','Ự',

                "Đ",
                'đ'
            );
            $string = str_replace($tohop,$dungsan,html_entity_decode($string));
            $string = strtolower(str_replace($marTViet,$marKoDau,$string));
        return $string;
    }

    public function search_list($arr_where = array()) {
        if( isset($_POST['search_from_list']) ) {
            unset($_POST['search_from_list']);
            $arr_where = array();
            foreach($_POST as $key => $value) {
                if($key == 'sku'){
                    $arr_where[$key]['operator'] = 'other';
                    $arr_where[$key]['values'] = array(
                                                    '$or' => array(
                                                                array('sku' => (int)$value),
                                                                array('sku' => (string)$value),
                                                                   )
                                                       );

                    continue;
                }
                 if($key == 'name_like'){
                    $value = $this->slug_string($value);
                    while(strpos($value,' ')!==false){
                        $value = str_replace(" ","-",$value);
                    }
                    $arr_where['short_name']['operator'] = 'LIKE';
                    $arr_where['short_name']['values'] =(string)$value;
                    continue;
                }
                if( $key == 'company_id' ) {
                    $value = new MongoId($value);
                }
                if( $key == 'in_stock' ) {
                    if($value == 'existed'){
                            $arr_where[$key]['operator'] = '>';
                            $arr_where[$key]['values'] = 0;
                            continue;
                    }else{
                            $value = (int)$value;
                    }

                }
                if( $key == 'specification' ) {
                    $value = (int)$value;
                }
                if($key == 'sell_price'){
                    $value = (int)$value;
                }
                if($key == 'prime_cost'){
                    $value = (int)$value;
                }
                if($key == 'status'){
                    $value = (string)$value;
                }
                $arr_where[$key]['operator'] = '=';
                $arr_where[$key]['values'] = $value;
            }
            $this->Session->write($this->name . '_where', $arr_where);
            $where_query = $this->arr_search_where();
            $amount = $this->opm->count($where_query);
            if ($amount > 1) {
                $arr_query = $this->opm->select_one($where_query, array('_id'), array('_id' => -1));
                if (isset($arr_query['_id'])) {
                    $arr_query['_id'] = (array) $arr_query['_id'];
                    $this->Session->write($this->name . 'ViewId', $arr_query['_id']['$id']);
                }
                echo 'lists';

            } else if ($amount == 1) {
                $arr_query = $this->opm->select_one($where_query, array('_id'));
                if (isset($arr_query['_id']))
                    echo 'entry/' . $arr_query['_id'];
                else
                    echo 'entry';
            }else {
                $this->Session->write($this->name . '_where', array());
                echo '0';
            }
            die;
        }
        pr($arr_where);die;
        parent::search_list($arr_where);
    }

    function build_database(){
        $this->selectModel('Product');
        $products = $this->Product->select_all(array('arr_where' => array(),'arr_field' => array('warehousing')));
        $arr_save = array();
        foreach($products as $key => $value){
            foreach($value['warehousing'] as $k => $v){
                 unset($value['warehousing'][$k]);
            }
            $arr_save = $value;
            $this->Product->save($arr_save);
        }
        echo 'xong';
        die;
    }

    function build_db_product()
    {
        $this->selectModel('Product');
        $product = $this->Product->select_all(array(
                                                'arr_where' => array('company_name'=>'Anh Thư')
                                              ));
        $arr_save = array();
        foreach($product as $key => $value){
            $arr_save = $value;
            $arr_save['company_id'] = new MongoId('5416b77500806a86024e1051');
            $arr_save['company_name'] = 'Chị Anh Thư';
            $this->Product->save($arr_save);
        }
        echo 'xong';
        die;
    }

    public function update_instock_product()
    {
	set_time_limit(0);
        $this->selectModel('Product');
        $products = $this->Product->select_all(array(
                                                'arr_where' => array('deleted'=> false)
                                              ));
        $date_created = "";
        $this->selectModel('Contact');
        foreach($products as $key => $product)
        {
            $date_created = $this->Contact->format_date($product['_id']->getTimestamp());
            $date_created = strtotime($date_created);
            $beindate = strtotime('01 April, 2015');
            if($date_created >= $beindate){
                $in_stock_prod = 0;
                $arr_po = $this->get_product_in_pur_order1($product['_id']);
                if(count($arr_po['product']))
                    foreach ($arr_po['product'] as $key => $value) {
                        $in_stock_prod+= isset($value['qty_in_stock'])?$value['qty_in_stock']:0;
                    }
                $arr_rpo = $this->get_product_in_return_pur_order1($product['_id']);

                if(count($arr_rpo['product']))
                    foreach ($arr_rpo['product'] as $key => $value) {
                        $in_stock_prod-= isset($value['qty_in_stock'])?$value['qty_in_stock']:0;
                    }
                $arr_so = $this->get_product_in_salesorder1($product['_id']);

                if(count($arr_so['product']))
                    foreach ($arr_so['product'] as $key => $value) {
                        $in_stock_prod-= isset($value['qty_in_salesorder'])?$value['qty_in_salesorder']:0;
                    }
                $arr_rso = $this->get_product_in_return_salesorder1($product['_id']);
                if(count($arr_rso['product']))
                    foreach ($arr_rso['product'] as $key => $value) {
                        $in_stock_prod+= isset($value['qty_in_salesorder'])?$value['qty_in_salesorder']:0;
                    }
                $product['in_stock'] = $in_stock_prod;
                $this->Product->save($product);
            }
        }
        echo 'done';
        die;
    }


    public function get_product_in_pur_order1($id){
        $arr_products = array('product' => array(),'total_in_salesorder' => 0);
        $this->selectModel('Purchaseorder');
        $arr_pur_orders = $this->Purchaseorder->collection->aggregate(
                        array(
                            '$match'=>array('purchase_orders_status' => array('$in' => array('Hoàn thành')),
                            'deleted' => false,
                            'products' => array(
                                '$exists' => true,
                                '$nin' => array('',null)
                                ),
                            ),
                        ),
                        array(
                            '$unwind'=>'$products',
                        ),
                         array(
                            '$match'=>array('products.products_id' => new MongoId($id),'products.deleted' => false)
                        ),
                        array(
                            '$project'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','purchord_date'=>'$purchord_date','products'=>'$products','purchase_orders_status'=>'$purchase_orders_status')
                        ),
                        array(
                            '$group'=>array(
                                          '_id'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','purchord_date'=>'$purchord_date','purchase_orders_status'=>'$purchase_orders_status'),
                                          'products'=>array('$push'=>'$products')
                                        )
                        )
                    );
        if(isset($arr_pur_orders['ok']) && !empty($arr_pur_orders['result'])){
            $arr_products['total_in_salesorder']  = 0;
            foreach($arr_pur_orders['result'] as $value){
                $qty_in_salesorder = 0;
                foreach($value['products'] as $product){
                    $qty_in_salesorder += isset($product['quantity']) ? $product['quantity'] : 0;
                }
                $arr_products['total_in_salesorder']  += $qty_in_salesorder;
                $arr_products['product'][] = array(
                        'warehousing_date' => date($value['_id']['purchord_date']->sec),
                        'code' => $value['_id']['code'],
                        'warehousing_by_id' => "123",
                        'warehousing_detail' => 'Tạo từ mua hàng #'. $value['_id']['code'],
                        'location_name' => $value['_id']['company_name'],
                        'qty_in_stock' => $qty_in_salesorder,
                    );
            }
        }
        return $arr_products;
    }

    public function get_product_in_return_pur_order1($id){
        $arr_products = array('product' => array(),'total_in_salesorder' => 0);
        $this->selectModel('Returnpurchaseorder');
        $arr_salesorders = $this->Returnpurchaseorder->collection->aggregate(
                        array(
                            '$match'=>array('purchase_orders_status' => array('$in' => array('Hoàn thành')),
                            'deleted' => false,
                            'products' => array(
                                '$exists' => true,
                                '$nin' => array('',null)
                                ),
                            ),
                        ),
                        array(
                            '$unwind'=>'$products',
                        ),
                         array(
                            '$match'=>array('products.products_id' => new MongoId($id),'products.deleted' => false)
                        ),
                        array(
                            '$project'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','purchord_date'=>'$purchord_date','products'=>'$products','purchase_orders_status'=>'$purchase_orders_status')
                        ),
                        array(
                            '$group'=>array(
                                          '_id'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','purchord_date'=>'$purchord_date','purchase_orders_status'=>'$purchase_orders_status'),
                                          'products'=>array('$push'=>'$products')
                                        )
                        )
                    );
        if(isset($arr_salesorders['ok']) && !empty($arr_salesorders['result'])){
            $arr_products['total_in_salesorder']  = 0;
            foreach($arr_salesorders['result'] as $value){
                $qty_in_salesorder = 0;
                foreach($value['products'] as $product){
                    $qty_in_salesorder += isset($product['quantity']) ? $product['quantity'] : 0;
                }
                $arr_products['total_in_salesorder']  += $qty_in_salesorder;
                $arr_products['product'][] = array(
                        'warehousing_date' => date($value['_id']['purchord_date']->sec),
                        'code' => $value['_id']['code'],
                        'warehousing_by_id' => "123",
                        'warehousing_detail' => 'Tạo từ trả hàng #'. $value['_id']['code'],
                        'location_name' => $value['_id']['company_name'],
                        'qty_in_stock' => $qty_in_salesorder,
                    );
            }
        }
        return $arr_products;
    }

   public function get_product_in_salesorder1($id){
        $arr_products = array('product' => array(),'total_in_salesorder' => 0);
        $this->selectModel('Salesorder');
        $arr_salesorders = $this->Salesorder->collection->aggregate(
                        array(
                            '$match'=>array('status' => array('$in' => array('Hoàn thành')),
                            'deleted' => false,
                            'products' => array(
                                '$exists' => true,
                                '$nin' => array('',null)
                                ),
                            ),
                        ),
                        array(
                            '$unwind'=>'$products',
                        ),
                         array(
                            '$match'=>array('products.products_id' => new MongoId($id),'products.deleted' => false)
                        ),
                        array(
                            '$project'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','salesorder_date'=>'$salesorder_date','products'=>'$products','status'=>'$status')
                        ),
                        array(
                            '$group'=>array(
                                          '_id'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','salesorder_date'=>'$salesorder_date','status'=>'$status'),
                                          'products'=>array('$push'=>'$products')
                                        )
                        )
                    );
        if(isset($arr_salesorders['ok']) && !empty($arr_salesorders['result'])){
            $arr_products['total_in_salesorder']  = 0;
            foreach($arr_salesorders['result'] as $value){
                $qty_in_salesorder = 0;
                foreach($value['products'] as $product){
                    $qty_in_salesorder += isset($product['quantity']) ? $product['quantity'] : 0;
                }
                $arr_products['total_in_salesorder']  += $qty_in_salesorder;
                $arr_products['product'][] = array(
                        'code' => $value['_id']['code'],
                        'company_name' => $value['_id']['company_name'],
                        'company_id' => $value['_id']['company_id'],
                        'salesorder_date' => date('d, M Y',$value['_id']['salesorder_date']->sec),
                        'salesorder_id' => $value['_id']['_id'],
                        'qty_in_salesorder' => $qty_in_salesorder,
                        'status' => $value['_id']['status'],
                         'status' => 'Tạo từ đơn hàng #'. $value['_id']['code']
                    );
            }
        }
        return $arr_products;
    }
//sai cho nay
    public function get_product_in_return_salesorder1($id){
        $arr_products = array('product' => array(),'total_in_salesorder' => 0);
        $this->selectModel('Returnsalesorder');
        $arr_salesorders = $this->Returnsalesorder->collection->aggregate(
                        array(
                            '$match'=>array('status' => array('$in' => array('Hoàn thành')),
                            'deleted' => false,
                            'products' => array(
                                '$exists' => true,
                                '$nin' => array('',null)
                                ),
                            ),
                        ),
                        array(
                            '$unwind'=>'$products',
                        ),
                         array(
                            '$match'=>array('products.products_id' => new MongoId($id),'products.deleted' => false)
                        ),
                        array(
                            '$project'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','salesorder_date'=>'$salesorder_date','products'=>'$products','status'=>'$status')
                        ),
                        array(
                            '$group'=>array(
                                          '_id'=>array('_id'=>'$_id','code'=>'$code','company_name'=>'$company_name','company_id'=>'$company_id','salesorder_date'=>'$salesorder_date','status'=>'$status'),
                                          'products'=>array('$push'=>'$products')
                                        )
                        )
                    );
        if(isset($arr_salesorders['ok']) && !empty($arr_salesorders['result'])){
            $arr_products['total_in_salesorder']  = 0;
            foreach($arr_salesorders['result'] as $value){
                $qty_in_salesorder = 0;
                foreach($value['products'] as $product){
                    $qty_in_salesorder += isset($product['quantity']) ? $product['quantity'] : 0;
                }
                $arr_products['total_in_salesorder']  += $qty_in_salesorder;
                $arr_products['product'][] = array(
                        'code' => $value['_id']['code'],
                        'company_name' => $value['_id']['company_name'],
                        'company_id' => $value['_id']['company_id'],
                        'salesorder_date' => date('d, M Y',$value['_id']['salesorder_date']->sec),
                        'salesorder_id' => $value['_id']['_id'],
                        'qty_in_salesorder' => $qty_in_salesorder,
                        //'status' => $value['_id']['status']
                        'status' => 'Tạo từ hàng trả #'. $value['_id']['code']
                    );
            }
        }
        return $arr_products;
    }

}