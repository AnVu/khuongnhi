<?php
App::uses('QuotationsController', 'Controller');
class QuotationsControllerCustom extends QuotationsController{
 	public function beforeFilter() {
        parent::beforeFilter();
    }


    public function view_pdf($getfile=false,$type='') {
        if(!$this->check_permission($this->name.'_@_entry_@_view'))
            $this->error_auth();
        $this->set('type',$type);
        $this->layout = 'pdf';
        $ids = $this->get_id();

        if ($ids != '') {
            $query = $this->opm->select_one(array('_id' => new MongoId($ids)));

            $arrtemp = $query;
            //sort product by parent
            if(isset($arrtemp['products']) && is_array($arrtemp['products']) ) {
                foreach($arrtemp['products'] as $keys=>$values){
                    if(isset($values['option_for']) && $values['option_for']!=''){
                        if(!isset($arrtemp['products'][$keys]['products_name']))
                            $arrtemp['products'][$keys]['products_name'] = '';
                        $arrtemp['products'][$keys]['products_name'] = '&bull; '.$arrtemp['products'][$keys]['products_name'];
                        $arrtemp['products'][$keys]['sku'] = '';
                        $arrtemp['products'][$keys]['sort_key'] = $values['option_for'].'-'.$keys;
                    }else
                        $arrtemp['products'][$keys]['sort_key'] = $keys.'-'.'0';
                }
                $arrtemp['products'] = $this->opm->aasort($arrtemp['products'],'sort_key');
            }

            //set header
            $this->set('logo_link', 'img/logo_anvy.jpg');
            if (empty($_SESSION['default_lang']))
                $_SESSION['default_lang'] = 'en';
            if($_SESSION['default_lang'] == 'vi')
                $this->set('company_address', 'Số 5 Đinh Tiên Hoàng, P3, Bình Thạnh<br />Tp Hồ Chí Minh<br />');
            else if($_SESSION['default_lang'] == 'en')
                $this->set('company_address', 'Unit 103, 3016 - 10th Ave NE<br />Calgary  AB  T2A  6A3<br />');

            //customer address
            $customer = '';
            if (isset($arrtemp['company_id']) && strlen($arrtemp['company_id']) == 24)
                $customer .= '<b>' . $this->get_name('Company', $arrtemp['company_id']) . '</b><br />';
            else if (isset($arrtemp['company_name']))
                $customer .= '<b>' . $arrtemp['company_name'] . '</b><br />';
            if (isset($arrtemp['contact_id']) && strlen($arrtemp['contact_id']) == 24)
                $customer .= $this->get_name('Contact', $arrtemp['contact_id']) . '<br />';
            else if (isset($arrtemp['contact_name']))
                $customer .= $arrtemp['contact_name'] . '<br />';

            //loop 2 address
            $arradd = array('invoice', 'shipping');
            foreach ($arradd as $vvs) {
                $kk = $vvs;
                $customer_address = '';
                if (isset($arrtemp[$kk . '_address']) && isset($arrtemp[$kk . '_address'][0]) && count($arrtemp[$kk . '_address']) > 0) {
                    $temp = $arrtemp[$kk . '_address'][0];
                    if (isset($temp[$kk . '_address_1']) && $temp[$kk . '_address_1'] != '')
                        $customer_address .= $temp[$kk . '_address_1'] . ', ';
                    if (isset($temp[$kk . '_address_2']) && $temp[$kk . '_address_2'] != '')
                        $customer_address .= $temp[$kk . '_address_2'] . ', ';
                    if (isset($temp[$kk . '_address_3']) && $temp[$kk . '_address_3'] != '')
                        $customer_address .= $temp[$kk . '_address_3'] . '.';
                    else
                        $customer_address .= '<br />';
                    if (isset($temp[$kk . '_town_city']) && $temp[$kk . '_town_city'] != '')
                        $customer_address .= $temp[$kk . '_town_city'].', ';

                    if (isset($temp[$kk . '_province_state']))
                        $customer_address .= ' ' . $temp[$kk . '_province_state'] . ', ';
                    else if (isset($temp[$kk . '_province_state_id']) && isset($temp[$kk . '_country_id'])) {
                        $keytemp = $temp[$kk . '_province_state_id'];
                        $provkey = $this->province($temp[$kk . '_country_id']);
                        if (isset($provkey[$temp]))
                            $customer_address .= ' ' . $provkey[$temp] . '<br/>';
                    }


                    if (isset($temp[$kk . '_zip_postcode']) && $temp[$kk . '_zip_postcode'] != '')
                        $customer_address .= $temp[$kk . '_zip_postcode'].'<br/>';
                    if (isset($temp[$kk . '_country']))
                        $customer_address .= ' ' . $temp[$kk . '_country'] . '<br />';
                    else
                        $customer_address .= '<br />';
                    $arr_address[$kk] = $customer_address;
                }
            }

            if (isset($arrtemp['heading']) && $arrtemp['heading'] != '')
                $heading = $arrtemp['heading'];
            else
                $heading = '';

            if (!isset($arr_address['invoice']))
                $arr_address['invoice'] = '';
            $this->set('customer_address', $customer . $arr_address['invoice']);

            if (!isset($arr_address['shipping']))
                $arr_address['shipping'] = '';
            if($arr_address['shipping']=='')
                 $arr_address['shipping'] = $arr_address['invoice'];
            $ship_to = '';
            if(isset($arrtemp['shipping_address'][0]['shipping_contact_name']))
                $ship_to = $arrtemp['shipping_address'][0]['shipping_contact_name'];
            if($ship_to == '')
                $ship_to = $this->get_name('Contact', $arrtemp['contact_id']);

            $this->set('shipping_address',$ship_to .'<br />'. $arr_address['shipping']);
            $this->set('ref_no', $arrtemp['code']);
            $this->set('quote_date', $this->opm->format_date($arrtemp['quotation_date']));

            /** Nội dung bảng giá */
            $date_now = date('Ymd');
            $numkey = explode("-",$arrtemp['code']);
            $filename = 'Q-'.$numkey[count($numkey)-1];
            $this->set('filename', $filename);
            $this->set('heading', $heading);

            //comments note
            $other_comment = '';
            if(isset($arrtemp['other_comment']))
                $other_comment = str_replace("\n","<br />",'<br />'.$arrtemp['other_comment']);
            $this->set('other_comment', $other_comment);

            $html_cont = '';
            $line_entry_data = $this->line_entry_data();
            $minimum = $this->get_minimum_order();
            if($arrtemp['sum_sub_total']<$minimum){
                $more_sub_total = $minimum - (float)$arrtemp['sum_sub_total'];
                if(isset($line_entry_data['products']) && !empty($line_entry_data['products'])){
                    $last_insert = end($line_entry_data['products']);
                    foreach($last_insert as $key=>$value){
                        if($key=='deleted' || $key == 'taxper') continue;
                        $last_insert[$key] = '';
                    }
                }
                $last_insert['_id'] = -1;
                $last_insert['products_id'] = '';
                $last_insert['products_name'] = 'Minimum Order Adjustment';
                $last_insert['quantity'] = '1';
                if(!isset($last_insert['taxper']))
                        $last_insert['taxper']= 0;
                $last_insert['custom_unit_price'] = $last_insert['sub_total'] = $more_sub_total;
                $last_insert['tax'] = $last_insert['sub_total']*$last_insert['taxper']/100;
                $last_insert['amount'] = $last_insert['sub_total']+$last_insert['tax'];
                array_push($line_entry_data['products'], $last_insert);
                $arrtemp['sum_sub_total']+=$more_sub_total;
                $arrtemp['sum_tax']+=$last_insert['tax'];
                $arrtemp['sum_amount']+=$last_insert['amount'];
            }
            if (isset($line_entry_data['products']) && !empty($line_entry_data['products'])) {
                $line = 0;
                $colum = 7;
                if(isset($arrtemp['options']) && !empty($arrtemp['options']) )
                    $options = $arrtemp['options'];
                if($type=='exclude_qty_price')
                    $colum = 5;
                else if($type=='category_heading_only')
                    $colum = 3;
                if($type=='' || $type=='exclude_qty_price'){
                    foreach ($line_entry_data['products'] as $keys => $values) {
                        if (isset($values['deleted']) && !$values['deleted']) {
                            if ($line % 2 == 0)
                                $bgs = '#fdfcfa';
                            else
                                $bgs = '#eeeeee';
                            //code
                            $html_cont .= '<tr style="background-color:' . $bgs . ';"><td class="first">';
                            if(isset($values['option_for'])&&is_numeric($values['option_for'])){
                                $values['sku'] = '';
                                $values['products_name'] = '&nbsp;&nbsp;&nbsp;•'.$values['products_name'];
                            }
                            if (isset($values['sku']))
                                $html_cont .= '  ' . $values['sku'];
                            else
                                $html_cont .= '  #' . $keys;
                            //desription
                            $html_cont .= '</td><td>';
                            if (isset($values['products_name']))
                                $html_cont .= str_replace("\n", "<br />", $values['products_name']);
                            else
                                $html_cont .= 'Empty';
                            //clear các dòng phía sau nếu là same product parent
                            if(isset($values['same_parent']) && $values['same_parent']==1){
                                $html_cont .= '</td><td></td><td></td><td></td><td></td><td class="end"></td></tr>';
                                 $line++;
                                continue;
                            }
                            //width
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['sizew']) && $values['sizew'] != '' && isset($values['sizew_unit']) && $values['sizew_unit'] != '')
                                $html_cont .= $values['sizew'] . ' (' . $values['sizew_unit'] . ')';
                            else if (isset($values['sizew']) && $values['sizew'] != '')
                                $html_cont .= $values['sizew'] . ' (in.)';
                            else
                                $html_cont .= '';
                            //height
                            if($type=='exclude_qty_price')
                                $html_cont .= '</td><td align="right">';
                            else
                                $html_cont .= '</td><td align="right">';
                            if (isset($values['sizeh']) && $values['sizeh'] != '' && isset($values['sizeh_unit']) && $values['sizeh_unit'] != '')
                                $html_cont .= $values['sizeh'] . ' (' . $values['sizeh_unit'] . ')';
                            else if (isset($values['sizeh']) && $values['sizeh'] != '')
                                $html_cont .= $values['sizeh'] . ' (in.)';
                            else
                                $html_cont .= '';
                            if($type==''){
                            //Unit price
                                $html_cont .= '</td><td align="right">';
                                if (!isset($values['custom_unit_price']))
                                    $values['custom_unit_price'] = (isset($values['unit_price']) ? $values['unit_price'] : 0);
                                $html_cont .= $this->opm->format_currency($values['custom_unit_price']);
                                //Qty
                                $html_cont .= '</td><td align="right">';
                                if (isset($values['quantity']))
                                    $html_cont .= $values['quantity'];
                                else
                                    $html_cont .= '';
                                //line total
                                $html_cont .= '</td><td align="right" class="end">';
                                if (isset($values['sub_total']))
                                    $html_cont .= $this->opm->format_currency($values['sub_total']);
                                else
                                    $html_cont .= '';
                                $html_cont .= '</td></tr>';
                            }
                            else if($type=='exclude_qty_price')
                                $html_cont .= '</td><td class="end"></td></tr>';
                            $line++;
                        }//end if deleted
                    }//end for
                }else if($type=='category'||$type=='category_heading_only'){
                    $this->selectModel('Product');
                    $cate = $this->Setting->select_option_vl(array('setting_value' => 'product_category'));
                    $group = array();
                    $arr_products = array();
                    foreach($line_entry_data['products'] as $keys => $values){
                        if (!isset($values['custom_unit_price']))
                            $values['custom_unit_price'] = (isset($values['unit_price']) ? $values['unit_price'] : 0);
                        if(isset($values['deleted'])&&$values['deleted']) continue;
                        $product['category'] = '';
                        if(isset($values['products_id']) && is_object($values['products_id']))
                            $product = $this->Product->select_one(array('_id'=> new MongoId($values['products_id'])),array('category'));
                        $product_cate = 'No category entered';
                        if(isset($cate[$product['category']]))
                            $product_cate = $cate[$product['category']];
                        if(isset($values['option_for']) && isset($arr_products[$values['option_for']])){
                            $product_cate = $arr_products[$values['option_for']]['category'];
                            $product['category'] = $arr_products[$values['option_for']]['category_id'];
                        }
                        else{
                            $arr_products[$values['_id']]['category'] = $product_cate;
                            $arr_products[$values['_id']]['category_id'] = (isset($product['category']) ? $product['category'] : '');
                        }
                        $group[$product_cate]['category_id'] = (isset($product['category']) ? $product['category'] : '');
                        $group[$product_cate]['category'] = (isset($product_cate) ? $product_cate : '' );
                        $group[$product_cate]['group'][$keys]['same_parent'] = (isset($values['same_parent']) ? $values['same_parent'] : 0);
                        if($type=='category'){
                            $group[$product_cate]['group'][$keys]['code'] = (isset($values['sku']) ? $values['sku'] : '');
                            $group[$product_cate]['group'][$keys]['products_name'] = (isset($values['products_name']) ? $values['products_name'] : '');
                            $group[$product_cate]['group'][$keys]['quantity'] = (isset($values['quantity']) ? $values['quantity'] : 0);
                            $group[$product_cate]['group'][$keys]['sub_total'] = (isset($values['sub_total'])&&$values['sub_total']!='' ? $values['sub_total'] : '0.00');
                            $group[$product_cate]['group'][$keys]['custom_unit_price'] = $values['custom_unit_price'];
                            $group[$product_cate]['group'][$keys]['sizew'] = (isset($values['sizew']) && $values['sizew'] != '' && isset($values['sizew_unit']) && $values['sizew_unit'] != '' ? $values['sizew'] . ' (' . $values['sizew_unit'] . ')' : (isset($values['sizew']) && $values['sizew'] != '') ?  $values['sizew'] . ' (in.)' : '');
                            $group[$product_cate]['group'][$keys]['sizeh'] = (isset($values['sizeh']) && $values['sizeh'] != '' && isset($values['sizeh_unit']) && $values['sizeh_unit'] != '' ? $values['sizeh'] . ' (' . $values['sizeh_unit'] . ')' : (isset($values['sizeh']) && $values['sizeh'] != '') ?  $values['sizeh'] . ' (in.)' : '');

                        }
                        else if($type=='category_heading_only'){
                            if(!isset($group[$product_cate]['sub_total']))
                                $group[$product_cate]['sub_total'] = $values['sub_total'];
                            else{
                                if($group[$product_cate]['group'][$keys]['same_parent'] == 0)
                                    $group[$product_cate]['sub_total'] += (isset($values['sub_total'])&&$values['sub_total']!='' ? $values['sub_total'] : 0);
                            }
                        }
                    }
                    if(!empty($group)){
                        ksort($group);
                        foreach($group as $value){
                            if($type=='category'){
                                $total_sub_total = 0;
                                $html_cont .= '<tr style="background-color:#757575;color: #fff;font-weight: bold">';
                                $html_cont .= '<td class="first" colspan="2">'.$value['category_id'].'</td>';
                                $html_cont .= '<td class="end" colspan="5">'.$value['category'].'</td>';
                                $html_cont .= '</tr>';
                                foreach($value['group'] as $val){
                                    if($val['same_parent'] == 0)
                                        $total_sub_total += (float)$val['sub_total'];
                                    $bgs = ($line % 2 == 0 ? '#fdfcfa' : '#eeeeee');
                                    $html_cont .= '<tr style="background-color:' . $bgs . ';">';
                                    $html_cont .= '<td class="first">'.$val['code'].'</td>';
                                    $html_cont .= '<td>'.$val['products_name'].'</td>';
                                    $line++;
                                    if($val['same_parent'] != 0){
                                        $html_cont .= '<td colspan="5"></td></tr>';
                                        continue;
                                    }
                                    $html_cont .= '<td align="right">'.$val['sizew'].'</td>';
                                    $html_cont .= '<td align="right">'.$val['sizeh'].'</td>';
                                    $html_cont .= '<td align="right">'.number_format($val['custom_unit_price'],2).'</td>';
                                    $html_cont .= '<td align="right">'.$val['quantity'].'</td>';
                                    $html_cont .= '<td class="end" align="right">'.number_format($val['sub_total'],2).'</td>';
                                    $html_cont .= '</tr>';
                                }
                                $bgs = ($line % 2 == 0 ? '#fdfcfa' : '#eeeeee');
                                $html_cont .= '<tr style="background-color:' . $bgs . ';">
                                                    <td class="first" colspan="6" align="right" style="border-bottom: 1px dash #000;"><strong>Total by category:</strong></td>
                                                    <td align="right" style="border-bottom: 1px dash #000;">'.number_format($total_sub_total,2).'</td>

                                                </tr>';
                            }
                            else if($type=='category_heading_only'){
                                $bgs = ($line % 2 == 0 ? '#fdfcfa' : '#eeeeee');
                                $html_cont .= '<tr style="background-color:' . $bgs . ';">';
                                $html_cont .= '<td>'.$value['category_id'].'</td>';
                                $html_cont .= '<td>'.$value['category'].'</td>';
                                $html_cont .= '<td align="right">'.number_format($value['sub_total'],2).'</td>';
                                $html_cont .= '</tr>';
                            }
                            $line++;

                        }
                    }

                }else if($type=='group'){
                    $arr_price = array();
                    foreach($line_entry_data['products'] as $product){
                        if(!isset($product['option_for'])) continue;
                        if(!isset($product['same_parent']) || $product['same_parent'] == 1) continue;
                        if (!isset($values['custom_unit_price']))
                            $product['custom_unit_price'] = (isset($product['unit_price']) ? $product['unit_price'] : 0);
                        if(!isset($arr_price[$product['option_for']]))
                            $arr_price[$product['option_for']]['unit_price'] = $arr_price[$product['option_for']]['sub_total'] = 0;
                        $arr_price[$product['option_for']]['unit_price'] += $product['custom_unit_price'];
                        $arr_price[$product['option_for']]['sub_total'] += $product['sub_total'];
                    }
                    foreach ($line_entry_data['products'] as $keys => $values) {
                        if (  isset($values['deleted']) && !$values['deleted']) {

                            if ($line % 2 == 0)
                                $bgs = '#fdfcfa';
                            else
                                $bgs = '#eeeeee';
                            //code
                            $html_cont .= '<tr style="background-color:' . $bgs . ';"><td class="first">';
                            if(isset($values['option_for'])&&is_numeric($values['option_for'])){
                                $values['sku'] = '';
                                $values['products_name'] = '&nbsp;&nbsp;&nbsp;•'.$values['products_name'];
                            }
                            if (isset($values['sku']))
                                $html_cont .= '  ' . $values['sku'];
                            else
                                $html_cont .= '  #' . $keys;
                            //desription
                            $html_cont .= '</td><td>';
                            if (isset($values['products_name']))
                                $html_cont .= str_replace("\n", "<br />", $values['products_name']);
                            else
                                $html_cont .= 'Empty';
                            //clear các dòng phía sau nếu là same product parent
                            if(isset($values['same_parent']) && $values['same_parent']==1){
                                $html_cont .= '</td><td></td><td></td><td></td><td></td><td class="end"></td></tr>';
                                $line++;
                                continue;
                            }
                            //width
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['sizew']) && $values['sizew'] != '' && isset($values['sizew_unit']) && $values['sizew_unit'] != '')
                                $html_cont .= $values['sizew'] . ' (' . $values['sizew_unit'] . ')';
                            else if (isset($values['sizew']) && $values['sizew'] != '')
                                $html_cont .= $values['sizew'] . ' (in.)';
                            else
                                $html_cont .= '';
                            //height
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['sizeh']) && $values['sizeh'] != '' && isset($values['sizeh_unit']) && $values['sizeh_unit'] != '')
                                $html_cont .= $values['sizeh'] . ' (' . $values['sizeh_unit'] . ')';
                            else if (isset($values['sizeh']) && $values['sizeh'] != '')
                                $html_cont .= $values['sizeh'] . ' (in.)';
                            else
                                $html_cont .= '';
                            if($values['_id']!=-1&&isset($values['same_parent']) && $values['same_parent']==0){
                                $html_cont .= '</td><td></td><td align="right">'.$values['quantity'].'</td><td class="end"></td></tr>';
                                $line++;
                                continue;
                            }
                            if(isset($arr_price[$values['_id']])){
                                $values['custom_unit_price'] += $arr_price[$values['_id']]['unit_price'];
                                $values['sub_total'] = str_replace(',', '', (string)$values['sub_total']);
                                $values['sub_total'] += $arr_price[$values['_id']]['sub_total'];
                            }
                            //Unit price
                            $html_cont .= '</td><td align="right">';
                            if(isset($arr_price[$values['_id']]))
                                $html_cont .= $this->opm->format_currency($values['sub_total'] / $values['quantity']);
                            else
                                $html_cont .= $this->opm->format_currency($values['custom_unit_price']);
                            //Qty
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['quantity']))
                                $html_cont .= $values['quantity'];
                            else
                                $html_cont .= '';
                            //line total
                            $html_cont .= '</td><td align="right" class="end">';
                            if (isset($values['sub_total']))
                                $html_cont .= $this->opm->format_currency($values['sub_total']);
                            else
                                $html_cont .= '';
                            $html_cont .= '</td></tr>';
                            $line++;
                        }//end if deleted
                    }
                }
                if ($line % 2 == 0) {
                    $bgs = '#fdfcfa';
                    $bgs2 = '#eeeeee';
                } else {
                    $bgs = '#eeeeee';
                    $bgs2 = '#fdfcfa';
                }

                $sub_total = $total = $taxtotal = 0.00;
                if (isset($arrtemp['sum_sub_total']))
                    $sub_total = $arrtemp['sum_sub_total'];
                if (isset($arrtemp['sum_tax']))
                    $taxtotal = $arrtemp['sum_tax'];
                if (isset($arrtemp['sum_amount']))
                    $total = $arrtemp['sum_amount'];
                //Sub Total
                $html_cont .= '<tr style="background-color:' . $bgs . ';">
                                    <td colspan="' . ($colum - 1) . '" align="right" style="font-weight:bold;border-top:2px solid #aaa;" class="first">Tổng trước thuế:</td>
                                    <td align="right" style="border-top:2px solid #aaa;" class="end">' . $this->opm->format_currency($sub_total) . '</td>
                               </tr>';
                //GST
                $html_cont .= '<tr style="background-color:' . $bgs2 . ';">
                                    <td colspan="' . ($colum - 1) . '" align="right" style="font-weight:bold;" class="first">VAT:</td>
                                    <td align="right" class="end">' . $this->opm->format_currency($taxtotal) . '</td>
                               </tr>';
                //Total
                $html_cont .= '<tr style="background-color:' . $bgs . ';">
                                    <td colspan="' . ($colum - 1) . '" align="right" style="font-weight:bold;" class="first bottom">Tổng cộng:</td>
                                    <td align="right" class="end bottom">' . $this->opm->format_currency($total) . '</td>
                               </tr>';
            }//end if


            $this->set('html_cont', $html_cont);
            if (isset($arrtemp['our_rep_id'])) {
                $this->set('user_name', ' ' . $this->get_name('Contact', $arrtemp['our_rep_id']));
            } else
                $this->set('user_name', ' ' . $this->opm->user_name());
            $this->set('qr_image','https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl='.URL.'/quotations/entry/'.$this->get_id().'&choe=UTF-8');
            //end set content
            //set footer
            if($_SESSION['default_lang']=='vi'){
                $this->render('vi_view_pdf');
            }
            if($getfile){
                return $filename.'.pdf';
            }
            $this->redirect('/upload/' . $filename . '.pdf');
        }
        die;
    }
    public function check_lock() {
        if ($this->get_id() != '') {
            $arr_tmp = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
            if (isset($arr_tmp['quotation_status'])&&$arr_tmp['quotation_status'] != 'In progress'&&$arr_tmp['quotation_status'] != 'Đang tiến hành')
                return true;
        } else
            return false;
    }


}
