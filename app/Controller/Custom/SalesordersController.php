<?php
App::uses('SalesordersController', 'Controller');

class SalesordersControllerCustom extends SalesordersController {
	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function rebuild_setting($arr_setting=array()){
		parent::rebuild_setting();
		$arr_settings = $this->opm->arr_settings;
		if(!$this->check_permission('shippings_@_entry_@_view'))
			unset($arr_settings['relationship']['ship_invoice']['block']['shipping']);
		if(!$this->check_permission('salesinvoices_@_entry_@_view'))
			unset($arr_settings['relationship']['ship_invoice']['block']['invoice']);
		$this->opm->arr_settings = $arr_settings;
		$params = isset($this->params->params['pass'][0]) ? $this->params->params['pass'][0] : null;
		$valid = false;
		if($this->params->params['action'] == 'entry' || $valid = in_array($params,array('line_entry','text_entry'))){
			$query = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())),array('job_id','status'));
			if(isset($query['status']) ){
				if(!in_array($query['status'], array('New','Mới')  ) ){
					if($valid){
						$this->opm->set_lock_option('line_entry', 'products');
						$this->opm->set_lock_option('text_entry', 'products');
						if($query['status'] == 'Trả hàng'){
							$this->opm->arr_settings['relationship']['line_entry']['block']['products']['field']['option']['edit'] = 1;
						}
					} else {
						$this->opm->set_lock(array('status'), 'out');
						$this->set('address_lock', '1');
					}
				}
			}else if(isset($query['job_id'])&&is_object($query['job_id'])){
				$this->selectModel('Job');
				$job = $this->Job->select_one(array('_id'=> new MongoId($query['job_id'])),array('status'));
				if(isset($job['status']) && in_array($query['status'], array('Completed','Hoàn thành')) ){
					if($valid){
						$this->opm->set_lock_option('line_entry', 'products');
						$this->opm->set_lock_option('text_entry', 'products');
					} else {
						$this->opm->set_lock(array('status'), 'out');
						$this->set('address_lock', '1');
					}
				}
			}
		}
	}

	//Export pdf
	public function view_pdf123($getfile=false,$type='') {
		$this->layout = 'pdf';
		$info_data = (object) array();
		$ids = $this->get_id();

		if ($ids != '') {
			$this->selectModel('Salesorder');
			$query = $this->Salesorder->select_one(array('_id' => new MongoId($ids)));
			$arrtemp = $query;
			$this->selectModel('Company');
			$company_id = $query['company_id'];
			$arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)),array('thanh_toan','no_cu'));
			$this->selectModel('Salesorder');
			$arr_salesorder = $this->Salesorder->select_all(array(
															 'arr_where' => array('company_id' => new MongoId($company_id), 'status' => 'Hoàn thành'),
															 'arr_field' => array('code','sum_amount','salesorder_date','products','status','sales_order_type'),
															 'arr_order' => array('salesorder_date' => 1)
															));
			$this->selectModel('Returnsalesorder');
			$arr_return_salesorder = $this->Returnsalesorder->select_all(array(
															 'arr_where' => array('company_id' => new MongoId($company_id), 'status' => 'Hoàn thành'),
															 'arr_field' => array('code','sum_amount','salesorder_date','products','status','sales_order_type', 'return_id'),
															 'arr_order' => array('salesorder_date' => 1)
															));
			$this->selectModel('Productreturnerror');
			$arr_product_return_error = $this->Productreturnerror->select_all(array(
															 'arr_where' => array('company_id' => new MongoId($company_id), 'purchase_orders_status' => 'Hoàn thành'),
															 'arr_field' => array('code','sum_amount','products','salesorder_date'),
															 'arr_order' => array('salesorder_date' => 1)
															));
			$con_lai = 0;
			$thanh_tien = 0;
			$arr_return = array();
			$arr_order = array();
			foreach($arr_salesorder as $key => $value){
				$time = $value['salesorder_date']->sec;
				while( isset($arr_return[$time]) )
					$time++;
				$value['type'] = 'Salesorder';
				$arr_return[$time] = $value;
			}
			$con_lai_return = 0;
			foreach($arr_return_salesorder as $k => $v){
				$time = $v['salesorder_date']->sec;
				while( isset($arr_return[$time]) )
					$time++;
				$v['type'] = 'Returnsalesorder';
				$arr_return[$time] = $v;
			}
			foreach($arr_product_return_error as $k_product_return => $v_product_return){
				$time = $v_product_return['salesorder_date']->sec;
				while( isset($arr_return[$time]) )
					$time++;
				$v_product_return['type'] = 'Productreturnerror';
				$arr_return[$time] = $v_product_return;
			}
			ksort($arr_return);
			foreach($arr_return as $key_return => $v_return){
				$key_return = (string)$v_return['_id'];
				if($v_return['type'] == 'Salesorder'){
					$arr_order[$key_return] = $v_return;
					$arr_order[$key_return]['no_cu'] = $con_lai;
					$arr_order[$key_return]['thanh_tien'] = $arr_order[$key_return]['sum_amount'] + $arr_order[$key_return]['no_cu'];
					if(isset($arr_company['thanh_toan']) ) {
						//foreach($arr_company['thanh_toan'] as $k => $thanh_toan){
						$array = array_filter($arr_company['thanh_toan'], function($array) use ($key_return){
							return $array['id'] == $key_return;
						});
						$thanh_toan = end($array);
						$arr_order[$key_return]['type_paid'] = isset($thanh_toan['type_paid']) ? $thanh_toan['type_paid'] : '';
						//if(!isset($thanh_toan['id']) || $thanh_toan['id'] != $v_return['_id']) continue;
						$arr_order[$key_return]['thanh_toan'] = isset($thanh_toan['amount']) ? (double)$thanh_toan['amount'] : 0;
						//}
					}
					else
						$arr_order[$key_return]['thanh_toan'] = 0;
					$thanh_tien = isset($arr_order[$key_return]['thanh_toan']) ? $arr_order[$key_return]['thanh_toan'] : 0 ;
					$arr_order[$key_return]['con_lai'] = $arr_order[$key_return]['thanh_tien'];
					$con_lai = $arr_order[$key_return]['thanh_tien'] - $thanh_tien;
				}else if($v_return['type'] == 'Returnsalesorder'){
					$arr_order[$key_return] = $v_return;
					$arr_order[$key_return]['no_cu'] = $con_lai;
					$sum_amount = isset($arr_order[$key_return]['sum_amount']) ? $arr_order[$key_return]['sum_amount'] : 0;
					$arr_order[$key_return]['thanh_tien'] = $arr_order[$key_return]['no_cu'] -  $sum_amount;
					if(isset($arr_company['thanh_toan'])){
						//foreach($arr_company['thanh_toan'] as $kk => $v_thanh_toan){
							//if(!isset($v_thanh_toan['id']) || $v_thanh_toan['id'] != $v_return['_id']) continue;
						$array = array_filter($arr_company['thanh_toan'], function($array) use ($key_return){
							return $array['id'] == $key_return;
						});
						$v_thanh_toan = end($array);
						$arr_order[$key_return]['thanh_toan'] = isset($v_thanh_toan['amount']) ? (double)$v_thanh_toan['amount'] : 0;
						$arr_order[$key_return]['type_paid'] = isset($v_thanh_toan['type_paid']) ? $v_thanh_toan['type_paid'] : '';

						//}
					}
					else
						$arr_order[$key_return]['thanh_toan'] = 0;
					$thanh_tien_return = isset($arr_order[$key_return]['thanh_toan']) ? $arr_order[$key_return]['thanh_toan'] : 0;
					$arr_order[$key_return]['con_lai'] = $arr_order[$key_return]['thanh_tien'];
					$con_lai = $arr_order[$key_return]['thanh_tien'] - $thanh_tien_return;
					$arr_order[$key_return]['return_id'] = 1;
				}else if($v_return['type'] == 'Productreturnerror'){
					$arr_order[$key_return] = $v_return;
					$arr_order[$key_return]['no_cu'] = $con_lai;
					$sum_amount = isset($arr_order[$key_return]['sum_amount']) ? $arr_order[$key_return]['sum_amount'] : 0;
					$arr_order[$key_return]['thanh_tien'] = $arr_order[$key_return]['no_cu'] - $sum_amount;
					if(isset($arr_company['thanh_toan'])){
						//foreach($arr_company['thanh_toan'] as $kkk => $v_thanh_toan_p){
							//if(!isset($v_thanh_toan_p['id']) || $v_thanh_toan_p['id'] != $v_return['_id']) continue;
						$array = array_filter($arr_company['thanh_toan'], function($array) use ($key_return){
								return $array['id'] == $key_return;
						});
						$v_thanh_toan_p = end($array);
						$arr_order[$key_return]['thanh_toan'] = isset($v_thanh_toan_p['amount']) ? (double)$v_thanh_toan_p['amount'] : 0;
						$arr_order[$key_return]['type_paid'] = isset($v_thanh_toan_p['type_paid']) ? $v_thanh_toan_p['type_paid'] : '';

						//}
					}else
						$arr_order[$key_return]['thanh_toan'] = 0;
					$thanh_tien_return_p = isset($arr_order[$key_return]['thanh_toan']) ? $arr_order[$key_return]['thanh_toan'] : 0;
					$arr_order[$key_return]['con_lai'] = $arr_order[$key_return]['thanh_tien'];
					$con_lai  = $arr_order[$key_return]['thanh_tien'] - $thanh_tien_return_p;
				}
			}
			$no_cu = 0;
			$sum_amount = 0;
			$con_lai = 0;
			foreach($arr_order as $key => $value){
				if($value['type'] == "Salesorder" && (string)$value['_id'] == $ids){
					$no_cu = $value['no_cu'];
					$sum_amount = $value['sum_amount'];
					$con_lai = $value['con_lai'];
				}
			}
			//set header
			$this->set('logo_link', 'img/logo_anvy.jpg');
			$this->set('company_address', 	'<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
											ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
											<span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
											ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
											Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
											Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>');

			//customer address
			//loop 2 address
			$arradd = array('invoice', 'shipping');
			foreach ($arradd as $vvs) {
				$kk = $vvs;
				$customer_address = '';
				if (isset($arrtemp[$kk . '_address']) && isset($arrtemp[$kk . '_address'][0]) && count($arrtemp[$kk . '_address']) > 0) {
					$temp = $arrtemp[$kk . '_address'][0];
					if (isset($temp[$kk . '_address_1']) && $temp[$kk . '_address_1'] != '')
						$customer_address .= $temp[$kk . '_address_1'] . ', ';
					if (isset($temp[$kk . '_address_2']) && $temp[$kk . '_address_2'] != '')
						$customer_address .= $temp[$kk . '_address_2'] . ', ';
					if (isset($temp[$kk . '_address_3']) && $temp[$kk . '_address_3'] != '')
						$customer_address .= $temp[$kk . '_address_3'] ;
					else
						$customer_address .= '';
					if (isset($temp[$kk . '_town_city']) && $temp[$kk . '_town_city'] != '')
						$customer_address .= $temp[$kk . '_town_city'].', ';

					if (isset($temp[$kk . '_province_state']))
						$customer_address .= ' ' . $temp[$kk . '_province_state'];
					else if (isset($temp[$kk . '_province_state_id']) && isset($temp[$kk . '_country_id'])) {
						$keytemp = $temp[$kk . '_province_state_id'];
						$provkey = $this->province($temp[$kk . '_country_id']);
						if (isset($provkey[$temp]))
							$customer_address .= ' ' . $provkey[$temp] . '';
					}
					$arr_address[$kk] = $customer_address;
				}
			}

			if (isset($arrtemp['heading']) && $arrtemp['heading'] != '')
				$heading = $arrtemp['heading'];
			else
				$heading = '';
			if (!isset($arr_address['invoice']))
				$arr_address['invoice'] = '';
			//$this->set('customer_address', $customer . $arr_address['invoice']);
			if (!isset($arr_address['shipping']))
				$arr_address['shipping'] = '';
			if(isset($arrtemp['shipping_address'][0]['shipping_contact_name']))
				$this->set('ship_to',$arrtemp['shipping_address'][0]['shipping_contact_name']);
			$this->set('shipping_address', $arr_address['shipping']);
			$this->set('invoice_address', $arr_address['invoice']);
			$this->set('ref_no', $arrtemp['code']);
			$phone = isset($arrtemp['phone'])?$arrtemp['phone']:'';
			$this->set('phone',$phone);
			$info_data->contact_name = $arrtemp['company_name'];
			$info_data->no = $arrtemp['code'];
			$info_data->job_no = $arrtemp['job_number'];
			$info_data->date = $arrtemp['salesorder_date']->sec;
			$info_data->po_no = $arrtemp['customer_po_no'];
			$info_data->ac_no = '';
			$info_data->terms = $arrtemp['payment_terms'];
			$info_data->current_day = date('H:i d-m-y');
			$info_data->required_date = $this->opm->format_date($arrtemp['payment_due_date']);

			$this->set('info_data', $info_data);
			/*             * Nội dung bảng giá */
			$date_now = date('Ymd');
			$numkey = explode("-",$info_data->no);
			$filename = 'SO-'.$numkey[count($numkey)-1].time();
			$other_comment = '';
			if(isset($arrtemp['other_comment']))
				$other_comment = str_replace("\n","<br />",'<br />'.$arrtemp['other_comment']);
			$this->set('other_comment',$other_comment);
			$this->set('filename', $filename);
			$this->set('heading', $heading);
			$html_cont = '';
			$line_entry_data = $this->line_entry_data();
			$minimum = $this->get_minimum_order();
			if($arrtemp['sum_sub_total']<$minimum){
				$more_sub_total = $minimum - (float)$arrtemp['sum_sub_total'];
				if(isset($line_entry_data['products']) && !empty($line_entry_data['products'])){
					$last_insert = end($line_entry_data['products']);
					foreach($last_insert as $key=>$value){
						if($key=='deleted' || $key == 'taxper') continue;
						$last_insert[$key] = '';
					}
				}
				$last_insert['_id'] = -1;
				$last_insert['products_name'] = 'Minimum Order Adjustment';
				$last_insert['quantity'] = '1';
				if(!isset($last_insert['taxper']))
						$last_insert['taxper']= 0;
				$last_insert['custom_unit_price'] = $last_insert['sub_total'] = $more_sub_total;
				$last_insert['tax'] = $last_insert['sub_total']*$last_insert['taxper']/100;
				$last_insert['amount'] = $last_insert['sub_total']+$last_insert['tax'];
				array_push($line_entry_data['products'], $last_insert);
				$arrtemp['sum_sub_total']+=$more_sub_total;
				$arrtemp['sum_tax']+=$last_insert['tax'];
				$arrtemp['sum_amount']+=$last_insert['amount'];
			}
			if (isset($line_entry_data['products']) && is_array($line_entry_data['products']) && count($line_entry_data['products']) > 0) {
				$line = 0;
				$colum = 7;
				$options = array();
				if(isset($arrtemp['options']) && !empty($arrtemp['options']) )
					$options = $arrtemp['options'];
				$arr_price = array();
				foreach($line_entry_data['products'] as $product){
					if(!isset($product['option_for'])) continue;
					if(!isset($product['same_parent']) || $product['same_parent'] == 1) continue;
					if (!isset($values['custom_unit_price']))
						$product['custom_unit_price'] = (isset($product['unit_price']) ? $product['unit_price'] : 0);
					if(!isset($arr_price[$product['option_for']]))
						$arr_price[$product['option_for']]['unit_price'] = $arr_price[$product['option_for']]['sub_total'] = 0;
					$arr_price[$product['option_for']]['unit_price'] += $product['custom_unit_price'];
					$arr_price[$product['option_for']]['sub_total'] += $product['sub_total'];
				}
				foreach ($line_entry_data['products'] as $values) {

					$html_cont .= '<tr style="background-color: #FFFFFF; font-size:12px ">';
					$html_cont .= '<td>'.(isset($values['sku']) ? $values['sku'] : '') .'</td>';
					$html_cont .= '<td>'.(isset($values['products_name']) ? $values['products_name'] : '') .'</td>';
					$html_cont .= '<td style="text-align: center;">'.(isset($values['oum']) ? $values['oum'] : '') .'</td>';
					$html_cont .= '<td style="text-align: center;">'.(isset($values['specification']) ? $values['specification'] : '') .'</td>';
					$html_cont .= '<td style="text-align: right;">'.(isset($values['quantity']) ? $values['quantity'] : '') .'</td>';
					$html_cont .= '<td style="text-align: right;">'.(isset($values['sell_price']) ? $this->opm->format_currency($values['sell_price']) : '') .'</td>';
					$html_cont .= '<td style="text-align: right;">'.(isset($values['amount']) ? $this->opm->format_currency($values['amount']) : '') .'</td>';
					$html_cont .= '</tr>';
					$line++;
				}//end for

				if ($line % 2 == 0) {
					$bgs = '#fdfcfa';
					$bgs2 = '#eeeeee';
				} else {
					$bgs = '#eeeeee';
					$bgs2 = '#fdfcfa';
				}


				$sub_total = $total = $taxtotal = 0.00;
				if (isset($arrtemp['sum_sub_total']))
					$sub_total = $arrtemp['sum_sub_total'];
				if (isset($arrtemp['sum_tax']))
					$taxtotal = $arrtemp['sum_tax'];
				if (isset($arrtemp['sum_amount']))
					$total = $arrtemp['sum_amount'];
				if($_SESSION['default_lang']=='vi'){
					$sub_total_label = 'TỔNG CỘNG';
					$hst_gst = 'Thuế';
					$total_label = 'TỔNG CỘNG';
				 } else {
					$sub_total_label = 'Sub total';
					$hst_gst = 'HST/GST';
					$total_label = 'Total';
				 }
				//Sub Total
				$html_cont .= '<tr style="background-color: #eeeeee">
									<td colspan="' . ($colum - 1) . '" align="center" style="font-weight:bold;border-top:2px solid #aaa;" class="first">'.$sub_total_label.':</td>
									<td align="right" style="border-top:2px solid #aaa;" class="end">' . $this->opm->format_currency($sub_total) . '</td>
							   </tr>';
			}//end if

			$this->set('html_cont', $html_cont);

			if($no_cu == 0){
				   echo $sum_amount;
				}else{
					echo $con_lai;
			}
			$last_table = '<table id="last_table">
								<tr >
									<td style="text-align:center; width: 55%; border: 1px solid #FFFFFF"></td>
									<td style="text-align:center; width: 15%;"><b>NỢ CŨ</b></td>
									<td style="text-align:center;width: 15%"><b>TOA MỚI</b></td>
									<td style="text-align:center;width: 15%"><b>TỔNG CỘNG</b></td>
								</tr>
								<tr>
									<td style="border: 1px solid #FFFFFF"> </td>
									<td style="text-align:center" >'.number_format($no_cu).' </td>
									<td style="text-align:center">'.number_format($sum_amount) .'</td>
									<td style="text-align:center">'.($no_cu ? number_format($con_lai) : number_format($sum_amount)).' </td>
								</tr>
							</table>';
			$last_table_final =  '<table>
								<tr style="font-size:15px" >
									<td style="text-align:center; width: 25%;">Người nhận hàng<br /> (Ký, họ tên)</td>
									<td style="text-align:center; width: 25%">Người thu tiền <br />(Ký, họ tên)</td>
									<td style="text-align:center;width: 25%">Người lập<br />(Ký, họ tên)</td>
									<td style="text-align:center;width: 25%">Trưởng đơn vị<br />(Ký, họ tên)</td>
								</tr>
								<tr>
									<td ></td>
									<td> </td>
									<td> </td>
									<td> </td>
								</tr>
							</table>';
			$this->set('last_table_final',$last_table_final);
			$this->set('last_table',$last_table);
			$this->set('type',$type);
			if (isset($arrtemp['our_csr'])) {
				$this->set('user_name', ' ' . $arrtemp['our_csr']);
			} else
				$this->set('user_name', ' ' . $this->opm->user_name());
			$this->set('qr_image','https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl='.URL.'/salesorders/entry/'.$this->get_id().'&choe=UTF-8');
			//end set content
			//set footer
			$this->render('view_pdf');
			if($getfile)
				return $filename.'.pdf';
			$this->redirect('/upload/' . $filename . '.pdf');
		}
	}

	public function view_pdf($getfile=false, $ids = '') {
		$this->layout = 'pdf';
		if($ids == '')
			$ids = $this->get_id();
		else
			$this->module_id = $ids;
		if ($ids != '') {
			$this->selectModel('Salesorder');
			$query = $this->opm->select_one(array('_id' => new MongoId($ids)));
			$print = false;
			$print = true;
			if( $print ) {
				if (!isset($_GET['print_pdf'])) {
					$filename = 'SO-' . $query['code'] . (empty($type) ? '-detailed' : ($type == 'group' ? '' : '-' . $type));
					if ($this->print_pdf(array(
						'report_file_name' => $filename,
						'report_url' => URL . '/salesorders/view_pdf/' . $getfile . '/' . $ids,
						'custom_footer' => 'order'

					))) {
						if ($getfile) {
							return $filename . '.pdf';
						}

						$this->redirect(URL . '/upload/' . $filename . '.pdf');
					}
					else {
						if ($getfile) {
							return false;
						}
						echo 'Please contact IT for this issue.';
						die;
					}
				}
			}
			$arrtemp = $query;
			$this->selectModel('Company');
			$company_id = $query['company_id'];
			$arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)),array('thanh_toan','no_cu'));
			$this->selectModel('Salesorder');
			$arr_salesorder = $this->Salesorder->select_all(array(
															 'arr_where' => array('company_id' => new MongoId($company_id), 'status' => 'Hoàn thành'),
															 'arr_field' => array('code','sum_amount','salesorder_date','products','status','sales_order_type'),
															 'arr_order' => array('salesorder_date' => 1, '_id' => 1)
															));
			$this->selectModel('Returnsalesorder');
			$arr_return_salesorder = $this->Returnsalesorder->select_all(array(
															 'arr_where' => array('company_id' => new MongoId($company_id), 'status' => 'Hoàn thành'),
															 'arr_field' => array('code','sum_amount','salesorder_date','products','status','sales_order_type', 'return_id'),
															 'arr_order' => array('salesorder_date' => 1)
															));
			$this->selectModel('Productreturnerror');
			$arr_product_return_error = $this->Productreturnerror->select_all(array(
															 'arr_where' => array('company_id' => new MongoId($company_id), 'purchase_orders_status' => 'Hoàn thành'),
															 'arr_field' => array('code','sum_amount','products','salesorder_date'),
															 'arr_order' => array('salesorder_date' => 1)
															));
			$this->selectModel('GSaleorder');
			$arr_product_gsalesorder = $this->GSaleorder->select_all(array(
														  'arr_where' => array('company_id' => new MongoID($company_id),'status' => 'Hoàn thành'),
														  'arr_field' => array('code','sum_amount','salesorder_date','status','sales_order_type'),
														  'arr_order' => array('salesorder_date' => 1)
														  ));
			$con_lai = 0;
			$thanh_tien = 0;
			$arr_return = array();
			$arr_order = array();
			foreach($arr_salesorder as $key => $value){
				$time = $value['salesorder_date']->sec;
				while( isset($arr_return[$time]) )
					$time++;
				$value['type'] = 'Salesorder';
				$arr_return[$time] = $value;
			}
			$con_lai_return = 0;
			foreach($arr_return_salesorder as $k => $v){
				$time = $v['salesorder_date']->sec;
				while( isset($arr_return[$time]) )
					$time++;
				$v['type'] = 'Returnsalesorder';
				$arr_return[$time] = $v;
			}
			foreach($arr_product_return_error as $k_product_return => $v_product_return){
				$time = $v_product_return['salesorder_date']->sec;
				while( isset($arr_return[$time]) )
					$time++;
				$v_product_return['type'] = 'Productreturnerror';
				$arr_return[$time] = $v_product_return;
			}
			foreach($arr_product_gsalesorder as $kk => $vv){
				$time = $vv['salesorder_date']->sec;
				while( isset($arr_return[$time]) )
					$time++;
				$vv['type'] = 'Salesorder';
				$arr_return[$time] = $vv;
			}
			ksort($arr_return);
			foreach($arr_return as $key_return => $v_return){
				$key_return = (string)$v_return['_id'];
				if($v_return['type'] == 'Salesorder'){
					$arr_order[$key_return] = $v_return;
					$arr_order[$key_return]['no_cu'] = $con_lai;
					$arr_order[$key_return]['thanh_tien'] = $arr_order[$key_return]['sum_amount'] + $arr_order[$key_return]['no_cu'];
					if(isset($arr_company['thanh_toan']) ) {
						//foreach($arr_company['thanh_toan'] as $k => $thanh_toan){
						$array = array_filter($arr_company['thanh_toan'], function($array) use ($key_return){
							return $array['id'] == $key_return;
						});
						$thanh_toan = end($array);
						$arr_order[$key_return]['type_paid'] = isset($thanh_toan['type_paid']) ? $thanh_toan['type_paid'] : '';
						//if(!isset($thanh_toan['id']) || $thanh_toan['id'] != $v_return['_id']) continue;
						$arr_order[$key_return]['thanh_toan'] = isset($thanh_toan['amount']) ? (double)$thanh_toan['amount'] : 0;
						//}
					}
					else
						$arr_order[$key_return]['thanh_toan'] = 0;
					$thanh_tien = isset($arr_order[$key_return]['thanh_toan']) ? $arr_order[$key_return]['thanh_toan'] : 0 ;
					$arr_order[$key_return]['con_lai'] = $arr_order[$key_return]['thanh_tien'];
					$con_lai = $arr_order[$key_return]['thanh_tien'] - $thanh_tien;
				}else if($v_return['type'] == 'Returnsalesorder'){
					$arr_order[$key_return] = $v_return;
					$arr_order[$key_return]['no_cu'] = $con_lai;
					$sum_amount = isset($arr_order[$key_return]['sum_amount']) ? $arr_order[$key_return]['sum_amount'] : 0;
					$arr_order[$key_return]['thanh_tien'] = $arr_order[$key_return]['no_cu'] -  $sum_amount;
					if(isset($arr_company['thanh_toan'])){
						//foreach($arr_company['thanh_toan'] as $kk => $v_thanh_toan){
							//if(!isset($v_thanh_toan['id']) || $v_thanh_toan['id'] != $v_return['_id']) continue;
						$array = array_filter($arr_company['thanh_toan'], function($array) use ($key_return){
							return $array['id'] == $key_return;
						});
						$v_thanh_toan = end($array);
						$arr_order[$key_return]['thanh_toan'] = isset($v_thanh_toan['amount']) ? (double)$v_thanh_toan['amount'] : 0;
						$arr_order[$key_return]['type_paid'] = isset($v_thanh_toan['type_paid']) ? $v_thanh_toan['type_paid'] : '';

						//}
					}
					else
						$arr_order[$key_return]['thanh_toan'] = 0;
					$thanh_tien_return = isset($arr_order[$key_return]['thanh_toan']) ? $arr_order[$key_return]['thanh_toan'] : 0;
					$arr_order[$key_return]['con_lai'] = $arr_order[$key_return]['thanh_tien'];
					$con_lai = $arr_order[$key_return]['thanh_tien'] - $thanh_tien_return;
					$arr_order[$key_return]['return_id'] = 1;
				}else if($v_return['type'] == 'Productreturnerror'){
					$arr_order[$key_return] = $v_return;
					$arr_order[$key_return]['no_cu'] = $con_lai;
					$sum_amount = isset($arr_order[$key_return]['sum_amount']) ? $arr_order[$key_return]['sum_amount'] : 0;
					$arr_order[$key_return]['thanh_tien'] = $arr_order[$key_return]['no_cu'] - $sum_amount;
					if(isset($arr_company['thanh_toan'])){
						//foreach($arr_company['thanh_toan'] as $kkk => $v_thanh_toan_p){
							//if(!isset($v_thanh_toan_p['id']) || $v_thanh_toan_p['id'] != $v_return['_id']) continue;
						$array = array_filter($arr_company['thanh_toan'], function($array) use ($key_return){
								return $array['id'] == $key_return;
						});
						$v_thanh_toan_p = end($array);
						$arr_order[$key_return]['thanh_toan'] = isset($v_thanh_toan_p['amount']) ? (double)$v_thanh_toan_p['amount'] : 0;
						$arr_order[$key_return]['type_paid'] = isset($v_thanh_toan_p['type_paid']) ? $v_thanh_toan_p['type_paid'] : '';

						//}
					}else
						$arr_order[$key_return]['thanh_toan'] = 0;
					$thanh_tien_return_p = isset($arr_order[$key_return]['thanh_toan']) ? $arr_order[$key_return]['thanh_toan'] : 0;
					$arr_order[$key_return]['con_lai'] = $arr_order[$key_return]['thanh_tien'];
					$con_lai  = $arr_order[$key_return]['thanh_tien'] - $thanh_tien_return_p;
				}
			}
			$no_cu = 0;
			$sum_amount = 0;
			$con_lai = 0;
			foreach($arr_order as $key => $value){
				if($value['type'] == "Salesorder" && (string)$value['_id'] == $ids){
					$no_cu = $value['no_cu'];
					$sum_amount = $value['sum_amount'];
					$con_lai = $value['con_lai'];
				}
			}

			if(!isset($query['currency']))
				$query['currency'] = 'cad';
			$arrtemp = $query;

			// customer address

			$customer = '';
			if (isset($arrtemp['company_id']) && strlen($arrtemp['company_id']) == 24) $customer.= '<b>' . $this->get_name('Company', $arrtemp['company_id']) . '</b><br />';
			else if (isset($arrtemp['company_name'])) $customer.= '<b>' . $arrtemp['company_name'] . '</b>';
			if (isset($arrtemp['contact_id']) && strlen($arrtemp['contact_id']) == 24) $customer.=  '<p>'.$this->get_name('Contact', $arrtemp['contact_id']).'</p>' ;
			else if (isset($arrtemp['contact_name'])) $customer.= '<p>'.$arrtemp['contact_name'].'</p>';

			// loop 2 address

			$arradd = array('invoice', 'shipping');
			foreach($arradd as $vvs) {
				$kk = $vvs;
				$customer_address = '';
				if (isset($arrtemp[$kk . '_address']) && isset($arrtemp[$kk . '_address'][0]) && count($arrtemp[$kk . '_address']) > 0) {
					$temp = $arrtemp[$kk . '_address'][0];
					if (isset($temp[$kk . '_address_1']) && $temp[$kk . '_address_1'] != '') $customer_address.= $temp[$kk . '_address_1'] . '<br />';
					if (isset($temp[$kk . '_address_2']) && $temp[$kk . '_address_2'] != '') $customer_address.= $temp[$kk . '_address_2'] . '<br />';
					if (isset($temp[$kk . '_address_3']) && $temp[$kk . '_address_3'] != '') $customer_address.= $temp[$kk . '_address_3'] . '<br />';
					if (isset($temp[$kk . '_town_city']) && $temp[$kk . '_town_city'] != '') $customer_address.= $temp[$kk . '_town_city'] . ', ';
					if (isset($temp[$kk . '_province_state'])) $customer_address.= ' ' . $temp[$kk . '_province_state'];
					else if (isset($temp[$kk . '_province_state_id']) && isset($temp[$kk . '_country_id'])) {
						$keytemp = $temp[$kk . '_province_state_id'];
						$provkey = $this->province($temp[$kk . '_country_id']);
						if (isset($provkey[$temp])) $customer_address.= ' ' . $provkey[$temp] . '<br/>';
					}

					if (isset($temp[$kk . '_zip_postcode']) && $temp[$kk . '_zip_postcode'] != '') $customer_address.= $temp[$kk . '_zip_postcode'] . '<br/>';
					$arr_address[$kk] = $customer_address;
				}
			}
			if (!isset($arr_address['invoice'])) $arr_address['invoice'] = '';
			$arrData['company'] = '<div style="font-size: 22px; margin-bottom: 10px;"><b>Tên khách hàng:</b>'.  $arrtemp['company_name'].' - <b>ĐC: </b>'.$arr_address['invoice'].'  - <b>ĐT:</b>'. $arrtemp['phone'] .'</div>';
			$arrData['date'] = $arrtemp['salesorder_date']->sec;
			$arrData['heading'] = 'Đơn Đặt Hàng '.$arrtemp['code'];
			if (!empty($customer)) $customer = "<p>{$customer}</p>";
			$arrData['customer_address'] = $customer . $arr_address['invoice'];
			if (!isset($arr_address['shipping'])) $arr_address['shipping'] = '';
			if ($arr_address['shipping'] == '') $arr_address['shipping'] = $arr_address['invoice'];
			$ship_to = '';
			if (isset($arrtemp['shipping_address'][0]['shipping_contact_name'])) $ship_to = $arrtemp['shipping_address'][0]['shipping_contact_name'];
			if ($ship_to == '') $ship_to = $this->get_name('Contact', $arrtemp['contact_id']);
			if (!empty($ship_to)) $ship_to = "<p>{$ship_to}</p>";
			$arrData['pdf_name'] = 'Sales invoice';
			$arrData['shipping_address'] = $ship_to . $arr_address['shipping'];

			if (isset($arrtemp['other_comment'])) $arrData['note'] = nl2br($arrtemp['other_comment']);
			$html_cont = '';
			$line_entry_data = $this->line_entry_data();

			if (isset($line_entry_data['products']) && is_array($line_entry_data['products']) && count($line_entry_data['products']) > 0) {
				$arrData['logo'] = '/img/logo_anvy.jpg';
				$arrData['company_address'] = '<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
											ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
											<span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
											ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
											Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
											Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>';
				$arrData['content'] = '';
				$arrData['title'] = array(
					'Mã' =>'font-size: 150% !important',
					'Tên sản phẩm'=>'font-size: 150% !important',
					'ĐVT' => 'text-align: right; font-size: 150% !important',
					'Quy cách' => 'text-align: right;font-size: 150% !important',
					'Số lượng' => 'text-align: center;font-size: 150% !important',
					'Đơn giá' => 'text-align: right;font-size: 150% !important',
					'Thành tiền' => 'text-align: right;font-size: 150% !important'
				);
				$options = array();
				$this->selectModel('Setting');
				$arr_currency = $this->Setting->select_option_vl(array('setting_value'=>'currency_type'));
				if(isset($arrtemp['options']) && !empty($arrtemp['options']) )
					$options = $arrtemp['options'];
					$arr_price = array();
					foreach($line_entry_data['products'] as $product){
						if(!isset($product['option_for'])) continue;
						if(!isset($product['same_parent']) || $product['same_parent'] == 1) continue;
						if (!isset($values['custom_unit_price']))
							$product['custom_unit_price'] = (isset($product['unit_price']) ? $product['unit_price'] : 0);
						if(!isset($arr_price[$product['option_for']]))
							$arr_price[$product['option_for']]['unit_price'] = $arr_price[$product['option_for']]['sub_total'] = 0;
						$product['custom_unit_price'] = (float)str_replace(',', '', $product['custom_unit_price']);
						$product['sub_total'] = (float)str_replace(',', '', $product['sub_total']);
						$arr_price[$product['option_for']]['unit_price'] += $product['custom_unit_price'];
						$arr_price[$product['option_for']]['sub_total'] += $product['sub_total'];
					}
					foreach ($line_entry_data['products'] as $values) {
						if (!isset($values['deleted']) || !$values['deleted']) {
							$arrData['content'].= '<tr>';
								$arrData['content'].= '
												<td style="font-size: 21px ; width:8%; ">' . $values['sku'] . '</td>
												<td style="font-size: 21px; width: 42%">' . $values['products_name'] . '</td>
												<td class="right_text" style="font-size: 21px; width:6%;">' . $values['oum'] . '</td>
												<td class="right_text" style="font-size: 21px; width:5%;">' . $values['specification'] . '</td>
												<td class="right_text" style="font-size: 21px; width:5%;">' . $values['quantity'] . '</td>
												<td class="right_text" style="font-size: 21px; width:17%;">' . $this->opm->format_currency($values['sell_price']) . '</td>
												<td class="right_text" style="font-size: 21px; font-weight:bold; width:17%;">' . $values['amount'] . '</td>
									';
							$arrData['content'].= '</tr>';
						} //
					}//end for


				$sub_total = $total = $taxtotal = 0.00;
				if (isset($line_entry_data['sum_sub_total']))
					$sub_total = $line_entry_data['sum_sub_total'];
				if (isset($line_entry_data['sum_tax']))
					$taxtotal = $line_entry_data['sum_tax'];
				if (isset($line_entry_data['sum_amount']))
					$total = $line_entry_data['sum_amount'];
				//Sub Total
				$arrData['sum_sub_total'] = $this->opm->format_currency($sub_total);
				$arrData['sum_tax'] = $this->opm->format_currency($taxtotal);
				$arrData['sum_amount'] = $this->opm->format_currency($total);
			}//end if
			$this->selectModel('Company');
			$company = $this->Company->select_one(array('system' => true),array('_id'));
			$this->selectModel('Salesaccount');
			$salesaccount = $this->Salesaccount->select_one(array('company_id' => $company['_id']),array('tax_no'));
			$arrData['render_path'] = '../Elements/view_pdf';
			$arrData['no_note'] = true;
			/*if($query['invoice_status'] == 'Cancelled') {
				$arrData['is_cancelled'] = true;
			}*/
			/*$arrData['extra_note'] = '<div style="font-size: 13pt">
										<span style="font-size: 16pt;font-weight: bold">Thank you for your business.</span><br />
										Please make cheque payable to <span style="font-size: 16px; font-weight: bold">Anvy Digital Imaging Inc.</span>
										<br />
										<br />
										<span style="font-size: 13pt;font-weight: bold">GST / Business Reg.#: </span>
									</div>';*/
			$arrData['last_table'] = '<table id="last_table" style="margin-bottom: 25px;" >
								<tr style="font-size:18px;">
									<td style="text-align:center; width: 40%; border: 1px solid #FFFFFF"></td>
									<td style="text-align:center; width: 20%;"><b>NỢ CŨ</b></td>
									<td style="text-align:center;width: 20%"><b>TOA MỚI</b></td>
									<td style="text-align:center;width: 20%"><b>TỔNG CỘNG</b></td>
								</tr>
								<tr style="font-size:25px;">
									<td style="border: 1px solid #FFFFFF"> </td>
									<td style="text-align:center; font-weight:bold;" >'.number_format($no_cu).' </td>
									<td style="text-align:center; font-weight:bold;">'.number_format($sum_amount) .'</td>
									<td style="text-align:center; font-weight:bold;">'.($no_cu ? number_format($con_lai) : number_format($sum_amount)).' </td>
								</tr>
							</table>';
			$arrData['last_table_final'] =  '<table id="last_table_final">
								<tr style="font-size:20px" >
									<td style="text-align:center; width: 25%;">Người nhận hàng<br /> (Ký, họ tên)</td>
									<td style="text-align:center; width: 25%">Người thu tiền <br />(Ký, họ tên)</td>
									<td style="text-align:center;width: 25%">Người lập<br />(Ký, họ tên)</td>
									<td style="text-align:center;width: 25%">Trưởng đơn vị<br />(Ký, họ tên)</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 25px;">
									<td></td>
									<td></td>
									<td></td>
									<td style="text-align:center;width: 25%; font-size: 20px;">Phan Thị Khuyến Hạnh</td>
								</tr>
							</table>';
			$this->render_pdf($arrData);
		}
	}

	//address
	public function set_entry_address($arr_tmp, $arr_set) {
		$address_fset = array('address_1', 'address_2', 'address_3', 'town_city', 'country', 'province_state', 'zip_postcode');
		$address_value = $address_province_id = $address_country_id = $address_province = $address_country = array();
		$address_controller = array('invoice', 'shipping');
		$address_value['invoice'] = $address_value['shipping'] = array('', '', '', '', "VN", '', '');
		$this->set('address_controller', $address_controller); //set
		$address_key = array('invoice','shipping');
		$this->set('address_key', $address_key); //set
		$address_country = $this->country();
		$this->set('shipping_contact_name', (isset($arr_tmp['shipping_address'][0]['shipping_contact_name']) ? $arr_tmp['shipping_address'][0]['shipping_contact_name'] : ''));
		foreach ($address_key as $kss => $vss) {
			//neu ton tai address trong data base
			if (isset($arr_tmp[$vss . '_address'][0])) {
				$arr_temp_op = $arr_tmp[$vss . '_address'][0];
				for ($i = 0; $i < count($address_fset); $i++) { //loop field and set value for display
					if (isset($arr_temp_op[$vss . '_' . $address_fset[$i]])) {
						$address_value[$vss][$i] = $arr_temp_op[$vss . '_' . $address_fset[$i]];
					} else {
						$address_value[$vss][$i] = '';
					}
				}
				//get province list and country list

				if (isset($arr_temp_op[$vss . '_country_id']))
					$address_province[$vss] = $this->province($arr_temp_op[$vss . '_country_id']);
				else
					$address_province[$vss] = $this->province();
				//set province
				if (isset($arr_temp_op[$vss . '_province_state_id']) && $arr_temp_op[$vss . '_province_state_id'] != '' && isset($address_province[$vss][$arr_temp_op[$vss . '_province_state_id']]))
					$address_province_id[$kss] = $arr_temp_op[$vss . '_province_state_id'];
				else if (isset($arr_temp_op[$vss . '_province_state']))
					$address_province_id[$kss] = $arr_temp_op[$vss . '_province_state'];
				else
					$address_province_id[$kss] = '';

				//set country
				if (isset($arr_temp_op[$vss . '_country_id'])) {
					$address_country_id[$kss] = $arr_temp_op[$vss . '_country_id'];
					$address_province[$vss] = $this->province($arr_temp_op[$vss . '_country_id']);
				} else {
					$address_country_id[$kss] = "VN";
					$address_province[$vss] = $this->province("VN");
				}

				$address_add[$vss] = '0';
				//chua co address trong data
			} else {
				$address_country_id[$kss] = "VN";
				$address_province[$vss] = $this->province("VN");
				$address_add[$vss] = '1';
			}
		}
		$this->set('address_value', $address_value);
		$address_hidden_field = array('invoice_address', 'shipping_address');
		$this->set('address_hidden_field', $address_hidden_field); //set
		$address_label[0] = $arr_set['field']['panel_2']['invoice_address']['name'];
		$address_label[0] = $arr_set['field']['panel_2']['shipping_address']['name'];
		$this->set('address_label', $address_label); //set
		$address_conner[0]['top'] = 'hgt fixbor';
		$address_conner[0]['bottom'] = 'fixbor2 jt_ppbot';
		$address_conner[1]['top'] = 'hgt';
		$address_conner[1]['bottom'] = 'fixbor3 jt_ppbot';
		$this->set('address_conner', $address_conner); //set
		$this->set('address_country', $address_country); //set
		$this->set('address_country_id', $address_country_id); //set
		$this->set('address_province', $address_province); //set
		$this->set('address_province_id', $address_province_id); //set
		$this->set('address_more_line', 3); //set
		$this->set('address_onchange', "save_address_pr('\"+keys+\"');");
		if (isset($arr_tmp['company_id']) && strlen($arr_tmp['company_id']) == 24)
			$this->set('address_company_id', 'company_id');
		if (isset($arr_tmp['contact_id']) && strlen($arr_tmp['contact_id']) == 24)
			$this->set('address_contact_id', 'contact_id');
		$this->set('address_add', $address_add);
	}

/*	function other(){
		$arr_tmp = array();
		$arr_tmp = $this->Salesorder->select_one(array('_id' => new MongoId($this->get_id())));

		$subdatas = array();
		$subdatas['detail'] = isset($arr_tmp['account_details'][0])?$arr_tmp['account_details'][0]:'';
		$subdatas['trade1'] = isset($arr_tmp['trade1'][0])?$arr_tmp['trade1'][0]:'';
		$subdatas['trade2'] = isset($arr_tmp['trade1'][1])?$arr_tmp['trade1'][1]:'';
		$subdatas['account_app'] = isset($arr_tmp['account_app'][0])?$arr_tmp['account_app'][0]:'';
		$subdatas['account_app']['approved_date'] = isset($subdatas['account_app']['approved_date']) && is_object($subdatas['account_app']['approved_date']) ? date('d M, Y',$subdatas['account_app']['approved_date']->sec):'';
		$this->selectModel('Salesorder');
		$this->set('subdatas', $subdatas);
	}*/

	function ajax_save(){
		$array_not_save = array('status'=>'error');
		$check_error = 0;
		if(!$this->check_cond_process_payment()){
			$array_not_save['message'] =  'ask_send_email_csr';
			die;
		} else if( isset($_POST['field'])){
			if($_POST['func']=='update'&&!$this->check_permission($this->name.'_@_entry_@_edit')){
				$array_not_save['message'] =  'You do not have permission on this action.';
				die;
			}
			if( $_POST['field'] == 'code') {
			   /* if(!$this->check_password(true)){
					echo 'wrong_pass';
					die;
				}
				$old = $this->opm->select_one(array('code'=>$value),array('_id'));
				if(isset($old['_id'])){
					echo 'code_existed';
					die;
				}
				$ids = $this->opm->update($_POST['ids'], $_POST['field'], $value);
				die;*/
			} else if(  $_POST['field'] == 'status'){
				if(!$this->check_password(true)){
					$array_not_save['message'] =  'wrong_pass';
					die;
				} else
				   $_POST['value'] = $_POST['value']['value'];
				$id = $this->get_id();
				if( $_POST['value'] == 'Hoàn thành'){
					$query = $this->opm->select_one(array('_id'=> new MongoId($id)),array('status','products'));
					if(isset($query['products']) && is_array($query['products'])){
						$this->selectModel('Product');

						foreach ($query['products'] as $key => $value) {
							if(isset($value['deleted']) && $value['deleted']) continue;
							if(!is_object($value['products_id'])) continue;
							$product = $this->Product->select_one(array('_id'=> new MongoId($value['products_id'])),array('in_stock', 'specification', 'prime_cost','sku'));
							if(!isset($product['in_stock'])) $product['in_stock'] = 0;
							if($product['in_stock'] > $value['quantity'] || $product['in_stock'] == $value['quantity']){
								$product['in_stock'] -= $value['quantity'];
								$product['von_luu'] = $product['in_stock'] * $product['prime_cost'] * $product['specification'];
							}else{
								$array_not_save['message'] = 'not_save';
								$array_not_save['sku'][] = $product['sku'];
								$check_error = 1;
							}
						}
					}
				} else if ($_POST['value'] != 'Hoàn thành'){
					$query = $this->opm->select_one(array('_id'=> new MongoId($id)),array('status','products','specification'));
					if(isset($query['products']) && is_array($query['products'])){
						$this->selectModel('Product');
						foreach ($query['products'] as $key => $value) {
							if(isset($value['deleted']) && $value['deleted']) continue;
							if(!is_object($value['products_id'])) continue;
							$product = $this->Product->select_one(array('_id'=>$value['products_id']),array('in_stock','prime_cost','specification'));
							if(!isset($product['in_stock'])) $product['in_stock'] = 0;
							$product['in_stock'] += $value['quantity'];
							$product['von_luu'] = $product['in_stock'] * $product['prime_cost'] * $product['specification'];
							$this->Product->save($product);
						}
					}
				}
			}

		}
		if($check_error){
			echo json_encode($array_not_save);
			die;
		}else{
			$query = $this->opm->select_one(array('_id'=> new MongoId($id)),array('status','products'));
			if(isset($query['products']) && is_array($query['products'])){
				$this->selectModel('Product');

				foreach ($query['products'] as $key => $value) {
					if(isset($value['deleted']) && $value['deleted']) continue;
					if(!is_object($value['products_id'])) continue;
					$product = $this->Product->select_one(array('_id'=> new MongoId($value['products_id'])),array('in_stock', 'specification', 'prime_cost','sku'));
					if(!isset($product['in_stock'])) $product['in_stock'] = 0;
					if($product['in_stock'] > $value['quantity'] || $product['in_stock'] == $value['quantity']){
						$product['in_stock'] -= $value['quantity'];
						$product['von_luu'] = $product['in_stock'] * $product['prime_cost'] * $product['specification'];
						$this->Product->save($product);
					}else{
						$array_not_save['message'] = 'not_save';
						$array_not_save['sku'][] = $product['sku'];
						$check_error = 1;
					}
				}
			}
		}
		parent::ajax_save();
	}

	function khuongnhi_cal_sum($products){




		$arr_sum = array('sum_sub_total' => 0,'sum_amount' => 0, 'sum_amount_interest' => 0);
		foreach($products as $key => $value){
			if(isset($value['deleted']) && $value['deleted']) continue;
			$arr_sum['sum_sub_total'] += isset($value['sub_total']) ? $value['sub_total'] : 0;
			$arr_sum['sum_amount'] += isset($value['amount']) ? $value['amount'] : 0;
			$arr_sum['sum_amount_interest'] += isset($value['amount_interest']) ? $value['amount_interest'] : 0;
		}
		$arr_sum['sum_tax'] = $arr_sum['sum_amount'] - $arr_sum['sum_sub_total'];
		return $arr_sum;
	}

	function khuongnhi_cal_price($arr_post = array()){
		if(isset($_POST)){
			$arr_post = $_POST;
		}
		$key = $arr_post['key'];
		$name = $arr_post['name'];
		$value = $arr_post['value'];
		$query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())),array('products'));
		$specification = 0;
		$line = $query['products'][$key];
		$line[$name] = $value;
		$specification = $line['specification'];
		$this->selectModel('Product');
		$product = $this->Product->select_one(array('_id' => $query['products'][$key]['products_id']),array('specification','prime_cost', 'in_stock'));
		if( !$specification && is_object($line['products_id'])){
			$product = $this->Product->select_one(array('_id' => $query['products'][$key]['products_id']),array('specification','prime_cost'));
			$prime_cost = $product['prime_cost'];
			if(!isset($product['specification']))
				$product['specification'] = 0;
			$specification = (float)$product['specification'];
		}
		$quantity = $line['quantity'];
		$message = '';
		$line['sub_total'] = $line['amount'] = $specification * $line['quantity'] * $line['sell_price'];
		$total = $specification * $line['quantity'] * $product['prime_cost'];
		$line['amount_interest'] = $line['amount'] - $total;
		$line['tax'] = 0;
		$query['products'][$key] = $line;
		$query = array_merge($query,$this->khuongnhi_cal_sum($query['products']));
		if($quantity > $product['in_stock'])
			$message = 'not_save';
		else $this->opm->save($query);
		echo json_encode(array(
						'message' => $message,
						'sub_total' => number_format( $line['sub_total']),
						'amount' => number_format( $line['sub_total']),
						'tax' => number_format( $line['sub_total']),
						'sum_sub_total' => number_format( $query['sum_sub_total']),
						'sum_tax' => number_format( $query['sum_tax']),
						'sum_amount' => number_format( $query['sum_amount']),
						'sum_amount_interest' => number_format($line['amount_interest']),
			));
		die;
	}

	function arr_associated_data($field = '', $value = '', $valueid = '' , $fieldopt='') {
		$arr_return = array();
		$arr_return[$field] = $value;
		if(isset($_POST['arr']) && is_string($_POST['arr']) && $_POST['arr']!='')
			$tmp_data = (array)json_decode($_POST['arr']);
		if(isset($tmp_data['keys'])&&$tmp_data['keys']=='update'
			&&!$this->check_permission($this->name.'_@_entry_@_edit')){
			echo 'You do not have permission on this action.';
			die;
		} else if (isset($tmp_data['keys'])&&$tmp_data['keys']=='add'
			&&!$this->check_permission($this->name.'_@_entry_@_add')){
			echo 'You do not have permission on this action.';
			die;
		}
		if ($field == 'company_name' && $valueid != '') {

			$arr_return = array();
			$arr_return['company_name'] = $value;
			$arr_return['company_id'] = new MongoId($valueid);
			$this->selectModel('Company');
			$query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
			$arr_return['name'] = $query['code'].'-'.$value;
			$this->selectModel('Salesaccount');
			$salesaccount = $this->Salesaccount->select_one(array('company_id' => $arr_return['company_id']));
			$arr_return['payment_terms'] = (isset($salesaccount['payment_terms']) ? $salesaccount['payment_terms'] : 0);
			$arr_return['payment_terms_id'] = (isset($salesaccount['payment_terms_id']) ? $salesaccount['payment_terms_id'] : 0);
			if(isset($query['products']) && !empty($query['products'])){
				foreach($query['products'] as $product_key=>$product){
					if(isset($product['deleted'])&&$product['deleted']) continue;
					if(isset($product['same_parent'])&&$product['same_parent']==1) continue;
					$this->ajax_cal_line(array('arr'=>array('id'=>$product_key),'field'=>'quantity','company_id'=>$arr_return['company_id']));
				}
			}
			$this->selectModel('Contact');
			$arr_contact = $arrtemp = array();
			$arr_company = $this->Company->select_one(array('_id'=>new MongoId($arr_return['company_id'])));
			if (isset($arr_company['contact_default_id']) && is_object($arr_company['contact_default_id'])) {
				$arr_contact = $this->Contact->select_one(array('_id' => new MongoId($arr_company['contact_default_id'])));
			}
			elseif($fieldopt!='')
			{
				$contact_id = $fieldopt;
				$arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
			}
			else
			{
				$arr_contact = $this->Contact->select_all(array(
					'arr_where' => array('company_id' => new MongoId($valueid)),
					'arr_order' => array('_id' => -1),
				));
				$arrtemp = iterator_to_array($arr_contact);
				if (count($arrtemp) > 0) {
					$arr_contact = current($arrtemp);
				} else
					$arr_contact = array();
			}


			if (isset($arr_contact['_id'])) {
				$arr_return['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
				$arr_return['contact_id'] = $arr_contact['_id'];
			} else {
				$arr_return['contact_name'] = '';
				$arr_return['contact_id'] = '';
			}

			if (isset($arr_company['our_rep_id']) && is_object($arr_company['our_rep_id'])) {
				$arr_return['our_rep_id'] = $arr_company['our_rep_id'];
				$arr_return['our_rep'] = $arr_company['our_rep'];
			} else {
				$arr_return['our_rep_id'] = $this->Company->user_id();
				$arr_return['our_rep'] = $this->Company->user_name();
			}




			if (isset($arr_company['our_csr_id']) && $arr_company['our_csr_id'] != '' && $arr_company['our_csr_id']!=null) {
				$arr_return['our_csr_id'] = $arr_company['our_csr_id'];
				$arr_return['our_csr'] = $arr_company['our_csr'];
			} else {
				$arr_return['our_csr_id'] = $this->Company->user_id();
				$arr_return['our_csr'] = $this->Company->user_name();
			}

			$arr_return['phone'] = '';
			if (isset($arr_company['phone']))
				$arr_return['phone'] = $arr_company['phone'];
			if (isset($arr_contact['direct_dial']) && $arr_contact['direct_dial'] != '')
				$arr_return['phone'] = $arr_contact['direct_dial'];

			$arr_return['company_phone'] = '';
			if (isset($arr_company['phone']))
				$arr_return['company_phone'] = $arr_company['phone'];


			$arr_return['direct_phone'] = '';
			if (isset($arr_contact['direct_dial']))
				$arr_return['direct_phone'] = $arr_contact['direct_dial'];

			$arr_return['mobile'] = '';
			if (isset($arr_contact['mobile']))
				$arr_return['mobile'] = $arr_contact['mobile'];


			$arr_return['home_phone'] = '';
			if (isset($arr_contact['home_phone']))
				$arr_return['home_phone'] = $arr_contact['home_phone'];

			$arr_return['email'] = '';
			if (isset($arr_company['email']))
				$arr_return['email'] = $arr_company['email'];
			if (isset($arr_contact['email']) && $arr_contact['email'] != '')
				$arr_return['email'] = $arr_contact['email'];


			$arr_return['fax'] = '';
			if (isset($arr_company['fax']))
				$arr_return['fax'] = $arr_company['fax'];
			if (isset($arr_contact['fax']) && $arr_contact['fax'] != '')
				$arr_return['fax'] = $arr_contact['fax'];

			//change address
			if (isset($arr_company['addresses_default_key']))
			{
				$add_default = $arr_company['addresses_default_key'];
				$arr_return['addresses_default_key']= $arr_company['addresses_default_key'];
			}
			if (isset($add_default) && isset($arr_company['addresses'][$add_default])) {
				foreach ($arr_company['addresses'][$add_default] as $ka => $va) {
					if ($ka != 'deleted')
						$arr_return['invoice_address'][0]['invoice_' . $ka] = $va;
					else
						$arr_return['invoice_address'][0][$ka] = $va;
				}
			}
		}
		else if($field == 'job_name'){
			$this->selectModel('Job');
			$job = $this->Job->select_one(array('_id'=> new MongoId($valueid)),array('no','name','custom_po_no'));
			$arr_return['job_number'] = $job['no'];
			$arr_return['job_name'] = (isset($job['name']) ? $job['name'] : '');
			$arr_return['job_id'] = new MongoId($job['_id']);
			$arr_return['customer_po_no'] = (isset($job['custom_po_no']) ? $job['custom_po_no'] : '');
		}
		else if ($field == 'contact_name' && $valueid != '') {
			$salesorder = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
			$arr_return['contact_id'] = new MongoId($valueid);
			if(!isset($salesorder['company_id']) || !is_object($salesorder['company_id'])){
				$salesorder = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())));
				if(!isset($salesorder['company_id']) || !is_object($salesorder['company_id'])){
					$this->selectModel('Salesaccount');
					$salesaccount = $this->Salesaccount->select_one(array('contact_id'=>$arr_return['contact_id']));
					$arr_return['payment_terms'] = (isset($salesaccount['payment_terms']) ? $salesaccount['payment_terms'] : 0);
					$arr_return['payment_terms_id'] = (isset($salesaccount['payment_terms_id']) ? $salesaccount['payment_terms_id'] : 0);
				}
			}
		}
		else if ($field == 'our_rep' && $valueid != '') {
			$arr_return['our_rep_id'] = new MongoId($valueid);
		}
		else if ($field == 'our_csr' && $valueid != '') {
			$arr_return['our_csr_id'] = new MongoId($valueid);
		}
		else if ($field == 'shipper' && $valueid != '') {
			$arr_return['shipper_id'] = new MongoId($valueid);
		}
		/**
		 * Save Line entry
		*/
		if($field == 'products'){

			if(isset($value[$valueid]) && isset($value[$valueid]['products_id']) && is_object($value[$valueid]['products_id']) && $fieldopt=='code'){
				$query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())),array('products'));
				$product_id = $value[$valueid]['products_id'];
				$value[$valueid] = array(
						'deleted' => false,
						'products_id' => '',
						'products_name' => 'Click để chọn sản phẩm...',
						'code' => '',
						'sku' => '',
						'quantity' => 0,
						'sell_price' => 0,
						'specification' => 0,
						'amount_interest' => 0,
					);
				if(is_object($product_id)){
					$this->selectModel('Product');
					$product = $this->Product->select_one(array('_id' => $product_id),array('sell_price','unit_price','name','sku','code','specification','oum','prime_cost'));
					$total = $product['sell_price'] * $product['specification'] * 0;
					$total_prime_cost = $product['prime_cost'] * $product['specification'] * 0;
					$total_all = $total - $total_prime_cost;
					$value[$valueid] = array(
						'deleted' => false,
						'products_id' => $product['_id'],
						'products_name' => $product['name'],
						'code' => $product['code'],
						'sku' => $product['sku'],
						'quantity' => 0,
						'sell_price' => isset($product['sell_price']) ? $product['sell_price'] : 0,
						'specification' => isset($product['specification']) ? $product['specification'] : 0,
						'sub_total' =>  isset($product['sell_price']) ? $product['sell_price'] : 0,
						'amount' => $product['sell_price'] * $product['specification'] * 0 ,
						'amount_interest' => $total_all,
						'tax' => 0,
						'oum' => isset($product['oum']) ? $product['oum'] : ''
					);
				}
				$arr_return = array();
				$arr_return = array_merge( $this->khuongnhi_cal_sum($value) );
			}
			$arr_return[$field] = $value;
		} else if($field=='asset_tags'){
			ksort($value);
			$arr_return[$field] = $value;


		//OPTIONS
		}
		return $arr_return;
	}


	public function view_pdf_return($getfile=false,$type='') {
		$this->layout = 'pdf';
		$info_data = (object) array();
		$ids = $this->get_id();
		if ($ids != '') {
			$query = $this->opm->select_one(array('_id' => new MongoId($ids)));
			$arrtemp = $query;
			$this->selectModel('Company');
			$company_id = $query['company_id'];
			$arr_company = $this->Company->select_one(array('_id' => new MongoId($company_id)),array('thanh_toan','no_cu'));
			$this->selectModel('Salesorder');
			$arr_salesorder = $this->Salesorder->select_all(array(
															 'arr_where' => array('company_id' => new MongoId($company_id)),
															 'arr_field' => array('code','sum_amount','salesorder_date'),
															 'arr_order' => array('salesorder_date' => 1)
															 ));
			$arr_order = array();
			$con_lai = 0;
			$thanh_tien = 0;
			$total_amount = 0;
			$total_sum = 0;
			foreach($arr_salesorder as $key => $value){
				$arr_order[$key] = $value;
				$arr_order[$key]['no_cu'] = $con_lai;
				$total_sum = $arr_order[$key]['thanh_tien'] = $arr_order[$key]['sum_amount'] + $arr_order[$key]['no_cu'];
				$total_amount += $arr_order[$key]['sum_amount'];
				if(isset($arr_company['thanh_toan']) ) {
					foreach($arr_company['thanh_toan'] as $k => $thanh_toan){
						if(!isset($thanh_toan['id']) || $thanh_toan['id'] != $value['_id']) continue;
						$arr_order[$key]['thanh_toan'] = isset($thanh_toan['amount']) ? (double)$thanh_toan['amount'] : 0;
						//break;
					}
				}
				else
					$arr_order[$key]['thanh_toan'] = 0;
				$thanh_tien = isset($arr_order[$key]['thanh_toan']) ? $arr_order[$key]['thanh_toan'] : 0 ;
				$con_lai = $arr_order[$key]['con_lai'] = $arr_order[$key]['thanh_tien'] - $thanh_tien ;
			}
			//set header
			$this->set('logo_link', 'img/logo_anvy.jpg');
			$this->set('company_address', 	'<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
											ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
											<span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
											ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
											Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
											Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>');

			//customer address
			//loop 2 address
			$arradd = array('invoice', 'shipping');
			foreach ($arradd as $vvs) {
				$kk = $vvs;
				$customer_address = '';
				if (isset($arrtemp[$kk . '_address']) && isset($arrtemp[$kk . '_address'][0]) && count($arrtemp[$kk . '_address']) > 0) {
					$temp = $arrtemp[$kk . '_address'][0];
					if (isset($temp[$kk . '_address_1']) && $temp[$kk . '_address_1'] != '')
						$customer_address .= $temp[$kk . '_address_1'] . ', ';
					if (isset($temp[$kk . '_address_2']) && $temp[$kk . '_address_2'] != '')
						$customer_address .= $temp[$kk . '_address_2'] . ', ';
					if (isset($temp[$kk . '_address_3']) && $temp[$kk . '_address_3'] != '')
						$customer_address .= $temp[$kk . '_address_3'] ;
					else
						$customer_address .= '';
					if (isset($temp[$kk . '_town_city']) && $temp[$kk . '_town_city'] != '')
						$customer_address .= $temp[$kk . '_town_city'].', ';

					if (isset($temp[$kk . '_province_state']))
						$customer_address .= ' ' . $temp[$kk . '_province_state'];
					else if (isset($temp[$kk . '_province_state_id']) && isset($temp[$kk . '_country_id'])) {
						$keytemp = $temp[$kk . '_province_state_id'];
						$provkey = $this->province($temp[$kk . '_country_id']);
						if (isset($provkey[$temp]))
							$customer_address .= ' ' . $provkey[$temp] . '';
					}
					$arr_address[$kk] = $customer_address;
				}
			}

			if (isset($arrtemp['heading']) && $arrtemp['heading'] != '')
				$heading = $arrtemp['heading'];
			else
				$heading = '';
			if (!isset($arr_address['invoice']))
				$arr_address['invoice'] = '';
			//$this->set('customer_address', $customer . $arr_address['invoice']);
			if (!isset($arr_address['shipping']))
				$arr_address['shipping'] = '';
			if(isset($arrtemp['shipping_address'][0]['shipping_contact_name']))
				$this->set('ship_to',$arrtemp['shipping_address'][0]['shipping_contact_name']);
			$this->set('shipping_address', $arr_address['shipping']);
			$this->set('ref_no', $arrtemp['code']);
			$phone = isset($arrtemp['phone'])?$arrtemp['phone']:'';
			$this->set('phone',$phone);
			$info_data->contact_name = $arrtemp['company_name'];
			$info_data->no = $arrtemp['code'];
			$info_data->job_no = $arrtemp['job_number'];
			$info_data->date = $this->opm->format_date($arrtemp['salesorder_date']);
			$info_data->po_no = $arrtemp['customer_po_no'];
			$info_data->ac_no = '';
			$info_data->terms = $arrtemp['payment_terms'];
			$info_data->current_day = date('H:i d-m-y');
			$info_data->required_date = $this->opm->format_date($arrtemp['payment_due_date']);

			$this->set('info_data', $info_data);
			/*             * Nội dung bảng giá */
			$date_now = date('Ymd');
			$numkey = explode("-",$info_data->no);
			$filename = 'SO-'.$numkey[count($numkey)-1].time();
			$other_comment = '';
			if(isset($arrtemp['other_comment']))
				$other_comment = str_replace("\n","<br />",'<br />'.$arrtemp['other_comment']);
			$this->set('other_comment',$other_comment);
			$this->set('filename', $filename);
			$this->set('heading', $heading);
			$html_cont = '';
			$line_entry_data = $this->line_entry_data();
			$minimum = $this->get_minimum_order();
			if($arrtemp['sum_sub_total']<$minimum){
				$more_sub_total = $minimum - (float)$arrtemp['sum_sub_total'];
				if(isset($line_entry_data['products']) && !empty($line_entry_data['products'])){
					$last_insert = end($line_entry_data['products']);
					foreach($last_insert as $key=>$value){
						if($key=='deleted' || $key == 'taxper') continue;
						$last_insert[$key] = '';
					}
				}
				$last_insert['_id'] = -1;
				$last_insert['products_name'] = 'Minimum Order Adjustment';
				$last_insert['quantity'] = '1';
				if(!isset($last_insert['taxper']))
						$last_insert['taxper']= 0;
				$last_insert['custom_unit_price'] = $last_insert['sub_total'] = $more_sub_total;
				$last_insert['tax'] = $last_insert['sub_total']*$last_insert['taxper']/100;
				$last_insert['amount'] = $last_insert['sub_total']+$last_insert['tax'];
				array_push($line_entry_data['products'], $last_insert);
				$arrtemp['sum_sub_total']+=$more_sub_total;
				$arrtemp['sum_tax']+=$last_insert['tax'];
				$arrtemp['sum_amount']+=$last_insert['amount'];
			}
			if (isset($line_entry_data['products']) && is_array($line_entry_data['products']) && count($line_entry_data['products']) > 0) {
				$line = 0;
				$colum = 7;
				$options = array();
				if(isset($arrtemp['options']) && !empty($arrtemp['options']) )
					$options = $arrtemp['options'];
				$arr_price = array();
				foreach($line_entry_data['products'] as $product){
					if(!isset($product['option_for'])) continue;
					if(!isset($product['same_parent']) || $product['same_parent'] == 1) continue;
					if (!isset($values['custom_unit_price']))
						$product['custom_unit_price'] = (isset($product['unit_price']) ? $product['unit_price'] : 0);
					if(!isset($arr_price[$product['option_for']]))
						$arr_price[$product['option_for']]['unit_price'] = $arr_price[$product['option_for']]['sub_total'] = 0;
					$arr_price[$product['option_for']]['unit_price'] += $product['custom_unit_price'];
					$arr_price[$product['option_for']]['sub_total'] += $product['sub_total'];
				}
				$total_amount = 0;
				$amount = 0;
				foreach ($line_entry_data['products'] as $values) {
					$return_total = 0;
					if(!empty($values['return_item'])){
						foreach($values['return_item'] as $item){
							$return_total += $item['return_quantity'];
						}
						$amount = $values['specification'] * $values['sell_price'] * $return_total;
						$total_amount += $amount;
						$html_cont .= '<tr style="background-color: #FFFFFF; font-size:12px ">';
						$html_cont .= '<td>'.(isset($values['sku']) ? $values['sku'] : '') .'</td>';
						$html_cont .= '<td>'.(isset($values['products_name']) ? $values['products_name'] : '') .'</td>';
						$html_cont .= '<td style="text-align: center;">'.(isset($values['oum']) ? $values['oum'] : '') .'</td>';
						$html_cont .= '<td style="text-align: center;">'.(isset($values['specification']) ? $values['specification'] : '') .'</td>';
						$html_cont .= '<td style="text-align: right;">'.(isset($return_total) ? $return_total : '') .'</td>';
						$html_cont .= '<td style="text-align: right;">'.(isset($values['sell_price']) ? $this->opm->format_currency($values['sell_price']) : '') .'</td>';
						$html_cont .= '<td style="text-align: right;">'.(isset($amount) ? $this->opm->format_currency($amount) : '') .'</td>';
						$html_cont .= '</tr>';
						$line++;
					}
				}//end for
				if ($line % 2 == 0) {
					$bgs = '#fdfcfa';
					$bgs2 = '#eeeeee';
				} else {
					$bgs = '#eeeeee';
					$bgs2 = '#fdfcfa';
				}


				$sub_total = $total = $taxtotal = 0.00;
				if (isset($arrtemp['sum_sub_total']))
					$sub_total = $arrtemp['sum_sub_total'];
				if (isset($arrtemp['sum_tax']))
					$taxtotal = $arrtemp['sum_tax'];
				if (isset($arrtemp['sum_amount']))
					$total = $arrtemp['sum_amount'];
				if($_SESSION['default_lang']=='vi'){
					$sub_total_label = 'TỔNG CỘNG';
					$hst_gst = 'Thuế';
					$total_label = 'TỔNG CỘNG';
				 } else {
					$sub_total_label = 'Sub total';
					$hst_gst = 'HST/GST';
					$total_label = 'Total';
				 }
				//Sub Total
				$html_cont .= '<tr style="background-color: #eeeeee">
									<td colspan="' . ($colum - 1) . '" align="center" style="font-size:25px; font-weight:bold;border-top:2px solid #aaa;" class="first">'.$sub_total_label.':</td>
									<td align="right" style="font-size:25px; font-weight:bold;border-top:2px solid #aaa;" class="end">' . $this->opm->format_currency($total_amount) . '</td>
							   </tr>';
			}//end if

			$this->set('html_cont', $html_cont);
			$no_cu = 0;
			$no_cu = $con_lai;
			$total = 0;
			$total = $no_cu - $total_amount;
			$last_table =  '<table id="last_table">
								<tr >
									<td style="text-align:center; width: 55%; border: 1px solid #FFFFFF"></td>
									<td style="text-align:center; width: 15%"><b>NỢ CŨ</b></td>
									<td style="text-align:center;width: 15%"><b>TOA TRẢ</b></td>
									<td style="text-align:center;width: 15%"><b>CÒN LẠI</b></td>
								</tr>
								<tr>
									<td style="border: 1px solid #FFFFFF"> </td>
									<td style="text-align:center;" >'.number_format($no_cu).' </td>
									<td style="text-align:center;">'.number_format($total_amount).'</td>
									<td style="text-align:center;">'.number_format($total).'</td>
								</tr>
							</table>';
			$last_table_final =  '<table>
								<tr style="font-size:15px" >
									<td style="text-align:center; width: 25%;">Người nhận hàng<br /> (Ký, họ tên)</td>
									<td style="text-align:center; width: 25%">Người thu tiền <br />(Ký, họ tên)</td>
									<td style="text-align:center;width: 25%">Người lập<br />(Ký, họ tên)</td>
									<td style="text-align:center;width: 25%">Trưởng đơn vị<br />(Ký, họ tên)</td>
								</tr>
								<tr>
									<td ></td>
									<td> </td>
									<td> </td>
									<td> </td>
								</tr>
							</table>';
			$this->set('last_table_final',$last_table_final);
			$this->set('last_table',$last_table);
			$this->set('type',$type);
			if (isset($arrtemp['our_csr'])) {
				$this->set('user_name', ' ' . $arrtemp['our_csr']);
			} else
				$this->set('user_name', ' ' . $this->opm->user_name());
			$this->set('qr_image','https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl='.URL.'/salesorders/entry/'.$this->get_id().'&choe=UTF-8');
			//end set content
			//set footer
			$this->render('view_pdf_return');
			if($getfile)
				return $filename.'.pdf';
			$this->redirect('/upload/' . $filename . '.pdf');
		}
		die;
	}

}