<?php
App::uses('JobsController', 'Controller');
class JobsControllerCustom extends JobsController{
 	public function beforeFilter() {
        parent::beforeFilter();
    }
	function auto_save() {
			if (!empty($this->data)) {
				$arr_return = array();
				$arr_post_data = $this->data['Job'];
				$arr_save = $arr_post_data;
				$this->selectModel('Job');
				if(isset($arr_save['no'])){
					if(!is_numeric($arr_save['no'])){
						echo json_encode(array('status'=>'error','message'=>'must_be_numberic'));
						die;
					}
					$arr_tmp = $this->Job->select_one(array('no' => (int) $arr_save['no'], '_id' => array('$ne' => new MongoId($arr_save['_id']))));
					if (isset($arr_tmp['no'])) {
						echo json_encode(array('status'=>'error','message'=>'ref_no_existed'));
						die;
					}
				}
				$arr_tmp = $this->Job->select_one(array('_id' =>  new MongoId($arr_save['_id'])),array('status','company_id','custom_po_no','status_id'));
				if(isset($arr_save['custom_po_no']) && isset($arr_tmp['custom_po_no']) && $arr_save['custom_po_no']!= $arr_tmp['custom_po_no']){
					foreach(array('Salesinvoice','Salesorder','Quotation') as $model){
						$this->selectModel($model);
						$this->$model->collection->update(
			                                           array('job_id'=>new MongoId($arr_save['_id'])),
			                                           array('$set'=>array(
			                                                 'customer_po_no'=>$arr_save['custom_po_no']
			                                                 )
			                                           ),
			                                           array('multiple'=>true)
		                                           );
					}
				}
				if($arr_save['status_id']=='Completed'){
					if($arr_tmp['status_id']!=$arr_save['status_id']){
						$this->selectModel('Salesorder');
						$salesorder = $this->Salesorder->count(array('job_id'=> new MongoId($arr_save['_id'])));
						$salesorder_completed = $this->Salesorder->count(array('job_id'=> new MongoId($arr_save['_id']),'status_id'=>'Completed'));
						$salesorder_completed =  $salesorder_completed + $this->Salesorder->count(array('job_id'=> new MongoId($arr_save['_id']),'status_id'=>'Hoàn thành'));

						if($salesorder > $salesorder_completed){
							echo json_encode(array('status'=>'error','message'=>'so_completed'));
							die;
						}
						$this->selectModel('Salesinvoice');
						$salesinvoice = $this->Salesinvoice->count(array('job_id'=> new MongoId($arr_save['_id'])));
						$salesinvoice_invoiced = $this->Salesinvoice->count(array('job_id'=> new MongoId($arr_save['_id']),'invoice_status'=>'Invoiced'));
						if($salesinvoice > $salesinvoice_invoiced){
							echo json_encode(array('status'=>'error','message'=>'si_invoiced'));
							die;
						}
						$salesorder_sum = $this->Salesorder->sum('sum_amount','tb_salesorder',array('job_id'=>new MongoId($arr_save['_id']),'deleted'=>false));
						$salesinvoice_sum = $this->Salesinvoice->sum('sum_amount','tb_salesinvoice',array('job_id'=>new MongoId($arr_save['_id']),'deleted'=>false));
						if(abs(($salesorder_sum - $salesinvoice_sum) / ($salesinvoice_sum==0? 1 :$salesinvoice_sum) ) > 0.001) {
							echo json_encode(array('status'=>'error','message'=>'sum_different'));
							die;
						}
					}
				}
				if($arr_save['status']!='Completed' && $arr_tmp['status']=='Completed'){
					if(isset($_POST['password'])){
						$this->selectModel('Stuffs');
						$change = $this->Stuffs->select_one(array('value'=>'Changing Code'));
						if(md5($_POST['password'])!=$change['password']){
							echo json_encode(array('status'=>'error','message'=>'wrong_pass'));
							die;
						}
					} else {
						echo json_encode(array('status'=>'error','message'=>'need_pass'));
						die;
					}
				}
				$work_start_sec = $this->Common->strtotime($arr_save['work_start'] . ' 00:00:00');
				$work_end_sec = $this->Common->strtotime($arr_save['work_end'] . ' 00:00:00');

				if ($work_start_sec > $work_end_sec) {
					echo json_encode(array('status'=>'error','message'=>'date_work'));
					die;
				}
				$arr_save['work_start'] = new MongoDate($work_start_sec);
				$arr_save['work_end'] = new MongoDate($work_end_sec);
				if (strlen(trim($arr_save['contact_id'])) == 24)
					$arr_save['contact_id'] = new MongoId($arr_save['contact_id']);
				else
					$arr_save['contact_id'] = '';
				if(strlen($arr_save['company_id'])==24){
					$arr_save['company_id'] = new MongoId($arr_save['company_id']);
					if(isset($arr_tmp['company_id'])
					   		&& (string)$arr_tmp['company_id']!= (string)$arr_save['company_id']){
						$this->selectModel('Company');
						$company = $this->Company->select_one(array('_id'=>$arr_save['company_id']),array('contact_default_id'));
						if(isset($company['contact_default_id'])&&is_object($company['contact_default_id'])){
							$this->selectModel('Contact');
							$contact = $this->Contact->select_one(array('_id'=>new MongoId($company['contact_default_id'])),array('full_name'));
							$arr_save['contact_name'] = '';
							$arr_save['contact_id'] = $company['contact_default_id'];
							if(isset($contact['full_name']))
								$arr_save['contact_name'] = $contact['full_name'];
							$arr_return['contact_id'] = (string)$arr_save['contact_id'];
							$arr_return['contact_name'] = $arr_save['contact_name'];
						}
					}
				}
				else
					$arr_save['company_id'] = '';
				if(strlen($arr_save['our_rep_id'])==24)
					$arr_save['our_rep_id'] = new MongoId($arr_save['our_rep_id']);
				else
					$arr_save['our_rep_id'] = '';
				if( strlen($arr_save['name']) > 0 )
					$arr_save['name']{0} = strtoupper($arr_save['name']{0});

				$this->selectModel('Job');
				if ($this->Job->save($arr_save))
					$arr_return['status'] = 'ok';
				 else
					$arr_return = array('status'=>'error','message'=>'Error: ' . $this->Job->arr_errors_save[1]);
				echo json_encode($arr_return);
			}
			die;
	}
}
?>