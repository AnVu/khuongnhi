<?php
// Attach lib cal_price
App::import('Vendor', 'cal_price/cal_price');
App::uses('AppController', 'Controller');

class PurchaseordersControllerCusTom extends PurchaseordersController {

	var $name = 'Purchaseorders';
	public $helpers = array();
	public $opm; //Option Module
	public $cal_price; //Option cal_price
	var $is_text = 0;
	public $modelName = 'Purchaseorder';

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set_module_before_filter('Purchaseorder');
	}
	public function save_data($field = '', $value = '', $ids = '', $valueid = '') {
		if (isset($_POST['field']))
			$field = $_POST['field'];
	   /* if (isset($_POST['value']))
			$value = $_POST['value'];*/
		$this->selectModel('Purchaseorder');
		$this->selectModel('Product');
		if($field=='purchase_orders_status' ){
			if(!$this->check_password(true)){
				echo 'wrong_pass';die;
			}else
				$_POST['value'] = $_POST['value']['value'];
			$id = new MongoId($this->get_id());
				if($_POST['value'] == 'Hoàn thành'){
					$id = new MongoId($this->get_id());
					$query = $this->Purchaseorder->select_one(array('_id' => $id), array('products','code','company_name','purchord_date'));
					if(isset($query['products'])){
						$arr_products = array();
						foreach($query['products'] as $key => $value){
							if(isset($value['deleted']) && $value['deleted']) continue;
							if(!is_object($value['products_id'])) continue;
							if(!isset($arr_products[(string)$value['products_id']]))
								$arr_products[(string)$value['products_id']] = 0;
							$arr_products[(string)$value['products_id']] += $value['quantity'];
						}
						foreach($arr_products as $product_id => $quantity){
							$product = $this->Product->select_one(array('_id' => new MongoId($product_id)), array('warehousing','in_stock','prime_cost','specification'));
						   /* $product['warehousing'][] = array(
															  'deleted' => false,
															  'location_name' => $query['company_name'],
															  'location_id' => new MongoId("541266e200806aa8277a4dbf"),
															  'warehousing_date' => $query['purchord_date'],
															  'code' =>$query['code'],
															  'warehousing_by_id'=> $this->Purchaseorder->user_id(),
															  'qty_in_stock' => $quantity,
															  'warehousing_detail' => 'Tạo từ Mua Hàng #'.$query['code'],
															  'purchaseorder_id' => $id
															  );*/
							$product['in_stock'] += $quantity;
							$product['von_luu'] = $product['in_stock'] * $product['prime_cost'] * $product['specification'];
							$this->Product->save($product);
						}
					}
				}else if( $_POST['value'] != "Hoàn thành"){
					$query = $this->Purchaseorder->select_one(array('_id' => $id), array('purchase_orders_status','products','code'));
					if($query['purchase_orders_status'] == 'Hoàn thành' && isset($query['products'])){
						$arr_products = array();
						foreach($query['products'] as $key => $value){
							if(isset($value['deleted']) && $value['deleted']) continue;
							if(!is_object($value['products_id'])) continue;
							if(!isset($arr_products[(string)$value['products_id']]))
								$arr_products[(string)$value['products_id']] = 0;
							$arr_products[(string)$value['products_id']] += $value['quantity'];
						}
						foreach($arr_products as $product_id => $quantity){
							$product = $this->Product->select_one(array('_id' => new MongoId($product_id)), array('warehousing','in_stock','prime_cost','specification'));
							/*foreach($product['warehousing'] as $k => $v){
								if(isset($v['deleted']) && $v['deleted']) continue;
								if(!isset($v['purchaseorder_id']) && $v['purchaseorder_id'] != $id) continue;
								$product['warehousing'][$k] = array('deleted' => true);
							}*/
							$product['in_stock'] -= $quantity;
							$product['von_luu'] = $product['in_stock'] * $product['prime_cost'] * $product['specification'];
							$this->Product->save($product);
						}
					}
				}
		}
		parent::save_data();
	}

	function arr_associated_data($field = '', $value = '', $valueid = '' , $fieldopt='') {
		$arr_return = array();
		$arr_return[$field] = $value;
		if(isset($_POST['arr']) && is_string($_POST['arr']) && $_POST['arr']!='')
			$tmp_data = (array)json_decode($_POST['arr']);
		if(isset($tmp_data['keys'])&&$tmp_data['keys']=='update'
			&&!$this->check_permission($this->name.'_@_entry_@_edit')){
			echo 'You do not have permission on this action.';
			die;
		} else if (isset($tmp_data['keys'])&&$tmp_data['keys']=='add'
			&&!$this->check_permission($this->name.'_@_entry_@_add')){
			echo 'You do not have permission on this action.';
			die;
		}
		if ($field == 'company_name' && $valueid != '') {

			$arr_return = array();
			$arr_return['company_name'] = $value;
			$arr_return['company_id'] = new MongoId($valueid);
			$this->selectModel('Company');
			$query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())));
			$arr_return['name'] = $query['code'].'-'.$value;
			$this->selectModel('Salesaccount');
			$salesaccount = $this->Salesaccount->select_one(array('company_id' => $arr_return['company_id']));
			$arr_return['payment_terms'] = (isset($salesaccount['payment_terms']) ? $salesaccount['payment_terms'] : 0);
			$arr_return['payment_terms_id'] = (isset($salesaccount['payment_terms_id']) ? $salesaccount['payment_terms_id'] : 0);
			if(isset($query['products']) && !empty($query['products'])){
				foreach($query['products'] as $product_key=>$product){
					if(isset($product['deleted'])&&$product['deleted']) continue;
					if(isset($product['same_parent'])&&$product['same_parent']==1) continue;
					//$this->ajax_cal_line(array('arr'=>array('id'=>$product_key),'field'=>'quantity','company_id'=>$arr_return['company_id']));
				}
			}
			$this->selectModel('Contact');
			$arr_contact = $arrtemp = array();
			$arr_company = $this->Company->select_one(array('_id'=>new MongoId($arr_return['company_id'])));
			if (isset($arr_company['contact_default_id']) && is_object($arr_company['contact_default_id'])) {
				$arr_contact = $this->Contact->select_one(array('_id' => new MongoId($arr_company['contact_default_id'])));
			}
			elseif($fieldopt!='')
			{
				$contact_id = $fieldopt;
				$arr_contact = $this->Contact->select_one(array('_id' => new MongoId($contact_id)));
			}
			else
			{
				$arr_contact = $this->Contact->select_all(array(
					'arr_where' => array('company_id' => new MongoId($valueid)),
					'arr_order' => array('_id' => -1),
				));
				$arrtemp = iterator_to_array($arr_contact);
				if (count($arrtemp) > 0) {
					$arr_contact = current($arrtemp);
				} else
					$arr_contact = array();
			}


			if (isset($arr_contact['_id'])) {
				$arr_return['contact_name'] = $arr_contact['first_name'] . ' ' . $arr_contact['last_name'];
				$arr_return['contact_id'] = $arr_contact['_id'];
			} else {
				$arr_return['contact_name'] = '';
				$arr_return['contact_id'] = '';
			}

			if (isset($arr_company['our_rep_id']) && is_object($arr_company['our_rep_id'])) {
				$arr_return['our_rep_id'] = $arr_company['our_rep_id'];
				$arr_return['our_rep'] = $arr_company['our_rep'];
			} else {
				$arr_return['our_rep_id'] = $this->Company->user_id();
				$arr_return['our_rep'] = $this->Company->user_name();
			}




			if (isset($arr_company['our_csr_id']) && $arr_company['our_csr_id'] != '' && $arr_company['our_csr_id']!=null) {
				$arr_return['our_csr_id'] = $arr_company['our_csr_id'];
				$arr_return['our_csr'] = $arr_company['our_csr'];
			} else {
				$arr_return['our_csr_id'] = $this->Company->user_id();
				$arr_return['our_csr'] = $this->Company->user_name();
			}

			$arr_return['phone'] = '';
			if (isset($arr_company['phone']))
				$arr_return['phone'] = $arr_company['phone'];
			if (isset($arr_contact['direct_dial']) && $arr_contact['direct_dial'] != '')
				$arr_return['phone'] = $arr_contact['direct_dial'];

			$arr_return['company_phone'] = '';
			if (isset($arr_company['phone']))
				$arr_return['company_phone'] = $arr_company['phone'];


			$arr_return['direct_phone'] = '';
			if (isset($arr_contact['direct_dial']))
				$arr_return['direct_phone'] = $arr_contact['direct_dial'];

			$arr_return['mobile'] = '';
			if (isset($arr_contact['mobile']))
				$arr_return['mobile'] = $arr_contact['mobile'];


			$arr_return['home_phone'] = '';
			if (isset($arr_contact['home_phone']))
				$arr_return['home_phone'] = $arr_contact['home_phone'];

			$arr_return['email'] = '';
			if (isset($arr_company['email']))
				$arr_return['email'] = $arr_company['email'];
			if (isset($arr_contact['email']) && $arr_contact['email'] != '')
				$arr_return['email'] = $arr_contact['email'];


			$arr_return['fax'] = '';
			if (isset($arr_company['fax']))
				$arr_return['fax'] = $arr_company['fax'];
			if (isset($arr_contact['fax']) && $arr_contact['fax'] != '')
				$arr_return['fax'] = $arr_contact['fax'];

			//change address
			if (isset($arr_company['addresses_default_key']))
			{
				$add_default = $arr_company['addresses_default_key'];
				$arr_return['addresses_default_key']= $arr_company['addresses_default_key'];
			}
			if (isset($add_default) && isset($arr_company['addresses'][$add_default])) {
				foreach ($arr_company['addresses'][$add_default] as $ka => $va) {
					if ($ka != 'deleted')
						$arr_return['invoice_address'][0]['invoice_' . $ka] = $va;
					else
						$arr_return['invoice_address'][0][$ka] = $va;
				}
			}
		}
		else if($field == 'job_name'){
			$this->selectModel('Job');
			$job = $this->Job->select_one(array('_id'=> new MongoId($valueid)),array('no','name','custom_po_no'));
			$arr_return['job_number'] = $job['no'];
			$arr_return['job_name'] = (isset($job['name']) ? $job['name'] : '');
			$arr_return['job_id'] = new MongoId($job['_id']);
			$arr_return['customer_po_no'] = (isset($job['custom_po_no']) ? $job['custom_po_no'] : '');
		}
		else if ($field == 'contact_name' && $valueid != '') {
			$salesorder = $this->opm->select_one(array('_id'=>new MongoId($this->get_id())));
			$arr_return['contact_id'] = new MongoId($valueid);
			if(!isset($salesorder['company_id']) || !is_object($salesorder['company_id'])){
				$salesorder = $this->opm->select_one(array('_id'=> new MongoId($this->get_id())));
				if(!isset($salesorder['company_id']) || !is_object($salesorder['company_id'])){
					$this->selectModel('Salesaccount');
					$salesaccount = $this->Salesaccount->select_one(array('contact_id'=>$arr_return['contact_id']));
					$arr_return['payment_terms'] = (isset($salesaccount['payment_terms']) ? $salesaccount['payment_terms'] : 0);
					$arr_return['payment_terms_id'] = (isset($salesaccount['payment_terms_id']) ? $salesaccount['payment_terms_id'] : 0);
				}
			}
		}
		else if ($field == 'our_rep' && $valueid != '') {
			$arr_return['our_rep_id'] = new MongoId($valueid);
		}
		else if ($field == 'our_csr' && $valueid != '') {
			$arr_return['our_csr_id'] = new MongoId($valueid);
		}
		else if ($field == 'shipper' && $valueid != '') {
			$arr_return['shipper_id'] = new MongoId($valueid);
		}
		/**
		 * Save Line entry
		*/
		if($field == 'products'){

			if(isset($value[$valueid]) && isset($value[$valueid]['products_id']) && is_object($value[$valueid]['products_id']) && $fieldopt=='code'){
				$query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())),array('products'));
				$product_id = $value[$valueid]['products_id'];
				$value[$valueid] = array(
						'deleted' => false,
						'products_id' => '',
						'products_name' => 'Click để chọn sản phẩm...',
						'code' => '',
						'sku' => '',
						'quantity' => 1,
						'specification' => 0,
						'prime_cost' => 0,
					);
				if(is_object($product_id)){
					$this->selectModel('Product');
					$product = $this->Product->select_one(array('_id' => $product_id),array('sell_price','unit_price','name','sku','code','specification','oum','prime_cost'));
					$value[$valueid] = array(
						'deleted' => false,
						'products_id' => $product['_id'],
						'products_name' => $product['name'],
						'code' => $product['code'],
						'sku' => $product['sku'],
						'quantity' => 1,
						'prime_cost' => isset($product['prime_cost']) ? $product['prime_cost'] : 0,
						'specification' => isset($product['specification']) ? $product['specification'] : 0,
						'sub_total' =>  isset($product['prime_cost']) ? $product['prime_cost'] : 0,
						'amount' => $product['prime_cost'] * $product['specification'] * 1,
						'oum' => isset($product['oum']) ? $product['oum'] : '',
						'tax' => 0,
						'tax' => 0,
						'tax' => isset($product['oum']) ? $product['oum'] : ''
					);
				}
				$arr_return = array();
				$arr_return = array_merge( $this->khuongnhi_cal_sum($value) );
			}
			$arr_return[$field] = $value;
		} else if($field=='asset_tags'){
			ksort($value);
			$arr_return[$field] = $value;


		//OPTIONS
		}
		return $arr_return;
	}



	public function view_pdf($getfile=false, $ids='') {
		$this->layout = 'pdf';
		if($ids == '')
			$ids = $this->get_id();
		else
			$this->module_id = $ids;
		if ($ids != '') {
			$this->selectModel('Purchaseorder');
			$query = $this->opm->select_one(array('_id' => new MongoId($ids)));
			$print = false;
			$print = true;
			if( $print ) {
				if (!isset($_GET['print_pdf'])) {
					$filename = 'PO-' . $query['code'] . (empty($type) ? '-detailed' : ($type == 'group' ? '' : '-' . $type));
					if ($this->print_pdf(array(
						'report_file_name' => $filename,
						'report_url' => URL . '/purchaseorders/view_pdf/' . $getfile . '/' . $ids,
						'custom_footer' => 'order'

					))) {
						if ($getfile) {
							return $filename . '.pdf';
						}

						$this->redirect(URL . '/upload/' . $filename . '.pdf');
					}
					else {
						if ($getfile) {
							return false;
						}
						echo 'Please contact IT for this issue.';
						die;
					}
				}
			}
			$arrtemp = $query;
			$info_data = (object) array();

		$this->selectModel('Purchaseorder');
		$this->selectModel('Company');
		$query = $this->Purchaseorder->select_one(array('_id' => new MongoId($ids)));
		$arrtemp = $query;
			//set header
		$company_id = (string)$query['company_id'];
		$arr_company = $this->Company->select_one(array('_id' => new MongoID($company_id)), array('thanh_toan','no_cu'));
		$arr_company['no_cu'] = 0;

		$arr_purchaseorder = $this->Purchaseorder->select_all(array(
														 'arr_where' => array('company_id' => new MongoId($company_id),'purchase_orders_status'=>"Hoàn thành"),
														 'arr_field' => array('code','sum_amount','purchord_date','products','payment_terms'),
														 'arr_order' => array('purchord_date' => 1)
														 ));
		$this->selectModel('Returnpurchaseorder');
		$arr_return_purchaseorder = $this->Returnpurchaseorder->select_all(array(
													   'arr_where' => array('company_id' => new MongoId($company_id),'purchase_orders_status'=>"Hoàn thành"),
													   'arr_field' => array('code','sum_amount','purchord_date','products','payment_terms','return_id'),
													   'arr_order' => array('purchord_date' => 1)
													   ));
		$this->selectModel('Producterrorcompany');
		$arr_product_error_company = $this->Producterrorcompany->select_all(array(
													 'arr_where' => array('company_id' => new MongoId($company_id), 'purchase_orders_status' => "Hoàn thành"),
													 'arr_field' => array('code','sum_amount','purchord_date','products','salesorder_date'),
													 'arr_order' => array('purchord_date' => 1)
													));


		$arr_pur = array();
		$con_lai = 0;
		$thanh_tien = 0;
		$thanh_tien_return = 0;
		$arr_return = array();
		foreach($arr_purchaseorder as $key => $value){
			$time = $value['purchord_date']->sec;
			while( isset($arr_return[$time]) )
				$time++;
			$value['type'] = 'Purchaseorder';
			$arr_return[$time] = $value;
		}
		$con_lai_return = 0;
		foreach($arr_return_purchaseorder as $k => $v){
			$time = $v['purchord_date']->sec;
			while( isset($arr_return[$time]) )
				$time++;
			$v['type'] = 'Returnpurchaseorder';
			$arr_return[$time] = $v;
		}
		foreach($arr_product_error_company as $k_product_return => $v_product_return){
			$time = $v_product_return['purchord_date']->sec;
			while( isset($arr_return[$time]) )
				$time++;
			$v_product_return['type'] = 'Producterrorcompany';
			$arr_return[$time] = $v_product_return;
		}
		ksort($arr_return);
		foreach($arr_return as $key_return => $v_return){
			$key_return = (string)$v_return['_id'];
			if($v_return['type'] == 'Purchaseorder'){
				$arr_pur[$key_return] = $v_return;
				$arr_pur[$key_return]['no_cu'] = $con_lai;
				$sum_amount = isset($arr_pur[$key_return]['sum_amount']) ? $arr_pur[$key_return]['sum_amount'] : 0;
				$arr_pur[$key_return]['thanh_tien'] = $sum_amount + $arr_pur[$key_return]['no_cu'];
				if(isset($arr_company['thanh_toan']) ) {
					//foreach($arr_company['thanh_toan'] as $k => $thanh_toan){
					$array = array_filter($arr_company['thanh_toan'], function($array) use ($key_return){
						return $array['id'] == $key_return;
					});
						$thanh_toan = end($array);
						//if(!isset($thanh_toan['id']) || $thanh_toan['id'] != $v_return['_id']) continue;
						//if($thanh_toan['id'] == $v_return['_id']){
							$arr_pur[$key_return]['thanh_toan'] = isset($thanh_toan['amount']) ? (double)$thanh_toan['amount'] : 0;
							$arr_pur[$key_return]['type_paid'] = isset($thanh_toan['type_paid']) ? $thanh_toan['type_paid'] : '';
						//}
						//break;
					//}
				}
				$thanh_tien = isset($arr_pur[$key_return]['thanh_toan']) ? $arr_pur[$key_return]['thanh_toan'] : 0 ;
				$arr_pur[$key_return]['con_lai'] = $arr_pur[$key_return]['thanh_tien'];
				$con_lai = $arr_pur[$key_return]['thanh_tien'] - $thanh_tien;
			}else if($v_return['type'] == 'Returnpurchaseorder'){
				$arr_pur[$key_return] = $v_return;
				$arr_pur[$key_return]['no_cu'] = $con_lai;
				$sum_amount = isset($arr_pur[$key_return]['sum_amount']) ? $arr_pur[$key_return]['sum_amount'] : 0;
				$arr_pur[$key_return]['thanh_tien'] =   $arr_pur[$key_return]['no_cu'] - $sum_amount;
				if(isset($arr_company['thanh_toan'])){
					//foreach($arr_company['thanh_toan'] as $kk => $v_thanh_toan){
						$array = array_filter($arr_company['thanh_toan'], function($array) use ($key_return){
							return $array['id'] == $key_return;
						});
						$v_thanh_toan = end($array);
						//if(!isset($v_thanh_toan['id']) || $v_thanh_toan['id'] != $v_return['_id']) continue;
						//if($thanh_toan['id'] == $v_return['_id']){
							$arr_pur[$key_return]['thanh_toan'] = isset($v_thanh_toan['amount']) ? (double)$v_thanh_toan['amount'] : 0;
							$arr_pur[$key_return]['type_paid'] = isset($v_thanh_toan['type_paid']) ? $v_thanh_toan['type_paid'] : '';
						//}
					//}
				}
				$thanh_tien_return = isset($arr_pur[$key_return]['thanh_toan']) ? $arr_pur[$key_return]['thanh_toan'] : 0;
				$arr_pur[$key_return]['con_lai'] = $arr_pur[$key_return]['thanh_tien'] ;
				$con_lai = $arr_pur[$key_return]['thanh_tien'] - $thanh_tien_return;
				$arr_pur[$key_return]['return_id'] = 1;
			}else if($v_return['type'] == 'Producterrorcompany'){
				$arr_pur[$key_return] = $v_return;
				$arr_pur[$key_return]['no_cu'] = $con_lai;
				$sum_amount = isset($arr_pur[$key_return]['sum_amount']) ? $arr_pur[$key_return]['sum_amount'] : 0;
				$arr_pur[$key_return]['thanh_tien'] = $arr_pur[$key_return]['no_cu'] - $sum_amount;
				if(isset($arr_company['thanh_toan'])){
					//foreach($arr_company['thanh_toan'] as $kkk => $v_thanh_toan_p){
						$array = array_filter($arr_company['thanh_toan'], function($array) use ($key_return){
							return $array['id'] == $key_return;
						});
						$v_thanh_toan_p = end($array);
						//if(!isset($v_thanh_toan_p['id']) || $v_thanh_toan_p['id'] != $v_return['_id']) continue;
						//if($thanh_toan['id'] == $v_return['_id']){
							$arr_pur[$key_return]['thanh_toan'] = isset($v_thanh_toan_p['amount']) ? (double)$v_thanh_toan_p['amount'] : 0;
							$arr_pur[$key_return]['type_paid'] = isset($v_thanh_toan_p['type_paid']) ? $v_thanh_toan_p['type_paid'] : '';
						//}
					//}
				}
				$thanh_tien_return_p = isset($arr_pur[$key_return]['thanh_toan']) ? $arr_pur[$key_return]['thanh_toan'] : 0;
				$arr_pur[$key_return]['con_lai'] = $arr_pur[$key_return]['thanh_tien'];
				$con_lai =  $arr_pur[$key_return]['thanh_tien'] - $thanh_tien_return_p;
			}
		}
			$no_cu = 0;
			$sum_amount = 0;
			$con_lai = 0;
			foreach($arr_pur as $key => $value){
				if($value['type'] == "Purchaseorder" && (string)$value['_id'] == $ids){
					$no_cu = $value['no_cu'];
					$sum_amount = $value['sum_amount'];
					$con_lai = $value['con_lai'];
				}
			}
		$arrData['logo'] = '/img/logo_anvy.jpg';
				$arrData['company_address'] = '<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
				ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
				<span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
				ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
				Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
				Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>';
				$arrData['content'] = '';
				$arrData['title'] = array(
					'Mã'=>'font-size: 150% !important',
					'Tên sản phẩm'=>'font-size: 150% !important',
					'ĐVT' => 'text-align: right;font-size: 150% !important',
					'Quy cách' => 'text-align: right;font-size: 150% !important',
					'Số lượng' => 'text-align: right;font-size: 150% !important',
					'Đơn giá' => 'text-align: right;font-size: 150% !important',
					'Thành tiền' => 'text-align: right;font-size: 150% !important'
				);

			$arradd = array('invoice', 'shipping');
			foreach ($arradd as $vvs) {
				$kk = $vvs;
				$customer_address = '';
				if (isset($arrtemp[$kk . '_address']) && isset($arrtemp[$kk . '_address'][0]) && count($arrtemp[$kk . '_address']) > 0) {
					$temp = $arrtemp[$kk . '_address'][0];
					if (isset($temp[$kk . '_address_1']) && $temp[$kk . '_address_1'] != '')
						$customer_address .= $temp[$kk . '_address_1'] . ', ';
					if (isset($temp[$kk . '_address_2']) && $temp[$kk . '_address_2'] != '')
						$customer_address .= $temp[$kk . '_address_2'] . ' ';
					if (isset($temp[$kk . '_address_3']) && $temp[$kk . '_address_3'] != '')
						$customer_address .= $temp[$kk . '_address_3'];
					else
						$customer_address .= '';
					if (isset($temp[$kk . '_town_city']) && $temp[$kk . '_town_city'] != '')
						$customer_address .= $temp[$kk . '_town_city'];

					if (isset($temp[$kk . '_province_state']))
						$customer_address .= ' ' . $temp[$kk . '_province_state'] . ' ';
					else if (isset($temp[$kk . '_province_state_id']) && isset($temp[$kk . '_country_id'])) {
						$keytemp = $temp[$kk . '_province_state_id'];
						$provkey = $this->province($temp[$kk . '_country_id']);
						if (isset($provkey[$temp]))
							$customer_address .= ' ' . $provkey[$temp] . ' ';
					}
					$arr_address[$kk] = $customer_address;
				}
			}


			if (isset($arrtemp['name']) && $arrtemp['name'] != '') {
				$heading = $arrtemp['name'];
			} else {
				$heading = '';
			}

			if ($arrtemp['ship_to_contact_name']) {
				$ship_to_contact_name = $arrtemp['ship_to_contact_name'] . '<br>';
			} else {
				$ship_to_contact_name = '';
			}
			$this->set('ship_to_contact_name', $ship_to_contact_name);

			if(isset($arr_address['invoice']))
				$this->set('shipping_address', $arr_address['invoice']);
			if (!isset($arr_address['shipping']))
				$arr_address['shipping'] = '';
			if(isset($arrtemp['shipping_address'][0]))
				$this->set('ship_to',$arrtemp['shipping_address'][0]);

			$this->set('ref_no', $arrtemp['code']);
			if (isset($arrtemp['purchord_date']) && is_object($arrtemp['purchord_date']))
				$this->set('purchord_date', $this->opm->format_date($arrtemp['purchord_date']));
			if (isset($arrtemp['required_date']) && is_object($arrtemp['required_date']))
				$this->set('required_date', $this->opm->format_date($arrtemp['required_date']));

			$arrData['company'] = '<div style="font-size: 22px; margin-bottom: 10px;"><b>Tên nhà cung cấp:</b>'.  $arrtemp['company_name'].' - <b>ĐC: </b>'.$arr_address['invoice'].'  - <b>ĐT:</b>'. $arrtemp['phone'] .'</div>';
			$arrData['date'] = $arrtemp['purchord_date']->sec;
			$arrData['heading'] = 'Phiếu Mua Hàng '.$arrtemp['code'];

			//set content
			$date_now = date('Ymd');
			$filename = 'PUR' . $date_now . '-' . $arrtemp['code'];
			$this->set('filename', $filename);

			$this->set('heading', $heading);
			$html_cont = '';
			if (isset($arrtemp['products']) && is_array($arrtemp['products']) && count($arrtemp['products']) > 0) {
				$line = $qty_sum = $total = 0;
				$colum = 7;
				foreach ($arrtemp['products'] as $keys => $values) {
					if (!isset($values['deleted']) || !$values['deleted']) {
						$arrData['content'].= '<tr>';
						$arrData['content'].= '
						<td style="font-size: 21px ; width:8%; ">' . $values['sku'] . '</td>
						<td style="font-size: 21px; width: 42%">' . $values['products_name'] . '</td>
						<td class="right_text" style="font-size: 21px; width:6%;">' . $values['oum'] . '</td>
						<td class="right_text" style="font-size: 21px; width:5%;">' . $values['specification'] . '</td>
						<td class="right_text" style="font-size: 21px; width:5%;">' . $values['quantity'] . '</td>
						<td class="right_text" style="font-size: 21px; width:17%;">' . $this->opm->format_currency($values['prime_cost']) . '</td>
						<td class="right_text" style="font-size: 21px; width:17%;">' . $this->opm->format_currency($values['amount']) . '</td>
						';
						$arrData['content'].= '</tr>';
					} //
				}//end for

				if ($line % 2 == 0) {
					$bgs = '#fdfcfa';
					$bgs2 = '#eeeeee';
				} else {
					$bgs = '#eeeeee';
					$bgs2 = '#fdfcfa';
				}



				$sub_total = $total = $taxtotal = 0.00;
				if (isset($arrtemp['sum_sub_total']))
					$sub_total = (float) $arrtemp['sum_sub_total'];
				if (isset($arrtemp['sum_tax']))
					$taxtotal = (float) $arrtemp['sum_tax'];
				if (isset($arrtemp['sum_amount']))
					$total = (float) $arrtemp['sum_amount'];
				//Sub Total
				$arrData['sum_sub_total'] = $this->opm->format_currency($sub_total);
				$arrData['sum_tax'] = $this->opm->format_currency($taxtotal);
				$arrData['sum_amount'] = $this->opm->format_currency($total);
			}//end if

			$this->selectModel('Company');
			$company = $this->Company->select_one(array('system' => true),array('_id'));
			$this->selectModel('Salesaccount');
			$salesaccount = $this->Salesaccount->select_one(array('company_id' => $company['_id']),array('tax_no'));
			$arrData['render_path'] = '../Elements/view_pdf';
			$arrData['no_note'] = true;
			$arrData['last_table'] = '<table id="last_table" style="margin-bottom: 25px;" >
								<tr style="font-size:18px;">
									<td style="text-align:center; width: 40%; border: 1px solid #FFFFFF"></td>
									<td style="text-align:center; width: 20%;"><b>NỢ CŨ</b></td>
									<td style="text-align:center;width: 20%"><b>TOA MỚI</b></td>
									<td style="text-align:center;width: 20%"><b>TỔNG CỘNG</b></td>
								</tr>
								<tr style="font-size:25px;">
									<td style="border: 1px solid #FFFFFF"> </td>
									<td style="text-align:center" >'.number_format($no_cu).' </td>
									<td style="text-align:center">'.number_format($sum_amount) .'</td>
									<td style="text-align:center">'.($no_cu ? number_format($con_lai) : number_format($sum_amount)).' </td>
								</tr>
							</table>';

			$arrData['last_table_final'] =  '<table id="last_table_final">
									<tr style="font-size:20px" >
										<td style="text-align:center; width: 25%;">Người nhận hàng<br /> (Ký, họ tên)</td>
										<td style="text-align:center; width: 25%">Người thu tiền <br />(Ký, họ tên)</td>
										<td style="text-align:center;width: 25%">Người lập<br />(Ký, họ tên)</td>
										<td style="text-align:center;width: 25%">Trưởng đơn vị<br />(Ký, họ tên)</td>
									</tr>
									<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr style="margin-top: 25px;">
											<td></td>
											<td></td>
											<td></td>
											<td style="text-align:center;width: 25%; font-size: 20px;">Phan Thị Khuyến Hạnh</td>
									</tr>
								</table>';
			$this->render_pdf($arrData);
		}
	}

	function khuongnhi_cal_sum($products){
		$arr_sum = array('sum_sub_total' => 0,'sum_amount' => 0);
		foreach($products as $key => $value){
			if(isset($value['deleted']) && $value['deleted']) continue;
			$arr_sum['sum_sub_total'] += $value['sub_total'];
			$arr_sum['sum_amount'] += $value['amount'];
		}
		$arr_sum['sum_tax'] = $arr_sum['sum_amount'] - $arr_sum['sum_sub_total'];
		return $arr_sum;
	}

	function khuongnhi_cal_price($arr_post = array()){
		if(isset($_POST)){
			$arr_post = $_POST;
		}
		$key = $arr_post['key'];
		$name = $arr_post['name'];
		$value = $arr_post['value'];
		$query = $this->opm->select_one(array('_id' => new MongoId($this->get_id())),array('products'));
		$specification = 0;
		$line = $query['products'][$key];
		$line[$name] = $value;
		$specification = $line['specification'];
		if( !$specification && is_object($line['products_id'])){
			$this->selectModel('Product');
			$product = $this->Product->select_one(array('_id' => $query['products'][$key]['products_id']),array('specification'));
			if(!isset($product['specification']))
				$product['specification'] = 0;
			$specification = (float)$product['specification'];
		}
		$line['sub_total'] = $line['amount'] = $specification * $line['quantity'] * $line['prime_cost'];
		$line['tax'] = 0;
		$query['products'][$key] = $line;
		$query = array_merge($query,$this->khuongnhi_cal_sum($query['products']));
		$this->opm->save($query);
		echo json_encode(array(
						'sub_total' => number_format( $line['sub_total']),
						'amount' => number_format( $line['sub_total']),
						'tax' => number_format( $line['sub_total']),
						'sum_sub_total' => number_format( $query['sum_sub_total']),
						'sum_tax' => number_format( $query['sum_tax']),
						'sum_amount' => number_format( $query['sum_amount']),
			));
		die;
	}



	public function line_entry() {
		$is_text = $this->is_text;
		$subdatas = $arr_ret = array();
		$codeauto = 0;
		$opname = 'products';
		$sum_sub_total = $sum_tax = 0;
		$subdatas[$opname] = array();
		$ids = $this->get_id();
		if ($ids != '') {
			//get entry data
			$date_modified = $this->opm->select_one(array('_id'=>new MongoId($ids)),array('date_modified'));
			$prefix_cache_name = 'line_purchaseorder_'.$ids.'_';
			$cache_name = $prefix_cache_name.$date_modified['date_modified']->sec;
			$arr_ret = Cache::read($cache_name);
			if(!$arr_ret){
				$arr_ret = $this->line_entry_data($opname, $is_text);
				Cache::write($cache_name,$arr_ret);
				$old_cache = $this->get_cache_keys_diff($cache_name,$prefix_cache_name);
				foreach($old_cache as $cache){
					Cache::delete($cache);
				}
			}
			if(isset($arr_ret[$opname])){
				$minimum = $this->get_minimum_order();
				if($arr_ret['sum_sub_total']<$minimum){
					$arr_ret = $this->get_minimum_order_adjustment($arr_ret,$minimum);
				}
				$subdatas[$opname] = $arr_ret[$opname];
			}
			$query = $this->opm->select_one(array('_id'=> new MongoId($ids)),array('status'));
			if(isset($query['status']) && $query['status'] == 'Cancelled' )
				$arr_ret['sum_sub_total'] = $arr_ret['sum_tax'] = $arr_ret['sum_amount'] = 0;
		}
		$this->set('subdatas', $subdatas);
		$codeauto = $this->opm->get_auto_code('code');
		$this->set('nextcode', $codeauto);
		$this->set('file_name', 'salesorder_' . $ids);
		$this->set('sum_sub_total', $arr_ret['sum_sub_total']);
		$this->set('sum_amount', $arr_ret['sum_amount']);
		$this->set('sum_tax', $arr_ret['sum_tax']);
		$link_add_atction['option'] = 'option_list';
		$this->set('link_add_atction', $link_add_atction);
		$this->set_select_data_list('relationship', 'line_entry');
		$this->set('icon_link_id', $ids);
		$this->set('mongo_id', $ids);
	}

	public function view_pdf_return($getfile=false){
		$this->layout = 'pdf';
		$ids = $this->get_id();
		if ($ids != '') {
			$query = $this->opm->select_one(array('_id' => new MongoId($ids)));
			$arrtemp = $query;
			//pr($arrtemp);die;
			//set header
			$this->set('logo_link', 'img/logo_anvy.jpg');
			$this->set('company_address', 	'<span style="font-size: 20px; font-weight:bold">CƠ SỞ MAY KHƯƠNG NHI</span><br/>
											ĐC: 33/6 ĐƯỜNG SỐ 19, P5, GÒ VẤP<br />
											<span style="font-weight:bold">SHOP KHƯƠNG NHI</span><br />
											ĐC: 5 ĐINH TIÊN HOÀNG, P3, BÌNH THẠNH - ĐT 08. <span style="font-weight:bold">35171589</span> <br />
											Liên hệ trực tiếp: <span style="font-weight:bold">KHƯƠNG NHI 0903 681 447</span><br />
											Chăm sóc khách hàng: <span style="font-weight:bold">091 88 44 179</span>');

			//customer address

			$arradd = array('invoice', 'shipping');
			foreach ($arradd as $vvs) {
				$kk = $vvs;
				$customer_address = '';
				if (isset($arrtemp[$kk . '_address']) && isset($arrtemp[$kk . '_address'][0]) && count($arrtemp[$kk . '_address']) > 0) {
					$temp = $arrtemp[$kk . '_address'][0];
					if (isset($temp[$kk . '_address_1']) && $temp[$kk . '_address_1'] != '')
						$customer_address .= $temp[$kk . '_address_1'] . ', ';
					if (isset($temp[$kk . '_address_2']) && $temp[$kk . '_address_2'] != '')
						$customer_address .= $temp[$kk . '_address_2'] . ' ';
					if (isset($temp[$kk . '_address_3']) && $temp[$kk . '_address_3'] != '')
						$customer_address .= $temp[$kk . '_address_3'];
					else
						$customer_address .= '';
					if (isset($temp[$kk . '_town_city']) && $temp[$kk . '_town_city'] != '')
						$customer_address .= $temp[$kk . '_town_city'];

					if (isset($temp[$kk . '_province_state']))
						$customer_address .= ' ' . $temp[$kk . '_province_state'] . ' ';
					else if (isset($temp[$kk . '_province_state_id']) && isset($temp[$kk . '_country_id'])) {
						$keytemp = $temp[$kk . '_province_state_id'];
						$provkey = $this->province($temp[$kk . '_country_id']);
						if (isset($provkey[$temp]))
							$customer_address .= ' ' . $provkey[$temp] . ' ';
					}
					$arr_address[$kk] = $customer_address;
				}
			}


			if (isset($arrtemp['name']) && $arrtemp['name'] != '') {
				$heading = $arrtemp['name'];
			} else {
				$heading = '';
			}

			if ($arrtemp['ship_to_contact_name']) {
				$ship_to_contact_name = $arrtemp['ship_to_contact_name'] . '<br>';
			} else {
				$ship_to_contact_name = '';
			}
			$this->set('ship_to_contact_name', $ship_to_contact_name);

			/*if(isset($arr_address['shipping']))
				$this->set('shipping_address', $arr_address['shipping']);*/
			if (!isset($arr_address['shipping']))
				$arr_address['shipping'] = '';
			if(isset($arrtemp['shipping_address'][0]))
				$this->set('ship_to',$arrtemp['shipping_address'][0]);

			$this->set('ref_no', $arrtemp['code']);
			if (isset($arrtemp['purchord_date']) && is_object($arrtemp['purchord_date']))
				$this->set('purchord_date', $this->opm->format_date($arrtemp['purchord_date']));
			if (isset($arrtemp['required_date']) && is_object($arrtemp['required_date']))
				$this->set('required_date', $this->opm->format_date($arrtemp['required_date']));

			//set content
			$date_now = date('Ymd');
			$filename = 'PUR' . $date_now . '-' . $arrtemp['code'];
			$this->set('filename', $filename);

			$this->set('heading', $heading);
			$html_cont = '';
			if (isset($arrtemp['products']) && is_array($arrtemp['products']) && count($arrtemp['products']) > 0) {
				$line = $qty_sum = $total = 0;
				$colum = 7;
				$amount = 0;
				$total_amount = 0;
				foreach ($arrtemp['products'] as $keys => $values) {
					$return_total = 0;
					if(!empty($values['return_item'])){
						foreach($values['return_item'] as $item){
							$return_total += $item['return_quantity'];
						}
						$amount = $values['specification'] * $values['sell_price'] * $return_total;
						$total_amount += $amount;
						$html_cont .= '<tr style="background-color: #FFFFFF; font-size:12px ">';
						$html_cont .= '<td>'.(isset($values['sku']) ? $values['sku'] : '') .'</td>';
						$html_cont .= '<td>'.(isset($values['products_name']) ? $values['products_name'] : '') .'</td>';
						$html_cont .= '<td style="text-align: center;">'.(isset($values['oum']) ? $values['oum'] : '') .'</td>';
						$html_cont .= '<td style="text-align: center;">'.(isset($values['specification']) ? $values['specification'] : '') .'</td>';
						$html_cont .= '<td style="text-align: right;">'.(isset($return_total) ? $return_total : '') .'</td>';
						$html_cont .= '<td style="text-align: right;">'.(isset($values['sell_price']) ? $this->opm->format_currency($values['sell_price']) :'').'</td>';
						$html_cont .= '<td style="text-align: right;">'.(isset($values['amount']) ? $this->opm->format_currency($amount) : '') .'</td>';
						$html_cont .= '</tr>';
						$line++;
						}
					}//end if deleted
				}//end for


				if ($line % 2 == 0) {
					$bgs = '#fdfcfa';
					$bgs2 = '#eeeeee';
				} else {
					$bgs = '#eeeeee';
					$bgs2 = '#fdfcfa';
				}

				$sub_total = $total = $taxtotal = 0.00;
				if (isset($arrtemp['sum_sub_total']))
					$sub_total = (float) $arrtemp['sum_sub_total'];
				if (isset($arrtemp['sum_tax']))
					$taxtotal = (float) $arrtemp['sum_tax'];
				if (isset($arrtemp['sum_amount']))
					$total = (float) $arrtemp['sum_amount'];
				//Sub Total
				//Total
				$html_cont .= '<tr style="background-color:' . $bgs . ';">
									<td colspan="' . ($colum - 1) . '" align="center" style="font-weight:bold;" class="first bottom">Tổng cộng:</td>
									<td align="right" class="end bottom">' . $this->opm->format_currency($total_amount) . '</td>
							   </tr>';
			}//end if


			$last_table =  '<table id="last_table">
								<tr >
									<td style="text-align:center; width: 55%; border: 1px solid #FFFFFF"></td>
									<td style="text-align:center; width: 15%;"><b>NỢ CŨ</b></td>
									<td style="text-align:center;width: 15%"><b>TOA TRẢ</b></td>
									<td style="text-align:center;width: 15%"><b>CÒN LẠI</b></td>
								</tr>
								<tr>
									<td style="border: 1px solid #FFFFFF"> </td>
									<td > </td>
									<td> </td>
									<td> </td>
								</tr>
							</table>';
			$last_table_final =  '<table>
								<tr style="font-size:15px" >
									<td style="text-align:center; width: 25%;">Người nhận hàng<br /> (Ký, họ tên)</td>
									<td style="text-align:center; width: 25%">Người thu tiền <br />(Ký, họ tên)</td>
									<td style="text-align:center;width: 25%">Người lập<br />(Ký, họ tên)</td>
									<td style="text-align:center;width: 25%">Trưởng đơn vị<br />(Ký, họ tên)</td>
								</tr>
								<tr>
									<td ></td>
									<td> </td>
									<td> </td>
									<td> </td>
								</tr>
							</table>';
			$this->set('last_table_final',$last_table_final);
			$this->set('last_table',$last_table);

			$info_data = (object)array();
			$info_data->date = $this->opm->format_date($arrtemp['purchord_date']);
			$info_data->phone = isset($arrtemp['phone']) ? $arrtemp['phone'] : '';
			$info_data->contact_name = $arrtemp['company_name'];
			$info_data->current_day = date('H:i d-m-y');
			$this->set('info_data', $info_data);
			$this->set('html_cont', $html_cont);
			if (isset($arrtemp['our_rep'])) {
				$this->set('user_name', ' ' . $arrtemp['our_rep']);
			} else
				$this->set('user_name', ' ' . $this->opm->user_name());
			//end set content
			//set footer
			 if($_SESSION['default_lang']=='vi'){
				$this->render('view_pdf_return');
			}
			if($getfile)
				return $filename.'.pdf';
			$this->redirect('/upload/' . $filename . '.pdf');
		die;
	}

	public function lists(){
		$this->set('_controller',$this);
		$this->selectModel('Purchaseorder');
		$limit = LIST_LIMIT;
		$skip = 0;
		$sort_field = 'purchord_date';
		$sort_type = -1;
		if( isset($_POST['sort']) && strlen($_POST['sort']['field']) > 0 ){
			if( $_POST['sort']['type'] == 'desc' ){
				$sort_type = -1;
			}
			$sort_field = $_POST['sort']['field'];
			$this->Session->write('Purchaseorders_lists_search_sort', array($sort_field, $sort_type));

		}elseif( $this->Session->check('Purchaseorders_lists_search_sort') ){
			$session_sort = $this->Session->read('Purchaseorders_lists_search_sort');
			$sort_field = $session_sort[0];
			$sort_type = $session_sort[1];
		}
		$arr_order = array($sort_field => $sort_type);
		$this->set('sort_field', $sort_field);
		$this->set('sort_type', ($sort_type === 1)?'asc':'desc');

		// dùng cho điều kiện
		$cond = $where_query = $this->arr_search_where();
		if(isset($_POST['search'])){
			if(isset($_POST['code'])){
				$cond = array("code"=>intval($_POST['code']));
			}else{
				$arr = array();
				if(isset($_POST['company_id'])){
					$arr['company_id'] = new MongoId($_POST['company_id']);
				}
				if(isset($_POST['purchord_date'])){
					$arr['purchord_date'] = array(
					                              		'$gte'=> new MongoDate($_POST['purchord_date']),
					                              		'$lt'=> new MongoDate($_POST['purchord_date']+86400)
					                              );
				}
				if(isset($_POST['purchase_orders_status'])){
					$arr['purchase_orders_status'] = $_POST['purchase_orders_status'];
				}
				$cond = $arr;
			}
		}

		// dùng cho phân trang
		$page_num = 1;
		if( isset($_POST['pagination']) && $_POST['pagination']['page-num'] > 0){
			$page_num = $_POST['pagination']['page-num'];
			$limit = $_POST['pagination']['page-list'];
			$skip = $limit*($page_num - 1);
		}
		$this->set('page_num', $page_num);
		$this->set('limit', $limit);

		// query
		$arr_orders = $this->Purchaseorder->select_all(array(
			'arr_where' => $cond,
			'arr_order' => $arr_order,
			'arr_field'	=> array('code','company_name','company_id','contact_name','contact_id','purchord_date','payment_due_date','heading','quotation_id','job_number','job_id','sum_sub_total','purchase_orders_status','sum_amount','sum_amount_interest'),
			'limit' => $limit,
			'skip' => $skip
		));
		$this->set('arr_orders', $arr_orders);

		$arr_company = array();
		foreach ($arr_orders as $key => $value) {
			$arr_company[(string)$value['company_id']] = $value['company_name'];
		}
		$arr_company = array_unique($arr_company);
		asort($arr_company);
		$this->set('arr_company', $arr_company);

		$arr_date = array();
		foreach ($arr_orders as $key => $value) {
			$date = date('m/d/Y',$value['purchord_date']->sec);
			$arr_date[] = strtotime((string)$date);
		}
		$arr_date = array_unique($arr_date);
		// pr($arr_date);
		// die;

		arsort($arr_date);
		$this->set('arr_date', $arr_date);

		$total_page = $total_record = $total_current = 0;
		if( is_object($arr_orders) ){
			$total_current = $arr_orders->count(true);
			$total_record = $arr_orders->count();
			if( $total_record%$limit != 0 ){
				$total_page = floor($total_record/$limit) + 1;
			}else{
				$total_page = $total_record/$limit;
			}
		}
		$this->set('total_current', $total_current);
		$this->set('total_page', $total_page);
		$this->set('total_record', $total_record);


		if ($this->request->is('ajax')) {
			$this->render('lists_ajax');
		}
		$this->set('sum', $total_record);
	}


}