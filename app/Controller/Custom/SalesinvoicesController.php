<?php
App::uses('SalesinvoicesController', 'Controller');

class SalesinvoicesControllerCustom extends SalesinvoicesController {
	public function beforeFilter() {
		parent::beforeFilter();
        //12212
	}
	public function view_pdf($getfile=false,$type='',$ids = '') {
		$this->layout = 'pdf';
		$info_data = (object) array();
		if($ids == '')
			$ids = $this->get_id();
		else
			$this->module_id = $ids;
		if ($ids != '') {
			$query = $this->opm->select_one(array('_id' => new MongoId($ids)));
			$arrtemp = $query;
			//set header
			$this->set('logo_link', 'img/logo_anvy.jpg');
			$this->set('company_address', 'Số 5 Đinh Tiên Hoàng<br/>P.3, Bình Thạnh<br />TP. Hồ Chí Minh');

			//customer address
			$customer = '';
			if (isset($arrtemp['company_id']) && strlen($arrtemp['company_id']) == 24)
				$customer .= '<b>' . $this->get_name('Company', $arrtemp['company_id']) . '</b><br />';
			else if (isset($arrtemp['company_name']))
				$customer .= '<b>' . $arrtemp['company_name'] . '</b><br />';
			if (isset($arrtemp['contact_id']) && strlen($arrtemp['contact_id']) == 24)
				$customer .= $this->get_name('Contact', $arrtemp['contact_id']) . '<br />';
			else if (isset($arrtemp['contact_name']))
				$customer .= $arrtemp['contact_name'] . '<br />';

			//loop 2 address
			$arradd = array('invoice','shipping');
			foreach ($arradd as $vvs) {
				$kk = $vvs;
				$customer_address = '';
				if (isset($arrtemp[$kk . '_address']) && isset($arrtemp[$kk . '_address'][0]) && count($arrtemp[$kk . '_address']) > 0) {
					$temp = $arrtemp[$kk . '_address'][0];
					if (isset($temp[$kk . '_address_1']) && $temp[$kk . '_address_1'] != '')
						$customer_address .= $temp[$kk . '_address_1'] . ', ';
					if (isset($temp[$kk . '_address_2']) && $temp[$kk . '_address_2'] != '')
						$customer_address .= $temp[$kk . '_address_2'] . ', ';
					if (isset($temp[$kk . '_address_3']) && $temp[$kk . '_address_3'] != '')
						$customer_address .= $temp[$kk . '_address_3'] . '<br />';
					else
						$customer_address .= '<br />';
					if (isset($temp[$kk . '_town_city']) && $temp[$kk . '_town_city'] != '')
						$customer_address .= $temp[$kk . '_town_city'].', ';

					if (isset($temp[$kk . '_province_state']))
						$customer_address .= ' ' . $temp[$kk . '_province_state'] . ' <br/>';
					else if (isset($temp[$kk . '_province_state_id']) && isset($temp[$kk . '_country_id'])) {
						$keytemp = $temp[$kk . '_province_state_id'];
						$provkey = $this->province($temp[$kk . '_country_id']);
						if (isset($provkey[$temp]))
							$customer_address .= ' ' . $provkey[$temp] . ', ';
					}


					if (isset($temp[$kk . '_zip_postcode']) && $temp[$kk . '_zip_postcode'] != '')
						$customer_address .= $temp[$kk . '_zip_postcode'].'<br/>';

					if (isset($temp[$kk . '_country']) && isset($temp[$kk . '_country_id']) && $temp[$kk . '_country_id'] != "CA")
						$customer_address .= ' ' . $temp[$kk . '_country'] . '<br />';
					else
						$customer_address .= '<br />';
					$arr_address[$kk] = $customer_address;
				}
			}


			if (isset($arrtemp['heading']) && $arrtemp['heading'] != '')
				$heading = $arrtemp['heading'];
			else
				$heading = '';
			if (!isset($arr_address['invoice']))
				$arr_address['invoice'] = '';
			$this->set('customer_address', $customer . $arr_address['invoice']);
			if (!isset($arr_address['shipping']))
				$arr_address['shipping'] = '';

			$this->set('shipping_address', $arr_address['shipping']);
			$this->set('ref_no', $arrtemp['code']);
			if(isset($arrtemp['shipping_address'][0]['shipping_contact_name']))
				$this->set('ship_to',$arrtemp['shipping_address'][0]['shipping_contact_name']);
			$info_data->contact_name = $arrtemp['contact_name'];
			$info_data->no = $arrtemp['code'];
			$info_data->job_no = $arrtemp['job_number'];
			$info_data->date = $this->opm->format_date($arrtemp['invoice_date']);
			$info_data->po_no = $arrtemp['customer_po_no'];
			$info_data->ac_no = '';
			$info_data->terms = ($arrtemp['payment_terms']!= 0 ? $arrtemp['payment_terms'] : '<span style="color: red; font-weight: bold;">DUE ON RECEIPT</span>');
			$info_data->due_date = $this->opm->format_date($arrtemp['payment_due_date']);

			$this->set('info_data', $info_data);
			/*             * Nội dung bảng giá */
			$date_now = date('Ymd');
			$numkey = explode("-",$info_data->no);
			$filename = 'INV-'.$numkey[count($numkey)-1];
			$other_comment = '';
			if(isset($arrtemp['other_comment']))
				$other_comment = str_replace("\n","<br />",'<br />'.$arrtemp['other_comment']);
			$this->set('other_comment',$other_comment);
			$this->set('filename', $filename);
			$this->set('heading', $heading);
			$html_cont = '';
			$line_entry_data = $this->line_entry_data();
			if($query['invoice_type'] != 'Credit'){
				$minimum = $this->get_minimum_order();
				if($arrtemp['sum_sub_total']<$minimum){
	                $more_sub_total = $minimum - (float)$arrtemp['sum_sub_total'];
	                if(isset($line_entry_data['products']) && !empty($line_entry_data['products'])){
	                    $last_insert = end($line_entry_data['products']);
	                    foreach($last_insert as $key=>$value){
	                        if($key=='deleted' || $key == 'taxper') continue;
	                        $last_insert[$key] = '';
	                    }
	                }
	                $last_insert['_id'] = -1;
	                $last_insert['products_name'] = 'Minimum Order Adjustment';
	                $last_insert['quantity'] = '1';
	                if(!isset($last_insert['taxper']))
	                        $last_insert['taxper']= 0;
	                $last_insert['custom_unit_price'] = $last_insert['sub_total'] = $more_sub_total;
	                $last_insert['tax'] = $last_insert['sub_total']*$last_insert['taxper']/100;
	                $last_insert['amount'] = $last_insert['sub_total']+$last_insert['tax'];
	                array_push($line_entry_data['products'], $last_insert);
	                $arrtemp['sum_sub_total']+=$more_sub_total;
	                $arrtemp['sum_tax']+=$last_insert['tax'];
	                $arrtemp['sum_amount']+=$last_insert['amount'];
	            }
        	}
			if (isset($line_entry_data['products']) && is_array($line_entry_data['products']) && count($line_entry_data['products']) > 0) {
				$line = 0;
				$colum = 7;
				$options = array();
				if(isset($arrtemp['options']) && !empty($arrtemp['options']) )
					$options = $arrtemp['options'];
				if($type == 'group'){
					$arr_price = array();
                    foreach($line_entry_data['products'] as $product){
                        if(!isset($product['option_for'])) continue;
                        if(!isset($product['same_parent']) || $product['same_parent'] == 1) continue;
                        if (!isset($values['custom_unit_price']))
                            $product['custom_unit_price'] = (isset($product['unit_price']) ? $product['unit_price'] : 0);
                        if(!isset($arr_price[$product['option_for']]))
                            $arr_price[$product['option_for']]['unit_price'] = $arr_price[$product['option_for']]['sub_total'] = 0;
                        $arr_price[$product['option_for']]['unit_price'] += $product['custom_unit_price'];
                        $arr_price[$product['option_for']]['sub_total'] += $product['sub_total'];
                    }
					foreach ($line_entry_data['products'] as $values) {
						$keys = $values['_id'];
                        if (!$values['deleted']) {
                            if ($line % 2 == 0)
                                $bgs = '#fdfcfa';
                            else
                                $bgs = '#eeeeee';
                            //code
                            $html_cont .= '<tr style="background-color:' . $bgs . ';"><td class="first">';
                            if(isset($values['option_for'])&&is_numeric($values['option_for'])){
                            	$values['sku'] = '';
                            	$values['products_name'] = '&nbsp;&nbsp;&nbsp;•'.$values['products_name'];
                            }
                             if (isset($values['sku']))
                                $html_cont .= '  ' . $values['sku'];
                            else
                                $html_cont .= '  #' . $keys;
                            //desription
                            $html_cont .= '</td><td>';
                            if (isset($values['products_name']))
                                $html_cont .= str_replace("\n", "<br />", $values['products_name']);
                            else
                                $html_cont .= 'Empty';

							//clear các dòng phía sau nếu là same product parent
							if(isset($values['same_parent']) && $values['same_parent']==1){
								$html_cont .= '</td><td></td><td></td><td></td><td></td><td class="end"></td></tr>';
								 $line++;
								continue;
							}


							//width
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['sizew']) && $values['sizew'] != '' && isset($values['sizew_unit']) && $values['sizew_unit'] != '')
                                $html_cont .= $values['sizew'] . ' (' . $values['sizew_unit'] . ')';
                            else if (isset($values['sizew']) && $values['sizew'] != '')
                                $html_cont .= $values['sizew'] . ' (in.)';
                            else
                                $html_cont .= '';
                            //height
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['sizeh']) && $values['sizeh'] != '' && isset($values['sizeh_unit']) && $values['sizeh_unit'] != '')
                                $html_cont .= $values['sizeh'] . ' (' . $values['sizeh_unit'] . ')';
                            else if (isset($values['sizeh']) && $values['sizeh'] != '')
                                $html_cont .= $values['sizeh'] . ' (in.)';
                            else
                                $html_cont .= '';
                            if($values['_id']!=-1&&isset($values['same_parent']) && $values['same_parent']==0){
                                $html_cont .= '</td><td></td><td align="right">'.$values['quantity'].'</td><td class="end"></td></tr>';
                                $line++;
                                continue;
                            }
							if (!isset($values['custom_unit_price']))
                                $values['custom_unit_price'] = (isset($values['unit_price']) ? $values['unit_price'] : 0);
                            if(isset($arr_price[$values['_id']])){
                                $values['custom_unit_price'] += $arr_price[$values['_id']]['unit_price'];
                                $values['sub_total'] = str_replace(',', '', (string)$values['sub_total']);
                                $values['sub_total'] += $arr_price[$values['_id']]['sub_total'];
                            }
                            //Unit price
                            $html_cont .= '</td><td align="right">';
                            if(isset($arr_price[$values['_id']]))
                                $html_cont .= $this->opm->format_currency($values['sub_total'] / $values['quantity']);
                            else
                                $html_cont .= $this->opm->format_currency($values['custom_unit_price']);
                            //Qty
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['quantity']))
                                $html_cont .= $values['quantity'];
                            else
                                $html_cont .= '';
                            //line sub_total
                            $html_cont .= '</td><td align="right" class="end">';
                            if (isset($values['sub_total']))
                                $html_cont .= $this->opm->format_currency($values['sub_total']);
                            else
                                $html_cont .= '';
                            $html_cont .= '</td></tr>';
                            $line++;
                        }//end if deleted
                    }//end for
				} else {
					foreach ($line_entry_data['products'] as $values) {
						$keys = $values['_id'];
                        if (!isset($values['deleted']) || !$values['deleted']) {
                            if ($line % 2 == 0)
                                $bgs = '#fdfcfa';
                            else
                                $bgs = '#eeeeee';
                            //code
                            $html_cont .= '<tr style="background-color:' . $bgs . ';"><td class="first">';
                            if(isset($values['option_for'])&&is_numeric($values['option_for'])){
                            	$values['sku'] = '';
                            	$values['products_name'] = '&nbsp;&nbsp;&nbsp;•'.$values['products_name'];
                            }
                             if (isset($values['sku']))
                                $html_cont .= '  ' . $values['sku'];
                            else
                                $html_cont .= '  #' . $keys;
                            //desription
                            $html_cont .= '</td><td>';
                            if (isset($values['products_name']))
                                $html_cont .= str_replace("\n", "<br />", $values['products_name']);
                            else
                                $html_cont .= 'Empty';

							//clear các dòng phía sau nếu là same product parent
							if(isset($values['same_parent']) && $values['same_parent']==1){
								$html_cont .= '</td><td></td><td></td><td></td><td></td><td class="end"></td></tr>';
								 $line++;
								continue;
							}


							//width
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['sizew']) && $values['sizew'] != '' && isset($values['sizew_unit']) && $values['sizew_unit'] != '')
                                $html_cont .= $values['sizew'] . ' (' . $values['sizew_unit'] . ')';
                            else if (isset($values['sizew']) && $values['sizew'] != '')
                                $html_cont .= $values['sizew'] . ' (in.)';
                            else
                                $html_cont .= '';
                            //height
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['sizeh']) && $values['sizeh'] != '' && isset($values['sizeh_unit']) && $values['sizeh_unit'] != '')
                                $html_cont .= $values['sizeh'] . ' (' . $values['sizeh_unit'] . ')';
                            else if (isset($values['sizeh']) && $values['sizeh'] != '')
                                $html_cont .= $values['sizeh'] . ' (in.)';
                            else
                                $html_cont .= '';
                            //Unit price
                            $html_cont .= '</td><td align="right">';
							if (!isset($values['custom_unit_price']))
                                $values['custom_unit_price'] = (isset($values['unit_price']) ? $values['unit_price'] : 0);
                            $html_cont .= $this->opm->format_currency($values['custom_unit_price']);
                            //Qty
                            $html_cont .= '</td><td align="right">';
                            if (isset($values['quantity']))
                                $html_cont .= $values['quantity'];
                            else
                                $html_cont .= '';
                            //line sub_total
                            $html_cont .= '</td><td align="right" class="end">';
                            if (isset($values['sub_total']))
                                $html_cont .= $this->opm->format_currency($values['sub_total']);
                            else
                                $html_cont .= '';
                            $html_cont .= '</td></tr>';
                            $line++;
                        }//end if deleted
                    }//end for
				}

				if ($line % 2 == 0) {
					$bgs = '#fdfcfa';
					$bgs2 = '#eeeeee';
				} else {
					$bgs = '#eeeeee';
					$bgs2 = '#fdfcfa';
				}

				$sub_total = $total = $taxtotal = 0.00;
				if (isset($arrtemp['sum_sub_total']))
					$sub_total = $arrtemp['sum_sub_total'];
				if (isset($arrtemp['sum_tax']))
					$taxtotal = $arrtemp['sum_tax'];
				if (isset($arrtemp['sum_amount']))
					$total = $arrtemp['sum_amount'];
				 if($_SESSION['default_lang']=='vi'){
					$sub_total_label = 'Tổng trước thuế';
					$hst_gst = 'Thuế';
					$total_label = 'Tổng sau thuế';
				 } else {
					$sub_total_label = 'Sub total';
					$hst_gst = 'HST/GST';
					$total_label = 'Total';
				 }
				//Sub Total
				$html_cont .= '<tr style="background-color:' . $bgs . ';">
									<td colspan="' . ($colum - 1) . '" align="right" style="font-weight:bold;border-top:2px solid #aaa;" class="first">'.$sub_total_label.':</td>
									<td align="right" style="border-top:2px solid #aaa;" class="end">' . $this->opm->format_currency($sub_total) . '</td>
							   </tr>';
				//GST
				$html_cont .= '<tr style="background-color:' . $bgs2 . ';">
									<td colspan="' . ($colum - 1) . '" align="right" style="font-weight:bold;" class="first">'.$hst_gst.':</td>
									<td align="right" class="end">' . $this->opm->format_currency($taxtotal) . '</td>
							   </tr>';
				//Total
				$html_cont .= '<tr style="background-color:' . $bgs . ';">
									<td colspan="' . ($colum - 1) . '" align="right" style="font-weight:bold;" class="first bottom">'.$total_label.':</td>
									<td align="right" class="end bottom">' . $this->opm->format_currency($total) . '</td>
							   </tr>';
			}//end if
			$this->selectModel('Company');
			$company = $this->Company->select_one(array('system' => true),array('_id'));
			$this->selectModel('Salesaccount');
			$salesaccount = $this->Salesaccount->select_one(array('company_id' => $company['_id']),array('tax_no'));
			if(!isset($salesaccount['tax_no']))
				$salesaccount['tax_no'] = '';
			$this->set('type',$type);
			$this->set('tax',$salesaccount['tax_no']);
			$this->set('html_cont', $html_cont);
			$this->set('credit', ($query['invoice_type'] == 'Credit' ? true : false) );
			if (isset($arrtemp['our_csr'])) {
				$this->set('user_name', ' ' . $arrtemp['our_csr']);
			} else
				$this->set('user_name', ' ' . $this->opm->user_name());
			//end set content
			//set footer
			$this->render('view_pdf');
			if($getfile)
				return $filename.'.pdf';
			$this->redirect('/upload/' . $filename . '.pdf');
		}
		die;
	}
}