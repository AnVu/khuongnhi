var page = require('webpage').create(),
system = require('system'),
address, output, size, orientation;
orientation = 'portrait';

address = system.args[1];
output = system.args[2];
page.viewportSize = { width: 600, height: 600 };
if(system.args.length>4 && system.args[4]!=''){
    orientation = system.args[4];
    if(system.args[4]!='landscape')
        orientation = 'portrait';
}
console.log(orientation);
var customFooter = {};
customFooter["order"] = '<li class="footer_li">Ghi chú: Hàng lỗi Quý Khách đổi/trả trong 3 tháng. Hàng không vừa ý Quý Khách được đổi trong 3 ngày (hàng còn nguyên ri, không dơ, không mất thẻ bài), không hoàn lại tiền.</li>';
if (system.args.length > 3 && system.args[2].substr(-4) === ".pdf") {
    size = system.args[3].split('*');
    var footerString;
    if( system.args[5] != undefined &&  system.args[5] != "" ) {
        footerString = customFooter[system.args[5]];
    } else {
        footerString = ' ';
    }
    footerString = '<div class="footer">' +
                        '<ul class="footer_ul" style="">' +
                            footerString +
                            '<li style="list-style: none; float: right;">Trang [PACGE_NUM]</li>' +
                        '</ul>' +
                    '</div>';
    if(orientation == 'portrait')
            var setting = {  format: system.args[3],
                        orientation: orientation,
                        margin: '0.3cm',
                        marginTop: '0.3cm',
                        width:  '8.5in',
                        height: '11in',
                        headerHeight : "25px"
                    };
    if(orientation == 'landscape')
            var setting = {  format: system.args[3],
                        orientation: orientation,
                        margin: '0.3cm',
                        marginTop: '0.3cm',
                        height:  '8.5in',
                        width: '11in',
                        headerHeight : "25px"
                    };
    if( system.args[5] != undefined && system.args[5] == 'no_footer' ) {
        setting["footer"] = {
                            height: "0.6cm",
                            contents: phantom.callback(function(pageNum, numPages) {
                                if( pageNum == 2 ) {
                                    page.evaluate(function() {
                                      return $('thead').append('<tr style="height:1px">');
                                    });
                                }
                                return pageNum;
                            })
                        };
    } else {
        setting["footer"] = {
                                height: "0.6cm",
                                contents: phantom.callback(function(pageNum, numPages) {
                                    page.evaluate(function() {
                                      return $('thead').append('<tr style="height:1px">');
                                    });
                                    return replaceCssWithComputedStyle(footerString.replace('[PACGE_NUM]', pageNum));
                                })
                            };
    }
    page.paperSize = size.length === 2 ? {  width: size[0], height: size[1], margin: '1cm'}
                                       : setting;
}
page.open(address, {encoding: "utf8"}, function (status) {
    if (status !== 'success') {
        console.log('Unable to load the address!');
        phantom.exit();
    } else {
        window.setTimeout(function () {
            page.injectJs('../default/js/jquery-1.10.2.min.js');
            page.render(output);
            console.log('ok');
            phantom.exit();
        }, 200);
    }
});

function replaceCssWithComputedStyle(html) {
  return page.evaluate(function(html) {
    var host = document.createElement('div');
    host.setAttribute('style', 'display:none;'); // Silly hack, or PhantomJS will 'blank' the main document for some reason
    host.innerHTML = html;

    // Append to get styling of parent page
    document.body.appendChild(host);

    var elements = host.getElementsByTagName('*');
    // Iterate in reverse order (depth first) so that styles do not impact eachother
    for (var i = elements.length - 1; i >= 0; i--) {
      elements[i].setAttribute('style', window.getComputedStyle(elements[i], null).cssText);
    }

    // Remove from parent page again, so we're clean
    document.body.removeChild(host);
    return host.innerHTML;
  }, html);
}
