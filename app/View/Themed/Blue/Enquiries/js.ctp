<?php //echo $this->element('js_entry');?>
<?php echo $this->element('js/permission_entry'); ?>
<script type="text/javascript">
$(function(){

	enquiries_update_entry_header();
});

function enquiries_update_entry_header(){
	$("#enquiry_company_header").html($("#EnquiryCompany").val());

	if( $.trim($("#EnquiryContactName").val()) != "" )
		$("#enquiry_contact_header").html(" - " + $("#EnquiryContactName").val());

	if( $.trim($("#EnquiryStatus").val()) != "" )
		$("#enquiry_status_header").html("Status: " + $("#EnquiryStatus").val());

	if( $.trim($("#EnquiryOurRep").val()) != "" )
		$("#enquiry_responsible_header").html(" | " + $("#EnquiryOurRep").val());
}

function enquiries_auto_save_entry(object){
	if( $.trim($("#EnquiryHeading").val()) == "" ){
		$("#EnquiryHeading").val( "#" + $("#EnquiryNo").val() + "-" + $("#EnquiryEnquiryName").val());
	}

	if( $.trim($("#EnquiryWeb").val()).length > 0 ){
		var web = $("#EnquiryWeb").val();
		if( web.substring(0, 7) != "http://" ){
			web = "http://" + web;
		}
		$("#enquiries_web").attr("href", web);
	}

	enquiries_update_entry_header();

	$("form :input", "#enquiries_form_auto_save").removeClass('error_input');

	$.ajax({
		url: '<?php echo URL; ?>/enquiries/auto_save',
		timeout: 15000,
		type:"post",
		data: $("form", "#enquiries_form_auto_save").serialize(),
		success: function(html){
			if(html == "ref_no"){
				$("#EnquiryNo").addClass('error_input');
				alerts('Message', 'This "no" existed, please choose another');

			}else if( html != "ok" ){
				alerts('Message', html);
			}
			console.log(html); // view log when debug
		}
	});
}

function after_choose_addresses(ids,names,keys){
		var address = new Object();
		var directs = ['name','address_1','address_2','address_3','town_city','province_state','province_state_id','zip_postcode','country'];
		for(var n in directs){
			address[keys+'_'+directs[n]] = $("#window_popup_addresses_"+names+"_"+directs[n]+'_'+ids+keys).val();
		}
		address[keys+'_country_id'] = $("#window_popup_addresses_"+names+"_country_id_"+ids+keys).val();
		address[keys+'_default'] = true;
		address['deleted'] = false;

		var address_0={'0':address};
		var invoice_address={'addresses':address_0};
		var jsonString = JSON.stringify(invoice_address);
		var arr_field = {'addresses':keys+'_address'};
		$(".k-window").fadeOut();
		save_muti_field(arr_field,jsonString,'',function(arr_ret){
			ajax_note('Saved.');
			address = arr_ret[keys+'_address'];
			address = address[0];
			for(var i in address){
				$("#"+ChangeFormatId(i)).val(address[i]);
			}
			//save tax
			var ShippingAddId = $("#ShippingProvinceStateId").val()
			if(keys=='shipping' || (keys=='invoice' && ShippingAddId=='')){
				var taxid = address[keys+'_province_state_id'];
				var allListElements = $( 'li[value="'+taxid+'"]' );
				var html = $("#tax").parent().find(allListElements);
				//console.log(taxid);
				//console.log(html);
				var tax = html[0].innerHTML;
				var taxval = tax.split("%");
				taxval = taxval[0];
				$('#tax').val(tax);
				$('#taxId').val(taxid);
				$('#tax').change();
			}
		});
}

</script>