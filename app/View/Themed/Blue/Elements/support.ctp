<?php echo $this->element('../'.$name.'/tab_option'); ?>
<div class="percent content_indent">
    <div class="float_left bg_nav_setup fix_block">
        <ul class="nav_setup support_nav">
            <li><a class="other_dif" href="#" >Help & FAQ</a></li>
        <?php
            $arr_content = array();
            if($supports->count()){
                foreach($supports as $support){
                    $arr_content[] = array('_id'=>$support['_id'],'content'=>$support['content']);
        ?>
            <li><a href="javascript:void(0)" class="faq" id="<?php echo $support['_id'] ?>"><?php echo $support['name']; ?></a></li>
        <?php
                }
            }
        ?>
            <li><a class="other_dif" href="#">Contact & Support </a></li>
            <li><a href="#" class="active">Contact Us</a></li>
            <li><a href="#" class="other_dif">Help &amp; FAQ</a></li>
            <li><a href="#">How to convert "My Collection" to an Order?</a></li>
            <li><a href="#">How to use Quick Tools -  "Add Filtered to Collecton"?</a></li>
            <li><a href="#">How to change your password?</a></li>
            <li><a href="#">How to edit an Order?</a></li>
            <li><a href="#">How to approve an order?</a></li>
            <li><a href="#">How to use My Collection?</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#" class="other_dif">Contact &amp; Support </a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">FAQ</a></li>
        </ul>
    </div>
    <div class="content_store hidden">
    <?php
        foreach($arr_content as $value){
    ?>
        <div id="content_<?php echo $value['_id'] ?>"><?php echo $value['content']; ?></div>
    <?php
        }
    ?>
    </div>
    <div class="percent_content float_left container_same_category1 h_content">
        <div class="paragr">
                        <div class="clear"><h1>How to convert "My Collection" to an Order?</h1></div>
                        <ul class="menu_left_ct">
                            <li class="list_title">category</li>
                            <li><a href="#tips_1">category - 1</a>
                                <ul class="menu_sub_left_1">
                                    <li><a href="#">hd -1</a></li>
                                    <li><a href="#">hd- 2</a></li>
                                </ul>
                            </li>
                            <li><a href="#tips_2">category - 2</a>
                                <ul class="menu_sub_left_1">
                                    <li><a href="#">hd -1</a></li>
                                    <li><a href="#">hd- 2</a></li>
                                </ul>
                            </li>
                            <li><a href="#tips_3">category -3</a></li>
                            <li><a href="#tips_4">category - 4</a></li>
                            <li><a href="#">category - 5</a></li>
                            <li><a href="#">category - 6</a></li>
                            <li><a href="#">category - 7</a></li>
                        </ul>
                        <p class="bold">After you have added products to you Collection, you can quickly order items in you Collection</p><br><br>
                        <p><span class="bold transform"><a id="tips_1" href="">Step 1:</a></span> Clicking on the "My Collection" will bring you to the "My Collection" page. Here, you can:</p>
                        <ul>
                            <li>Enter the required quantity for each item you want to order,</li>
                            <li>Check or uncheck the item you want to order, and</li>
                            <li>Select at least one or more locations you want your items to be sent to. Note: You need to have "Allocate" permission to be able to order for more than locations.</li>
                        </ul>
                        <p>After entering all required information, click the "Add to Order" button. </p>
                        <p><img alt="" src="../../images/cv_collect.png"></p><br><br>
                        <p><span class="bold transform"><a id="tips_2" href="">Step 2:</a></span> In the "Information" tab of the "Order Detail" page, you will need to enter the required Order information such as "PO number", "Order Ref", "Notes", "Date Required". You can also review your Items and Distribution before clickon "Submit Your order" to send your Order in for processing.</p>
                         <p><img alt="" src="../../images/cv_order.png"></p>
                         <p class="bold">After you have added products to you Collection, you can quickly order items in you Collection</p><br><br>
                        <p><span class="bold transform"><a id="tips_3" href="">Step 3:</a></span> Clicking on the "My Collection" will bring you to the "My Collection" page. Here, you can:</p>
                        <ul>
                            <li>Enter the required quantity for each item you want to order,</li>
                            <li>Check or uncheck the item you want to order, and</li>
                            <li>Select at least one or more locations you want your items to be sent to. Note: You need to have "Allocate" permission to be able to order for more than locations.</li>
                        </ul>
                        <p>After entering all required information, click the "Add to Order" button. </p>
                        <p><img alt="" src="../../images/cv_collect.png"></p>
                        <p><span class="bold transform"><a id="tips_4" href="">Step 4:</a></span> In the "Information" tab of the "Order Detail" page, you will need to enter the required Order information such as "PO number", "Order Ref", "Notes", "Date Required". You can also review your Items and Distribution before clickon "Submit Your order" to send your Order in for processing.</p>
                         <p><img alt="" src="../../images/cv_order.png"></p><br><br>
                    </div>
        <p class="clear"></p>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $(".faq").click(function(){
            $(".faq").removeClass("active");
            $(this).addClass("active");
            var id = $(this).attr("id");
            $(".paragr").html($("#content_"+id).html());
        });
        $(".faq:first").click().addClass("active");
    })
</script>