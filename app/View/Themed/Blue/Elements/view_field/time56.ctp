<div class="styled_select" style="margin: 0;width:40%;float:left;">
	<select name="<?php echo  'time5_hour_'.$arr_vls['_id'];?>" style="border:none;background:transparent;" id="<?php echo 'time5_hour_'.$arr_vls['_id'];?>">
		<?php for ($i=0; $i < 24; $i++) {
					$j = $i;
					if($j < 10)$j = '0'.$j;
					if($i > 7 && $i < 18){
						$str = 'BgOptionHour';
					} else {
						$str = '';
					}
		?>
					<option value="<?php echo $j; ?>:00" class="<?php echo $str; ?>" <?php if (isset($arr_vls['time5_hour']) && $arr_vls['time5_hour']==$j.':00') echo 'selected="selected"' ?>><?php echo $j; ?>:00</option>
					<option value="<?php echo $j; ?>:30" class="<?php echo $str; ?>" <?php if (isset($arr_vls['time5_hour']) && $arr_vls['time5_hour']==$j.':30') echo 'selected="selected"' ?>><?php echo $j; ?>:30</option>
		<?php }?>	
	</select>	
</div>
<div class="styled_select" style="margin: 0;width:40%;float:left;">
	<select name="<?php echo  'time6_hour_'.$arr_vls['_id'];?>" style="border:none;background:transparent;" id="<?php echo  'time6_hour_'.$arr_vls['_id'];?>">
		<?php for ($i=0; $i < 24; $i++) {
					$j = $i;
					if($j < 10)$j = '0'.$j;
					if($i > 7 && $i < 18){
						$str = 'BgOptionHour';
					} else {
						$str = '';
					}
		?>
					<option value="<?php echo $j; ?>:00" class="<?php echo $str; ?>" <?php if (isset($arr_vls['time6_hour']) && $arr_vls['time6_hour']==$j.':00') echo 'selected="selected"' ?>><?php echo $j; ?>:00</option>
					<option value="<?php echo $j; ?>:30" class="<?php echo $str; ?>" <?php if (isset($arr_vls['time6_hour']) && $arr_vls['time6_hour']==$j.':30') echo 'selected="selected"' ?>><?php echo $j; ?>:30</option>
		<?php }?>	
	</select>	
</div>