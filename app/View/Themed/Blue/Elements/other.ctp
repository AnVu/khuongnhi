<?php if($this->Common->check_permission($controller.'_@_other_tab_@_view',$arr_permission)): ?>
<div class="clear_percent" style="width:100%; margin:0px;">
	<div class="clear_percent_6a float_left">
		<div class="clear">
			<div class="clear_percent_15 float_left">
				<div class="tab_1">
				 <form id="other_record" method="POST">
					<p class="clear">
						<span class="label_1 float_left fixbor"><?php echo translate('Record entry type'); ?></span>
					<div class="width_in float_left indent_input_tp">
						<input type="text" class="input_1 float_left" name="record_type" id="record_type" value="<?php echo (isset($data['record_type'])? $data['record_type'] : ''); ?>" />
						<input type="hidden" value="<?php echo (isset($data['record_type'])? $data['record_type'] : ''); ?>" />
						<script type="text/javascript">
							$(function(){
								$("#record_type").combobox(<?php echo json_encode(array('Line entry'=>'Line entry','Production'=>'Production')); ?>);
							});
						</script>
					</div>
					<p class="clear"></p>
					</p>
					<p class="clear">
						<span class="label_1 float_left">Fax</span>
					<div class="width_in float_left indent_input_tp">
						<input class="input_1 float_left" type="text" id="fax" name="fax" value="<?php if(isset($data['fax'])) echo $data['fax'];?>">
						<span class="icon_down_pl"></span>
					</div>
					</p>
					<p class="clear">
						<span class="label_1 float_left"><?php echo translate('Heading'); ?></span>
					<div class="width_in float_left indent_input_tp">
						<input class="input_1 float_left" type="text" value="<?php if(isset($data['heading'])) echo $data['heading'];?>"  name="heading" id="heading" >
					</div>
					</p>
					<p class="clear">
						<span class="label_1 float_left"><?php echo translate('Include signature'); ?></span>
					<div class="width_in float_left indent_input_tp">
						<div class="in_active2">
							<label class="m_check2">
								<input type="checkbox" <?php if(isset($data['include_signature'])&&$data['include_signature']==1){?> checked <?php }?> id="include_signature" />
								<span class="bx_check dent_chk"></span>
								<input type="hidden" name="include_signature" class="include_signature" value="<?php echo (isset($data['include_signature'])&&$data['include_signature']==1 ? 1:0 ); ?>" />
							</label>
							<span class="inactive dent_check"></span>
							<p class="clear"></p>
						</div>
					</div>
					</p>
					<p class="clear">
						<span class="label_1 float_left"><?php echo translate('Inc. sign off section'); ?></span>
					<div class="width_in float_left indent_input_tp">
						<div class="in_active2">
							<label class="m_check2">
								<input type="checkbox" <?php if(isset($data['sign_off_section'])&&$data['sign_off_section']==1){?> checked <?php }?> id="sign_off_section" />
								<span class="bx_check dent_chk"></span>
								<input type="hidden" value="<?php echo (isset($data['sign_off_section'])&&$data['sign_off_section']==1 ? 1:0 ); ?>" class="sign_off_section" name="sign_off_section">
							</label>
							<span class="inactive dent_check"></span>
							<p class="clear"></p>
						</div>
					</div>
					</p>
					<p class="clear">
						<span class="label_1 float_left"><?php echo translate('Use own letterhead'); ?></span>
					<div class="width_in float_left indent_input_tp">
						<div class="in_active2">
							<label class="m_check2">
								<input type="checkbox"  <?php if(isset($data['own_letterhead'])&&$data['own_letterhead']==1){?> checked <?php }?> id="own_letterhead" />
								<span class="bx_check dent_chk"></span>
								<input type="hidden" value="<?php echo (isset($data['own_letterhead'])&&$data['own_letterhead']==1 ? 1:0 ); ?>" class="own_letterhead" name="own_letterhead">
							</label>
							<span class="inactive dent_check"></span>
							<p class="clear"></p>
						</div>
					</div>
					</p>
					<p class="clear">
						<span class="label_1 float_left hgt5 fixbor2" style="height:70px !important"><?php echo translate('Include images'); ?></span>
					<div class="width_in float_left indent_input_tp">
						<div class="in_active2">
							<label class="m_check2">
								<input type="checkbox" id="include_images" <?php if(isset($data['include_images'])&&$data['include_images']==1){?> checked <?php }?> />
								<span class="bx_check dent_chk"></span>
								<input type="hidden" value="<?php echo (isset($data['include_images'])&&$data['include_images']==1 ? 1:0 ); ?>" class="include_images" name="include_images" />
							</label>
							<span class="inactive dent_check"></span>
							<span class="float_left dent_check color_hidden"><?php echo translate('linked to Products module'); ?></span>
							<p class="clear"></p>
						</div>
					</div>
					</p>
					<p class="clear"></p>
					</form>
				</div><!--END Tab1 -->

			</div>
			<div class="clear_percent_14 float_left">
				<div class="tab_1 full_width">
					<span class="title_block bo_ra1">
						<span class="fl_dent"><h4><?php echo translate('Comments on'); echo ' '.$controller; ?></h4></span>
					</span>
					<form id="other_comment">
						<textarea class="area_t2"  style="height:165px;background-color: transparent"><?php if(isset($data['other_comment'])) echo $data['other_comment'];?></textarea>
					</form>
					<span class="title_block bo_ra2">
						<p class="cent"><?php echo translate('These details appear on the print / email version of the document'); ?></p>
					</span>
				</div><!--END Tab1 -->
			</div>
			<p class="clear"></p>
		</div>
		<div class="full_width block_dent9 " style="width: 143%">
			<?php echo $this->element('communications'); ?>
		</div>
	</div>
	<div class="clear_percent_7a float_left">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="float_left">
					<span class="fl_dent"><h4><?php echo translate('Commission'); ?></h4></span>
				</span>
			</span>
			<div class="tab_2_inner" id="commission">
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Name'); ?></span>
				<div class="width_in3a float_left indent_input_tp">
					<input class="input_1 float_left" id="name" type="text" value="<?php if(isset($data['name'])) echo $data['name'];?>">
					<span class="icon_down_new float_right"></span>
				</div>
				</p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Sale amount'); ?></span>
				<div class="width_in3a float_left indent_input_tp">
					<input class="input_1 float_left" id="sale_amount" type="text" value="<?php if(isset($data['sale_amount'])) echo $data['sale_amount'];?>">
				</div>
				</p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Cost of sale'); ?></span>
				<div class="width_in3a float_left indent_input_tp">
					<input class="input_1 float_left bor_active" id="cost_of_sale" type="text" value="<?php if(isset($data['cost_of_sale'])) echo $data['cost_of_sale'];?>">
				</div>
				</p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Profit for commission'); ?></span>
				<div class="width_in3a float_left indent_input_tp">
					<input class="input_1 float_left" id="profit_for_commission" type="text" value="<?php if(isset($data['profit_for_commission'])) echo $data['profit_for_commission'];?>">
				</div>
				</p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Rate'); ?></span>
				<div class="width_in3a float_left indent_input_tp">
					<div class="styled_select">
						<select id="rate">
							<option <?php if(isset($data['rate']) && $data['rate']=="Base on") echo "selected";?>><?php echo translate('Base on'); ?></option>
							<option <?php if(isset($data['rate']) && $data['rate']=="Developper") echo "selected";?> ><?php echo translate('Developper'); ?></option>
							<option <?php if(isset($data['rate']) && $data['rate']=="Production") echo "selected";?>><?php echo translate('Production'); ?></option>
							<option <?php if(isset($data['rate']) && $data['rate']=="Manuafacture") echo "selected";?> ><?php echo translate('Manuafacture'); ?></option>
						</select>
					</div>
				</div>
				</p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2"><?php echo translate('Commission amount'); ?></span>
				<div class="width_in3a float_left indent_input_tp">
					<input class="input_1 float_left" id="commission_amount" type="text" value="<?php if(isset($data['commission_amount'])) echo $data['commission_amount'];?>">
				</div>
				</p>
				<p class="clear">
					<span class="label_1 float_left minw_lab2" style="height:50%"><?php echo translate('Paid'); ?></span>
				<div class="width_in3a float_left indent_input_tp">
					<div class="in_active2">
						<label class="m_check2">
							<input type="checkbox" id="paid" <?php if(isset($data['paid'])&&$data['paid']=="true"){?> checked <?php }?> >
							<span class="bx_check dent_chk"></span>
							<input type="hidden" name="paid" class="paid" value="<?php echo (isset($data['paid'])&&$data['paid']==1 ? 1:0 ); ?>" />
						</label>
						<span class="inactive dent_check"></span>
						<p class="clear"></p>
					</div>
				</div>
				</p>
				<p class="clear"></p>
			</div>

			<span class="title_block bo_ra2"></span>
		</div><!--END Tab1 -->
	</div>
</div>
<p class="clear"></p>
<script type="text/javascript">
	<?php if($this->Common->check_permission($controller.'_@_other_tab_@_edit',$arr_permission)): ?>
	function other_tab_auto_save(id,content){
		$.ajax({
			url: "<?php echo URL; ?>/<?php echo $controller;?>/other_tab_auto_save/",
			timeout: 15000,
			type: "POST",
			data: { id: id, content : content },
			success: function(html){
				if(html != "ok"){
					alerts("Error: ", html);
				}

			}
		});
		return false;
	}
	<?php endif; ?>
    $(function(){
    	<?php if(!$this->Common->check_permission($controller.'_@_other_tab_@_edit',$arr_permission)): ?>
    	$("input,textarea,select","#load_subtab").each(function(){
    		$(this).attr('disabled','disabled');
    	});
    	$(".combobox_selector","#load_subtab").remove();
    	<?php else: ?>
    	$("#other_record :input").change(function() {
    		$(".jt_ajax_note").html("Saving...       ");
    		var id = $(this).attr('id');
    		if(id=='include_signature' || id=='sign_off_section'
    			|| id=='own_letterhead' || id=='include_images'){
    			$('.'+id).val(0);
    			if($(this).is(":checked"))
    				$('.'+id).val(1);
    		}
    		var ids = $("#mongo_id").val();
    		data = $("input","form#other_record").serialize();
			other_tab_auto_save(ids,data);
			ajax_note("Saving...Saved !");
		});
	 	$("#other_comment textarea").change(function() {
	 		$(".jt_ajax_note").html("Saving...       ");
			var ids = $("#mongo_id").val();
			var data = 'other_comment='+$(this).val();
			other_tab_auto_save(ids,data);
			ajax_note("Saving...Saved !");
		});
		$("#commission input,#commission select").change(function() {
			$(".jt_ajax_note").html("Saving...       ");
			var ids = $("#mongo_id").val();
			var id = $(this).attr('id'); 
			if (id == 'paid')
				var data = id + '=' + $('#paid').prop('checked') ;
			else 
				var data = id + '=' + $(this).val(); 
			other_tab_auto_save(ids,data);
			ajax_note("Saving...Saved !");
		});
    	<?php endif; ?>
    });

</script>
<?php endif; ?>