<script type="text/javascript">
    $(function() {
        $("form :input", "#timelog_form_auto_save").change(function() {
            timelog_auto_save_entry();
        });
        $("#time_change").change(function() {
            caldate();
        });
    });

    function caldate() {
        $.ajax({
            url: '<?php echo URL; ?>/timelogs/entry_caltime',
            timeout: 15000,
            type: "post",
            data: $("form", "#timelog_form_auto_save").serialize(),
            success: function(html) {
                $('#TimelogTotalTime').val(html);
                timelog_auto_save_entry();
            }
        });
    }

    function timelog_auto_save_entry() {
        $.ajax({
            url: '<?php echo URL; ?>/timelogs/auto_save',
            timeout: 15000,
            type: "post",
            data: $("form", "#timelog_form_auto_save").serialize(),
            success: function(html) {
                console.log(html); // view log when debug
            }
        });
    }



    function timelog_entry_delete(id) {
        confirms("Message", "Are you sure you want to delete?",
                function() {
                    $.ajax({
                        url: '<?php echo URL; ?>/timelog/delete/' + id,
                        timeout: 15000,
                        success: function(html) {
                            if (html != "ok") {
                                alerts("Error: ", html);
                            } else {
                                $("#DocUse_" + id).fadeOut();
                            }
                            console.log(html); // view log when debug
                        }
                    });
                }, function() {
            //else do somthing
        });
    }


    function tasks_update_entry_header() {
        $("#task_name_header").html($("#TaskName").val());
        $("#task_work_start_header").html($("#TaskWorkStart").val());
        $("#task_status_header").html($("#TaskStatus option[value='" + $("#TaskStatus").val() + "']").text());
        $("#task_assign_to_header").html($("#TaskOurRep").val());

        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var f_firstDate = new Date($("#TaskWorkEnd").val());
        var firstDate = new Date(f_firstDate.getFullYear(), f_firstDate.getMonth() + 1, f_firstDate.getDate());
        var f_secondDate = new Date();
        var secondDate = new Date(f_secondDate.getFullYear(), f_secondDate.getMonth() + 1, f_secondDate.getDate());

        // Math.abs(
        var diffDays = Math.round((firstDate.getTime() - secondDate.getTime()) / (oneDay));

        // var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
        if (parseInt(diffDays) < 0) {
            $("#tasks_days_left").attr("style", "color: red");

        } else {
            $("#tasks_days_left").removeAttr("style");

        }
        $("#tasks_days_left").val(diffDays);
    }
</script>

