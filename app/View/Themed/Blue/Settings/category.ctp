<style>
	.tab_tab_category{
		margin:12px;
	}

	#detail_category span{
		margin:10px auto; 
	}
</style>
<ul class="menu_control2 float_right" id='menu_category' style='margin-top:-0.5%;'>
    	   	<li>
    	   		<?php $list_category = translate('List Category');  ?>
            	<a href="javascript:void(0)" class="" onclick="show_category_detail('list_category', '<?php echo $list_category; ?>')" id='list_category'>
					<?php echo translate('List'); ?>           
				</a>
            </li> 
    	    <li>
    	    	<?php $new_category = translate('New Category');  ?>
            	<a href="javascript:void(0)" class="active" onclick="show_category_detail('entry_category','<?php echo $new_category; ?>')" id='entry_category'>
					<?php echo translate('Entry'); ?>      
				</a>
            </li>
            
</ul>
<div id="category_detail">
	<div class="tab_1 full_width">
		<span class="title_block bo_ra1">
			<span class="fl_dent"><h4></h4></span>
		</span>
		<div id='detail_category'>
			
		</div>
		<div id="branch"></div>
	</div>
</div>


<script type="text/javascript">
	$(function(){
		$("#list_category").click(); // click menu li dau tien khi page load xong
	});
	function show_category_detail(id,title){
		$('h4','.fl_dent').text(title);
		//alert(id);
		$('#menu_category a').removeClass("active");
		$('#'+id).addClass("active");
		//alert(object);
		
					$.ajax({
										url: '<?php echo URL; ?>/settings/'+id+'/',
										type:"POST",
										timeout: 15000,
										success: function(html){
											$("#detail_category").html(html);
										}
					}).done(function(){
						$('.cat_lv1').first().click();
						
					});
		

	}
</script>