<script type="text/javascript">
    $(function(){
      //  delete_message_detail_null('<?php echo $message_type; ?>');
     });
</script>
<div class="tab_1 full_width">
    <span class="title_block bo_ra1">
        <span class="fl_dent">
            <h4>
                <?php echo $message_type;?>
            </h4>
        </span>
        <a title="Add new message" href="javascript:void(0)" onclick="settings_listmenu_detail_add('<?php echo $message_type; ?>')">
            <span class="icon_down_tl top_f"></span>
        </a>

        <div class="new_select_black" style="width:30%; float:right; margin-right:8px;">
        <div class="new_select_black_left">&nbsp;</div>
        <div class="new_select_black_center" style="width:94%;">
            <input type="text" id="language_list" combobox_blank="1" value="<?php if(isset($arr_language_value)) echo $arr_language[$arr_language_value];?>" />
            <input type="hidden" id="language_listId" value="<?php if(isset($arr_language_value)) echo $arr_language_value;?>" />
            <script type="text/javascript">
                $(function () {
                    $("#language_list").combobox(<?php if(isset($arr_language)) echo json_encode($arr_language);?>);
                });
            </script>
        </div>
        <div class="new_select_black_right">&nbsp;</div>
    </div>

    </span>
    <ul class="ul_mag clear bg3">

        <li class="hg_padd" style="width:62%">Content</li>
        <li class="hg_padd" style="width:35%">Key</li>


    </ul>
    <input type="hidden" id="all_field_of_option" >
    <div class="container_same_category" style="height:449px">
        <?php
        $i = 1;
        foreach ($arr_setting as $key) {

                $i = 3 - $i;
            ?>

            <?php echo $this->Form->create('Setting', array('id' => 'SettingForm_content')); ?>

            <?php echo $this->Form->hidden('Setting._id', array('value' => $key['_id'])); ?>

            <ul class="ul_mag clear bg<?php echo $i; ?>">
                <?php
                    foreach($arr_language as $k => $v){
                        $names = 'content_'.$k;
                        if($k==DEFAULT_LANG)
                            $names = 'content';

                        $types = 'hidden';
                        if($k==$arr_language_value){
                            $types = 'text';
                    }
                 ?>
                <li class="hg_padd line_mg" style="width:62%; position: relative">
                    <?php
                    echo $this->Form->input('Setting.content', array(
                        'class' => 'input_inner input_inner_w bg' . $i,
                        'value' => isset($key[$names])?$key[$names]:'',
                        'type'  => $types,
                    ));
                    ?>
                </li>
                <?php } ?>
                <li class="hg_padd line_mg" style="width:35%; position: relative">
                    <?php
                    echo $this->Form->input('Setting.key', array(
                        'class' => 'input_inner input_inner_w bg' . $i,
                        'value' => $key['key']
                    ));
                    ?>
                </li>
            </ul>
            <?php echo $this->Form->end(); ?>
        <?php } ?>
    </div>

    <span class="title_block bo_ra2">
        <span class="float_left bt_block"><?php echo translate('Edit or create values for list'); ?>.</span>
    </span>
</div>

<script type="text/javascript">

    $(function(){

        $('.container_same_category').mCustomScrollbar({
            scrollButtons:{
                enable:false
            }
        });
    });
</script>