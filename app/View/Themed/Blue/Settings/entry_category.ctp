

<form action="javascript:void(0)" method="POST" accept-charset="utf-8" style='margin-left:40%;'>
	<input type="hidden" value="<?php if(isset($cat_choose['_id']))echo $cat_choose['_id'];?>" id="id_cat" name="" value="">
	<span>
	<?php echo translate('Name category'); ?>: <input type="text" name="name_category" id="name_category" value="<?php echo $cat_choose['name']; ?>" placeholder="Name category"/>
	</span>
	<span>
	<?php echo translate('Type category'); ?>: <select name="type_category" id="type_category" value="" placeholder="Type category">
		<option value="0" <?php if($cat_choose['is_parent']!=1) echo"selected='selected'"?> >Children</option>
		<option value="1"<?php if($cat_choose['is_parent']==1) echo"selected='selected'"?>>Parent</option>
	</select>
	</span>
	<span>
	<?php echo translate('Parent'); ?>: <select name="parent" id="parent" value="" check="<?php if(isset($cat_choose['parent_id']))echo $cat_choose['parent_id'];?>"placeholder="Parent category"></select>
	</span>
	<span>
		<button id='submit_entry_category'><?php echo translate('Submit category'); ?></button>
	</span>
	<span id="notice" style="color:#ff0000;"></span>
</form>



<script type="text/javascript">

		$('#type_category').on('change',function(){
			if($('#type_category').val()==0){
				$.ajax({
					url:'<?php echo URL;?>/settings/get_parent/',
					success:function(data){
						
						var json=$.parseJSON(data);

						var i=0;
						$.each(json,function(key,value){
							var html_code='';
							
							if($('#parent').attr('check')==''){
								if(i==0){
									html_code = '<option value="'+key+'"" selected="selected">'+value+'</option>';
									i++;
								}
								else{
									html_code = '<option value="'+key+'"">'+value+'</option>';
								}
							}
							else{
								if(key==$('#parent').attr('check')){
									html_code = '<option value="'+key+'"" selected="selected">'+value+'</option>';
								}
								else{
									html_code = '<option value="'+key+'"">'+value+'</option>';
								}
							}
							$('#parent').append(html_code);
							
						});	
					}
				});
			}
			else{
				$('#parent').find('option').remove();
			}
		});

		$('#type_category').change();

		$('#submit_entry_category').on('click',function(){
			var id=$('#id_cat').val();
			var name=$('#name_category').val();
			var type=$('#type_category').val();
			var parent=$('#parent').val();
			$.ajax({
				url:'<?php echo URL;?>/settings/update_category/',
				type:"POST",
				data:{id:id,name:name,type:type,parent:parent},
				success:function(data){
					$('#notice').html(data);
				}
			}).done(function(){
				setTimeout(function(){
				$('#list_category').click();
			},1000);		
						
			});
		});


</script>