<div id='list_category' style="height:500px;">
<span id="notice" style="color:#ff0000;text-align:center;margin-bottom: -1%;"></span>
	<!--- Category level 1 -->
	<div id="lv1" class='category_lv'>
	<span class="cat_title"> Category:</span>
	<?php
	//pr($cat_lv1);
	$i=0;
	foreach ($cat_lv1 as $key => $value) {
		if($i%2==0){
			echo '<span id="'.$key.'" class="cat_list light cat_lv1">'.$value.' <a class="delete_cat"onclick="click_delete(this)">Delete</a><a class="edit_cat"onclick="click_edit(this)">Edit</a> </span>';
		}
		else{
			echo '<span id="'.$key.'" class="cat_list cat_lv1">'.$value.' <a class="delete_cat"onclick="click_delete(this)">Delete</a><a class="edit_cat"onclick="click_edit(this)">Edit</a></span>';
		}
		$i++;
	}
	?>

	</div>

	<!--- Category level 2 -->
	<div id="lv2" class='category_lv'>
		
	</div>

	<!--- Category level 3 -->
	<div id="lv3" class='category_lv'>
			
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('.cat_lv1').first().click();
	});

	$('.cat_lv1').on('click',function(){
		var title=$(this).text().split('DeleteEdit')[0];
		$('#lv2').empty().append('<span class="cat_title">'+title+':</span>');
		$('.cat_lv1').removeClass('active_cat');
		$(this).addClass('active_cat');

		$('#lv3').empty();
		var id=this.id;
		$.ajax({
			url:'<?php echo URL;?>/settings/category_child',
			type:'POST',
			data:{parent_id:id},
			success:function(data){
				var arr_lv2=$.parseJSON(data);
				var i=0;
				$.each(arr_lv2,function(key,value){
					if(i%2==0){
						var html_code='<span id="'+key+'" class="cat_list light cat_lv2">'+value+' <a class="delete_cat"onclick="click_delete(this)">Delete</a><a class="edit_cat"onclick="click_edit(this)">Edit</a></span>';
						$('#lv2').append(html_code);
					}
					else{
						var html_code='<span id="'+key+'" class="cat_list cat_lv2">'+value+' <a class="delete_cat"onclick="click_delete(this)">Delete</a><a class="edit_cat"onclick="click_edit(this)">Edit</a></span>';
						$('#lv2').append(html_code);
					}
					i++;
				});
			}
		}).done(function(){
			$('.cat_lv2').first().click();
		});
	});

$("#lv2").delegate( ".cat_lv2", "click", function() {
	var title=$(this).text().split('DeleteEdit')[0];
	$('#lv3').empty().append('<span class="cat_title">'+title+':</span>');
	$('.cat_lv2').removeClass('active_cat');
	$(this).addClass('active_cat');

		var id=this.id;
		$.ajax({
			url:'<?php echo URL;?>/settings/category_child',
			type:'POST',
			data:{parent_id:id},
			success:function(data){
				var arr_lv3=$.parseJSON(data);
				var i=0;
				$.each(arr_lv3,function(key,value){
					if(i%2==0){
						var html_code='<span id="'+key+'" class="cat_list light cat_lv3">'+value+' <a class="delete_cat"onclick="click_delete(this)">Delete</a><a class="edit_cat"onclick="click_edit(this)">Edit</a></span>';
						$('#lv3').append(html_code);
					}
					else{
						var html_code='<span id="'+key+'" class="cat_list cat_lv3">'+value+' <a class="delete_cat"onclick="click_delete(this)">Delete</a><a class="edit_cat"onclick="click_edit(this)">Edit</a></span>';
						$('#lv3').append(html_code);
					}
					i++;
				});
			}
		});
});

function click_edit(obj){
	$('h4','.fl_dent').text('Edit Category');
	var id_cat=$(obj).parent().attr('id');
	//alert(id_cat);
	$.ajax({
		url:'<?php echo URL;?>/settings/entry_category',
		type:'POST',
		data:{id:id_cat},
		success:function(data){
			$("#detail_category").html(data);
			
		}
	}).done(function(){
		//show_category_detail('list_category','List Category');
	});
};

function click_delete(obj){
	//alert(123321);
	$('h4','.fl_dent').text('Edit Category');
	var id_cat=$(obj).parent().attr('id');
	var name_cat=$(obj).parent().text().split('DeleteEdit')[0];
	if(confirm("Do you want to delete "+name_cat+"?")==true){
		$.ajax({
			url:'<?php echo URL;?>/settings/delete_category',
			type:'POST',
			data:{id:id_cat},
			success:function(data){
				$("#notice").html(data);
				
			}
		}).done(function(){
			setTimeout(function(){
					$('#list_category').click();
				},1000);	
		});
	}
	

}
</script>