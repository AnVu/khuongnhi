<?php
$author = 'Khuong Nhi';
 if($_SESSION['default_lang']=='vi'){
 	$title = 'Đặt hàng - Khuong Nhi';
 	$subject = 'Đặt hàng';
	$keywords = 'Đặt hàng, PDF';
	$shipping_address_label = 'Nơi nhận';
	$salesorder_label = '<span style="color:#b32017">Đ</span>ặt hàng';
	$salesorder = 'Đặt hàng';
	$job_no = 'Công việc';
	$date = 'Ngày';
	$cpo_no = 'Số CPO';
	$ac_no = 'Số A/c';
	$terms = 'Số ngày trả';
	$due_date = 'Hạn trả';
	$custom_po_no = 'Số KH';
	$required_date = 'Ngày hết hạn';
	$sku = 'Mã';
	$desc = 'Miêu tả';
	$width = 'Rộng';
	$height = 'Cao';
	$unit_price = 'Đơn giá';
	$qty = 'SL';
	$line_total = 'Tổng';
	$sub_total = 'Tổng trước thuế';
	$hst_gst = 'Thuế';
	$total = 'Tổng sau thuế';
	$note = 'Ghi chú';
	$fotter_text = 'Đây là ước tính dựa trên thông tin cung cấp. Báo giá có giá trị trong 30 ngày.    ';
 } else {
	$title = 'Khuong Nhi Sale Order';
	$subject = 'Sale Order';
	$keywords = 'Sale Order, PDF';
	$shipping_address_label = 'Shipping address';
	$salesorder_label = '<span style="color:#b32017">S</span>ales Order';
	$salesorder = 'Sales Order';
	$job_no = 'Job no';
	$date = 'Date';
	$cpo_no = 'CPO No';
	$ac_no = 'A/c no';
	$terms = 'Terms';
	$due_date = 'Due Date';
	$required_date = 'Required date';
	$custom_po_no = 'Customer PO no';
	$sku = 'SKU';
	$desc = 'Description';
	$width = 'Width';
	$height = 'Height';
	$unit_price = 'Unit Price';
	$qty = 'Qty';
	$line_total = 'Line total';
	$sub_total = 'Sub total';
	$hst_gst = 'HST/GST';
	$total = 'Total';
	$note = 'Note';
	$fotter_text = 'This is an estimate based on the supplied information. Quotation is valid for 30 days.    ';
 }
App::import('Vendor','xtcpdf');
$pdf = new XTCPDF('P','mm','USLETTER',true,'UTF-8',false,false);
$pdf->xfootertext = $fotter_text;
$pdf->xfooterfontsize = 10;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($author);
$pdf->SetTitle($title);
$pdf->SetSubject($subject);
$pdf->SetKeywords($keywords);

// set default header data
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(2);

// set margins
$pdf->SetMargins(10, 3, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$textfont = 'freesans';
// set font
$pdf->SetFont($textfont, '', 9);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '
	<table cellpadding="2" cellspacing="0" style="width:100%; margin: 0px auto">
		<tr>
			<td width="55%" valign="top" style="color:#1f1f1f;">
				<img src="'.$logo_link.'" alt="" />
				<div style="margin-bottom:1px; margin-top:4px;border-bottom: 1px solid #cbcbcb;">
					'.$company_address.'
				</div>
				<div>'.$customer_address.'</div>
			</td>
			<td width="25%" valign="top">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<br />
				<table style="width:100%; margin: 0px auto">
					<tr>
						<td><b>'.$shipping_address_label.':</b></td>
					</tr>
					<tr>
						<td>'.(isset($ship_to)&&$ship_to!='' ? $ship_to:$info_data->contact_name).'</td>
					</tr>
					<tr>
						<td>'.$shipping_address.'</td>
					</tr>
				</table>
			</td>
			<td width="20%" valign="top" align="right">
					<div style=" text-align:right; font-size:20px; font-weight:bold; color: #919295; border-bottom: 1px solid #cbcbcb;width:30%;">'.$salesorder_label.'</div>
					<div style="float:right;">
					<span style="font-weight:bold;">'.$salesorder.':</span> '.$info_data->no.' <br />
					<span style="font-weight:bold;">'.$job_no.':</span> '.$info_data->job_no.'<br />
					<span style="font-weight:bold;">'.$date.':</span> '.$info_data->date.'<br />
					<span style="font-weight:bold;">'.$custom_po_no.':</span> '.$info_data->po_no.'<br />
					<span style="font-weight:bold;">'.$ac_no.':</span> '.$info_data->ac_no.'<br />
					<span style="font-weight:bold;">'.$terms.':</span> '.$info_data->terms.' days<br />
					<span style="font-weight:bold;">'.$required_date.':</span> '.$info_data->required_date.'<br />
				</div>
			</td>
		</tr>
	</table>
	<div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div><br />
		';
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

//<div class="option">Option 01</div>

$html2 = '
	<style>
		td{
			line-height:2px;
		}
		td.first{
			border-left:1px solid #e5e4e3;
		}
		td.end{
			border-right:1px solid #e5e4e3;
		}
		td.top{
			color:#fff;
			font-weight:bold;
			background-color:#911b12;
			border-top:1px solid #e5e4e3;
		}
		td.bottom{
			border-bottom:1px solid #e5e4e3;
		}
		.option{
			color: #3d3d3d;
			font-weight:bold;
			font-size:14px;
			text-align: center;
			width:100%;
		}
		table.maintb{

		}
	</style>


	<div class="option">'.$heading.'</div><br />
	<table cellpadding="3" cellspacing="0" class="maintb">
		<tr>
			<td width="12%" class="first top">
				&nbsp;'.$sku.'
			</td>
			<td width="30%" class="top">
				'.$desc.'
			</td>
			<td align="right" width="9%" class="top">
				'.$width.'
			</td>
			<td align="right" width="9%" class="top">
				'.$height.'
			</td>
			<td align="right" width="15%" class="top">
				'.$unit_price.'
			</td>
			<td align="right" width="10%" class="top">
				'.$qty.'
			</td>
			<td align="right" width="15%" class="end top">
				'.$line_total.'
			</td>
		</tr>';
$html2 .= $html_cont;
//echo $html_cont; die;

$html2 .= '</table>';

$pdf->writeHTML($html2, true, false, true, false, '');



//PLEASE SIGN AND DATE TO INDICATE YOUR ACCEPTANCE
$html3 = '<style>
		td{
			line-height:2px;
		}
		td.first{
			border-left:1px solid #e5e4e3;
		}
		td.end{
			border-right:1px solid #e5e4e3;
		}
		td.top{
			color:#fff;
			font-weight:bold;
			background-color:#565656;
			border-top:1px solid #e5e4e3;
		}
		td.bottom{
			border-bottom:1px solid #e5e4e3;
			border-right:1px solid #e5e4e3;
			height:120px;
		}
		.option{
			color: #3d3d3d;
			font-weight:bold;
			font-size:18px;
		}
		table.maintb{

		}
	</style>
	';
if($other_comment!='')
	$html3 .= "<div style=\"line-height:2px;\"><u><b>'.$note.':</b></u>".$other_comment."</div>";
else
	$html3 .= '<div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"><u><b>'.$note.':</b></u></div><div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div><div style="border-bottom: 1px dashed #9f9f9f; height:1px; clear:both"></div>';
$pdf->writeHTML($html3, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();



// ---------------------------------------------------------
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->Output('example_001.pdf', 'I');

$pdf->Output('upload/'.$filename.'.pdf', 'F');
echo '<script>window.location.assign("'.URL.'/upload/'.$filename.'.pdf");</script>';
?>