<?php
	if(isset($arr_settings['relationship'][$sub_tab]['block']))
	foreach($arr_settings['relationship'][$sub_tab]['block'] as $key => $arr_val){
		echo $this->element('box',array('key'=>$key,'arr_val'=>$arr_val));
	}
?>
<p class="clear"></p>

<script type="text/javascript">
var over_credit_limit='0';
function check_over_credit_limit(){
    $.ajax({
    		url: '<?php echo URL.'/'.$controller;?>/check_over_credit_limit',
    		type:"POST",
    		timeout: 15000,
    		success: function(ret){
    		console.log(ret);
    		    if(ret=='over')
    		       over_credit_limit='1';
    		    else
    		       over_credit_limit='0';
    		}
    });
}
function create_full_shipping(){
     $.ajax({
                url: "<?php echo URL; ?>/<?php echo $controller;?>/create_full_shipping",
                timeout: 15000,
                type: "POST",
                success: function(html){

                   if(html=='full_shipping'){
                       alerts('Message','This sales order has already been part or fully shipped.');
                   }
                   else if(html=='no_company')
                   {
                       alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
                   }
                   else if(html=='no_product')
                   {
                       alerts('Message','No items have been entered on this transaction yet.');
                   }
                   else if(html=='over')
                   {
                      alerts('Message','This sales order was greater than this company credit limit.');
                   }
                   else
                   {
                      window.location.assign(html);
                   }

                }
            });

     return false;
}
function create_part_shipment(){
     $.ajax({
                url: "<?php echo URL; ?>/<?php echo $controller;?>/create_part_shipment",
                timeout: 15000,
                type: "POST",
                success: function(html){

                   console.log(html);
                        if(html=='no_company'){
                            alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
                        }
                        else if(html=='no_product'){
                            alerts('Message','No items have been entered on this transaction yet.');
                        }
                        else if(html=='part_shipped'){
                            alerts('Message',"There are no line items remaining to part invoice / ship.");
                        }
                        else if(html=='over'){
                            alerts('Message','This sales order was greater than this company credit limit.');
                        }
                        else{
                            window.location.assign(html);
                        }
                }
            });

     return false;
}




function create_full_salesinvoice(){
    $.ajax({
        url: "<?php echo URL; ?>/<?php echo $controller;?>/create_full_salesinvoice",
        timeout: 15000,
        type: "POST",
        success: function(html){
        console.log(html);

            if(html=='full_invoiced'){
                alerts('Message','This sales order has already been part or fully invoiced.');
            }
            else if(html=='no_company')
            {
                alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
            }
            else if(html=='no_product')
            {
                alerts('Message','No items have been entered on this transaction yet.');
            }
            else
            {
               window.location.assign(html);
            }
        }
    });

    return false;
}
function create_part_invoice(){
    $.ajax({
        url: "<?php echo URL; ?>/<?php echo $controller;?>/create_part_invoice",
        timeout: 15000,
        type: "POST",
        success: function(html){
            console.log(html);
            if(html=='no_company'){
                alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
            }
            else if(html=='no_product'){
                alerts('Message','No items have been entered on this transaction yet.');
            }
            else if(html=='part_invoiced'){
                alerts('Message',"There are no line items remaining to part invoice / ship.");
            }
            else{
                window.location.assign(html);
            }
        }
    });

    return false;
}


$(function(){

check_over_credit_limit();

});


var salesorder_id= $("#mongo_id").val();
var full_invoiced = '<?php if(isset($full_invoiced)) echo $full_invoiced;?>';
var full_shipped = '<?php if(isset($full_shipped))echo $full_shipped;?>';
var part_invoiced = '<?php if(isset($part_invoiced))echo $part_invoiced;?>';
var part_shipped = '<?php if(isset($part_shipped))echo $part_shipped;?>';
var no_product = '<?php if(isset($no_product))echo $no_product;?>';



$("#create_part_invoice").click(function() {
if($("#company_id").val()=='')
{
    alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
}
else if(part_invoiced=='0')
{
    alerts('Message',"There are no line items remaining to part invoice / ship.");
}
else if(no_product=='0')
{
     alerts('Message','No items have been entered on this transaction yet.');
}
else
{
    create_part_invoice();
   // window.location.assign("<?php echo URL; ?>/salesorders/receive_item/"+salesorder_id+"");
}
});






$("#create_full_invoice").click(function() {
if($("#company_id").val()=='')
{
     alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
}
else if(full_invoiced=='0')
{
     alerts('Message','This sales order has already been part or fully invoiced.');
}
else if(no_product=='0')
{
     alerts('Message','No items have been entered on this transaction yet.');
}
else
{
    create_full_salesinvoice();
 //window.location.assign("<?php echo URL; ?>/salesinvoices/create_salesinvoice_from_salesorder/"+salesorder_id+"");
}

});








$("#create_part_shipment").click(function() {
if($("#company_id").val()=='')
{
     alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
}
else if(part_shipped=='0')
{
     alerts('Message','There are no line items remaining to part invoice / ship.');
}
else if(no_product=='0')
{
     alerts('Message','No items have been entered on this transaction yet.');
}
else if(over_credit_limit=='1')
{
     alerts('Message','This sales order was greater than this company credit limit.');
}
else
{
        create_part_shipment();
        //window.location.assign("<?php echo URL; ?>/salesorders/receive_item/"+salesorder_id+"");
}

});










$("#create_full_shipment").click(function() {
if($("#company_id").val()=='')
{
     alerts('Message','This function cannot be performed as there is no company or contact linked to this record.');
}
else if(full_shipped=='0')
{
      alerts('Message','This sales order has already been part or fully shipped.');
}
else if(no_product=='0')
{
     alerts('Message','No items have been entered on this transaction yet.');
}
else if(over_credit_limit=='1')
{
     alerts('Message','This sales order was greater than this company credit limit.');
}
else
{

    create_full_shipping();
    //window.location.assign("<?php echo URL; ?>/shippings/create_shipping_from_salesorder/"+salesorder_id+"");
}
});

</script>