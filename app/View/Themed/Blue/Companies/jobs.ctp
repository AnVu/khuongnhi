<?php
	if(isset($arr_settings['relationship'][$sub_tab]['block']))
	foreach($arr_settings['relationship'][$sub_tab]['block'] as $key => $arr_val){
		echo $this->element('box',array('key'=>$key,'arr_val'=>$arr_val));
	}
?>
<p class="clear"></p>

<script type="text/javascript">
	$(function(){
		$('#bt_add_jobs').click(function(){
			var ids = $("#mongo_id").val();
			$.ajax({
				url:"<?php echo URL;?>/companies/jobs_add/" + ids,
				timeout: 15000,
				success: function(html){
					 location.replace(html);
				}
			});
		})

		$("input",'#editview_box_default').change(function(){
			var ids = $("#mongo_id").val();
			var names = $(this).attr("name");
			var inval = $(this).val();
			save_data(names,inval,'',function(){})
		});

		$(".del_jobs").click(function(){
            var names = $(this).attr("id");
            var ids = names.split("_");
            ids = ids[ ids.length - 1];
            confirms( "Message", "Are you sure you want to delete?",
            function(){
                $.ajax({
                    url: '<?php echo URL; ?>/companies/jobs_delete/' + ids,
                    success: function(html){
                        if(html == "ok"){
                            $(".del_jobs_" + ids).fadeOut();
                            reload_subtab('jobs');
                        }else{
                            console.log(html);
                        }
                    }
                });
            },function(){
                //else do somthing
            });
        });
	})
</script>