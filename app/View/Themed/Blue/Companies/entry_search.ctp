<?php echo $this->element('entry_tab_option');?>
<div id="content" class="fix_magr">
	<div class="clear">

		<div class="clear_percent">
			<div class="block_dent_a">
				<div class="title_1 float_left"><h1><span id="company_name_header"></span></h1></div>
				<div class="title_1 right_txt float_right"><h1><span id="company_phoneprefix_header" style="display:none">: </span><span id="company_phone_header"></span></h1></div>
			</div>
		</div>

		<div id="<?php echo $controller; ?>_entry_search">
			<form>
				<div class="clear_percent">
					<div class="clear_percent_1 float_left">
						<div class="tab_1 block_dent_a">
							<p class="clear">
								<span class="label_1 float_left fixbor"><?php echo translate('Company no'); ?></span>
							</p>
							<div class="indent_new width_in float_left">
								<?php echo $this->Form->input('Company.no', array(
										'class' => 'input_4 float_left width_ina22',
										'style' => 'margin-top: 1.6%;width: 13% !important;',
										'placeholder' => 1
								)); ?>
								<div class="in_active width_active_in2">
									<input type="hidden" name="data[Company][is_supplier]" id="CompanyIsSupplier_" value="0">
									<span class="inactive"><?php echo translate('Supplier'); ?></span>
									<label class="m_check2">
										<?php echo $this->Form->input('Company.is_supplier', array(
												'type' => 'checkbox',
										)); ?>
										<span></span>
									</label>
								</div>
								<div class="in_active width_active_in2">
									<input type="hidden" name="data[Company][is_customer]" id="CompanyIsCustomer_" value="0">
									<span class="inactive"><?php echo translate('Customer'); ?></span>
									<label class="m_check2">
										<?php echo $this->Form->input('Company.is_customer', array(
												'type' => 'checkbox'
										)); ?>
										<span></span>
									</label>
								</div>
							</div>

							<p class="clear">
								<span class="label_1 float_left"><?php echo translate('Company name'); ?></span>
							</p>
							<div class="width_in float_left indent_input_tp">
								<?php echo $this->Form->input('Company.name', array(
										'class' => 'input_4 float_left',
										'placeholder' => 1
								)); ?>
							</div>

							<p class="clear">
								<span class="label_1 float_left"><?php echo translate('Type'); ?></span>
							</p>

							<div class="width_in float_left indent_input_tp">
								<?php echo $this->Form->input('Company.type_name', array(
										'class' => 'input_4 input_select',
										'readonly' => true,
										'placeholder' => 1
								)); ?>
								<?php echo $this->Form->hidden('Company.type_name_id'); ?>
								<script type="text/javascript">
							        $(function () {
							            $("#CompanyTypeName").combobox(<?php echo json_encode($arr_company_type); ?>);
							        });
							    </script>
							</div>

							<p class="clear">
								<span class="label_1 float_left"><?php echo translate('Phone'); ?></span>
							</p>
							<div class="indent_new width_in float_left">
								<?php echo $this->Form->input('Company.phone', array(
										'class' => 'input_4 float_left',
										'placeholder' => 1
								)); ?>
								<span class="icon_phonec"></span>
							</div>

							<p class="clear">
								<span class="label_1 float_left"><?php echo translate('Fax'); ?></span>
							</p>
							<div class="indent_new width_in float_left">
								<?php echo $this->Form->input('Company.fax', array(
										'class' => 'input_4 float_left',
										'placeholder' => 1
								)); ?>
								<span class="icon_down_pl"></span>
							</div>

							<p class="clear">
								<span class="label_1 float_left"><?php echo translate('Email'); ?></span>
							</p>
							<div class="indent_new width_in float_left">
								<?php echo $this->Form->input('Company.email', array(
										'class' => 'input_4 float_left',
										'type' => 'email',
										'placeholder' => 1
								)); ?>
								<span class="icon_emaili"></span>
							</div>

							<p class="clear">
								<span class="label_1 float_left fixbor2">
									<span style="display:inline;"><?php echo translate('Web'); ?></span>
								</span>
							</p>
							<div class="indent_new width_in float_left">
								<?php echo $this->Form->input('Company.web', array(
										'class' => 'input_4 float_left',
										'placeholder' => 1
								)); ?>
							</div>

							<p class="clear"></p>
						</div><!--END Tab1 -->
					</div>

					<div class="float_right" style="width: 72%;">
						<div class="tab_1 float_left block_dent8">
							<?php

								echo $this->element('box_type/address', array(
								    'address_mode' => 'search',
									'address_label' => array('Default address'),
									'address_more_line' => 0,
									'address_controller' => array('Company'),
									'address_country_id' => array(), //array($this->data['Company']['addresses'][$key]['country_id']),
									'address_value' => array(
										'default' => array(
											'', //$this->data['Company']['addresses'][$key]['address_1'],
											'', //'', //$this->data['Company']['addresses'][$key]['address_2'],
											'', //$this->data['Company']['addresses'][$key]['address_3'],
											'', //$this->data['Company']['addresses'][$key]['town_city'],
											'', //$this->data['Company']['addresses'][$key]['country_id'],
											'', //
											'', //$this->data['Company']['addresses'][$key]['province_state_id'],
											''  //$this->data['Company']['addresses'][$key]['zip_postcode']
										)
									),
									'address_key' => array('default'),
									'address_conner' => array(
										array(
											'top' => 'hgt fixbor',
											'bottom' => 'fixbor3 fix_bottom_address fix_bot_bor'
										)
									)
							)); ?>

							<div class="tab_1_inner float_left">

								<!-- Business Type -->
								<p class="clear">
									<span class="label_1 float_left  minw_lab "><?php echo translate('Business Type'); ?></span>
								</p>
								<div class="width_in3 float_left indent_input_tp">
									<?php echo $this->Form->input('Company.business_type', array(
											'class' => 'input_4 input_select',
											'readonly' => true,
											'placeholder' => 1
									)); ?>
									<?php echo $this->Form->hidden('Company.business_type_id'); ?>
								</div>
								<script type="text/javascript">
							        $(function () {
							            $("#CompanyBusinessType").combobox(<?php echo json_encode($arr_business_type); ?>);
							        });
							    </script>

								<!-- Industry -->
								<p class="clear">
									<span class="label_1 float_left  minw_lab "><?php echo translate('Industry'); ?></span>
								</p>
								<div class="width_in3 float_left indent_input_tp">
									<?php echo $this->Form->input('Company.industry', array(
											'class' => 'input_4 input_select',
											'readonly' => true,
											'placeholder' => 1
									)); ?>
									<?php echo $this->Form->hidden('Company.industry_id'); ?>
								</div>
								<script type="text/javascript">
							        $(function () {
							            $("#CompanyIndustry").combobox(<?php echo json_encode($arr_industry); ?>);
							        });
							    </script>

							    <!-- Industry -->
								<p class="clear">
									<span class="label_1 float_left  minw_lab "><?php echo translate('Size'); ?></span>
								</p>
								<div class="width_in3 float_left indent_input_tp">
									<?php echo $this->Form->input('Company.size', array(
											'class' => 'input_4 input_select',
											'readonly' => true,
											'placeholder' => 1
									)); ?>
									<?php echo $this->Form->hidden('Company.size_id'); ?>
								</div>
								<script type="text/javascript">
							        $(function () {
							            $("#CompanySize").combobox(<?php echo json_encode($arr_size); ?>);
							        });
							    </script>

							    <!-- Our rep -->
								<p class="clear">
									<span class="label_1 float_left minw_lab">
										<?php
											$link = 'javascript:void(0)';
											if(isset($this->data['Company']['our_rep_id']) && is_object($this->data['Company']['our_rep_id'])){
												$link = URL . '/contacts/entry/' . $this->data['Company']['our_rep_id'];
											}
										?>
										<span style="display:inline;" onclick="jt_link_module(this, '<?php echo msg('QUOTATION_CREATE_LINK'); ?> Contacts', '<?php echo URL; ?>/contacts/add')" href="<?php echo $link; ?>" class="jt_box_line_span" id="link_to_contacts" ><?php echo translate('Our Rep'); ?></span>

									</span>
								</p>
								<div class="width_in3 float_left indent_input_tp">
									<?php echo $this->Form->hidden('Company.our_rep_id'); ?>
									<?php echo $this->Form->input('Company.our_rep', array(
											'class' => 'input_4 float_left ',
											'readonly' => true,
											'placeholder' => 1
									)); ?>
									<span id="click_open_window_contacts_responsible" class="iconw_m indent_dw_m"></span>
									<script type="text/javascript">
										$(function(){
											// kiểm tra xem đã chọn company chưa
											company_init_popup_contacts_responsible();
										});

										function company_init_popup_contacts_responsible(){
											var parameter_get = "?is_employee=1";
											window_popup("contacts", "Specify contact", "_responsible", "", parameter_get);

										}

										function after_choose_contacts_responsible(contact_id, contact_name){
											$("#link_to_contacts").attr("href", "<?php echo URL; ?>/contacts/entry/" + contact_id);
											$("#CompanyOurRepId").val(contact_id);
											$("#CompanyOurRep").val(contact_name);
											$("#window_popup_contacts_responsible").data("kendoWindow").close();
											return false;
										}
									</script>
								</div>

								<p class="clear">
									<span class="label_1 float_left minw_lab">
										<?php
											$link = 'javascript:void(0)';
											if(isset($this->data['Company']['our_csr_id']) && is_object($this->data['Company']['our_csr_id'])){
												$link = URL . '/contacts/entry/' . $this->data['Company']['our_csr_id'];
											}
										?>
										<span style="display:inline;" onclick="jt_link_module(this, '<?php echo msg('QUOTATION_CREATE_LINK'); ?> Contacts', '<?php echo URL; ?>/contacts/add')" href="<?php echo $link; ?>" class="jt_box_line_span" id="link_to_contacts_csr" ><?php echo translate('Our CSR'); ?></span>
									</span>
								</p>

								<div class="width_in3 float_left indent_input_tp">
									<?php echo $this->Form->hidden('Company.our_csr_id'); ?>
									<?php echo $this->Form->input('Company.our_csr', array(
											'class' => 'input_4 float_left ',
											'readonly' => true,
											'placeholder' => 1
									)); ?>
									<span id="click_open_window_contacts_csr" class="iconw_m indent_dw_m"></span>
									<script type="text/javascript">
										$(function(){
											// kiểm tra xem đã chọn company chưa
											company_init_popup_contacts_csr();
										});

										function company_init_popup_contacts_csr(){
											var parameter_get = "?is_employee=1";
											window_popup("contacts", "Specify contact", "_csr", "", parameter_get);

										}

										function after_choose_contacts_csr(contact_id, contact_name){
											$("#link_to_contacts_csr").attr("href", "<?php echo URL; ?>/contacts/entry/" + contact_id);
											$("#CompanyOurCsrId").val(contact_id);
											$("#CompanyOurCsr").val(contact_name);
											$("#window_popup_contacts_csr").data("kendoWindow").close();
											return false;
										}
									</script>
								</div>

								<p class="clear">
									<span class="label_1 float_left minw_lab"></span>
								</p>
								<div class="width_in3 float_left indent_input_tp">
									<input type="text" readonly="true" class="input_select">
								</div>

								<p class="clear">
									<span class="label_1 float_left minw_lab fixbor3"><?php echo translate('Inactive'); ?></span>
								</p>
								<div class="width_in3 float_left indent_input_tp" style="margin-top: 9px;">
									<input type="hidden" name="data[Company][inactive]" id="CompanyInactive_" value="0">
									<label class="m_check2">
										<?php echo $this->Form->input('Company.inactive', array(
												'type' => 'checkbox'
										)); ?>
										<span class="bx_check dent_chk"></span>
									</label>
									<span class="inactive dent_check"></span>
								</div>

							</div>

							<!-- TAB 3 -->
							<div class="tab_1_inner float_left">

								<p class="clear">
									<span class="label_1 float_left minw_lab"></span>
								</p>
								<div class="width_in3 float_left indent_input_tp"></div>

								<p class="clear">
									<span class="label_1 float_left minw_lab"></span>
								</p>
								<div class="width_in3 float_left indent_input_tp"></div>

								<p class="clear">
									<span class="label_1 float_left minw_lab"></span>
								</p>
								<div class="width_in3 float_left indent_input_tp"></div>

								<p class="clear">
									<span class="label_1 float_left minw_lab"></span>
								</p>
								<div class="width_in3 float_left indent_input_tp"></div>

								<p class="clear">
									<span class="label_1 float_left minw_lab"></span>
								</p>
								<div class="width_in3 float_left indent_input_tp"></div>

								<p class="clear">
									<span class="label_1 float_left minw_lab"></span>
								</p>
								<div class="width_in3 float_left indent_input_tp"></div>

								<p class="clear">
									<span class="label_1 float_left minw_lab fixbor3"></span>
								</p>
								<div class="width_in3 float_left indent_input_tp"></div>

							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="clear"></div>
		</div><!--  END DIV companies_form_auto_save -->

		<!-- DIV NOI DUNG CUA CAC SUBTAB -->
		<div id="companies_sub_content" class="jt_sub_content">
		</div>

	</div>
	<div class="clear"></div>
</div>

<?php echo $this->element('../Companies/js_search'); ?>