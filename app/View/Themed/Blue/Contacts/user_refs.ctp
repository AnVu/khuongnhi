<div class="clear_percent_4a float_left">
    <div class="tab_1 full_width">
        <span class="title_block bo_ra1">
            <span class="float_left">
                <span class="fl_dent">
                    <h4><?php echo translate('System login details'); ?></h4>
                </span>
            </span>
        </span>
        <!-- baonam -->
        <?php echo $this->Form->create('Contact', array('id' => 'ContactRefsForm')); ?>
        <?php echo $this->Form->hidden('Contact._id'); ?>
        <div class="tab_2_inner">
            <p class="clear">
                <span class="label_1 float_left minw_lab2"><?php echo translate('Username'); ?></span>
            </p>
            <div class="width_in3a float_left indent_input_tp">
                <?php
                    $array = array(
                        'class' => 'input_1 float_left',
                        'value' => $this->data['Contact']['user_name_contact'],
                        'readonly' => true
                    );
                    if($this->Common->check_permission($controller.'_@_change_username_@_edit',$arr_permission)){
                        $array['readonly'] = false;
                        $array['onchange'] = 'change_password(this)';
                    }

                ?>
                <?php echo $this->Form->input('Contact.username_contact', $array); ?>
            </div>
            <p class="clear">
                <span class="label_1 float_left minw_lab2"><?php echo translate('Password'); ?></span>
            </p>
            <div class="width_in3a float_left indent_input_tp">
                <?php echo $this->Form->input('Contact.password_contact', array(
                    'class' => 'input_1 float_left',
                    'type' => 'password',
                    'value' => '123456',
                    'onchange' => 'change_password(this)'
                    )); ?>
            </div>
            <p class="clear">
                <span class="label_1 float_left minw_lab2 fixbor3"><?php echo translate('Set user as default as login'); ?></span>
            </p>
            <div class="width_in3a float_left indent_input_tp">
                <div class="in_active2">
                    <label class="m_check2">
                    <input type="checkbox">
                    <span class="bx_check dent_chk"></span>
                    </label>
                    <span class="inactive dent_check color_hidden">(<?php echo translate('if multi user will default all users'); ?>)</span>
                    <p class="clear"></p>
                </div>
            </div>
            <p class="clear"></p>
            <div>
                <div class="title_block">
                    <span class="fl_dent">
                        <h4><?php echo translate('Language & Theme'); ?> </h4>
                    </span>
                </div>
                <div class="tab_2_inner">
                    <p class="clear">
                        <span class="label_1 float_left minw_lab2" style=""><?php echo translate('Choice language') ?></span>
                    </p>
                    <div class="width_in3 float_left indent_input_tp" id="shipping_province" style="width:60.5%">
                        <?php if(!empty($arr_language)){?>
                        <input name="language" value="<?php if(isset($arr_return['language']) && isset($arr_language[$arr_return['language']])) echo $arr_language[$arr_return['language']]; else echo 'Tiếng Việt';?>"  id="language_id" class="input_select" readonly="readonly" type="text">
                        <input name "languageid" type="hidden" value="<?php if(isset($arr_language['value'])) echo $arr_language['value'];?>" id="language_idId"/>
                        <script type="text/javascript">
                            $(function(){
                              $("#language_id").combobox(<?php echo json_encode($arr_language); ?>);
                            });
                        </script>
                        <?php }?>
                    </div>
                    <style type="text/css">
                        .contain_role{
                        border-bottom: 1px solid #DDD;
                        height: 10px;
                        padding: 6px 6px 2px;
                        }
                        .contain_role a{
                        color: blue;
                        }
                    </style>
                    <p class="clear">
                        <span class="label_1 float_left minw_lab2 fixbor3" style="height: 64px"><?php echo translate('Theme') ?></span>
                    </p>
                    <div class="width_in3 float_left indent_input_tp"  style="width:60.5%">
                        <input name="theme" value="<?php if(isset($arr_return['theme']) ) echo $arr_return['theme']; else echo 'Default';?>"  id="theme" class="input_select" readonly="readonly" type="text">
                        <input type="hidden" id="themeId"/>
                        <script type="text/javascript">
                            $(function(){
                              $("#theme").combobox({"default":"Default","blue":"Blue"});
                            });
                        </script>
                    </div>
                </div>
            </div>
            <p class="clear"></p>
        </div>
        <?php echo $this->Form->end(); ?>
        <span class="title_block bo_ra2"></span>
    </div>
    <!--END Tab1 -->
</div>
  <p class="clear"></p>
    <p class="clear"></p>
<script type="text/javascript">
    function clearOnlogin(){
    $("input#on_login_set_quick_view").val("");
        var fieldname = $("input#on_login_set_quick_view").attr("name");
            var values = $("input#on_login_set_quick_view").val();
            var ids = $("#ContactId").val();
        $.ajax({
           url: '<?php echo URL; ?>/<?php echo $controller;?>/save_data_for_non_model',
           timeout: 15000,
           type: "POST",
           data: { fieldname : fieldname,values:values,ids:ids },
           success: function(html){
           }
       });
       return false;
    }
     $("form#profile_form input,select,radio").change(function() {
            var fieldname =$(this).attr("name");
            var values = $(this).val();
            var ids = $("#ContactId").val();
          var fieldtype = $(this).attr("type");

            if(fieldtype=='checkbox'){
                if($(this).is(':checked'))
                    values = 1;
                else
                    values = 0;
            }

            $.ajax({
                url: '<?php echo URL; ?>/<?php echo $controller;?>/save_data_for_non_model',
                timeout: 15000,
                type: "POST",
                data: { fieldname : fieldname,values:values,ids:ids},
                success: function(html){
                    console.log(html);
                }
            });

            return false;
        });

    /* $("form#ContactRefsForm input,select").change(function(){
        var fieldname = $(this).attr("name");
        var values = $(this).val();
        var ids = $("ContactId").val();
        //alert(ids);
        $.ajax({
          url:'<?php echo URL; ?>/<?php echo $controller;?>/save_data_for_non_model',
          timeout: 15000,
          type: "POST",
          data: { fieldname : fieldname,values:values,ids:ids},
          success: function(html){
              console.log(html);
            }
        });
        return false;
     });*/

        $("#jobtraq_related input, #jobtraq_related select, #jobtraq_related radio,#language_id,#theme").change(function() {
                var fieldname =$(this).attr("name");
                var values = $(this).val();
            if(fieldname=='language')
                values = $("#language_idId").val();
                var ids = $("#ContactId").val();
            //alert(ids);
                var fieldtype = $(this).attr("type");

                if(fieldtype=='checkbox'){
                    if($(this).is(':checked'))
                        values = 1;
                    else
                        values = 0;
                }
                $.ajax({
                    url: '<?php echo URL; ?>/<?php echo $controller;?>/save_data_for_non_model',
                    timeout: 15000,
                    type: "POST",
                    data: { fieldname : fieldname,values:values,ids:ids },
                    success: function(html){
                        if(html == 'ok'){
                            if(fieldname=='theme')
                                location.reload();
                        }
                        console.log(html);
                    }
                });

                return false;
            });

    function change_password(object){
        if($(this).attr("id") == "ContactUserNameContact")
            $(this).val( $.trim($(this).val()) );
        $.ajax({
            url: '<?php echo URL; ?>/contacts/user_refs_auto_save',
            timeout: 15000,
            type:"post",
            data: $(object).closest('form').serialize(),
            success: function(html){
                if( html != "ok" )alerts("Error: ", html);
            }
        });
    }
</script>