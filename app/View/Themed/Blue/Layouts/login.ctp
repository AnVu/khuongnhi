<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Khuong Nhi</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('reset');
		echo $this->Html->css('style');
		echo $this->Html->css('jt_screen');
	?>

	<script type="text/javascript">
		function check_device(){
	       var uagent = navigator.userAgent.toLowerCase();
	       if (uagent.search("android") > -1)
	          window.location.assign("http://jt.anvy.net/users/login")
	       else if (uagent.search("ios") > -1)
	          window.location.assign("http://jt.anvy.net/users/login")
	       else if (uagent.search("windowphone") > -1)
	          window.location.assign("http://jt.anvy.net/users/login")
	       else
	          console.log('Destop');
	    }
    </script>
</head>

<body onload="check_device()">
	<?php echo $this->fetch('content'); ?>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
