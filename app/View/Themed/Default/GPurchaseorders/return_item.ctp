<?php

    if(!$this->Common->check_permission_array(array('shippings_@_entry_@_add','purchaseorders_@_shipping_received_tab_@_add'),'and',$arr_permission))
?>
<style type="text/css">
ul.ul_mag li.hg_padd {
  overflow: visible !important;
}
.bg4 {
  background: none repeat scroll 0 0 #949494;
  color: #fff;
}

.bg4 span h4 {
  margin-left: 1%;
  width: 100%;
}
</style>
<?php echo $this->element('../' . $name . '/tab_option'); ?>
<div class="tab_1 full_width" style="margin:1% 1%; width:98%;">
    <span class="title_block bo_ra1">
        <span class="fl_dent">
            <h4>
               <?php
                    echo translate('Purchase Order').': '.$arr_po['code'].' ('.translate('Return').')';
                ?>
            </h4>
        </span>
    </span>
    <p class="clear"></p>
    <ul class="ul_mag clear bg3">
        <li class="hg_padd center_text" style="width:10%"><?php echo translate('Code'); ?></li>
        <li class="hg_padd" style="width:30%"><?php echo translate('Name / detail'); ?></li>
        <li class="hg_padd right_txt" style="width:6%"><?php echo translate('Original qty'); ?></li>
        <div class="float_left" style="width:15%;">
            <div class="tab_title_purchasing">
                <span class="block_purcharsing"><?php echo translate('Received') ; ?></span>
            </div>
            <div>
                <li class="hg_padd line_mg right_txt" style="width:50%"><?php echo translate('Received') ; ?></li>
                <li class="hg_padd line_mg right_txt" style="width:47%"><?php echo translate('Balance'); ?></li>
            </div>
        </div>
        <div class="float_left" style="width:15%;">
            <div class="tab_title_purchasing">
                <span class="block_purcharsing"><?php echo translate('Returned'); ?></span>
            </div>
            <div>
                <li class="hg_padd line_mg right_txt" style="width:50%"><?php echo translate('Returned'); ?></li>
                <li class="hg_padd line_mg right_txt" style="width:47%"><?php echo translate('Balance'); ?></li>
            </div>
        </div>
        <li class="hg_padd right_txt" style="width:6%"><?php echo translate('Returned now'); ?></li>
    </ul>
    <div class="container_same_category mCustomScrollbar _mCS_4">
        <div class="mCustomScrollBox mCS-light" style="position:relative; height:100%; overflow:scroll; max-width:100%;">
            <div class="mCSB_container mCS_no_scrollbar" style="position: relative; top: 0px;">
                <form method="POST" enctype="multipart/form-data" id="form_return">

                    <?php
                    $i = 1;
                    ?>
                    <?php foreach ($arr_po['products'] as $key => $po): ?>
                        <?php if ($po['deleted'] == false && isset($po['quantity']) && $po['quantity'] > 0 && (isset($po['balance_returned'])&&$po['balance_returned']!=0) ):
                        ?>

                            <?php
                            // set pg;
                            $pg = ($i%2==0 ? 2 : 1);
                            ?>
                            <ul class="ul_mag clear bg<?php echo $pg; ?>" style="overflow:none;">
                                <input type="hidden" disable="disabled" id="balance_value_<?php echo $key; ?>" value="<?php echo (isset($po['balance_returned'])?$po['balance_returned']:0);; ?>" />
                                <li class="hg_padd center_text" style="width:10%"><?php if (isset($po['code'])) echo $po['code']; ?></li>
                                <li class="hg_padd" style="width:30%"><?php if (isset($po['products_name'])) echo $po['products_name']; ?></li>
                                <li id="product_quantity" class="hg_padd right_txt" style="width:6%"><?php if (isset($po['quantity'])) echo $po['quantity']; ?></li>
                                <li id="received_quantity" class="hg_padd right_txt" style="width:6.7%"><?php echo (isset($po['quantity_received']) ? $po['quantity_received'] : 0); ?></li>
                                <li id="received_balance" class="hg_padd right_txt" style="width:6.4%">
                                    <?php
                                        if(isset($po['balance_received'])) echo $po['balance_received']; else echo 0;
                                    ?>
                                </li>
                                <li id="returned_<?php echo $key; ?>" class="hg_padd right_txt" style="width:6.7%">
                                    <?php echo (isset($po['quantity_returned']) ? $po['quantity_returned'] : 0); ?>

                                </li>
                                <li id="balance_<?php echo $key; ?>" class="hg_padd right_txt" style="width:6.2%">
                                    <?php
                                        echo (isset($po['balance_returned'])?$po['balance_returned']:0);
                                    ?>
                                </li>
                                <li class="hg_padd right_text" style="width:6%">

                                    <input type="text" rel="0" onkeypress="return isPrice(event);" id="return_now_<?php echo $key; ?>" name="quantity[<?php echo $key ?>]" value="0" class="validate input_inner jt_box_save right_txt"/>
                                </li>
                            </ul>
                        <?php $i++; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>

                </form>
                <?php
                    $k = 8 - $i;
                    if($k>0)
                        for($j=0; $j < $k ; $j++ )
                        {
                            $pg = ($i%2==0 ? 2 : 1);
                            echo ' <ul class="ul_mag clear bg'.$pg.'" style="overflow:none;"></ul>';
                            $i++;
                        }
                ?>
            </div>
        </div>
    </div>
    <span class="title_block bo_ra2">
        <span class="bt_block float_right no_bg">
            <div class="dent_input float_right">
                <input type="button" class="btn_pur" id="bt_cancel" onclick="window.location.replace('<?php echo URL; ?>/purchaseorders/entry')" name="cancel" value="Cancel" style="width: 100px; cursor: pointer">
                <input type="button" class="btn_pur" onclick="fr_submit()" id="bt_continue" name="continue" value="Continue" style="width: 100px; cursor: pointer">
            </div>
        </span>
    </span>
</div>

<script>
    $(function() {
        $('.validate').change(function() {
            $("#bt_continue").removeAttr('disabled');
            var ids = $(this).attr('id');
            var values = $(this).val();
            var old = $(this).attr('rel');
            var tmp = ids.split("_");
            tmp = tmp[2];
            var balance = parseInt($("#balance_value_" + tmp).val());
            if (values > balance) {
                $("#bt_continue").attr('disabled', 'disabled');
                alerts('Message', 'Product returned is not greater than the received products', function() {
                    $("#" + ids).val(old);
                    if(parseInt(old) == parseInt(balance))
                        $("#bt_continue").removeAttr('disabled');
                });
            }
            else {
                 $("#" + ids).val(parseInt(values));
                $("#" + ids).attr('rel', parseInt(values));
            }
        });
    });
    function fr_submit() {
        $("#bt_continue").attr('disabled', 'disabled');
        $("form#form_return").submit();
    }
</script>