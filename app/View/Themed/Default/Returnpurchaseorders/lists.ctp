<?php
	echo $this->element('entry_tab_option', array('no_show_delete' => true));
?>
<?php echo $this->element('js/lists_view'); ?>
<div id="content" class="fix_magr">
	<form method="POST" id="sort_form">
	<div class="w_ul2 ul_res2">
		<ul class="ul_mag clear bg top_header_inner2 ul_res2" id="sort">
			<li class="hg_padd" style="width:.5%"></li>
			<li class="hg_padd " style="width:5%">
				<label>Mã đặt hàng</label>
				<span id="code" class="desc"></span>
			</li>
			<li class="hg_padd " style="width:15%">
				<label>Tên công ty</label>
				<span id="company_name" class="desc"></span>
			</li>
			<li class="hg_padd center_txt" style="width:8%">
				<label>Ngày đặt hàng</label>
				<span id="purchord_date" class="desc"></span>

			</li>
			<li class="hg_padd right_txt" style="width:8%">
				<label>Tổng tiền đặt hàng</label>
				<span id="sum_sub_total" class="desc"></span>
			</li>
			<li class="hg_padd " style="width:8%">
				<label>Tình trạng</label>
				<span id="purchase_orders_status" class="desc"></span>
			</li>
			<li class="hg_padd bor_mt" style="width:.5%"></li>
		</ul>

		<br/>

		<ul class="ul_mag clear bg" id="sort2" style="position: fixed">
			<li class="hg_padd" style="width:.5%"></li>
			<li class="hg_padd center_txt" style="width:5%">
				<select class="select_order" name="code" id="code">
					<option value=""></option>
					<?php
						foreach ($arr_orders as $order){
							echo '<option value="'.$order['code'].'">'.$order['code'].'</option>';
						}
					?>
				</select>
			</li>
			<li class="hg_padd" style="width:15%">
				<select class="select_order" name="company_id" id="company">
					<option value=""></option>
					<?php
						foreach ($arr_company as $key => $value){
							echo '<option value="'.$key.'">'.$value.'</option>';
						}
					?>
				</select>
			</li>
			<li class="hg_padd center_txt" style="width:8%">
				<select class="select_order" name="purchord_date" id="date">
					<option value=""></option>
					<?php
						foreach ($arr_date as $date){
							echo '<option value="'.$date.'">'.date('d/m/Y',$date).'</option>';
						}
					?>
				</select>
			</li>
			<li class="hg_padd" style="width:8%"></li>
			<li class="hg_padd" style="width:8%">
				<select class="select_order" name="purchase_orders_status">
					<option value=""></option>
					<option value="Mới">Mới</option>
					<option value="Hoàn thành">Hoàn thành</option>
				</select>
			</li>
			<li class="hg_padd bor_mt" style="width:.5%"></li>
		</ul>

		<div id="lists_view_content" style="margin-top:15px;">
			<?php echo $this->element('../Returnpurchaseorders/lists_ajax') ?>
		</div>

	</div>
	 </form>
</div>
<link href="<?php echo URL.'/theme/default/css/'?>select2.css" rel="stylesheet" />
<style type="text/css" media="screen">
	.select2-container--default .select2-selection--single{
		border-radius: 0 !important;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow b {
		border-color: #000 transparent transparent transparent;
		border-style: solid;
		border-width: 6px 3px 0 3px;
		height: 0;
		left: 50%;
		margin-left: -4px;
		margin-top: -2px;
		position: absolute;
		top: 25%;
		width: 0;
	}
	.select2-container--default.select2-container--open .select2-selection--single .select2-selection__arrow b {
		border-color: transparent transparent #000 transparent;
		border-width: 0 3px 6px 3px;
	}
	.select2-container--default .select2-results > .select2-results__options{
		overflow-x: hidden;
	}
	.select2-results__option{
		padding:4px;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 16px;
	}
	#select2-sku_search-container{
		text-align: center;
		margin-left: 15px;
	}
	.select2-results__option:first-child{
		height: 16px;
	}
	.select2-results__option{
		font-family: arial,sans-serif,verdana;
		font-size: 11px;
	}

</style>
<script src="<?php echo URL.'/theme/default/js/'?>select2.min.js"></script>
<script type="text/javascript">
	function salesorders_lists_delete(id) {
		confirms("Message", "Are you sure you want to delete?",
				function() {
					$.ajax({
						url: '<?php echo URL; ?>/returnpurchaseorders/delete/' + id,
						timeout: 15000,
						success: function(html) {
							$("#lists_view_content").html(html);
						}
					});
				}, function() {
			//else do somthing
		});

		return false;
	}

	$("select#company").select2();
	$("select#date").select2();
	$("select#code").select2();

	$('.select_order').on('change',function(){
		var name = this.name;
		var arr_data={};
		arr_data['search']  = true;
		if(name == 'code'){
			arr_data[name] = $("select[name="+name+"]").val();
		}else{
			arr_field = ['company_id','purchord_date','purchase_orders_status'];
			$.each(arr_field,function(key,value){
				if($("select[name="+value+"]").val() != '')
					arr_data[value] = $("select[name="+value+"]").val();
			})
		}
		$.ajax({
			url:"<?php echo URL?>/returnpurchaseorders/lists",
			type:'POST',
			data:arr_data,
			success:function(html){
				$("#lists_view_content").html(html);
			}
		});

	})


</script>