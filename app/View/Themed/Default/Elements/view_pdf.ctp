<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo URL; ?>/favicon.ico" type="image/x-icon" rel="icon">
<link href="<?php echo URL; ?>/favicon.ico" type="image/x-icon" rel="shortcut icon">
<title>KN</title>
<style type="text/css" media="print,screen">
#wrap{
	width:100%;
	margin: auto;
	float:left;
	/*font-size: 10.5pt;*/
	font-family: Arial, Helvetica, sans-serif;
}

#wrap p {
	line-height: 4px;
}

#content{
	width: 100%;
	margin: 0px auto;
}

#header{
	width: 100%;
	border-bottom: 1pt dashed #ccc;
}

#header > tbody > tr > td:nth-child(1) {
	width: 65%;
	vertical-align: top;
}

#header > tbody > tr > td:nth-child(2) {
	text-align: right;
	vertical-align: top;
}

#header #right_info {
	border-top:1pt solid #ccc; padding-top: 15px; text-align: left;float: right;
}
#address{
	padding-bottom: 10px;
	border-top: 1pt solid #ccc;
	width: 100%;
}
#address td{
	vertical-align: top;
	width: 49%;
}
#address b {
	line-height: 18px;
}
#bottom_header {
	display: inline-block;
	width: 100%;
}
#billing_address{
	padding-bottom: 10px;
	display: inline-block;
	float:left;
	width: 50%;
}
#shipping_address{
	display: inline-block;
	float:left;
}
#title_pdf{
	font-size: 200%;
	font-weight: bolder;
	text-transform: capitalize;
	color:#919295;
}
#title_pdf span{
	color:#b32017;
}
#title_pdf #border {
	border-bottom:1pt solid #ccc;
	width: 70%;
	margin-left: 30%;
}

.row{
	width:100%;
	margin-bottom: 10px;
}

#pdf_content{
	width: 100%;
}
#pdf_content #heading{
	padding: 10px 0;
	text-align: center;
	font-weight: bold;
	text-transform: capitalize;
	font-size: 20px;
}

#product_list, #sum_table{
	width: 100%;
	border: none;
	border-spacing: 0;
}

#product_list tr th{
	text-align: left;
	background: #EAE1E0;
	color: #000;
	padding: 5px;
	font-size: 12pt;
}
#product_list tr td, #sum_table tr td{
	font-size: 12pt;
	padding: 8px;
}
#product_list tr td,#product_list tr th,#sum_table tr td,#sum_table tr th{
	border:1px solid #121111;
}
#product_list tr td p{
	margin-bottom: 10px;
}
/*#product_list tr:nth-child(even) td{
	background: #eeeeee;

}
#product_list tr:nth-child(odd) td{
	background: #fdfcfa;
}*/

#sum_table tr.sum_title{
	text-align: right !important;
	font-weight: 600;
}

#product_list tr.sum_title td:nth-child(2){
	font-weight: normal !important;
}

#product_list tr td.option_product{
	padding-left: 15px;
}
span.bullet::before{
	content:"\2022";
	padding-right: 5px;
	font-weight: bold;
}

#right_info {
	float: right;
}

#right_info td:first-child {
	font-weight: bold !important;
}

#right_info td:last-child {
	padding-left: 20px !important;
}
.right_text {
	text-align: right;
}
.center_text {
	text-align: center;
}
.bold_text {
	font-weight: bold;
}
#note{
	margin-top: 20px;
	page-break-inside:avoid;
	page-break-after:auto;
}
#note strong {
	text-decoration: underline;
}
#note div {
	border-bottom: 1pt dashed #9f9f9f;
	margin-bottom: 30px;
}
#quotation-note {
	page-break-inside:avoid;
	page-break-after:auto;
	page-break-before:auto;
}
#product_list tfoot td {
	border: none !important;
	background: none !important;
}
#product_list .note td {
	border: none !important;
	background: none !important;
}
.quotation tr:first-child td {
	color:#fff;
	font-weight:bold;
	background-color:#565656;
	height: 25px;
}
.quotation tr:last-child td {
	border:1pt solid #e5e4e3;
	height: 150px;
}
table#product_list {
   page-break-inside:always;
 }
#product_list tr{
  page-break-inside:avoid;
  page-break-after:auto;
 }
#last_table{
	width: 100%;
}
#last_table_final{
	width: 100%;
}
@media print {
	thead {display: table-header-group;}
}
.footer {
	display: table; width: 100%; margin: 100px auto 0; border-top: 1px solid #000;
}
<?php if( DS == '\\' ) { ?>
.footer_ul {
	font-family: "Times New Roman", Times, serif;
	font-size: 10.5px;
}
<?php } else { ?>
.footer_ul {
	font-family: "Times New Roman", Times, serif;
	font-size: 10px;
}
<?php } ?>
.footer_li {
	list-style: none; float: left; margin-right: 20px;
}
.cancelled {
	font-size: 26pt;
	width: 100px;
	height: 100px;
	top: 10px;
	left: 50%;
	margin-left: -50px;
	color: red;
	position: absolute;
	-webkit-transform: rotate(-20deg) skew(-20deg, 0);
	   -moz-transform: rotate(-20deg) skew(-20deg, 0);
		-ms-transform: rotate(-20deg) skew(-20deg, 0);
		 -o-transform: rotate(-20deg) skew(-20deg, 0);
			transform: rotate(-20deg) skew(-20deg, 0);
}
</style>
</head>
<body>
	<div class="div_details" id="wrap" style="margin-left: 4px;">
		<div id="content">
			<!-- <table id="header">
				<tr>
					<td>
						<?php if(isset($arr_data['logo'])): ?>
						<img src="<?php echo $arr_data['logo']; ?>" id="logo">
						<?php else: ?>
						<img src="/img/logo.svg"  style="margin: -50px -18px; height: 150px; width: 275px;">
						<?php endif; ?>
					</td>
					<td>
						<?php if(isset($arr_data['pdf_name'])): ?>
							<span id="title_pdf">
							<?php
								$name = trim($arr_data['pdf_name']);
								$first_letter = substr($name, 0, 1);
								$name = '<span>'.$first_letter.'</span>'.substr($name, 1);
								echo $name;
							?>
							</span>
							<?php if(!isset($arr_data['right_info'])){ ?>
							<div id="border"></div>
							<?php } ?>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td>
						<?php if( !isset($arr_data['company_address']) ): ?>
						<?php else: ?>
						<?php echo $arr_data['company_address']; ?>
						<?php endif; ?>
					</td>
					<td rowspan="2">
						<?php if(isset($arr_data['right_info'])): ?>
						<table id="right_info">
							<?php foreach($arr_data['right_info'] as $name => $value): ?>
							<tr>
								<td><?php echo $name ?>:</td>
								<td><?php echo $value; ?></td>
							</tr>
							<?php endforeach; ?>
						</table>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td>
						<table id="address">
							<tr>
								<td>
									<?php if(isset($arr_data['customer_address'])):  ?>
									<?php echo $arr_data['customer_address']; ?>
									<?php endif; ?>
								</td>
								<td>
									<?php if(isset($arr_data['shipping_address'])):  ?>
										<p><b>Shipping address:</b></p>
										<?php echo $arr_data['shipping_address']; ?>
									<?php endif; ?>
								</td>

							</tr>
						</table>
					</td>
				</tr>
			</table> -->
			<div class="wrapper">
				<div class="header">
					 <table cellpadding="2" cellspacing="0" style="width:100%;">
						<tr>
							<td></td>
							<td style="text-align: right; vertical-align: top; height: 1px"><?php echo isset($arr_data['current_day'])?$arr_data['current_day']:''; ?></td>
						</tr>
						<tr>
							<td rowspan="2" style="color:#1f1f1f;width:35%;vertical-align: top;">
								<img  src="<?php echo URL; ?>/img/logo_anvy.jpg" alt="" style="width:300px; height:190px"/>
							</td>
							<td style="vertical-align: top; padding-top: 30px; font-size: 18px; line-height: 25px"> <?php echo isset($arr_data['company_address'])?$arr_data['company_address']:''; ?></td>
						</tr>
					</table>
					 <?php if(!empty($left_info)){ ?>
					<div style="padding: 10px 0 20px;float:left">
						<p style="font-weight:bold;"><?php echo $left_info['label']; ?></p>
						<p><?php echo $left_info['name']; ?></p>
						<?php echo $left_info['address']; ?>
					</div>
					<?php } ?>
					<?php if(!empty($right_info)){ ?>
					<div style="padding: 10px 0 20px;float:right">
						<p style="font-weight:bold"><?php echo $right_info['label']; ?></p>
						<p><?php echo $right_info['name']; ?></p>
						<?php echo $right_info['address']; ?>
					</div>
					<?php } ?>
					<!-- <p class="clear"></p> -->
				</div>
			</div>
			<div id="pdf_content">
				<div class="row" id="heading">
					<span style="font-size: 44px !important">
						<?php if(isset($arr_data['heading'])): ?>
						<?php echo $arr_data['heading'];  ?>
						<?php endif; ?>
					</span><br />
					<span style="font-size: 25px !important;  font-weight: normal !important;">Ngày: <?php echo date('d-m-Y',$arr_data['date']) ?> </span>
				</div>
				<div id="company">
					<?php if(isset($arr_data['company'])): ?>
					<?php echo $arr_data['company']; ?>
					<?php endif; ?>
				</div>
				<table id="product_list">
					<thead>
						<tr>
							<?php
								$count = 1;
								if(isset($arr_data['title'])){
									$count = count($arr_data['title']);
									$title = '';
									foreach($arr_data['title'] as $key=>$value){
										if(is_numeric($key))
											$title .= '<th>'.$value.'</th>';
										else
											$title .= '<th style="'.$value.'">'.$key.'</th>';
									}
									echo $title;
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php if(isset($arr_data['content'])) echo $arr_data['content']; ?>
					</tbody>
				</table>

				<table id="sum_table" style="margin-top: 2px; margin-bottom: 25px;">
						 <tr  class="sum_title" >
							<td style="font-weight:bold;font-size: 21px !important; width:83.8%;" colspan="<?php echo $count-1; ?>">
								Tổng cộng:
							</td>
							<td style="font-weight:bold;font-size: 21px !important; width:16.1%; font-weight: 600;">
								<?php if(isset($arr_data['sum_amount'])) echo $arr_data['sum_amount']; ?>
							</td>
						</tr>
				</table>
				<div>
					<?php if(isset($arr_data['last_table'])): ?>
					<?php echo $arr_data['last_table'];  ?>
					<?php endif; ?>
				</div>
				 <div>
					<?php if(isset($arr_data['last_table_final'])): ?>
					<?php echo $arr_data['last_table_final'];  ?>
					<?php endif; ?>
				</div>
				<!-- <div class="footer" >
				<?php echo $arr_data['footer']; ?>
				</div> -->
			</div>
		</div>
	</div>
	<?php echo $this->Html->script('jquery-1.10.2.min'); ?>
	<script type="text/javascript">
		var height = Math.round($("#product_list").height());
		var width = Math.round($("#product_list").width());
		var degree = Math.round((height * 57.29) / width);
		var t = $("#product_list").offset().top + (height / 2) - ($(".cancelled").height() / 2) + 15;
		$(".cancelled").css({
			"top" : t + "px",
			/*"-webkit-transform": "rotate(-"+degree+"deg) skew(-"+degree+"deg, 0)",
			"-moz-transform": "rotate(-"+degree+"deg) skew(-"+degree+"deg, 0)",
			"-ms-transform": "rotate(-"+degree+"deg) skew(-"+degree+"deg, 0)",
			"-o-transform": "rotate(-"+degree+"deg) skew(-"+degree+"deg, 0)",
			"transform": "rotate(-"+degree+"deg) skew(-"+degree+"deg, 0)",*/
		});
	</script>
</body>
</html>