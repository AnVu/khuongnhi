<?php echo $this->element('../'.$name.'/tab_option'); $m = 0;?>
<div id="content">
	<form method="POST" id="sort_form">
		<div class="w_ul2 ul_res2">
			<!--Phần tiêu đề-->
			<ul class="ul_mag clear bg top_header_inner2 ul_res2" id="sort">
				<li class="hg_padd" style="width:.5%"></li>
				 <?php foreach ($list_field as $ks => $vls){?>
					<li class="hg_padd" style="cursor:pointer;<?php if(isset($vls['css'])) echo $vls['css']; ?>" title="Sort">
						<label style="cursor:pointer;"> <?php if(isset($arr_set['name'][$ks])) echo $arr_set['name'][$ks]; ?></label>
						<?php //if(!isset($vls['sort']) || (isset($vls['sort']) && $vls['sort']!=0)){?>
						<?php if(isset($vls['sort'])){?>
						<span id="<?php echo $ks ?>" class="desc<?php //if(isset($sort_type) && isset($sort_key) && $sort_key==$ks && $sort_type =='desc') echo 'asc'; else echo 'desc';?>" <?php //style="display:if(isset($sort_key) && $sort_key==$ks) echo 'block'; else echo 'none';;"?>></span>
						<?php }?>
					</li>
				 <?php } ?>
				 <li class="hg_padd bor_mt" style="width:.5%"></li>
			</ul>

			<br />
			<ul class="ul_mag clear bg" id="sort2" style="position: fixed">
				<li class="hg_padd" style="width:.5%"></li>
				<?php foreach ($list_field as $ks => $vls){?>
					<li class="hg_padd" style="cursor:pointer;<?php if(isset($vls['css'])) echo $vls['css']; ?>" title="Sort">
						<?php
						if ( $arr_set['name'][$ks]=='Mã sản phẩm') {
							// echo '<input type=text list=sku_search_list id="sku_search" name="sku" style="text-align: center;width: 100px">';

							echo '<select id="sku_search" name="sku" style="text-align: center;width: 125px">';
							foreach ($arr_sku as $key => $value) {
								echo '<option label="'.$value["sku"].'" value="'.$value["sku"].'">'.$value["sku"].'</option>';
							}
							echo '</datalist>';
						}

						if ( $arr_set['name'][$ks]=='Tên sản phẩm') {
							echo '<select id="name_search" name="name" style="width:100%">';
							foreach ($arr_names as $key => $value) {
								echo '<option value="'.$value["name"].'">'.$value["name"].'</option>';
							}
							echo '</select>';
						}
						if ( $arr_set['name'][$ks]=='Nhà cung cấp') {
							echo '<select id="company_name_search" name="company_id" style="width:100%">';
							foreach ($arr_suppliers as $key => $value) {
								echo '<option id="company_no_'.$value["no"].'" value="'.$value["_id"].'">'.$value["name"].'</option>';
							}
							echo '</select>';
						}
						if ( $arr_set['name'][$ks]=='Đơn vị bán') {
							echo '<select id="oum_search" name="oum" style="width:100%;">';
							echo '<option> </option>';
							foreach ($arr_oums as $key => $value) {
								echo '<option value="'.$value["value"].'">'.$value["name_vi"].'</option>';
							}
							echo '</select>';
						}
						if ( $arr_set['name'][$ks]=='Số lượng kho') {
							echo '<select id="in_stock_search" name="in_stock" style="width:100%">';
							echo '<option> </option>';
							echo '<option value="existed"> Còn Hàng </option>';
							foreach ($arr_in_stock as $key => $value) {
								echo '<option value="'.$key.'">'.$value.'</option>';
							}
							echo '</select>';
						}
						if ( $arr_set['name'][$ks]=='Giá NPP') {
							echo '<select id="sell_price_search" name="sell_price" style="width:100%">';
							echo '<option> </option>';
							foreach ($arr_sell_price as $key => $value) {
								echo '<option value="'.$key.'">'.number_format($value,0).'</option>';
							}
							echo '</select>';
						}
						if ( $arr_set['name'][$ks]=='Giá gốc') {
							echo '<select id="prime_cost_search" name="prime_cost" style="width:100%">';
							echo '<option> </option>';
							foreach ($arr_prime_cost as $key => $value) {
								echo '<option value="'.$key.'">'.number_format($value,0).'</option>';
							}
							echo '</select>';
						}
						if ( $arr_set['name'][$ks]=='Quy cách') {
							echo '<select id="specification_search" name="specification" style="width:100%">';
							 echo '<option> </option>';
							foreach ($arr_specification as $key => $value) {
								echo '<option value="'.$key.'">'.$value.'</option>';
							}
							echo '</select>';
						}
						if ( $arr_set['name'][$ks]=='Tình trạng') {
							echo '<select id="status_search" name="status" style="width:100%">';
							 echo '<option> </option>';
							foreach ($arr_status as $key => $value) {
								echo '<option value="'.$value["value"].'">'.$value['name_vi'].'</option>';
							}
							echo '</select>';
						}
						else
							echo '<input type="text" id="'.$ks.'_search" style="width:100%" name="'.$ks.'">'
						?>
					</li>
				<?php } ?>
				<li class="hg_padd bor_mt" style="width:.5%"></li>
			</ul>

			<!-- Phần thay data : load n=LIST_LIMIT cái đầu tiên, sau đó chạy ajax qv_ajax() để load tiếp n=LIST_LIMIT cái lần 2-->
			<div id="lists_view_content" style="margin-top: 10px">
				<?php echo $this->element('lists_ajax'); ?>
			</div>
		</div>
	</form>
</div>
<input type="hidden" id="str_search_name" value="">
<link href="<?php echo URL.'/theme/default/css/'?>select2.css" rel="stylesheet" />
<style type="text/css" media="screen">
	.select2-container--default .select2-selection--single{
		border-radius: 0 !important;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow b {
		border-color: #000 transparent transparent transparent;
		border-style: solid;
		border-width: 6px 3px 0 3px;
		height: 0;
		left: 50%;
		margin-left: -4px;
		margin-top: -2px;
		position: absolute;
		top: 25%;
		width: 0;
	}
	.select2-container--default.select2-container--open .select2-selection--single .select2-selection__arrow b {
		border-color: transparent transparent #000 transparent;
		border-width: 0 3px 6px 3px;
	}
	.select2-container--default .select2-results > .select2-results__options{
		overflow-x: hidden;
	}
	.select2-results__option{
		padding:4px;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 16px;
	}
	#select2-sku_search-container{
		text-align: center;
		margin-left: 15px;
	}
	.select2-results__option:first-child{
		height: 16px;
	}
	.select2-results__option{
		font-family: arial,sans-serif,verdana;
		font-size: 11px;
	}

	#search_by_name span{
		background: url("/theme/default/images/icon.png") no-repeat scroll -584px -74px transparent;
		width: 20px;
		height: 20px;
		display: block;
	}

</style>
<script src="<?php echo URL.'/theme/default/js/'?>select2.min.js"></script>
<script>
	var list_old_input = JSON.parse('<?php echo json_encode($old_input); ?>');
	console.log(list_old_input);
	$.each(list_old_input,function(key,value){
		if(key == '$or'){
			value = value[1].sku;
			key = 'sku';
		}

		element = $("[name="+key+"]");
		if(element.prop('tagName')=='INPUT'){
			element.val(value);
		}
		// if(element.prop('tagName')=='SELECT'){
		// 	element.find($("option[value="+value+"]")).prop('selected',true);
		// }
		if(element.prop('tagName')=='SELECT'){
			if( key=='company_id')
				element.find($("option[value="+value['$id']+"]")).prop('selected',true);
			else if(key=='in_stock' && typeof(value) =='object')
				element.find($("select[name=in_stock] option[value=existed]")).prop('selected',true);
			else if(key=='name' && typeof(value) =='object'){
				$("#str_search_name").val(value.regex);
			}
			else
				element.find($("select[name="+key+"] option")).each(function(key,element){
					if($(element).prop('value') == value){
						$(element).prop('selected',true);
					}else{
						$(element).prop('selected',false);
					}
				})
		}

	});

	$("#sku_search").select2();
	$("#name_search").select2();
	$("#company_name_search").select2();
	var create = 1;
	$("#name_search").on('select2:open',function(e){
		if(create){
			$("#select2-name_search-results").parent().prev().find('input').val($("#str_search_name").val());
			$("#select2-name_search-results").parent().prev().find('input').css('width','89.8%').css('float','left');
			$("#select2-name_search-results").parent().prev().append('<button id="search_by_name" onclick="search_list_by_name(this)"><span></span></button >')
			create =0;
		}
	});

</script>
<?php echo $this->element('js/lists_view'); ?>
<?php echo $this->element('js_list');?>