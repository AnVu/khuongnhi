<style>
	.list_color span.k-widget.k-colorpicker.k-header{
		float: left;
		margin-right: 10px;
		width: 15px;
		display: block !important;
	}
</style>

<?php
	$color = array('','','','');
	if(isset($arr_field['default'])){
		$color = explode(',',$arr_field['default']);
	}
	$count = count($color);
	if($count < 4)
		$count = 4;
?>

<li class="hg_padd line_mg center_txt list_color" style="height:auto; list-style: none;">
	<?php for($i = 0; $i < $count; $i++){ if($i && $i%6 ==0) echo '<div class="clear"></div>';?>
	<input name="status_color_<?php echo $i; ?>" id="status_color_<?php echo $i; ?>"  class="color-picker" value="<?php echo isset($color[$i]) ? $color[$i] : '';?>" type="text">
	<?php } ?>
</li>

<script type="text/javascript">
	$(function(){
		$(".list_color").parent().prev().append('&nbsp;<a title="Add Color" id="bt_add_color" onclick="addColor()"><span class="icon_down_tl top_f" style="float:right; margin-top: 3px;"></span></a>');
		$(".color-picker").each(function(){
			$(this).kendoColorPicker({
				buttons: false
			});
			var input = $(this);
			var colorPicker = $(this).data("kendoColorPicker");
			colorPicker.bind({
				change: function() {
					changeColor();
				}
			});
		});
	})
	function changeColor(e){
		var length = $(".color-picker",".list_color").length;
		var color = "";
		for(var i = 0; i < length; i++){
			if($("#status_color_"+i).val() == undefined || $("#status_color_"+i).val() == "") continue;
		 	color += $("#status_color_"+i).val()+',';
		}
		if(color.slice(-1) == ","){
			color = color.slice(0, color.length -1);
		}
		save_data('color_default',color);
	}
	function addColor(){
		var html = "";
		var length = $(".color-picker",".list_color").length;
		if(length == 10){
			alerts("Message","You cannot add more than 10 color pickers.");
			return false;
		}
		if(length && length%5 == 0){
			html += '<div class="clear"></div>';
		}
		html += '<input name="status_color_'+length+'" id="status_color_'+length+'" class="color-picker" value="" type="text" />';
		$(".list_color").append(html);
		$("#status_color_"+length).kendoColorPicker({
			buttons: false
		});
		var colorPicker = $("#status_color_"+length).data("kendoColorPicker");
		colorPicker.bind({
			change: function() {
				changeColor();
			}
		});
	}
</script>