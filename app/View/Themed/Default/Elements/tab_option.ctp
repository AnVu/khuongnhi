<?php
	$option_entry = array(
						'add' =>translate('New'),
						'delete'=>translate('Delete'),
						'entry_search' =>translate('Find'),
						'find_all'=>translate('Find all'),
						'support'=>translate('Support'),
						'history'=>translate('History'),
						//'omit' =>'Omit',
						//'sorts'=>'Sort',
						//'prints'=>'Print',
					);
	  $option_search_entry = array(
						'add' =>translate('New'),
						//'delete'=>'Delete',
						'entry_search' =>translate('Find'),
						'continues' =>translate('Continue'),
						'cancel' =>translate('Cancel'),
						//'omit'=>'Omit',
					);


      $actionlist_entry = array(
						'entry' =>translate('Entry'),
						'lists'=>translate('List'),
						'options' =>translate('Options'),
					);
      $actionlist_search_entry = array(
						'entry' =>'Entry',
						'lists'=>'List',
					);
      if(!$this->Common->check_permission($controller.'_@_entry_@_add',$arr_permission))
      	unset($option_entry['add'],$option_search_entry['add']);
      if(!$this->Common->check_permission($controller.'_@_entry_@_delete',$arr_permission))
      	unset($option_entry['delete']);
      if(!$this->Common->check_permission($controller.'_@_options_@_',$arr_permission,true))
      	unset($actionlist_entry['options']);
	  if($action=='entry_search'){
	  		$option	= $option_search_entry;
			$actionlist = $actionlist_search_entry;
	  }else{
	  		$option	= $option_entry;
			$actionlist = $actionlist_entry;
	  }

?>

<style type="text/css">
	.vonluu{float:left; margin-left: 1%; width: 10%; margin-top: 0.3%}
</style>


<div class="bg_menu">

    <ul class="menu_control float_left">
    	<?php foreach($option as $ks=>$vls){
			if($ks=='delete')
				$link = "onclick=\"confirm_delete('".URL."/".$controller."/".$ks."/".$iditem."');\" style=\"cursor:pointer;\" ";
			else if($ks=='continues')
				$link = 'onclick="search_entry();" style=" cursor:pointer;"';
			else
				$link = 'href="'.URL.'/'.$controller.'/'.$ks.'"';
		?>
        	<li>
            	<a <?php echo $link;?>  class="<?php if($ks!='support'&&$ks!='history') {?>entry_menu_<?php }else{ ?>icon <?php } ?><?php echo $ks;?> <?php if($ks==$action) echo 'active';?>">
            		<?php echo $vls;?>
                </a>
            </li>
        <?php }?>
    </ul>

	<!-- 	<?php if($controller == 'products'){?>
	<div style="width: 14.3%; float: right; margin-right:37%;">
		<input type="text" class="right_txt" id="vonluu_sum" readonly="readonly" value="" style="width: 59%; margin-top:-16%;" />
		<input type="button" id="print_pdf" name="print_pdf" />
	</div>
	<?php } ?> -->
	<?php if($controller == 'products'){?>

	<ul style="float:left; margin-left: 10%; margin-top:0.5%; color: white">Tổng vốn lưu: </ul>
	<ul class="vonluu" >
		<input type="text" id="vonluu_sum" readonly="readonly" value="" style="width: 100%; text-align: center; " />
	</ul>
	<ul style="margin-left: 1%; width: 5%; float:left; margin-top: 0.5% ">
		<a href="<?php echo URL; ?>/products/print_pdf_by_vonluu">IN PDF</a>
	</ul>
	<?php } ?>

    <ul class="menu_control2 float_right">
    	<?php foreach($actionlist as $ks=>$vls){?>
        	<li>
            	<a href="<?php echo URL.'/'.$controller.'/'.$ks; ?>" class="<?php if($action == $ks) echo 'active';?>">
					<?php echo $vls;?>
                 </a>
            </li>
         <?php }?>
    </ul>
</div>

<script type="text/javascript">
$(function(){
	$.ajax({
		url:"<?php echo URL.'/products/get_sum_vonluu' ?>",
		success: function (result){
			result = JSON.parse(result);
			$("#vonluu_sum").val(result.sum_vonluu);
		}
	});
})

</script>