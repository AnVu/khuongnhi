<?php echo $this->element('tab_option'); ?>
<span style="display: none" id="click_open_window_products" style=""></span>
<script type="text/javascript">
	function search_list_by_name(obj){
		var data={};
		data['search_from_list'] = true;
		data['name_like'] = $(obj).prev().val();
		console.log(data);
		$.ajax({
			url: '<?php echo URL . '/' . $controller; ?>/search_list' ,
			type: 'post',
			data: data,
			success: function(links) {
				if (links == '0')
					alert('No found items');
				else if (links != '')
					window.location.assign('lists');
			}
		});	
	}
	$(function(){
		$("#copy_and_create_new_record").attr("onclick","confirms_duplicate();");
		window_popup('products', 'Specify Product','product_name','click_open_window_products');
		$("#sort_form").delegate("select","change",function(){
		 	var select_id = $(this).attr('id');
		 	console.log(select_id);
		 	var field = $(this).attr('name');
		 	value = $("#" + select_id).val();
		 	if (field == "123") {
		 		data = {};
		 		data[field] = value;
		 		data[field + '_id'] = value;
		 		data['search_from_list'] = true;
			 	$.ajax({
					url: '<?php echo URL . '/' . $controller; ?>/search_list' ,
					type: 'post',
					data: data,
					success: function(links) {
						console.log(links);
						if (links == '0')
							alert('No found items');
						else if (links != '')
							window.location.assign('lists');
					}
				});
			}
		 	else if (field == "company_id" || field == "name" || field == "in_stock" || field == "sku" || field == "oum" || field == "specification" || field == 'sell_price' || field == 'prime_cost' || field == 'status' || field == 'no') {
		 		data = {};
		 		if(field=="sku" || field == "name"){
		 			if($("select[name="+field+"]").val() !=undefined && $("select[name="+field+"]").val() !=''){
		 				data[field] = $("select[name="+field+"]").val();
		 			}
		 		}else{
			 		arr_field = [
			 			'company_id',
			 			'in_stock',
			 			'oum',
			 			'specification',
			 			'sell_price',
			 			'prime_cost',
			 			'status'
			 		];
			 		for(i=0;i<arr_field.length;i++){
			 			if($("select[name="+arr_field[i]+"]").val() !=undefined && $("select[name="+arr_field[i]+"]").val() !=''){
			 				data[arr_field[i]] = $("select[name="+arr_field[i]+"]").val();
			 			}
			 		}
			 	}
		 		data['search_from_list'] = true;
		 		console.log(data);
		 		// return false;
			 	$.ajax({
					url: '<?php echo URL . '/' . $controller; ?>/search_list' ,
					type: 'post',
					data: data,
					success: function(links) {
						if (links == '0')
							alert('No found items');
						else if (links != '')
							window.location.assign('lists');
					}
				});
			}
		})

 		$("#sort_form").delegate("input","change",function(){
 			var id = $(this).attr('id');
		 	var field = $(this).attr('name');
		 	value = $("#" + id).val();
		 	data = {};


			if (field == "sell_price") {
		 		data['sell_price_cb'] = value;
			 	$.ajax({
					url: '<?php echo URL . '/' . $controller; ?>/search_list' ,
					type: 'post',
					data: data,
					success: function(links) {
						if (links == '0')
							alert('No found items');
						else if (links != '')
							window.location.assign('lists');
					}
				});
			}
			else {
				data[field] = value;
				$.ajax({
					url: '<?php echo URL . '/' . $controller; ?>/search_list' ,
					type: 'post',
					data: data,
					success: function(links) {
						if (links == '0')
							alert('No found items');
						else if (links != '')
							window.location.assign('lists');
					}
				});
			}
 		})

 		$("#company_name_search #company_no_").click(function(){
	        $(location).attr('href','<?php echo URL ?>/products/find_all');
	    })
	})
	function choose_product(){
		$("#click_open_window_products").click();
	}
	function confirms_duplicate(){
		confirms3('Message','Do you want "duplicate" or "replace" a product?',['Duplicate','Replace','']
		   	,function(){
				$.ajax({
					url: '<?php echo URL . '/' . $controller; ?>/duplicate_product' ,
					success: function(links) {
						if (links != '')
							window.location.assign(links);
					}
				});
			},function(){
				choose_product();
			},function(){
				return false;
			});
	}
	function after_choose_products(ids){
		$("#window_popup_productsproduct_name").data("kendoWindow").close();
		$.ajax({
			url: '<?php echo URL . '/' . $controller; ?>/replace_product' ,
			type: 'post',
			data: {id:ids},
			success: function(links) {
				if (links != '')
					window.location.assign(links);
			}
		});
	}
</script>