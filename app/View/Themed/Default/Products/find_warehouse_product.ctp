<div class="bg_menu"></div>
<div class="tab_1 half_width" style="width:45%;margin:5% auto;">
<span class="title_block bo_ra1">
    <span class="float_left">
        <span class="fl_dent">
            <h4>Tìm kiếm theo ngày nhập kho</h4>
        </span>
    </span>
</span>
<form id="form_contact" action="javascript:void(0)" method="POST">
    <div class="tab_2_inner">
        <p class="clear">
            <span class="label_1 float_left minw_lab2">Ngày tìm kiếm</span>
        </p>
        <div class="indent_new width_in float_left" style="width:61%">
            <?php
                echo $this->Form->input('date_equals',array(
                    'class'=>'input_4 float_left JtSelectDate validate',
                    'name'=>'date_equals',
                    'readonly' => true,
                    ));
                ?>
        </div>
        <p></p>
        <p class="clear">
            <span class="label_1 float_left minw_lab2" style="height:50%">Khoảng thời gian: </span>
        </p>
        <div class="indent_new width_in float_left" style="width:25%">
            <?php echo $this->Form->input('date_from', array(
                'class' => 'input_4 float_left JtSelectDate validate',
                'readonly' => true,
                'name'=>'date_from',
                'style' =>'padding:0 3%;'
                )); ?>
        </div>
        <span class="float_left" style="margin-top:2%">Tới</span>
        <?php echo $this->Form->input('date_to', array(
            'class' => 'input_4 float_left jt_input_search JtSelectDate validate',
            'readonly' => true,
            'name'=>'date_to',
            'style' =>'padding: .3% 1%;width: 30%;'
            )); ?>

        <p></p>
        <p class="clear"></p>
    </div>
    <div>
        <span class="title_block bo_ra2">
            <ul class="menu_control float_right" style="margin:-1% -5%;width:35%">
                <li><a href="javascript:void(0)" id="CancelButton" style="margin-top: 6%;font-size: 10px;line-height: 4px;border-radius: 3px;box-shadow: 0px 1px 2px">Cancel</a></li>
                <li style="margin-left:10%"><a style="margin-top: 6%;font-size: 10px;line-height: 4px;border-radius: 3px;box-shadow: 0px 1px 2px" id="ContinueButton" href="javascript:void(0)">Continue</a></li>
            </ul>
            <p class="clear"></p>
        </span>
        <input type="hidden" name="submit" />
    </div>
</form>

<script type="text/javascript">
    $(function(){
        $("#date_equals").change(function(){
            $("#date_from").val("");
            $("#date_to").val("");
        });
        $("#date_from, #date_to").change(function(){
            $("#date_equals").val("");
        });
        $('#ContinueButton').click(function(){
            var empty = check_Empty();
            if(empty==true)
            {
                confirms('Warning','Chưa nhập ngày tìm kiếm !');
                return false;
            } else{
                $.ajax({
                    url: '<?php echo URL; ?>/products/find_warehouse_product',
                    type: 'POST',
                    data:  $('input','#form_contact').serialize(),
                    success : function(result) {
                           window.location = result;
                    }
                 });
            }
        });
    });

    function check_Empty(){
        var status = true;
        $('.validate').each(function(){
            if($(this).val()!=''){
                status = false;
                return false;
            }
        });
        return status;
    }
</script>