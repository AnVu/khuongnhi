<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="format-detection" content="telephone=no" />
        <!-- WARNING: for iOS 7, remove the width=device-width and height=device-height attributes. See https://issues.apache.org/jira/browse/CB-4323 -->
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
        <link rel="stylesheet" type="text/css" href="http://code.jquery.com/mobile/1.4.1/jquery.mobile-1.4.1.min.css" />
        <title>Jobtraq</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.1/jquery.mobile-1.4.1.min.js"></script>
        <?php
            echo $this->Html->css('mobile_index');
            echo $this->Html->script('mobile_index');
            echo $this->Html->script('phonegap');
        ?>
    </head>
    <body id="no_bg">
        <div class="app reset_app">
            <div class="content reset_top">
                <div class="pages_content">
                    <div data-role="main" class="ui-content">
                        <form method="post" action="demoform.asp">
                          <fieldset data-role="collapsible" data-theme="b" data-content-theme="b">
                            <legend>Alert</legend>
                            <div data-role="controlgroup">
                              <div data-role="main" class="ui-content">
                                <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable">
                                  <thead>
                                    <tr>
                                      <th data-priority="6">ID</th>
                                      <th>Type</th>
                                      <th>Date</th>
                                      <th data-priority="1">Time</th>
                                      <th data-priority="2">Detail</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                      $STT = 0;
                                      foreach ($arr_data['task'] as $value) {
                                        $STT += 1;
                                        ?>
                                        <tr title="view detail">
                                            <td><?php echo $STT; ?></td>
                                            <td align="left"><?php echo translate('Task'); ?></td>
                                            <td align="center">
                                                <?php echo $this->Common->format_date($value['work_end']->sec, false); ?>
                                            </td>
                                            <td align="center"><?php echo date( "H:i", $value['work_end']->sec); ?></td>
                                            <td><?php echo $value['name']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php
                                        foreach ($arr_data['communication'] as $value) {
                                            $STT += 1;
                                            ?>
                                            <tr title="view detail">
                                                <td><?php echo $STT; ?></td>
                                                <td align="left"><?php echo translate('Message'); ?></td>
                                                <td align="center">
                                                    <?php echo $this->Common->format_date($value['date_modified']->sec, false); ?>
                                                </td>
                                                <td align="center"><?php echo date( "H:i", $value['date_modified']->sec); ?></td>
                                                <td ><?php echo $value['message_content']; ?></td>
                                            </tr>
                                    <?php } ?>
                                    <?php
                                        if($STT==0)
                                            echo '<td></td><td>No data</td>';
                                    ?>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </fieldset>
                          <fieldset data-role="collapsible" data-theme="b" data-content-theme="b">
                            <legend>Dashboard</legend>
                            <div data-role="controlgroup">
                              This is Dashboard content
                            </div>
                          <input type="submit" data-inline="true" value="Submit" data-theme="b">
                          </fieldset>
                          <fieldset data-role="collapsible" data-theme="b" data-content-theme="b">
                            <legend>Calendar</legend>
                            <div data-role="controlgroup">
                              This is Dashboard Calendar
                            </div>
                          <input type="submit" data-inline="true" value="Submit" data-theme="b">
                          </fieldset>
                          <fieldset data-role="collapsible" data-theme="b" data-content-theme="b">
                            <legend>Setup</legend>
                            <div data-role="controlgroup">
                               This is Dashboard Setup
                            </div>
                          <input type="submit" data-inline="true" value="Submit" data-theme="b">
                          </fieldset>
                          <fieldset data-role="collapsible" data-theme="b" data-content-theme="b">
                            <legend>Info / Help</legend>
                            <div data-role="controlgroup">
                              This is Dashboard Info / Help
                            </div>
                          <input type="submit" data-inline="true" value="Submit" data-theme="b">
                          </fieldset>
                        </form>
                      </div>
                </div>
            </div>
            <div data-role="footer">
                <p>&copy 2009 - 2013 Anvy Digital Imaging, All Rights Reserved.</p>
            </div>
        </div>
        <script type="text/javascript">
            app.initialize();
        </script>
    </body>
</html>
