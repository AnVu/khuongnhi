<div id="content">
    <div class="jt_ajax_note">Loading...</div>
    <!-- Title -->
    <div class="jbcont">
        <div class="jt_module_title float_left jt_t_left">
            <h1>
                <span class="md_center"></span>
                <span id="md_name">
                    <?php if(isset($products_name)) echo $products_name;?>
                </span>
                <a id="add_return_item"  target="_blank">
                    <input id="btn_return_item" value="+" style="width:3%;" type="button">
                </a>
             </h1>

        </div>

        <div class="jt_module_title " >

        </div>
    </div>

    <div id="option_list_form_auto_save">.

        <!-- Add form -->
        <form class="form_<?php echo $controller;?>_option" action="" method="post" class="float_left">
            <div class="clear_percent">
               <?php echo $this->element('box',array('panel'=>$arr_field_options['option'],'key'=>'option','arr_val'=>$arr_field_options['option']['option'])); ?>
            </div>
            <div class="clear"></div>
            <input type="submit" class="hidden" id="submit" name="submit" />
            <input type="hidden" name="submit" />
            <input type="hidden" id="products_id" value="<?php if(isset($products_id)) echo $products_id;?>" />
            <input type="hidden" name="product_key" id="subitems" value="<?php if(isset($subitems)) echo $subitems;?>" />
        </form>
        <span id="groupstr" style="display:none;"><?php if(isset($groupstr)) echo $groupstr;?></span>
    </div>
    <?php if(isset($products_note)) { ?>
    <div class="jbcont">
        <div class="jt_module_title float_left jt_t_left">
            <h2>
                <span class="md_center"></span>
                <span id="md_name">
                    <?php echo $products_note;?>
                </span>
            </h2>
        </div>
    </div>
    <?php } ?>
    <?php echo $this->element('../'.$name.'/js',array('no_alert_input'=>true)); ?>
</div>
<input type="hidden" id="subitems" value="<?php echo $subitems; ?>" />
<span class="hidden" id="option_popup"></span>
<span class="hidden" id="save_custom_product"></span>

<?php  echo $this->element('js/option_list_js');  ?>

<script type="text/javascript">
    $("#add_return_item").click(function() {
        if( $("#confirms_window" ).attr("id") == undefined ){
                var html = '<div id="password_confirm" >';
                    html +=    '<div class="jt_box" style=" width:100%;">';
                    html +=       '<div class="jt_box_line"><div class=" jt_box_label " style=" width:25%;"></div><div id="alert_message"></div></div>';
                    html +=       '<div class="jt_box_line">';
                    html +=          '<div class=" jt_box_label " style=" width:25%;height: 75px">Số lượng</div>';
                    html +=          '<div class="jt_box_field " style=" width:71%"><input name="return_quantity" id="return_quantity" class="input_1 float_left" type="text" value=""></div><input style="margin-top:2%" type="button" class="jt_confirms_window_cancel" id="confirms_cancel" value=" Cancel " /><input style="margin-top:2%" type="button" class="jt_confirms_window_ok" value=" Ok " id="confirms_ok" />';
                    html +=       '</div>';
                    html +=       '</div>';
                    html +=    '</div>';
                    html += '</div>';
                $('<div id="confirms_window" style="width: 99%; padding: 0px; overflow: auto;">'+html+'</div>').appendTo("body");
            }
                var confirms_window = $("#confirms_window");
                confirms_window.kendoWindow({
                    width: "355px",
                    height: "100px",
                    title: "Nhập số lượng trả",
                    visible: false,
                    activate: function(){
                      $('#password').focus();
                    }
                });

                confirms_window.data("kendoWindow").center();
                confirms_window.data("kendoWindow").open();
                var object = this;
                $("#confirms_ok").unbind("click");
                $("#confirms_ok").click(function() {
                    var values = {};
                    var subitems = $("#subitems").val();
                    var return_quantity = $("#return_quantity").val();
                    $.ajax({
                        url: "<?php echo URL.'/'.$controller.'/return_item' ?>",
                        type:"POST",
                        data: {key: subitems, return_quantity:return_quantity },
                        success: function(html){
                            $("#option_popup_window").html(html);
                        }
                    });
                    confirms_window.data("kendoWindow").destroy();
                });
                $('#confirms_cancel').click(function() {
                    $("#alert_message").html("");
                    $("#code").val($("#code").attr("rel"));
                    confirms_window.data("kendoWindow").destroy();
                });
        return false;
    });
</script>