<div class="clear_percent">
	<div class="clear_percent_19 float_left">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="fl_dent"><h4><?php echo translate('Tasks'); ?></h4></span>
			</span>
			<div class="title_small_once">
				<ul class="ul_mag clear bg3">
					<li class="hg_padd">
						<?php echo translate('Messsage type'); ?> <span class="normal">(<?php echo translate('click a task name to do on right'); ?>)</span>
					</li>
				</ul>
			</div>
			<div id="list_and_menu_height" class="container_same_category" style="height: 449px;overflow-y: auto">
				<ul class="find_list setup_menu">
					<li onclick="show_administrator_detail(this,'backup_restore')">
						<a href="javascript:void(0)" class="active">
							<?php echo translate('Backup - Restore');?>
						</a>
					</li>
					<li onclick="show_administrator_detail(this,'rebuild_product')">
						<a href="javascript:void(0)">
							<?php echo translate('Rebuild product');?>
						</a>
					</li>
					<li onclick="show_administrator_detail(this,'rebuild_quotation')">
						<a href="javascript:void(0)">
							<?php echo translate('Rebuild quotation');?>
						</a>
					</li>
					<li onclick="show_administrator_detail(this,'rebuild_sales_order')">
						<a href="javascript:void(0)">
							<?php echo translate('Rebuild sales oder');?>
						</a>
					</li>
					<li id ="check_update">
						<a href="javascript:void(0)">
							<?php echo translate('Check core update');?>
						</a>
					</li>
					<li onclick="show_administrator_detail(this,'custom_update')">
						<a href="javascript:void(0)">
							<?php echo translate('Custom Update');?>
						</a>
					</li>
				</ul>
			</div>
			<span class="title_block bo_ra2"></span>
		</div><!--END Tab1 -->
	</div>
	<div class="clear_percent_9_arrow float_left">
		<div class="full_width box_arrow">
			<span class="icon_emp" style="cursor:default"></span>
		</div>
	</div>
	<div class="clear_percent_11 float_left" id="administrator_detail">
		<!-- Detail -->
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("li:first", "#list_and_menu_height").click();
		$("#check_update").click(function(){
			$.ajax({
				url:  "<?php echo URL.'/update/check_update' ?>",
				success: function(result){
					result = $.parseJSON(result);
					if(result.Status == "Up to date"){
						alerts("Message",result.Message);
					} else {
						confirms("Message",result.Message+"<br />Do you want to update?"
						         ,function(){
						         	$.ajax({
						         		url : "<?php echo URL.'/update/do_update' ?>",
						         		success: function(result){
						         			alerts("Message",result);
						         		}
						         	});
						         },function(){
						         	return false;
						         });
					}
				}
			});
		});
	});

	function show_administrator_detail(object,id){
		$("#list_and_menu_height a").removeClass("active");
		$("a", object).addClass("active");
		$.ajax({
			url: '<?php echo URL; ?>/settings/administrator_'+id+'/',
			type:"POST",
			timeout: 15000,
			success: function(html){
				$("#administrator_detail").html(html);
			}
		});
	}
</script>