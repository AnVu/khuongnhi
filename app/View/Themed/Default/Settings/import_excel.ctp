<div class="tab_1 " style="height:80px; margin-top: 25px; width: 40%">
    <span class="title_block bo_ra1" >
        <span class="fl_dent">
            <h4>
                <?php echo translate('Import'); ?>
            </h4>
        </span>
    </span>
    <div style="margin-top:10px" >
	    <span>
		    <form id="import_excel_form" method="post" enctype="multipart/form-data" action="javascript:void(0)">
			    <input type="file" name="import_excel" id="import_excel" style="width:40%;"/>
			    <button id="upload_button" onclick="js_import_excel()" style="margin-left: 2%;" >Submit</button>
			    <span id='message' style='color:#ff0000; text-decoration: none;display:none; text-align: center; width:55%;' href=''>Waiting...</span>
			</form>
		</span>
    </div>
</div>

<script type="text/javascript">
	function js_import_excel(){
		$.ajax({
			url:'<?php echo URL; ?>/settings/import_excel',
			type:'POST',
			data: {file: $("import_excel").val()},
			success:function(){
				$("#message").fadeIn(500);
			}
		});
	}
</script>