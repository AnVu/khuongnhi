<div class="tab_1 full_width" style="height:150px;">
    <span class="title_block bo_ra1">
        <span class="fl_dent">
            <h4>
                Custom Update
            </h4>
        </span>
    </span>
    <div class="custom_update_content" style="margin:12px;">
    	<form id="custom_update_form" method="post" enctype="multipart/form-data" >
			<input type="file" accept="application/zip" id="custom_update" name="custom_update" action="<?php echo URL.'/settings/administrator_custom_update' ?>" />
			<input type="button" id="update" value="Update" />
		</form>
    </div>
</div>
<script type="text/javascript">
$(function(){
	$("#custom_update_form").submit(function(){
		$.ajax({
     		url : "<?php echo URL.'/settings/administrator_custom_update' ?>",
     		type: "POST",
     		data: new FormData(this),
			processData: false,
			contentType: false,
     		success: function(result){
     			alerts("Message",result);
     		}
     	});
		return false;
	})
	$("#update").click(function(){
		if($("#custom_update").val() == ""){
			alerts("Message","Choose a file to upload first.");
			return false;
		}
		confirms("Message","Do you want to update?",
		         function(){
		         	$("#custom_update").submit();
		         }, function(){
		         	return false;
		         })
	});
})
</script>