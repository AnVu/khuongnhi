<style type="text/css">
#confirms_window .jt_confirms_window_ok{
	width: 15%;
	height: 18%;
	margin-top: 14%;
	margin-left: 80%;
}
.changePadding ul li{
	padding-left: 4px !important;
}
.clear_percent_40 {
width: 39%;
}
.clear_percent_11 {
width: 77%;
}
</style>
<div class="clear_percent">
	<div class="clear_percent_40 float_left">
		<div class="tab_1 full_width changePadding">
			<span class="title_block bo_ra1">
				<span class="fl_dent"><h4><?php echo translate('All cached'); ?></h4></span>
				<a title="Delete all cache" href="javascript:void(0)" onclick="delete_cache('all')" style="text-decoration: none; font-weight: bold;"><span style="margin-top: 1px;">X</span></a>
			</span>
			<ul class="ul_mag clear bg3">
				<li class="hg_padd center_text no_border"><?php echo translate('Cached'); ?></li>
			</ul>
			<div id="cache_height" class="container_same_category" style="height: 449px;overflow-y: auto">
				<ul class="find_list setup_menu">
				<?php $i = 1;$count = 0;
				foreach ($arr_cache as $key => $value) { $count += 1; ?>
					<li>
						<a class="clickfirst" href="javascript:void(0)" id="cache_<?php echo $value; ?>" onclick="cache_detail(this, '<?php echo $value; ?>')"><?php echo $value; ?></a>
					</li>
				<?php $i = 3 - $i;
				}
				echo '</ul>';

				if ($count < 20) {
				$count = 20 - $count;
				for ($j = 0; $j < $count; $j++) {
					$i = 3 - $i;
					?>
					<ul class="find_list">
					</ul>
					<?php
				}
			}
			?>

			</div>
			<span class="title_block bo_ra2"></span>
		</div><!--END Tab1 -->
	</div>
	<div class="clear_percent_9_arrow float_left">
		<div class="full_width box_arrow">
			<span class="icon_emp" style="cursor:default"></span>
		</div>
	</div>
	<div class="float_left" style="width: 56%" id="cache_detail">
		<!-- Detail -->
	</div>
</div>

<style type="text/css">
#cache_height ul:hover, #cache_height ul:hover input{
	background-color: #B8B8B8;
}
</style>

<script type="text/javascript">
	$(window).load(function() {
		$('.container_same_category').mCustomScrollbar({
			scrollButtons:{
				enable:false
			}
		});
	});
</script>

<script type="text/javascript">

	$(function(){
		$('.container_same_category').mCustomScrollbar({
			scrollButtons:{
				enable:false
			}
		});
		$(".clickfirst:first", "#cache_height").click(); // click menu li dau tien khi page load xong
	});

	// click cot 2
	function cache_detail(object, name){

		$("#cache_height a").removeClass("active").parents("li").removeClass("active");
		$(object).addClass("active").parents("li").addClass("active");

		get_detail(name);
	}

	function get_detail(name){
		$.ajax({
			url: '<?php echo URL; ?>/settings/cache_detail/' + name,
			timeout: 15000,
			success: function(html){
				$("div#cache_detail").html(html);
				$('.container_same_category', "#cache_detail").mCustomScrollbar({
					scrollButtons:{
						enable:false
					}
				});
			}
		});
	}

	function delete_cache(name){
		$.ajax({
			url: '<?php echo URL; ?>/settings/cache_delete/' + name,
			timeout: 15000,
			success: function(result){
				if(result=="ok"){
					if(name == "all")
						$("#cache","#settings_ul_nav_setup").click();
					else
						$("#cache_"+name).parent().fadeOut(400,function(){
							$(this).remove();
							$(".clickfirst:first", "#cache_height").click();
						});
				} else {
					alerts("Message",result);
				}
			}
		});
	}

</script>