<style>
	.tab_backup_restore{
		margin:12px;
	}
</style>
<div class="tab_1 full_width" style="height:85px;">
    <span class="title_block bo_ra1">
        <span class="fl_dent">
            <h4>
                <?php echo translate('Backup'); ?>
            </h4>
        </span>
    </span>
    <div class="tab_backup_restore">
	    <select id='select_folder_backup' style="width:50%;">Choose folder backup:
	    	<option value='<?php echo ROOT."/app/webroot/upload"; ?>' selected='selected'>
	    		Folder Upload
	    	</option>
	    	<option value='<?php echo ROOT."/app/webroot/css"; ?>'>
	    		Folder CSS
	    	</option>
	    	<option value='<?php echo ROOT."/app/webroot/js"; ?>'>
	    		Folder JS
	    	</option>
	    	<option value='<?php echo ROOT; ?>'>
	    		All
	    	</option>

	    </select>
	    <button id="backup_button" onclick="$('#notice_bk').text('Waiting...').show();backup();" style="margin: auto 2%;"><?php echo translate('Backup'); ?></button>
	    <button id="clear_backup_button" onclick="clear_backup()" disabled="">Clear backup file</button>
	    <span id='notice_bk' style='color:#ff0000; text-decoration: none;display:none; text-align: center; width:55%;' href=''>Waiting...</span>
    </div>
</div>

<div class="tab_1 full_width" style="height:110px; margin-top: 25px;">
    <span class="title_block bo_ra1">
        <span class="fl_dent">
            <h4>
                <?php echo translate('Restore'); ?>
            </h4>
        </span>
    </span>
    <div class="tab_backup_restore">
	    <span>
		    <form id="restore_form" method="post" enctype="multipart/form-data" action="javascript:void(0)">
			    <input type="file" name="file_backup" id="file_backup" style="width:50%;"/>
			    <button id="upload_button" onclick="$('#notice_res').text('Uploading...').show();upload();" style="margin-left: 2%;" >Upload</button>
			</form>
		</span>

		<span>
			Folder restore: <?php echo ROOT;?><input type="text" name="folder_restore" id="folder_restore" style="width:38.1%;" value="/" />
			<input type="hidden" id='check_upload' value='0'>
			<button id="restore_button" onclick="restore()" style="margin-left: 2%;" disabled=""><?php echo translate('Restore'); ?></button>
		</span>
		<span id='notice_res' style='color:#ff0000; text-decoration: none; text-align: center; width:55%;' href=''>
		</span>
    </div>
</div>

<div id="content_backup_restore"></div>
<script type="text/javascript">

	$('#file_backup').on('click',function(){
		$('#restore_button').prop('disabled',true);
		$('#file_backup').prop('readonly',false);
	});

	var folder_restore=$('#folder_restore');
	folder_restore.keypress(function(){
		 if($('#check_upload').val()=='1'){
		 	if(folder_restore.val()!=""){
			 	$('#restore_button').prop('disabled',false);
			 	$('#file_backup').prop('readonly',true);
			 }
			 else{
			  	$('#restore_button').prop('disabled',true);
			 	$('#file_backup').prop('readonly',false);
			 }

		}
	});

	$('#select_folder_backup').change(function(){
		$('#notice_bk').html("");
	});

	function backup () {

		var folder=$('#select_folder_backup').val();
		$.ajax({
			url:'<?php echo URL;?>/settings/administrator_backup_restore/',
			type:'POST',
			data:{task:'backup',folder:folder},
			success:function(){

			}
		})
		.done(function (html) {
			$('#notice_bk').html(html);
			$('#clear_backup_button').prop('disabled',false);
			$('#backup_button').prop('disabled',true);
			$('#select_folder_backup').prop('disabled',true);
		});
	}


	function upload() {

    	var fileInput = document.getElementById('file_backup');
    	if(fileInput.value!=""){
			var file = fileInput.files[0];
			var file_size=fileInput.files[0].size;
			if(file_size>22){
				if(file_size>8388608){
					$('#notice_res').text("File size "+file_size+" bytes exceeds the limit of 8388608 bytes");
					setTimeout(function(){
							$('#notice_res').fadeOut(500);
						}, 3000);
				}
				else{
				    // fd dung de luu gia tri goi len
				    var fd = new FormData();
				    fd.append('file', file);
				    fd.append('task', 'upload');
				    fd.append('folder_restore','folder_resstore')
				    // xhr dung de goi data bang ajax
				    var xhr = new XMLHttpRequest();
				    xhr.open('POST', '<?php echo URL;?>/settings/administrator_backup_restore/', true);


				    xhr.onload = function(result) {
				      if (this.status == 200) {
				       $('#notice_res').show();
				       result.innerHTML=this.response;
				       document.getElementById('notice_res').innerHTML=result.innerHTML;
				       if (result.innerHTML=='Uploading success'||result.innerHTML=='File already exists') {
					       if($('#folder_restore').val()!=""){
					       		$('#restore_button').prop('disabled',false);
						 	$('#file_backup').prop('readonly',true);
					       		$('#check_upload').val('1');
					       }else{
					       		$('#check_upload').val('1');
					       }
					       setTimeout(function(){
							$('#notice_res').fadeOut(500);
							}, 3000);
					    }
					    else{
					    	$('#check_upload').val('0');
					    	setTimeout(function(){
							$('#notice_res').fadeOut(500);
						}, 3000);
					    }

				      }
				    }

				    xhr.send(fd);
				}
			}
			else{
				$('#notice_res').text("File is empty");
				setTimeout(function(){
							$('#notice_res').fadeOut(500);
						}, 3000);
			}
		}
		else{
			$('#notice_res').text('Please choose a file to upload');
		}
	}


	function restore(){
		$('#restore_button').prop('disabled',true);
			 	$('#file_backup').prop('readonly',false);
		$.ajax({
			url:'<?php echo URL;?>/settings/administrator_backup_restore/',
			type:'POST',
			data:{task:'restore',folder:folder_restore.val(),file:$('#file_backup').val()},
			success:function(){
				$("#notice_res").fadeIn(500);
			}
		})
		.done(function (html) {
			$('#notice_res').html(html);
			setTimeout(function(){
				$('#notice_res').fadeOut(500);
			}, 3000);
		});
	}


	function clear_backup(){
		$.ajax({
			url:'<?php echo URL;?>/settings/administrator_backup_restore/',
			type:'POST',
			data:{task:'clear_backup'},
			success:function(){
				$("#notice_bk").fadeIn(500);
			}
		})
		.done(function (html) {
			$('#notice_bk').html(html);
			$('#clear_backup_button').prop('disabled',true);
			$('#backup_button').prop('disabled',false);
			$('#select_folder_backup').prop('disabled',false);

		});
	}
</script>