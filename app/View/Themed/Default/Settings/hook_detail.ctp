<div class="tab_1 full_width">
    <span class="title_block bo_ra1">
        <span class="fl_dent">
            <h4>
                <?php echo $hook_value['setting_name'];?>
            </h4>
        </span>
    </span>
    <ul class="ul_mag clear bg3">
    	<?php
    		$width = 95/count($hook_value['option']);
    		foreach($hook_value['option'] as $value)
    			echo '<li class="hg_padd" style="width:'.$width.'%">'.ucfirst(str_replace('_', ' ', $value['value'])).'</li>';
    	?>
    </ul>
    <input type="hidden" id="all_field_of_option" >
    <div class="hook_content" style="height:449px">
    	<input type="hidden" name="data[_id]" value="<?php echo $hook_value['_id'] ?>" />
        <ul class="ul_mag clear bg1">
        	<?php foreach($hook_value['option'] as $key=>$value){ ?>
            <li class="hg_padd line_mg" style="width:<?php echo $width ?>%; position: relative">
                <?php
                echo $this->Form->input('hook'.$value['value'], array(
                    'class' => 'input_inner input_inner_w bg1',
                    'value' => $value['name'],
                    'name'	=> 'data[option]['.$key.'][name]'
                ));
                ?>
                <input type="hidden" name="data[option][<?php echo $key ?>][value]" value="<?php echo $value['value'] ?>" />
            </li>
            <?php } ?>
        </ul>
    </div>

    <span class="title_block bo_ra2">
        <span class="float_left bt_block"><?php echo translate('Edit values for list'); ?>.</span>
    </span>
</div>
<script type="text/javascript">
$(function(){
	$("input",".hook_content").change(function(){
		$.ajax({
			url:"<?php echo URL; ?>/settings/hook_detail/<?php echo $hook_value['setting_value']; ?>",
			type: 'POST',
			data: $("input",".hook_content").serialize(),
			success: function(html){
				if(html!='')
					$("#hook_detail").html(html);
			}
		});
	});
})
</script>