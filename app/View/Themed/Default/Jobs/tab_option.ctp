<?php echo $this->element('entry_tab_option'); ?>
<script type="text/javascript">
	$(function(){
		$("#combine_salesorders").removeAttr("onclick");
		$("#combine_salesorders").click(function(){
			$.ajax({
				url: "<?php echo URL.'/jobs/combine_salesorders' ?>",
				success: function(result){
					result = $.parseJSON(result);
					if(result.status != 'ok')
						alerts("Message",result.message);
					else
						window.location = result.url;
				}
			})
		});
	})
</script>