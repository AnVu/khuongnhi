<?php

    $i = 2;
    $sum_quotations = 0;
    $delete = $this->Common->check_permission($controller.'_@_entry_@_delete',$arr_permission);
?><br>
<?php foreach ($arr_jobs as $value): ?>
    <?php
    if ($i == 2)
        $i = $i - 1;
    else
        $i = $i + 1;
    ?>
    <ul class="ul_mag clear bg<?php echo $i ?>" id="jobs_<?php echo (string) $value['_id']; ?>">
        <li class="hg_padd" style="width:1%">
            <a style="color: blue" href="<?php echo URL; ?>/jobs/entry/<?php echo $value['_id']; ?>"><span class="icon_emp"></span></a>
        </li>
        <li class="hg_padd center_txt" style="width:4%"><?php echo $value['no']; ?></li>
        <li class="hg_padd" style="width:13%">
            <?php if (isset($value['company_id']) && is_object($value['company_id']) ) { ?>
                            <a href="<?php echo URL; ?>/companies/entry/<?php echo $value['company_id']; ?>">
                                <?php echo $value['company_name']; ?>
                            </a>
                        <?php } ?>
        </li>
        <li class="hg_padd" style="width:11%"><?php echo $value['name']; ?></li>
        <li class="hg_padd center_txt" style="width:8%"><?php echo $this->Common->format_date($value['work_end']->sec, false); ?></li>
        <li class="hg_padd" style="width:6%"><?php echo $value['status']; ?></li>
        <li class="hg_padd right_txt" style="width:10%">
        <?php
            $arr_quote = $quotation->select_all(array(
                'arr_where' => array(
                                     'job_id' => new MongoId($value['_id']),
                                     'quotation_status'=>array('$ne'=>'Cancelled'),
                                     ),
                'arr_field' => array('_id','sum_sub_total','status')
            ));
            $sum = 0;
            foreach($arr_quote as $key=>$quote){
                $minimum = $_controller->get_minimum_order('Quotation',$quote['_id']);
                if($quote['sum_sub_total']<$minimum)
                    $sum += $minimum;
                else
                    $sum += $quote['sum_sub_total'];
            }
            $sum_quotations += $sum;
            echo $this->Common->format_currency((float)$sum);
        ?>
        </li>


		<?php //SALES TOTAL
            $arr_salesorder = $salesorder->select_all(array(
                'arr_where' => array(
                                     'job_id' => new MongoId($value['_id']),
                                     'status'=>array('$ne'=>'Cancelled')
                                     ),
                'arr_field' => array('_id','sum_sub_total','status')
            ));
            $order_sum = 0; $m = $iscolor = 0;
            foreach($arr_salesorder as $key=>$order){
                $minimum = $_controller->get_minimum_order('Salesorder',$order['_id']);
                if($order['sum_sub_total']<$minimum)
                    $order_sum += $minimum;
                else
                    $order_sum += $order['sum_sub_total'];

				if(isset($order['status']) && $order['status']!='Completed')
					$iscolor++;
				$m++;

            }
		?>

        <?php // INVOICE TOTAL
            $arr_salesinvoice = $salesinvoice->select_all(array(
                'arr_where' => array(
                                     'job_id' => new MongoId($value['_id']),
                                     'invoice_status'=>array('$ne'=>'Cancelled'),
                                     ),
                'arr_field' => array('_id','sum_sub_total','invoice_status')
            ));
            $invoice_sum = 0;$m = $isred = $isblue = 0;
            foreach($arr_salesinvoice as $key=>$invoice){
                $minimum = $_controller->get_minimum_order('Salesinvoice',$invoice['_id']);
                if($invoice['sum_sub_total']<$minimum)
                    $invoice_sum += $minimum;
                else
                    $invoice_sum += $invoice['sum_sub_total'];

                if(isset($invoice['invoice_status']) && $invoice['invoice_status']!='Invoiced')
                    $isred++;
                if(isset($invoice['invoice_status']) && $invoice['invoice_status']!='Paid')
                    $isblue++;
                $m++;
            }
        ?>
        <?php
            $unbalance = '';
            if(abs(($order_sum - $invoice_sum) / ($invoice_sum==0? 1 :$invoice_sum) ) > 0.001)
                $unbalance = 'background-color: rgb(247, 215, 134);';
        ?>
        <li class="hg_padd right_txt" style="width:10%;<?php if($unbalance){ echo $unbalance; }  if($iscolor==0 && $m>0) echo 'color:red;';?>">
            <?php echo $this->Common->format_currency((float)$order_sum);?>
        </li>




        <li class="hg_padd right_txt" style="width:10%;<?php if($unbalance){ echo $unbalance; }  if($isred==0 && $m>0) { echo 'color:red;'; } else if($isblue==0 && $m>0) { echo 'color:blue;'; }?>">
        	<?php echo $this->Common->format_currency((float)$invoice_sum);?>
        </li>


        <li class="hg_padd" style="width:8%"><?php if (isset($value['type']) && isset($arr_jobs_type[$value['type']])) echo $arr_jobs_type[$value['type']]; ?></li>
        <li class="hg_padd center_txt" style="width:3%">
            <?php
            if ( in_array( $value['status_id'], array('New', 'Confirmed')) && isset($value['work_end']) && is_object($value['work_end'])) {
                if ($value['work_end']->sec < strtotime('now')) {
                    echo '<span class="Late">X</span>';
                }
            }?>
        </li>
        <li class="hg_padd bor_mt" style="width:3%">
            <?php if($delete): ?>
            <div class="middle_check">
                <a href="javascript:void(0)" title="Delete link" onclick="jobs_lists_delete('<?php echo $value['_id']; ?>')">
                    <span class="icon_remove2"></span>
                </a>
            </div>
        <?php endif; ?>
        </li>
    </ul>
<?php endforeach;
echo $this->element('popup/pagination_lists');
?>