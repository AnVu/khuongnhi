<?php echo $this->element('tab_option'); ?>
<script type="text/javascript">
	$(function(){
        $("#batch_invoices_via_email").attr("onclick","");
        $("#batch_invoices_via_mail").attr("onclick","");
		$('#create_shipping').attr('onclick', 'create_shipping()');
        batch_popup("Specify Sales invoice","","batch_invoices_via_email");
        batch_popup("Specify Sales invoice","pdf_only","batch_invoices_via_mail");
	});

	function create_shipping(){
            $.ajax({
                url: "<?php echo URL; ?>/<?php echo $controller;?>/create_shipping",
                type: "POST",
                success: function(result){
                    result = JSON.parse(result);
                    if(result.status != "ok")
                        alerts("Message",result.message);
                    else
                        location.replace(result.url);
                }
            });
    }
function batch_popup(title, key, key_click_open, parameter_get, force_re_install){
    controller = "salesinvoices";
    // ---- set default ----


    if( key == undefined ){
        key = "";
    }

    if( parameter_get == undefined ){
        parameter_get = "";
    }

    if( force_re_install == undefined ){
        force_re_install = "";
    }

    var div_popup_id = controller + key;

    // -------------- bắt đầu code -------------------------------

    // Kiểm tra tồn tại trước khi tạo lại
    if( $("#window_popup_" + div_popup_id).attr("id") == undefined ){
        $('<div id="window_popup_' + div_popup_id + '" style="display:none; min-width:300px;"></div>').appendTo("body");

    }else if(force_re_install != "force_re_install") {

        // không cần tạo lại khung window popup mà chỉ cần bind click lại như cũ là được
        var window_popup = $("#window_popup_" + div_popup_id);
        if( key_click_open != undefined && key_click_open != "" ){
            var undo = $("#" + key_click_open);
        }else{
            var undo = $("#click_open_window_" + div_popup_id);
        }

        undo.bind("click", function() {
            if( force_re_install != "no_auto_close")
            $(".k-window").fadeOut('slow');
            window_popup.data("kendoWindow").center();
            window_popup.data("kendoWindow").open();
            $(".container_same_category", window_popup).mCustomScrollbar({
                scrollButtons:{
                    enable:false
                },
                advanced:{
                    updateOnContentResize: true,
                    autoScrollOnFocus: false,
                }
            });
        });
        return false;
        // end
    }

    if( title == undefined ){
        alert("You must set a title for function: window_popup(controller, title, key, key_click_open) . Thanks.");
    }

    var window_popup = $("#window_popup_" + div_popup_id);

    if( key_click_open != undefined && key_click_open != "" ){
        var undo = $("#" + key_click_open);
    }else{
        var undo = $("#click_open_window_" + div_popup_id);
    }

    // refesh lại kendo window để hiển thị các giá trị chọn mới
    if( force_re_install == "force_re_install" ){

        if( window_popup.attr("id") == undefined || $.trim(window_popup.html()) == "" ){
            console.log("Bạn chưa khai báo window popup '" + "#window_popup_" + div_popup_id + "', vui lòng kiểm tra lại code, thanks.");
        }else{
            window_popup.data("kendoWindow").refresh({
                url: "<?php echo URL; ?>/"+ controller +"/popup/" + key + parameter_get
            });
            return true;
        }

    }

    undo.bind("click", function() {
        if( force_re_install != "no_auto_close")
            $(".k-window").fadeOut('slow');
        window_popup.data("kendoWindow").center();
        window_popup.data("kendoWindow").open();
        $(".container_same_category", window_popup).mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            advanced:{
                updateOnContentResize: true,
                autoScrollOnFocus: false,
            }
        });
    });

    window_popup.kendoWindow({
        iframe: false,
        actions: ["Maximize", "Close"],
        content: "<?php echo URL; ?>/"+ controller +"/batch_invoices_popup/" + key + parameter_get,
        visible: false,
        width: "auto",
        title: title,
        open: onOpen,
        // activate: onActivate,
    }).data("kendoWindow").center();

}

</script>