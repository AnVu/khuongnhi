<?php echo $this->element('js/line'); ?>
<script>
$(document).ready(function() {
	$(".viewcheck_docket_check").change(function(){
		var ids  = $(this).attr("id").split("_");
		var index = ids.length;
		var ids = ids[index-1];
		var value = 0;
		if($(this).is(":checked"))
			value = 1;
		$.ajax({
			url: "<?php echo URL.'/'.$controller.'/save_docket_check' ?>",
			type: "POST",
			data: {key:ids,value:value},
			success: function(result){
				if(result!='ok')
					alerts('Message',result);
			}
		})
	});
	$("#docket_check_checkall").change(function(){
		$(".viewcheck_docket_check").removeAttr("checked");
		var data = Object();
		if($(this).is(":checked")){
			$(".viewcheck_docket_check").prop("checked",true);
			data['all'] = true;
		} else
			data['all'] = false;
		$.ajax({
			url: "<?php echo URL.'/'.$controller.'/save_docket_check/' ?>",
			type: "POST",
			data: data,
			success: function(result){
				if(result!='ok')
					alerts('Message',result);
			}
		})
	});
	$(".viewprice_custom_unit_price").focus(function(){
		$(this).attr("rel",$(this).val());
	});
	var storeExtraRow = $("#listbox_products_Extra_Row").html();
	//fix combobox hide by div
	//fixHiddenCombobox();
	//end fix
	/*var is_add = $("#is_add").val();
	if(is_add){
		var ids = $(".choice_sku:last","#container_products").attr("id");
		if(ids!=undefined){
			ids  = ids.split("_");
			var ind = ids.length;
			var ids = ids[ind-1];
			if($("#txt_products_name_"+ids).offset()!=undefined){
				var offset = $("#txt_products_name_"+ids).offset().top;
				$("#container_products").mCustomScrollbar("scrollTo", "bottom");
			}
		}
		$("#is_add").val(0);
	}*/
	//tạo thêm 1 products line mới
	$("#bt_add_products").click(function() {
		var datas = {
			'products_id' : '',
			'products_name' : 'This is new record. Click for edit ',
			'quantity' : 0,
			'sku':'',
			'adj_qty' : 0,
			'sizew_unit' : 'in',
			'sizeh_unit' : 'in',
			'sell_by' : 'area',
			'oum' : 'Sq.ft.',
		};
		$("#is_add").val(1);
		save_option('products',datas,'',1,'line_entry','add');
	});
	$(".details").kendoTooltip({
		autoHide: false,
    	showOn: "click",
    	position: "right",
    	width: 600,
      	height: 250,
    	content: function(e) {
          	var Object = e.target;
			var id = Object.attr('id');
			var value = $("#div_"+id).html();
			var html = '<textarea class="details_store" id="textarea_'+id+'">'+value+'</textarea>';
			return html;
        }
	});
	$("body").delegate(".details_store","change",function(){
		var ids  = $(this).attr("id").split("_");
		var index = ids.length;
		var ids = ids[index-1];
		var value = $(this).val();
		var data = {"details":value};

		$("#div_"+$(this).attr("id")).html(value);
		save_option('products',data,ids,0,'line_entry','update');
	});
	$(".del_products").focusout(function(){
		ajax_note("");
		var ids = $(this).attr("id");
			ids = ids.split("_");
		var index = ids.length;
		ids  = parseInt(ids[index-1])+1;
		$(".jt_line_over").removeClass('jt_line_over');
		$("#listbox_products_"+ids).addClass('jt_line_over');
	});
	$(".icon_emp4").click(function(){
		var ids = $('#mongo_id').val();
		window.location.assign("<?php echo URL;?>/quotations/rfqs_list/"+ids);
	});
	$(".rowedit input").keydown(function(e){
		var keycode = e.keyCode;
		if(keycode==39){
			if($(this).parent().hasClass('combobox'))
				nextinput = $(this).parent().parent().parent().next().find('input');
			else
			nextinput = $(this).parent().parent().next().find('input');
			nextinput.focus();
			nextinput.click();
		}
		if(keycode==38){
			var id_current = $(this).attr("id").split('_');
			id_current = parseInt(id_current[id_current.length - 1]);
			if(id_current > 0){
				ul_prev = $("#listbox_products_"+(id_current-1));
				class_input = $(this).attr("class").split(" ");
				class_input = class_input[class_input.length-1];
				input_prev = ul_prev.find("."+class_input);
				input_prev.focus();
				input_prev.click();
			}
		}
		if(keycode==37){
			if($(this).parent().hasClass('combobox'))
				nextinput = $(this).parent().parent().parent().prev().find('input');
			else
				nextinput = $(this).parent().parent().prev().find('input');
			nextinput.focus();
			nextinput.click();
		}
		if(keycode==40){
			var id_current = $(this).attr("id").split('_');
			id_current = parseInt(id_current[id_current.length - 1]);
			if(id_current >= 0){
				ul_prev = $("#listbox_products_"+(id_current+1));
				class_input = $(this).attr("class").split(" ");
				class_input = class_input[class_input.length-1];
				input_prev = ul_prev.find("."+class_input);
				input_prev.focus();
				input_prev.click();
			}
		}
	});
	$('.rowedit input').change(function(){
		//nhan id
		var isreload=0;
		var names = $(this).attr("name");
		var intext = 'box_test_'+names;
		var inval = $(this).val();
		var ids  = names.split("_");
		var index = ids.length;
		var ids = ids[index-1];
		var price_key = new Array("sell_price","unit_price","sub_total","tax","amount");
		var pricetext_key = new Array("unit_price","sub_total","amount");
		//khoi tao gia tri luu
		names = names.replace("_"+ids,"");
		names = names.replace("cb_","");
		if(names=='sell_price' || names=='area' || names=='sub_total' || names=='amount' || names=='unit_price' )
			inval = UnFortmatPrice(inval);
		var values = new Object();
			values[names]=inval;
		if(names == "products_name"){
			save_option('products',values,ids);
			return false;
		}
		//format price
		if(price_key.indexOf(names) != -1){
			$('#'+names+'_'+ids).val(FortmatPrice(inval));
		}else
			$('#'+names+'_'+ids).val(inval);
		$.ajax({
			url : "<?php echo URL.'/salesorders/khuongnhi_cal_price' ?>",
			type: "POST",
			data: {key : ids, name : names, value : inval },
			dataType: "json",
			success: function(result){
				if(result.message == 'not_save'){
					ajax_note('Lớn hơn số lượng kho');
					$("#line_entry").click();
				}else{
					//$("#line_entry").click();
					$("#txt_amount_"+ids).text(result.amount);
					$("#sum_sub_total").val(result.sum_sub_total);
					$("#sum_tax").val(result.sum_tax);
					$("#sum_amount").val(result.sum_amount);
				}

			}
		})

	});
});

$('.ul_mag.clear.line_box').on("click",function(){
	$('.ul_mag.clear.line_box').removeClass('addColor');
	$(this).addClass('addColor');
})
$('.ul_mag.clear.line_box *').on("click",function(){
		$('.ul_mag.clear.line_box').removeClass('addColor');
		$(this).closest('.ul_mag.clear.line_box').addClass('addColor');
		//$(window).scrollTop($(this).offset()['top']-300)
})
function after_choose_products(ids,names,keys){
	var origin_keys = keys; // BaoNam, dùng cho (1)
	$("#window_popup_products"+ origin_keys).data("kendoWindow").close();
	var keys = $("#product_choice_sku").val();
	var opid  = keys.split("_");
	var	index = opid.length;
		opid = opid[index-1];
		keys = keys.replace("_"+opid,"");
	if(keys=='change'){
		var values = new Object();
		var arr = {
					"code"		:"code",
					"sku"		:"sku",
					"sell_by"	:"sell_by",
					"sell_price":"sell_price",
					"unit_price":"unit_price",
				 };
		// BaoNam 13/11/2013
		// thay vì request về server thì lấy mảng json từ popup luôn cho nhanh hơn
		var arr_data_from = JSON.parse($("#after_choose_products"+ origin_keys + ids).val()); //(1)
		values = arr;
		for(var m in arr){
			if(m!='_id'){
				keyss = arr[m];
				values[keyss] = arr_data_from[m];
			}
		}
		values['custom_unit_price'] = arr_data_from['unit_price'];
		//tax
		values['products_id'] = ids;
		values['products_name'] = arr_data_from.name; // names : BaoNam: không dùng name nữa vì bị lỗi charset
		console.log(values);
		save_option("products",values,opid,0,'line_entry','',function(opid){
			reload_subtab('line_entry');
		},'code');


	}
}
function cal_line_entry(data,fieldchange,callBack){
	$.ajax({
		url : "<?php echo URL.'/'.$controller;?>/cal_price_line",
		type:"POST",
		data: {data:JSON.stringify(data),fieldchange:fieldchange},
		success: function(result){
			if(typeof callBack == "function")
				callBack(result);
		}
	});
}

function change_uom_item(val,ids){
	var units = '';
	if(val=='unit')
		units = 'Unit';
	else
		units = 'Sq.ft.';
	var old_html = $("#box_edit_oum"+"_"+ids).html();
		old_html = old_html.split("<span");
		old_html = old_html[1].split(">");
		old_html = old_html[1];
		old_html = old_html.replace('value','value="'+units+'" title');
		$("#box_edit_oum"+"_"+ids+" .combobox").remove();
		$("#box_edit_oum"+"_"+ids).prepend(old_html+"  />");

	$.ajax({
		url: '<?php echo URL.'/';?>products/select_render',
		dataType: "json", //luu y neu dung json
		type:"POST",
		data: {sell_by:val},
		success: function(jsondata){
			$("#oum"+"_"+ids).combobox(jsondata);
		}
	});
}
</script>

<style type="text/css">
.addColor{
	background-color: rgb(85, 164, 213);
}
</style>