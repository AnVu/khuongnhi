<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>

	<?php echo $this->Html->charset(); ?>
	<title>Khuong Nhi</title>
	<link href="<?php echo URL; ?>/favicon.ico" type="image/x-icon" rel="icon">
	<link href="<?php echo URL; ?>/favicon.ico" type="image/x-icon" rel="shortcut icon">
	<?php
		echo $this->Minify->css(array('reset','style','jt_screen','jt_vunguyen','jobtraq_chat','jquery-ui-1.10.3.custom.min','kendo/kendo.common.min','kendo/kendo.anvy.min','jquery.mCustomScrollbar'));
		echo $this->Minify->script(array('jquery-1.10.2.min','jquery-ui-1.10.3.custom.min','jquery.combobox','jquery.autosize.min','jquery.mousewheel.min','jquery.mCustomScrollbar.concat.min'));

	?>
</head>

<body>
	<div id="wrapper">
		<?php echo $this->Html->charset(); ?>
		<?php echo $this->element('header');?>
		<?php echo $this->fetch('content');?>

		<?php if(!isset($set_footer))
				$set_footer = 'footer';
			  echo $this->element($set_footer);
		?>
	</div>
	<script type="text/javascript">
		localStorage.setItem("format_date","<?php echo $_SESSION['format_date']; ?>");
	</script>
	<?php
		echo $this->element('loading');
		echo $this->element('window');
		echo $this->Minify->script(array('main.js','kendo/kendo.web.min'));
		echo $this->Js->writeBuffer();
	?>
	<script type="text/javascript">
	$(function(){
		$.ajax({
			url: '<?php echo URL; ?>/homes/alerts_check',
			success: function(html){
				var json = JSON.parse(html);

				if( json.has_alert == 1){
					set_alert_footer(json);
				}else{
					remove_alert_footer();
				}

				set_interval_check_alert();
			}
		});
	});

	function set_interval_check_alert(){
		var stillAlive = setInterval(function () {
			var now = new Date();
			var sec = now.getSeconds();
			if( sec == 0 ){
				$.ajax({
					url: '<?php echo URL; ?>/homes/alerts_check',
					success: function(html){
						var json = JSON.parse(html);
						if( json.has_alert == 1){
							set_alert_footer(json);
						}else{
							remove_alert_footer();
						}
					}
				});
			}
		}, 1000);
	}

	function set_alert_footer(json){
		var alerts_footer = $("#alerts_footer");
		$("#alerts_title", alerts_footer).addClass("color_ac");
		alerts_footer.addClass("active").attr("onclick", "open_alerts()");

		var str = "";
		if( json.communication > 0 ){
			str += "(" + json.communication + ") ";
		}
		if( json.task > 0 ){
			str += "(" + json.task + ") ";
		}
		$("#alerts_num", alerts_footer).html(": " +str);
	}
	function remove_alert_footer(){
		var alerts_footer = $("#alerts_footer");
		$("#alerts_title", alerts_footer).removeClass("color_ac");
		alerts_footer.removeClass("active").attr("onclick", "return false;");
		$("#alerts_num", alerts_footer).html("");
	}
	function open_alerts(){
		$.ajax({
			url: '<?php echo URL; ?>/homes/alerts_open',
			success: function(html){
				popup_show("Your alerts", html);
			}
		});
	}

	</script>
</body>
</html>