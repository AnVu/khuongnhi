<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent"><h4><?php echo translate('Working hours for this contact'); ?></h4></span>
		<span class="fl_dent">
			<a href="<?php echo URL ;?>/contacts/print_hrs_worked_for_employee_w1" target="_blank">
				<input class="btn_pur" id="hour_for_this_contact" type="button" value="<?php echo translate('Export PDF 1'); ?>" style="width: 75px;">
			</a>
			<a href="<?php echo URL ;?>/contacts/print_hrs_worked_for_employee_w2" target="_blank">
				<input class="btn_pur" id="hour_for_this_contact" type="button" value="<?php echo translate('Export PDF 2'); ?>" style="width: 75px;">
			</a>
		</span>
	</span>
	<p class="clear"></p>
	<ul class="ul_mag clear bg3">
		<li class="hg_padd" style="width:1.5%"></li>
		<li class="hg_padd" style="width:7%"><?php echo translate('DAY'); ?></li>
		<li class="hg_padd" style="width:11.1%"><?php echo translate('Week 1'); ?></li><!-- center_txt -->
		<li class="hg_padd" style="width:4%"><?php echo translate('Lunch 1'); ?></li>
		<li class="hg_padd" style="width:2%"></li>
		<li class="hg_padd" style="width:11.1%"><?php echo translate('Week 2'); ?></li>
		<li class="hg_padd" style="width:4%"><?php echo translate('Lunch 2'); ?></li>
		<li class="hg_padd" style="width:2%"></li>
		<li class="hg_padd line_mg" style="width:11.1%"><?php echo translate('Week 3'); ?></li>
		<li class="hg_padd" style="width:4%"><?php echo translate('Lunch 3'); ?></li>
		<li class="hg_padd" style="width:2%"></li>
		<li class="hg_padd line_mg" style="width:11.1%"><?php echo translate('Week 4'); ?></li>
		<li class="hg_padd" style="width:4%"><?php echo translate('Lunch 4'); ?></li>
	</ul>
	<div id="contacts_work_hour">
		<?php
			//$arr_hour_lunch = array('0.25','0.5','0.45','1','1.25','1.5','1.45','2');
			$arr_hour_lunch['0.25'] = array('value'=>'0.25','class'=>'BgOptionHour','name'=>'0.25');
			$arr_hour_lunch['0.5'] = array('value'=>'0.5','class'=>'BgOptionHour','name'=>'0.5');
			$arr_hour_lunch['0.45'] = array('value'=>'0.45','class'=>'BgOptionHour','name'=>'0.45');
			$arr_hour_lunch['1'] = array('value'=>'1','class'=>'BgOptionHour','name'=>'1');
			$arr_hour_lunch['1.25'] = array('value'=>'1.25','class'=>'BgOptionHour','name'=>'1.25');
			$arr_hour_lunch['1.5'] = array('value'=>'1.5','class'=>'BgOptionHour','name'=>'1.5');
			$arr_hour_lunch['1.45'] = array('value'=>'1.45','class'=>'BgOptionHour','name'=>'1.45');
			$arr_hour_lunch['2'] = array('value'=>'2','class'=>'BgOptionHour','name'=>'2');

			$arr_day = array( translate('Monday'), translate('Tuesday'), translate('Wednesday'), translate('Thursday'), translate('Friday'), translate('Saturday'), translate('Sunday') );
			for ($i=0; $i < 24; $i++) {
				$j = $i;
				if($j < 10)$j = '0'.$j;
				if($i > 7 && $i < 18){
					$arr_hour[$j.':00'] = array('name'=> $j.':00', 'value'=> $j.':00', 'class'=>'BgOptionHour');
					$arr_hour[$j.':15'] = array('name'=> $j.':15', 'value'=> $j.':15', 'class'=>'BgOptionHour');
					$arr_hour[$j.':30'] = array('name'=> $j.':30', 'value'=> $j.':30', 'class'=>'BgOptionHour');
					$arr_hour[$j.':45'] = array('name'=> $j.':45', 'value'=> $j.':45', 'class'=>'BgOptionHour');
				}else{
					$arr_hour[$j.':00'] = $j.':00';
					$arr_hour[$j.':15'] = $j.':15';
					$arr_hour[$j.':30'] = $j.':30';
					$arr_hour[$j.':45'] = $j.':45';
				}
			}



			$i = 1;
			for ($k=0; $k < 7; $k++) {
		?>
			<?php echo $this->Form->hidden('Work._id', array( 'value' => $contact_id )); ?>
			<ul class="ul_mag clear bg<?php echo $i; ?>" id="contacts_work_hour_<?php echo $k; ?>">
				<li class="hg_padd" style="width:1.5%"></li>
				<li class="hg_padd" style="width:7%"><?php echo $arr_day[$k]; ?></li>

				<!-- TIME 1 -->
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.from1', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time1',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['time1']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:1%">-</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.to1', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time2',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['time2']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.lunch1', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour_lunch,
										'rel' => $k,
										'time' => 'lunch1',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['lunch1']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:2%; background:rgb(170, 218, 224)"></li>

				<!-- TIME 2 -->
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.from2', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time3',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['time3']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:1%">-</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.to2', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time4',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['time4']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.lunch2', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour_lunch,
										'rel' => $k,
										'time' => 'lunch2',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['lunch2']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:2%; background:rgb(170, 218, 224)"></li>

				<!-- TIME 3 -->
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.from3', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time5',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['time5']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:1%">-</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.to3', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time6',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['time6']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.lunch3', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour_lunch,
										'rel' => $k,
										'time' => 'lunch3',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['lunch3']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:2%; background:rgb(170, 218, 224)"></li>


				<!-- Time 4 -->
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.from4', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time7',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['time7']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:1%">-</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.to4', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour,
										'rel' => $k,
										'time' => 'time8',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['time8']:'')
							)); ?>
						</div>
					</div>
				</li>
				<li class="hg_padd center_txt" style="width:4%">
					<div class="select_inner width_select" style="width: 100%; margin: 0;">
						<div class="styled_select" style="margin: 0;">
							<?php echo $this->Form->input('Work.lunch4', array(
										'style' => 'margin-top: -3px;',
										'class' => 'force_reload',
										'options' => $arr_hour_lunch,
										'rel' => $k,
										'time' => 'lunch4',
										'empty' => '   ',
										'value' => (isset($workings_hours[$k])?$workings_hours[$k]['lunch4']:'')
							)); ?>
						</div>
					</div>
				</li>
			</ul>
			<?php $i = 3 - $i;
			}
		?>
	</div>
	<span class="hit"></span>
	<span class="title_block bo_ra2">
		<span class="bt_block float_right no_bg" style="float:right; font-weight:bold"><?php echo translate('Total'); ?> :
			<input style="text-align: center; color:red" class="input_w2" type="text" readonly="true" id="total_hour_in_week" value="<?php echo number_format($total_hour_in_month,2); ?>">(<?php echo translate('hours'); ?>)
		</span>
		<span class="bt_block float_right no_bg" style="float:left; font-weight:bold; margin-left:6%;width: 15%;"><?php echo translate('Total W1'); ?>:
			<input style="text-align: center; color:red" class="input_w2" type="text" readonly="true" id="total_week1" value="<?php echo number_format($total_week1,2); ?>">(<?php echo translate('hours'); ?>)
		</span>
		<span class="bt_block float_right no_bg" style="float:left; font-weight:bold;width: 15%; margin-left:2%"><?php echo translate('Total W2'); ?>:
			<input style="text-align: center; color:red" class="input_w2" type="text" readonly="true" id="total_week2" value="<?php echo number_format($total_week2,2); ?>">(<?php echo translate('hours'); ?>)
		</span>
		<span class="bt_block float_right no_bg" style="float:left; font-weight:bold; width: 15%;margin-left:2%"><?php echo translate('Total W3'); ?>:
			<input style="text-align: center; color:red" class="input_w2" type="text" readonly="true" id="total_week3" value="<?php echo number_format($total_week3,2); ?>">(<?php echo translate('hours'); ?>)
		</span>
		<span class="bt_block float_right no_bg" style="float:left; font-weight:bold; width: 15%;margin-left:2%"><?php echo translate('Total W4'); ?>:
			<input style="text-align: center; color:red" class="input_w2" type="text" readonly="true" id="total_week4" value="<?php echo number_format($total_week4,2); ?>">(<?php echo translate('hours'); ?>)
		</span>
	</span>
</div>

<script type="text/javascript">
$(function(){
	$(":input", "#contacts_work_hour").change(function() {
		var ids = $("#mongo_id").val();
		$.ajax({
			url: '<?php echo URL; ?>/contacts/work_hour_auto_save/' + ids,
			timeout: 15000,
			type:"post",
			data: { day: $(this).attr("rel"), time: $(this).attr("time"), value: $(this).val() },
			success: function(html){
				if( html == "reload" ){
					$("#workings_holidays").click();
				}else if( html != "ok" ){
					alerts("Error: ", html, function(){ $("#workings_holidays").click(); });
				}
			}
		});
	});
});
</script>