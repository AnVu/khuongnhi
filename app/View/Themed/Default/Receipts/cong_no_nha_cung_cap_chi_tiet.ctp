<style type="text/css">
.backg3{
	background-color: #B8B8B8;
}
</style>
<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4 id="setting_name">
				Danh sách đơn hàng
			</h4>
			<a href="/gpurchaseorders/add" style="z-index:999999;position: absolute; top: 115px; margin-left: 120px;" title="Add line" id="bt_add_products">
				<span class="icon_down_tl top_f"></span>
			</a>
		</span>

	</span>

	<ul class="ul_mag clear bg3">
		<li class="hg_padd center_text" style="width:15%">Ngày đặt hàng</li>
		<li class="hg_padd " style="width:13% ; text-align: right">Tổng tiền toa hàng</li>
		<li class="hg_padd " style="width:10% ; text-align: right">Nợ cũ</li>
		<li class="hg_padd center_text" style="width:15%">Thanh toán</li>
		<li class="hg_padd " style="width:10% ; text-align: right">Còn lại</li>
		<li class="hg_padd center_text" style="width:15%">Ngày thanh toán</li>
		<li class="hg_padd center_text" style="width:13% ; text-align: right">Hình thức thanh toán</li>
	</ul>

	<div id="paid" class="container_same_category" style="height: 561px;overflow-y: auto">
		<?php
		$stt = 1;
		$i = 1;
		$count = count($arr_pur);
		$tong_dat_hang = 0;
		$tong_thanh_toan = 0;
		foreach ($arr_pur as $key => $value) {
			 $is_return = isset($value['return_id']) ? $value['return_id'] : 0;
		             if($is_return == 1){
		              $tong_dat_hang -= $value['sum_amount'];
		             }else{
		              $tong_dat_hang += $value['sum_amount'];
		             }
		?>
		<?php echo $this->Form->create('Setting', array('id' => 'SettingForm_' . $key)); ?>
		<?php $return_id = isset($value['return_id']) ? $value['return_id'] : 0 ?>
 		<ul class="ul_mag clear bg<?php echo $i;?>">
			<li class="hg_padd" style="width:15%">
				<span style="text-align:center" id="date_modified" name="date_modified"  class="bg<?php echo $i; ?>"><?php echo date('d-m-Y',$value['purchord_date']->sec) ?></span>
			</li>
  			<li class="hg_padd" style="width:13%">
  			<?php if(/*$value['products'][0]['oum'] == 'Cái' ||*/ $return_id == 1){ ?>
				<input style="text-align: right" type="text" readonly name="sum_amount" id="sum_amount" value="<?php if(isset($value['sum_amount']))   echo '('; echo number_format($value['sum_amount']); echo ')' ?>" class="input_inner bg<?php echo $i; ?>" />
				<?php }else{ ?>
				<?php $sum_amount = isset($value['sum_amount']) ? (double)$value['sum_amount'] : 0 ?>
				<input style="text-align: right" type="text" name="sum_amount" id="sum_amount" type="text" readonly class="input_inner bg<?php echo $i; ?>" value="<?php if(isset($value['sum_amount'])) echo  number_format($sum_amount) ?>" />
				<?php } ?>
			</li>

			<li class="hg_padd" style="width:10%">
				<input style="text-align: right" type="text" name="no_cu" readonly value="<?php echo number_format($value['no_cu']) ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>
			<li class="hg_padd" style="width:15%">
				<?php
					$thanh_toan = 0;
					$thanh_toan = isset($value['thanh_toan']) ? $value['thanh_toan'] : 0;
					$tong_thanh_toan += $thanh_toan;
				 ?>
				<input style="text-align: right;width:100%;font-size:11px;" class="paid" type="text" name="paid" id="<?php echo $value['_id']; ?>" value="<?php echo number_format($thanh_toan) ?>" class="input_inner bg<?php echo $i; ?>"   />
			</li>
			<li class="hg_padd" style="width:10%">
				<input style="text-align: right" type="text" name="total_balance" value="<?php echo number_format($value['con_lai'] ) ?>" class="input_inner bg<?php echo $i; ?>" readonly />
			</li>
			<li class="hg_padd" style="width:15%">
				<input style="text-align: center" class="date_paid" type="text" name="date_paid" id="<?php echo $value['_id']; ?>" value="<?php echo isset($value['date_paid'])? $value['date_paid']:'' ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>
			<li class="hg_padd" style="width:13%; text-align: center">
				<select name="type_paid">
					<option value=""></option>
					<?php
						$tmp = isset($value['type_paid']) ? $value['type_paid'] : '';
					?>
					<option value="chuyen_khoan" <?php if($tmp == 'chuyen_khoan') echo 'selected'; ?>>Chuyển khoản</option>
					<option value="tien_mat" <?php if($tmp == 'tien_mat') echo 'selected'; ?>>Tiền mặt</option>
				</select>
			</li>
			<input type="hidden"  name="company_id" id="company_id" value="<?php echo $company_id?>" />
			<input type="hidden" id="salesorder_id" name="salesorder_id" value="<?php echo $value['_id'] ?>" />
		</ul>
		<?php echo $this->Form->end(); ?>
		<?php } ?>
	</div>
	<div style="position: absolute;top: 115px;width:69%">
		<input class="input_7" style=" width:13%; text-align:right; margin-left: 16%" id="sum_sub_total" value="<?php echo number_format($tong_dat_hang) ?>" readonly="readonly" type="text">
		<input class="input_7" style=" width:15.5%; text-align:right; margin-left:11%" id="sum_sub_total" value="<?php echo number_format($tong_thanh_toan) ?>" readonly="readonly" type="text">
		<input class="input_7" style=" width:9.7%; text-align:right; margin-left: 0.3%" id="sum_sub_total" value="<?php echo number_format($tong_dat_hang - $tong_thanh_toan) ?>" readonly="readonly" type="text">
	</div>
	<span class="title_block bo_ra2">

	</span>
</div>
<script type="text/javascript">
	$(function(){
		$(".paid").click(function(){
			$(this).val(" ");
		})
	})
</script>



