<?php echo $this->element('../'.$name.'/tab_option'); ?>
<style type="text/css">
#confirms_window .jt_confirms_window_ok{
	width: 15%;
	height: 18%;
	margin-top: 14%;
	margin-left: 80%;
}
#list_and_menu_height ul li{
	padding-left: 4px !important;
}
</style>
<div class="clear_percent">
	<div class="clear_percent_19 float_left">
		<div class="tab_1 full_width">
			<span class="title_block bo_ra1">
				<span class="fl_dent"><h4>Công nợ khách hàng &nbsp;&nbsp;</h4></span>
			<?php 
				$month_now = intval( date('m') );
				$year_now = intval( date('Y') );
			?>

			<select name="month" onchange="load_cong_no_detail();">
				<?php 
					for($i=1;$i<=12;$i++){
				?>
					<option value="<?php echo $i; ?>"  <?php echo $month_now==$i?'selected':''  ?>>Tháng <?php echo $i; ?></option>
				<?php
					}
				?>
				<option value="all">Xem tất cả</option>
			</select>
			<select name="year"  onchange="load_cong_no_detail();">
				<?php 
					for($i=$first_month['year'];$i<=$year_now;$i++){
				?>
					<option value="<?php echo $i; ?>"  <?php echo $year_now==$i?'selected':''  ?>>Năm <?php echo $i; ?></option>
				<?php
					}
				?>
			</select>

			</span>

			<ul class="ul_mag clear bg3">
				<li class="hg_padd center_text" style="width:100%;border: none;">Danh sách khách hàng</li>
			</ul>
			<div class="center_text bg3">
				<select id="select_company">
					<option value="all"></option>
					<?php
					$i = 1;
					foreach ($company_name as $key => $value) {
					?>
					<option  value="<?php echo $value['_id'] ?>"><?php echo $value['name'] ?></option>
					<?php $i = 3 - $i;
					} ?>
				</select>
			</div>
			<div id="list_and_menu_height" class="container_same_category" style="height: 561px;overflow-y: auto">
				<?php
				$i = 1;
				foreach ($company_name as $key => $value) {
				?>
				<ul class="ul_mag clear bg<?php echo $i; ?>" id="system_<?php echo $value['_id']; ?>">
					<li class="hg_padd" style="width: 44.3%;border: none;">
						<input type="text" value="<?php if(isset($value['name'])){ echo $value['name']; }else{ echo $value['setting_value']; } ?>" class="input_inner bg<?php echo $i; ?>" readonly="">
						</li>
					<li  style="width: 10%;border: none;"></li>
					<li class="hg_padd center_text clickfirst" style="width: 20.1%;border-right: none;cursor:pointer" onclick="settings_list_menu_detail(this, '<?php echo $value['_id']; ?>')">
						<span class="icon_emp"></span>
						<span style="margin-top: -13px; margin-left: 20px"><a target="_blank" onclick="click_print('<?php echo $value['_id']; ?>')" >IN PDF</a></span>
					</li>

				</ul>
				<?php $i = 3 - $i;
				} ?>

			</div>
			<span class="title_block bo_ra2"></span>
		</div><!--END Tab1 -->
	</div>
	<div class="clear_percent_9_arrow float_left">
		<div class="full_width box_arrow">
			<span class="icon_emp" style="cursor:default"></span>
		</div>
	</div>
	<div class="clear_percent_11 float_left" id="list_and_menu_detail">
		<!-- Detail -->
	</div>
</div>
<input type="hidden" id="id_choosen" value="">

<style type="text/css">
#list_and_menu_height ul:hover, #list_and_menu_height ul:hover input{
	background-color: #B8B8B8;
}
</style>
<link href="<?php echo URL.'/theme/default/css/'?>select2.css" rel="stylesheet" />
<style type="text/css" media="screen">
	.select2-container--default .select2-selection--single{
		border-radius: 0 !important;
		height:20px;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow b {
		border-color: #000 transparent transparent transparent;
		border-style: solid;
		border-width: 6px 3px 0 3px;
		height: 0;
		left: 50%;
		margin-left: -4px;
		margin-top: -2px;
		position: absolute;
		top: 25%;
		width: 0;
	}
	.select2-container--default.select2-container--open .select2-selection--single .select2-selection__arrow b {
		border-color: transparent transparent #000 transparent;
		border-width: 0 3px 6px 3px;
	}
	.select2-container--default .select2-results > .select2-results__options{
		overflow-x: hidden;
	}
	.select2-results__option{
		padding:4px;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 16px;
	}
	#select2-sku_search-container{
		text-align: center;
		margin-left: 15px;
	}
	.select2-results__option:first-child{
		height: 16px;
	}
	.select2-results__option{
		font-family: arial,sans-serif,verdana;
		font-size: 11px;
	}
 	.select2-container{
 		margin: auto;
 	}
</style>
<script src="<?php echo URL.'/theme/default/js/'?>select2.min.js"></script>
<script type="text/javascript">

	$(function(){

		$('.container_same_category').mCustomScrollbar({
			scrollButtons:{
				enable:false
			}
		});

		$(".clickfirst:first", "#list_and_menu_height").click(); // click menu li dau tien khi page load xong

	});

	list_all_company = $("#list_and_menu_height").html();


	function click_print(ids){
		var month = $('select[name="month"]').val();
		var year = $('select[name="year"]').val();

		window.location = "<?php echo URL?>/receipts/in_cong_no_khach_hang/" + ids +'/' + month+'/'+year;
	}

	function settings_list_menu_detail(object, ids){
		var month = $('select[name="month"]').val();
		var year = $('select[name="year"]').val();
		$("#list_and_menu_height ul").attr("style", "");
		$("input", "#list_and_menu_height").attr("style", "");

		var ul = $(object).parents("ul");
		$(ul).attr("style", "background-color: #B8B8B8;");
		$("input", ul).attr("style", "background-color: #B8B8B8;");
		$("#id_choosen").val(ids);
		$.ajax({
			url: '<?php echo URL; ?>/receipts/thanh_toan_cong_no_detail/' + ids + '/' + month+'/'+year,
			timeout: 15000,
			success: function(html){
				$("div#list_and_menu_detail").html(html);
				list_and_menu_detail_input_change();
			}
		});
	}

	function load_cong_no_detail(){
		if( $("#id_choosen").val() != ''){
			var month = $('select[name="month"]').val();
			var year = $('select[name="year"]').val();
			var ids = $("#id_choosen").val();
			$.ajax({
				url: '<?php echo URL; ?>/receipts/thanh_toan_cong_no_detail/' + ids + '/' + month+'/'+year,
				timeout: 15000,
				success: function(html){
					$("div#list_and_menu_detail").html(html);
					list_and_menu_detail_input_change();
				}
			});
		}
	}

	function list_and_menu_detail_input_change(){
		$("form :input", "#paid").change(function() {
			var fieldname = $(".paid").attr("name");
			if(fieldname == 'paid'){
				if( $("#confirms_window" ).attr("id") == undefined ){
					var html = '<div id="password_confirm" >';
						html +=	   '<div class="jt_box" style=" width:100%;">';
						html +=		  '<div class="jt_box_line"><div class=" jt_box_label " style=" width:25%;"></div><div id="alert_message"></div></div>';
						html +=	      '<div class="jt_box_line">';
						html +=	         '<div class=" jt_box_label " style=" width:25%;height: 75px">Password</div>';
						html +=	         '<div class="jt_box_field " style=" width:71%"><input name="password" id="password"  class="input_1 float_left" type="password" value=""></div><input style="margin-top:2%" type="button" class="jt_confirms_window_cancel" id="confirms_cancel" value=" Cancel " /><input style="margin-top:2%; height: 20px !important; margin-left: 10% !important;" type="button" class="jt_confirms_window_ok" value=" Ok " id="confirms_ok" />';
						html +=	      '</div>';
						html +=	      '</div>';
						html +=	   '</div>';
						html +=	'</div>';
					$('<div id="confirms_window" style="width: 99%; padding: 0px; overflow: auto;">'+html+'</div>').appendTo("body");
				}
				var confirms_window = $("#confirms_window");
				confirms_window.kendoWindow({
					width: "355px",
					height: "100px",
					title: "Enter password",
					visible: false,
					activate: function(){
					  $('#password').focus();
					}
				});

				confirms_window.data("kendoWindow").center();
				confirms_window.data("kendoWindow").open();
				var object = this;
				$("#confirms_ok").unbind("click");
				$("#password").keypress(function(e){
					 if(e.which == 13) {
				        $("#confirms_ok").trigger("click");
				    }
				})
				$("#confirms_ok").click(function() {
					$("#alert_message").html("");
					if( $("#password").val().trim()==''  ){
						$("#alert_message").html('<span style="margin-left: 5px; color:red; font-weight: 700">Password must not be empty.</span');
						$("#password").focus();
						return false;
					}
					var values = {};
					values["password"] = $("#password").val();
					values["value"] = $(object).val();
					$("#list_and_menu_height ul").attr("style", "");
					$("input", "#list_and_menu_height").attr("style", "");
					$.ajax({
						url: '<?php echo URL; ?>/receipts/list_salersorders',
						timeout: 15000,
						type:"post",
						data: $(":input", $(object).closest("ul")).serialize(),
						success: function(html){
							if( html != "ok" )alerts("Error: ", html);
							$(".icon_emp","ul#system_"+$("#company_id").val()).click();
						}
					});
			       	confirms_window.data("kendoWindow").destroy();
				});
				$('#confirms_cancel').click(function() {
					$("#alert_message").html("");
					$("#code").val($("#code").attr("rel"));
			       	confirms_window.data("kendoWindow").destroy();
			       	$(".icon_emp","ul#system_"+$("#company_id").val()).click();
			    });
			    return false;
			}
		});
	}

	$("#select_company").select2();

	$("#select_company").on("change",function(){
		html='';
		if($(this).val()=='all'){
			$("#list_and_menu_height").html(list_all_company);
		}else{
			var id = $(this).val();
			var name = $("#select_company option[value="+id+"]").text();
			html+='<ul class="ul_mag clear bg" id="system_'+id+'">'+
					'<li class="hg_padd" style="width: 44.3%;border: none;">'+
						'<input type="text" value="'+name+'" class="input_inner bg" readonly="">'+
					'</li>'+
					'<li  style="width: 10%;border: none;"></li>'+
					'<li class="hg_padd center_text clickfirst" style="width: 20.1%;border-right: none;cursor:pointer" onclick="settings_list_menu_detail(this,\''+id+'\')">'+
						'<span class="icon_emp"></span>'+
						'<span style="margin-top: -13px; margin-left: 20px"><a target="_blank" onclick="click_print(\''+id+'\')" >IN PDF</a></span>'+
					'</li>'+
				'</ul>';
			$("#list_and_menu_height").html(html);
			$("li.clickfirst").trigger("click");
		}
	})
	/*function enterpass(){
		alert(event.which);
		if(e.keycode==13){
			$("#confirms_ok").trigger("click");
		}
	}*/

</script>