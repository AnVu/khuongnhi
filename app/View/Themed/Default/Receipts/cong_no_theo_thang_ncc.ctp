<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4 id="setting_name">
			</h4>
		</span>
	</span>

	<ul class="ul_mag clear bg3">
		<li class="hg_padd center_text" style="width:30%">Tháng</li>
		<li class="hg_padd center_text" style="width:10%">Tổng tiền toa hàng</li>
		<li class="hg_padd center_text no_border" style="width:10%">Nợ cũ</li>
		<li class="hg_padd center_text no_border" style="width:10%">Thanh toán</li>
		<li class="hg_padd center_text no_border" style="width:10%">Còn lại</li>
	</ul>

	<div class="container_same_category" style="height: 449px;overflow-y: auto">
		<?php
		$stt = 1;
		$i = 1;
		$count = count($arr_data);
		$total_no_cu = 0;
		$total_thanh_toan = 0;
		$total_con_lai = 0;
		$tong_toa_hang = 0;
		foreach ($arr_data as $key => $value) {
			$total_no_cu += $value['no_cu'];
			$total_thanh_toan += $value['thanh_toan'];
			$total_con_lai += $value['con_lai'];
			$tong_toa_hang += $value['sum_amount'];
			$i = 3 - $i;
		?>
		<?php echo $this->Form->create('Setting', array('id' => 'SettingForm_' . $key)); ?>
		<input type="hidden" id="purchaseorder_id" value="<?php echo $key ?>" />
 		<ul class="ul_mag clear bg<?php echo $i; ?>">
			<li class="hg_padd" style="width:30%">
				<input style="text-align:center" type="text" name="date_modified" value="<?php echo $value['company_name'];?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>

			<li class="hg_padd" style="width:10%">
				<input style="text-align: right" type="text" name="sum_amount" value="<?php echo number_format($value['sum_amount']) ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>

			<li class="hg_padd" style="width:10%">
				<input style="text-align: right" type="text" name="no_cu" "readonly" value="<?php echo number_format($value['no_cu']) ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>

			<li class="hg_padd" style="width:10%">
				<input style="text-align: right" "readonly" type="text" name="paid" value="<?php echo number_format($value['thanh_toan']) ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>

			<li class="hg_padd" style="width:10%">
				<input style="text-align: right" type="text" name="total_balance" value="<?php echo number_format($value['con_lai'] ) ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>
		</ul>
		<input type="hidden" name="company_id" id="company_id" value="<?php echo $value['company_id']?>" />
		<?php echo $this->Form->end(); ?>
		<?php } ?>
	</div>
	<div style="position: absolute; top: 114px;   width: 65%;">
		<input class="input_7" style=" width:12%; text-align:right; margin-left:  26%;" id="sum_sub_total" value="<?php echo number_format($tong_toa_hang) ?>" readonly="readonly" type="text">
		<input class="input_7" style=" width:12%; text-align:right; margin-left: 4%;" id="sum_sub_total" value="<?php echo number_format($total_no_cu) ?>" readonly="readonly" type="text">
		<input class="input_7" style=" width:12%; text-align:right; margin-left: 4.3%;" id="sum_sub_total" value="<?php echo number_format($total_thanh_toan) ?>" readonly="readonly" type="text">
		<input class="input_7" style=" width:12%; text-align:right; margin-left: 4.3%;" id="sum_sub_total" value="<?php echo number_format($total_con_lai) ?>" readonly="readonly" type="text">
	</div>
	<span class="title_block bo_ra2">

	</span>
</div>
