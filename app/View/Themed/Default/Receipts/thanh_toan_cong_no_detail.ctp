<style type="text/css">
.backg3{
	background-color: #B8B8B8;
}
</style>
<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4 id="setting_name">
				Danh sách đơn hàng
			</h4>
			<a href="/gsaleorders/add" style="z-index:999999;position: absolute; top: 115px; margin-left: 120px;" title="Add line" id="bt_add_products">
				<span class="icon_down_tl top_f"></span>
			</a>
		</span>

	</span>

	<ul class="ul_mag clear bg3">
		<li class="hg_padd center_text" style="width:15%">Ngày đặt hàng</li>
		<li class="hg_padd " style="width:13% ; text-align: right">Tổng tiền toa hàng</li>
		<li class="hg_padd " style="width:10% ; text-align: right">Nợ cũ</li>
		<li class="hg_padd center_text" style="width:15%">Thanh toán</li>
		<li class="hg_padd " style="width:10% ; text-align: right">Còn lại</li>
		<li class="hg_padd center_text" style="width:15%">Ngày thanh toán</li>
		<li class="hg_padd center_text" style="width:13% ; text-align: right">Hình thức thanh toán</li>
	</ul>

	<div id="paid" class="container_same_category" style="height: 561px;overflow-y: auto">
		<?php
		$stt = 1;
		$i = 1;
		$count = count($arr_order);
		/*uasort($arr_order, function($a,$b){
            return $a['salesorder_date']->sec > $b['salesorder_date']->sec;
    	});*/
		$tong_dat_hang = 0;
		$tong_thanh_toan = 0;
		$tong_con_lai = 0;

		foreach ($arr_order as $key => $value) {
			  $is_return = isset($value['return_id']) ? $value['return_id'] : 0;
		             if($is_return == 1){
		              $tong_dat_hang -= $value['sum_amount'];
		             }else{
		              $tong_dat_hang += $value['sum_amount'];
		             }
		             $i = 3 - $i;
			?>
			<?php echo $this->Form->create('Setting', array('id' => 'SettingForm_' . $key)); ?>
			<?php $return_id = isset($value['return_id']) ? $value['return_id'] : 0 ?>
	 		<ul class="ul_mag clear bg<?php echo $i; ?> ">
				<li class="hg_padd" style="width:15%">
					<span style="text-align:center"  name="date_modified" class=" bg<?php echo $i; ?>"><?php echo date('d-m-Y',$value['salesorder_date']->sec) ?></span>
				</li>

				<li class="hg_padd" style="width:13%">
					<?php $var = isset($value['products'][0]['oum']) ? $value['products'][0]['oum'] : '' ?>
					<?php if($var == 'Cái' || $return_id == 1){ ?>
					<input style="text-align: right" type="text" name="sum_amount" id="sum_amount" value="<?php if(isset($value['sum_amount']))   echo '('; echo number_format($value['sum_amount']); echo ')' ?>" class="input_inner bg<?php echo $i; ?>" readonly />
					<?php }else{ ?>
					<?php $sum_amount = isset($value['sum_amount']) ? (double)$value['sum_amount'] : 0 ?>
					<input style="text-align: right" type="text" name="sum_amount" id="sum_amount" value="<?php echo number_format($sum_amount) ?>" class="input_inner bg<?php echo $i; ?>" readonly />
					<?php } ?>
				</li>

				<li class="hg_padd" style="width:10%">
					<span style="text-align: right"  name="no_cu" class=" bg<?php echo $i; ?>"><?php echo number_format($value['no_cu']) ?></span>
				</li>
				<li class="hg_padd" style="width:15%">
					<?php
						$thanh_toan = 0;
						$thanh_toan = isset($value['thanh_toan']) ? $value['thanh_toan'] : 0;
						$tong_thanh_toan += $thanh_toan;
					 ?>
					<input style="text-align: right; width:100%;font-size:11px;" class="paid" type="text" name="paid" id="<?php echo $value['_id']; ?>" value="<?php echo number_format($thanh_toan) ?>" class="input_inner bg<?php echo $i; ?>"  />
				</li>
				<li class="hg_padd" style="width:10%">
					<span style="text-align: right"  name="total_balance" class=" bg<?php echo $i; ?>"><?php echo number_format($value['con_lai'] ) ?> </span>
				</li>
				<li class="hg_padd" style="width:15%">
					<input style="text-align: center" class="date_paid" type="text" name="date_paid" id="<?php echo $value['_id']; ?>" value="<?php echo isset($value['date_paid'])?$value['date_paid']:''; ?>" class="input_inner bg<?php echo $i; ?>"  />
				</li>
				<li class="hg_padd" style="width:13%; text-align: center">
					<select name="type_paid">
						<option value=""></option>
						<option value="chuyen_khoan" <?php if($value['type_paid'] == 'chuyen_khoan') echo 'selected'; ?>>Chuyển khoản</option>
						<option value="tien_mat" <?php if($value['type_paid'] == 'tien_mat') echo 'selected'; ?>>Tiền mặt</option>
					</select>
				</li>
				<input type="hidden" name="company_id" id="company_id" value="<?php echo $company_id?>" />
				<input type="hidden" name="salesorder_id" id="salesorder_id" value="<?php echo $value['_id'] ?>" />
			</ul>
			<?php }?>
			<?php echo $this->Form->end(); ?>

	</div>
	<div style="position: absolute;top: 115px;width:69%">
		<input class="input_7" style=" width:13%; text-align:right; margin-left: 16%" id="sum_sub_total" value="<?php echo number_format($tong_dat_hang) ?>" readonly="readonly" type="text">
		<input class="input_7" style=" width:15.5%; text-align:right; margin-left:11%" id="sum_sub_total" value="<?php echo number_format($tong_thanh_toan) ?>" readonly="readonly" type="text">
		<input class="input_7" style=" width:9.7%; text-align:right; margin-left: 0.3%" id="sum_sub_total" value="<?php echo number_format($tong_dat_hang - $tong_thanh_toan) ?>" readonly="readonly" type="text">
	</div>
	<span class="title_block bo_ra2">
		<span class="float_left bt_block" style="width: 100%">

		</span>
	</span>
</div>

<script type="text/javascript">
	$(function(){
		$(".paid").click(function(){
			$(this).val(" ");
		})
	})
</script>
