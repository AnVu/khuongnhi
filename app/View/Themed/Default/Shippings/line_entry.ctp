<?php
		foreach($arr_settings['relationship'][$sub_tab]['block'] as $key => $arr_val){

			echo $this->element('box',array('key'=>$key,'arr_val'=>$arr_val));

		}
?>
<p class="clear"></p>
<script>
$(document).ready(function() {
	fixHiddenCombobox();
	//tạo thêm 1 products line mới
	$("#bt_add_products").click(function() {
		var datas = {
			'products_id' : '',
			'products_name' : 'This is new record. Click for edit',
			'sizew_unit' : 'in',
			'sizeh_unit' : 'in',
			'sell_by' : 'area',
			'quantity':0,
			'prev':0,
			'now':0,
			'balance': 0,
			'available':'',
			'oum' : 'Sq.ft.',
		};
		save_option('products',datas,'',1,'line_entry','add');
	});

	$(".del_products").focusin(function(){
		ajax_note_set("");
		var ids = $(this).attr("id");
		ids  = ids.split("_");
		var ind = ids.length;
		var idfield =  parseInt(ids[ind-1]);
		ajax_note_set(" Press ENTER to delete the line:"+(idfield+1));
	});

	$(".del_products").focusout(function(){
		ajax_note("");
		var ids = $(this).attr("id");
			ids = ids.split("_");
		var index = ids.length;
		ids  = parseInt(ids[index-1])+1;
		$(".jt_line_over").removeClass('jt_line_over');
		$("#listbox_products_"+ids).addClass('jt_line_over');
	});

	$(".choice_code").click(function(){
		var ids = $(this).attr("id");
		var key_click_open = ids;
		ids  = ids.split("_");
		var ind = ids.length;
		var ids = ids[ind-1];
	});
	$( "#load_subtab" ).delegate(".rowedit input,.viewcheck_omit","focus",function(){
		$(this).attr('oldValue',$(this).val());
	});

	<?php if($this->Common->check_permission('shippings_@_entry_@_edit',$arr_permission)){?>
	$( "#load_subtab" ).delegate(".rowedit input,.viewcheck_omit","change",function(){
		//nhan id
		var isreload=0;
		var names = $(this).attr("name");
		var intext = 'box_test_'+names;
		var inval = $(this).val();
		var ids  = names.split("_");

		var index = ids.length;
		var ids = ids[index-1];
		if(names=='shipped_'+ids)
		{
			var qty = $('#quantity_'+ids).val();
			var prev = 0;
			if($('#txt_prev_shipped_'+ids).html()!=undefined)
				prev = parseInt($('#txt_prev_shipped_'+ids).html());
			console.log(qty+' '+prev);
			if(qty==undefined||qty==0)
			{
				alerts('Message','The quantity of product must be existed!.');
				$(this).val($(this).attr('oldValue'));
				return false;
			}
			if(parseInt(inval)>(parseInt(qty)-prev))
			{
				alerts('Message','The ship quantity cannot be greater than the balance quantity.');
				$(this).val($(this).attr('oldValue'));
				return false;
			}
		}
		else if(names=='quantity_'+ids)
		{
			var now = parseInt($('#shipped_'+ids).val());
			if(now!=undefined||now!=0)
			{
				if(parseInt(inval)<now)
				{
					alerts('Message','The new quantity cannot be less than the shipped quantity.');
					$(this).val($(this).attr('oldValue'));
					return false;
				}
			}
		}
		var price_key = new Array("sizew","sizeh","area");
		//khoi tao gia tri luu
		names = names.replace("_"+ids,"");
		names = names.replace("cb_","");
		if(names=='sizew' || names=='sizeh' || names=='area')
			inval = UnFortmatPrice(inval);
		var values = new Object();
			values[names]=inval;

		//format price
		if(price_key.indexOf(names) != -1){
			$('#'+names+'_'+ids).val(FortmatPrice(inval));
		}else
			$('#'+names+'_'+ids).val(inval);

		//if is select box
		if($('#'+names+'_'+ids).parent().attr('class')=='combobox'){
			values[names]=$('#'+names+'_'+ids+'Id').val();
		}
		//set default oum
		if(names=='sell_by'){
			var newval = $('#sell_by_'+ids+'Id').val();
			if(newval=='unit')
				values['oum'] = 'unit';//set default
			else
				values['oum'] = 'Sq.ft.';//set default
			change_uom_item(newval,ids);
		}

		values['id']=ids;
		cal_line_entry(values,function(ret){
			var i,tem,txtval;
			for(i in ret){
				if(i=='products_name' && names=='products_name'){
					$('#'+i+'_'+ids).val(inval);
				}else if($('#'+i+'_'+ids).parent().attr('class')=='combobox'){
					$('#'+i+'_'+ids+'Id').val(ret[i]);
				}else if(i=='quantity' || i =='shipped'){
					var value = 0;
					if($('#quantity_'+ids).val() != undefined)
						value = parseInt($('#quantity_'+ids).val());
					if($('#shipped_'+ids).val() != undefined)
						value = value - parseInt($('#shipped_'+ids).val());
					if($('span#txt_prev_shipped_'+ids).html()!=undefined)
						value = value - parseInt($('span#txt_prev_shipped_'+ids).html());
					$('span#txt_balance_shipped_'+ids).html(value);
				}else
					$('#'+i+'_'+ids).val(ret[i]);
			}

		});

	});
<?php }else{?>
	$("#load_subtab").find("a").each(function(){
        $(this).remove();
    });
<?php } ?>

});


/**
/* Tính lại tổng trong database
/*
**/
function update_sum(handleData){
	//định nghĩa lại field trong database để tính
	var keyfield = {
			"sub_total"		: "sub_total",
			"tax"			: "tax",
			"amount"		: "amount",
			"sum_sub_total"	: "sum_sub_total",
			"sum_tax"		: "sum_tax",
			"sum_amount"	: "sum_amount"
		};
	var keyfield = JSON.stringify(keyfield);
	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/update_sum',
		type:"POST",
		dataType: "json",
		data: {subdoc:'products',keyfield:keyfield},
		success: function(ret){
			$('#sum_sub_total').val(FortmatPrice(sum_ret['sum_sub_total']));
			$('#sum_tax').val(FortmatPrice(sum_ret['sum_tax']));
			$('#sum_amount').val(FortmatPrice(sum_ret['sum_amount']));
			if(handleData!=undefined)
				handleData(ret);
		}
	});
}




/**
/* Tính lại giá cho 1 line
/* Use: values là mảng data cần thay đổi
**/
function cal_line_entry(values,handleData){
	var jsonString = JSON.stringify(values);
	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/ajax_cal_line',
		type:"POST",
		dataType: "json",
		data: {arr:values},
		success: function(ret){
			if(handleData!=undefined)
				handleData(ret);
		}
	});
}


/**
/* Change list box OUM (after Sell price)
/*
**/
function change_uom_item(val,ids){
	var units = '';
	if(val=='unit')
		units = 'unit';
	else
		units = 'Sq.ft.';
	var old_html = $("#box_edit_oum"+"_"+ids).html();
		old_html = old_html.split("<span");
		old_html = old_html[1].split(">");
		old_html = old_html[1];
		old_html = old_html.replace('value','value="'+units+'" title');
		$("#box_edit_oum"+"_"+ids+" .combobox").remove();
		$("#box_edit_oum"+"_"+ids).prepend(old_html+"  />");

	$.ajax({
		url: '<?php echo URL.'/';?>products/select_render',
		dataType: "json", //luu y neu dung json
		type:"POST",
		data: {sell_by:val},
		success: function(jsondata){
			$("#oum"+"_"+ids).combobox(jsondata);
		}
	});
}



function after_choose_products(ids,names,keys){
	var origin_keys = keys; // BaoNam, dùng cho (1)
	$("#window_popup_products"+ origin_keys).data("kendoWindow").close();
	var keys = $("#product_choice_sku").val();
	var opid  = keys.split("_");
	var	index = opid.length;
		opid = opid[index-1];
		keys = keys.replace("_"+opid,"");
	if(keys=='change'){
		var values = new Object();
		var arr = { "_id"		:"products_id",
					"code"		:"code",
					"option"	:"option",
					"sell_by"	:"sell_by",
					"oum"		:"oum",
					"markup"	:"markup",
					"profit"	:"profit",
					"sizeh"		:"sizeh",
					"sizeh_unit":"sizeh_unit",
					"sizew"		:"sizew",
					"sizew_unit":"sizew_unit",
					"is_custom_size":"is_custom_size"
				 };
		// BaoNam 13/11/2013
		// thay vì request về server thì lấy mảng json từ popup luôn cho nhanh hơn
		var arr_data_from = JSON.parse($("#after_choose_products"+ origin_keys + ids).val()); //(1)
		values = arr;
		for(var m in arr){
			if(m!='_id'){
				keyss = arr[m];
				values[keyss] = arr_data_from[m];
			}
		}
		 values['products_id'] = ids;
		values['products_name'] = arr_data_from.name; // names : BaoNam: không dùng name nữa vì bị lỗi charset
		save_option("products",values,opid,0,'line_entry','',function(opid){
			var newval = new Object();
			//newval = values;
			newval['id'] = parseInt(opid);
			newval['sell_price'] = arr_data_from.sell_price;
			reload_subtab('line_entry');
		});


	}
}


</script>