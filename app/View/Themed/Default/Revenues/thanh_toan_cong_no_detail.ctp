<style type="text/css">
.backg3{
	background-color: #B8B8B8;
}
</style>
<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4 id="setting_name">
				Doanh số công ty
			</h4>
		</span>
	</span>

	<ul class="ul_mag clear bg3">
		<li class="hg_padd center_text" style="width:20%">Ngày</li>
		<li class="hg_padd" style="width:15%; text-align: right">Doanh số</li>
		<li class="hg_padd" style="width:15%; text-align: right">Khoảng giảm</li>
		<li class="hg_padd" style="width:15%; text-align: right">Lãi thực</li>
		<li class="hg_padd" style="width:15%; text-align: right">Lợi nhuận</li>
	</ul>

	<div class="container_same_category" style="height: 561px;overflow-y: auto">
		<?php
		$stt = 1;
		$i = 1;
		$count = count($arr_order);
		uasort($arr_order, function($a,$b){
            return $a['salesorder_date']->sec > $b['salesorder_date']->sec;
    	});
    	$total_sum_amount = 0;
    	$total_khoang_giam = 0;
    	$total_interest = 0;
    	$total_loi_nhuan = 0;
		foreach ($arr_order as $key => $value) {
				$total_sum_amount += $value['sum_amount'];
				$total_khoang_giam += $value['khoang_giam'];
				$total_interest += $value['loi_nhuan'];
				$total_loi_nhuan += $value['loi_nhuan'];
				$i = 3 - $i;
			?>
			<?php echo $this->Form->create('Setting', array('id' => 'SettingForm_' . $key)); ?>
			<input type="hidden" id="salesorder_id" value="<?php echo $key ?>" />
			<?php $return_id = isset($value['return_id']) ? $value['return_id'] : 0 ?>
	 		<ul class="ul_mag clear bg<?php echo $i; ?> ">
				<li class="hg_padd" style="width:20%">
					<input style="text-align:center" type="text" name="date_modified" value="<?php echo date('d-m-Y',$value['salesorder_date']->sec) ?>" class="input_inner bg<?php echo $i; ?>"  />
				</li>

				<li class="hg_padd" style="width:15%">
					<input style="text-align: right" type="text" name="sum_amount" id="sum_amount" value="<?php if(isset($value['sum_amount'])) echo  number_format($value['sum_amount']) ?>" class="input_inner bg<?php echo $i; ?>" readonly />
				</li>

				<li class="hg_padd" style="width:15%">
					<?php $value['products'][0]['oum'] = isset($value['products'][0]['oum']) ? $value['products'][0]['oum'] : '' ?>
					<?php if($value['products'][0]['oum'] == 'Cái' || $return_id == 1){ ?>
						<input style="text-align: right" type="text" name="no_cu" "reaonly" value="<?php  echo '(';  echo number_format($value['khoang_giam']); echo ')' ?>" class="input_inner bg<?php echo $i; ?>"  />
					<?php }else{ ?>
						<input style="text-align: right" type="text" name="no_cu" "reaonly" value="<?php echo number_format($value['khoang_giam']) ?>" class="input_inner bg<?php echo $i; ?>"  />
					<?php } ?>
				</li>
				<li class="hg_padd" style="width:15%">
					<?php if($value['products'][0]['oum'] == 'Cái' || $return_id == 1){ ?>
						<input style="text-align: right" type="text" name="sum_amount_interest" id="sum_amount_interest" value="<?php echo '('; if(isset($value['loi_nhuan'])) echo  number_format($value['loi_nhuan']); echo ')' ?>" class="input_inner bg<?php echo $i; ?>" readonly />
					<?php }else{ ?>
						<input style="text-align: right" type="text" name="sum_amount_interest" id="sum_amount_interest" value="<?php if(isset($value['loi_nhuan'])) echo  number_format($value['loi_nhuan']) ?>" class="input_inner bg<?php echo $i; ?>" readonly />
					<?php } ?>
				</li>
				<li class="hg_padd" style="width:15%">
					<?php if($value['products'][0]['oum'] == 'Cái' || $return_id == 1){ ?>
						<input style="text-align: right" type="text" name="loi_nhuan" id="loi_nhuan" value="<?php echo '('; if(isset($value['loi_nhuan'])) echo  number_format($value['loi_nhuan']); echo ')' ?>" class="input_inner bg<?php echo $i; ?>" readonly />
					<?php }else{ ?>
						<input style="text-align: right" type="text" name="loi_nhuan" id="loi_nhuan" value="<?php if(isset($value['loi_nhuan'])) echo  number_format($value['loi_nhuan']) ?>" class="input_inner bg<?php echo $i; ?>" readonly />
					<?php } ?>
				</li>
			</ul>
			<input type="hidden"  name="company_id" id="company_id" value="<?php echo $company_id?>" />
			<?php echo $this->Form->end(); ?>
			<?php } ?>
	</div>
	<div style="position: absolute; top: 114px; margin-left: 14%; width: 100%">
		<input class="input_7" style=" width:10.5%; text-align:right;"  value=" <?php echo number_format($total_sum_amount); ?>" readonly="readonly" type="text" />
		<input class="input_7" style=" width:10.5%; text-align:right;"  value="<?php echo number_format($total_khoang_giam); ?>" readonly="readonly" type="text" />
		<input class="input_7" style=" width:10.5%; text-align:right;"  value="<?php echo number_format($total_loi_nhuan) ?>" readonly="readonly" type="text" />
		<input class="input_7" style=" width:10.5%; text-align:right;"  value="<?php echo number_format($total_interest) ?>" readonly="readonly" type="text" />
	</div>
	<span class="title_block bo_ra2">

	</span>
</div>


