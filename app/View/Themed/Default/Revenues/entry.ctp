
</style>
<?php echo $this->element('../'.$name.'/tab_option'); ?>
<style type="text/css">
.entry_receipt{
	background-color: #1198B6;
	height: 60px;
	width: 200px;
	font-weight: bold;
	/*float:left;*/
	display: inline-block;
	margin-left: 10px;
	text-align: center;
	padding-top:2.5%;
	font-size: 17px;
}
.entry_receipt a:hover{
	font-size: 18px;
	text-decoration: none;
}
ul.ul_mag li.hg_padd{
	  overflow: initial !important;
}
</style>
<div>
	<div class="entry_receipt">
	    <a style=" margin-top: 50px" href="<?php echo URL?>/revenues/thanh_toan_cong_no">Doanh số</a>
	</div>
	<div class="entry_receipt">
	    <a href="<?php echo URL?>/revenues/cong_no_thang">Doanh số tháng</a>
	</div>
	<div class="entry_receipt" >
	    <a href="<?php echo URL?>/revenues/cong_no_nam">Doanh số năm</a>
	</div>
</div>

<?php
	$tong_mua_hang = 0;
	$tong_doanh_thu = 0;
?>
<div style="width: 49%; float: left; margin-right: 1%;">
	<div class="tab_1 " style="width: 100% !important; float:left; margin-top:2%; margin-left: 1%;">
		<span class="title_block bo_ra1">
			<span class="fl_dent">
				<h4>Thu khách hàng</h4>
			</span>
		</span>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd center_text" style="width:30%">Khách hàng</li>
			<li class="hg_padd center_text" style="width:30%">Ngày </li>
			<li class="hg_padd  no_border" style="width:10%">Tổng thu</li>
		</ul>
		<div class="container_same_category" style="overflow-y: auto;height:175px;">
			<?php
			$stt = 1;
			$i = 1;
			foreach ($arr_data as $key => $value) {
				if(!isset($value['thanh_toan']) || empty($value['thanh_toan'])) continue;
				//$replace = str_replace(',','',$value['amount']);
				$tong_doanh_thu += $value['thanh_toan'];
				$i = 3 - $i;
			?>
				<?php echo $this->Form->create('Setting', array('id' => 'SettingForm_' . $key)); ?>
				<ul class="ul_mag clear bg<?php echo $i; ?>">
					<li class="hg_padd center_text" style="width:10%"><?php echo $stt++; ?></li>
					<li class="hg_padd center_text" style="width:20%">
						<input type="text" name="Setting[name]" value="<?php echo $value['company_name'] ?>" class="input_inner bg<?php echo $i; ?>"/>
					</li>
					<li class="hg_padd center_text" style="width:30%">
						<input style="text-align: center"  type="text" name="Setting[value][host]" value="<?php //echo  date('d-m-Y',$value['date']->sec) ?>" class="input_inner bg<?php echo $i; ?>" />
					</li>
					<li class="hg_padd center_text" style="width:25%">
						<input style="text-align: right" type="text" name="Setting[value][port]"  value="<?php echo number_format($value['thanh_toan']) ?>" class="input_inner bg<?php echo $i; ?>" />
					</li>
				</ul>
			<?php }
				echo $this->Form->end();
			?>

		</div>
		<div style="position: absolute;top: 246px;margin-left: 38%;">
			<input class="input_7" style=" width:68%; text-align:right; float:left; margin-left: 10%" id="tong_doanh_thu" value="<?php echo $tong_doanh_thu ?>" readonly="readonly" type="hidden">
		</div>
		<span class="title_block bo_ra2">
			<span class="float_left bt_block">
			</span>
		</span>
	</div>
	<div class="tab_1 " style="width: 100%%; float: left; margin-top:1.9%; margin-left: 1%;">
		<span class="title_block bo_ra1">
			<span class="fl_dent">
				<h4>Chi khách hàng</h4>
			</span>
		</span>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd center_text" style="width:30%">Công ty </li>
			<li class="hg_padd center_text" style="width:30%">Ngày </li>
			<li  class="hg_padd  no_border" style="width:10%">Tổng chi</li>
		</ul>
		<div class="container_same_category" style="overflow-y: auto;height:175px;">
			<?php
			$stt = 1;
			$i = 1;
			foreach ($arr_pur as $key => $value) {
				if(!isset($value['thanh_toan']) || empty($value['thanh_toan'])) continue;
				//$replace = str_replace(',','',$value['amount']);
				$tong_mua_hang += $value['thanh_toan'];
				$i = 3 - $i;
			?>
				<?php echo $this->Form->create('Setting', array('id' => 'SettingForm_' . $key)); ?>
				<ul class="ul_mag clear bg<?php echo $i; ?>">
					<li class="hg_padd center_text" style="width:10%"><?php echo $stt++; ?></li>
					<li class="hg_padd center_text" style="width:20%">
						<input type="text" name="Setting[name]" value="<?php echo $value['company_name'] ?>" class="input_inner bg<?php echo $i; ?>"/>
					</li>
					<li class="hg_padd center_text" style="width:30%">
						<input style="text-align: center"  type="text" name="Setting[value][host]" value="<?php //echo  date('d-m-Y',$value['date']->sec) ?>" class="input_inner bg<?php echo $i; ?>" />
					</li>
					<li class="hg_padd center_text" style="width:25%">
						<input style="text-align: right" type="text" name="Setting[value][port]"  value="<?php echo number_format($value['thanh_toan']) ?>" class="input_inner bg<?php echo $i; ?>" />
					</li>
				</ul>
			<?php }
				echo $this->Form->end();
			?>

		</div>
		<div style="position: absolute;top: 246px;margin-left: 34%;">
			<input class="input_7" style=" width:68%; text-align:right; float:left; margin-left: 10%" id="tong_mua_hang" value="<?php echo $tong_mua_hang ?>" readonly="readonly" type="hidden">
		</div>
		<span class="title_block bo_ra2">
			<span class="float_left bt_block"></span>
		</span>
	</div>
</div>

<div style="width: 49%; float: left;">

	<div id="other_paid" class="tab_1"  style="width: 100%%; float:left; margin-top:2%; margin-left: 1%;">
		<span class="title_block bo_ra1">
			<span class="fl_dent">
				<h4>Chi khác</h4>
			</span>
			<a title="Add new other" href="javascript:void(0)" onclick="other_add()">
				<span class="icon_down_tl top_f"></span>
			</a>
			<span class="fl_dent">
				Tháng:
					<select name="" id="month" onchange="load_list_other();">
						<?php
						$now_month = intval(date('m'));
						 for($i=1;$i<13;$i++){  ?>
						<option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
						<?php }  ?>
						<option value="all" selected>Tất cả</option>
					</select>
			</span>
			<span class="fl_dent">
				Năm:

					<select name="" id="year" onchange="load_list_other();">
						<?php
						$now_year = intval(date('Y'));
						 for($i=$first_month['year'];$i<=$now_year;$i++){  ?>
						<option value="<?php echo $i; ?>" <?php echo $i==$now_year?'selected':'' ?>><?php echo $i; ?></option>
						<?php }  ?>
					</select>
			</span>
			<span class="fl_dent">
				Người chi:
					<select name="" id="rep" onchange="load_list_other();">
						<?php foreach ($arr_our_rep as $key => $value){  ?>
						<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
						<?php }  ?>
						<option value="all" selected>Tất cả</option>
					</select>
			</span>
		</span>
		<span class="title_block">
			<?php
				$tong_chi_khac = 0;
				foreach ($arr_other as $key => $value) {
					if(isset( $value['amount_paid']))
						$tong_chi_khac += $value['amount_paid'];
				}
			?>
			<span style="width:13%;display:inline-block;margin-right:5px;">Tồng chi khác: </span><input class="right_txt" style="width:15%;" type="text" id="tong_chi_khac" value="<?php echo number_format($tong_chi_khac); ?>" readonly="">
		</span>
		<ul class="ul_mag clear bg3">
			<li class="hg_padd" style="width:13%">Ngày chi</li>
			<li class="hg_padd right_txt" style="width:15%">Số tiền</li>
			<li class="hg_padd center_txt" style="width:35%">Lý do</li>
			<li class="hg_padd" style="width:20%">Người chi</li>
		</ul>
		<div class="container_same_category" style="height:51%;overflow-y: auto" id="list_other">
			 <?php $i = 1; $j=0; $count = 0;
			foreach ($arr_other as $key => $value) {
				// if(isset( $value['amount_paid']))
				// 	$tong_chi_khac += $value['amount_paid'];
				$i = 3 - $i; $count += 1;
			?>
			<?php echo $this->Form->create('Equipment', array('id' => 'EquipmentForm_'.$key)); ?>
			<?php echo $this->Form->hidden('Equipment._id', array( 'value' => $value['_id'] )); ?>
			<ul class="ul_mag clear bg<?php echo $i; ?>">
				<li class="hg_padd line_mg" style="width:13%">
					<?php echo $this->Form->input('Equipment.date_paid', array(
							'class' => 'revenues_JtSelectDate input_1 float_left bg'.$i,
									'readonly' => true,
							'id' => 'date_paid_'.$count,
							'value' => (isset($value['date_paid']))? date("d/m/Y",$value['date_paid']->sec): date("d/m/Y")

					)); ?>
				</li>
				<li class="hg_padd line_mg" style="width:15%">
					<?php echo $this->Form->input('Equipment.amount_paid', array(
							'class' => 'right_txt amount_paid input_inner input_inner_w bg'.$i,
							'value' => (isset($value['amount_paid']))?number_format($value['amount_paid'],0):''
					)); ?>
				</li>
				<li class="hg_padd line_mg" style="width:35%">
					<?php echo $this->Form->input('Equipment.description', array(
							'class' => 'center_txt input_inner input_inner_w bg'.$i,
							'value' => (isset($value['description']))?$value['description']:''
					)); ?>
				</li>
				<li class="hg_padd line_mg" style="width:20%">
					<?php echo $this->Form->input('Equipment.our_rep', array(
							'class' => 'input_select  our_rep',
							'readonly' => true,
							'id' => 'our_rep_'.$count,
							'value'=>(isset($value['our_rep']))?$value['our_rep']:''
					));
					 echo $this->Form->hidden('Equipment.our_rep_id',array(
					 	'id'=>'our_rep_'.$count.'Id',
					 	'value'=>(isset($value['our_rep_id']))?$value['our_rep_id']:''
					 ));
					 ?>

				</li>
				<li class="hg_padd line_mg" style="width:5%">
					<div class="middle_check">
						<a title="Delete link" style="cursor:pointer;" class="delete_link" onclick="delete_record('<?php echo (string)$value['_id'] ;?>');" id="del_<?php echo (string)$value['_id'] ;?>">
							<span class="icon_remove2"></span>
						</a>
					</div>
				</li>

			</ul>
			<?php $j++; echo $this->Form->end(); ?>

			<?php
			} ?>

			<?php if( $count < 20 ){
					$count = 20 - $count;
					for ($j=0; $j < $count; $j++) {
						$i = 3 - $i;
			?>
			<ul class="ul_mag clear bg<?php echo $i; ?>">
			</ul>
			<?php }
			} ?>

		</div>
		<span class="title_block bo_ra2">
		</span>
	</div>
</div>





<div style="position:absolute; right: 1%; font-size: 15px; font-weight: bold;   margin-top: -80px;">
	<p>
		<span style="float:left;margin:5px 10px">Tổng thu: </span>
		<span style="float:right;margin:5px 10px"><?php echo number_format($tong_doanh_thu) ?></span>
	</p>
	<p>
		<span style="float:left;margin:5px 10px">Tổng chi: </span>
		<span style="float:right;margin:5px 10px">-<?php echo number_format($tong_mua_hang) ?></span>
	</p>
	<p>
		<span style="float:left;margin:5px 10px">Chi khác: </span>
		<span style="float:right;margin:5px 10px" id="tong_chi_khac">-<?php echo number_format($tong_chi_khac) ?></span>
	</p>
	<p>
		<span style="float:left;margin:5px 10px">Số tiền còn: </span>
		<span style="float:right;margin:5px 10px" id="con_lai"><?php echo number_format($tong_doanh_thu - $tong_mua_hang - $tong_chi_khac) ?></span>
	</p>
</div>


<script type="text/javascript">

	$(function(){
		mount_paid();
		$(".our_rep").combobox(<?php echo json_encode($arr_our_rep); ?>);
		$('.revenues_JtSelectDate').datepicker({dateFormat:'dd/mm/yy',changeMonth: true, changeYear: true, yearRange: "c-70:c+3"});
		$('.datainput_allocation .combobox_button').click(function(){
			$('.hg_padd datainput_allocation .combobox_selector').attr('style','width: 100px !important; left: -10px; top: 14px;');
		});


    });
</script>
<script type="text/javascript">
	$(function(){

		$('.container_same_category').mCustomScrollbar({
			scrollButtons:{
				enable:false
			}
		});

		$("#other_paid").on('change', "form :input", function() {
			$.ajax({
				url: '<?php echo URL; ?>/revenues/other_auto_save',
				timeout: 15000,
				type:"post",
				data: $(this).closest('form').serialize(),
				success: function(data){
					if(data.status == "ok" ){
						$("span#tong_chi_khac").text(data.tong_chi_khac);
						var tong_chi_khac_sort = 0;
						$('.amount_paid').each(function(){
							value = $(this).val().replace(',','');
							while(value.indexOf(',')>0){
								value = value.replace(',','');
							}
							tong_chi_khac_sort+=parseInt(value);
						})
						$("input#tong_chi_khac").val(tong_chi_khac_sort.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
						con_lai =  $("#tong_doanh_thu").val() - parseFloat(data.tong_chi_khac_value) - parseFloat($("#tong_mua_hang").val());
						con_lai = con_lai.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						$("span#con_lai").text(con_lai);
					}else{
						alerts("Error: ", data.message);
					}
				}
			});
		});
	});


	function other_add(){
		$.ajax({
			url: '<?php echo URL; ?>/revenues/other_add',
			timeout: 15000,
			success: function(result){
				result = $.parseJSON(result);
				var count = $('#other_paid .container_same_category form').length;
				var bg = count % 2 == 0 ? 'bg2' : 'bg1';
				// var arr_date = new Date().toString().split(' ');
				// var date = arr_date[2]+' '+arr_date[1]+', '+arr_date[3];
				var date = new Date();
				var ngay = date.getDate();
				if(ngay<10){
					ngay='0'+ngay;
				}
				var thang = date.getMonth()+1;
				if(thang<10){
					thang='0'+thang;
				}
				var nam = date.getFullYear();
				date = ngay+'/'+thang+'/'+nam;
				if( result.status == 'ok' ) {
					var html = '<form action="/revenues/entry" id="EquipmentForm_'+ result.id +'" method="post" accept-charset="utf-8">' +
									'<input type="hidden" name="_method" value="POST">' +
									'<input type="hidden" name="data[Equipment][_id]" value="'+ result.id +'" id="EquipmentId">' +
									'<ul class="ul_mag clear '+ bg +'">' +
										'<li class="hg_padd line_mg" style="width:13%">' +
											'<input name="data[Equipment][date_paid]" class="revenues_JtSelectDate input_1 float_left '+ bg +'" value="'+date+'" type="text" id="EquipmentDatePaid_'+result.id +'" readonly="">' +
										'</li>' +
										'<li class="hg_padd line_mg" style="width:15%">' +
											'<input name="data[Equipment][amount_paid]" class="right_txt amount_paid input_inner input_inner_w '+ bg +'" value="" type="text" id="EquipmentAmountPaid">' +
										'</li>' +
										'<li class="hg_padd line_mg" style="width:35%">' +
											'<input name="data[Equipment][description]" class="center_txt input_inner input_inner_w '+ bg +'" value="" type="text" id="EquipmentDescription">' +
										'</li>' +
										'<li class="hg_padd line_mg" style="width:20%">' +
											'<input name="data[Equipment][our_rep]" class="input_select  our_rep" id="our_rep_'+(count+1)+'" type="text" readonly="">' +
											'<input type="hidden" name="data[Equipment][our_rep_id]" id="our_rep_'+(count+1)+'Id" value="">'+
										'</li>' +
										'<li class="hg_padd line_mg" style="width:5%">'+
											'<div class="middle_check">'+
												'<a title="Delete link" style="cursor:pointer;" class="delete_link" onclick="delete_record(\''+result.id.trim()+'\');" id="del_'+result.id+'">'+
													'<span class="icon_remove2"></span>'+
												'</a>'+
											'</div>'+
										'</li>'+
									'</ul>' +
								'</form>';
					$('.mCSB_container ul:first-child', '#other_paid').remove();
					if( count ) {
						$('.mCSB_container form:first-child', '#other_paid').prepend(html);
					} else {
						$('.mCSB_container', '#other_paid').prepend(html);
					}
					$('.mCSB_container ul_mag:last', '#other_paid').remove();
					$('.revenues_JtSelectDate').datepicker({dateFormat:'dd/mm/yy',changeMonth: true, changeYear: true, yearRange: "c-70:c+3"});
					$('#our_rep_'+(count+1)).combobox(<?php echo json_encode($arr_our_rep); ?>);
					mount_paid();
				} else {
					alerts('Message', result.message);
				}
			}
		});
	}


	function delete_record(iditem){
		var str = 'Are you sure you want to delete this record?';
		confirms('Confirm delete', str,
				function(){
					$("#confirms_ok").focus();
					$.ajax({
						url: '<?php echo URL; ?>/revenues/other_delete/'+iditem,
						type:"POST",
						success: function(data){
							if(data.status=='ok'){
								var bg_class = $("#del_"+iditem).parent().parent().parent().attr('class');
								$("#del_"+iditem).parent().parent().parent().animate({
									  opacity:'0.1',
									  height:'1px'
									},500,function(){$(this).remove();});
								$("span#tong_chi_khac").text(data.tong_chi_khac);
								con_lai =  $("#tong_doanh_thu").val() - parseFloat(data.tong_chi_khac_value) - parseFloat($("#tong_mua_hang").val());
								con_lai = con_lai.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								$("span#con_lai").text(con_lai);
								var html = '<ul class="'+bg_class+'"></ul>';
								$('.mCSB_container', '#other_paid').append(html);
							}else{
								alerts("Error: ", data.message);
							}
						}
					});
				},function(){
					//else
					console.log('Cancel');
				});
	}

	function mount_paid(){
		$(".amount_paid").keydown(function(e){
	            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	                 // Allow: Ctrl+A
	                (e.keyCode == 65 && e.ctrlKey === true) ||
	                 // Allow: home, end, left, right
	                (e.keyCode >= 35 && e.keyCode <= 39)) {
	                     // let it happen, don't do anything
	                     return;
	            }
	            // Ensure that it is a number and stop the keypress
	            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)  ) {
	                e.preventDefault();
	            }
	        });

		$(".amount_paid").keyup(function(e){
			if($(this).val().length){
				var value = $(this).val().replace(',','');
				while(value.indexOf(',') > 0){
					value = value.replace(',','');
				}
				value = parseInt(value);
				value = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				$(this).val(value);

			}
		})
		$(".amount_paid").on("blur",function(){
			$(this).trigger('change');
		})
	}

	function load_list_other(){
		var month = $("#month").val();
		var year = $("#year").val();
		var rep = $("#rep").val();
		data = {};
		data['sort']=true;
		data['month']=month;
		data['year']=year;
		data['rep']=rep;
		$.ajax({
			url: '<?php echo URL; ?>/revenues/entry',
			type: 'POST',
			data:data,
			success:function(data){
				data = $.parseJSON(data);
				count_row = 20 - data.length;
				var html='';
				var tong_chi_khac = 0;
				if(data.length >0){
					$.each(data,function(key,value){
						var bg = key%2==0?'bg2':'bg1';
						var date = new Date(value['date_paid']['sec'] * 1000);
						var ngay = date.getDate();
						if(ngay<10){
							ngay='0'+ngay;
						}
						var thang = date.getMonth()+1;
						if(thang<10){
							thang='0'+thang;
						}
						var nam = date.getFullYear();
						date = ngay+'/'+thang+'/'+nam;
						if(value['amount_paid']==undefined){
							value['amount_paid']=0;
						}else{
							tong_chi_khac+=value['amount_paid'] ;
							value['amount_paid'] = value['amount_paid'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
						}
						if(value['description']==undefined){
							value['description']='';
						}
						if(value['our_rep']==undefined){
							value['our_rep']='';
						}

						html += '<form action="/revenues/entry" id="EquipmentForm_'+ value['_id']['$id'] +'" method="post" accept-charset="utf-8">' +
										'<input type="hidden" name="_method" value="POST">'+
										'<input type="hidden" name="data[Equipment][_id]" value="'+ value['_id']['$id'] +'" id="EquipmentId">' +
										'<ul class="ul_mag clear '+ bg +'">' +
											'<li class="hg_padd line_mg" style="width:13%">' +
												'<input name="data[Equipment][date_paid]" class="revenues_JtSelectDate input_1 float_left '+ bg +'" value="'+date+'" type="text" id="EquipmentDatePaid_'+value['_id']['$id'] +'" readonly="">' +
											'</li>'  +
											'<li class="hg_padd line_mg" style="width:15%">' +
												'<input name="data[Equipment][amount_paid]" class="right_txt amount_paid input_inner input_inner_w '+ bg +'" value="'+value['amount_paid']+'" type="text" id="EquipmentAmountPaid">' +
											'</li>' +
											'<li class="hg_padd line_mg" style="width:35%">' +
												'<input name="data[Equipment][description]" class="center_txt input_inner input_inner_w '+ bg +'" value="'+value['description']+'" type="text" id="EquipmentDescription">' +
											'</li>' +
											'<li class="hg_padd line_mg" style="width:20%">' +
												'<input name="data[Equipment][our_rep]" class="input_select  our_rep" id="our_rep_'+(key+1)+'" type="text" value="'+value['our_rep']+'" readonly="">' +
												'<input type="hidden" name="data[Equipment][our_rep_id]" id="our_rep_'+(key+1)+'Id" value="">'+
											'</li>' +
											'<li class="hg_padd line_mg" style="width:5%">'+
												'<div class="middle_check">'+
													'<a title="Delete link" style="cursor:pointer;" class="delete_link" onclick="delete_record(\''+value['_id']['$id'].trim()+'\');" id="del_'+value['_id']['$id']+'">'+
														'<span class="icon_remove2"></span>'+
													'</a>'+
												'</div>'+
											'</li>'+
										'</ul>' +
									'</form>';
					})
				}
				for(i=data.length;i<20;i++){
					var bg = i%2==0?'bg2':'bg1';
					html+='<ul class="ul_mag clear '+bg+'"></ul>';
				}
				$('.mCSB_container', '#other_paid').html(html);
				$('.revenues_JtSelectDate').datepicker({dateFormat:'dd/mm/yy',changeMonth: true, changeYear: true, yearRange: "c-70:c+3"});
				$('.our_rep').combobox(<?php echo json_encode($arr_our_rep); ?>);
				mount_paid();
				$("#tong_chi_khac").val(tong_chi_khac.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
			}
		})

	}

</script>



