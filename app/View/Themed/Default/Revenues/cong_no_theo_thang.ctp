<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4 id="setting_name">
			</h4>
		</span>
	</span>

	<ul class="ul_mag clear bg3">
		<li class="hg_padd center_text" style="width:30%">Tên công ty</li>
		<li class="hg_padd center_text" style="width:12%">Doanh số</li>
		<li class="hg_padd center_text no_border" style="width:12%">Khoảng giảm</li>
		<li class="hg_padd center_text no_border" style="width:12%">Lãi thực</li>
		<li class="hg_padd center_text no_border" style="width:12%">Lợi nhuận</li>
	</ul>

	<div class="container_same_category" style="height: 449px;overflow-y: auto">
		<?php
		$stt = 1;
		$i = 1;
		$count = count($arr_data);
		$total_no_cu = 0;
		$tong_khoang_giam = 0;
		$total_sum_amount = 0;
		$tong_loi_nhuan = 0;
		foreach ($arr_data as $key => $value) {
			$khoang_giam = 0;
			$loi_nhuan = 0;
			$sum_amount = 0;
			foreach($value['sum_amount'] as $kk => $vvv){
				$sum_amount += $vvv;
				$total_sum_amount += $vvv;
			}
			foreach($value['khoang_giam'] as $k => $v){
				$khoang_giam  += $v;
				$tong_khoang_giam += $v;
			}
			foreach($value['loi_nhuan'] as $kk => $vv){
				$loi_nhuan += $vv;
				$tong_loi_nhuan += $vv;
			}
			$i = 3 - $i;
		?>
		<?php echo $this->Form->create('Setting', array('id' => 'SettingForm_' . $key)); ?>
		<input type="hidden" id="salesorder_id" value="<?php echo $key ?>" />
 		<ul class="ul_mag clear bg<?php echo $i; ?>">
			<li class="hg_padd" style="width:30%">
				<input style="text-align:center" type="text" name="date_modified" value="<?php echo $value['company_name'];?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>

			<li class="hg_padd" style="width:12%">
				<input style="text-align: right" type="text" name="sum_amount" value="<?php echo number_format($sum_amount) ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>


			<li class="hg_padd" style="width:12%">
				<input style="text-align: right" "readonly" type="text" name="paid" value="<?php echo  number_format(isset($khoang_giam ) ? $khoang_giam * -1 : 0)  ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>
			<li class="hg_padd" style="width:12%">
				<input style="text-align: right" type="text" name="no_cu" "readonly" value="<?php echo number_format($loi_nhuan)?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>
			<li class="hg_padd" style="width:12%">
				<input style="text-align: right" type="text" name="total_balance" value="<?php echo number_format($loi_nhuan + $khoang_giam ) ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>
		</ul>
		<?php echo $this->Form->end(); ?>
		<?php } ?>
	</div>
	<div style="  position: absolute;top: 115px;width: 65%;">
		<input class="input_7" style=" width:12%; text-align:right;  margin-left: 32%;" id="sum_sub_total" value="<?php echo number_format($total_sum_amount) ?>" readonly="readonly" type="text">
		<input class="input_7" style=" width:12%; text-align:right;  margin-left: 1%; " id="sum_sub_total" value="<?php echo number_format($tong_khoang_giam) ?>" readonly="readonly" type="text">
		<input class="input_7" style=" width:12%; text-align:right;   margin-left: 1%;" id="sum_sub_total" value="<?php echo number_format($tong_loi_nhuan) ?>" readonly="readonly" type="text">
		<input class="input_7" style=" width:12%; text-align:right;   margin-left: 1%;" id="sum_sub_total" value="<?php echo number_format($tong_loi_nhuan + $tong_khoang_giam) ?>" readonly="readonly" type="text">
	</div>
	<span class="title_block bo_ra2">

	</span>
</div>
