<style type="text/css">
.backg3{
	background-color: #B8B8B8;
}
</style>
<div class="tab_1 full_width">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4 id="setting_name">
				Danh sách đơn hàng
			</h4>
		</span>
	</span>

	<ul class="ul_mag clear bg3">
		<li class="hg_padd center_text" style="width:20%">Ngày mua hàng</li>
		<li class="hg_padd center_text" style="width:15%">Tổng tiền toa hàng</li>
		<li class="hg_padd center_text" style="width:15%">Nợ cũ</li>
		<li class="hg_padd center_text" style="width:20%">Thanh toán</li>
		<li class="hg_padd center_text" style="width:15%">Còn lại</li>
	</ul>

	<div id="paid" class="container_same_category" style="height: 317px;overflow-y: auto">
		<?php
		$stt = 1;
		$i = 1;
		$count = count($arr_pur);
		foreach ($arr_pur as $key => $value) {
			$i = 3 - $i;
		?>
		<?php echo $this->Form->create('Setting', array('id' => 'SettingForm_' . $key)); ?>
		<input type="hidden" id="salesorder_id" value="<?php echo $value['_id'] ?>" />
		<?php $return_id = isset($value['return_id']) ? $value['return_id'] : 0 ?>
 		<ul class="ul_mag clear bg<?php echo $i;?>">
			<li class="hg_padd" style="width:20%">
				<input style="text-align:center" type="text" id="date_modified" name="date_modified" value="<?php echo date('d-m-Y',$value['purchord_date']->sec) ?>" class="input_inner bg<?php echo $i; ?>" readonly />
			</li>
  			<li class="hg_padd" style="width:15%">
  			<?php if($value['products'][0]['oum'] == 'Cái' || $return_id == 1){ ?>
				<input style="text-align: right" type="text" name="sum_amount" id="sum_amount" value="<?php if(isset($value['sum_amount']))   echo '('; echo number_format($value['sum_amount']); echo ')' ?>" class="input_inner bg<?php echo $i; ?>" readonly />
				<?php }else{ ?>
				<input style="text-align: right" type="text" name="sum_amount" id="sum_amount" value="<?php if(isset($value['sum_amount'])) echo  number_format($value['sum_amount']) ?>" class="input_inner bg<?php echo $i; ?>" readonly />
				<?php } ?>
			</li>

			<li class="hg_padd" style="width:15%">
				<input style="text-align: right" type="text" name="no_cu" readonly value="<?php echo number_format($value['no_cu']) ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>
			<li class="hg_padd" style="width:20%">
				<?php
					$thanh_toan = 0;
					$thanh_toan = isset($value['thanh_toan']) ? $value['thanh_toan'] : 0;
				 ?>
				<input style="text-align: center" class="paid" type="text" name="paid" id="<?php echo $value['_id']; ?>" value="<?php echo number_format($thanh_toan) ?>" class="input_inner bg<?php echo $i; ?>"   />
			</li>
			<li class="hg_padd" style="width:15%">
				<input style="text-align: right" type="text" name="total_balance" value="<?php echo number_format($value['con_lai'] ) ?>" class="input_inner bg<?php echo $i; ?>" readonly />
			</li>
		</ul>
		<input type="hidden"  name="company_id" id="company_id" value="<?php echo $company_id?>" />
		<?php echo $this->Form->end(); ?>
		<?php } ?>
	</div>
	<span class="title_block bo_ra2">
		<span class="float_left bt_block"><?php echo translate('Edit or create values for list'); ?>.</span>
	</span>
</div>

<div class="tab_1 full_width" style="margin-top: 11px">
	<span class="title_block bo_ra1">
		<span class="fl_dent">
			<h4 id="setting_name">
				Xem lại số lần trả cho từng đơn hàng
			</h4>
		</span>
	</span>
	<ul class="ul_mag clear bg3">
		<li class="hg_padd center_text" style="width:20%">Ngày trả tiền</li>
		<li class="hg_padd " style="width:20%;text-align: left ">Hình thức thanh toán</li>
		<li class="hg_padd " style="width:20%; text-align: right">Số tiền thanh toán</li>
	</ul>
	<div id="paid_amount" class="container_same_category" style="height: 224px;overflow-y: auto">
		<?php
		$stt = 1;
		$i = 1;
		$count = count($arr_company);
		/*usort($arr_company['thanh_toan'], function($a,$b){
            return $a['date_modified']->sec > $b['date_modified']->sec;
    	});*/
		foreach ($arr_company['thanh_toan'] as $key => $value) {
			$i = 3 - $i;
		?>
		<?php echo $this->Form->create('Setting', array('id' => 'SettingForm_' . $key)); ?>
		<input type="hidden" id="salesorder_id" value="<?php echo $key ?>" />
 		<ul class="ul_mag clear bg<?php echo $i;?>">
			<li class="hg_padd" style="width:20%">
				<input style="text-align:center" type="text" name="date_modified" value="<?php echo $value['date_modified'] ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>
			<li class="hg_padd" style="width:20%">
				<!-- <input style="text-align: right" type="text" name="no_cu" "reaonly" value="<?php echo  $value['sum_amount'] ?>" class="input_inner bg<?php echo $i; ?>"  /> -->
				<select name="type_paid">
					<option value="chuyen_khoan">Chuyển khoản</option>
					<option value="tien_mat">Tiền mặt</option>
				</select>
			</li>

			<li class="hg_padd" style="width:20%">
				<input style="text-align: right" type="text" name="no_cu" "reaonly" value="<?php if(isset($value['amount']))  echo number_format((int)$value['amount']) ?>" class="input_inner bg<?php echo $i; ?>"  />
			</li>
		</ul>
		<input type="hidden"  name="company_id" id="company_id" value="<?php echo $company_id?>" />
		<?php echo $this->Form->end(); ?>
		<?php } ?>
	</div>
	<span class="title_block bo_ra2">
		<span class="float_left bt_block"><?php echo translate('Edit or create values for list'); ?>.</span>
	</span>
</div>
