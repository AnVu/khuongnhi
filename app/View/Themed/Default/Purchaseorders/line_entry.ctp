<?php echo $this->element('js/line'); ?>
<?php
		foreach($arr_settings['relationship'][$sub_tab]['block'] as $key => $arr_val){

			echo $this->element('box',array('key'=>$key,'arr_val'=>$arr_val));

		}
?>
<p class="clear"></p>
<input type="hidden" id="is_add" value="0" />
<script>
$(document).ready(function() {
	//tạo thêm 1 products line mới
	$("#bt_add_products").click(function() {
		/*var taxper = $('#tax').val();
		taxper = taxper.split("%");
		taxper = taxper[0];*/
		var datas = {
			'products_id' : '',
			'products_name' : 'This is new record. Click for edit',
			'sizew_unit' : 'in',
			'sizeh_unit' : 'in',
			'sell_by' : 'area',
			'quantity':0,
			'oum' : 'Sq.ft.',
		};
		$("#is_add").val(1);
		save_option('products',datas,'',1,'line_entry','add');
	});

/*	fixHiddenCombobox();
	var is_add = $("#is_add").val();
	if(is_add){
		var ids = $(".choice_sku:last","#container_products").attr("id");
		if(ids!=undefined){
			ids  = ids.split("_");
			var ind = ids.length;
			var ids = ids[ind-1];
			if($("#txt_products_name_"+ids).offset()!=undefined){
				var offset = $("#txt_products_name_"+ids).offset().top;
				$("#container_products").mCustomScrollbar("scrollTo", "bottom");
			}
		}
		$("#is_add").val(0);
	}*/
	$(".del_products").focusin(function(){
		ajax_note_set("");
		var ids = $(this).attr("id");
		ids  = ids.split("_");
		var ind = ids.length;
		var idfield =  parseInt(ids[ind-1]);
		ajax_note_set(" Press ENTER to delete the line:"+(idfield+1));
	});

	$(".del_products").focusout(function(){
		ajax_note("");
		var ids = $(this).attr("id");
			ids = ids.split("_");
		var index = ids.length;
		ids  = parseInt(ids[index-1])+1;
		$(".jt_line_over").removeClass('jt_line_over');
		$("#listbox_products_"+ids).addClass('jt_line_over');
	});

	$(".choice_code").click(function(){
		var ids = $(this).attr("id");
		var key_click_open = ids;
		ids  = ids.split("_");
		var ind = ids.length;
		var ids = ids[ind-1];
	});

	$(".rowedit input").keydown(function(e){
		var keycode = e.keyCode;
		if(keycode==39){
			if($(this).parent().hasClass('combobox'))
				nextinput = $(this).parent().parent().parent().next().find('input');
			else
			nextinput = $(this).parent().parent().next().find('input');
			nextinput.focus();
			nextinput.click();
		}
		if(keycode==38){
			var id_current = $(this).attr("id").split('_');
			id_current = parseInt(id_current[id_current.length - 1]);
			if(id_current > 0){
				ul_prev = $("#listbox_products_"+(id_current-1));
				class_input = $(this).attr("class").split(" ");
				class_input = class_input[class_input.length-1];
				input_prev = ul_prev.find("."+class_input);
				input_prev.focus();
				input_prev.click();
			}
		}
		if(keycode==37){
			if($(this).parent().hasClass('combobox'))
				nextinput = $(this).parent().parent().parent().prev().find('input');
			else
				nextinput = $(this).parent().parent().prev().find('input');
			nextinput.focus();
			nextinput.click();
		}
		if(keycode==40){
			var id_current = $(this).attr("id").split('_');
			id_current = parseInt(id_current[id_current.length - 1]);
			if(id_current >= 0){
				ul_prev = $("#listbox_products_"+(id_current+1));
				class_input = $(this).attr("class").split(" ");
				class_input = class_input[class_input.length-1];
				input_prev = ul_prev.find("."+class_input);
				input_prev.focus();
				input_prev.click();
			}
		}
	});

	$('.rowedit input').change(function(){
		//nhan id
		var isreload=1;
		var names = $(this).attr("name");
		var intext = 'box_test_'+names;
		var inval = $(this).val();
		var ids  = names.split("_");
		var index = ids.length;
		var ids = ids[index-1];
		var price_key = new Array("sell_price","unit_price","sub_total","tax","amount");
		var pricetext_key = new Array("unit_price","sub_total","amount");
		//khoi tao gia tri luu
		names = names.replace("_"+ids,"");
		names = names.replace("cb_","");
		if(names=='sell_price' || names=='area' || names=='sub_total' || names=='amount' || names=='unit_price' )
			inval = UnFortmatPrice(inval);
		var values = new Object();
			values[names]=inval;
		if(names == "products_name"){
			save_option('products',values,ids);
			return false;
		}
		//format price
		if(price_key.indexOf(names) != -1){
			$('#'+names+'_'+ids).val(FortmatPrice(inval));
		}else
			$('#'+names+'_'+ids).val(inval);
		$.ajax({
			url : "<?php echo URL.'/purchaseorders/khuongnhi_cal_price' ?>",
			type: "POST",
			data: {key : ids, name : names, value : inval },
			dataType: "json",
			success: function(result){
				$("#txt_amount_"+ids).text(result.amount);
				$("#sum_sub_total").val(result.sum_sub_total);
				$("#sum_tax").val(result.sum_tax);
				$("#sum_amount").val(result.sum_amount);
			}
		})

	});
	$('.ul_mag.clear.line_box').on("click",function(){
		$('.ul_mag.clear.line_box').removeClass('addColor');
		$(this).addClass('addColor');
	})
$('.ul_mag.clear.line_box *').on("click",function(){
		$('.ul_mag.clear.line_box').removeClass('addColor');
		$(this).closest('.ul_mag.clear.line_box').addClass('addColor');
})

});

function update_sum(handleData){
	var keyfield = {
			"sub_total"		: "sub_total",
			"tax"			: "tax",
			"amount"		: "amount",
			"sum_sub_total"	: "sum_sub_total",
			"sum_tax"		: "sum_tax",
			"sum_amount"	: "sum_amount"
		};
	var keyfield = JSON.stringify(keyfield);
	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/update_sum',
		type:"POST",
		dataType: "json",
		data: {subdoc:'products',keyfield:keyfield},
		success: function(ret){
			$('#sum_sub_total').val(FortmatPrice(sum_ret['sum_sub_total']));
			$('#sum_tax').val(FortmatPrice(sum_ret['sum_tax']));
			$('#sum_amount').val(FortmatPrice(sum_ret['sum_amount']));
			if(handleData!=undefined)
				handleData(ret);
		}
	});
}



/**
/* Tính lại giá cho 1 line
/* Use: values là mảng data cần thay đổi
**/
function cal_line_entry(values,handleData){
	var jsonString = JSON.stringify(values);
	$.ajax({
		url: '<?php echo URL.'/'.$controller;?>/ajax_cal_line',
		type:"POST",
		dataType: "json",
		data: {arr:values},
		success: function(ret){
			if(handleData!=undefined)
				handleData(ret);
		}
	});
}


/**
/* Change list box OUM (after Sell price)
/*
**/
function change_uom_item(val,ids){
	var units = '';
	if(val=='unit')
		units = 'unit';
	else
		units = 'Sq.ft.';
	var old_html = $("#box_edit_oum"+"_"+ids).html();
		old_html = old_html.split("<span");
		old_html = old_html[1].split(">");
		old_html = old_html[1];
		old_html = old_html.replace('value','value="'+units+'" title');
		$("#box_edit_oum"+"_"+ids+" .combobox").remove();
		$("#box_edit_oum"+"_"+ids).prepend(old_html+"  />");

	$.ajax({
		url: '<?php echo URL.'/';?>products/select_render',
		dataType: "json", //luu y neu dung json
		type:"POST",
		data: {sell_by:val},
		success: function(jsondata){
			$("#oum"+"_"+ids).combobox(jsondata);
		}
	});
}

function after_choose_products(ids,names,keys){
	var origin_keys = keys;
	$("#window_popup_products"+ origin_keys).data("kendoWindow").close();
	var keys = $("#product_choice_sku").val();
	var opid  = keys.split("_");
	var	index = opid.length;
		opid = opid[index-1];
		keys = keys.replace("_"+opid,"");
	if(keys=='change'){
		var values = new Object();
		var arr = {
					"code"		:"code",
					"sku"		:"sku",
					"sell_by"	:"sell_by",
					"sell_price":"sell_price",
					"unit_price":"unit_price",
					"prime_cost": "prime_cost",
				 };
		var arr_data_from = JSON.parse($("#after_choose_products"+ origin_keys + ids).val());
		values = arr;
		for(var m in arr){
			if(m!='_id'){
				keyss = arr[m];
				values[keyss] = arr_data_from[m];
			}
		}
		values['custom_unit_price'] = arr_data_from['unit_price'];
		//tax
		values['products_id'] = ids;
		values['products_name'] = arr_data_from.name;
		console.log(values);
		save_option("products",values,opid,0,'line_entry','',function(opid){
			reload_subtab('line_entry');
		},'code');
	}
}

</script>

<style type="text/css">
.addColor{
	background-color: rgb(85, 164, 213);
}
</style>