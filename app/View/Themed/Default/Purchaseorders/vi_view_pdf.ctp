<?php
$author = 'Khuong Nhi';
 if($_SESSION['default_lang']=='vi'){
 	//$title = 'Đặt hàng - Khuong Nhi';
 	$subject = 'Đặt hàng';
	$keywords = 'Đặt hàng, PDF';
	$shipping_address_label = 'Nơi nhận';
	$salesorder_label = 'Phiếu Mua Hàng';
	$salesorder = 'Đặt hàng';
	$date = 'Ngày';
	$due_date = 'Hạn trả';
	$sku = 'Mã';
	$desc = 'Tên sản phẩm';
	$width = 'Quy cách';
	$height = 'Đv tính';
	$unit_price = 'Đơn giá';
	$qty = 'Số lượng';
	$line_total = 'Thành tiền';
	$sub_total = 'Tổng trước thuế';
	$hst_gst = 'Thuế';
	$total = 'Tổng sau thuế';
	//$note = 'Ghi chú';
	$fotter_text = 'Đây là ước tính dựa trên thông tin cung cấp. Báo giá có giá trị trong 30 ngày.    ';
 } else {
	$title = 'Khuong Nhi Sale Order';
	$subject = 'Đặt hàng';
	$keywords = 'Sale Order, PDF';
	$shipping_address_label = 'Shipping address';
	$salesorder_label = 'Đơn Đặt Hàng';
	$salesorder = 'Sales Order';
	$job_no = 'Job no';
	$date = 'Date';
	$cpo_no = 'CPO No';
	$ac_no = 'A/c no';
	$terms = 'Terms';
	$due_date = 'Due Date';
	$required_date = 'Required date';
	$custom_po_no = 'Customer PO no';
	$sku = 'SKU';
	$desc = 'Description';
	$width = 'Width';
	$height = 'Height';
	$unit_price = 'Unit Price';
	$qty = 'Qty';
	$line_total = 'Line total';
	$sub_total = 'Sub total';
	$hst_gst = 'HST/GST';
	$total = 'Total';
	$note = 'Note';
	$fotter_text = 'This is an estimate based on the supplied information. Quotation is valid for 30 days.    ';
 }
App::import('Vendor','xtcpdf');
$pdf = new XTCPDF('P','mm','A4',true,'UTF-8',false,false);
$pdf->xfootertext = $fotter_text;
$pdf->xfooterfontsize = 10;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($author);
//$pdf->SetTitle($title);
$pdf->SetSubject($subject);
$pdf->SetKeywords($keywords);
$pdf->isSO = 1;
/*$pdf->xfootertext = 'Ghi chú: Hàng lỗi Quý Khách đổi/trả trong 3 tháng. Hàng không vừa ý Quý Khách được đổi trong 3 ngày (hàng còn nguyên ri, không dơ, không mất thẻ bài), không hoàn lại tiền.';*/
$pdf->xfootertext = '';
// set default header data
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(2);

// set margins
$pdf->SetMargins(10, 3, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(5);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 0);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$textfont = 'freesans';
// set font
$pdf->SetFont($textfont, '', 9);

// add a page
$pdf->AddPage();
// create some HTML content #1f1f1f
$html = '
	<table cellpadding="2" cellspacing="0" style="width:100%;">
		<tr>
			<td rowspan="2" style="color:#1f1f1f;width:35%;">
				<img  src="'.$logo_link.'" alt=""  style="width:220px; height:134px"/>
			</td>
			<td style="width:35%"></td>
			<td style="text-align: right">'.$info_data->current_day.'</td>
		</tr>
		<tr>
			<td colspan="2" >
				<div style="margin-bottom:1px;font-size:14px">
					'.$company_address.'
				</div>
			</td>
		</tr>
	</table>
	<div style="text-align:center; font-size:30px; font-weight:bold; color: #000000; width:30%; ">'.$salesorder_label.' '.$info_data->code.'</div>
	<span style="text-align: center; font-size: 20px">(Ngày: '.date('d-m-Y',$info_data->date).')</span>
	<div style="height:1px"></div>
	<div style="font-size: 18px"><b>Tên nhà cung cấp:</b> '.$info_data->company_name.' - <b>ĐC:</b> '.$shipping_address.' - <b>SĐT:</b>'.$info_data->phone.'</div>
		';
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

//<div class="option">Option 01</div>

$html2 = '
	<style>
		td{
			line-height:2px;
			border: 0.5px solid #303030;
		}
		td.first{
			border-left:0.5px solid #303030;
		}
		td.end{
			border-right:0.5px solid #303030;
		}
		td.top{
			color:#303030;
			font-weight:bold;
			background-color:#eeeeee;
			border-top:0.5px solid #303030;
		}
		td.bottom{
			border-bottom:0.5px solid #303030;
		}
	</style>


	<table cellpadding="3" cellspacing="0" class="maintb">
		<tr >
			<td width="12%" class="first top">
				&nbsp;'.$sku.'
			</td>
			<td width="30%" class="top">
				'.$desc.'
			</td>
			<td align="center" width="9%" class="top">
				'.$height.'
			</td>
			<td align="right" width="9%" class="top">
				'.$width.'
			</td>
			<td align="right" width="10%" class="top">
			    '.$qty.'
			</td>
			<td align="right" width="15%" class="top">
				'.$unit_price.'
			</td>
			<td align="right" width="15%" class="end top">
				'.$line_total.'
			</td>
		</tr>';
$html2 .= $html_cont;
//echo $html_cont; die;

$html2 .= '</table>';

$pdf->writeHTML($html2, true, false, true, false, '');

$html4 .= $last_table;
$pdf->writeHTML($html4, true, false, true, false, '');

$html3 .= $last_table_final;
$pdf->writeHTML($html3, true, false, true, false, '');




//PLEASE SIGN AND DATE TO INDICATE YOUR ACCEPTANCE
$html3 = '<style>
		table.maintb td{
			line-height:2px;
		}
		table.maintb td.first{
			border-left:1px solid #e5e4e3;
		}
		table.maintb td.end{
			border-right:1px solid #e5e4e3;
		}
		table.maintb td.top{
			color:#000000;
			font-weight:bold;
			background-color:#565656;
			border-top:1px solid #e5e4e3;
		}
		table.maintb td.bottom{
			border-bottom:1px solid #e5e4e3;
			border-right:1px solid #e5e4e3;
			height:120px;
		}
		.option{
			color: #000000;
			font-weight:bold;
			font-size:18px;
		}
		table.maintb{

		}

	</style>
	';

// reset pointer to the last page
$pdf->lastPage();



// ---------------------------------------------------------
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->Output('example_001.pdf', 'I');

$pdf->Output('upload/'.$filename.'.pdf', 'F');
echo '<script>window.location.assign("'.URL.'/upload/'.$filename.'.pdf");</script>';
?>