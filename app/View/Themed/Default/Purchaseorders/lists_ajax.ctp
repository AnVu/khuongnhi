<?php
    $i = 2;
    $delete = $this->Common->check_permission($controller.'_@_entry_@_delete',$arr_permission);
    $arr_companies = Cache::read('arr_companies');
    if(!$arr_companies)
        $arr_companies = array();
    $count = count($arr_companies);
    $original_minimun = Cache::read('minimun');
    $product = Cache::read('minimun_product');
    $_controller->selectModel('Company');
    $_controller->selectModel('Quotation');
    $_controller->selectModel('Job');
    if(!$original_minimun){
        $_controller->selectModel('Product');
        $_controller->selectModel('Stuffs');
        $original_minimun = 50;
        $product = $_controller->Stuffs->select_one(array('value'=>"Minimun Order Adjustment"),array('product_id'));
        $p = $_controller->Product->select_one(array('_id'=> new MongoId($product['product_id'])),array('sell_price'));
        if(isset($p['sell_price']))
            $original_minimun = (float)$p['sell_price'];
        Cache::write('minimun',$original_minimun);
        Cache::write('minimun_product',$product);
    }
?><br>
<?php
    $current_date = strtotime(date('Y-m-d'));
?>
<?php foreach ($arr_orders as $order){
    if ($i == 2)
        $i = $i - 1;
    else
        $i = $i + 1;
    $late = '';
?>
    <ul class="ul_mag clear bg<?php echo $i ?>" id="order_<?php echo (string) $order['_id']; ?>" <?php //echo $late; ?>>
        <?php
            if(!isset($order['company_id']))
                $order['company_id'] = '';
            if($order['purchase_orders_status'] == 'Cancelled')
                $order['sum_sub_total'] = 0;
            else if(!isset($arr_companies[(string)$order['company_id']])){
                $minimun = $original_minimun;
                if(is_object($order['company_id'])){
                    $company = $_controller->Company->select_one(array('_id'=>new MongoId($order['company_id'])),array('pricing'));
                    if(isset($company['pricing'])&&!empty($company['pricing'])){
                        foreach($company['pricing'] as $pricing){
                            if(isset($pricing['deleted'])&&$pricing['deleted']) continue;
                            if((string)$pricing['product_id']!=(string)$product['product_id']) continue;
                            if(!isset($pricing['price_break']) || empty($pricing['price_break'])) continue;
                            $price_break = reset($pricing['price_break']);
                            $minimun = (float)$price_break['unit_price']; break;
                        }
                    }
                } else {
                    $minimun = $original_minimun;
                }
                $arr_companies[(string)$order['company_id']] = $minimun;
            } else
                $minimun = $arr_companies[(string)$order['company_id']];
        ?>
        <li class="hg_padd" style="width:.5%">
            <a href="<?php echo URL; ?>/purchaseorders/entry/<?php echo $order['_id']; ?>"><span class="icon_emp"></span></a>
        </li>
        <li class="hg_padd center_txt" style="width:5%"><?php echo $order['code']; ?></li>
        <li class="hg_padd" style="width:15%">
            <?php if (isset($order['company_id']) && is_object($order['company_id']) ) { ?>
                <a style="text-decoration: none;" href="<?php echo URL; ?>/companies/entry/<?php echo $order['company_id']; ?>">
                    <?php echo $order['company_name']; ?>
                </a>
            <?php } ?>
        </li>

        <li class="hg_padd center_txt" style="width:8%"><?php echo $this->Common->format_date($order['purchord_date']->sec, false); ?></li>

        <li class="hg_padd right_txt" style="width:8%;">
            <?php
                echo $this->Common->format_currency($order['sum_amount']);
            ?>
        </li>

        <!-- <li class="hg_padd right_txt" style="width:8%;">
            <?php
            if(isset($order['sum_amount_interest']))
                echo $this->Common->format_currency($order['sum_amount_interest']);
            else
                echo 0;
            ?>
        </li> -->

        <li class="hg_padd" style="width:8%">
            <?php echo $order['purchase_orders_status']; ?>
        </li>
        <li class="hg_padd bor_mt" style="width:.5%">
            <?php if($delete){ ?>
            <div class="middle_check">
                <a href="javascript:void(0)" title="Delete link" onclick="salesorders_lists_delete('<?php echo $order['_id']; ?>')">
                    <span class="icon_remove2"></span>
                </a>
            </div>
        <?php } ?>
        </li>
    </ul>
<?php }
if(count($arr_companies) > $count)
    Cache::write('arr_companies',$arr_companies);
echo $this->element('popup/pagination_lists');
?>