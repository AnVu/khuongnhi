<?php
App::uses('Controller', 'Controller');
class AppController extends Controller {
	var $components = array('Session', 'Common');
	var $helpers = array('Html', 'Form', 'Common');
	public $viewClass = 'Theme';
	public $theme = 'default';
	var $uses = null;
	var $modelName = '';

	function beforeFilter() {
			if ($this->request->is('ajax') && !$this->Session->check('arr_user')) {
				echo 'Your session is time out, please re-login. Thank you.<script type="text/javascript">location.reload(true);</script>';
				exit;
			}
			if ($this->request->url != 'users/login' && !$this->Session->check('arr_user')) {
				$this->redirect('/users/login');
				die;
			}
		$this->set('controller', $this->params->params['controller']);
		$this->set('model', $this->modelName);
		$this->set('action', $this->params->params['action']);
	}
	function beforeRender() {
		if ($this->request->is('ajax')) {
			$this->set('ajax', true);
			$this->layout = 'ajax';
		}
		if (is_object($this->db)) {
			// tạm thời không ngắt kết nối để tối ưu các request đỡ phải mất thời gian kết nối nhiều lần, vd: các ajax trên page chẳng hạn
			// if (!$this->mongo_disconnect()) {
			//     die('Can not disconnect mongodb!');
			// }
		}
	}

	protected $db = null;
	private $connectionDB = null;

	function mongo_connect() {
		if (!is_object($this->connectionDB)) {
			// http://docs.mongodb.org/manual/reference/connection-string/#connections-standard-connection-string-format
			// set Timeout và không cần close nữa
			$this->connectionDB = new Mongo(IP_CONNECT_MONGODB . ':27017?connectTimeoutMS=300000');
			$this->db = $this->connectionDB->selectDB(DB_CONNECT_MONGODB);
		}
	}

	function mongo_disconnect() {
		// $this->db->command(array("logout" => 1));
		return $this->connectionDB->close();
	}

	function selectModel($model) {
		$this->mongo_connect();
		if (is_object($this->db) && !is_object($this->$model)) {
			if (file_exists(APP . 'Model' . DS . $model . '.php')) {
				require_once APP . 'Model' . DS . $model . '.php';
				$this->$model = new $model($this->db);
			} else {
				//echo 'File ' . APP.'Model'.DS.$model.'.php' . ' does not exist';die;
			}
		}
	}
}