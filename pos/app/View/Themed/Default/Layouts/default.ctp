<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" >
<head>
	<?php echo $this->Html->charset(); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	<title>Control POS</title>
	<?php
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('style');
		echo $this->Html->css('jt_vunguyen');
		echo $this->Html->css('jquery-ui');
		echo $this->Html->css('kendo/kendo.common.min');
		echo $this->Html->css('kendo/kendo.anvy.min');

		echo $this->Html->script('jquery-1.10.2.min');
		echo $this->Html->script('jquery-ui');
		echo $this->Html->script('jquery.bpopup.min');
	?>

	 <script type="text/javascript">
	      $(document).ready(function(){
	      	$(".language").click(function(e){
	      		$(".dropdown_list").slideToggle(200);
	      		e.stopPropagation();
	      	})
	      	$(document).click(function(){
	      		if($(".dropdown_list").is(':visible')){
	      			$(".dropdown_list", this).slideUp();
	      		}
	      	})
	      });
	</script>

</head>

<body>
	<?php
		echo $this->fetch('content');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('main.js');
		echo $this->Html->script('kendo/kendo.web');
		echo $this->Html->script('jquery.slimscroll');
		echo $this->Html->script('kendo/kendo.all.min');
	?>
    <script type="text/javascript">
	    $(function(){
	    	if($('#scroll_div').attr('id')!=undefined)
		      $('#scroll_div').slimScroll({
		          alwaysVisible: true
		      });
	    });
		</script>
</body>
</html>