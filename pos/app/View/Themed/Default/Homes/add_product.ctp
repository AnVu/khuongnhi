<table class="table">
        <h2>
          <div class="title_details">
            <input type="hidden" id="order_id" value="<?php if(isset($order['_id'])){ echo $order['_id']; } ?>">
            <input type="text" id="order_name" class="h2_title" onchange="edit_order()" placeholder="Type new order" value="<?php if(isset($order['name'])){ echo $order['name']; } ?>">
           </div>
        </h2>
        <script type="text/javascript">
            function add_new_order(){
               // var dNow = new Date();
                //var localdate= (dNow.getMonth()+1) + '-' + dNow.getDate() + '-' + dNow.getFullYear() + '_' + dNow.getHours() + ':' + dNow.getMinutes();
                //$(".title_details input").val("");
                $("#list_product_of_order").html("");
                $("#total_price").html("");
                $("#total").html("");
                $("#order_id").val("");
                //$("#order_name").val(localdate).trigger("change");
                $("#order_name").val("");
            }
            function edit_order(){
                var order_name = $("#order_name").val();
                if(order_name.length > 0){
                    $.ajax({
                        url: '<?php echo URL; ?>/homes/edit_order/' + order_name + "/" + $("#order_id").val(),
                        timeout: 15000,
                        success: function(html) {
                            if($("#order_id").val() == ""){
                                // add new
                                $("#order_id").val(html);
                                var contain = $("#sum_price");
                                $("#total_price", contain).html("0");
                                $("#total", contain).html("0");
                            }else{
                                // update
                                if(html != 'ok'){
                                    alert("Error: " + html);
                                }
                            }
                        }
                    });
                }
            }
        </script>
        <thead class="order_detailed reset_width_table">
          <tr>
            <th><?php if(isset($arr_language['no'])) echo $arr_language['no'];?></th>
            <th><?php if(isset($arr_language['products'])) echo $arr_language['products'];?></th>
            <th><?php if(isset($arr_language['qty'])) echo $arr_language['qty'];?></th>
            <th><?php if(isset($arr_language['price'])) echo $arr_language['price'];?></th>
            <th><?php if(isset($arr_language['Total'])) echo $arr_language['Total'];?></th>
          </tr>
        </thead>
      </table>

      <div id="scroll_div" class="block_dent">
      	<table class="table reset_table">
	        <tbody id="list_product_of_order">
	        	<?php
	        		$STT = 1;
	        		$sum_price = 0;
	        		if(isset($order['products']))
	        			foreach($order['products'] as $key => $value){
	        				$sum_price += $value['price']*$value['qty'];
	        	 ?>
	          <tr>
	          	<td>
		          	<?php
		          		if(strlen($STT)===1){
		          			echo '00';
		          		}if(strlen($STT)===2){
		          			echo '0';
		          		}
		          		echo $STT++;
		          	?>
	          	</td>
	          	<td>
	          		<input type="text" value="<?php echo $value['name'];?>" onchange="update_product_order(this,'<?php echo $value['product_id'];?>','name')"/>
	          	</td>
	            <td>
	            	<input type="text" value="<?php echo $value['qty']; ?>" class="center_txt" onchange="update_product_order(this, '<?php echo $value['product_id']; ?>', 'qty')">
	            </td>
	            <td>
	            	<input readonly="true" type="text" value="<?php echo number_format($value['price'],2); ?>$" class="center_txt" onchange="update_product_order(this, '<?php echo $value['product_id']; ?>', 'price')">
	            </td>
	            <td>
	            	<input readonly="true" type="text" value="<?php echo number_format($value['price']*$value['qty'],2); ?>$" class="center_txt" onchange="update_product_order(this, '<?php echo $value['product_id']; ?>', 'price')">
	            </td>
	          </tr>
	          <?php  } ?>
	        </tbody>
        </table>
      </div>
      <div class="price_m sum_price" id="sum_price">
        <table class="table">
          <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td><?php if(isset($arr_language['Total_Price'])) echo $arr_language['Total_Price'];?></td>
              <td id="total_price"><?php echo number_format($sum_price,2);?> $</td>
            <tr>
              <td><?php if(isset($arr_language['Tax_Breakdown'])) echo $arr_language['Tax_Breakdown'];?><br/>CND 21%</td>
              <td></td>
              <td></td>
              <td></td>
              <td>0.00$</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td><?php if(isset($arr_language['Total_Price'])) echo $arr_language['Total_Price'];?></td>
              <td id="total"><?php echo number_format($sum_price,2);?> $</td>
            </tr>
          </tbody>
        </table>
      </div>