<nav class="top_header">
	<ul>
	    <li><a href="" title=""><i class="fa fa-caret-down"></i></a></li>
	    <li><a href="javascript:void(0)" onclick="add_new_order()"><i class="fa fa-plus"></i>new order</a></li>
	    <li class="icon-logo"></li>
	  </ul>
 </nav>
<table class="table">
        <h2>
          <div class="title_details">
            <input type="hidden" id="order_id" value="<?php if(isset($order['_id'])){ echo $order['_id']; } ?>">
            <input type="text" id="order_name" class="h2_title" onchange="edit_order()" placeholder="Type new order" value="<?php if(isset($order['name'])){ echo $order['name']; } ?>">
           </div>
        </h2>
        <script type="text/javascript">
            function add_new_order(){
                $(".title_details input").val("");
                $("#list_product_of_order").html("");
                $("#total_price").html("");
                $("#total").html("");
                $("#order_id").val("");
            }
            function edit_order(){
                if($("#order_name").val().length > 0)
                    $.ajax({
                        url: '<?php echo URL; ?>/homes/edit_order/' + $("#order_name").val() + "/" + $("#order_id").val() ,
                        timeout: 15000,
                        success: function(html) {
                            if($("#order_id").val() == ""){
                                // add new
                                $("#order_id").val(html);
                                var contain = $("#sum_price");
                                $("#total_price", contain).html("0");
                                $("#total", contain).html("0");
                            }else{
                                // update
                                if(html != 'ok'){
                                    alert("Error: " + html);
                                }
                            }
                        }
                    });
            }
        </script>
        <thead class="order_detailed reset_width_table">
          <tr>
            <th>STT</th>
            <th>Tên sản phẩm</th>
            <th>SL</th>
            <th>Giá SP</th>
            <th>Tổng</th>
          </tr>
        </thead>
      </table>

      <div id="scroll_div" class="block_dent">
      	<table class="table reset_table">
	        <tbody id="list_product_of_order">
	        	<?php
	        		$STT = 1;
	        		$sum_price = 0;
	        		if(isset($list))
	        			foreach($list['products'] as $k => $v){
	        				$sum_price += $v['price']*$v['qty'];
	        	 ?>
	          <tr>
	          	<td>
		          	<?php
		          		if(strlen($STT)===1){
		          			echo '00';
		          		}if(strlen($STT)===2){
		          			echo '0';
		          		}
		          		echo $STT++;
		          	?>
	          	</td>
	          	<td>
	          		<input type="text" readonly value="<?php echo $v['name']; ?>" >
	          	</td>
	            <td>
	            	<input type="text" readonly value="<?php echo $v['qty'];  ?>" class="center_txt" >
	            </td>
	            <td>
	            	<input type="text" readonly value="<?php echo $v['price'];  ?>" class="center_txt" >
	            </td>
	            <td>
	            	<input type="text" readonly value="<?php echo $v['price']*$v['qty'];  ?>" class="center_txt" >
	            </td>
	          </tr>
	          <?php } ?>
	        </tbody>
        </table>
      </div>
      <div class="price_m sum_price" id="sum_price">
        <table class="table">
          <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td>Total Price:</td>
              <td id="total_price"><?php echo number_format($sum_price);?> $</td>
            <tr>
              <td>Tax Breakdown <br/>CND 21%</td>
              <td></td>
              <td></td>
              <td></td>
              <td>100$</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td>Total Price:</td>
              <td id="total" style="text-align: right"><?php echo number_format($sum_price);?> $</td>
            </tr>
          </tbody>
        </table>
      </div>