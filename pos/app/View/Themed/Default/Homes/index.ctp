<div class="col-xs-6 left_container">
    <div id="ajax_list_view">
      <nav class="top_header">
        <ul>
            <li><a href="" title=""><i class="fa fa-caret-down"></i></a></li>
            <li>
                <a href="javascript:void(0)" onclick="add_new_order()" ><i class="fa fa-plus" ></i>
                        <?php if(isset($arr_language['new_order'])) echo $arr_language['new_order'];?>
                </a>
            </li>

            <li><a href=" <?php echo URL;?>/users/logout">Logout</a></li>
            <li class="icon-logo"></li>
          </ul>

      </nav>
    <div class="table-responsive" id="div_order_box">
        <?php echo $this->element('../Homes/add_product'); ?>

    </div>
    </div>
    <nav class="top_header bottom_nav">
        <ul id="pos_menu_id">
            <li><a href="<?php echo URL; ?>/settings" id="settings" ><?php if(isset($arr_language['setup_price'])) echo $arr_language['setup_price'];?></a></li>
            <li><a href="javascript:void(0)" id="view_list" onclick="pos_menu(this,'view_list')" class="view_list"><?php if(isset($arr_language['view_list'])) echo $arr_language['view_list'];?></a></li>
            <li><a href="" title=""><?php if(isset($arr_language['print'])) echo $arr_language['print'];?></a></li>
            <li><a href="javascript:void(0)" id="check_out" onclick="check_out()" class="check_out"><?php if(isset($arr_language['check_out'])) echo $arr_language['check_out'];?></a></li>
          </ul>
      </nav>
    </div>
    <div class="col-xs-6 right_container">
      <div class="search">
        <form action=""  accept-charset="utf-8" class="forms-controls" onsubmit="search_product();return false">
          <input id="search_product_input" type="text" class="form-control" placeholder="<?php if(isset($arr_language['search_barcode'])) echo $arr_language['search_barcode'];?>">
        </form>
          <div class="col-xs-12">
            <div class="col-xs-9 sub_list_category">
                <select id="choose_category" class="form-control" onchange="choose_category($(this).val());">
                    <option value="all">All</option>
                <?php
                    foreach($product_category as $id=>$value)
                        echo '<option value="'.$id.'">'.$value.'</option>';
                ?>
                </select>
            </div>
            <div class="col-xs-3">
              <div class="col-xs-6"><a class="thumbs_clk" href="javascript:void(0)" title=""><i class="fa fa-th"></i></a></div>
              <div class="col-xs-6"><a class="list_clk active" href="javascript:void(0)" title=""><i class="fa fa-list"></i></a></div>
            </div>
          </div>
      </div>
      <div class="clear"></div>
      <div id="list_product" class="table-responsive">
        <?php echo $this->element('../Homes/get_product_from_category'); ?>
      </div>
    </div>

<script type="text/javascript">
    function update_product_order(object, product_id, field){
        $.ajax({
            url: '<?php echo URL; ?>/homes/update_product_order/?product_id=' + product_id + "&field=" + field + "&order_id=" + $("#order_id").val() + "&new_value=" + $(object).val(),
            timeout: 15000,
            success: function(html){
                if(html != "ok")
                    alerts("Error: " + html);
                if(field == "qty")
                    $.ajax({
                        url: '<?php echo URL;?>/homes/reload_product/' + $("#order_id").val(),
                        timeout: 15000,
                        success: function(html){
                            $(".div_order_box").html(html);
                        }
                    });
            }
        });
    }

    function choose_category(category_id){
        $.ajax({
            url: '<?php echo URL;?>/homes/get_product_from_category/'+category_id,
            timeout: 15000,
            success: function(html){
                $("#list_product").html(html);
            }
        });
    }

    function add_new_line(product_id,quantity_id,order_id){
        if($("#order_name").val() == ''){
            alert('Order name not blank!');exist();
        }
        if(product_id==undefined)
            product_id = $("tr:first","#list_product").attr('rel');
        if(quantity_id==undefined)
            quantity = 1;
        else
            quantity = $("#"+quantity_id).val();
        if(order_id==undefined)
            order_id = $("#order_id").val();
        $.ajax({
            url: '<?php echo URL;?>/homes/add_product/' + product_id  + "/"+quantity+"/" + order_id,
            timeout: 15000,
            success: function(html){
                if(html == 'Order or product does not exist!')
                    alert(html);
                else{
                    $("#div_order_box").html(html);
                    $('#scroll_div').slimScroll({
                          alwaysVisible: true
                      });
                }
            }
        });
    }

    function search_product(){
        $.ajax({
            url:'<?php echo URL;?>/homes/get_product_name/' + $("#search_product_input").val(),
            timeout: 15000,
            success: function(html){
                if(html == '123')
                    alert(html);
                else{
                    $("#list_product").html(html);
                    add_new_line();
                }
                $("#search_product_input").val('');
                $("#search_product_input").focus();
            }
        });
    }

    function pos_menu(object, pos_menu){
        $("#pos_menu_id a").removeClass("active");
        $(object).addClass("active");
        $.ajax({
            url: "<?php echo URL;?>/homes/" + pos_menu,
            timeout: 15000,
            success: function(html){
                if(pos_menu == 'check_out'){
                    if($("#receipt_popup").attr("id")==undefined)
                        $("body").append("<div id=\"receipt_popup\">"+html+"</div>");
                    else
                        $("#receipt_popup").html(html);
                     $('#element_to_pop_up').bPopup({
                        easing: 'easeOutBack', //uses jQuery easing plugin
                            speed: 450,
                            transition: 'slideDown'
                        });
                }
                else
                    $("#ajax_list_view").html(html);
            }
        });
    }

    function view_list_detail(order_id, order_name){
        $.ajax({
            url:'<?php echo URL; ?>/homes/view_list_detail/' + order_id,
            timeout: 15000,
            success: function(html){
                $("#ajax_list_view").html(html);
                $("#order_id").val(order_id);
                $("#order_name").val(order_name);
                $('#scroll_div').slimScroll({
                  alwaysVisible: true
                });
            }
        });
    }

    function display_order(ids){
        if(ids=='now'){
            $(".now_orders").removeClass('selected');
            $(".old_orders").addClass('selected');
            $(".now_order_detail").css("display","none");
            $(".old_order_detail").css("display","block");
            $("#check_out").css("display","none");
            if($("#tmp_div").attr("id")==undefined)
                $("<div id=\"tmp_div\" class=\"col-xs-6 right_container\"></div>").appendTo("body").css({"z-index":"1000","background-color":"rgba(114, 112, 110, 0.28)","left": "50%","float": "right","position":"fixed"});
            else
                $("#tmp_div").css("display","block");
        }else if(ids=='old'){
            $(".old_orders").removeClass('selected');
            $(".now_orders").addClass('selected');
            $(".now_order_detail").css("display","block");
            $(".old_order_detail").css("display","none");
            $("#check_out").css("display","block");
            $("#tmp_div").css("display","none");
        }
    }

    function check_out(){
        $.ajax({
            url:'<?php echo URL;?>/homes/check_out/' + $("#order_id").val(),
            timeout: 15000,
            success: function(html){
                if($("#receipt_popup").attr("id")==undefined)
                    $("body").append("<div id=\"receipt_popup\">"+html+"</div>");
                else
                 $("#receipt_popup").html(html);
                 $('#element_to_pop_up').bPopup({
                    easing: 'easeOutBack', //uses jQuery easing plugin
                        speed: 450,
                        transition: 'slideDown'
                    });
            }
        });
    }

    function settings(){
        $.ajax({
            url:'<?php echo URL;?>/homes/settings',
            timeout: 15000,
            success: function(html){

            }
        });
    }

</script>