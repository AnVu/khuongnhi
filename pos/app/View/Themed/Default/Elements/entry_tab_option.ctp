<div class="bg_menu">
	<ul class="menu_control float_left">

		<?php if( !in_array( $controller, array('salesaccounts') ) ){ ?>
		<?php
		if($this->Common->check_permission($controller.'_@_entry_@_add', $arr_permission)):?>
			<li><a href="<?php echo URL; ?>/<?php echo $controller; ?>/add">New</a></li>
		<?php endif; ?>
		<?php } ?>

		<?php if( $action == 'lists' ){  ?>
			<li><a href="<?php echo URL; ?>/<?php echo $controller; ?>/entry_search">Find</a></li>

		<?php }elseif( $action == 'entry' || $action == 'options'){ ?>
			<?php if( $action == 'entry' && (!isset($no_show_delete) || !$no_show_delete) ){ ?>
				<?php if($this->Common->check_permission($controller.'_@_entry_@_delete', $arr_permission) ):?>
					<li><a onclick='!delete_entry_http(this); return false;' href="<?php echo URL; ?>/<?php echo $controller; ?>/delete/<?php echo isset($this->data[$model]['_id'])?$this->data[$model]['_id']:'';?>">Delete</a></li>
				<?php endif; ?>
			<?php } ?>
			<li><a href="<?php echo URL; ?>/<?php echo $controller; ?>/entry_search">Find</a></li>

		<?php }elseif( $action != 'products_pricing' ){ ?>
			<li><a href="<?php echo URL; ?>/<?php echo $controller; ?>/entry_search">Find</a></li>
			<li><a href="javascript:void(0)" onclick="mainjs_entry_search_ajax('<?php echo $controller; ?>');return false;">Continue</a></li>
			<li><a href="<?php echo URL; ?>/<?php echo $controller.'/entry'; ?>">Cancel</a></li>

		<?php } ?>

		<?php if( $this->Session->check($controller.'_entry_search_cond') ){ ?>
        	<li><a href="<?php echo URL; ?>/<?php echo $controller; ?>/entry_search_all">Find all</a></li>

        <?php } ?>
        <li><a href="<?php echo URL.'/'.$controller.'/support/'; ?>" class="icon support">Support</a></li>
        <li><a href="<?php echo URL.'/'.$controller.'/history/'; ?>" class="icon history end">History</a></li>
	</ul>
	<ul class="menu_control2 float_right">
		<?php if( $this->Common->check_permission($controller.'_@_entry_@_view', $arr_permission) ): ?>
		<li><a href="<?php echo URL; ?>/<?php echo $controller; ?>/entry" class="<?php if($action == 'entry'){ ?>active<?php } ?>">Entry</a></li>
		<li><a href="<?php echo URL; ?>/<?php echo $controller; ?>/lists" class="<?php if($action == 'lists'){ ?>active<?php } ?>">List</a></li>
		<?php endif; ?>
		<?php if($this->Common->check_permission($controller.'_@_options_@_',$arr_permission,true)): ?>
		<li class="com_den"><a  href="<?php echo URL; ?>/<?php echo $controller; ?>/options" class="<?php if($action == 'options'){ ?>active<?php } ?>">Options</a></li>
		<?php endif; ?>
	</ul>
</div>

<script type="text/javascript">
	function delete_entry_http(object){
		!confirms( "Message", "Are you sure you want to delete?",
			function(){ location.href = $(object).attr("href"); },
			function(){  }
		);
	}
</script>