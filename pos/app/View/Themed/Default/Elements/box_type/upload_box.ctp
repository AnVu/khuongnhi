<?php
	$block =  $arr_settings['relationship'][$sub_tab]['block'][$blockname]['field'];
?>

<?php if(isset($img_path)){?>
    <div class="box_image" style="width:100%; padding:0; height:147px; vertical-align:middle;">
        <a href="<?php echo URL.'/docs/entry/'.$doc_id;?>"><img src="<?php echo URL.$img_path;?>" alt="Images" style=" max-width:100%; max-height:100%;" /></a>
    </div>
<?php }else{?>

    <div style=" width:100%; height:100%; display:table; text-align:center; font-size:11px; vertical-align:middle; <?php if(isset($block['files']['css'])) echo $block['files']['css']; ?>">
        <?php if(isset($block['files']['name'])) echo '<br /><br /><br /><br />'.$block['files']['name']; ?>
    </div>

<?php }?>
