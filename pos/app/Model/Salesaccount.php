<?php
require_once APP.'Model'.DS.'AppModel.php';
class Salesaccount extends AppModel {
	public function __construct($db) {
		if(is_object($db)){
			$this->collection = $db->selectCollection('tb_salesaccount');
			$this->collection->ensureIndex(array('deleted'=>1), array('name'=>'deleted_id_key'));
			$this->db = $db;
		}
	}

	// kết hợp cập nhật và thêm mới chung 1 function
	public function update_account($id, $option = array()){
		if( !is_object($id) )$id = new MongoId($id);

		// kiểm tra là Company hay Contact
		$arr_save = array();
		if( !isset($option['model']) ){
			$arr_save = $this->select_one(array('_id' => $id));
		}else{
			if( $option['model'] == 'Company' ){
				$arr_save = $this->select_one(array('company_id' => $id));
			}elseif( $option['model'] == 'Contact' ){
				$arr_save = $this->select_one(array('contact_id' => $id));
			}
		}

		// tự động thêm mới SA nếu chưa tồn tại
		if( !isset($arr_save['_id']) ){
			if( $option['model'] == 'Company' ){
				$this->arr_default_before_save = array('company_id' => $id);
			}elseif( $option['model'] == 'Contact' ){
				$this->arr_default_before_save = array('contact_id' => $id);
			}
			$arr_save = $this->add();
		}

		// update balance, invoices_credits, receipts
		if( isset($option['balance']) ){
			$arr_save['balance'] = (float)$arr_save['balance'] + $option['balance'];
		}
		if( isset($option['invoices_credits']) ){

			$arr_save['invoices_credits'] = (float)$arr_save['invoices_credits'] + $option['invoices_credits'];
		}
		if( isset($option['receipts']) ){
			$arr_save['receipts'] = (float)$arr_save['receipts'] + $option['receipts'];
		}

		if (!$this->save($arr_save)) {
			echo 'Error: ' . $this->arr_errors_save[1]; die;
		}
		return $arr_save;
	}

	public $arr_default_before_save = array();
	public function add() {
		$arr_tmp = $this->select_one(array(), array(), array('no' => -1));
		$arr_save = array();
		$arr_save['no'] = 1;
		if (isset($arr_tmp['no']))
			$arr_save['no'] = $arr_tmp['no'] + 1;
		$arr_save['status'] = 'Current';
		$arr_save['status_id'] = 'Current';
		$arr_save['balance'] = 0;
		$arr_save['invoices_credits'] = 0;
		$arr_save['receipts'] = 0;
		$arr_save['credit_limit'] = 0;
		$arr_save['invoices_credits'] = 0;
		$arr_save['quotation_limit'] = 0;
		$arr_save['difference'] = 0;
		$arr_save['payment_terms'] = $arr_save['payment_terms_id'] = '';
		$arr_save['tax_code'] = $arr_save['tax_code_id'] = '';
		$arr_save['nominal_code'] = $arr_save['nominal_code_id'] = '';
		$arr_save = array_merge($arr_save, $this->arr_default_before_save);
		if ($this->save($arr_save)) {
			$arr_save['_id'] = $this->mongo_id_after_save;
			return $arr_save;
		} else {
			echo 'Error: ' . $this->arr_errors_save[1];die;
		}
	}
}