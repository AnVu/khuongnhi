<?php
require_once APP.'Model'.DS.'AppModel.php';
class Company extends AppModel {
	public function __construct($db) {
		if(is_object($db)){
			$this->collection = $db->selectCollection('tb_company');
			$this->collection->ensureIndex(array('deleted'=>1), array('name'=>'deleted_id_key'));
			// $this->collection->ensureIndex(array('name'=>1, 'caseInsensitive' => false), array('name'=>'name_id_key'));
			$this->collection->ensureIndex(array("lower('name')" =>1, 'caseInsensitive' => true), array('name'=>'name_id_key'));

			$this->db = $db;
		}
	}

	public $arr_default_before_save = array();
	public function add() {
		$arr_tmp = $this->select_one(array(), array(), array('no' => -1));
		$arr_save = array();
		$arr_save['no'] = 1;
		if (isset($arr_tmp['no']))
			$arr_save['no'] = $arr_tmp['no'] + 1;
		$arr_save['web'] = '';
		$arr_save['name'] = '';
		$arr_save['is_customer'] = 1;
		$arr_save['is_supplier'] = 0;
		$arr_save['phone'] = '';
		$arr_save['addresses'] = array(
			array(
				'name' => '',
				'deleted' => false,
				'default' => true,
				'country' => 'Canada',
				'country_id' => "CA",
				'province_state' => '',
				'province_state_id' => '',
				'address_1' => '',
				'address_2' => '',
				'address_3' => '',
				'town_city' => '',
				'zip_postcode' => ''
			)
		);
		$arr_save['addresses_default_key'] = 0;
		$arr_save['our_rep'] = $_SESSION['arr_user']['contact_name'];
		$arr_save['our_rep_id'] = $_SESSION['arr_user']['contact_id'];
		$arr_save['our_csr'] = $_SESSION['arr_user']['contact_name'];
		$arr_save['our_csr_id'] = $_SESSION['arr_user']['contact_id'];

		$arr_save = array_merge($arr_save, $this->arr_default_before_save);
		// pr($arr_save);die;
		if ($this->save($arr_save)) {
			return $this->mongo_id_after_save;
		} else {
			echo 'Error: ' . $this->arr_errors_save[1];die;
		}
	}
}