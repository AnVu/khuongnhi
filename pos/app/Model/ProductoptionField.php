<?php
$ModuleField = array();
$ModuleField = array(
	'module_name' 	=> __('Productoption'),
	'module_label' 	=> __('Product Option'),
	'colection' 	=> 'tb_productoption',
	'title_field'	=> array('code','name','status','description'),
);

//============= *** FIELDS *** =============//

// Panel 1
$ModuleField['field']['panel_1'] = array(
	'setup'	=> array(
			'css'	=> 'width:100%;',
			'lablewith' => '18',
			'blockcss' => 'width:49%;float:left;',
			),

	'code'	=>array(
			'name' 		=> __('Code'),
			'type' 		=> 'text',
			'moreclass' => 'fixbor',
			'listview'	=>	array(
								'order'	=>	'1',
								'with'	=>	'3',
								'align'	=>	'center',
								'css' => 'width:3%;',
							),
			),

	'name'	=>array(
			'name' 		=>  __('Name'),
			'type' 		=> 'text',
			'listview'	=>	array(
								'order'	=>	'1',
								'with'	=>	'15',
								'css' => 'width:15%;',
						),
			),

	'mongo_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
			),

	'date_modified'=>array(
			'type' 		=> 'hidden',
			),

	'created_by'=>array(
			'type' 		=> 'hidden',
			),

	'modified_by'=>array(
			'type' 		=> 'hidden',
			),
	'types'	=>array(
			'name' 		=>  __('Option type'),
			'type' 		=> 'select',
			'droplist'	=> 'productoption_option_type',
			'default'	=> 'S',
			'listview'	=>	array(
								'order'	=>	'1',
								'with'	=>	'8',
								'css' => 'width:8%;',
						),
			),

	'field_types'	=>array(
			'name' 		=>  __('Field type'),
			'type' 		=> 'select',
			'droplist'	=> 'productoption_field_type',
			'default'	=> 'select',
			'moreclass' => 'fixbor2',
			'listview'	=>	array(
								'order'	=>	'1',
								'with'	=>	'8',
								'css' => 'width:8%;',
						),
			),


);


// Panel 2
$ModuleField['field']['panel_2'] = array(
	'setup'	=> array(
			'css'	=> 'width:100%;',
			'lablewith' => '8',
			'blockcss' => 'width:50%;float:right;',
			),

	'status'	=>array(
			'name' 		=>  __('Status'),
			'type' 		=> 'select',
			'droplist'	=> 'product_statuses',
			'default'	=> '1',
			'moreclass' => 'fixbor',
			'listview'	=>	array(
								'order'	=>	'1',
								'with'	=>	'10',
								'css' => 'width:10%;',
						),
			),

	'parent'	=>array(
			'name' 		=>  __('Parent'),
			'type' 		=> 'relationship',
			'id'		=> 'parent_id',
			'cls'		=> 'productoptions',
			'with'		=> '100',
			'listview'	=>	array(
								'order'	=>	'1',
								'with'	=>	'10',
								'css' => 'width:10%;',
						),
			),

	'parent_id'	=>array(
			'type' 		=> 'id',
			'element_input' => ' class="jthidden"',
			),
	'description'=>array(
			'name' 		=>  __('Description'),
			'type' 		=> 'text',
			'listview'	=>	array(
								'order'	=>	'1',
								'with'	=>	'34',
								'css' => 'width:34%;',
						),
			),
	'none'	=>array(
			'type' 		=> 'not_in_data',
			'moreclass' => 'fixbor2',
			),
);




//============ *** RELATIONSHIP *** =============//

//====== GENERAL =======//
$ModuleField['relationship']['general']['name'] =  __('General');

//Parent List
$ModuleField['relationship']['general']['block']['valuelist'] = array(
	'title'		=> __('Option value lists'),
	'css'		=> 'width:33%;',
	'height' 	=> '300',
	'add'		=>'Add value',
	'type'		=> 'listview_box',
	'reltb'		=> 'tb_productoption@valuelist',//tb@option
	'deleted' 	=> '5',
	'field'		=> array(
				'valuelist_name' => array(
					'name' 		=>  __('Label'),
					'width'=>'33',
					'edit' => '1',
				),
				'op_value' => array(
					'name' 		=>  __('Key'),
					'width'=> '33',
				),
				'default' => array(
					'name' 		=>  __('Default'),
					'type'	=> 'checkbox',
					'width'=>'15',
					'align' => 'center',
				),
	),
);


//Parent List
$ModuleField['relationship']['general']['block']['childlist'] = array(
	'title'		=> __('Child List'),
	'css'		=> 'width:32%;margin-left:1%;',
	'height' 	=> '300',
	//'add'		=>'Add Options Child',
	'type'		=> 'listview_box',
	'link'		=> array('w'=>'5', 'cls'=>'productoptions'),
	'deleted' 	=> '5',
	'field'		=> array(
				'code' => array(
					'name' 		=>  __('Code'),
					'width'=>'8',
					'align' => 'center',
				),
				'name' => array(
					'name' 		=>  __('Name'),
					'width'=>'30',

				),
				'field_types' => array(
					'name' 		=>  __('Field type'),
					'type'	=> 'select',
					'width'=>'20',
				),
				'types' => array(
					'name' 		=>  __('Type'),
					'type'	=> 'select',
					'width'=>'20',
				),
	),
);


//Other Options
$ModuleField['relationship']['general']['block']['otheroption'] = array(
	'title'		=> __('Other Options'),
	'css'		=> 'width:33%; margin-left:1%;',
	'height' 	=> '300',
	//'searchselect'=>'1',
	//'search'	=>'1',
	'type'		=> 'listview_box',
	'link'		=> array('w'=>'4', 'cls'=>'productoptions'),
	'deleted'	=> '3',
	'field'		=> array(
				'code' => array(
					'name' 		=>  __('Code'),
					'width'=>'8',
					'align' => 'center',
				),
				'name' => array(
					'name' 		=>  __('Name'),
					'width'=>'30',

				),
				'field_types' => array(
					'name' 		=>  __('Field type'),
					'type'	=> 'select',
					'width'=>'15',
				),
				'types' => array(
					'name' 		=>  __('Type'),
					'type'	=> 'select',
					'width'=>'15',
				),
				'parent' => array(
					'name' 		=>  __('Parent'),
					'width'=>'15',
				),
	),
);


//====== PRODUCTS =======//
$ModuleField['relationship']['productsetup']['name'] =  __('Products');

// Setup for product type
$ModuleField['relationship']['productsetup']['block']['protype'] = array(
	'title'		=> __('Setup for product types'),
	'css'		=> 'width:30%;',
	'height' 	=> '300',
	'type'		=> 'listview_box',
	'link'		=> array('w'=>'1', 'cls'=>'productoptions'),
	'deleted' 	=> '6',
	'add'		=> 'Add product type',
	'field'		=> array(
				'no' => array(
					'name' 		=>  __('No.'),
					'width'=>'15',
					'align' => 'center',
				),
				'type_name' => array(
					'name' 		=>  __('Type name'),
					'width'=>'35',

				),
				'type_value' => array(
					'name' 		=>  __('Type value'),
					'width'=>'35',
				),
	),
);

// Setup for product items
$ModuleField['relationship']['productsetup']['block']['proitem'] = array(
	'title'		=> __('Setup for product items'),
	'css'		=> 'width:69%; margin-left:1%;',
	'height' 	=> '300',
	'type'		=> 'listview_box',
	'link'		=> array('w'=>'1', 'cls'=>'productoptions'),
	'field'		=> array(
				'code' => array(
					'name' 		=>  __('Code'),
					'width'=>'8',
					'align' => 'center',
				),
				'name' => array(
					'name' 		=>  __('Name'),
					'width'=>'25',

				),
				'types' => array(
					'name' 		=>  __('Type'),
					'width'=>'15',
				),
				'status' => array(
					'name' 		=>  __('Status'),
					'width'=>'15',
				),
				'description' => array(
					'name' 		=>  __('Description'),
					'width'=>'28',
				),
	),
);



//====== DOCUMENTS =======//
//$ModuleField['relationship']['documents']['name'] = __('Documents');


//====== OTHERS =======//
//$ModuleField['relationship']['others']['name'] = __('Others');








$ProductoptionField = $ModuleField;