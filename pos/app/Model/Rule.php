<?php
require_once APP.'Model'.DS.'AppModel.php';
class Rule extends AppModel {
	public $arr_settings = array(
								'module_name' => 'Rule'
							);
	public $arr_temp = array();
	public $temp = '';
	public $arr_key_nosave = array('setup','none','none2','mongo_id');
	public $arr_type_nosave = array('autocomplete','id');

	function __construct($db) {
		$this->_setting();
		if(is_object($db)){
			$this->collection = $db->selectCollection($this->arr_settings['colection']);
			$this->collection->ensureIndex(array('deleted'=>1), array('name'=>'deleted_id_key'));
			$this->db = $db;
		}
	}
}